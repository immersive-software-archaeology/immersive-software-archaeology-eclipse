package dk.itu.cs.isa.model.system.coalescence.sourcecode;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.core.resources.IFile;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.util.ISASystemModelToFileSystemUtil;
import dk.itu.cs.isa.storage.ISAModelStorage;

public class SourceCodeClassifierRequestHandler extends ISAAbstractCoalescenceRequestHandler {

	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		String classifierQualifiedName = baseRequest.getParameter("classifierQualifiedName");
		
		if(systemName == null || classifierQualifiedName == null) {
			String examplePath1 = "systemName=Ant&classifierQualifiedName=org.apache.tools.ant.taskdefs.PropertyHelperTask";
			String examplePath2 = "systemName=ArgoUML&classifierQualifiedName=org.argouml.uml.ui.behavior.state_machines.ActionNewCallEvent";
			String examplePath3 = "systemName=JetUML&classifierQualifiedName=org.jetuml.application.UserPreferences";
			printInvalidParametersHTMLPage(baseRequest, response, null, examplePath1, examplePath2, examplePath3);
			return;
		}
		
		String code;
		JSONObject sortedJSONWithMemberCode;
		try {
			ISASoftwareSystemModel structureModel = ISAModelStorage.INSTANCE.loadStructureModel(systemName);
			if(structureModel == null)
				throw new NullPointerException("Cannot load system model for \"" + systemName + "\". Did the meta model change?");

			ISAClassifier isaClassifier = (ISAClassifier) structureModel.getSystemElementByName(classifierQualifiedName);
			IFile classifierFile = ISASystemModelToFileSystemUtil.getFileForClassifier((ISAClassifier) isaClassifier);
			
			code = new String(classifierFile.getContents().readAllBytes(), classifierFile.getCharset());
			sortedJSONWithMemberCode = constructMemberNameToSourceCodeMapping(isaClassifier, code, 0);
			
			// temporarily:
			code = SourceCodeFormatter.formatCode(code);
		}
		catch(Exception e) {
			e.printStackTrace();

			printHTMLPage(baseRequest, response, HttpServletResponse.SC_NOT_FOUND, "Could not find source file", e,
					"The ISA Eclipse plugins were not able to find a file for that classifier.<br>"
					+ "Please check whether your Eclipse runtime contains projects with respective source files. "
					+ "These must match the qualified names of your loaded ISA models!");
			return;
		}

		response.setContentType("application/json; charset=utf-8");
//		response.setContentType("text/java; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
		PrintWriter out = response.getWriter();
		out.println(sortedJSONWithMemberCode.toString(4));
//		out.println(code);
		out.close();
		
		baseRequest.setHandled(true);
	}

	private JSONObject constructMemberNameToSourceCodeMapping(ISAClassifier isaClassifier, String code, int offset) {
		JSONObject result = new JSONObject();
		JSONArray array = new JSONArray();
		
		List<ISAMember> members = isaClassifier.getContainedMembers();
		if(members.isEmpty()) {
			addNewEntry(array, null, code, offset);
		}
		else {
			int currentPosition = offset;
			for(ISAMember currentMember : members) {
				if(currentMember.getStartPositionInDocument() > currentPosition) {
					// Place intermediate text
					String memberCode = code.substring(currentPosition-offset, currentMember.getStartPositionInDocument()-offset);
					addNewEntry(array, null, memberCode, offset);
				}
				
				String memberCode = code.substring(currentMember.getStartPositionInDocument()-offset, currentMember.getEndPositionInDocument()-offset);
				addNewEntry(array, currentMember, memberCode, offset);
				
				currentPosition = currentMember.getEndPositionInDocument();
			}
			String remainingCode = code.substring(members.get(members.size()-1).getEndPositionInDocument()-offset);
			addNewEntry(array, null, remainingCode, offset);
		}
		
		result.put("entries", array);
		return result;
	}

	private void addNewEntry(JSONArray array, ISAMember member, String memberCode, int offset) {
		JSONObject newEntry;
		if(member != null) {			
			if(member instanceof ISAClassifier) {
				newEntry = constructMemberNameToSourceCodeMapping((ISAClassifier) member, memberCode, member.getStartPositionInDocument());
				newEntry.put("memberQualifiedName", member.getFullyQualifiedName());
			}
			else {
				newEntry = new JSONObject();
				newEntry.put("code", SourceCodeFormatter.formatCode(memberCode));
				newEntry.put("memberQualifiedName", member.getFullyQualifiedName());
			}
		}
		else {
			newEntry = new JSONObject();
			newEntry.put("code", SourceCodeFormatter.formatCode(memberCode));
		}
		array.put(newEntry);
	}

}
























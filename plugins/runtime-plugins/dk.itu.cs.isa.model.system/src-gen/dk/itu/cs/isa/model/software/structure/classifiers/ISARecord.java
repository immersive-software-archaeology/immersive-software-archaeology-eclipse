/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer;

import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Record</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISARecord#getComponents <em>Components</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISARecord()
 * @model
 * @generated
 */
public interface ISARecord extends ISAClassifier, ISAConstructorContainer, ISAClassifierContainer, ISAInterfaceExtendingElement {
	/**
	 * Returns the value of the '<em><b>Components</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.members.ISARecordComponent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Components</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISARecord_Components()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISARecordComponent> getComponents();

} // ISARecord

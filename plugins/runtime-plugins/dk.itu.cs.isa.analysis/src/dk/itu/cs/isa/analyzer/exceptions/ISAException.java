package dk.itu.cs.isa.analyzer.exceptions;

public class ISAException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5966047426563274848L;
	


	public ISAException() {
		super();
	}
	
	public ISAException(String message) {
		super(message);
	}
	
}

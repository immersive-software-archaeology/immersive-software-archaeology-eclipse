package dk.itu.cs.isa.model.quality.sonarqube.preferences;

import java.io.File;

import org.eclipse.core.runtime.Platform;

import dk.itu.cs.isa.preferences.ISAPreferences;

public class ISAPreferencesSonarQube extends ISAPreferences {
	
	public static final String SONAR_QUBE_EXECUTABLE_PATH = "SONAR_QUBE_EXECUTABLE_PATH";
	public static final String SONAR_SCANNER_EXECUTABLE_PATH = "SONAR_SCANNER_EXECUTABLE_PATH";
	
	public static final String SONAR_SCANNER_DEBUG_MODE = "SONAR_SCANNER_DEBUG_MODE";
	
	public static final String SONAR_QUBE_LOGIN = "SONAR_QUBE_LOGIN";
	public static final String SONAR_QUBE_PASSWORD = "SONAR_QUBE_PASSWORD";
	
	

	@Override
    public void initializeDefaultPreferences() {
		initializeDefaultSonarQubePath();
		initializeDefaultSonarScannerPath();
		
		ISAPreferencesSonarQube.setBoolean(ISAPreferencesSonarQube.SONAR_SCANNER_DEBUG_MODE, false);
		
		ISAPreferencesSonarQube.setString(ISAPreferencesSonarQube.SONAR_QUBE_LOGIN, "admin");
		ISAPreferencesSonarQube.setString(ISAPreferencesSonarQube.SONAR_QUBE_PASSWORD, "admin");
	}

	public void initializeDefaultSonarQubePath() {
		ISAPreferencesSonarQube.setString(ISAPreferencesSonarQube.SONAR_QUBE_EXECUTABLE_PATH, findFolderInParentOfWorkspaces("sonarqube", "bin", "windows-x86-64", "StartSonar.bat"));
	}
	
	public void initializeDefaultSonarScannerPath() {
		ISAPreferencesSonarQube.setString(ISAPreferencesSonarQube.SONAR_SCANNER_EXECUTABLE_PATH, findFolderInParentOfWorkspaces("sonar-scanner", "bin", "sonar-scanner.bat"));
	}

	private String findFolderInParentOfWorkspaces(String folderNamePrefix, String ... subFolders) {
		File currentFolder = new File(Platform.getLocation().toOSString());
		currentFolder = currentFolder.getParentFile();
		
		while(currentFolder != null && currentFolder.exists()) {
			File lastMatch = null;
			for(File childFile : currentFolder.listFiles()) {
				if(childFile.getName().startsWith(folderNamePrefix)) {
					lastMatch = childFile;
				}
			}
			if(lastMatch != null) {
				for(String subFolder : subFolders) {
					for(File childFile : lastMatch.listFiles()) {
						if(childFile.getName().equals(subFolder)) {
							lastMatch = childFile;
						}
					}
				}
				return lastMatch.getAbsolutePath();
			}
			
			currentFolder = currentFolder.getParentFile();
		}
		return "";
	}

}

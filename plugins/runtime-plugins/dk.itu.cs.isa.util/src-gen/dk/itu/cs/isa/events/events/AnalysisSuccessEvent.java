/**
 */
package dk.itu.cs.isa.events.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Analysis Success Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.AnalysisSuccessEvent#getCalculationTime <em>Calculation Time</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getAnalysisSuccessEvent()
 * @model
 * @generated
 */
public interface AnalysisSuccessEvent extends OutputEvent {
	/**
	 * Returns the value of the '<em><b>Calculation Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculation Time</em>' attribute.
	 * @see #setCalculationTime(int)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getAnalysisSuccessEvent_CalculationTime()
	 * @model required="true"
	 * @generated
	 */
	int getCalculationTime();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.AnalysisSuccessEvent#getCalculationTime <em>Calculation Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Calculation Time</em>' attribute.
	 * @see #getCalculationTime()
	 * @generated
	 */
	void setCalculationTime(int value);

} // AnalysisSuccessEvent

/**
 */
package dk.itu.cs.isa.multiplayer;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Log</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.EventLog#getLog <em>Log</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getEventLog()
 * @model
 * @generated
 */
public interface EventLog extends EObject {
	/**
	 * Returns the value of the '<em><b>Log</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.multiplayer.AbstractEvent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Log</em>' containment reference list.
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getEventLog_Log()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractEvent> getLog();

} // EventLog

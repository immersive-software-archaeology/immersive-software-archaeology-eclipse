package dk.itu.cs.isa.coalescence.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;



public class ISAStandardPortForwardServer extends ISAAbstractJettyHTTPServer {
	
	public static final int PORT = 80;
	
	public ISAStandardPortForwardServer() {
		init("ISA Standard Port Forward Server", PORT, new AbstractHandler() {

			@Override
			public void handle(String target, Request baseRequest, HttpServletRequest request,
					HttpServletResponse response) throws IOException, ServletException {
				response.sendRedirect("http://" + baseRequest.getServerName() + ":9005" + target);
				baseRequest.setHandled(true);
			}
			
		});
	}
	
}















/**
 */
package dk.itu.cs.isa.multiplayer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Clear All Relations Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getClearAllRelationsEvent()
 * @model
 * @generated
 */
public interface ClearAllRelationsEvent extends AbstractEvent {
} // ClearAllRelationsEvent

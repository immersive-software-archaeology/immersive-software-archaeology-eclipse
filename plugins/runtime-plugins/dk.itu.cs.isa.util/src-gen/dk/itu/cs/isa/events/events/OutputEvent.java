/**
 */
package dk.itu.cs.isa.events.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.OutputEvent#getCause <em>Cause</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getOutputEvent()
 * @model
 * @generated
 */
public interface OutputEvent extends GUIEvent {
	/**
	 * Returns the value of the '<em><b>Cause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cause</em>' attribute.
	 * @see #setCause(Object)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getOutputEvent_Cause()
	 * @model transient="true"
	 * @generated
	 */
	Object getCause();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.OutputEvent#getCause <em>Cause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cause</em>' attribute.
	 * @see #getCause()
	 * @generated
	 */
	void setCause(Object value);

} // OutputEvent

/**
 */
package dk.itu.cs.isa.model.software.structure.misc.impl;

import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAParameterImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAParameterImpl#isArrayType <em>Array Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAParameterImpl#isVarArg <em>Var Arg</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAParameterImpl extends ISANamedSoftwareElementImpl implements ISAParameter {
	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ISAType type;

	/**
	 * The default value of the '{@link #isArrayType() <em>Array Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArrayType()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ARRAY_TYPE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isArrayType() <em>Array Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArrayType()
	 * @generated
	 * @ordered
	 */
	protected boolean arrayType = ARRAY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #isVarArg() <em>Var Arg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVarArg()
	 * @generated
	 * @ordered
	 */
	protected static final boolean VAR_ARG_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isVarArg() <em>Var Arg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVarArg()
	 * @generated
	 * @ordered
	 */
	protected boolean varArg = VAR_ARG_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiscPackage.Literals.ISA_PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (ISAType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MiscPackage.ISA_PARAMETER__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetType(ISAType newType, NotificationChain msgs) {
		ISAType oldType = type;
		type = newType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MiscPackage.ISA_PARAMETER__TYPE, oldType, newType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(ISAType newType) {
		if (newType != type) {
			NotificationChain msgs = null;
			if (type != null)
				msgs = ((InternalEObject)type).eInverseRemove(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
			if (newType != null)
				msgs = ((InternalEObject)newType).eInverseAdd(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
			msgs = basicSetType(newType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiscPackage.ISA_PARAMETER__TYPE, newType, newType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isArrayType() {
		return arrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArrayType(boolean newArrayType) {
		boolean oldArrayType = arrayType;
		arrayType = newArrayType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiscPackage.ISA_PARAMETER__ARRAY_TYPE, oldArrayType, arrayType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isVarArg() {
		return varArg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVarArg(boolean newVarArg) {
		boolean oldVarArg = varArg;
		varArg = newVarArg;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiscPackage.ISA_PARAMETER__VAR_ARG, oldVarArg, varArg));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiscPackage.ISA_PARAMETER__TYPE:
				if (type != null)
					msgs = ((InternalEObject)type).eInverseRemove(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
				return basicSetType((ISAType)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiscPackage.ISA_PARAMETER__TYPE:
				return basicSetType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiscPackage.ISA_PARAMETER__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case MiscPackage.ISA_PARAMETER__ARRAY_TYPE:
				return isArrayType();
			case MiscPackage.ISA_PARAMETER__VAR_ARG:
				return isVarArg();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiscPackage.ISA_PARAMETER__TYPE:
				setType((ISAType)newValue);
				return;
			case MiscPackage.ISA_PARAMETER__ARRAY_TYPE:
				setArrayType((Boolean)newValue);
				return;
			case MiscPackage.ISA_PARAMETER__VAR_ARG:
				setVarArg((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiscPackage.ISA_PARAMETER__TYPE:
				setType((ISAType)null);
				return;
			case MiscPackage.ISA_PARAMETER__ARRAY_TYPE:
				setArrayType(ARRAY_TYPE_EDEFAULT);
				return;
			case MiscPackage.ISA_PARAMETER__VAR_ARG:
				setVarArg(VAR_ARG_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiscPackage.ISA_PARAMETER__TYPE:
				return type != null;
			case MiscPackage.ISA_PARAMETER__ARRAY_TYPE:
				return arrayType != ARRAY_TYPE_EDEFAULT;
			case MiscPackage.ISA_PARAMETER__VAR_ARG:
				return varArg != VAR_ARG_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISATypedElement.class) {
			switch (derivedFeatureID) {
				case MiscPackage.ISA_PARAMETER__TYPE: return MiscPackage.ISA_TYPED_ELEMENT__TYPE;
				case MiscPackage.ISA_PARAMETER__ARRAY_TYPE: return MiscPackage.ISA_TYPED_ELEMENT__ARRAY_TYPE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISATypedElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_TYPED_ELEMENT__TYPE: return MiscPackage.ISA_PARAMETER__TYPE;
				case MiscPackage.ISA_TYPED_ELEMENT__ARRAY_TYPE: return MiscPackage.ISA_PARAMETER__ARRAY_TYPE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (arrayType: ");
		result.append(arrayType);
		result.append(", varArg: ");
		result.append(varArg);
		result.append(')');
		return result.toString();
	}

} //ISAParameterImpl

/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Body Part Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo#getPosition <em>Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo#getRotation <em>Rotation</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getBodyPartInfo()
 * @model abstract="true"
 * @generated
 */
public interface BodyPartInfo extends EObject {
	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' containment reference.
	 * @see #setPosition(ISAVector3)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getBodyPartInfo_Position()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getPosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo#getPosition <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' containment reference.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rotation</em>' containment reference.
	 * @see #setRotation(ISAVector3)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getBodyPartInfo_Rotation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getRotation();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo#getRotation <em>Rotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rotation</em>' containment reference.
	 * @see #getRotation()
	 * @generated
	 */
	void setRotation(ISAVector3 value);

} // BodyPartInfo

/**
 */
package dk.itu.cs.isa.multiplayer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Change Element Active State Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getElementType <em>Element Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getElementId <em>Element Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#isNewState <em>New State</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getPosition <em>Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getRotation <em>Rotation</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getChangeElementActiveStateEvent()
 * @model
 * @generated
 */
public interface ChangeElementActiveStateEvent extends AbstractEvent {
	/**
	 * Returns the value of the '<em><b>Element Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.itu.cs.isa.multiplayer.ElementType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Type</em>' attribute.
	 * @see dk.itu.cs.isa.multiplayer.ElementType
	 * @see #setElementType(ElementType)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getChangeElementActiveStateEvent_ElementType()
	 * @model required="true"
	 * @generated
	 */
	ElementType getElementType();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getElementType <em>Element Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Type</em>' attribute.
	 * @see dk.itu.cs.isa.multiplayer.ElementType
	 * @see #getElementType()
	 * @generated
	 */
	void setElementType(ElementType value);

	/**
	 * Returns the value of the '<em><b>Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Id</em>' attribute.
	 * @see #setElementId(int)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getChangeElementActiveStateEvent_ElementId()
	 * @model required="true"
	 * @generated
	 */
	int getElementId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getElementId <em>Element Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Id</em>' attribute.
	 * @see #getElementId()
	 * @generated
	 */
	void setElementId(int value);

	/**
	 * Returns the value of the '<em><b>New State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New State</em>' attribute.
	 * @see #setNewState(boolean)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getChangeElementActiveStateEvent_NewState()
	 * @model required="true"
	 * @generated
	 */
	boolean isNewState();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#isNewState <em>New State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New State</em>' attribute.
	 * @see #isNewState()
	 * @generated
	 */
	void setNewState(boolean value);

	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' containment reference.
	 * @see #setPosition(ISAVector3)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getChangeElementActiveStateEvent_Position()
	 * @model containment="true"
	 * @generated
	 */
	ISAVector3 getPosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getPosition <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' containment reference.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rotation</em>' containment reference.
	 * @see #setRotation(ISAVector3)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getChangeElementActiveStateEvent_Rotation()
	 * @model containment="true"
	 * @generated
	 */
	ISAVector3 getRotation();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getRotation <em>Rotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rotation</em>' containment reference.
	 * @see #getRotation()
	 * @generated
	 */
	void setRotation(ISAVector3 value);

} // ChangeElementActiveStateEvent

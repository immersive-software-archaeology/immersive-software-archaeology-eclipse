



var scaleRange = document.getElementById("scaleRange");
var scaleLabel = document.getElementById("scaleLabel");
scaleLabel.innerHTML = scaleRange.value + "%";

/*scaleRange.oninput = function() {
  scaleLabel.innerHTML = this.value + "%";
  setup();
}*/


var distanceRange = document.getElementById("distanceRange");
var distanceLabel = document.getElementById("distanceLabel");
distanceLabel.innerHTML = distanceRange.value + "%";

/*distanceRange.oninput = function() {
  distanceLabel.innerHTML = this.value + "%";
  setup();
}*/


updateScales = function(d) {
	scaleLabel.innerHTML = scaleRange.value + "%";
	distanceLabel.innerHTML = distanceRange.value + "%";
	setup();
}










//document.getElementById("container").appendChild(document.createTextNode("test"));

// Some constants controlling the graph appearance
const PADDING_BUBBLE = 0 // distance between edge end and bubble
const PADDING_LABEL = 10 // distance between edge end and engineer name
const BUBBLE_SIZE_MIN = 4
const BUBBLE_SIZE_MAX = 20
const NUMBER_OF_STARS = 1000;
const PLANET_DISTANCE_FROM_CENTER = 3;
var diameter = 500,
    radius = diameter / 2,
    innerRadius = radius - 170; // between center and edge end

// The 'cluster' function takes 1 argument as input. It also has methods (??) like cluster.separation(), cluster.size() and cluster.nodeSize()
var cluster = d3.cluster()
    .size([360, innerRadius]);

var line = d3.radialLine()
    .curve(d3.curveBundle.beta(0.85))
    .radius(function (d) { return d.y; })
    .angle(function (d) { return d.x / 180 * Math.PI; });

var svg;/* = d3.select("div#container")
	.append("svg")
    //.attr("width", diameter)
    //.attr("height", diameter)
	.attr("preserveAspectRatio", "xMidYMid   meet")
	.attr("viewBox", "0 0 " + diameter + " " + diameter)
	.classed("svg-content", true)
    .append("g")
    .attr("transform", "translate(" + radius + "," + radius + ")");*/
	
var stars;/* = svg.append("g").selectAll(".star"),
	link = svg.append("g").selectAll(".link"),
    label = svg.append("g").selectAll(".label"),
    bubble = svg.append("g").selectAll(".bubble");*/

// Add a scale for bubble size
var bubbleSizeScale = d3.scaleLinear()
    .domain([0, 100])
    .range([BUBBLE_SIZE_MIN, BUBBLE_SIZE_MAX]);



mouseover = function (hoveredNode) {
	link.each(function (anyNode) {
		if(anyNode.source === hoveredNode || anyNode.target === hoveredNode) {
			d3.select(this).attr("class", "link-hover");
		}
		else {
			d3.select(this).attr("class", "link-passive");
		}
	})
	bubble.each(function (anyNode) {
		if(anyNode === hoveredNode) {
			d3.select(this).attr("class", "bubble-hover");
		}
		else {
			d3.select(this).attr("class", "bubble");
		}
	})
	label.each(function (anyNode) {
		if(anyNode === hoveredNode) {
			d3.select(this).attr("class", "label-hover");
		}
		else {
			d3.select(this).attr("class", "label");
		}
	})
}

mouseout = function (d) {
	link.each(function (anyNode) {
		d3.select(this).attr("class", "link");
	})
	bubble.each(function (anyNode) {
		d3.select(this).attr("class", "bubble");
	})
	label.each(function (anyNode) {
		d3.select(this).attr("class", "label");
	})
}

click = function (d) {
	console.log(d);
}



var setup = function() {
	diameter = 5 * scaleRange.value;
    radius = diameter / 2;
    innerRadius = radius - 170;
	
	// The 'cluster' function takes 1 argument as input. It also has methods (??) like cluster.separation(), cluster.size() and cluster.nodeSize()
	cluster = d3.cluster()
		.size([360, innerRadius]);

	line = d3.radialLine()
		.curve(d3.curveBundle.beta(0.85))
		.radius(function (d) { return d.y; })
		.angle(function (d) { return d.x / 180 * Math.PI; });

	d3.select("div#container").selectAll("*").remove();
	svg = d3.select("div#container")
		.append("svg")
		//.attr("width", diameter)
		//.attr("height", diameter)
		.attr("preserveAspectRatio", "xMidYMid   meet")
		.attr("viewBox", "0 0 " + diameter + " " + diameter)
		.classed("svg-content", true)
		.append("g")
		.attr("transform", "translate(" + radius + "," + radius + ")");
		
	stars = svg.append("g").selectAll(".star"),
		link = svg.append("g").selectAll(".link"),
		label = svg.append("g").selectAll(".label"),
		bubble = svg.append("g").selectAll(".bubble");

	// Add a scale for bubble size
	bubbleSizeScale = d3.scaleLinear()
		.domain([0, 100])
		.range([BUBBLE_SIZE_MIN, BUBBLE_SIZE_MAX]);
	
	
	// Reformat the data
	root = buildHierarchy(rawData)
		//debugger;
		.sum(function (d) { /*console.log(d);*/ return d.size; });
	// console.log(root)

	// Build an object that gives feature of each leaves
	cluster(root);
	leaves = root.leaves()
	setPositions(root);

	stars = stars
		.data(function () {
			starPositions = [];
			for(var i=0; i<NUMBER_OF_STARS; i++) {
				starPositions.push(i);
			}
			return starPositions;
		})
		.enter().append("circle")
		.attr("class", "star")
		.attr("transform", function (d) { return "translate(" + (Math.random() * diameter*2 - diameter) + "," + (Math.random() * diameter*2 - diameter) + ")" })
		.attr('r', function (d) { return 2 * Math.random() * Math.random(); })
		.attr('opacity', function (d) { return 0.7 * Math.random(); })

	// Leaves is an array of Objects. 1 item = one leaf. Provides x and y for leaf position in the svg. Also gives details about its parent.
	link = link
		.data(calculateLinks(leaves))
		.enter().append("path")
		.each(function (d) { d.source = d[0], d.target = d[d.length - 1]; })
		.attr("class", "link")
		.attr("d", line)

	label = label
		.data(leaves)
		.enter().append("text")
		.attr("class", "label")
		.attr("dy", "0.31em")
		//.attr("transform", function (d) { return "rotate(" + (d.x - 90) + ")translate(" + (d.y + PADDING_LABEL) + ",0)" + (d.x < 180 ? "" : "rotate(180)"); })
		//.attr("transform", function (d) { /*console.log(d.data.name + ": " + d.x + " | " + d.y);*/ return "rotate(" + (d.x - 90) + ")translate(" + (d.y + PADDING_LABEL + bubbleSizeScale(d.value)) + ",0) rotate(" + (-d.x + 90) + ")"; })
		.attr("transform", function (d) { return "rotate(" + (d.x - 90) + ") translate(" + (d.y) + ",0) rotate(" + (-d.x + 90) + ") translate(0,"+ ((d.x < 90 || d.x > 270)?-1:1) * (PADDING_LABEL+(d.value)) +")"; })
		//.attr("text-anchor", function (d) { return d.x < 180 ? "start" : "end"; })
		.attr("text-anchor", function (d) { return "middle"; })
		.text(function (d) { return d.data.key; })
		.on("mouseover", mouseover)
		.on("mouseout", mouseout)
		.on("click", click);

	bubble = bubble
		.data(leaves)
		.enter().append("circle")
		.attr("class", "bubble")
		.attr("transform", function (d) { return "rotate(" + (d.x - 90) + ")translate(" + (d.y + PADDING_BUBBLE) + ",0)" })
		.attr('r', function (d) { return (d.value); })
		.on("mouseover", mouseover)
		.on("mouseout", mouseout)
		.on("click", click);
}
setup();

	

function buildHierarchy(classes) {
    var map = {};

    function find(name, data) {
        var node = map[name], i;
        if (!node) {
            node = map[name] = data || { name: name, children: [] };
            if (name.length) {
                node.parent = find(name.substring(0, i = name.lastIndexOf(".")));
                node.parent.children.push(node);
                node.key = name.substring(i + 1);
            }
        }
        return node;
    }

    classes.forEach(function (d) {
        find(d.name, d);
    });

    return d3.hierarchy(map[""]);
}

// Return a list of links for the given array of nodes.
function calculateLinks(nodes) {
    var map = {},
        links = [];

    // Compute a map from name to node.
    nodes.forEach(function (d) {
        map[d.data.name] = d;
    });

    // For each link, construct a link from the source to target node.
    nodes.forEach(function (d) {
        if (d.data.links) d.data.links.forEach(function (i) {
            links.push(map[d.data.name].path(map[i]));
        });
    });

    return links;
}

function setPositions(node) {
	//console.log(node);
	if(node.data.x_vis)
		node.x = node.data.x_vis;
	else
		node.x = 0;
	
	if(node.data.y_vis)
		node.y = node.data.y_vis;
	else
		node.y = 0;
	
	/*
	var angleDegrees = 180;
	var angleRadians = Math.toRadians(angle);
	var x = Math.cos(angleRadians);
	var y = Math.sin(angleRadians);
	*/
	
	var magnitude = Math.sqrt(node.x*node.x + node.y*node.y);
	if(magnitude != 0) {
		var angle = radians2degrees(Math.atan2(-node.x / magnitude, node.y / magnitude));
		node.x = angle + 180;
		node.y = magnitude * radius / PLANET_DISTANCE_FROM_CENTER * distanceRange.value / 100;
		//console.log(node.data.name + " | angle=" + node.x + " | distance=" + node.y);
	}
	
	if(node.children) {
		node.children.forEach(function (child) {
			setPositions(child);
		});
	}
}

function degrees2radians(degrees)
{
  return degrees * (Math.PI/180);
}
function radians2degrees(radians)
{
  return radians * (180/Math.PI);
}
/**
 */
package dk.itu.cs.isa.multiplayer.util;

import dk.itu.cs.isa.multiplayer.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage
 * @generated
 */
public class MultiplayerSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MultiplayerPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplayerSwitch() {
		if (modelPackage == null) {
			modelPackage = MultiplayerPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MultiplayerPackage.EVENT_LOG: {
				EventLog eventLog = (EventLog)theEObject;
				T result = caseEventLog(eventLog);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.ABSTRACT_EVENT: {
				AbstractEvent abstractEvent = (AbstractEvent)theEObject;
				T result = caseAbstractEvent(abstractEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.SHOW_RELATION_EVENT: {
				ShowRelationEvent showRelationEvent = (ShowRelationEvent)theEObject;
				T result = caseShowRelationEvent(showRelationEvent);
				if (result == null) result = caseAbstractEvent(showRelationEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.CLEAR_ONE_RELATION_EVENT: {
				ClearOneRelationEvent clearOneRelationEvent = (ClearOneRelationEvent)theEObject;
				T result = caseClearOneRelationEvent(clearOneRelationEvent);
				if (result == null) result = caseAbstractEvent(clearOneRelationEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.CLEAR_RELATIONS_EVENT: {
				ClearRelationsEvent clearRelationsEvent = (ClearRelationsEvent)theEObject;
				T result = caseClearRelationsEvent(clearRelationsEvent);
				if (result == null) result = caseAbstractEvent(clearRelationsEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.CLEAR_ALL_RELATIONS_EVENT: {
				ClearAllRelationsEvent clearAllRelationsEvent = (ClearAllRelationsEvent)theEObject;
				T result = caseClearAllRelationsEvent(clearAllRelationsEvent);
				if (result == null) result = caseAbstractEvent(clearAllRelationsEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT: {
				ChangeElementActiveStateEvent changeElementActiveStateEvent = (ChangeElementActiveStateEvent)theEObject;
				T result = caseChangeElementActiveStateEvent(changeElementActiveStateEvent);
				if (result == null) result = caseAbstractEvent(changeElementActiveStateEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.SYSTEM_MOVE_EVENT: {
				SystemMoveEvent systemMoveEvent = (SystemMoveEvent)theEObject;
				T result = caseSystemMoveEvent(systemMoveEvent);
				if (result == null) result = caseAbstractEvent(systemMoveEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.CHANGE_NAMEPLATE_VISIBILITY_EVENT: {
				ChangeNameplateVisibilityEvent changeNameplateVisibilityEvent = (ChangeNameplateVisibilityEvent)theEObject;
				T result = caseChangeNameplateVisibilityEvent(changeNameplateVisibilityEvent);
				if (result == null) result = caseAbstractEvent(changeNameplateVisibilityEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT: {
				SphereOpenCloseEvent sphereOpenCloseEvent = (SphereOpenCloseEvent)theEObject;
				T result = caseSphereOpenCloseEvent(sphereOpenCloseEvent);
				if (result == null) result = caseAbstractEvent(sphereOpenCloseEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.AUDIO_EVENT: {
				AudioEvent audioEvent = (AudioEvent)theEObject;
				T result = caseAudioEvent(audioEvent);
				if (result == null) result = caseAbstractEvent(audioEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MultiplayerPackage.ISA_VECTOR3: {
				ISAVector3 isaVector3 = (ISAVector3)theEObject;
				T result = caseISAVector3(isaVector3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Log</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Log</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventLog(EventLog object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractEvent(AbstractEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show Relation Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show Relation Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShowRelationEvent(ShowRelationEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clear One Relation Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clear One Relation Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClearOneRelationEvent(ClearOneRelationEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clear Relations Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clear Relations Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClearRelationsEvent(ClearRelationsEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clear All Relations Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clear All Relations Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClearAllRelationsEvent(ClearAllRelationsEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Element Active State Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Element Active State Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeElementActiveStateEvent(ChangeElementActiveStateEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Move Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Move Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemMoveEvent(SystemMoveEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Nameplate Visibility Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Nameplate Visibility Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeNameplateVisibilityEvent(ChangeNameplateVisibilityEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sphere Open Close Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sphere Open Close Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSphereOpenCloseEvent(SphereOpenCloseEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Audio Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Audio Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAudioEvent(AudioEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Vector3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Vector3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAVector3(ISAVector3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MultiplayerSwitch

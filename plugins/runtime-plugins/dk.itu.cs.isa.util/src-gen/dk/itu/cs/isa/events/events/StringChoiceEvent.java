/**
 */
package dk.itu.cs.isa.events.events;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Choice Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.StringChoiceEvent#getOptions <em>Options</em>}</li>
 *   <li>{@link dk.itu.cs.isa.events.events.StringChoiceEvent#getSelection <em>Selection</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getStringChoiceEvent()
 * @model
 * @generated
 */
public interface StringChoiceEvent extends InputEvent {
	/**
	 * Returns the value of the '<em><b>Options</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Options</em>' attribute list.
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getStringChoiceEvent_Options()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getOptions();

	/**
	 * Returns the value of the '<em><b>Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection</em>' attribute.
	 * @see #setSelection(String)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getStringChoiceEvent_Selection()
	 * @model
	 * @generated
	 */
	String getSelection();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.StringChoiceEvent#getSelection <em>Selection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection</em>' attribute.
	 * @see #getSelection()
	 * @generated
	 */
	void setSelection(String value);

} // StringChoiceEvent

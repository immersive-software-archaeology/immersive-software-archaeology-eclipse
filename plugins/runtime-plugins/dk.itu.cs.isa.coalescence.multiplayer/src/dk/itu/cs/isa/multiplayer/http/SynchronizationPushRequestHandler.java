package dk.itu.cs.isa.multiplayer.http;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.extensions.ExtensionPointHandler;
import dk.itu.cs.isa.coalescence.server.extensions.ISAAbstractSocketImplementation;
import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.multiplayer.AbstractEvent;
import dk.itu.cs.isa.multiplayer.EventLog;
import dk.itu.cs.isa.multiplayer.MultiplayerPackage;
import dk.itu.cs.isa.multiplayer.udp.SynchronizationUdpServer;
import dk.itu.cs.isa.util.EcoreUtilISA;

public class SynchronizationPushRequestHandler extends ISAAbstractCoalescenceRequestHandler {

	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		if(systemName == null) {
			String examplePath1 = "systemName=JetUML";
			printInvalidParametersHTMLPage(baseRequest, response, null, examplePath1);
			return;
		}
		
		try {
			String requestBodyXML = "";
			while(true) {
				String line = baseRequest.getReader().readLine();
				if(line == null)
					break;
				requestBodyXML += line + System.lineSeparator();
			}
			
			EObject modelRoot = null;
			if(requestBodyXML != null && !requestBodyXML.isBlank())
				modelRoot = EcoreUtilISA.getRootModelObjectFromXMLString(requestBodyXML, MultiplayerPackage.eINSTANCE, "multiplayer").get(0);
			
			if(modelRoot == null || (!(modelRoot instanceof AbstractEvent) && !(modelRoot instanceof EventLog))) {
				printHTMLPage(baseRequest, response, HttpServletResponse.SC_BAD_REQUEST,
						"Your request body did not contain an operation", null,
						"The purpose of this HTTP interface is to send visualization events (e.g., showing relation lines "
						+ "between elements) to the Eclipse server backend.<br>That is done by writing XML encoded operations "
						+ "into the body of this HTTP request.");
				return;
			}
			
			final List<AbstractEvent> submittedOperations = new ArrayList<>();
			if(modelRoot instanceof AbstractEvent) {
				submittedOperations.add((AbstractEvent) modelRoot);
			}
			else if(modelRoot instanceof EventLog) {
				submittedOperations.addAll(((EventLog) modelRoot).getLog());
			}

			for(AbstractEvent submittedOperation : submittedOperations) {
				submittedOperation.setTimestamp(System.currentTimeMillis());
				if(submittedOperation.getClientId() == 0)
					 throw new Exception("The submitted operation did not contain an id for the client who issued it (was set to 0).");
			}
			
			SynchronizationUdpServer server = null;
			for(ISAAbstractSocketImplementation s : ExtensionPointHandler.INSTANCE.getSockets()) {
				if(s instanceof SynchronizationUdpServer) {
					server = (SynchronizationUdpServer) s;
					break;
				}
			}
			if(server == null)
				 throw new NullPointerException("Synchronization socket not found.");
			server.pushOperations(systemName, submittedOperations);
			
//			Display.getDefault().asyncExec(() -> {
				for(AbstractEvent submittedOperation : submittedOperations)
					ISAEventSystem.fireStandardConsoleMessage("Pushed " + submittedOperation.getClass().getSimpleName() + " from client with id=" + submittedOperation.getClientId());
//			});
			
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_OK, "Event synchronized", null, "Everything went fine.");
		}
		catch(Exception e) {
			e.printStackTrace();
			
	    	response.setContentType("text/plain; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_CONFLICT);
			
			PrintWriter out = response.getWriter();
			out.print(e.getMessage());
			out.close();
		}
		baseRequest.setHandled(true);
	}

}

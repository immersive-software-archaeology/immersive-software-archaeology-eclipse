package dk.itu.cs.isa.analyzer.interfaces;

public interface ISystemAnalysisObserver {

	public void notifyAnalysisFinished(String systemName, int executionDurationMillis);

}

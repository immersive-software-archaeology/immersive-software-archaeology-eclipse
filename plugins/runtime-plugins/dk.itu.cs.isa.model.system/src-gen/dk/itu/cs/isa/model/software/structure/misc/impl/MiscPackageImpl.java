/**
 */
package dk.itu.cs.isa.model.software.structure.misc.impl;

import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage;

import dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;

import dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;

import dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl;

import dk.itu.cs.isa.model.software.structure.impl.StructurePackageImpl;

import dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage;

import dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl;

import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl;

import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifier;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;
import dk.itu.cs.isa.model.software.structure.misc.MiscFactory;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MiscPackageImpl extends EPackageImpl implements MiscPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaNamedSoftwareElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaTypedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaModifiableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaReferencingElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaReferenceableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaStatementContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaParametrizableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum isaModifierEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MiscPackageImpl() {
		super(eNS_URI, MiscFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link MiscPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MiscPackage init() {
		if (isInited) return (MiscPackage)EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredMiscPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		MiscPackageImpl theMiscPackage = registeredMiscPackage instanceof MiscPackageImpl ? (MiscPackageImpl)registeredMiscPackage : new MiscPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);
		StructurePackageImpl theStructurePackage = (StructurePackageImpl)(registeredPackage instanceof StructurePackageImpl ? registeredPackage : StructurePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);
		ArchitecturePackageImpl theArchitecturePackage = (ArchitecturePackageImpl)(registeredPackage instanceof ArchitecturePackageImpl ? registeredPackage : ArchitecturePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FilesystemPackage.eNS_URI);
		FilesystemPackageImpl theFilesystemPackage = (FilesystemPackageImpl)(registeredPackage instanceof FilesystemPackageImpl ? registeredPackage : FilesystemPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LibrariesPackage.eNS_URI);
		LibrariesPackageImpl theLibrariesPackage = (LibrariesPackageImpl)(registeredPackage instanceof LibrariesPackageImpl ? registeredPackage : LibrariesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl)(registeredPackage instanceof ClassifiersPackageImpl ? registeredPackage : ClassifiersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);
		MembersPackageImpl theMembersPackage = (MembersPackageImpl)(registeredPackage instanceof MembersPackageImpl ? registeredPackage : MembersPackage.eINSTANCE);

		// Create package meta-data objects
		theMiscPackage.createPackageContents();
		theStructurePackage.createPackageContents();
		theArchitecturePackage.createPackageContents();
		theFilesystemPackage.createPackageContents();
		theLibrariesPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theMembersPackage.createPackageContents();

		// Initialize created meta-data
		theMiscPackage.initializePackageContents();
		theStructurePackage.initializePackageContents();
		theArchitecturePackage.initializePackageContents();
		theFilesystemPackage.initializePackageContents();
		theLibrariesPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theMembersPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMiscPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MiscPackage.eNS_URI, theMiscPackage);
		return theMiscPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISANamedSoftwareElement() {
		return isaNamedSoftwareElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISANamedSoftwareElement_Name() {
		return (EAttribute)isaNamedSoftwareElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISANamedSoftwareElement__GetFullyQualifiedName() {
		return isaNamedSoftwareElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISANamedSoftwareElement__ToString() {
		return isaNamedSoftwareElementEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISATypedElement() {
		return isaTypedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISATypedElement_Type() {
		return (EReference)isaTypedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISATypedElement_ArrayType() {
		return (EAttribute)isaTypedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAType() {
		return isaTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAType_UsagesAsType() {
		return (EReference)isaTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAModifiableElement() {
		return isaModifiableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAModifiableElement_Modifiers() {
		return (EAttribute)isaModifiableElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAReferencingElement() {
		return isaReferencingElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAReferencingElement_TypeReferences() {
		return (EReference)isaReferencingElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAReferencingElement_ConstructorCalls() {
		return (EReference)isaReferencingElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAReferencingElement_MethodCalls() {
		return (EReference)isaReferencingElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAReferencingElement_FieldAccesses() {
		return (EReference)isaReferencingElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAReferencingElement_ConstantAccesses() {
		return (EReference)isaReferencingElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAReferencingElement_ComponentAccesses() {
		return (EReference)isaReferencingElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAReference() {
		return isaReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAReference_Weight() {
		return (EAttribute)isaReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAReference_Target() {
		return (EReference)isaReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISAReference__ToString() {
		return isaReferenceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAReferenceableElement() {
		return isaReferenceableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAReferenceableElement_IncomingReferences() {
		return (EReference)isaReferenceableElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAStatementContainer() {
		return isaStatementContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAStatementContainer_NumberOfStatements() {
		return (EAttribute)isaStatementContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAStatementContainer_NumberOfExpressions() {
		return (EAttribute)isaStatementContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAStatementContainer_NumberOfControlFlowSplits() {
		return (EAttribute)isaStatementContainerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAStatementContainer_CognitiveComplexity() {
		return (EAttribute)isaStatementContainerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAParametrizableElement() {
		return isaParametrizableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAParametrizableElement_Parameters() {
		return (EReference)isaParametrizableElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAParameter() {
		return isaParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAParameter_VarArg() {
		return (EAttribute)isaParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getISAModifier() {
		return isaModifierEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MiscFactory getMiscFactory() {
		return (MiscFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaNamedSoftwareElementEClass = createEClass(ISA_NAMED_SOFTWARE_ELEMENT);
		createEAttribute(isaNamedSoftwareElementEClass, ISA_NAMED_SOFTWARE_ELEMENT__NAME);
		createEOperation(isaNamedSoftwareElementEClass, ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME);
		createEOperation(isaNamedSoftwareElementEClass, ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING);

		isaTypedElementEClass = createEClass(ISA_TYPED_ELEMENT);
		createEReference(isaTypedElementEClass, ISA_TYPED_ELEMENT__TYPE);
		createEAttribute(isaTypedElementEClass, ISA_TYPED_ELEMENT__ARRAY_TYPE);

		isaTypeEClass = createEClass(ISA_TYPE);
		createEReference(isaTypeEClass, ISA_TYPE__USAGES_AS_TYPE);

		isaModifiableElementEClass = createEClass(ISA_MODIFIABLE_ELEMENT);
		createEAttribute(isaModifiableElementEClass, ISA_MODIFIABLE_ELEMENT__MODIFIERS);

		isaReferencingElementEClass = createEClass(ISA_REFERENCING_ELEMENT);
		createEReference(isaReferencingElementEClass, ISA_REFERENCING_ELEMENT__TYPE_REFERENCES);
		createEReference(isaReferencingElementEClass, ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS);
		createEReference(isaReferencingElementEClass, ISA_REFERENCING_ELEMENT__METHOD_CALLS);
		createEReference(isaReferencingElementEClass, ISA_REFERENCING_ELEMENT__FIELD_ACCESSES);
		createEReference(isaReferencingElementEClass, ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES);
		createEReference(isaReferencingElementEClass, ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES);

		isaReferenceEClass = createEClass(ISA_REFERENCE);
		createEAttribute(isaReferenceEClass, ISA_REFERENCE__WEIGHT);
		createEReference(isaReferenceEClass, ISA_REFERENCE__TARGET);
		createEOperation(isaReferenceEClass, ISA_REFERENCE___TO_STRING);

		isaReferenceableElementEClass = createEClass(ISA_REFERENCEABLE_ELEMENT);
		createEReference(isaReferenceableElementEClass, ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES);

		isaStatementContainerEClass = createEClass(ISA_STATEMENT_CONTAINER);
		createEAttribute(isaStatementContainerEClass, ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS);
		createEAttribute(isaStatementContainerEClass, ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS);
		createEAttribute(isaStatementContainerEClass, ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS);
		createEAttribute(isaStatementContainerEClass, ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY);

		isaParametrizableElementEClass = createEClass(ISA_PARAMETRIZABLE_ELEMENT);
		createEReference(isaParametrizableElementEClass, ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS);

		isaParameterEClass = createEClass(ISA_PARAMETER);
		createEAttribute(isaParameterEClass, ISA_PARAMETER__VAR_ARG);

		// Create enums
		isaModifierEEnum = createEEnum(ISA_MODIFIER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MembersPackage theMembersPackage = (MembersPackage)EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);

		// Create type parameters
		ETypeParameter isaReferenceEClass_T = addETypeParameter(isaReferenceEClass, "T");

		// Set bounds for type parameters
		EGenericType g1 = createEGenericType(this.getISAReferenceableElement());
		isaReferenceEClass_T.getEBounds().add(g1);

		// Add supertypes to classes
		isaTypeEClass.getESuperTypes().add(this.getISAReferenceableElement());
		isaReferenceableElementEClass.getESuperTypes().add(this.getISANamedSoftwareElement());
		isaStatementContainerEClass.getESuperTypes().add(this.getISAReferencingElement());
		isaParameterEClass.getESuperTypes().add(this.getISANamedSoftwareElement());
		isaParameterEClass.getESuperTypes().add(this.getISATypedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(isaNamedSoftwareElementEClass, ISANamedSoftwareElement.class, "ISANamedSoftwareElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISANamedSoftwareElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, ISANamedSoftwareElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getISANamedSoftwareElement__GetFullyQualifiedName(), ecorePackage.getEString(), "getFullyQualifiedName", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getISANamedSoftwareElement__ToString(), ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(isaTypedElementEClass, ISATypedElement.class, "ISATypedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISATypedElement_Type(), this.getISAType(), this.getISAType_UsagesAsType(), "type", null, 0, 1, ISATypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISATypedElement_ArrayType(), ecorePackage.getEBoolean(), "arrayType", null, 1, 1, ISATypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaTypeEClass, ISAType.class, "ISAType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAType_UsagesAsType(), this.getISATypedElement(), this.getISATypedElement_Type(), "usagesAsType", null, 0, -1, ISAType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaModifiableElementEClass, ISAModifiableElement.class, "ISAModifiableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAModifiableElement_Modifiers(), this.getISAModifier(), "modifiers", null, 0, -1, ISAModifiableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaReferencingElementEClass, ISAReferencingElement.class, "ISAReferencingElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(this.getISAReference());
		EGenericType g2 = createEGenericType(this.getISAType());
		g1.getETypeArguments().add(g2);
		initEReference(getISAReferencingElement_TypeReferences(), g1, null, "typeReferences", null, 0, -1, ISAReferencingElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getISAReference());
		g2 = createEGenericType(theMembersPackage.getISAConstructor());
		g1.getETypeArguments().add(g2);
		initEReference(getISAReferencingElement_ConstructorCalls(), g1, null, "constructorCalls", null, 0, -1, ISAReferencingElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getISAReference());
		g2 = createEGenericType(theMembersPackage.getISAMethod());
		g1.getETypeArguments().add(g2);
		initEReference(getISAReferencingElement_MethodCalls(), g1, null, "methodCalls", null, 0, -1, ISAReferencingElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getISAReference());
		g2 = createEGenericType(theMembersPackage.getISAField());
		g1.getETypeArguments().add(g2);
		initEReference(getISAReferencingElement_FieldAccesses(), g1, null, "fieldAccesses", null, 0, -1, ISAReferencingElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getISAReference());
		g2 = createEGenericType(theMembersPackage.getISAEnumConstant());
		g1.getETypeArguments().add(g2);
		initEReference(getISAReferencingElement_ConstantAccesses(), g1, null, "constantAccesses", null, 0, -1, ISAReferencingElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getISAReference());
		g2 = createEGenericType(theMembersPackage.getISARecordComponent());
		g1.getETypeArguments().add(g2);
		initEReference(getISAReferencingElement_ComponentAccesses(), g1, null, "componentAccesses", null, 0, -1, ISAReferencingElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaReferenceEClass, ISAReference.class, "ISAReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAReference_Weight(), ecorePackage.getEInt(), "weight", null, 1, 1, ISAReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(isaReferenceEClass_T);
		initEReference(getISAReference_Target(), g1, this.getISAReferenceableElement_IncomingReferences(), "target", null, 0, 1, ISAReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getISAReference__ToString(), ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(isaReferenceableElementEClass, ISAReferenceableElement.class, "ISAReferenceableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(this.getISAReference());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEReference(getISAReferenceableElement_IncomingReferences(), g1, this.getISAReference_Target(), "incomingReferences", null, 0, -1, ISAReferenceableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaStatementContainerEClass, ISAStatementContainer.class, "ISAStatementContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAStatementContainer_NumberOfStatements(), ecorePackage.getEInt(), "numberOfStatements", null, 1, 1, ISAStatementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAStatementContainer_NumberOfExpressions(), ecorePackage.getEInt(), "numberOfExpressions", null, 1, 1, ISAStatementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAStatementContainer_NumberOfControlFlowSplits(), ecorePackage.getEInt(), "numberOfControlFlowSplits", null, 1, 1, ISAStatementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAStatementContainer_CognitiveComplexity(), ecorePackage.getEInt(), "cognitiveComplexity", null, 1, 1, ISAStatementContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaParametrizableElementEClass, ISAParametrizableElement.class, "ISAParametrizableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAParametrizableElement_Parameters(), this.getISAParameter(), null, "parameters", null, 0, -1, ISAParametrizableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaParameterEClass, ISAParameter.class, "ISAParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAParameter_VarArg(), ecorePackage.getEBoolean(), "varArg", null, 1, 1, ISAParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(isaModifierEEnum, ISAModifier.class, "ISAModifier");
		addEEnumLiteral(isaModifierEEnum, ISAModifier.NONE);
		addEEnumLiteral(isaModifierEEnum, ISAModifier.PRIVATE);
		addEEnumLiteral(isaModifierEEnum, ISAModifier.PROTECTED);
		addEEnumLiteral(isaModifierEEnum, ISAModifier.PUBLIC);
		addEEnumLiteral(isaModifierEEnum, ISAModifier.FINAL);
		addEEnumLiteral(isaModifierEEnum, ISAModifier.STATIC);
	}

} //MiscPackageImpl

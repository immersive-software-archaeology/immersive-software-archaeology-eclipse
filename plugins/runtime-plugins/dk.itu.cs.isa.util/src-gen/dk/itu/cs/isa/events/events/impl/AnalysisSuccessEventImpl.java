/**
 */
package dk.itu.cs.isa.events.events.impl;

import dk.itu.cs.isa.events.events.AnalysisSuccessEvent;
import dk.itu.cs.isa.events.events.EventsPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Analysis Success Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.impl.AnalysisSuccessEventImpl#getCalculationTime <em>Calculation Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnalysisSuccessEventImpl extends OutputEventImpl implements AnalysisSuccessEvent {
	/**
	 * The default value of the '{@link #getCalculationTime() <em>Calculation Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculationTime()
	 * @generated
	 * @ordered
	 */
	protected static final int CALCULATION_TIME_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCalculationTime() <em>Calculation Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculationTime()
	 * @generated
	 * @ordered
	 */
	protected int calculationTime = CALCULATION_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnalysisSuccessEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.ANALYSIS_SUCCESS_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getCalculationTime() {
		return calculationTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCalculationTime(int newCalculationTime) {
		int oldCalculationTime = calculationTime;
		calculationTime = newCalculationTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.ANALYSIS_SUCCESS_EVENT__CALCULATION_TIME, oldCalculationTime, calculationTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.ANALYSIS_SUCCESS_EVENT__CALCULATION_TIME:
				return getCalculationTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.ANALYSIS_SUCCESS_EVENT__CALCULATION_TIME:
				setCalculationTime((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.ANALYSIS_SUCCESS_EVENT__CALCULATION_TIME:
				setCalculationTime(CALCULATION_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.ANALYSIS_SUCCESS_EVENT__CALCULATION_TIME:
				return calculationTime != CALCULATION_TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (calculationTime: ");
		result.append(calculationTime);
		result.append(')');
		return result.toString();
	}

} //AnalysisSuccessEventImpl

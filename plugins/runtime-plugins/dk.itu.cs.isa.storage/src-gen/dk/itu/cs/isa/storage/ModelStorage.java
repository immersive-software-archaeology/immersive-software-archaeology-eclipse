/**
 */
package dk.itu.cs.isa.storage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Storage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.storage.ModelStorage#getEntries <em>Entries</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.ModelStorage#getBaseFolder <em>Base Folder</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.storage.StoragePackage#getModelStorage()
 * @model
 * @generated
 */
public interface ModelStorage extends EObject {
	/**
	 * Returns the value of the '<em><b>Entries</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.storage.ModelStorageEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entries</em>' containment reference list.
	 * @see dk.itu.cs.isa.storage.StoragePackage#getModelStorage_Entries()
	 * @model containment="true"
	 * @generated
	 */
	EList<ModelStorageEntry> getEntries();

	/**
	 * Returns the value of the '<em><b>Base Folder</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Folder</em>' attribute.
	 * @see #setBaseFolder(String)
	 * @see dk.itu.cs.isa.storage.StoragePackage#getModelStorage_BaseFolder()
	 * @model required="true"
	 * @generated
	 */
	String getBaseFolder();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.storage.ModelStorage#getBaseFolder <em>Base Folder</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Folder</em>' attribute.
	 * @see #getBaseFolder()
	 * @generated
	 */
	void setBaseFolder(String value);

} // ModelStorage

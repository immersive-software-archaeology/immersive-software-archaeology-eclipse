/**
 */
package dk.itu.cs.isa.model.software.structure.libraries.impl;

import dk.itu.cs.isa.model.software.structure.libraries.ISAPrimitiveType;
import dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.ISATypeImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Primitive Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ISAPrimitiveTypeImpl extends ISATypeImpl implements ISAPrimitiveType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAPrimitiveTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibrariesPackage.Literals.ISA_PRIMITIVE_TYPE;
	}

} //ISAPrimitiveTypeImpl

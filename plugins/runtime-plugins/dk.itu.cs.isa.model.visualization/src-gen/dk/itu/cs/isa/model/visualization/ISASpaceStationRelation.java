/**
 */
package dk.itu.cs.isa.model.visualization;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Space Station Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationRelation#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationRelation()
 * @model
 * @generated
 */
public interface ISASpaceStationRelation extends ISAAbstractVisualRelation {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ISASpaceStation)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationRelation_Target()
	 * @model required="true"
	 * @generated
	 */
	ISASpaceStation getTarget();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationRelation#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ISASpaceStation value);

} // ISASpaceStationRelation

/**
 */
package dk.itu.cs.isa.model.system.quality;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ISA Quality Issue Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAQualityIssueType()
 * @model
 * @generated
 */
public enum ISAQualityIssueType implements Enumerator {
	/**
	 * The '<em><b>CODE SMELL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CODE_SMELL_VALUE
	 * @generated
	 * @ordered
	 */
	CODE_SMELL(0, "CODE_SMELL", "CODE_SMELL"),

	/**
	 * The '<em><b>BUG</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BUG_VALUE
	 * @generated
	 * @ordered
	 */
	BUG(1, "BUG", "BUG"),

	/**
	 * The '<em><b>VULNERABILITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VULNERABILITY_VALUE
	 * @generated
	 * @ordered
	 */
	VULNERABILITY(2, "VULNERABILITY", "VULNERABILITY");

	/**
	 * The '<em><b>CODE SMELL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CODE_SMELL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CODE_SMELL_VALUE = 0;

	/**
	 * The '<em><b>BUG</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BUG
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BUG_VALUE = 1;

	/**
	 * The '<em><b>VULNERABILITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VULNERABILITY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VULNERABILITY_VALUE = 2;

	/**
	 * An array of all the '<em><b>ISA Quality Issue Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ISAQualityIssueType[] VALUES_ARRAY =
		new ISAQualityIssueType[] {
			CODE_SMELL,
			BUG,
			VULNERABILITY,
		};

	/**
	 * A public read-only list of all the '<em><b>ISA Quality Issue Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ISAQualityIssueType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ISA Quality Issue Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ISAQualityIssueType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ISAQualityIssueType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ISA Quality Issue Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ISAQualityIssueType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ISAQualityIssueType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ISA Quality Issue Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ISAQualityIssueType get(int value) {
		switch (value) {
			case CODE_SMELL_VALUE: return CODE_SMELL;
			case BUG_VALUE: return BUG;
			case VULNERABILITY_VALUE: return VULNERABILITY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ISAQualityIssueType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ISAQualityIssueType

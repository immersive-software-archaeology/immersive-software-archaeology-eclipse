/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Concrete Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAConcreteClass()
 * @model
 * @generated
 */
public interface ISAConcreteClass extends ISAClass {
} // ISAConcreteClass

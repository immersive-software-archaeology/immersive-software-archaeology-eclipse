/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>History</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory#getSystemName <em>System Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory#getWhiteboards <em>Whiteboards</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory#getEventLog <em>Event Log</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboardHistory()
 * @model
 * @generated
 */
public interface WhiteboardHistory extends EObject {
	/**
	 * Returns the value of the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Name</em>' attribute.
	 * @see #setSystemName(String)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboardHistory_SystemName()
	 * @model required="true"
	 * @generated
	 */
	String getSystemName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory#getSystemName <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Name</em>' attribute.
	 * @see #getSystemName()
	 * @generated
	 */
	void setSystemName(String value);

	/**
	 * Returns the value of the '<em><b>Whiteboards</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.whiteboard.Whiteboard}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getContainingHistory <em>Containing History</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Whiteboards</em>' containment reference list.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboardHistory_Whiteboards()
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getContainingHistory
	 * @model opposite="containingHistory" containment="true" required="true"
	 * @generated
	 */
	EList<Whiteboard> getWhiteboards();

	/**
	 * Returns the value of the '<em><b>Event Log</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Log</em>' containment reference list.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboardHistory_EventLog()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractWhiteboardEvent> getEventLog();

} // WhiteboardHistory

package dk.itu.cs.isa.multiplayer.http;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import dk.itu.cs.isa.coalescence.server.extensions.ExtensionPointHandler;
import dk.itu.cs.isa.coalescence.server.extensions.ISAAbstractSocketImplementation;
import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;

public class SocketListRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
    	response.setContentType("application/json; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
		PrintWriter out = response.getWriter();
		out.print(getSocketInfoAsJSON());
		out.close();
		
		baseRequest.setHandled(true);
	}

	private String getSocketInfoAsJSON() {
		JSONObject responseJSON = new JSONObject();
		JSONArray entries = new JSONArray();
		
		for(ISAAbstractSocketImplementation socket : ExtensionPointHandler.INSTANCE.getSockets()) {
			JSONObject entryJSON = new JSONObject();
			entryJSON.put("name", socket.getName());
			entryJSON.put("port", socket.getPort());
			entries.put(entryJSON);
		}
		
		responseJSON.put("entries", entries);
    	return responseJSON.toString(2);
    }

}

/**
 */
package dk.itu.cs.isa.events.events;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.events.events.EventsPackage
 * @generated
 */
public interface EventsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EventsFactory eINSTANCE = dk.itu.cs.isa.events.events.impl.EventsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Console Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Console Event</em>'.
	 * @generated
	 */
	ConsoleEvent createConsoleEvent();

	/**
	 * Returns a new object of class '<em>Output Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Output Event</em>'.
	 * @generated
	 */
	OutputEvent createOutputEvent();

	/**
	 * Returns a new object of class '<em>Analysis Success Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Analysis Success Event</em>'.
	 * @generated
	 */
	AnalysisSuccessEvent createAnalysisSuccessEvent();

	/**
	 * Returns a new object of class '<em>Analysis Setup Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Analysis Setup Event</em>'.
	 * @generated
	 */
	AnalysisSetupEvent createAnalysisSetupEvent();

	/**
	 * Returns a new object of class '<em>Boolean Input Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Input Event</em>'.
	 * @generated
	 */
	BooleanInputEvent createBooleanInputEvent();

	/**
	 * Returns a new object of class '<em>Int Input Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Input Event</em>'.
	 * @generated
	 */
	IntInputEvent createIntInputEvent();

	/**
	 * Returns a new object of class '<em>String Input Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Input Event</em>'.
	 * @generated
	 */
	StringInputEvent createStringInputEvent();

	/**
	 * Returns a new object of class '<em>String Choice Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Choice Event</em>'.
	 * @generated
	 */
	StringChoiceEvent createStringChoiceEvent();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EventsPackage getEventsPackage();

} //EventsFactory

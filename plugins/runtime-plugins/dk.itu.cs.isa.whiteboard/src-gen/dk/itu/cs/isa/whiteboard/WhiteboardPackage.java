/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.whiteboard.WhiteboardFactory
 * @model kind="package"
 * @generated
 */
public interface WhiteboardPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "whiteboard";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/whiteboard";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "whiteboard";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WhiteboardPackage eINSTANCE = dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardHistoryImpl <em>History</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardHistoryImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getWhiteboardHistory()
	 * @generated
	 */
	int WHITEBOARD_HISTORY = 0;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD_HISTORY__SYSTEM_NAME = 0;

	/**
	 * The feature id for the '<em><b>Whiteboards</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD_HISTORY__WHITEBOARDS = 1;

	/**
	 * The feature id for the '<em><b>Event Log</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD_HISTORY__EVENT_LOG = 2;

	/**
	 * The number of structural features of the '<em>History</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD_HISTORY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>History</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD_HISTORY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl <em>Whiteboard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getWhiteboard()
	 * @generated
	 */
	int WHITEBOARD = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD__ID = 0;

	/**
	 * The feature id for the '<em><b>Containing History</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD__CONTAINING_HISTORY = 1;

	/**
	 * The feature id for the '<em><b>Bitmap Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD__BITMAP_BASE64 = 2;

	/**
	 * The feature id for the '<em><b>Pins</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD__PINS = 3;

	/**
	 * The feature id for the '<em><b>Photos</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD__PHOTOS = 4;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD__POSITION = 5;

	/**
	 * The feature id for the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD__ROTATION = 6;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD__SIZE = 7;

	/**
	 * The feature id for the '<em><b>Undo Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD__UNDO_DEPTH = 8;

	/**
	 * The number of structural features of the '<em>Whiteboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Whiteboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHITEBOARD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PinImpl <em>Pin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PinImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPin()
	 * @generated
	 */
	int PIN = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__ID = 0;

	/**
	 * The feature id for the '<em><b>Element Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__ELEMENT_QUALIFIED_NAME = 1;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__TEXTURE_SPACE_POSITION = 2;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__LOCAL_SPACE_POSITION = 3;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__COLOR = 4;

	/**
	 * The feature id for the '<em><b>Whiteboard</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__WHITEBOARD = 5;

	/**
	 * The number of structural features of the '<em>Pin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Pin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PhotoImpl <em>Photo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PhotoImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPhoto()
	 * @generated
	 */
	int PHOTO = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO__ID = 0;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO__TEXTURE_SPACE_POSITION = 1;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO__LOCAL_SPACE_POSITION = 2;

	/**
	 * The feature id for the '<em><b>Local Space Forward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO__LOCAL_SPACE_FORWARD_DIRECTION = 3;

	/**
	 * The feature id for the '<em><b>Local Space Upward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO__LOCAL_SPACE_UPWARD_DIRECTION = 4;

	/**
	 * The feature id for the '<em><b>Bitmap Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO__BITMAP_BASE64 = 5;

	/**
	 * The number of structural features of the '<em>Photo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Photo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.ISAVector3Impl <em>ISA Vector3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.ISAVector3Impl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getISAVector3()
	 * @generated
	 */
	int ISA_VECTOR3 = 4;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__Z = 2;

	/**
	 * The number of structural features of the '<em>ISA Vector3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>ISA Vector3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.ISAVector2Impl <em>ISA Vector2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.ISAVector2Impl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getISAVector2()
	 * @generated
	 */
	int ISA_VECTOR2 = 5;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR2__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR2__Y = 1;

	/**
	 * The number of structural features of the '<em>ISA Vector2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR2_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>ISA Vector2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR2_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.ISAColorImpl <em>ISA Color</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.ISAColorImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getISAColor()
	 * @generated
	 */
	int ISA_COLOR = 6;

	/**
	 * The feature id for the '<em><b>R</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_COLOR__R = 0;

	/**
	 * The feature id for the '<em><b>G</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_COLOR__G = 1;

	/**
	 * The feature id for the '<em><b>B</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_COLOR__B = 2;

	/**
	 * The feature id for the '<em><b>A</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_COLOR__A = 3;

	/**
	 * The number of structural features of the '<em>ISA Color</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_COLOR_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>ISA Color</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_COLOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractWhiteboardEventImpl <em>Abstract Whiteboard Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.AbstractWhiteboardEventImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractWhiteboardEvent()
	 * @generated
	 */
	int ABSTRACT_WHITEBOARD_EVENT = 7;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID = 0;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID = 1;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP = 2;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED = 3;

	/**
	 * The number of structural features of the '<em>Abstract Whiteboard Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Abstract Whiteboard Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_WHITEBOARD_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.BoardSpawnEventImpl <em>Board Spawn Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.BoardSpawnEventImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getBoardSpawnEvent()
	 * @generated
	 */
	int BOARD_SPAWN_EVENT = 8;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_SPAWN_EVENT__SUBMITTING_CLIENT_ID = ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_SPAWN_EVENT__WHITEBOARD_ID = ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_SPAWN_EVENT__TIMESTAMP = ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_SPAWN_EVENT__CONFIRMATION_REQUIRED = ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>World Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>World Space Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Board Spawn Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_SPAWN_EVENT_FEATURE_COUNT = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Board Spawn Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_SPAWN_EVENT_OPERATION_COUNT = ABSTRACT_WHITEBOARD_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.BoardDuplicateEventImpl <em>Board Duplicate Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.BoardDuplicateEventImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getBoardDuplicateEvent()
	 * @generated
	 */
	int BOARD_DUPLICATE_EVENT = 9;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DUPLICATE_EVENT__SUBMITTING_CLIENT_ID = ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DUPLICATE_EVENT__WHITEBOARD_ID = ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DUPLICATE_EVENT__TIMESTAMP = ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DUPLICATE_EVENT__CONFIRMATION_REQUIRED = ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>World Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DUPLICATE_EVENT__WORLD_SPACE_POSITION = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>World Space Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DUPLICATE_EVENT__WORLD_SPACE_ROTATION = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Board Duplicate Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DUPLICATE_EVENT_FEATURE_COUNT = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Board Duplicate Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DUPLICATE_EVENT_OPERATION_COUNT = ABSTRACT_WHITEBOARD_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.BoardDestroyEventImpl <em>Board Destroy Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.BoardDestroyEventImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getBoardDestroyEvent()
	 * @generated
	 */
	int BOARD_DESTROY_EVENT = 10;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DESTROY_EVENT__SUBMITTING_CLIENT_ID = ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DESTROY_EVENT__WHITEBOARD_ID = ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DESTROY_EVENT__TIMESTAMP = ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DESTROY_EVENT__CONFIRMATION_REQUIRED = ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED;

	/**
	 * The number of structural features of the '<em>Board Destroy Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DESTROY_EVENT_FEATURE_COUNT = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Board Destroy Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_DESTROY_EVENT_OPERATION_COUNT = ABSTRACT_WHITEBOARD_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.UndoEventImpl <em>Undo Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.UndoEventImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getUndoEvent()
	 * @generated
	 */
	int UNDO_EVENT = 11;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDO_EVENT__SUBMITTING_CLIENT_ID = ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDO_EVENT__WHITEBOARD_ID = ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDO_EVENT__TIMESTAMP = ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDO_EVENT__CONFIRMATION_REQUIRED = ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED;

	/**
	 * The number of structural features of the '<em>Undo Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDO_EVENT_FEATURE_COUNT = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Undo Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDO_EVENT_OPERATION_COUNT = ABSTRACT_WHITEBOARD_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.RedoEventImpl <em>Redo Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.RedoEventImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getRedoEvent()
	 * @generated
	 */
	int REDO_EVENT = 12;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDO_EVENT__SUBMITTING_CLIENT_ID = ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDO_EVENT__WHITEBOARD_ID = ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDO_EVENT__TIMESTAMP = ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDO_EVENT__CONFIRMATION_REQUIRED = ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED;

	/**
	 * The number of structural features of the '<em>Redo Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDO_EVENT_FEATURE_COUNT = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Redo Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDO_EVENT_OPERATION_COUNT = ABSTRACT_WHITEBOARD_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractOperationImpl <em>Abstract Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.AbstractOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractOperation()
	 * @generated
	 */
	int ABSTRACT_OPERATION = 13;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OPERATION__WHITEBOARD_ID = ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OPERATION__TIMESTAMP = ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED;

	/**
	 * The number of structural features of the '<em>Abstract Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OPERATION_FEATURE_COUNT = ABSTRACT_WHITEBOARD_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Abstract Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OPERATION_OPERATION_COUNT = ABSTRACT_WHITEBOARD_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractCompoundOperationImpl <em>Abstract Compound Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.AbstractCompoundOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractCompoundOperation()
	 * @generated
	 */
	int ABSTRACT_COMPOUND_OPERATION = 14;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPOUND_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPOUND_OPERATION__WHITEBOARD_ID = ABSTRACT_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPOUND_OPERATION__TIMESTAMP = ABSTRACT_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPOUND_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Sub Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS = ABSTRACT_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Abstract Compound Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPOUND_OPERATION_FEATURE_COUNT = ABSTRACT_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Abstract Compound Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMPOUND_OPERATION_OPERATION_COUNT = ABSTRACT_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PinSplitOperationImpl <em>Pin Split Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PinSplitOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinSplitOperation()
	 * @generated
	 */
	int PIN_SPLIT_OPERATION = 15;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SPLIT_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_COMPOUND_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SPLIT_OPERATION__WHITEBOARD_ID = ABSTRACT_COMPOUND_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SPLIT_OPERATION__TIMESTAMP = ABSTRACT_COMPOUND_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SPLIT_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_COMPOUND_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Sub Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SPLIT_OPERATION__SUB_OPERATIONS = ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS;

	/**
	 * The feature id for the '<em><b>Pinned Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SPLIT_OPERATION__PINNED_ELEMENT_NAME = ABSTRACT_COMPOUND_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SPLIT_OPERATION__PIN_ID = ABSTRACT_COMPOUND_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Pin Split Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SPLIT_OPERATION_FEATURE_COUNT = ABSTRACT_COMPOUND_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Pin Split Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SPLIT_OPERATION_OPERATION_COUNT = ABSTRACT_COMPOUND_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PinsRemoveAllOperationImpl <em>Pins Remove All Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PinsRemoveAllOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinsRemoveAllOperation()
	 * @generated
	 */
	int PINS_REMOVE_ALL_OPERATION = 16;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINS_REMOVE_ALL_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_COMPOUND_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINS_REMOVE_ALL_OPERATION__WHITEBOARD_ID = ABSTRACT_COMPOUND_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINS_REMOVE_ALL_OPERATION__TIMESTAMP = ABSTRACT_COMPOUND_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINS_REMOVE_ALL_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_COMPOUND_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Sub Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINS_REMOVE_ALL_OPERATION__SUB_OPERATIONS = ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS;

	/**
	 * The number of structural features of the '<em>Pins Remove All Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINS_REMOVE_ALL_OPERATION_FEATURE_COUNT = ABSTRACT_COMPOUND_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pins Remove All Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PINS_REMOVE_ALL_OPERATION_OPERATION_COUNT = ABSTRACT_COMPOUND_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractPinOperationImpl <em>Abstract Pin Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.AbstractPinOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractPinOperation()
	 * @generated
	 */
	int ABSTRACT_PIN_OPERATION = 17;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION__WHITEBOARD_ID = ABSTRACT_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION__TIMESTAMP = ABSTRACT_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION__PIN_ID = ABSTRACT_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pinned Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME = ABSTRACT_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION = ABSTRACT_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION = ABSTRACT_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION__COLOR = ABSTRACT_OPERATION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Abstract Pin Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION_FEATURE_COUNT = ABSTRACT_OPERATION_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Abstract Pin Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PIN_OPERATION_OPERATION_COUNT = ABSTRACT_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PinSoftwareAddOperationImpl <em>Pin Software Add Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PinSoftwareAddOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinSoftwareAddOperation()
	 * @generated
	 */
	int PIN_SOFTWARE_ADD_OPERATION = 18;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_PIN_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION__WHITEBOARD_ID = ABSTRACT_PIN_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION__TIMESTAMP = ABSTRACT_PIN_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_PIN_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION__PIN_ID = ABSTRACT_PIN_OPERATION__PIN_ID;

	/**
	 * The feature id for the '<em><b>Pinned Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION__PINNED_ELEMENT_NAME = ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION__TEXTURE_SPACE_POSITION = ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION__LOCAL_SPACE_POSITION = ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION__COLOR = ABSTRACT_PIN_OPERATION__COLOR;

	/**
	 * The number of structural features of the '<em>Pin Software Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION_FEATURE_COUNT = ABSTRACT_PIN_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pin Software Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_SOFTWARE_ADD_OPERATION_OPERATION_COUNT = ABSTRACT_PIN_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PinAudioAddOperationImpl <em>Pin Audio Add Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PinAudioAddOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinAudioAddOperation()
	 * @generated
	 */
	int PIN_AUDIO_ADD_OPERATION = 19;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_PIN_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__WHITEBOARD_ID = ABSTRACT_PIN_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__TIMESTAMP = ABSTRACT_PIN_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_PIN_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__PIN_ID = ABSTRACT_PIN_OPERATION__PIN_ID;

	/**
	 * The feature id for the '<em><b>Pinned Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__PINNED_ELEMENT_NAME = ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__TEXTURE_SPACE_POSITION = ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__LOCAL_SPACE_POSITION = ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__COLOR = ABSTRACT_PIN_OPERATION__COLOR;

	/**
	 * The feature id for the '<em><b>Sound Wave Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_BASE64 = ABSTRACT_PIN_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sound Wave Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_FREQUENCY = ABSTRACT_PIN_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Pin Audio Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION_FEATURE_COUNT = ABSTRACT_PIN_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Pin Audio Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_AUDIO_ADD_OPERATION_OPERATION_COUNT = ABSTRACT_PIN_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PinRemoveOperationImpl <em>Pin Remove Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PinRemoveOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinRemoveOperation()
	 * @generated
	 */
	int PIN_REMOVE_OPERATION = 20;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_PIN_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION__WHITEBOARD_ID = ABSTRACT_PIN_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION__TIMESTAMP = ABSTRACT_PIN_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_PIN_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION__PIN_ID = ABSTRACT_PIN_OPERATION__PIN_ID;

	/**
	 * The feature id for the '<em><b>Pinned Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION__PINNED_ELEMENT_NAME = ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION__TEXTURE_SPACE_POSITION = ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION__LOCAL_SPACE_POSITION = ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION__COLOR = ABSTRACT_PIN_OPERATION__COLOR;

	/**
	 * The number of structural features of the '<em>Pin Remove Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION_FEATURE_COUNT = ABSTRACT_PIN_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pin Remove Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_REMOVE_OPERATION_OPERATION_COUNT = ABSTRACT_PIN_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PinMoveOperationImpl <em>Pin Move Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PinMoveOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinMoveOperation()
	 * @generated
	 */
	int PIN_MOVE_OPERATION = 21;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_PIN_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__WHITEBOARD_ID = ABSTRACT_PIN_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__TIMESTAMP = ABSTRACT_PIN_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_PIN_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__PIN_ID = ABSTRACT_PIN_OPERATION__PIN_ID;

	/**
	 * The feature id for the '<em><b>Pinned Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__PINNED_ELEMENT_NAME = ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__TEXTURE_SPACE_POSITION = ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__LOCAL_SPACE_POSITION = ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__COLOR = ABSTRACT_PIN_OPERATION__COLOR;

	/**
	 * The feature id for the '<em><b>Pre Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION = ABSTRACT_PIN_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Pin Move Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION_FEATURE_COUNT = ABSTRACT_PIN_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Pin Move Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_MOVE_OPERATION_OPERATION_COUNT = ABSTRACT_PIN_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractDrawOperationImpl <em>Abstract Draw Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.AbstractDrawOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractDrawOperation()
	 * @generated
	 */
	int ABSTRACT_DRAW_OPERATION = 22;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DRAW_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DRAW_OPERATION__WHITEBOARD_ID = ABSTRACT_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DRAW_OPERATION__TIMESTAMP = ABSTRACT_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DRAW_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DRAW_OPERATION__COLOR = ABSTRACT_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pen Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DRAW_OPERATION__PEN_SIZE = ABSTRACT_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Drawn Lines</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DRAW_OPERATION__DRAWN_LINES = ABSTRACT_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Abstract Draw Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DRAW_OPERATION_FEATURE_COUNT = ABSTRACT_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Abstract Draw Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DRAW_OPERATION_OPERATION_COUNT = ABSTRACT_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.DrawFreehandOperationImpl <em>Draw Freehand Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.DrawFreehandOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getDrawFreehandOperation()
	 * @generated
	 */
	int DRAW_FREEHAND_OPERATION = 23;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_FREEHAND_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_DRAW_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_FREEHAND_OPERATION__WHITEBOARD_ID = ABSTRACT_DRAW_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_FREEHAND_OPERATION__TIMESTAMP = ABSTRACT_DRAW_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_FREEHAND_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_DRAW_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_FREEHAND_OPERATION__COLOR = ABSTRACT_DRAW_OPERATION__COLOR;

	/**
	 * The feature id for the '<em><b>Pen Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_FREEHAND_OPERATION__PEN_SIZE = ABSTRACT_DRAW_OPERATION__PEN_SIZE;

	/**
	 * The feature id for the '<em><b>Drawn Lines</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_FREEHAND_OPERATION__DRAWN_LINES = ABSTRACT_DRAW_OPERATION__DRAWN_LINES;

	/**
	 * The number of structural features of the '<em>Draw Freehand Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_FREEHAND_OPERATION_FEATURE_COUNT = ABSTRACT_DRAW_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Draw Freehand Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_FREEHAND_OPERATION_OPERATION_COUNT = ABSTRACT_DRAW_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.DrawModuleOperationImpl <em>Draw Module Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.DrawModuleOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getDrawModuleOperation()
	 * @generated
	 */
	int DRAW_MODULE_OPERATION = 24;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_MODULE_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_DRAW_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_MODULE_OPERATION__WHITEBOARD_ID = ABSTRACT_DRAW_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_MODULE_OPERATION__TIMESTAMP = ABSTRACT_DRAW_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_MODULE_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_DRAW_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_MODULE_OPERATION__COLOR = ABSTRACT_DRAW_OPERATION__COLOR;

	/**
	 * The feature id for the '<em><b>Pen Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_MODULE_OPERATION__PEN_SIZE = ABSTRACT_DRAW_OPERATION__PEN_SIZE;

	/**
	 * The feature id for the '<em><b>Drawn Lines</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_MODULE_OPERATION__DRAWN_LINES = ABSTRACT_DRAW_OPERATION__DRAWN_LINES;

	/**
	 * The number of structural features of the '<em>Draw Module Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_MODULE_OPERATION_FEATURE_COUNT = ABSTRACT_DRAW_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Draw Module Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_MODULE_OPERATION_OPERATION_COUNT = ABSTRACT_DRAW_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.DrawArrowOperationImpl <em>Draw Arrow Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.DrawArrowOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getDrawArrowOperation()
	 * @generated
	 */
	int DRAW_ARROW_OPERATION = 25;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_DRAW_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION__WHITEBOARD_ID = ABSTRACT_DRAW_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION__TIMESTAMP = ABSTRACT_DRAW_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_DRAW_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION__COLOR = ABSTRACT_DRAW_OPERATION__COLOR;

	/**
	 * The feature id for the '<em><b>Pen Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION__PEN_SIZE = ABSTRACT_DRAW_OPERATION__PEN_SIZE;

	/**
	 * The feature id for the '<em><b>Drawn Lines</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION__DRAWN_LINES = ABSTRACT_DRAW_OPERATION__DRAWN_LINES;

	/**
	 * The feature id for the '<em><b>Auto Draw Arrow Head</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION__AUTO_DRAW_ARROW_HEAD = ABSTRACT_DRAW_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Draw Arrow Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION_FEATURE_COUNT = ABSTRACT_DRAW_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Draw Arrow Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRAW_ARROW_OPERATION_OPERATION_COUNT = ABSTRACT_DRAW_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PaintClearAllOperationImpl <em>Paint Clear All Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PaintClearAllOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPaintClearAllOperation()
	 * @generated
	 */
	int PAINT_CLEAR_ALL_OPERATION = 26;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_CLEAR_ALL_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_DRAW_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_CLEAR_ALL_OPERATION__WHITEBOARD_ID = ABSTRACT_DRAW_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_CLEAR_ALL_OPERATION__TIMESTAMP = ABSTRACT_DRAW_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_CLEAR_ALL_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_DRAW_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_CLEAR_ALL_OPERATION__COLOR = ABSTRACT_DRAW_OPERATION__COLOR;

	/**
	 * The feature id for the '<em><b>Pen Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_CLEAR_ALL_OPERATION__PEN_SIZE = ABSTRACT_DRAW_OPERATION__PEN_SIZE;

	/**
	 * The feature id for the '<em><b>Drawn Lines</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_CLEAR_ALL_OPERATION__DRAWN_LINES = ABSTRACT_DRAW_OPERATION__DRAWN_LINES;

	/**
	 * The number of structural features of the '<em>Paint Clear All Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_CLEAR_ALL_OPERATION_FEATURE_COUNT = ABSTRACT_DRAW_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Paint Clear All Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_CLEAR_ALL_OPERATION_OPERATION_COUNT = ABSTRACT_DRAW_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PaintEraseOperationImpl <em>Paint Erase Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PaintEraseOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPaintEraseOperation()
	 * @generated
	 */
	int PAINT_ERASE_OPERATION = 27;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_ERASE_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_DRAW_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_ERASE_OPERATION__WHITEBOARD_ID = ABSTRACT_DRAW_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_ERASE_OPERATION__TIMESTAMP = ABSTRACT_DRAW_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_ERASE_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_DRAW_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_ERASE_OPERATION__COLOR = ABSTRACT_DRAW_OPERATION__COLOR;

	/**
	 * The feature id for the '<em><b>Pen Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_ERASE_OPERATION__PEN_SIZE = ABSTRACT_DRAW_OPERATION__PEN_SIZE;

	/**
	 * The feature id for the '<em><b>Drawn Lines</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_ERASE_OPERATION__DRAWN_LINES = ABSTRACT_DRAW_OPERATION__DRAWN_LINES;

	/**
	 * The number of structural features of the '<em>Paint Erase Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_ERASE_OPERATION_FEATURE_COUNT = ABSTRACT_DRAW_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Paint Erase Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAINT_ERASE_OPERATION_OPERATION_COUNT = ABSTRACT_DRAW_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.BoardEnlargeOperationImpl <em>Board Enlarge Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.BoardEnlargeOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getBoardEnlargeOperation()
	 * @generated
	 */
	int BOARD_ENLARGE_OPERATION = 28;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_ENLARGE_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_DRAW_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_ENLARGE_OPERATION__WHITEBOARD_ID = ABSTRACT_DRAW_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_ENLARGE_OPERATION__TIMESTAMP = ABSTRACT_DRAW_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_ENLARGE_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_DRAW_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_ENLARGE_OPERATION__COLOR = ABSTRACT_DRAW_OPERATION__COLOR;

	/**
	 * The feature id for the '<em><b>Pen Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_ENLARGE_OPERATION__PEN_SIZE = ABSTRACT_DRAW_OPERATION__PEN_SIZE;

	/**
	 * The feature id for the '<em><b>Drawn Lines</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_ENLARGE_OPERATION__DRAWN_LINES = ABSTRACT_DRAW_OPERATION__DRAWN_LINES;

	/**
	 * The number of structural features of the '<em>Board Enlarge Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_ENLARGE_OPERATION_FEATURE_COUNT = ABSTRACT_DRAW_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Board Enlarge Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOARD_ENLARGE_OPERATION_OPERATION_COUNT = ABSTRACT_DRAW_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl <em>Abstract Photo Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractPhotoOperation()
	 * @generated
	 */
	int ABSTRACT_PHOTO_OPERATION = 29;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__WHITEBOARD_ID = ABSTRACT_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__TIMESTAMP = ABSTRACT_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Photo Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__PHOTO_ID = ABSTRACT_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bitmap Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64 = ABSTRACT_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION = ABSTRACT_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Local Space Forward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION = ABSTRACT_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Local Space Upward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION = ABSTRACT_OPERATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION = ABSTRACT_OPERATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Camera Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_OPERATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Camera Rotation When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_OPERATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>System Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_OPERATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>System Size When Photo Was Taken</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_OPERATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Relations Visible When Photo Was Taken</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_OPERATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Open Capsules Element Qualified Names When Photo Was Taken</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_OPERATION_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Abstract Photo Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION_FEATURE_COUNT = ABSTRACT_OPERATION_FEATURE_COUNT + 12;

	/**
	 * The number of operations of the '<em>Abstract Photo Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PHOTO_OPERATION_OPERATION_COUNT = ABSTRACT_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PhotoAddOperationImpl <em>Photo Add Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PhotoAddOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPhotoAddOperation()
	 * @generated
	 */
	int PHOTO_ADD_OPERATION = 30;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_PHOTO_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__WHITEBOARD_ID = ABSTRACT_PHOTO_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__TIMESTAMP = ABSTRACT_PHOTO_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_PHOTO_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Photo Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__PHOTO_ID = ABSTRACT_PHOTO_OPERATION__PHOTO_ID;

	/**
	 * The feature id for the '<em><b>Bitmap Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__BITMAP_BASE64 = ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__LOCAL_SPACE_POSITION = ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Local Space Forward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION = ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION;

	/**
	 * The feature id for the '<em><b>Local Space Upward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION = ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__TEXTURE_SPACE_POSITION = ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Camera Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Camera Rotation When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>System Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>System Size When Photo Was Taken</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Relations Visible When Photo Was Taken</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Open Capsules Element Qualified Names When Photo Was Taken</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The number of structural features of the '<em>Photo Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION_FEATURE_COUNT = ABSTRACT_PHOTO_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Photo Add Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_ADD_OPERATION_OPERATION_COUNT = ABSTRACT_PHOTO_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PhotoMoveOperationImpl <em>Photo Move Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PhotoMoveOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPhotoMoveOperation()
	 * @generated
	 */
	int PHOTO_MOVE_OPERATION = 31;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_PHOTO_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__WHITEBOARD_ID = ABSTRACT_PHOTO_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__TIMESTAMP = ABSTRACT_PHOTO_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_PHOTO_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Photo Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__PHOTO_ID = ABSTRACT_PHOTO_OPERATION__PHOTO_ID;

	/**
	 * The feature id for the '<em><b>Bitmap Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__BITMAP_BASE64 = ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__LOCAL_SPACE_POSITION = ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Local Space Forward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION = ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION;

	/**
	 * The feature id for the '<em><b>Local Space Upward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION = ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__TEXTURE_SPACE_POSITION = ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Camera Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Camera Rotation When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>System Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>System Size When Photo Was Taken</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Relations Visible When Photo Was Taken</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Open Capsules Element Qualified Names When Photo Was Taken</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Pre Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION = ABSTRACT_PHOTO_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pre Local Space Forward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION = ABSTRACT_PHOTO_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Pre Local Space Upward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION = ABSTRACT_PHOTO_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Pre Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION = ABSTRACT_PHOTO_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Photo Move Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION_FEATURE_COUNT = ABSTRACT_PHOTO_OPERATION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Photo Move Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_MOVE_OPERATION_OPERATION_COUNT = ABSTRACT_PHOTO_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.PhotoRemoveOperationImpl <em>Photo Remove Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.PhotoRemoveOperationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPhotoRemoveOperation()
	 * @generated
	 */
	int PHOTO_REMOVE_OPERATION = 32;

	/**
	 * The feature id for the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__SUBMITTING_CLIENT_ID = ABSTRACT_PHOTO_OPERATION__SUBMITTING_CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__WHITEBOARD_ID = ABSTRACT_PHOTO_OPERATION__WHITEBOARD_ID;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__TIMESTAMP = ABSTRACT_PHOTO_OPERATION__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__CONFIRMATION_REQUIRED = ABSTRACT_PHOTO_OPERATION__CONFIRMATION_REQUIRED;

	/**
	 * The feature id for the '<em><b>Photo Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__PHOTO_ID = ABSTRACT_PHOTO_OPERATION__PHOTO_ID;

	/**
	 * The feature id for the '<em><b>Bitmap Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__BITMAP_BASE64 = ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64;

	/**
	 * The feature id for the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__LOCAL_SPACE_POSITION = ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Local Space Forward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION = ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION;

	/**
	 * The feature id for the '<em><b>Local Space Upward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION = ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION;

	/**
	 * The feature id for the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__TEXTURE_SPACE_POSITION = ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION;

	/**
	 * The feature id for the '<em><b>Camera Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Camera Rotation When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>System Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>System Size When Photo Was Taken</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Relations Visible When Photo Was Taken</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Open Capsules Element Qualified Names When Photo Was Taken</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN = ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN;

	/**
	 * The feature id for the '<em><b>Play Dissolve Animation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION__PLAY_DISSOLVE_ANIMATION = ABSTRACT_PHOTO_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Photo Remove Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION_FEATURE_COUNT = ABSTRACT_PHOTO_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Photo Remove Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHOTO_REMOVE_OPERATION_OPERATION_COUNT = ABSTRACT_PHOTO_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.whiteboard.impl.VisibleRelationImpl <em>Visible Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.whiteboard.impl.VisibleRelationImpl
	 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getVisibleRelation()
	 * @generated
	 */
	int VISIBLE_RELATION = 33;

	/**
	 * The feature id for the '<em><b>Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISIBLE_RELATION__ELEMENT_NAME = 0;

	/**
	 * The feature id for the '<em><b>Forward</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISIBLE_RELATION__FORWARD = 1;

	/**
	 * The feature id for the '<em><b>Relation Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISIBLE_RELATION__RELATION_TYPE = 2;

	/**
	 * The number of structural features of the '<em>Visible Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISIBLE_RELATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Visible Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISIBLE_RELATION_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory <em>History</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>History</em>'.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardHistory
	 * @generated
	 */
	EClass getWhiteboardHistory();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory#getSystemName <em>System Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Name</em>'.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardHistory#getSystemName()
	 * @see #getWhiteboardHistory()
	 * @generated
	 */
	EAttribute getWhiteboardHistory_SystemName();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory#getWhiteboards <em>Whiteboards</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Whiteboards</em>'.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardHistory#getWhiteboards()
	 * @see #getWhiteboardHistory()
	 * @generated
	 */
	EReference getWhiteboardHistory_Whiteboards();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory#getEventLog <em>Event Log</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Log</em>'.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardHistory#getEventLog()
	 * @see #getWhiteboardHistory()
	 * @generated
	 */
	EReference getWhiteboardHistory_EventLog();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.Whiteboard <em>Whiteboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Whiteboard</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard
	 * @generated
	 */
	EClass getWhiteboard();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getId()
	 * @see #getWhiteboard()
	 * @generated
	 */
	EAttribute getWhiteboard_Id();

	/**
	 * Returns the meta object for the container reference '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getContainingHistory <em>Containing History</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Containing History</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getContainingHistory()
	 * @see #getWhiteboard()
	 * @generated
	 */
	EReference getWhiteboard_ContainingHistory();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getBitmapBase64 <em>Bitmap Base64</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bitmap Base64</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getBitmapBase64()
	 * @see #getWhiteboard()
	 * @generated
	 */
	EAttribute getWhiteboard_BitmapBase64();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getPins <em>Pins</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pins</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getPins()
	 * @see #getWhiteboard()
	 * @generated
	 */
	EReference getWhiteboard_Pins();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getPhotos <em>Photos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Photos</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getPhotos()
	 * @see #getWhiteboard()
	 * @generated
	 */
	EReference getWhiteboard_Photos();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getPosition()
	 * @see #getWhiteboard()
	 * @generated
	 */
	EReference getWhiteboard_Position();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getRotation <em>Rotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rotation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getRotation()
	 * @see #getWhiteboard()
	 * @generated
	 */
	EReference getWhiteboard_Rotation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getSize()
	 * @see #getWhiteboard()
	 * @generated
	 */
	EAttribute getWhiteboard_Size();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getUndoDepth <em>Undo Depth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Undo Depth</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getUndoDepth()
	 * @see #getWhiteboard()
	 * @generated
	 */
	EAttribute getWhiteboard_UndoDepth();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.Pin <em>Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pin</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Pin
	 * @generated
	 */
	EClass getPin();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.Pin#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Pin#getId()
	 * @see #getPin()
	 * @generated
	 */
	EAttribute getPin_Id();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.Pin#getElementQualifiedName <em>Element Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Qualified Name</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Pin#getElementQualifiedName()
	 * @see #getPin()
	 * @generated
	 */
	EAttribute getPin_ElementQualifiedName();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.Pin#getTextureSpacePosition <em>Texture Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Texture Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Pin#getTextureSpacePosition()
	 * @see #getPin()
	 * @generated
	 */
	EReference getPin_TextureSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.Pin#getLocalSpacePosition <em>Local Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Pin#getLocalSpacePosition()
	 * @see #getPin()
	 * @generated
	 */
	EReference getPin_LocalSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.Pin#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Color</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Pin#getColor()
	 * @see #getPin()
	 * @generated
	 */
	EReference getPin_Color();

	/**
	 * Returns the meta object for the container reference '{@link dk.itu.cs.isa.whiteboard.Pin#getWhiteboard <em>Whiteboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Whiteboard</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Pin#getWhiteboard()
	 * @see #getPin()
	 * @generated
	 */
	EReference getPin_Whiteboard();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.Photo <em>Photo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Photo</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Photo
	 * @generated
	 */
	EClass getPhoto();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.Photo#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Photo#getId()
	 * @see #getPhoto()
	 * @generated
	 */
	EAttribute getPhoto_Id();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.Photo#getTextureSpacePosition <em>Texture Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Texture Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Photo#getTextureSpacePosition()
	 * @see #getPhoto()
	 * @generated
	 */
	EReference getPhoto_TextureSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.Photo#getLocalSpacePosition <em>Local Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Photo#getLocalSpacePosition()
	 * @see #getPhoto()
	 * @generated
	 */
	EReference getPhoto_LocalSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.Photo#getLocalSpaceForwardDirection <em>Local Space Forward Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Space Forward Direction</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Photo#getLocalSpaceForwardDirection()
	 * @see #getPhoto()
	 * @generated
	 */
	EReference getPhoto_LocalSpaceForwardDirection();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.Photo#getLocalSpaceUpwardDirection <em>Local Space Upward Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Space Upward Direction</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Photo#getLocalSpaceUpwardDirection()
	 * @see #getPhoto()
	 * @generated
	 */
	EReference getPhoto_LocalSpaceUpwardDirection();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.Photo#getBitmapBase64 <em>Bitmap Base64</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bitmap Base64</em>'.
	 * @see dk.itu.cs.isa.whiteboard.Photo#getBitmapBase64()
	 * @see #getPhoto()
	 * @generated
	 */
	EAttribute getPhoto_BitmapBase64();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.ISAVector3 <em>ISA Vector3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Vector3</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAVector3
	 * @generated
	 */
	EClass getISAVector3();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.ISAVector3#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAVector3#getX()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_X();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.ISAVector3#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAVector3#getY()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_Y();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.ISAVector3#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAVector3#getZ()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_Z();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.ISAVector2 <em>ISA Vector2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Vector2</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAVector2
	 * @generated
	 */
	EClass getISAVector2();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.ISAVector2#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAVector2#getX()
	 * @see #getISAVector2()
	 * @generated
	 */
	EAttribute getISAVector2_X();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.ISAVector2#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAVector2#getY()
	 * @see #getISAVector2()
	 * @generated
	 */
	EAttribute getISAVector2_Y();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.ISAColor <em>ISA Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Color</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAColor
	 * @generated
	 */
	EClass getISAColor();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.ISAColor#getR <em>R</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>R</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAColor#getR()
	 * @see #getISAColor()
	 * @generated
	 */
	EAttribute getISAColor_R();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.ISAColor#getG <em>G</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>G</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAColor#getG()
	 * @see #getISAColor()
	 * @generated
	 */
	EAttribute getISAColor_G();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.ISAColor#getB <em>B</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>B</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAColor#getB()
	 * @see #getISAColor()
	 * @generated
	 */
	EAttribute getISAColor_B();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.ISAColor#getA <em>A</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>A</em>'.
	 * @see dk.itu.cs.isa.whiteboard.ISAColor#getA()
	 * @see #getISAColor()
	 * @generated
	 */
	EAttribute getISAColor_A();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent <em>Abstract Whiteboard Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Whiteboard Event</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent
	 * @generated
	 */
	EClass getAbstractWhiteboardEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getSubmittingClientId <em>Submitting Client Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Submitting Client Id</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getSubmittingClientId()
	 * @see #getAbstractWhiteboardEvent()
	 * @generated
	 */
	EAttribute getAbstractWhiteboardEvent_SubmittingClientId();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getWhiteboardId <em>Whiteboard Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Whiteboard Id</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getWhiteboardId()
	 * @see #getAbstractWhiteboardEvent()
	 * @generated
	 */
	EAttribute getAbstractWhiteboardEvent_WhiteboardId();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getTimestamp()
	 * @see #getAbstractWhiteboardEvent()
	 * @generated
	 */
	EAttribute getAbstractWhiteboardEvent_Timestamp();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#isConfirmationRequired <em>Confirmation Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Confirmation Required</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#isConfirmationRequired()
	 * @see #getAbstractWhiteboardEvent()
	 * @generated
	 */
	EAttribute getAbstractWhiteboardEvent_ConfirmationRequired();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.BoardSpawnEvent <em>Board Spawn Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Board Spawn Event</em>'.
	 * @see dk.itu.cs.isa.whiteboard.BoardSpawnEvent
	 * @generated
	 */
	EClass getBoardSpawnEvent();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.BoardSpawnEvent#getWorldSpacePosition <em>World Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>World Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.BoardSpawnEvent#getWorldSpacePosition()
	 * @see #getBoardSpawnEvent()
	 * @generated
	 */
	EReference getBoardSpawnEvent_WorldSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.BoardSpawnEvent#getWorldSpaceRotation <em>World Space Rotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>World Space Rotation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.BoardSpawnEvent#getWorldSpaceRotation()
	 * @see #getBoardSpawnEvent()
	 * @generated
	 */
	EReference getBoardSpawnEvent_WorldSpaceRotation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.BoardDuplicateEvent <em>Board Duplicate Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Board Duplicate Event</em>'.
	 * @see dk.itu.cs.isa.whiteboard.BoardDuplicateEvent
	 * @generated
	 */
	EClass getBoardDuplicateEvent();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.BoardDuplicateEvent#getWorldSpacePosition <em>World Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>World Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.BoardDuplicateEvent#getWorldSpacePosition()
	 * @see #getBoardDuplicateEvent()
	 * @generated
	 */
	EReference getBoardDuplicateEvent_WorldSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.BoardDuplicateEvent#getWorldSpaceRotation <em>World Space Rotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>World Space Rotation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.BoardDuplicateEvent#getWorldSpaceRotation()
	 * @see #getBoardDuplicateEvent()
	 * @generated
	 */
	EReference getBoardDuplicateEvent_WorldSpaceRotation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.BoardDestroyEvent <em>Board Destroy Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Board Destroy Event</em>'.
	 * @see dk.itu.cs.isa.whiteboard.BoardDestroyEvent
	 * @generated
	 */
	EClass getBoardDestroyEvent();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.UndoEvent <em>Undo Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Undo Event</em>'.
	 * @see dk.itu.cs.isa.whiteboard.UndoEvent
	 * @generated
	 */
	EClass getUndoEvent();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.RedoEvent <em>Redo Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Redo Event</em>'.
	 * @see dk.itu.cs.isa.whiteboard.RedoEvent
	 * @generated
	 */
	EClass getRedoEvent();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.AbstractOperation <em>Abstract Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractOperation
	 * @generated
	 */
	EClass getAbstractOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.AbstractCompoundOperation <em>Abstract Compound Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Compound Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractCompoundOperation
	 * @generated
	 */
	EClass getAbstractCompoundOperation();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.whiteboard.AbstractCompoundOperation#getSubOperations <em>Sub Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Operations</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractCompoundOperation#getSubOperations()
	 * @see #getAbstractCompoundOperation()
	 * @generated
	 */
	EReference getAbstractCompoundOperation_SubOperations();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PinSplitOperation <em>Pin Split Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pin Split Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinSplitOperation
	 * @generated
	 */
	EClass getPinSplitOperation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.PinSplitOperation#getPinnedElementName <em>Pinned Element Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pinned Element Name</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinSplitOperation#getPinnedElementName()
	 * @see #getPinSplitOperation()
	 * @generated
	 */
	EAttribute getPinSplitOperation_PinnedElementName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.PinSplitOperation#getPinId <em>Pin Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pin Id</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinSplitOperation#getPinId()
	 * @see #getPinSplitOperation()
	 * @generated
	 */
	EAttribute getPinSplitOperation_PinId();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PinsRemoveAllOperation <em>Pins Remove All Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pins Remove All Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinsRemoveAllOperation
	 * @generated
	 */
	EClass getPinsRemoveAllOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation <em>Abstract Pin Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Pin Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPinOperation
	 * @generated
	 */
	EClass getAbstractPinOperation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getPinId <em>Pin Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pin Id</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPinOperation#getPinId()
	 * @see #getAbstractPinOperation()
	 * @generated
	 */
	EAttribute getAbstractPinOperation_PinId();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getPinnedElementName <em>Pinned Element Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pinned Element Name</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPinOperation#getPinnedElementName()
	 * @see #getAbstractPinOperation()
	 * @generated
	 */
	EAttribute getAbstractPinOperation_PinnedElementName();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getTextureSpacePosition <em>Texture Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Texture Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPinOperation#getTextureSpacePosition()
	 * @see #getAbstractPinOperation()
	 * @generated
	 */
	EReference getAbstractPinOperation_TextureSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getLocalSpacePosition <em>Local Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPinOperation#getLocalSpacePosition()
	 * @see #getAbstractPinOperation()
	 * @generated
	 */
	EReference getAbstractPinOperation_LocalSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Color</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPinOperation#getColor()
	 * @see #getAbstractPinOperation()
	 * @generated
	 */
	EReference getAbstractPinOperation_Color();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PinSoftwareAddOperation <em>Pin Software Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pin Software Add Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinSoftwareAddOperation
	 * @generated
	 */
	EClass getPinSoftwareAddOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PinAudioAddOperation <em>Pin Audio Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pin Audio Add Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinAudioAddOperation
	 * @generated
	 */
	EClass getPinAudioAddOperation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.PinAudioAddOperation#getSoundWaveBase64 <em>Sound Wave Base64</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sound Wave Base64</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinAudioAddOperation#getSoundWaveBase64()
	 * @see #getPinAudioAddOperation()
	 * @generated
	 */
	EAttribute getPinAudioAddOperation_SoundWaveBase64();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.PinAudioAddOperation#getSoundWaveFrequency <em>Sound Wave Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sound Wave Frequency</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinAudioAddOperation#getSoundWaveFrequency()
	 * @see #getPinAudioAddOperation()
	 * @generated
	 */
	EAttribute getPinAudioAddOperation_SoundWaveFrequency();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PinRemoveOperation <em>Pin Remove Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pin Remove Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinRemoveOperation
	 * @generated
	 */
	EClass getPinRemoveOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PinMoveOperation <em>Pin Move Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pin Move Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinMoveOperation
	 * @generated
	 */
	EClass getPinMoveOperation();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.PinMoveOperation#getPreTextureSpacePosition <em>Pre Texture Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pre Texture Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PinMoveOperation#getPreTextureSpacePosition()
	 * @see #getPinMoveOperation()
	 * @generated
	 */
	EReference getPinMoveOperation_PreTextureSpacePosition();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation <em>Abstract Draw Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Draw Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractDrawOperation
	 * @generated
	 */
	EClass getAbstractDrawOperation();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Color</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getColor()
	 * @see #getAbstractDrawOperation()
	 * @generated
	 */
	EReference getAbstractDrawOperation_Color();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getPenSize <em>Pen Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pen Size</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getPenSize()
	 * @see #getAbstractDrawOperation()
	 * @generated
	 */
	EAttribute getAbstractDrawOperation_PenSize();

	/**
	 * Returns the meta object for the attribute list '{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getDrawnLines <em>Drawn Lines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Drawn Lines</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getDrawnLines()
	 * @see #getAbstractDrawOperation()
	 * @generated
	 */
	EAttribute getAbstractDrawOperation_DrawnLines();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.DrawFreehandOperation <em>Draw Freehand Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Draw Freehand Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.DrawFreehandOperation
	 * @generated
	 */
	EClass getDrawFreehandOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.DrawModuleOperation <em>Draw Module Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Draw Module Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.DrawModuleOperation
	 * @generated
	 */
	EClass getDrawModuleOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.DrawArrowOperation <em>Draw Arrow Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Draw Arrow Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.DrawArrowOperation
	 * @generated
	 */
	EClass getDrawArrowOperation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.DrawArrowOperation#isAutoDrawArrowHead <em>Auto Draw Arrow Head</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Auto Draw Arrow Head</em>'.
	 * @see dk.itu.cs.isa.whiteboard.DrawArrowOperation#isAutoDrawArrowHead()
	 * @see #getDrawArrowOperation()
	 * @generated
	 */
	EAttribute getDrawArrowOperation_AutoDrawArrowHead();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PaintClearAllOperation <em>Paint Clear All Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paint Clear All Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PaintClearAllOperation
	 * @generated
	 */
	EClass getPaintClearAllOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PaintEraseOperation <em>Paint Erase Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paint Erase Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PaintEraseOperation
	 * @generated
	 */
	EClass getPaintEraseOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.BoardEnlargeOperation <em>Board Enlarge Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Board Enlarge Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.BoardEnlargeOperation
	 * @generated
	 */
	EClass getBoardEnlargeOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation <em>Abstract Photo Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Photo Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation
	 * @generated
	 */
	EClass getAbstractPhotoOperation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getPhotoId <em>Photo Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Photo Id</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getPhotoId()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EAttribute getAbstractPhotoOperation_PhotoId();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getBitmapBase64 <em>Bitmap Base64</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bitmap Base64</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getBitmapBase64()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EAttribute getAbstractPhotoOperation_BitmapBase64();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpacePosition <em>Local Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpacePosition()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EReference getAbstractPhotoOperation_LocalSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpaceForwardDirection <em>Local Space Forward Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Space Forward Direction</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpaceForwardDirection()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EReference getAbstractPhotoOperation_LocalSpaceForwardDirection();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpaceUpwardDirection <em>Local Space Upward Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Space Upward Direction</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpaceUpwardDirection()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EReference getAbstractPhotoOperation_LocalSpaceUpwardDirection();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getTextureSpacePosition <em>Texture Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Texture Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getTextureSpacePosition()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EReference getAbstractPhotoOperation_TextureSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getCameraPositionWhenPhotoWasTaken <em>Camera Position When Photo Was Taken</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Camera Position When Photo Was Taken</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getCameraPositionWhenPhotoWasTaken()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EReference getAbstractPhotoOperation_CameraPositionWhenPhotoWasTaken();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getCameraRotationWhenPhotoWasTaken <em>Camera Rotation When Photo Was Taken</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Camera Rotation When Photo Was Taken</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getCameraRotationWhenPhotoWasTaken()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EReference getAbstractPhotoOperation_CameraRotationWhenPhotoWasTaken();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getRelationsVisibleWhenPhotoWasTaken <em>Relations Visible When Photo Was Taken</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relations Visible When Photo Was Taken</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getRelationsVisibleWhenPhotoWasTaken()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EReference getAbstractPhotoOperation_RelationsVisibleWhenPhotoWasTaken();

	/**
	 * Returns the meta object for the attribute list '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken <em>Open Capsules Element Qualified Names When Photo Was Taken</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Open Capsules Element Qualified Names When Photo Was Taken</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EAttribute getAbstractPhotoOperation_OpenCapsulesElementQualifiedNamesWhenPhotoWasTaken();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getSystemSizeWhenPhotoWasTaken <em>System Size When Photo Was Taken</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Size When Photo Was Taken</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getSystemSizeWhenPhotoWasTaken()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EAttribute getAbstractPhotoOperation_SystemSizeWhenPhotoWasTaken();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getSystemPositionWhenPhotoWasTaken <em>System Position When Photo Was Taken</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>System Position When Photo Was Taken</em>'.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getSystemPositionWhenPhotoWasTaken()
	 * @see #getAbstractPhotoOperation()
	 * @generated
	 */
	EReference getAbstractPhotoOperation_SystemPositionWhenPhotoWasTaken();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PhotoAddOperation <em>Photo Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Photo Add Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PhotoAddOperation
	 * @generated
	 */
	EClass getPhotoAddOperation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation <em>Photo Move Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Photo Move Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PhotoMoveOperation
	 * @generated
	 */
	EClass getPhotoMoveOperation();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpacePosition <em>Pre Local Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pre Local Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpacePosition()
	 * @see #getPhotoMoveOperation()
	 * @generated
	 */
	EReference getPhotoMoveOperation_PreLocalSpacePosition();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpaceForwardDirection <em>Pre Local Space Forward Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pre Local Space Forward Direction</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpaceForwardDirection()
	 * @see #getPhotoMoveOperation()
	 * @generated
	 */
	EReference getPhotoMoveOperation_PreLocalSpaceForwardDirection();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpaceUpwardDirection <em>Pre Local Space Upward Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pre Local Space Upward Direction</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpaceUpwardDirection()
	 * @see #getPhotoMoveOperation()
	 * @generated
	 */
	EReference getPhotoMoveOperation_PreLocalSpaceUpwardDirection();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreTextureSpacePosition <em>Pre Texture Space Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pre Texture Space Position</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreTextureSpacePosition()
	 * @see #getPhotoMoveOperation()
	 * @generated
	 */
	EReference getPhotoMoveOperation_PreTextureSpacePosition();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.PhotoRemoveOperation <em>Photo Remove Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Photo Remove Operation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PhotoRemoveOperation
	 * @generated
	 */
	EClass getPhotoRemoveOperation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.PhotoRemoveOperation#isPlayDissolveAnimation <em>Play Dissolve Animation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Play Dissolve Animation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.PhotoRemoveOperation#isPlayDissolveAnimation()
	 * @see #getPhotoRemoveOperation()
	 * @generated
	 */
	EAttribute getPhotoRemoveOperation_PlayDissolveAnimation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.whiteboard.VisibleRelation <em>Visible Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visible Relation</em>'.
	 * @see dk.itu.cs.isa.whiteboard.VisibleRelation
	 * @generated
	 */
	EClass getVisibleRelation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.VisibleRelation#getElementName <em>Element Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Name</em>'.
	 * @see dk.itu.cs.isa.whiteboard.VisibleRelation#getElementName()
	 * @see #getVisibleRelation()
	 * @generated
	 */
	EAttribute getVisibleRelation_ElementName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.VisibleRelation#isForward <em>Forward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Forward</em>'.
	 * @see dk.itu.cs.isa.whiteboard.VisibleRelation#isForward()
	 * @see #getVisibleRelation()
	 * @generated
	 */
	EAttribute getVisibleRelation_Forward();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.whiteboard.VisibleRelation#getRelationType <em>Relation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Relation Type</em>'.
	 * @see dk.itu.cs.isa.whiteboard.VisibleRelation#getRelationType()
	 * @see #getVisibleRelation()
	 * @generated
	 */
	EAttribute getVisibleRelation_RelationType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WhiteboardFactory getWhiteboardFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardHistoryImpl <em>History</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardHistoryImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getWhiteboardHistory()
		 * @generated
		 */
		EClass WHITEBOARD_HISTORY = eINSTANCE.getWhiteboardHistory();

		/**
		 * The meta object literal for the '<em><b>System Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WHITEBOARD_HISTORY__SYSTEM_NAME = eINSTANCE.getWhiteboardHistory_SystemName();

		/**
		 * The meta object literal for the '<em><b>Whiteboards</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHITEBOARD_HISTORY__WHITEBOARDS = eINSTANCE.getWhiteboardHistory_Whiteboards();

		/**
		 * The meta object literal for the '<em><b>Event Log</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHITEBOARD_HISTORY__EVENT_LOG = eINSTANCE.getWhiteboardHistory_EventLog();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl <em>Whiteboard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getWhiteboard()
		 * @generated
		 */
		EClass WHITEBOARD = eINSTANCE.getWhiteboard();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WHITEBOARD__ID = eINSTANCE.getWhiteboard_Id();

		/**
		 * The meta object literal for the '<em><b>Containing History</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHITEBOARD__CONTAINING_HISTORY = eINSTANCE.getWhiteboard_ContainingHistory();

		/**
		 * The meta object literal for the '<em><b>Bitmap Base64</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WHITEBOARD__BITMAP_BASE64 = eINSTANCE.getWhiteboard_BitmapBase64();

		/**
		 * The meta object literal for the '<em><b>Pins</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHITEBOARD__PINS = eINSTANCE.getWhiteboard_Pins();

		/**
		 * The meta object literal for the '<em><b>Photos</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHITEBOARD__PHOTOS = eINSTANCE.getWhiteboard_Photos();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHITEBOARD__POSITION = eINSTANCE.getWhiteboard_Position();

		/**
		 * The meta object literal for the '<em><b>Rotation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHITEBOARD__ROTATION = eINSTANCE.getWhiteboard_Rotation();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WHITEBOARD__SIZE = eINSTANCE.getWhiteboard_Size();

		/**
		 * The meta object literal for the '<em><b>Undo Depth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WHITEBOARD__UNDO_DEPTH = eINSTANCE.getWhiteboard_UndoDepth();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PinImpl <em>Pin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PinImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPin()
		 * @generated
		 */
		EClass PIN = eINSTANCE.getPin();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIN__ID = eINSTANCE.getPin_Id();

		/**
		 * The meta object literal for the '<em><b>Element Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIN__ELEMENT_QUALIFIED_NAME = eINSTANCE.getPin_ElementQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Texture Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIN__TEXTURE_SPACE_POSITION = eINSTANCE.getPin_TextureSpacePosition();

		/**
		 * The meta object literal for the '<em><b>Local Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIN__LOCAL_SPACE_POSITION = eINSTANCE.getPin_LocalSpacePosition();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIN__COLOR = eINSTANCE.getPin_Color();

		/**
		 * The meta object literal for the '<em><b>Whiteboard</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIN__WHITEBOARD = eINSTANCE.getPin_Whiteboard();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PhotoImpl <em>Photo</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PhotoImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPhoto()
		 * @generated
		 */
		EClass PHOTO = eINSTANCE.getPhoto();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHOTO__ID = eINSTANCE.getPhoto_Id();

		/**
		 * The meta object literal for the '<em><b>Texture Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHOTO__TEXTURE_SPACE_POSITION = eINSTANCE.getPhoto_TextureSpacePosition();

		/**
		 * The meta object literal for the '<em><b>Local Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHOTO__LOCAL_SPACE_POSITION = eINSTANCE.getPhoto_LocalSpacePosition();

		/**
		 * The meta object literal for the '<em><b>Local Space Forward Direction</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHOTO__LOCAL_SPACE_FORWARD_DIRECTION = eINSTANCE.getPhoto_LocalSpaceForwardDirection();

		/**
		 * The meta object literal for the '<em><b>Local Space Upward Direction</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHOTO__LOCAL_SPACE_UPWARD_DIRECTION = eINSTANCE.getPhoto_LocalSpaceUpwardDirection();

		/**
		 * The meta object literal for the '<em><b>Bitmap Base64</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHOTO__BITMAP_BASE64 = eINSTANCE.getPhoto_BitmapBase64();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.ISAVector3Impl <em>ISA Vector3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.ISAVector3Impl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getISAVector3()
		 * @generated
		 */
		EClass ISA_VECTOR3 = eINSTANCE.getISAVector3();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__X = eINSTANCE.getISAVector3_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__Y = eINSTANCE.getISAVector3_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__Z = eINSTANCE.getISAVector3_Z();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.ISAVector2Impl <em>ISA Vector2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.ISAVector2Impl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getISAVector2()
		 * @generated
		 */
		EClass ISA_VECTOR2 = eINSTANCE.getISAVector2();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR2__X = eINSTANCE.getISAVector2_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR2__Y = eINSTANCE.getISAVector2_Y();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.ISAColorImpl <em>ISA Color</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.ISAColorImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getISAColor()
		 * @generated
		 */
		EClass ISA_COLOR = eINSTANCE.getISAColor();

		/**
		 * The meta object literal for the '<em><b>R</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_COLOR__R = eINSTANCE.getISAColor_R();

		/**
		 * The meta object literal for the '<em><b>G</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_COLOR__G = eINSTANCE.getISAColor_G();

		/**
		 * The meta object literal for the '<em><b>B</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_COLOR__B = eINSTANCE.getISAColor_B();

		/**
		 * The meta object literal for the '<em><b>A</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_COLOR__A = eINSTANCE.getISAColor_A();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractWhiteboardEventImpl <em>Abstract Whiteboard Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.AbstractWhiteboardEventImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractWhiteboardEvent()
		 * @generated
		 */
		EClass ABSTRACT_WHITEBOARD_EVENT = eINSTANCE.getAbstractWhiteboardEvent();

		/**
		 * The meta object literal for the '<em><b>Submitting Client Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID = eINSTANCE.getAbstractWhiteboardEvent_SubmittingClientId();

		/**
		 * The meta object literal for the '<em><b>Whiteboard Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID = eINSTANCE.getAbstractWhiteboardEvent_WhiteboardId();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP = eINSTANCE.getAbstractWhiteboardEvent_Timestamp();

		/**
		 * The meta object literal for the '<em><b>Confirmation Required</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED = eINSTANCE.getAbstractWhiteboardEvent_ConfirmationRequired();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.BoardSpawnEventImpl <em>Board Spawn Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.BoardSpawnEventImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getBoardSpawnEvent()
		 * @generated
		 */
		EClass BOARD_SPAWN_EVENT = eINSTANCE.getBoardSpawnEvent();

		/**
		 * The meta object literal for the '<em><b>World Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION = eINSTANCE.getBoardSpawnEvent_WorldSpacePosition();

		/**
		 * The meta object literal for the '<em><b>World Space Rotation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION = eINSTANCE.getBoardSpawnEvent_WorldSpaceRotation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.BoardDuplicateEventImpl <em>Board Duplicate Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.BoardDuplicateEventImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getBoardDuplicateEvent()
		 * @generated
		 */
		EClass BOARD_DUPLICATE_EVENT = eINSTANCE.getBoardDuplicateEvent();

		/**
		 * The meta object literal for the '<em><b>World Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOARD_DUPLICATE_EVENT__WORLD_SPACE_POSITION = eINSTANCE.getBoardDuplicateEvent_WorldSpacePosition();

		/**
		 * The meta object literal for the '<em><b>World Space Rotation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOARD_DUPLICATE_EVENT__WORLD_SPACE_ROTATION = eINSTANCE.getBoardDuplicateEvent_WorldSpaceRotation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.BoardDestroyEventImpl <em>Board Destroy Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.BoardDestroyEventImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getBoardDestroyEvent()
		 * @generated
		 */
		EClass BOARD_DESTROY_EVENT = eINSTANCE.getBoardDestroyEvent();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.UndoEventImpl <em>Undo Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.UndoEventImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getUndoEvent()
		 * @generated
		 */
		EClass UNDO_EVENT = eINSTANCE.getUndoEvent();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.RedoEventImpl <em>Redo Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.RedoEventImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getRedoEvent()
		 * @generated
		 */
		EClass REDO_EVENT = eINSTANCE.getRedoEvent();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractOperationImpl <em>Abstract Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.AbstractOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractOperation()
		 * @generated
		 */
		EClass ABSTRACT_OPERATION = eINSTANCE.getAbstractOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractCompoundOperationImpl <em>Abstract Compound Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.AbstractCompoundOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractCompoundOperation()
		 * @generated
		 */
		EClass ABSTRACT_COMPOUND_OPERATION = eINSTANCE.getAbstractCompoundOperation();

		/**
		 * The meta object literal for the '<em><b>Sub Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS = eINSTANCE.getAbstractCompoundOperation_SubOperations();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PinSplitOperationImpl <em>Pin Split Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PinSplitOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinSplitOperation()
		 * @generated
		 */
		EClass PIN_SPLIT_OPERATION = eINSTANCE.getPinSplitOperation();

		/**
		 * The meta object literal for the '<em><b>Pinned Element Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIN_SPLIT_OPERATION__PINNED_ELEMENT_NAME = eINSTANCE.getPinSplitOperation_PinnedElementName();

		/**
		 * The meta object literal for the '<em><b>Pin Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIN_SPLIT_OPERATION__PIN_ID = eINSTANCE.getPinSplitOperation_PinId();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PinsRemoveAllOperationImpl <em>Pins Remove All Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PinsRemoveAllOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinsRemoveAllOperation()
		 * @generated
		 */
		EClass PINS_REMOVE_ALL_OPERATION = eINSTANCE.getPinsRemoveAllOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractPinOperationImpl <em>Abstract Pin Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.AbstractPinOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractPinOperation()
		 * @generated
		 */
		EClass ABSTRACT_PIN_OPERATION = eINSTANCE.getAbstractPinOperation();

		/**
		 * The meta object literal for the '<em><b>Pin Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_PIN_OPERATION__PIN_ID = eINSTANCE.getAbstractPinOperation_PinId();

		/**
		 * The meta object literal for the '<em><b>Pinned Element Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME = eINSTANCE.getAbstractPinOperation_PinnedElementName();

		/**
		 * The meta object literal for the '<em><b>Texture Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION = eINSTANCE.getAbstractPinOperation_TextureSpacePosition();

		/**
		 * The meta object literal for the '<em><b>Local Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION = eINSTANCE.getAbstractPinOperation_LocalSpacePosition();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PIN_OPERATION__COLOR = eINSTANCE.getAbstractPinOperation_Color();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PinSoftwareAddOperationImpl <em>Pin Software Add Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PinSoftwareAddOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinSoftwareAddOperation()
		 * @generated
		 */
		EClass PIN_SOFTWARE_ADD_OPERATION = eINSTANCE.getPinSoftwareAddOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PinAudioAddOperationImpl <em>Pin Audio Add Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PinAudioAddOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinAudioAddOperation()
		 * @generated
		 */
		EClass PIN_AUDIO_ADD_OPERATION = eINSTANCE.getPinAudioAddOperation();

		/**
		 * The meta object literal for the '<em><b>Sound Wave Base64</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_BASE64 = eINSTANCE.getPinAudioAddOperation_SoundWaveBase64();

		/**
		 * The meta object literal for the '<em><b>Sound Wave Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_FREQUENCY = eINSTANCE.getPinAudioAddOperation_SoundWaveFrequency();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PinRemoveOperationImpl <em>Pin Remove Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PinRemoveOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinRemoveOperation()
		 * @generated
		 */
		EClass PIN_REMOVE_OPERATION = eINSTANCE.getPinRemoveOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PinMoveOperationImpl <em>Pin Move Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PinMoveOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPinMoveOperation()
		 * @generated
		 */
		EClass PIN_MOVE_OPERATION = eINSTANCE.getPinMoveOperation();

		/**
		 * The meta object literal for the '<em><b>Pre Texture Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION = eINSTANCE.getPinMoveOperation_PreTextureSpacePosition();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractDrawOperationImpl <em>Abstract Draw Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.AbstractDrawOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractDrawOperation()
		 * @generated
		 */
		EClass ABSTRACT_DRAW_OPERATION = eINSTANCE.getAbstractDrawOperation();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_DRAW_OPERATION__COLOR = eINSTANCE.getAbstractDrawOperation_Color();

		/**
		 * The meta object literal for the '<em><b>Pen Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_DRAW_OPERATION__PEN_SIZE = eINSTANCE.getAbstractDrawOperation_PenSize();

		/**
		 * The meta object literal for the '<em><b>Drawn Lines</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_DRAW_OPERATION__DRAWN_LINES = eINSTANCE.getAbstractDrawOperation_DrawnLines();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.DrawFreehandOperationImpl <em>Draw Freehand Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.DrawFreehandOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getDrawFreehandOperation()
		 * @generated
		 */
		EClass DRAW_FREEHAND_OPERATION = eINSTANCE.getDrawFreehandOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.DrawModuleOperationImpl <em>Draw Module Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.DrawModuleOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getDrawModuleOperation()
		 * @generated
		 */
		EClass DRAW_MODULE_OPERATION = eINSTANCE.getDrawModuleOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.DrawArrowOperationImpl <em>Draw Arrow Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.DrawArrowOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getDrawArrowOperation()
		 * @generated
		 */
		EClass DRAW_ARROW_OPERATION = eINSTANCE.getDrawArrowOperation();

		/**
		 * The meta object literal for the '<em><b>Auto Draw Arrow Head</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DRAW_ARROW_OPERATION__AUTO_DRAW_ARROW_HEAD = eINSTANCE.getDrawArrowOperation_AutoDrawArrowHead();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PaintClearAllOperationImpl <em>Paint Clear All Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PaintClearAllOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPaintClearAllOperation()
		 * @generated
		 */
		EClass PAINT_CLEAR_ALL_OPERATION = eINSTANCE.getPaintClearAllOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PaintEraseOperationImpl <em>Paint Erase Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PaintEraseOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPaintEraseOperation()
		 * @generated
		 */
		EClass PAINT_ERASE_OPERATION = eINSTANCE.getPaintEraseOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.BoardEnlargeOperationImpl <em>Board Enlarge Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.BoardEnlargeOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getBoardEnlargeOperation()
		 * @generated
		 */
		EClass BOARD_ENLARGE_OPERATION = eINSTANCE.getBoardEnlargeOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl <em>Abstract Photo Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getAbstractPhotoOperation()
		 * @generated
		 */
		EClass ABSTRACT_PHOTO_OPERATION = eINSTANCE.getAbstractPhotoOperation();

		/**
		 * The meta object literal for the '<em><b>Photo Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_PHOTO_OPERATION__PHOTO_ID = eINSTANCE.getAbstractPhotoOperation_PhotoId();

		/**
		 * The meta object literal for the '<em><b>Bitmap Base64</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64 = eINSTANCE.getAbstractPhotoOperation_BitmapBase64();

		/**
		 * The meta object literal for the '<em><b>Local Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION = eINSTANCE.getAbstractPhotoOperation_LocalSpacePosition();

		/**
		 * The meta object literal for the '<em><b>Local Space Forward Direction</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION = eINSTANCE.getAbstractPhotoOperation_LocalSpaceForwardDirection();

		/**
		 * The meta object literal for the '<em><b>Local Space Upward Direction</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION = eINSTANCE.getAbstractPhotoOperation_LocalSpaceUpwardDirection();

		/**
		 * The meta object literal for the '<em><b>Texture Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION = eINSTANCE.getAbstractPhotoOperation_TextureSpacePosition();

		/**
		 * The meta object literal for the '<em><b>Camera Position When Photo Was Taken</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN = eINSTANCE.getAbstractPhotoOperation_CameraPositionWhenPhotoWasTaken();

		/**
		 * The meta object literal for the '<em><b>Camera Rotation When Photo Was Taken</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN = eINSTANCE.getAbstractPhotoOperation_CameraRotationWhenPhotoWasTaken();

		/**
		 * The meta object literal for the '<em><b>Relations Visible When Photo Was Taken</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN = eINSTANCE.getAbstractPhotoOperation_RelationsVisibleWhenPhotoWasTaken();

		/**
		 * The meta object literal for the '<em><b>Open Capsules Element Qualified Names When Photo Was Taken</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN = eINSTANCE.getAbstractPhotoOperation_OpenCapsulesElementQualifiedNamesWhenPhotoWasTaken();

		/**
		 * The meta object literal for the '<em><b>System Size When Photo Was Taken</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN = eINSTANCE.getAbstractPhotoOperation_SystemSizeWhenPhotoWasTaken();

		/**
		 * The meta object literal for the '<em><b>System Position When Photo Was Taken</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN = eINSTANCE.getAbstractPhotoOperation_SystemPositionWhenPhotoWasTaken();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PhotoAddOperationImpl <em>Photo Add Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PhotoAddOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPhotoAddOperation()
		 * @generated
		 */
		EClass PHOTO_ADD_OPERATION = eINSTANCE.getPhotoAddOperation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PhotoMoveOperationImpl <em>Photo Move Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PhotoMoveOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPhotoMoveOperation()
		 * @generated
		 */
		EClass PHOTO_MOVE_OPERATION = eINSTANCE.getPhotoMoveOperation();

		/**
		 * The meta object literal for the '<em><b>Pre Local Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION = eINSTANCE.getPhotoMoveOperation_PreLocalSpacePosition();

		/**
		 * The meta object literal for the '<em><b>Pre Local Space Forward Direction</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION = eINSTANCE.getPhotoMoveOperation_PreLocalSpaceForwardDirection();

		/**
		 * The meta object literal for the '<em><b>Pre Local Space Upward Direction</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION = eINSTANCE.getPhotoMoveOperation_PreLocalSpaceUpwardDirection();

		/**
		 * The meta object literal for the '<em><b>Pre Texture Space Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION = eINSTANCE.getPhotoMoveOperation_PreTextureSpacePosition();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.PhotoRemoveOperationImpl <em>Photo Remove Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.PhotoRemoveOperationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getPhotoRemoveOperation()
		 * @generated
		 */
		EClass PHOTO_REMOVE_OPERATION = eINSTANCE.getPhotoRemoveOperation();

		/**
		 * The meta object literal for the '<em><b>Play Dissolve Animation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHOTO_REMOVE_OPERATION__PLAY_DISSOLVE_ANIMATION = eINSTANCE.getPhotoRemoveOperation_PlayDissolveAnimation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.whiteboard.impl.VisibleRelationImpl <em>Visible Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.whiteboard.impl.VisibleRelationImpl
		 * @see dk.itu.cs.isa.whiteboard.impl.WhiteboardPackageImpl#getVisibleRelation()
		 * @generated
		 */
		EClass VISIBLE_RELATION = eINSTANCE.getVisibleRelation();

		/**
		 * The meta object literal for the '<em><b>Element Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISIBLE_RELATION__ELEMENT_NAME = eINSTANCE.getVisibleRelation_ElementName();

		/**
		 * The meta object literal for the '<em><b>Forward</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISIBLE_RELATION__FORWARD = eINSTANCE.getVisibleRelation_Forward();

		/**
		 * The meta object literal for the '<em><b>Relation Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VISIBLE_RELATION__RELATION_TYPE = eINSTANCE.getVisibleRelation_RelationType();

	}

} //WhiteboardPackage

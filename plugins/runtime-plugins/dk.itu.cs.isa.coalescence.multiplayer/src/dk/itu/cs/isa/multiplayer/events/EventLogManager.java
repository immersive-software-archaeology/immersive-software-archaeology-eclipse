package dk.itu.cs.isa.multiplayer.events;

import java.util.List;

import dk.itu.cs.isa.multiplayer.AbstractEvent;
import dk.itu.cs.isa.multiplayer.AudioEvent;
import dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent;
import dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent;
import dk.itu.cs.isa.multiplayer.ClearAllRelationsEvent;
import dk.itu.cs.isa.multiplayer.ClearRelationsEvent;
import dk.itu.cs.isa.multiplayer.ElementType;
import dk.itu.cs.isa.multiplayer.EventLog;
import dk.itu.cs.isa.multiplayer.ClearOneRelationEvent;
import dk.itu.cs.isa.multiplayer.MultiplayerFactory;
import dk.itu.cs.isa.multiplayer.ShowRelationEvent;
import dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent;
import dk.itu.cs.isa.multiplayer.SystemMoveEvent;

public class EventLogManager {

	private EventLog log;
	
	

	public EventLogManager() {
		log = MultiplayerFactory.eINSTANCE.createEventLog();
	}
	
	

	public EventLog getLog() {
		return log;
	}

	public void addOperationsAndCleanUp(List<AbstractEvent> submittedOperations) {
		log.getLog().addAll(submittedOperations);
		cleanLog();
	}
	
	

	private void cleanLog() {
		for (int i = log.getLog().size() - 1; i >= 0; i--) {
			if(i >= log.getLog().size())
				i = log.getLog().size() - 1;
			
			AbstractEvent youngerEvent = log.getLog().get(i);
			if (youngerEvent == null)
				continue;

			for (int j = i - 1; j >= 0; j--) {
				if(j >= i)
					j = i - 1;
				
				AbstractEvent olderEvent = log.getLog().get(j);
				if (olderEvent == null)
					continue;

				if (!youngerEvent.getSystemName().equals(olderEvent.getSystemName()))
					System.err.println("How can this be: system names of events in same log don't match!?");

				boolean eventRemoved;
				if (youngerEvent instanceof ShowRelationEvent)
					eventRemoved = handleCleanUp((ShowRelationEvent) youngerEvent, olderEvent);
				else if (youngerEvent instanceof ClearOneRelationEvent)
					eventRemoved = handleCleanUp((ClearOneRelationEvent) youngerEvent, olderEvent);
				else if (youngerEvent instanceof ClearRelationsEvent)
					eventRemoved = handleCleanUp((ClearRelationsEvent) youngerEvent, olderEvent);
				else if (youngerEvent instanceof ClearAllRelationsEvent)
					eventRemoved = handleCleanUp((ClearAllRelationsEvent) youngerEvent, olderEvent);
				else if (youngerEvent instanceof ChangeElementActiveStateEvent)
					eventRemoved = handleCleanUp((ChangeElementActiveStateEvent) youngerEvent, olderEvent);
				else if (youngerEvent instanceof SystemMoveEvent)
					eventRemoved = handleCleanUp((SystemMoveEvent) youngerEvent, olderEvent);
				else if (youngerEvent instanceof ChangeNameplateVisibilityEvent)
					eventRemoved = handleCleanUp((ChangeNameplateVisibilityEvent) youngerEvent, olderEvent);
				else if (youngerEvent instanceof SphereOpenCloseEvent)
					eventRemoved = handleCleanUp((SphereOpenCloseEvent) youngerEvent, olderEvent);
				else if (youngerEvent instanceof AudioEvent)
					eventRemoved = handleCleanUp((AudioEvent) youngerEvent, olderEvent);
				else throw new RuntimeException("Encountered an unhandled kind of event when cleaning up the log.");
				
				if(eventRemoved) {
					log.getLog().remove(j);
					i--;
					//j--;
					if(log.getLog().size() == 0)
						break;
				}
			}
		}
	}



	private boolean handleCleanUp(ShowRelationEvent youngerEvent, AbstractEvent olderEvent) {
		if (olderEvent instanceof ShowRelationEvent) {
			if (youngerEvent.getElementName().equals(((ShowRelationEvent) olderEvent).getElementName()))
				if (youngerEvent.getDirection() == ((ShowRelationEvent) olderEvent).getDirection())
					if (youngerEvent.getRelationType() == ((ShowRelationEvent) olderEvent).getRelationType())
						return true;
		} else if (olderEvent instanceof ClearOneRelationEvent) {
			if (youngerEvent.getElementName().equals(((ClearOneRelationEvent) olderEvent).getElementName()))
				if (youngerEvent.getDirection() == ((ClearOneRelationEvent) olderEvent).getDirection())
					if (youngerEvent.getRelationType() == ((ClearOneRelationEvent) olderEvent).getRelationType())
						return true;
		}
		return false;
	}

	private boolean handleCleanUp(ClearOneRelationEvent youngerEvent, AbstractEvent olderEvent) {
		if (olderEvent instanceof ShowRelationEvent) {
			if (youngerEvent.getElementName().equals(((ShowRelationEvent) olderEvent).getElementName()))
				if (youngerEvent.getDirection() == ((ShowRelationEvent) olderEvent).getDirection())
					if (youngerEvent.getRelationType() == ((ShowRelationEvent) olderEvent).getRelationType())
						return true;
		} else if (olderEvent instanceof ClearOneRelationEvent) {
			if (youngerEvent.getElementName().equals(((ClearOneRelationEvent) olderEvent).getElementName()))
				if (youngerEvent.getDirection() == ((ClearOneRelationEvent) olderEvent).getDirection())
					if (youngerEvent.getRelationType() == ((ClearOneRelationEvent) olderEvent).getRelationType())
						return true;
		}
		return false;
	}

	private boolean handleCleanUp(ClearRelationsEvent youngerEvent, AbstractEvent olderEvent) {
		if (olderEvent instanceof ShowRelationEvent) {
			if (((ClearRelationsEvent) youngerEvent).getElementName().equals(((ShowRelationEvent) olderEvent).getElementName()))
				return true;
		} else if (olderEvent instanceof ClearOneRelationEvent) {
			if (((ClearRelationsEvent) youngerEvent).getElementName().equals(((ClearOneRelationEvent) olderEvent).getElementName()))
				return true;
		} else if (olderEvent instanceof ClearRelationsEvent) {
			if (((ClearRelationsEvent) youngerEvent).getElementName().equals(((ClearRelationsEvent) olderEvent).getElementName()))
				return true;
		}
		return false;
	}

	private boolean handleCleanUp(ClearAllRelationsEvent youngerEvent, AbstractEvent olderEvent) {
		if (olderEvent instanceof ShowRelationEvent)
			return true;
		else if (olderEvent instanceof ClearOneRelationEvent)
			return true;
		else if (olderEvent instanceof ClearRelationsEvent)
			return true;
		else if (olderEvent instanceof ClearAllRelationsEvent)
			return true;
		else
			return false;
	}

	private boolean handleCleanUp(ChangeElementActiveStateEvent youngerEvent, AbstractEvent olderEvent) {
		if (!(olderEvent instanceof ChangeElementActiveStateEvent))
			return false;
		
		int element1 = youngerEvent.getElementId();
		int element2 = ((ChangeElementActiveStateEvent) olderEvent).getElementId();
		if (element1 != element2)
			return false;
		
		ElementType type1 = youngerEvent.getElementType();
		ElementType type2 = ((ChangeElementActiveStateEvent) olderEvent).getElementType();
		if (type1 != type2)
			return false;
		
		// commented out because it does not matter _who_ sends the operation,
		// at this point we know it is obsolete anyways
//		int client1 = youngerEvent.getClientId();
//		int client2 = olderEvent.getClientId();
//		if (client1 != client2)
//			return false;
		
		return true;
	}

	private boolean handleCleanUp(SystemMoveEvent youngerEvent, AbstractEvent olderEvent) {
		return olderEvent instanceof SystemMoveEvent;
	}
	
	private boolean handleCleanUp(ChangeNameplateVisibilityEvent youngerEvent, AbstractEvent olderEvent) {
		return olderEvent instanceof ChangeNameplateVisibilityEvent;
	}
	
	private boolean handleCleanUp(SphereOpenCloseEvent youngerEvent, AbstractEvent olderEvent) {
		if (!(olderEvent instanceof SphereOpenCloseEvent))
			return false;
		
		String element1 = youngerEvent.getElementName();
		String element2 = ((SphereOpenCloseEvent) olderEvent).getElementName();
		return element1.equals(element2);
	}
	
	private boolean handleCleanUp(AudioEvent youngerEvent, AbstractEvent olderEvent) {
		if (!(olderEvent instanceof AudioEvent))
			return false;
		
		int id1 = ((AudioEvent) youngerEvent).getPinId();
		int id2 = ((AudioEvent) olderEvent).getPinId();
		if (id1 != id2)
			return false;
		
		return true;
	}

}





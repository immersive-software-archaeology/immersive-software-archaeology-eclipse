/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAAbstractClass;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Abstract Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ISAAbstractClassImpl extends ISAClassImpl implements ISAAbstractClass {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAAbstractClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.ISA_ABSTRACT_CLASS;
	}

} //ISAAbstractClassImpl

package dk.itu.cs.isa.eclipse.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.progress.UIJob;

import dk.itu.cs.isa.analyzer.interfaces.ISystemAnalysisObserver;
import dk.itu.cs.isa.events.ISAEventSystem;

public class SystemModelsObserver implements ISystemAnalysisObserver {

	private static List<ISAUpdatableView> registeredViews = new ArrayList<>();

	
	
	public static boolean registerView(ISAUpdatableView view) {
		return registeredViews.add(view);
	}
	public static boolean unregisterView(ISAUpdatableView view) {
		return registeredViews.remove(view);
	}
	
	
	
	public static void updateViews() {
		for(ISAUpdatableView view : registeredViews)
			view.update();
	}
	
	

	@Override
	public void notifyAnalysisFinished(String systemName, int executionDurationMillis) {
		updateViews();
		
		int returnCode = ISAEventSystem.fireAnalysisFinishedDialog(executionDurationMillis).getReturnCode();
		if(returnCode == 1) {
			final IWorkbench workbench = PlatformUI.getWorkbench();
			new UIJob("Switching perspectives") {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					try {
						workbench.showPerspective("dk.itu.cs.isa.eclipse.ui.perspective", workbench.getActiveWorkbenchWindow());
					} catch (WorkbenchException e) {
						return new Status(IStatus.ERROR, this.getClass(), "Error while switching perspectives", e);
					}
					return Status.OK_STATUS;
				}
			}.run(new NullProgressMonitor());
		}
	}

}

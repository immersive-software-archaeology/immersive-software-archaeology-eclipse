/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.ISAVector3;
import dk.itu.cs.isa.whiteboard.Photo;
import dk.itu.cs.isa.whiteboard.Pin;
import dk.itu.cs.isa.whiteboard.Whiteboard;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Whiteboard</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl#getId <em>Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl#getContainingHistory <em>Containing History</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl#getBitmapBase64 <em>Bitmap Base64</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl#getPins <em>Pins</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl#getPhotos <em>Photos</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl#getRotation <em>Rotation</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl#getSize <em>Size</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardImpl#getUndoDepth <em>Undo Depth</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WhiteboardImpl extends MinimalEObjectImpl.Container implements Whiteboard {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getBitmapBase64() <em>Bitmap Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitmapBase64()
	 * @generated
	 * @ordered
	 */
	protected static final String BITMAP_BASE64_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBitmapBase64() <em>Bitmap Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitmapBase64()
	 * @generated
	 * @ordered
	 */
	protected String bitmapBase64 = BITMAP_BASE64_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPins() <em>Pins</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPins()
	 * @generated
	 * @ordered
	 */
	protected EList<Pin> pins;

	/**
	 * The cached value of the '{@link #getPhotos() <em>Photos</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhotos()
	 * @generated
	 * @ordered
	 */
	protected EList<Photo> photos;

	/**
	 * The cached value of the '{@link #getPosition() <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 position;

	/**
	 * The cached value of the '{@link #getRotation() <em>Rotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRotation()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 rotation;

	/**
	 * The default value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected static final int SIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected int size = SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUndoDepth() <em>Undo Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUndoDepth()
	 * @generated
	 * @ordered
	 */
	protected static final int UNDO_DEPTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getUndoDepth() <em>Undo Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUndoDepth()
	 * @generated
	 * @ordered
	 */
	protected int undoDepth = UNDO_DEPTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WhiteboardImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.WHITEBOARD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhiteboardHistory getContainingHistory() {
		if (eContainerFeatureID() != WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY) return null;
		return (WhiteboardHistory)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainingHistory(WhiteboardHistory newContainingHistory, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContainingHistory, WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingHistory(WhiteboardHistory newContainingHistory) {
		if (newContainingHistory != eInternalContainer() || (eContainerFeatureID() != WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY && newContainingHistory != null)) {
			if (EcoreUtil.isAncestor(this, newContainingHistory))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainingHistory != null)
				msgs = ((InternalEObject)newContainingHistory).eInverseAdd(this, WhiteboardPackage.WHITEBOARD_HISTORY__WHITEBOARDS, WhiteboardHistory.class, msgs);
			msgs = basicSetContainingHistory(newContainingHistory, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY, newContainingHistory, newContainingHistory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBitmapBase64() {
		return bitmapBase64;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitmapBase64(String newBitmapBase64) {
		String oldBitmapBase64 = bitmapBase64;
		bitmapBase64 = newBitmapBase64;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD__BITMAP_BASE64, oldBitmapBase64, bitmapBase64));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Pin> getPins() {
		if (pins == null) {
			pins = new EObjectContainmentWithInverseEList<Pin>(Pin.class, this, WhiteboardPackage.WHITEBOARD__PINS, WhiteboardPackage.PIN__WHITEBOARD);
		}
		return pins;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Photo> getPhotos() {
		if (photos == null) {
			photos = new EObjectContainmentEList<Photo>(Photo.class, this, WhiteboardPackage.WHITEBOARD__PHOTOS);
		}
		return photos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getPosition() {
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPosition(ISAVector3 newPosition, NotificationChain msgs) {
		ISAVector3 oldPosition = position;
		position = newPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD__POSITION, oldPosition, newPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(ISAVector3 newPosition) {
		if (newPosition != position) {
			NotificationChain msgs = null;
			if (position != null)
				msgs = ((InternalEObject)position).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.WHITEBOARD__POSITION, null, msgs);
			if (newPosition != null)
				msgs = ((InternalEObject)newPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.WHITEBOARD__POSITION, null, msgs);
			msgs = basicSetPosition(newPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD__POSITION, newPosition, newPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getRotation() {
		return rotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRotation(ISAVector3 newRotation, NotificationChain msgs) {
		ISAVector3 oldRotation = rotation;
		rotation = newRotation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD__ROTATION, oldRotation, newRotation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRotation(ISAVector3 newRotation) {
		if (newRotation != rotation) {
			NotificationChain msgs = null;
			if (rotation != null)
				msgs = ((InternalEObject)rotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.WHITEBOARD__ROTATION, null, msgs);
			if (newRotation != null)
				msgs = ((InternalEObject)newRotation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.WHITEBOARD__ROTATION, null, msgs);
			msgs = basicSetRotation(newRotation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD__ROTATION, newRotation, newRotation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSize() {
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSize(int newSize) {
		int oldSize = size;
		size = newSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD__SIZE, oldSize, size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getUndoDepth() {
		return undoDepth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUndoDepth(int newUndoDepth) {
		int oldUndoDepth = undoDepth;
		undoDepth = newUndoDepth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD__UNDO_DEPTH, oldUndoDepth, undoDepth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainingHistory((WhiteboardHistory)otherEnd, msgs);
			case WhiteboardPackage.WHITEBOARD__PINS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPins()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY:
				return basicSetContainingHistory(null, msgs);
			case WhiteboardPackage.WHITEBOARD__PINS:
				return ((InternalEList<?>)getPins()).basicRemove(otherEnd, msgs);
			case WhiteboardPackage.WHITEBOARD__PHOTOS:
				return ((InternalEList<?>)getPhotos()).basicRemove(otherEnd, msgs);
			case WhiteboardPackage.WHITEBOARD__POSITION:
				return basicSetPosition(null, msgs);
			case WhiteboardPackage.WHITEBOARD__ROTATION:
				return basicSetRotation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY:
				return eInternalContainer().eInverseRemove(this, WhiteboardPackage.WHITEBOARD_HISTORY__WHITEBOARDS, WhiteboardHistory.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD__ID:
				return getId();
			case WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY:
				return getContainingHistory();
			case WhiteboardPackage.WHITEBOARD__BITMAP_BASE64:
				return getBitmapBase64();
			case WhiteboardPackage.WHITEBOARD__PINS:
				return getPins();
			case WhiteboardPackage.WHITEBOARD__PHOTOS:
				return getPhotos();
			case WhiteboardPackage.WHITEBOARD__POSITION:
				return getPosition();
			case WhiteboardPackage.WHITEBOARD__ROTATION:
				return getRotation();
			case WhiteboardPackage.WHITEBOARD__SIZE:
				return getSize();
			case WhiteboardPackage.WHITEBOARD__UNDO_DEPTH:
				return getUndoDepth();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD__ID:
				setId((Integer)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY:
				setContainingHistory((WhiteboardHistory)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD__BITMAP_BASE64:
				setBitmapBase64((String)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD__PINS:
				getPins().clear();
				getPins().addAll((Collection<? extends Pin>)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD__PHOTOS:
				getPhotos().clear();
				getPhotos().addAll((Collection<? extends Photo>)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD__POSITION:
				setPosition((ISAVector3)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD__ROTATION:
				setRotation((ISAVector3)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD__SIZE:
				setSize((Integer)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD__UNDO_DEPTH:
				setUndoDepth((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD__ID:
				setId(ID_EDEFAULT);
				return;
			case WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY:
				setContainingHistory((WhiteboardHistory)null);
				return;
			case WhiteboardPackage.WHITEBOARD__BITMAP_BASE64:
				setBitmapBase64(BITMAP_BASE64_EDEFAULT);
				return;
			case WhiteboardPackage.WHITEBOARD__PINS:
				getPins().clear();
				return;
			case WhiteboardPackage.WHITEBOARD__PHOTOS:
				getPhotos().clear();
				return;
			case WhiteboardPackage.WHITEBOARD__POSITION:
				setPosition((ISAVector3)null);
				return;
			case WhiteboardPackage.WHITEBOARD__ROTATION:
				setRotation((ISAVector3)null);
				return;
			case WhiteboardPackage.WHITEBOARD__SIZE:
				setSize(SIZE_EDEFAULT);
				return;
			case WhiteboardPackage.WHITEBOARD__UNDO_DEPTH:
				setUndoDepth(UNDO_DEPTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD__ID:
				return id != ID_EDEFAULT;
			case WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY:
				return getContainingHistory() != null;
			case WhiteboardPackage.WHITEBOARD__BITMAP_BASE64:
				return BITMAP_BASE64_EDEFAULT == null ? bitmapBase64 != null : !BITMAP_BASE64_EDEFAULT.equals(bitmapBase64);
			case WhiteboardPackage.WHITEBOARD__PINS:
				return pins != null && !pins.isEmpty();
			case WhiteboardPackage.WHITEBOARD__PHOTOS:
				return photos != null && !photos.isEmpty();
			case WhiteboardPackage.WHITEBOARD__POSITION:
				return position != null;
			case WhiteboardPackage.WHITEBOARD__ROTATION:
				return rotation != null;
			case WhiteboardPackage.WHITEBOARD__SIZE:
				return size != SIZE_EDEFAULT;
			case WhiteboardPackage.WHITEBOARD__UNDO_DEPTH:
				return undoDepth != UNDO_DEPTH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", bitmapBase64: ");
		result.append(bitmapBase64);
		result.append(", size: ");
		result.append(size);
		result.append(", undoDepth: ");
		result.append(undoDepth);
		result.append(')');
		return result.toString();
	}

} //WhiteboardImpl

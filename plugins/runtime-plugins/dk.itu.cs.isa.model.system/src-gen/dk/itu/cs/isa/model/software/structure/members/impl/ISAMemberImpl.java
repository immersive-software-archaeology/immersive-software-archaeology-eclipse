/**
 */
package dk.itu.cs.isa.model.software.structure.members.impl;

import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifier;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceableElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Member</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMemberImpl#getModifiers <em>Modifiers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMemberImpl#getNumberInOrderOfDeclaration <em>Number In Order Of Declaration</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMemberImpl#getStartPositionInDocument <em>Start Position In Document</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMemberImpl#getEndPositionInDocument <em>End Position In Document</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAMemberImpl extends ISAReferenceableElementImpl implements ISAMember {
	/**
	 * The cached value of the '{@link #getModifiers() <em>Modifiers</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAModifier> modifiers;

	/**
	 * The default value of the '{@link #getNumberInOrderOfDeclaration() <em>Number In Order Of Declaration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberInOrderOfDeclaration()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberInOrderOfDeclaration() <em>Number In Order Of Declaration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberInOrderOfDeclaration()
	 * @generated
	 * @ordered
	 */
	protected int numberInOrderOfDeclaration = NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartPositionInDocument() <em>Start Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected static final int START_POSITION_IN_DOCUMENT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStartPositionInDocument() <em>Start Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected int startPositionInDocument = START_POSITION_IN_DOCUMENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndPositionInDocument() <em>End Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected static final int END_POSITION_IN_DOCUMENT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEndPositionInDocument() <em>End Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected int endPositionInDocument = END_POSITION_IN_DOCUMENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAMemberImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MembersPackage.Literals.ISA_MEMBER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAModifier> getModifiers() {
		if (modifiers == null) {
			modifiers = new EDataTypeUniqueEList<ISAModifier>(ISAModifier.class, this, MembersPackage.ISA_MEMBER__MODIFIERS);
		}
		return modifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberInOrderOfDeclaration() {
		return numberInOrderOfDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberInOrderOfDeclaration(int newNumberInOrderOfDeclaration) {
		int oldNumberInOrderOfDeclaration = numberInOrderOfDeclaration;
		numberInOrderOfDeclaration = newNumberInOrderOfDeclaration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION, oldNumberInOrderOfDeclaration, numberInOrderOfDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getStartPositionInDocument() {
		return startPositionInDocument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartPositionInDocument(int newStartPositionInDocument) {
		int oldStartPositionInDocument = startPositionInDocument;
		startPositionInDocument = newStartPositionInDocument;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_MEMBER__START_POSITION_IN_DOCUMENT, oldStartPositionInDocument, startPositionInDocument));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getEndPositionInDocument() {
		return endPositionInDocument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndPositionInDocument(int newEndPositionInDocument) {
		int oldEndPositionInDocument = endPositionInDocument;
		endPositionInDocument = newEndPositionInDocument;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_MEMBER__END_POSITION_IN_DOCUMENT, oldEndPositionInDocument, endPositionInDocument));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MembersPackage.ISA_MEMBER__MODIFIERS:
				return getModifiers();
			case MembersPackage.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION:
				return getNumberInOrderOfDeclaration();
			case MembersPackage.ISA_MEMBER__START_POSITION_IN_DOCUMENT:
				return getStartPositionInDocument();
			case MembersPackage.ISA_MEMBER__END_POSITION_IN_DOCUMENT:
				return getEndPositionInDocument();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MembersPackage.ISA_MEMBER__MODIFIERS:
				getModifiers().clear();
				getModifiers().addAll((Collection<? extends ISAModifier>)newValue);
				return;
			case MembersPackage.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION:
				setNumberInOrderOfDeclaration((Integer)newValue);
				return;
			case MembersPackage.ISA_MEMBER__START_POSITION_IN_DOCUMENT:
				setStartPositionInDocument((Integer)newValue);
				return;
			case MembersPackage.ISA_MEMBER__END_POSITION_IN_DOCUMENT:
				setEndPositionInDocument((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_MEMBER__MODIFIERS:
				getModifiers().clear();
				return;
			case MembersPackage.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION:
				setNumberInOrderOfDeclaration(NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT);
				return;
			case MembersPackage.ISA_MEMBER__START_POSITION_IN_DOCUMENT:
				setStartPositionInDocument(START_POSITION_IN_DOCUMENT_EDEFAULT);
				return;
			case MembersPackage.ISA_MEMBER__END_POSITION_IN_DOCUMENT:
				setEndPositionInDocument(END_POSITION_IN_DOCUMENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_MEMBER__MODIFIERS:
				return modifiers != null && !modifiers.isEmpty();
			case MembersPackage.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION:
				return numberInOrderOfDeclaration != NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT;
			case MembersPackage.ISA_MEMBER__START_POSITION_IN_DOCUMENT:
				return startPositionInDocument != START_POSITION_IN_DOCUMENT_EDEFAULT;
			case MembersPackage.ISA_MEMBER__END_POSITION_IN_DOCUMENT:
				return endPositionInDocument != END_POSITION_IN_DOCUMENT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAModifiableElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_MEMBER__MODIFIERS: return MiscPackage.ISA_MODIFIABLE_ELEMENT__MODIFIERS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAModifiableElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_MODIFIABLE_ELEMENT__MODIFIERS: return MembersPackage.ISA_MEMBER__MODIFIERS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (modifiers: ");
		result.append(modifiers);
		result.append(", numberInOrderOfDeclaration: ");
		result.append(numberInOrderOfDeclaration);
		result.append(", startPositionInDocument: ");
		result.append(startPositionInDocument);
		result.append(", endPositionInDocument: ");
		result.append(endPositionInDocument);
		result.append(')');
		return result.toString();
	}

} //ISAMemberImpl

/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Photo Move Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpacePosition <em>Pre Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpaceForwardDirection <em>Pre Local Space Forward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpaceUpwardDirection <em>Pre Local Space Upward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreTextureSpacePosition <em>Pre Texture Space Position</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhotoMoveOperation()
 * @model
 * @generated
 */
public interface PhotoMoveOperation extends AbstractPhotoOperation {
	/**
	 * Returns the value of the '<em><b>Pre Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Local Space Position</em>' containment reference.
	 * @see #setPreLocalSpacePosition(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhotoMoveOperation_PreLocalSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getPreLocalSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpacePosition <em>Pre Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Local Space Position</em>' containment reference.
	 * @see #getPreLocalSpacePosition()
	 * @generated
	 */
	void setPreLocalSpacePosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Pre Local Space Forward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Local Space Forward Direction</em>' containment reference.
	 * @see #setPreLocalSpaceForwardDirection(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhotoMoveOperation_PreLocalSpaceForwardDirection()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getPreLocalSpaceForwardDirection();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpaceForwardDirection <em>Pre Local Space Forward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Local Space Forward Direction</em>' containment reference.
	 * @see #getPreLocalSpaceForwardDirection()
	 * @generated
	 */
	void setPreLocalSpaceForwardDirection(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Pre Local Space Upward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Local Space Upward Direction</em>' containment reference.
	 * @see #setPreLocalSpaceUpwardDirection(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhotoMoveOperation_PreLocalSpaceUpwardDirection()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getPreLocalSpaceUpwardDirection();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreLocalSpaceUpwardDirection <em>Pre Local Space Upward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Local Space Upward Direction</em>' containment reference.
	 * @see #getPreLocalSpaceUpwardDirection()
	 * @generated
	 */
	void setPreLocalSpaceUpwardDirection(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Pre Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Texture Space Position</em>' containment reference.
	 * @see #setPreTextureSpacePosition(ISAVector2)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhotoMoveOperation_PreTextureSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector2 getPreTextureSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation#getPreTextureSpacePosition <em>Pre Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Texture Space Position</em>' containment reference.
	 * @see #getPreTextureSpacePosition()
	 * @generated
	 */
	void setPreTextureSpacePosition(ISAVector2 value);

} // PhotoMoveOperation

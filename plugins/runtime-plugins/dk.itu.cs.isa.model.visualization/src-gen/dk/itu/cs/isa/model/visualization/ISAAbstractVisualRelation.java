/**
 */
package dk.itu.cs.isa.model.visualization;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Abstract Visual Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISAAbstractVisualRelation#getWeight <em>Weight</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAAbstractVisualRelation()
 * @model abstract="true"
 * @generated
 */
public interface ISAAbstractVisualRelation extends EObject {
	/**
	 * Returns the value of the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Weight</em>' attribute.
	 * @see #setWeight(float)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAAbstractVisualRelation_Weight()
	 * @model required="true"
	 * @generated
	 */
	float getWeight();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISAAbstractVisualRelation#getWeight <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Weight</em>' attribute.
	 * @see #getWeight()
	 * @generated
	 */
	void setWeight(float value);

} // ISAAbstractVisualRelation

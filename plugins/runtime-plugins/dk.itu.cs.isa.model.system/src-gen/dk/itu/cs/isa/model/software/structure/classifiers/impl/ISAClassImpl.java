/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassImpl#getConstructors <em>Constructors</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassImpl#getExtendedClass <em>Extended Class</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassImpl#getImplementedInterfaces <em>Implemented Interfaces</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassImpl#getUsagesAsExtendedClass <em>Usages As Extended Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ISAClassImpl extends ISAClassifierImpl implements ISAClass {
	/**
	 * The cached value of the '{@link #getConstructors() <em>Constructors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstructors()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAConstructor> constructors;

	/**
	 * The cached value of the '{@link #getExtendedClass() <em>Extended Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendedClass()
	 * @generated
	 * @ordered
	 */
	protected ISAClass extendedClass;

	/**
	 * The cached value of the '{@link #getImplementedInterfaces() <em>Implemented Interfaces</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementedInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAInterface> implementedInterfaces;

	/**
	 * The cached value of the '{@link #getUsagesAsExtendedClass() <em>Usages As Extended Class</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsagesAsExtendedClass()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAClass> usagesAsExtendedClass;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.ISA_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAClass getExtendedClass() {
		if (extendedClass != null && extendedClass.eIsProxy()) {
			InternalEObject oldExtendedClass = (InternalEObject)extendedClass;
			extendedClass = (ISAClass)eResolveProxy(oldExtendedClass);
			if (extendedClass != oldExtendedClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS, oldExtendedClass, extendedClass));
			}
		}
		return extendedClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAClass basicGetExtendedClass() {
		return extendedClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExtendedClass(ISAClass newExtendedClass, NotificationChain msgs) {
		ISAClass oldExtendedClass = extendedClass;
		extendedClass = newExtendedClass;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS, oldExtendedClass, newExtendedClass);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExtendedClass(ISAClass newExtendedClass) {
		if (newExtendedClass != extendedClass) {
			NotificationChain msgs = null;
			if (extendedClass != null)
				msgs = ((InternalEObject)extendedClass).eInverseRemove(this, ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS, ISAClass.class, msgs);
			if (newExtendedClass != null)
				msgs = ((InternalEObject)newExtendedClass).eInverseAdd(this, ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS, ISAClass.class, msgs);
			msgs = basicSetExtendedClass(newExtendedClass, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS, newExtendedClass, newExtendedClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAInterface> getImplementedInterfaces() {
		if (implementedInterfaces == null) {
			implementedInterfaces = new EObjectWithInverseResolvingEList.ManyInverse<ISAInterface>(ISAInterface.class, this, ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES, ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE);
		}
		return implementedInterfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAConstructor> getConstructors() {
		if (constructors == null) {
			constructors = new EObjectContainmentEList<ISAConstructor>(ISAConstructor.class, this, ClassifiersPackage.ISA_CLASS__CONSTRUCTORS);
		}
		return constructors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAClass> getUsagesAsExtendedClass() {
		if (usagesAsExtendedClass == null) {
			usagesAsExtendedClass = new EObjectWithInverseResolvingEList<ISAClass>(ISAClass.class, this, ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS, ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS);
		}
		return usagesAsExtendedClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS:
				if (extendedClass != null)
					msgs = ((InternalEObject)extendedClass).eInverseRemove(this, ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS, ISAClass.class, msgs);
				return basicSetExtendedClass((ISAClass)otherEnd, msgs);
			case ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getImplementedInterfaces()).basicAdd(otherEnd, msgs);
			case ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getUsagesAsExtendedClass()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASS__CONSTRUCTORS:
				return ((InternalEList<?>)getConstructors()).basicRemove(otherEnd, msgs);
			case ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS:
				return basicSetExtendedClass(null, msgs);
			case ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES:
				return ((InternalEList<?>)getImplementedInterfaces()).basicRemove(otherEnd, msgs);
			case ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS:
				return ((InternalEList<?>)getUsagesAsExtendedClass()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASS__CONSTRUCTORS:
				return getConstructors();
			case ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS:
				if (resolve) return getExtendedClass();
				return basicGetExtendedClass();
			case ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES:
				return getImplementedInterfaces();
			case ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS:
				return getUsagesAsExtendedClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASS__CONSTRUCTORS:
				getConstructors().clear();
				getConstructors().addAll((Collection<? extends ISAConstructor>)newValue);
				return;
			case ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS:
				setExtendedClass((ISAClass)newValue);
				return;
			case ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES:
				getImplementedInterfaces().clear();
				getImplementedInterfaces().addAll((Collection<? extends ISAInterface>)newValue);
				return;
			case ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS:
				getUsagesAsExtendedClass().clear();
				getUsagesAsExtendedClass().addAll((Collection<? extends ISAClass>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASS__CONSTRUCTORS:
				getConstructors().clear();
				return;
			case ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS:
				setExtendedClass((ISAClass)null);
				return;
			case ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES:
				getImplementedInterfaces().clear();
				return;
			case ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS:
				getUsagesAsExtendedClass().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASS__CONSTRUCTORS:
				return constructors != null && !constructors.isEmpty();
			case ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS:
				return extendedClass != null;
			case ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES:
				return implementedInterfaces != null && !implementedInterfaces.isEmpty();
			case ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS:
				return usagesAsExtendedClass != null && !usagesAsExtendedClass.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAConstructorContainer.class) {
			switch (derivedFeatureID) {
				case ClassifiersPackage.ISA_CLASS__CONSTRUCTORS: return MembersPackage.ISA_CONSTRUCTOR_CONTAINER__CONSTRUCTORS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAConstructorContainer.class) {
			switch (baseFeatureID) {
				case MembersPackage.ISA_CONSTRUCTOR_CONTAINER__CONSTRUCTORS: return ClassifiersPackage.ISA_CLASS__CONSTRUCTORS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ISAClassImpl

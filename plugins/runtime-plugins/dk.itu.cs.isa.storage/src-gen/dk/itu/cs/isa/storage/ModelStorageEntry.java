/**
 */
package dk.itu.cs.isa.storage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Storage Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.storage.ModelStorageEntry#getSystemName <em>System Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.ModelStorageEntry#getNumberOfClassifiers <em>Number Of Classifiers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.ModelStorageEntry#getStructureModelFileName <em>Structure Model File Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.ModelStorageEntry#getQualityModelFileName <em>Quality Model File Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.ModelStorageEntry#getVisualizationModelFileName <em>Visualization Model File Name</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.storage.StoragePackage#getModelStorageEntry()
 * @model
 * @generated
 */
public interface ModelStorageEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Name</em>' attribute.
	 * @see #setSystemName(String)
	 * @see dk.itu.cs.isa.storage.StoragePackage#getModelStorageEntry_SystemName()
	 * @model required="true"
	 * @generated
	 */
	String getSystemName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getSystemName <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Name</em>' attribute.
	 * @see #getSystemName()
	 * @generated
	 */
	void setSystemName(String value);

	/**
	 * Returns the value of the '<em><b>Number Of Classifiers</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Classifiers</em>' attribute.
	 * @see #setNumberOfClassifiers(int)
	 * @see dk.itu.cs.isa.storage.StoragePackage#getModelStorageEntry_NumberOfClassifiers()
	 * @model required="true"
	 * @generated
	 */
	int getNumberOfClassifiers();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getNumberOfClassifiers <em>Number Of Classifiers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Classifiers</em>' attribute.
	 * @see #getNumberOfClassifiers()
	 * @generated
	 */
	void setNumberOfClassifiers(int value);

	/**
	 * Returns the value of the '<em><b>Structure Model File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Structure Model File Name</em>' attribute.
	 * @see #setStructureModelFileName(String)
	 * @see dk.itu.cs.isa.storage.StoragePackage#getModelStorageEntry_StructureModelFileName()
	 * @model
	 * @generated
	 */
	String getStructureModelFileName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getStructureModelFileName <em>Structure Model File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Structure Model File Name</em>' attribute.
	 * @see #getStructureModelFileName()
	 * @generated
	 */
	void setStructureModelFileName(String value);

	/**
	 * Returns the value of the '<em><b>Quality Model File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quality Model File Name</em>' attribute.
	 * @see #setQualityModelFileName(String)
	 * @see dk.itu.cs.isa.storage.StoragePackage#getModelStorageEntry_QualityModelFileName()
	 * @model
	 * @generated
	 */
	String getQualityModelFileName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getQualityModelFileName <em>Quality Model File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quality Model File Name</em>' attribute.
	 * @see #getQualityModelFileName()
	 * @generated
	 */
	void setQualityModelFileName(String value);

	/**
	 * Returns the value of the '<em><b>Visualization Model File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visualization Model File Name</em>' attribute.
	 * @see #setVisualizationModelFileName(String)
	 * @see dk.itu.cs.isa.storage.StoragePackage#getModelStorageEntry_VisualizationModelFileName()
	 * @model
	 * @generated
	 */
	String getVisualizationModelFileName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getVisualizationModelFileName <em>Visualization Model File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visualization Model File Name</em>' attribute.
	 * @see #getVisualizationModelFileName()
	 * @generated
	 */
	void setVisualizationModelFileName(String value);

} // ModelStorageEntry

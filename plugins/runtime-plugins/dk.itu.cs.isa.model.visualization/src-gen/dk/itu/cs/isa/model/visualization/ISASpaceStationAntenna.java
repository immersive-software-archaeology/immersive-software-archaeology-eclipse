/**
 */
package dk.itu.cs.isa.model.visualization;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Space Station Antenna</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isPublic <em>Public</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isEnumConstant <em>Enum Constant</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isRecordComponent <em>Record Component</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationAntenna()
 * @model
 * @generated
 */
public interface ISASpaceStationAntenna extends ISAAbstractSpaceStationComponent {
	/**
	 * Returns the value of the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Public</em>' attribute.
	 * @see #setPublic(boolean)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationAntenna_Public()
	 * @model required="true"
	 * @generated
	 */
	boolean isPublic();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isPublic <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Public</em>' attribute.
	 * @see #isPublic()
	 * @generated
	 */
	void setPublic(boolean value);

	/**
	 * Returns the value of the '<em><b>Enum Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum Constant</em>' attribute.
	 * @see #setEnumConstant(boolean)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationAntenna_EnumConstant()
	 * @model required="true"
	 * @generated
	 */
	boolean isEnumConstant();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isEnumConstant <em>Enum Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enum Constant</em>' attribute.
	 * @see #isEnumConstant()
	 * @generated
	 */
	void setEnumConstant(boolean value);

	/**
	 * Returns the value of the '<em><b>Record Component</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Component</em>' attribute.
	 * @see #setRecordComponent(boolean)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationAntenna_RecordComponent()
	 * @model required="true"
	 * @generated
	 */
	boolean isRecordComponent();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isRecordComponent <em>Record Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Component</em>' attribute.
	 * @see #isRecordComponent()
	 * @generated
	 */
	void setRecordComponent(boolean value);

} // ISASpaceStationAntenna

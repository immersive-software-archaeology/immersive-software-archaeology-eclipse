/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem.util;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer;

import dk.itu.cs.isa.model.software.structure.filesystem.*;

import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage
 * @generated
 */
public class FilesystemAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FilesystemPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FilesystemAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FilesystemPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FilesystemSwitch<Adapter> modelSwitch =
		new FilesystemSwitch<Adapter>() {
			@Override
			public Adapter caseISAProject(ISAProject object) {
				return createISAProjectAdapter();
			}
			@Override
			public Adapter caseISAPackageContainer(ISAPackageContainer object) {
				return createISAPackageContainerAdapter();
			}
			@Override
			public Adapter caseISAPackageRoot(ISAPackageRoot object) {
				return createISAPackageRootAdapter();
			}
			@Override
			public Adapter caseISAPackage(ISAPackage object) {
				return createISAPackageAdapter();
			}
			@Override
			public Adapter caseISANamedSoftwareElement(ISANamedSoftwareElement object) {
				return createISANamedSoftwareElementAdapter();
			}
			@Override
			public Adapter caseISAClassifierContainer(ISAClassifierContainer object) {
				return createISAClassifierContainerAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject <em>ISA Project</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAProject
	 * @generated
	 */
	public Adapter createISAProjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer <em>ISA Package Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer
	 * @generated
	 */
	public Adapter createISAPackageContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot <em>ISA Package Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot
	 * @generated
	 */
	public Adapter createISAPackageRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage <em>ISA Package</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage
	 * @generated
	 */
	public Adapter createISAPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement <em>ISA Named Software Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement
	 * @generated
	 */
	public Adapter createISANamedSoftwareElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer <em>ISA Classifier Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer
	 * @generated
	 */
	public Adapter createISAClassifierContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FilesystemAdapterFactory

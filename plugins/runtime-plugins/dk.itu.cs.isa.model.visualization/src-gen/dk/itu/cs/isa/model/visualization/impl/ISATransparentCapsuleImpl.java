/**
 */
package dk.itu.cs.isa.model.visualization.impl;

import dk.itu.cs.isa.model.visualization.ISASpaceStation;
import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Transparent Capsule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISATransparentCapsuleImpl#getSubCapsules <em>Sub Capsules</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISATransparentCapsuleImpl#getSpaceStations <em>Space Stations</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISATransparentCapsuleImpl#getTags <em>Tags</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISATransparentCapsuleImpl extends ISANamedVisualElementImpl implements ISATransparentCapsule {
	/**
	 * The cached value of the '{@link #getSubCapsules() <em>Sub Capsules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubCapsules()
	 * @generated
	 * @ordered
	 */
	protected EList<ISATransparentCapsule> subCapsules;

	/**
	 * The cached value of the '{@link #getSpaceStations() <em>Space Stations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpaceStations()
	 * @generated
	 * @ordered
	 */
	protected EList<ISASpaceStation> spaceStations;

	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tags;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISATransparentCapsuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualizationPackage.Literals.ISA_TRANSPARENT_CAPSULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISATransparentCapsule> getSubCapsules() {
		if (subCapsules == null) {
			subCapsules = new EObjectContainmentEList<ISATransparentCapsule>(ISATransparentCapsule.class, this, VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SUB_CAPSULES);
		}
		return subCapsules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISASpaceStation> getSpaceStations() {
		if (spaceStations == null) {
			spaceStations = new EObjectContainmentEList<ISASpaceStation>(ISASpaceStation.class, this, VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SPACE_STATIONS);
		}
		return spaceStations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getTags() {
		if (tags == null) {
			tags = new EDataTypeUniqueEList<String>(String.class, this, VisualizationPackage.ISA_TRANSPARENT_CAPSULE__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SUB_CAPSULES:
				return ((InternalEList<?>)getSubCapsules()).basicRemove(otherEnd, msgs);
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SPACE_STATIONS:
				return ((InternalEList<?>)getSpaceStations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SUB_CAPSULES:
				return getSubCapsules();
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SPACE_STATIONS:
				return getSpaceStations();
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__TAGS:
				return getTags();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SUB_CAPSULES:
				getSubCapsules().clear();
				getSubCapsules().addAll((Collection<? extends ISATransparentCapsule>)newValue);
				return;
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SPACE_STATIONS:
				getSpaceStations().clear();
				getSpaceStations().addAll((Collection<? extends ISASpaceStation>)newValue);
				return;
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SUB_CAPSULES:
				getSubCapsules().clear();
				return;
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SPACE_STATIONS:
				getSpaceStations().clear();
				return;
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__TAGS:
				getTags().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SUB_CAPSULES:
				return subCapsules != null && !subCapsules.isEmpty();
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__SPACE_STATIONS:
				return spaceStations != null && !spaceStations.isEmpty();
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE__TAGS:
				return tags != null && !tags.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (tags: ");
		result.append(tags);
		result.append(')');
		return result.toString();
	}

} //ISATransparentCapsuleImpl

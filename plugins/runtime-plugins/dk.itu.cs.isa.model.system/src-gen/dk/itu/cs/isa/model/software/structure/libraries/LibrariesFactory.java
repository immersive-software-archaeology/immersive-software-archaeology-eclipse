/**
 */
package dk.itu.cs.isa.model.software.structure.libraries;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage
 * @generated
 */
public interface LibrariesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LibrariesFactory eINSTANCE = dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ISA Library</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Library</em>'.
	 * @generated
	 */
	ISALibrary createISALibrary();

	/**
	 * Returns a new object of class '<em>ISA Primitive Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Primitive Type</em>'.
	 * @generated
	 */
	ISAPrimitiveType createISAPrimitiveType();

	/**
	 * Returns a new object of class '<em>ISA Library Classifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Library Classifier</em>'.
	 * @generated
	 */
	ISALibraryClassifier createISALibraryClassifier();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	LibrariesPackage getLibrariesPackage();

} //LibrariesFactory

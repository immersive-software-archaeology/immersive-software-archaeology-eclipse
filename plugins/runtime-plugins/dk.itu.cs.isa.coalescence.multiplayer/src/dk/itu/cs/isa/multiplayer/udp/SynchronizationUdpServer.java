package dk.itu.cs.isa.multiplayer.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.itu.cs.isa.coalescence.server.extensions.ISAAbstractSocketImplementation;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.multiplayer.AbstractEvent;
import dk.itu.cs.isa.multiplayer.EventLog;
import dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoFactory;
import dk.itu.cs.isa.multiplayer.clientinfo.HandInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.HandPose;
import dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3;
import dk.itu.cs.isa.multiplayer.events.EventLogManager;
import dk.itu.cs.isa.util.TimeUtil;
import dk.itu.cs.isa.whiteboard.Whiteboard;
import dk.itu.cs.isa.whiteboard.WhiteboardFactory;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardHistoryUtil;
import dk.itu.cs.isa.whiteboard.WhiteboardStorage;

public class SynchronizationUdpServer implements ISAAbstractSocketImplementation {
	
	private static enum PacketType {
		FAILURE((byte) 0),
		OUTDATED((byte) 1),
		CONNECT((byte) 8),
		DISCONNECT((byte) 16),
		TRANSFORM_UPDATE((byte) 32);
		
		private byte value;
		private PacketType(byte value) {
			this.value = value;
		}
	}
	
	public static final int packetLength = 512;
	public static final int maxClients = 4;
	public static final int connectionTimeout = 5000;

	private DatagramSocket socket;
	private boolean running;

	private byte nextPlayerID = 1;
	private final Map<String, List<ClientInfo>> systemToClientsMap = new HashMap<>();
	
	private final List<WhiteboardHistory> dirtyHistories = new ArrayList<>();

	private final Map<String, EventLogManager> eventLogManagers = new HashMap<>();
	

	
	@Override
	public void init(int port) throws SocketException {
		socket = new DatagramSocket(port);
		ISAEventSystem.fireStandardConsoleMessage("Initialized socket \"" + getName() + "\" on port " + port);
	}
	
	@Override
	public int getPort() {
		return socket.getLocalPort();
	}
	
	@Override
	public String getName() {
		return "Synchronization Socket";
	}
	
	@Override
	public void stop() {
		systemToClientsMap.clear();
		eventLogManagers.clear();
		running = false;
		if(socket != null)
			socket.close();
	}
	

	
	private EventLog getLog(String systemName) {
		EventLogManager manager = eventLogManagers.get(systemName);
		if(manager == null) {
			manager = new EventLogManager();
			eventLogManagers.put(systemName, manager);
		}
		return manager.getLog();
	}

	public synchronized void pushOperations(String systemName, List<AbstractEvent> submittedOperations) {
		EventLogManager manager = eventLogManagers.get(systemName);
		manager.addOperationsAndCleanUp(submittedOperations);
	}
	
//	private void removeAllEventsOnElement(String systemName, String targetElementName) {
//		for(int i=0; i<log.getLog().size(); i++) {
//			AbstractEvent event = log.getLog().get(i);
//			String thisTargetElementName = getTargetElementName(event);
//			if(targetElementName.equals(thisTargetElementName))
//				if(systemName.equals(event.getSystemName()))
//					log.getLog().remove(i--);
//		}
//	}
//	
//	private String getTargetElementName(AbstractEvent submittedOperation) {
//		if(submittedOperation instanceof HideRelationsEvent)
//			return ((HideRelationsEvent) submittedOperation).getElementName();
//		else if(submittedOperation instanceof ShowRelationsEvent)
//			return ((ShowRelationsEvent) submittedOperation).getElementName();
//		return null;
//	}

	public synchronized EventLog fetchOperations(String systemName, long lastUpdateStamp) {
		EventLog result = EcoreUtil.copy(getLog(systemName));
		for(int i=0; i<result.getLog().size(); i++)
			if(result.getLog().get(i).getTimestamp() <= lastUpdateStamp)
				result.getLog().remove(i--);
		return result;
	}
	
	private synchronized long getLatestEventTimestamp(String systemName) {
		EventLog log = getLog(systemName);
		for(int i=log.getLog().size()-1; i >= 0; i--) {
			AbstractEvent e = log.getLog().get(i);
			if(e != null)
				return e.getTimestamp();
		}
		return 0;
		
//		long result = 0;
//		for(int i=0; i<log.getLog().size(); i++) {
//			AbstractEvent e = log.getLog().get(i);
//			if(e == null) {
//				// this can happen when the housekeeping happens at the same time
//				i--;
//				continue;
//			}
//			if(e.getTimestamp() > result)
//				result = e.getTimestamp();
//		}
//		return result;
	}
	

	
	public int getNumberOfConnectedClients() {
		int number = 0;
		for(List<ClientInfo> clients : systemToClientsMap.values())
			number += clients.size();
		return number;
	}
	
	public int getNumberOfConnectedClients(String systemName) {
		List<ClientInfo> clients = systemToClientsMap.get(systemName);
		if(clients == null)
			return 0;
		return clients.size();
	}
	
	public byte getClientId(String systemName, int index) {
		List<ClientInfo> clients = systemToClientsMap.get(systemName);
		return clients.get(index).getId();
	}
	
	public byte getClientColor(String systemName, int index) {
		List<ClientInfo> clients = systemToClientsMap.get(systemName);
		return clients.get(index).getColor();
	}
	
	public String toString() {
		String result = "Connected Clients: " + getNumberOfConnectedClients() + System.lineSeparator();
		
		for(List<ClientInfo> clients : systemToClientsMap.values())
			for(ClientInfo client : clients)
				result += System.lineSeparator() + "#" + client.getId()
						+ " [" + client.getSystemName() + "] "
						+ "(" + getOnlineTimeAsString(client) + ", " + getFpsAsUnsignedInt(client.getFps()) + ", ping=" + client.getPing() + ")";
		return result;
	}
	
	private String getFpsAsUnsignedInt(byte fpsByte) {
		int fps = Byte.toUnsignedInt(fpsByte);
		if(fps > 250)
			return "fps>250";
		else
			return "fps=" + fps;
	}
	
	private String getOnlineTimeAsString(ClientInfo client) {
		long onlineTimeMillis = System.currentTimeMillis() - client.getConnectTimestamp();
		return TimeUtil.millisToMinuteString(onlineTimeMillis);
	}
	
	private byte getNextPlayerID() {
		byte id;
		while(true) {
			id = nextPlayerID;
			nextPlayerID = (byte) ( (nextPlayerID + 1) % 64 );
			if(nextPlayerID == 0)
				nextPlayerID++;
			
			boolean idValid = true;
			for(List<ClientInfo> clientsInSystem : systemToClientsMap.values())
				for(ClientInfo client : clientsInSystem)
					if(client.getId() == id)
						idValid = false;
						
			if(idValid)
				break;
		}
		return id;
	}

	private byte getAvailablePlayerColor() {
		byte colorHueByte = 0;

		while(true) {
			boolean valid = true;
			for(List<ClientInfo> clientsInSystem : systemToClientsMap.values())
				for(ClientInfo client : clientsInSystem)
					if(client.getColor() == colorHueByte)
						valid = false;
			if(valid)
				return colorHueByte;
			
			int colorHueInt = colorHueByte + Math.round(255f / (float)maxClients);
			if(colorHueByte > 255)
				return 0;
			
			colorHueByte = (byte) colorHueInt;
		}
	}

	private void onPlayerHoldsWhiteboard(String systemName, HandInfo holdingHand) {
		Whiteboard board = WhiteboardStorage.INSTANCE.getWhiteboardByID(systemName, holdingHand.getHeldObject().getId());
		if(board == null)
			return;
		
		if(board.getPosition() == null)
			board.setPosition(WhiteboardFactory.eINSTANCE.createISAVector3());
		if(board.getRotation() == null)
			board.setRotation(WhiteboardFactory.eINSTANCE.createISAVector3());
		
		board.getPosition().setX(holdingHand.getHeldObject().getPosition().getX());
		board.getPosition().setY(holdingHand.getHeldObject().getPosition().getY());
		board.getPosition().setZ(holdingHand.getHeldObject().getPosition().getZ());
		
		board.getRotation().setX(holdingHand.getHeldObject().getRotation().getX());
		board.getRotation().setY(holdingHand.getHeldObject().getRotation().getY());
		board.getRotation().setZ(holdingHand.getHeldObject().getRotation().getZ());
		
		dirtyHistories.add(board.getContainingHistory());
	}
	
	
	
	/*
	 * look at net-protocol.txt for information on the protocol
	 */
	@Override
	public void run() {
		systemToClientsMap.clear();
		running = true;
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
					try {
						if(!running)
							return;
						Thread.sleep(1000);
						performHouseKeeping();
						
						try {
							List<WhiteboardHistory> dirtyHistoriesCopy = new ArrayList<>(dirtyHistories);
							dirtyHistories.clear();
							
							for(WhiteboardHistory history : dirtyHistoriesCopy) {
								WhiteboardHistoryUtil.updateWhiteboardPositionEvents(history);
								WhiteboardStorage.INSTANCE.save(history, false);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
		}, "ISA UDP Synchronization Housekeeping").start();

		while (running) {
			DatagramPacket receivedPacket = new DatagramPacket(new byte[packetLength], packetLength);
			try {
				socket.receive(receivedPacket);
			} catch (IOException e) {
				if(!running)
					return;
				e.printStackTrace();
			}
			
			performHouseKeeping();
			
			byte[] receivedDatagram = receivedPacket.getData();
			DatagramReader reader = new DatagramReader(receivedDatagram);
			
			byte[] responseDatagram;
			
			byte messageType = reader.readByte();
			if(messageType == PacketType.CONNECT.value) {
				responseDatagram = handleConnectPackage(receivedPacket, reader);
			}
			else if(messageType == PacketType.DISCONNECT.value) {
				responseDatagram = handleDisconnectPackage(receivedPacket, reader);
			}
			else if(messageType == PacketType.TRANSFORM_UPDATE.value) {
				responseDatagram = handleTransformUpdatePackage(receivedPacket, reader);
			}
			else {
				responseDatagram = handleError(receivedPacket, PacketType.FAILURE, "Unknown kind of message");
			}
			
			try {
				InetAddress address = receivedPacket.getAddress();
				int port = receivedPacket.getPort();
				DatagramPacket packageToSend = new DatagramPacket(responseDatagram, responseDatagram.length, address, port);
				socket.send(packageToSend);
			}
			catch(IOException e) {
				System.err.println(e);
			}
		}
		socket.close();
	}
	
	private synchronized void performHouseKeeping() {
		for(List<ClientInfo> clients : systemToClientsMap.values()) {
			List<ClientInfo> toRemove = new ArrayList<>();
			
			for(ClientInfo client : clients) {
				if(isTimedOut(client))
					toRemove.add(client);
			}
			
			for(ClientInfo clientToRemove : toRemove) {			
				clients.remove(clientToRemove);
				ISAEventSystem.fireStandardConsoleMessage("Client with id=" + clientToRemove.getId() + " timed out...");
			}
		}
	}
	
	private boolean isTimedOut(ClientInfo client) {
		if(client.getLastServerTimestamp() > 0 && System.currentTimeMillis() - client.getLastServerTimestamp() > connectionTimeout)
			return true;
		if(client.getLastServerTimestamp() == 0 && System.currentTimeMillis() - client.getConnectTimestamp() > connectionTimeout)
			return true;
		return false;
	}
	
	

	private byte[] handleError(DatagramPacket receivedPacket, PacketType type, String message) {
		DatagramWriter writer = new DatagramWriter(packetLength);
		
		writer.write(type.value);
		writer.write((byte) message.length());
		writer.write(message);
		
		return writer.getDatagram();
	}
	
	

	private byte[] handleConnectPackage(DatagramPacket receivedPacket, DatagramReader reader) {
		byte systemNameLength = reader.readByte();
		String systemName = reader.readString(systemNameLength);
		if(systemName.trim().isBlank()) {
			ISAEventSystem.fireStandardConsoleMessage("A client did not specify a system name.");
			return handleError(receivedPacket, PacketType.FAILURE, "No system name specified");
		}
		
		List<ClientInfo> clients = systemToClientsMap.get(systemName);
		if(clients == null) {
			clients = new ArrayList<>();
			systemToClientsMap.put(systemName, clients);
		}
		
		if(clients.size() < maxClients) {
			// Perform server state changes
			byte id = getNextPlayerID();
			
			ClientInfo newPlayer = ClientinfoFactory.eINSTANCE.createClientInfo();
			newPlayer.setId(id);
			newPlayer.setColor(getAvailablePlayerColor());
			newPlayer.setSystemName(systemName);
			newPlayer.setConnectTimestamp(System.currentTimeMillis());
			{
				newPlayer.setHead(ClientinfoFactory.eINSTANCE.createHeadInfo());
				initializeBodyPart(newPlayer.getHead());
				
				newPlayer.setLeftHand(ClientinfoFactory.eINSTANCE.createHandInfo());
				newPlayer.getLeftHand().setIndexFingerTipPosition(ClientinfoFactory.eINSTANCE.createISAVector3());
				newPlayer.getLeftHand().setHeldObject(ClientinfoFactory.eINSTANCE.createHandHeldObjectInfo());
				initializeBodyPart(newPlayer.getLeftHand());
				initializeBodyPart(newPlayer.getLeftHand().getHeldObject());
				
				newPlayer.setRightHand(ClientinfoFactory.eINSTANCE.createHandInfo());
				newPlayer.getRightHand().setIndexFingerTipPosition(ClientinfoFactory.eINSTANCE.createISAVector3());
				newPlayer.getRightHand().setHeldObject(ClientinfoFactory.eINSTANCE.createHandHeldObjectInfo());
				initializeBodyPart(newPlayer.getRightHand());
				initializeBodyPart(newPlayer.getRightHand().getHeldObject());
			}
			clients.add(newPlayer);
			
			ISAEventSystem.fireStandardConsoleMessage("Client with id=" + id + " connected!");
			
			// Respond
			DatagramWriter writer = new DatagramWriter(packetLength);
			
			writer.write(PacketType.CONNECT.value);
			writer.write(newPlayer.getId());
			
			return writer.getDatagram();
		}
		else {
			ISAEventSystem.fireStandardConsoleMessage("Turning down a client because the server is full.");
			return handleError(receivedPacket, PacketType.FAILURE, "Server is full");
		}
	}
	
	private void initializeBodyPart(BodyPartInfo info) {
		info.setPosition(ClientinfoFactory.eINSTANCE.createISAVector3());
		info.setRotation(ClientinfoFactory.eINSTANCE.createISAVector3());
	}
	
	
	
	private byte[] handleDisconnectPackage(DatagramPacket receivedPacket, DatagramReader reader) {
		byte clientID = reader.readByte();
		
		for(List<ClientInfo> connectedClients : systemToClientsMap.values()) {
			for(ClientInfo client : connectedClients) {
				if(client.getId() == clientID) {			
					connectedClients.remove(client);
					
					// Respond
					DatagramWriter writer = new DatagramWriter(packetLength);
					writer.write(PacketType.DISCONNECT.value);
					return writer.getDatagram();
				}
			}
		}
		return handleError(receivedPacket, PacketType.FAILURE, "Disconnect: No client with id=" + clientID + " found on the server");
	}
	
	
	
	private byte[] handleTransformUpdatePackage(DatagramPacket receivedPacket, DatagramReader reader) {
		byte updatingClientID = reader.readByte();
		ClientInfo updatingClient = null;
		String systemName = null;
		Collection<ClientInfo> allConnectedClients = null;
		
		for(String currentSystemName : systemToClientsMap.keySet()) {
			List<ClientInfo> clientsOfThisSystem = systemToClientsMap.get(currentSystemName);
			for(ClientInfo client : clientsOfThisSystem) {
				if(client.getId() == updatingClientID) {
					systemName = currentSystemName;
					updatingClient = client;
					allConnectedClients = clientsOfThisSystem;
				}
			}
		}
		
		if(updatingClient == null)
			return handleError(receivedPacket, PacketType.FAILURE, "Transform Update: No client with id=" + updatingClientID + " found on the server");

		long clientSideTimeStamp = reader.readLong();
		long lastTimeStamp = updatingClient.getLastClientTimestamp();
		
		byte packageNumber = reader.readByte();
		
		if(clientSideTimeStamp < lastTimeStamp) {
			// ignore, this packet arrived delayed, we do not care anymore
			return handleError(receivedPacket, PacketType.OUTDATED, "Packet outdated");
		}
		updatingClient.setPing((int)(clientSideTimeStamp - updatingClient.getLastClientTimestamp() - 1000/50));

		updatingClient.setLastClientTimestamp(clientSideTimeStamp);
		updatingClient.setLastServerTimestamp(System.currentTimeMillis());
		
		byte fps = reader.readByte();
		updatingClient.setFps(fps);
		
		// Read and update the player' position
		{
			// head
			setVector(updatingClient.getHead().getPosition(), reader.readFloatArray(3));
			setVector(updatingClient.getHead().getRotation(), reader.readFloatArray(3));

			// update hands with held objects
			readClientHandData(systemName, updatingClient.getLeftHand(), reader);
			readClientHandData(systemName, updatingClient.getRightHand(), reader);
		}
		
		// Return the position of all other clients
		{
			DatagramWriter writer = new DatagramWriter(packetLength);
			
			writer.write(PacketType.TRANSFORM_UPDATE.value);
			writer.write(System.currentTimeMillis());
			writer.write(packageNumber);
			
			writer.write(getLatestEventTimestamp(systemName));
			writer.write(WhiteboardStorage.INSTANCE.getLatestOperationTimestamp(systemName));
			
			writer.write((byte) (allConnectedClients.size()-1));

			for(ClientInfo otherClient : allConnectedClients) {
				if(otherClient.getId() == updatingClient.getId())
					continue;
				
				writer.write(otherClient.getId());

				// head
				writer.write(otherClient.getHead().getPosition());
				writer.write(otherClient.getHead().getRotation());

//				System.out.println(otherClient.getId() + ": " + otherClient.getRightHand().getPosition());
				writeClientHandData(otherClient.getLeftHand(), writer);
				writeClientHandData(otherClient.getRightHand(), writer);
			}
			
//			System.out.println(writer);
			
			return writer.getDatagram();
		}
	}

	private void readClientHandData(String systemName, HandInfo hand, DatagramReader reader) {
		hand.setPose(HandPose.get(reader.readByte()));

		setVector(hand.getPosition(), reader.readFloatArray(3));
		setVector(hand.getRotation(), reader.readFloatArray(3));
		
		setVector(hand.getIndexFingerTipPosition(), reader.readFloatArray(3));

		// held object
		{
			hand.getHeldObject().setId(reader.readInt());
	
			setVector(hand.getHeldObject().getPosition(), reader.readFloatArray(3));
			setVector(hand.getHeldObject().getRotation(), reader.readFloatArray(3));
		}
		
		// check if the player holds a whiteboard
		if(hand.getPose() == HandPose.WHITEBOARD)
			onPlayerHoldsWhiteboard(systemName, hand);
	}

	private void writeClientHandData(HandInfo hand, DatagramWriter writer) {
		writer.write(hand.getPose());

		writer.write(hand.getPosition());
		writer.write(hand.getRotation());
		
		writer.write(hand.getIndexFingerTipPosition());
		
		// held object
		{
			writer.write(hand.getHeldObject().getId());

			writer.write(hand.getHeldObject().getPosition());
			writer.write(hand.getHeldObject().getRotation());
		}
	}

	private void setVector(ISAVector3 isaVector, float[] floatArray) {
		if(floatArray.length != 3)
			throw new IllegalArgumentException("Tried to update a 3D vector with an array of length " + floatArray.length + " (length of 3 is expected).");
		isaVector.setX(floatArray[0]);
		isaVector.setY(floatArray[1]);
		isaVector.setZ(floatArray[2]);
	}
	
	
	
}
































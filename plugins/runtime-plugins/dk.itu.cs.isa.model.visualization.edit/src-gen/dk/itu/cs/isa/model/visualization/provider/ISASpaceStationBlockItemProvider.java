/**
 */
package dk.itu.cs.isa.model.visualization.provider;


import dk.itu.cs.isa.model.visualization.ISASpaceStationBlock;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ISASpaceStationBlockItemProvider extends ISAAbstractSpaceStationComponentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASpaceStationBlockItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addHeightPropertyDescriptor(object);
			addDiameterPropertyDescriptor(object);
			addConstructorPropertyDescriptor(object);
			addAbstractPropertyDescriptor(object);
			addPublicPropertyDescriptor(object);
			addSystemEntryPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Height feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHeightPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISASpaceStationBlock_height_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISASpaceStationBlock_height_feature", "_UI_ISASpaceStationBlock_type"),
				 VisualizationPackage.Literals.ISA_SPACE_STATION_BLOCK__HEIGHT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Diameter feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDiameterPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISASpaceStationBlock_diameter_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISASpaceStationBlock_diameter_feature", "_UI_ISASpaceStationBlock_type"),
				 VisualizationPackage.Literals.ISA_SPACE_STATION_BLOCK__DIAMETER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Constructor feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstructorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISASpaceStationBlock_constructor_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISASpaceStationBlock_constructor_feature", "_UI_ISASpaceStationBlock_type"),
				 VisualizationPackage.Literals.ISA_SPACE_STATION_BLOCK__CONSTRUCTOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Abstract feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAbstractPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISASpaceStationBlock_abstract_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISASpaceStationBlock_abstract_feature", "_UI_ISASpaceStationBlock_type"),
				 VisualizationPackage.Literals.ISA_SPACE_STATION_BLOCK__ABSTRACT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Public feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPublicPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISASpaceStationBlock_public_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISASpaceStationBlock_public_feature", "_UI_ISASpaceStationBlock_type"),
				 VisualizationPackage.Literals.ISA_SPACE_STATION_BLOCK__PUBLIC,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the System Entry feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSystemEntryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISASpaceStationBlock_systemEntry_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISASpaceStationBlock_systemEntry_feature", "_UI_ISASpaceStationBlock_type"),
				 VisualizationPackage.Literals.ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns ISASpaceStationBlock.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ISASpaceStationBlock"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ISASpaceStationBlock)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ISASpaceStationBlock_type") :
			getString("_UI_ISASpaceStationBlock_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ISASpaceStationBlock.class)) {
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__HEIGHT:
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__DIAMETER:
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__CONSTRUCTOR:
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__ABSTRACT:
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__PUBLIC:
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == VisualizationPackage.Literals.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE ||
			childFeature == VisualizationPackage.Literals.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}

package dk.itu.cs.isa.coalescence.server.extensions;

import java.net.SocketException;

public interface ISAAbstractSocketImplementation extends Runnable {
	
	public void init(int port) throws SocketException;
	
	public void stop();
	
	public int getPort();
	
	public String getName();

}

/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;
import dk.itu.cs.isa.model.software.structure.classifiers.ISARecord;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;

public class ISAProjectImplCustom extends ISAProjectImpl {
	
	@Override
	public ISAPackage getPackage(String packageName) {
		String[] nameSegments = packageName.split("\\.");
		if(nameSegments.length == 0)
			return null;
		
		rootLoop: for(ISAPackageRoot root : this.getPackageRoots()) {
			ISAPackageContainer currentContainer = root;
			
			nameLoop: for(String segment : nameSegments) {
				for(ISAPackage p : currentContainer.getSubPackages()) {
					if(p.getName().equals(segment)) {
						currentContainer = p;
						continue nameLoop;
					}
				}
				continue rootLoop;
			}
			
			return (ISAPackage) currentContainer;
		}
		
		return null;
	}
	

	@Override
	public ISANamedSoftwareElement getElement(String qualifiedElementName) {
		if(qualifiedElementName.contains(";")) {
			// Find a member
			String qualifiedClassOrPackageName = qualifiedElementName.split(";")[0];
			ISANamedSoftwareElement containingClassifier = findClassOrPackage(qualifiedClassOrPackageName);
			if(containingClassifier == null || !(containingClassifier instanceof ISAClassifier))
				return null;
			
			String memberSignature = qualifiedElementName.split(";")[1];
			return findMemberInClassifier(memberSignature, (ISAClassifier) containingClassifier);
		}
		else {
			// Find a classifier
			return findClassOrPackage(qualifiedElementName);
		}
	}
	
	private ISANamedSoftwareElement findClassOrPackage(String qualifiedName) {
		if(qualifiedName.startsWith("project:/")) {
			// A package root was specified explicitly
			String[] split = qualifiedName.split("/");
			qualifiedName = split[split.length-1];
			
			if(split.length == 2)
				return this;
			
			String packageRootName = split[2];
			for(int i=3; i<split.length-1; i++)
				packageRootName += "/" + split[i];
			
			// find the package root and search in it
			for(ISAPackageRoot root : getPackageRoots())
				if(root.getName().equals(packageRootName))
					return searchForElementInPackageRoot(root, qualifiedName);
			return null;
		}
		
		for(ISAPackageRoot root : this.getPackageRoots()) {
			ISANamedSoftwareElement foundElement = searchForElementInPackageRoot(root, qualifiedName);
			if(foundElement != null)
				return foundElement;
		}
		return null;
	}
	
	private ISANamedSoftwareElement searchForElementInPackageRoot(ISAPackageRoot root, String qualifiedName) {
		List<String> nameSegments = Arrays.asList(qualifiedName.split("\\."));
		if(nameSegments.isEmpty())
			return null;
		
		ISANamedSoftwareElement currentElement = root;
		nameSegmentLoop:
		for(int i=0; i<nameSegments.size(); i++) {
			String nameSegment = nameSegments.get(i);
			// Go through each child and see if its name fits the current segment
			for(EObject child : currentElement.eContents()) {
				if(child instanceof ISANamedSoftwareElement) {
					ISANamedSoftwareElement namedChild = (ISANamedSoftwareElement) child;
//					System.out.println(namedChild.getName());
					if(namedChild.getName().equals(nameSegment)) {
						if(i == nameSegments.size()-1)
							return namedChild;
						
						currentElement = namedChild;
						continue nameSegmentLoop;
					}
				}
			}
			
			// No matching child found
			return null;
		}
		return null;
	}
	
	
	
	private ISAMember findMemberInClassifier(String memberSignature, ISAClassifier containingClassifier) {
		if(memberSignature.contains("(")) {
			// Search for a method or constructor
			
			String name = memberSignature.substring(0, memberSignature.indexOf("("));
			String parameters = memberSignature.substring(memberSignature.indexOf("(")+1, memberSignature.length()-1);
			List<String> parameterTypes;
			if(parameters.isBlank())
				parameterTypes = new ArrayList<>(0);
			else
				parameterTypes = Arrays.asList(parameters.split(","));

			List<ISAParametrizableElement> methodsAndConstructors = new ArrayList<>();
			methodsAndConstructors.addAll(containingClassifier.getMethods());
			if(containingClassifier instanceof ISAClass)
				methodsAndConstructors.addAll(((ISAClass) containingClassifier).getConstructors());
			else if(containingClassifier instanceof ISAEnum)
				methodsAndConstructors.addAll(((ISAEnum) containingClassifier).getConstructors());
			
			methodLoop:
			for(ISAParametrizableElement methodOrConstructor : methodsAndConstructors) {
				if(((ISANamedSoftwareElement) methodOrConstructor).getName().equals(name)) {
					List<ISAParameter> modelParams = methodOrConstructor.getParameters();
					if(modelParams.size() != parameterTypes.size())
						continue;
					for(int i=0; i<modelParams.size(); i++) {
						ISAType type = modelParams.get(i).getType();
						
//						boolean typesMatch = false;
//						if(type.getFullyQualifiedName().startsWith("java.lang") && type.getFullyQualifiedName().endsWith(parameterTypes.get(i)))
//							typesMatch = true;
//						if(type.getFullyQualifiedName().equals(parameterTypes.get(i)))
//							typesMatch = true;
						
						// This check might be a little too inclusive, but otherwise checks will sometimes fail
						// TODO revisit this!
						if(!type.getFullyQualifiedName().endsWith(parameterTypes.get(i)))
							continue methodLoop;
					}
					
					return (ISAMember) methodOrConstructor;
				}
			}
		}
		else {
			// Search for a field, constant, or record component
			
			for(ISAField field : containingClassifier.getFields()) {
				if(field.getName().equals(memberSignature)) {
					return field;
				}
			}
			
			if(containingClassifier instanceof ISAEnum) {
				for(ISAEnumConstant constant : ((ISAEnum) containingClassifier).getConstants()) {
					if(constant.getName().equals(memberSignature)) {
						return constant;
					}
				}
			}
			
			if(containingClassifier instanceof ISARecord) {
				for(ISARecordComponent component : ((ISARecord) containingClassifier).getComponents()) {
					if(component.getName().equals(memberSignature)) {
						return component;
					}
				}
			}
		}
		
		return null;
	}
	
}











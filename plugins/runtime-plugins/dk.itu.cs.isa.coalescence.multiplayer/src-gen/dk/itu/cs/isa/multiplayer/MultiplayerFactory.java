/**
 */
package dk.itu.cs.isa.multiplayer;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage
 * @generated
 */
public interface MultiplayerFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MultiplayerFactory eINSTANCE = dk.itu.cs.isa.multiplayer.impl.MultiplayerFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Event Log</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Log</em>'.
	 * @generated
	 */
	EventLog createEventLog();

	/**
	 * Returns a new object of class '<em>Show Relation Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Show Relation Event</em>'.
	 * @generated
	 */
	ShowRelationEvent createShowRelationEvent();

	/**
	 * Returns a new object of class '<em>Clear One Relation Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Clear One Relation Event</em>'.
	 * @generated
	 */
	ClearOneRelationEvent createClearOneRelationEvent();

	/**
	 * Returns a new object of class '<em>Clear Relations Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Clear Relations Event</em>'.
	 * @generated
	 */
	ClearRelationsEvent createClearRelationsEvent();

	/**
	 * Returns a new object of class '<em>Clear All Relations Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Clear All Relations Event</em>'.
	 * @generated
	 */
	ClearAllRelationsEvent createClearAllRelationsEvent();

	/**
	 * Returns a new object of class '<em>Change Element Active State Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Element Active State Event</em>'.
	 * @generated
	 */
	ChangeElementActiveStateEvent createChangeElementActiveStateEvent();

	/**
	 * Returns a new object of class '<em>System Move Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System Move Event</em>'.
	 * @generated
	 */
	SystemMoveEvent createSystemMoveEvent();

	/**
	 * Returns a new object of class '<em>Change Nameplate Visibility Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Nameplate Visibility Event</em>'.
	 * @generated
	 */
	ChangeNameplateVisibilityEvent createChangeNameplateVisibilityEvent();

	/**
	 * Returns a new object of class '<em>Sphere Open Close Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sphere Open Close Event</em>'.
	 * @generated
	 */
	SphereOpenCloseEvent createSphereOpenCloseEvent();

	/**
	 * Returns a new object of class '<em>Audio Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Audio Event</em>'.
	 * @generated
	 */
	AudioEvent createAudioEvent();

	/**
	 * Returns a new object of class '<em>ISA Vector3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Vector3</em>'.
	 * @generated
	 */
	ISAVector3 createISAVector3();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MultiplayerPackage getMultiplayerPackage();

} //MultiplayerFactory

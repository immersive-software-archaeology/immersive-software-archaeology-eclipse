package dk.itu.cs.isa.analyzer.interfaces;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;

public interface ISystemModelCreator extends ISAModelHandler {

	public ISASoftwareSystemModel run(String name, List<IProject> selectedProjects, IProgressMonitor monitor) throws Exception;
	
}

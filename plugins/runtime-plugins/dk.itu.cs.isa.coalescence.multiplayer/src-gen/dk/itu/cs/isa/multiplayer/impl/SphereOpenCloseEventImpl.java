/**
 */
package dk.itu.cs.isa.multiplayer.impl;

import dk.itu.cs.isa.multiplayer.MultiplayerPackage;
import dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sphere Open Close Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.SphereOpenCloseEventImpl#getElementName <em>Element Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.SphereOpenCloseEventImpl#isNowOpen <em>Now Open</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SphereOpenCloseEventImpl extends AbstractEventImpl implements SphereOpenCloseEvent {
	/**
	 * The default value of the '{@link #getElementName() <em>Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementName()
	 * @generated
	 * @ordered
	 */
	protected static final String ELEMENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getElementName() <em>Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementName()
	 * @generated
	 * @ordered
	 */
	protected String elementName = ELEMENT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isNowOpen() <em>Now Open</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNowOpen()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NOW_OPEN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNowOpen() <em>Now Open</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNowOpen()
	 * @generated
	 * @ordered
	 */
	protected boolean nowOpen = NOW_OPEN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SphereOpenCloseEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MultiplayerPackage.Literals.SPHERE_OPEN_CLOSE_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getElementName() {
		return elementName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementName(String newElementName) {
		String oldElementName = elementName;
		elementName = newElementName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__ELEMENT_NAME, oldElementName, elementName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNowOpen() {
		return nowOpen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNowOpen(boolean newNowOpen) {
		boolean oldNowOpen = nowOpen;
		nowOpen = newNowOpen;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__NOW_OPEN, oldNowOpen, nowOpen));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__ELEMENT_NAME:
				return getElementName();
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__NOW_OPEN:
				return isNowOpen();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__ELEMENT_NAME:
				setElementName((String)newValue);
				return;
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__NOW_OPEN:
				setNowOpen((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__ELEMENT_NAME:
				setElementName(ELEMENT_NAME_EDEFAULT);
				return;
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__NOW_OPEN:
				setNowOpen(NOW_OPEN_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__ELEMENT_NAME:
				return ELEMENT_NAME_EDEFAULT == null ? elementName != null : !ELEMENT_NAME_EDEFAULT.equals(elementName);
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT__NOW_OPEN:
				return nowOpen != NOW_OPEN_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (elementName: ");
		result.append(elementName);
		result.append(", nowOpen: ");
		result.append(nowOpen);
		result.append(')');
		return result.toString();
	}

} //SphereOpenCloseEventImpl

/**
 */
package dk.itu.cs.isa.events.events;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GUI Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.GUIEvent#getTitle <em>Title</em>}</li>
 *   <li>{@link dk.itu.cs.isa.events.events.GUIEvent#getButtonLabels <em>Button Labels</em>}</li>
 *   <li>{@link dk.itu.cs.isa.events.events.GUIEvent#getReturnCode <em>Return Code</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getGUIEvent()
 * @model abstract="true"
 * @generated
 */
public interface GUIEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getGUIEvent_Title()
	 * @model required="true"
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.GUIEvent#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Button Labels</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Button Labels</em>' attribute list.
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getGUIEvent_ButtonLabels()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getButtonLabels();

	/**
	 * Returns the value of the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Code</em>' attribute.
	 * @see #setReturnCode(int)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getGUIEvent_ReturnCode()
	 * @model required="true"
	 * @generated
	 */
	int getReturnCode();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.GUIEvent#getReturnCode <em>Return Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Code</em>' attribute.
	 * @see #getReturnCode()
	 * @generated
	 */
	void setReturnCode(int value);

} // GUIEvent

/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WhiteboardFactoryImpl extends EFactoryImpl implements WhiteboardFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WhiteboardFactory init() {
		try {
			WhiteboardFactory theWhiteboardFactory = (WhiteboardFactory)EPackage.Registry.INSTANCE.getEFactory(WhiteboardPackage.eNS_URI);
			if (theWhiteboardFactory != null) {
				return theWhiteboardFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WhiteboardFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhiteboardFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WhiteboardPackage.WHITEBOARD_HISTORY: return createWhiteboardHistory();
			case WhiteboardPackage.WHITEBOARD: return createWhiteboard();
			case WhiteboardPackage.PIN: return createPin();
			case WhiteboardPackage.PHOTO: return createPhoto();
			case WhiteboardPackage.ISA_VECTOR3: return createISAVector3();
			case WhiteboardPackage.ISA_VECTOR2: return createISAVector2();
			case WhiteboardPackage.ISA_COLOR: return createISAColor();
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT: return createAbstractWhiteboardEvent();
			case WhiteboardPackage.BOARD_SPAWN_EVENT: return createBoardSpawnEvent();
			case WhiteboardPackage.BOARD_DUPLICATE_EVENT: return createBoardDuplicateEvent();
			case WhiteboardPackage.BOARD_DESTROY_EVENT: return createBoardDestroyEvent();
			case WhiteboardPackage.UNDO_EVENT: return createUndoEvent();
			case WhiteboardPackage.REDO_EVENT: return createRedoEvent();
			case WhiteboardPackage.PIN_SPLIT_OPERATION: return createPinSplitOperation();
			case WhiteboardPackage.PINS_REMOVE_ALL_OPERATION: return createPinsRemoveAllOperation();
			case WhiteboardPackage.PIN_SOFTWARE_ADD_OPERATION: return createPinSoftwareAddOperation();
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION: return createPinAudioAddOperation();
			case WhiteboardPackage.PIN_REMOVE_OPERATION: return createPinRemoveOperation();
			case WhiteboardPackage.PIN_MOVE_OPERATION: return createPinMoveOperation();
			case WhiteboardPackage.DRAW_FREEHAND_OPERATION: return createDrawFreehandOperation();
			case WhiteboardPackage.DRAW_MODULE_OPERATION: return createDrawModuleOperation();
			case WhiteboardPackage.DRAW_ARROW_OPERATION: return createDrawArrowOperation();
			case WhiteboardPackage.PAINT_CLEAR_ALL_OPERATION: return createPaintClearAllOperation();
			case WhiteboardPackage.PAINT_ERASE_OPERATION: return createPaintEraseOperation();
			case WhiteboardPackage.BOARD_ENLARGE_OPERATION: return createBoardEnlargeOperation();
			case WhiteboardPackage.PHOTO_ADD_OPERATION: return createPhotoAddOperation();
			case WhiteboardPackage.PHOTO_MOVE_OPERATION: return createPhotoMoveOperation();
			case WhiteboardPackage.PHOTO_REMOVE_OPERATION: return createPhotoRemoveOperation();
			case WhiteboardPackage.VISIBLE_RELATION: return createVisibleRelation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhiteboardHistory createWhiteboardHistory() {
		WhiteboardHistoryImpl whiteboardHistory = new WhiteboardHistoryImpl();
		return whiteboardHistory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Whiteboard createWhiteboard() {
		WhiteboardImpl whiteboard = new WhiteboardImpl();
		return whiteboard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Pin createPin() {
		PinImpl pin = new PinImpl();
		return pin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Photo createPhoto() {
		PhotoImpl photo = new PhotoImpl();
		return photo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 createISAVector3() {
		ISAVector3Impl isaVector3 = new ISAVector3Impl();
		return isaVector3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector2 createISAVector2() {
		ISAVector2Impl isaVector2 = new ISAVector2Impl();
		return isaVector2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAColor createISAColor() {
		ISAColorImpl isaColor = new ISAColorImpl();
		return isaColor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractWhiteboardEvent createAbstractWhiteboardEvent() {
		AbstractWhiteboardEventImpl abstractWhiteboardEvent = new AbstractWhiteboardEventImpl();
		return abstractWhiteboardEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoardSpawnEvent createBoardSpawnEvent() {
		BoardSpawnEventImpl boardSpawnEvent = new BoardSpawnEventImpl();
		return boardSpawnEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoardDuplicateEvent createBoardDuplicateEvent() {
		BoardDuplicateEventImpl boardDuplicateEvent = new BoardDuplicateEventImpl();
		return boardDuplicateEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoardDestroyEvent createBoardDestroyEvent() {
		BoardDestroyEventImpl boardDestroyEvent = new BoardDestroyEventImpl();
		return boardDestroyEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UndoEvent createUndoEvent() {
		UndoEventImpl undoEvent = new UndoEventImpl();
		return undoEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RedoEvent createRedoEvent() {
		RedoEventImpl redoEvent = new RedoEventImpl();
		return redoEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PinSplitOperation createPinSplitOperation() {
		PinSplitOperationImpl pinSplitOperation = new PinSplitOperationImpl();
		return pinSplitOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PinsRemoveAllOperation createPinsRemoveAllOperation() {
		PinsRemoveAllOperationImpl pinsRemoveAllOperation = new PinsRemoveAllOperationImpl();
		return pinsRemoveAllOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PinSoftwareAddOperation createPinSoftwareAddOperation() {
		PinSoftwareAddOperationImpl pinSoftwareAddOperation = new PinSoftwareAddOperationImpl();
		return pinSoftwareAddOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PinAudioAddOperation createPinAudioAddOperation() {
		PinAudioAddOperationImpl pinAudioAddOperation = new PinAudioAddOperationImpl();
		return pinAudioAddOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PinRemoveOperation createPinRemoveOperation() {
		PinRemoveOperationImpl pinRemoveOperation = new PinRemoveOperationImpl();
		return pinRemoveOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PinMoveOperation createPinMoveOperation() {
		PinMoveOperationImpl pinMoveOperation = new PinMoveOperationImpl();
		return pinMoveOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DrawFreehandOperation createDrawFreehandOperation() {
		DrawFreehandOperationImpl drawFreehandOperation = new DrawFreehandOperationImpl();
		return drawFreehandOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DrawModuleOperation createDrawModuleOperation() {
		DrawModuleOperationImpl drawModuleOperation = new DrawModuleOperationImpl();
		return drawModuleOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DrawArrowOperation createDrawArrowOperation() {
		DrawArrowOperationImpl drawArrowOperation = new DrawArrowOperationImpl();
		return drawArrowOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaintClearAllOperation createPaintClearAllOperation() {
		PaintClearAllOperationImpl paintClearAllOperation = new PaintClearAllOperationImpl();
		return paintClearAllOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaintEraseOperation createPaintEraseOperation() {
		PaintEraseOperationImpl paintEraseOperation = new PaintEraseOperationImpl();
		return paintEraseOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoardEnlargeOperation createBoardEnlargeOperation() {
		BoardEnlargeOperationImpl boardEnlargeOperation = new BoardEnlargeOperationImpl();
		return boardEnlargeOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhotoAddOperation createPhotoAddOperation() {
		PhotoAddOperationImpl photoAddOperation = new PhotoAddOperationImpl();
		return photoAddOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhotoMoveOperation createPhotoMoveOperation() {
		PhotoMoveOperationImpl photoMoveOperation = new PhotoMoveOperationImpl();
		return photoMoveOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhotoRemoveOperation createPhotoRemoveOperation() {
		PhotoRemoveOperationImpl photoRemoveOperation = new PhotoRemoveOperationImpl();
		return photoRemoveOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibleRelation createVisibleRelation() {
		VisibleRelationImpl visibleRelation = new VisibleRelationImpl();
		return visibleRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhiteboardPackage getWhiteboardPackage() {
		return (WhiteboardPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WhiteboardPackage getPackage() {
		return WhiteboardPackage.eINSTANCE;
	}

} //WhiteboardFactoryImpl

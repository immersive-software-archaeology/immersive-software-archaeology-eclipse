/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent;
import dk.itu.cs.isa.whiteboard.Whiteboard;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>History</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardHistoryImpl#getSystemName <em>System Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardHistoryImpl#getWhiteboards <em>Whiteboards</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.WhiteboardHistoryImpl#getEventLog <em>Event Log</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WhiteboardHistoryImpl extends MinimalEObjectImpl.Container implements WhiteboardHistory {
	/**
	 * The default value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemName()
	 * @generated
	 * @ordered
	 */
	protected static final String SYSTEM_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemName()
	 * @generated
	 * @ordered
	 */
	protected String systemName = SYSTEM_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getWhiteboards() <em>Whiteboards</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWhiteboards()
	 * @generated
	 * @ordered
	 */
	protected EList<Whiteboard> whiteboards;

	/**
	 * The cached value of the '{@link #getEventLog() <em>Event Log</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventLog()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractWhiteboardEvent> eventLog;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WhiteboardHistoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.WHITEBOARD_HISTORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemName(String newSystemName) {
		String oldSystemName = systemName;
		systemName = newSystemName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.WHITEBOARD_HISTORY__SYSTEM_NAME, oldSystemName, systemName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Whiteboard> getWhiteboards() {
		if (whiteboards == null) {
			whiteboards = new EObjectContainmentWithInverseEList<Whiteboard>(Whiteboard.class, this, WhiteboardPackage.WHITEBOARD_HISTORY__WHITEBOARDS, WhiteboardPackage.WHITEBOARD__CONTAINING_HISTORY);
		}
		return whiteboards;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractWhiteboardEvent> getEventLog() {
		if (eventLog == null) {
			eventLog = new EObjectContainmentEList<AbstractWhiteboardEvent>(AbstractWhiteboardEvent.class, this, WhiteboardPackage.WHITEBOARD_HISTORY__EVENT_LOG);
		}
		return eventLog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD_HISTORY__WHITEBOARDS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getWhiteboards()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD_HISTORY__WHITEBOARDS:
				return ((InternalEList<?>)getWhiteboards()).basicRemove(otherEnd, msgs);
			case WhiteboardPackage.WHITEBOARD_HISTORY__EVENT_LOG:
				return ((InternalEList<?>)getEventLog()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD_HISTORY__SYSTEM_NAME:
				return getSystemName();
			case WhiteboardPackage.WHITEBOARD_HISTORY__WHITEBOARDS:
				return getWhiteboards();
			case WhiteboardPackage.WHITEBOARD_HISTORY__EVENT_LOG:
				return getEventLog();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD_HISTORY__SYSTEM_NAME:
				setSystemName((String)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD_HISTORY__WHITEBOARDS:
				getWhiteboards().clear();
				getWhiteboards().addAll((Collection<? extends Whiteboard>)newValue);
				return;
			case WhiteboardPackage.WHITEBOARD_HISTORY__EVENT_LOG:
				getEventLog().clear();
				getEventLog().addAll((Collection<? extends AbstractWhiteboardEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD_HISTORY__SYSTEM_NAME:
				setSystemName(SYSTEM_NAME_EDEFAULT);
				return;
			case WhiteboardPackage.WHITEBOARD_HISTORY__WHITEBOARDS:
				getWhiteboards().clear();
				return;
			case WhiteboardPackage.WHITEBOARD_HISTORY__EVENT_LOG:
				getEventLog().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.WHITEBOARD_HISTORY__SYSTEM_NAME:
				return SYSTEM_NAME_EDEFAULT == null ? systemName != null : !SYSTEM_NAME_EDEFAULT.equals(systemName);
			case WhiteboardPackage.WHITEBOARD_HISTORY__WHITEBOARDS:
				return whiteboards != null && !whiteboards.isEmpty();
			case WhiteboardPackage.WHITEBOARD_HISTORY__EVENT_LOG:
				return eventLog != null && !eventLog.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (systemName: ");
		result.append(systemName);
		result.append(')');
		return result.toString();
	}

} //WhiteboardHistoryImpl

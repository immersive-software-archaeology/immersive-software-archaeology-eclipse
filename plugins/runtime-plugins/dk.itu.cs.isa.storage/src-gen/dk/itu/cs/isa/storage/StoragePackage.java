/**
 */
package dk.itu.cs.isa.storage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.storage.StorageFactory
 * @model kind="package"
 * @generated
 */
public interface StoragePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "storage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/storage";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "storage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StoragePackage eINSTANCE = dk.itu.cs.isa.storage.impl.StoragePackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.storage.impl.ModelStorageImpl <em>Model Storage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.storage.impl.ModelStorageImpl
	 * @see dk.itu.cs.isa.storage.impl.StoragePackageImpl#getModelStorage()
	 * @generated
	 */
	int MODEL_STORAGE = 0;

	/**
	 * The feature id for the '<em><b>Entries</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE__ENTRIES = 0;

	/**
	 * The feature id for the '<em><b>Base Folder</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE__BASE_FOLDER = 1;

	/**
	 * The number of structural features of the '<em>Model Storage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Model Storage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.storage.impl.ModelStorageEntryImpl <em>Model Storage Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.storage.impl.ModelStorageEntryImpl
	 * @see dk.itu.cs.isa.storage.impl.StoragePackageImpl#getModelStorageEntry()
	 * @generated
	 */
	int MODEL_STORAGE_ENTRY = 1;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE_ENTRY__SYSTEM_NAME = 0;

	/**
	 * The feature id for the '<em><b>Number Of Classifiers</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE_ENTRY__NUMBER_OF_CLASSIFIERS = 1;

	/**
	 * The feature id for the '<em><b>Structure Model File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE_ENTRY__STRUCTURE_MODEL_FILE_NAME = 2;

	/**
	 * The feature id for the '<em><b>Quality Model File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE_ENTRY__QUALITY_MODEL_FILE_NAME = 3;

	/**
	 * The feature id for the '<em><b>Visualization Model File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE_ENTRY__VISUALIZATION_MODEL_FILE_NAME = 4;

	/**
	 * The number of structural features of the '<em>Model Storage Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE_ENTRY_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Model Storage Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_STORAGE_ENTRY_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.storage.ModelStorage <em>Model Storage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Storage</em>'.
	 * @see dk.itu.cs.isa.storage.ModelStorage
	 * @generated
	 */
	EClass getModelStorage();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.storage.ModelStorage#getEntries <em>Entries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Entries</em>'.
	 * @see dk.itu.cs.isa.storage.ModelStorage#getEntries()
	 * @see #getModelStorage()
	 * @generated
	 */
	EReference getModelStorage_Entries();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.storage.ModelStorage#getBaseFolder <em>Base Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Folder</em>'.
	 * @see dk.itu.cs.isa.storage.ModelStorage#getBaseFolder()
	 * @see #getModelStorage()
	 * @generated
	 */
	EAttribute getModelStorage_BaseFolder();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.storage.ModelStorageEntry <em>Model Storage Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Storage Entry</em>'.
	 * @see dk.itu.cs.isa.storage.ModelStorageEntry
	 * @generated
	 */
	EClass getModelStorageEntry();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getSystemName <em>System Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Name</em>'.
	 * @see dk.itu.cs.isa.storage.ModelStorageEntry#getSystemName()
	 * @see #getModelStorageEntry()
	 * @generated
	 */
	EAttribute getModelStorageEntry_SystemName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getNumberOfClassifiers <em>Number Of Classifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Classifiers</em>'.
	 * @see dk.itu.cs.isa.storage.ModelStorageEntry#getNumberOfClassifiers()
	 * @see #getModelStorageEntry()
	 * @generated
	 */
	EAttribute getModelStorageEntry_NumberOfClassifiers();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getStructureModelFileName <em>Structure Model File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Structure Model File Name</em>'.
	 * @see dk.itu.cs.isa.storage.ModelStorageEntry#getStructureModelFileName()
	 * @see #getModelStorageEntry()
	 * @generated
	 */
	EAttribute getModelStorageEntry_StructureModelFileName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getQualityModelFileName <em>Quality Model File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quality Model File Name</em>'.
	 * @see dk.itu.cs.isa.storage.ModelStorageEntry#getQualityModelFileName()
	 * @see #getModelStorageEntry()
	 * @generated
	 */
	EAttribute getModelStorageEntry_QualityModelFileName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.storage.ModelStorageEntry#getVisualizationModelFileName <em>Visualization Model File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visualization Model File Name</em>'.
	 * @see dk.itu.cs.isa.storage.ModelStorageEntry#getVisualizationModelFileName()
	 * @see #getModelStorageEntry()
	 * @generated
	 */
	EAttribute getModelStorageEntry_VisualizationModelFileName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StorageFactory getStorageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.storage.impl.ModelStorageImpl <em>Model Storage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.storage.impl.ModelStorageImpl
		 * @see dk.itu.cs.isa.storage.impl.StoragePackageImpl#getModelStorage()
		 * @generated
		 */
		EClass MODEL_STORAGE = eINSTANCE.getModelStorage();

		/**
		 * The meta object literal for the '<em><b>Entries</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_STORAGE__ENTRIES = eINSTANCE.getModelStorage_Entries();

		/**
		 * The meta object literal for the '<em><b>Base Folder</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_STORAGE__BASE_FOLDER = eINSTANCE.getModelStorage_BaseFolder();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.storage.impl.ModelStorageEntryImpl <em>Model Storage Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.storage.impl.ModelStorageEntryImpl
		 * @see dk.itu.cs.isa.storage.impl.StoragePackageImpl#getModelStorageEntry()
		 * @generated
		 */
		EClass MODEL_STORAGE_ENTRY = eINSTANCE.getModelStorageEntry();

		/**
		 * The meta object literal for the '<em><b>System Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_STORAGE_ENTRY__SYSTEM_NAME = eINSTANCE.getModelStorageEntry_SystemName();

		/**
		 * The meta object literal for the '<em><b>Number Of Classifiers</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_STORAGE_ENTRY__NUMBER_OF_CLASSIFIERS = eINSTANCE.getModelStorageEntry_NumberOfClassifiers();

		/**
		 * The meta object literal for the '<em><b>Structure Model File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_STORAGE_ENTRY__STRUCTURE_MODEL_FILE_NAME = eINSTANCE.getModelStorageEntry_StructureModelFileName();

		/**
		 * The meta object literal for the '<em><b>Quality Model File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_STORAGE_ENTRY__QUALITY_MODEL_FILE_NAME = eINSTANCE.getModelStorageEntry_QualityModelFileName();

		/**
		 * The meta object literal for the '<em><b>Visualization Model File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_STORAGE_ENTRY__VISUALIZATION_MODEL_FILE_NAME = eINSTANCE.getModelStorageEntry_VisualizationModelFileName();

	}

} //StoragePackage

package dk.itu.cs.isa.model.system.architecture.preferences;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.preferences.ISAPreferences;

public class ISAPreferencesClustering extends ISAPreferences {

	public static final String CLUSTERING_PACKAGE_INFLUENCE = "CLUSTERING_PACKAGE_INFLUENCE";
	public static final String CLUSTERING_SIBLING_LINKAGE_INFLUENCE = "CLUSTERING_USE_SIBLING_LINKAGE";

	public static final String EPSILON_SAMPLING_RATE = "EPSILON_SAMPLING_RATE";

	public static final String DBSCAN_MINIMUM_CLUSTER_SIZE = "DBSCAN_MINUMUM_CLUSTER_SIZE";
	public static final String DBSCAN_MAXIMUM_CLUSTER_SIZE = "DBSCAN_MAXIMUM_CLUSTER_SIZE";

	@Override
    public void initializeDefaultPreferences() {
		ISAPreferencesClustering.setInt(ISAPreferencesClustering.CLUSTERING_PACKAGE_INFLUENCE, 0);
		ISAPreferencesClustering.setInt(ISAPreferencesClustering.CLUSTERING_SIBLING_LINKAGE_INFLUENCE, 60);
		
		ISAPreferencesClustering.setInt(ISAPreferencesClustering.EPSILON_SAMPLING_RATE, 64);
		
		ISAPreferencesClustering.setInt(ISAPreferencesClustering.DBSCAN_MINIMUM_CLUSTER_SIZE, 15);
		ISAPreferencesClustering.setInt(ISAPreferencesClustering.DBSCAN_MAXIMUM_CLUSTER_SIZE, 50);
	}
	
	
	
	public static void writeMetaInformation(ISASoftwareSystemModel isaSystem) {
		isaSystem.getMetaInfo().add("CLUSTERING_PACKAGE_INFLUENCE="+ISAPreferencesClustering.getInt(ISAPreferencesClustering.CLUSTERING_PACKAGE_INFLUENCE, 0));
		isaSystem.getMetaInfo().add("CLUSTERING_SIBLING_LINKAGE_INFLUENCE="+ISAPreferencesClustering.getInt(ISAPreferencesClustering.CLUSTERING_SIBLING_LINKAGE_INFLUENCE, 60));
		
		isaSystem.getMetaInfo().add("EPSILON_SAMPLING_RATE=1/"+ISAPreferencesClustering.getInt(ISAPreferencesClustering.EPSILON_SAMPLING_RATE, 64));
		
		isaSystem.getMetaInfo().add("DBSCAN_MINIMUM_CLUSTER_SIZE="+ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MINIMUM_CLUSTER_SIZE, 15));
		isaSystem.getMetaInfo().add("DBSCAN_MAXIMUM_CLUSTER_SIZE="+ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MAXIMUM_CLUSTER_SIZE, 50));
	}
	
}

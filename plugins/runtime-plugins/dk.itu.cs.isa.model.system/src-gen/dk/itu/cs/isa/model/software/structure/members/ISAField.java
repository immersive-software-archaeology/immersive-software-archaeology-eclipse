/**
 */
package dk.itu.cs.isa.model.software.structure.members;

import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Field</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAField()
 * @model
 * @generated
 */
public interface ISAField extends ISAMember, ISAReferencingElement, ISATypedElement {
} // ISAField

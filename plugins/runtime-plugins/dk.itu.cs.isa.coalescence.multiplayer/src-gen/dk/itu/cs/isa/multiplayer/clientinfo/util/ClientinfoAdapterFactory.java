/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo.util;

import dk.itu.cs.isa.multiplayer.clientinfo.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage
 * @generated
 */
public class ClientinfoAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ClientinfoPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClientinfoAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ClientinfoPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClientinfoSwitch<Adapter> modelSwitch =
		new ClientinfoSwitch<Adapter>() {
			@Override
			public Adapter caseClientInfo(ClientInfo object) {
				return createClientInfoAdapter();
			}
			@Override
			public Adapter caseBodyPartInfo(BodyPartInfo object) {
				return createBodyPartInfoAdapter();
			}
			@Override
			public Adapter caseHeadInfo(HeadInfo object) {
				return createHeadInfoAdapter();
			}
			@Override
			public Adapter caseHandInfo(HandInfo object) {
				return createHandInfoAdapter();
			}
			@Override
			public Adapter caseHandHeldObjectInfo(HandHeldObjectInfo object) {
				return createHandHeldObjectInfoAdapter();
			}
			@Override
			public Adapter caseISAVector3(ISAVector3 object) {
				return createISAVector3Adapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo <em>Client Info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo
	 * @generated
	 */
	public Adapter createClientInfoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo <em>Body Part Info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo
	 * @generated
	 */
	public Adapter createBodyPartInfoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.clientinfo.HeadInfo <em>Head Info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HeadInfo
	 * @generated
	 */
	public Adapter createHeadInfoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo <em>Hand Info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandInfo
	 * @generated
	 */
	public Adapter createHandInfoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo <em>Hand Held Object Info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo
	 * @generated
	 */
	public Adapter createHandHeldObjectInfoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3 <em>ISA Vector3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3
	 * @generated
	 */
	public Adapter createISAVector3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ClientinfoAdapterFactory

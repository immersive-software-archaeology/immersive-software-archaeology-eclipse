package dk.itu.cs.isa.coalescence.server.requests;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.ISACoalescenceServer;

public abstract class ISAAbstractCoalescenceRequestHandler {
	
	private String name, targetPath;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(this.name == null)
			this.name = name;
	}

	public String getTargetPath() {
		return targetPath;
	}

	public void setTargetPath(String targetPath) {
		if(this.targetPath == null)
			this.targetPath = targetPath;
	}
	
	
	
	public abstract void handle(Request baseRequest, HttpServletResponse response) throws IOException;
	
	
	
	protected void printInvalidParametersHTMLPage(Request baseRequest, HttpServletResponse response, String explanation, String ... exampleParameters) throws IOException {
		for(int i=0; i<exampleParameters.length; i++)
			exampleParameters[i] = "http://" + baseRequest.getServerName() + ":" + ISACoalescenceServer.PORT + "/" + this.getTargetPath() + "?" + exampleParameters[i];
		for(int i=0; i<exampleParameters.length; i++)
			exampleParameters[i] = "<a href=\""+ exampleParameters[i] +"\">"+ exampleParameters[i] +"</a>";
		
		String examplePathsAsHtmlList = "<ul>";
		for(int i=0; i<exampleParameters.length; i++)
			examplePathsAsHtmlList += "<li>"+ exampleParameters[i] +"</li>";
		examplePathsAsHtmlList += "</ul>";
		
		printHTMLPage(baseRequest, response, HttpServletResponse.SC_BAD_REQUEST,
				"Your request contained empty or unsensible parameters", null,
				explanation, "Valid example request path(s) would be:", examplePathsAsHtmlList);
	}
	
	protected void printHTMLPage(Request baseRequest, HttpServletResponse response, int status, String heading, Exception e, String... lines) throws IOException {
		response.setContentType("text/html; charset=utf-8");
		response.setStatus(status);
		
		PrintWriter out = response.getWriter();
		out.println("<html style=\"font-family: 'Open Sans', sans-serif;\">");
		out.println("<h1>"+ heading +"</h1>");
		for(String line : lines)
			if(line != null && !line.isBlank())
				out.println("<p>"+ line +"</p>");
		
		if(e != null) {
			StringWriter stackTraceStringWriter = new StringWriter();
			PrintWriter stackTraceWriter = new PrintWriter(stackTraceStringWriter);
			e.printStackTrace(stackTraceWriter);
			stackTraceWriter.flush();
			
			String stackTraceString = stackTraceStringWriter.toString();
			stackTraceString = stackTraceString.replaceAll("\\n", "</span><br><span style=\"display: inline-block;margin-left:50px;\">"); // width:20px;display: inline-block;
			out.println("<p><span>"+ stackTraceString +"</span></p>");
		}
		
		if(!baseRequest.getPathInContext().equals("/")) {
			// Provide back button
			out.println(
					"<p style=\"margin-top: 50px;\">"
						+ "<strong>&larr;</strong> "
						+ "<a href=\"http://" + baseRequest.getServerName() + ":" + ISACoalescenceServer.PORT + "/" + "\">"
							+ "Overview of Available Paths"
						+ "</a>"
					+ "</p>");
		}

		out.println("<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" id=\"ISA_Logo\" x=\"0px\" y=\"0px\" width=\"1313.982px\" height=\"765.354px\" viewBox=\"0 0 1313.982 765.354\" enable-background=\"new 0 0 1313.982 765.354\" xml:space=\"preserve\" style=\"height: auto; width: 50vw; position: fixed; top: 5vw; right: 5vw; z-index: -1; opacity: 0.2 \">\r\n"
				+ "<path fill=\"#CC3F17\" d=\"M604.305,382.677c0,31.261,25.434,56.693,56.693,56.693c31.261,0,56.693-25.433,56.693-56.693  s-25.433-56.692-56.693-56.692C629.738,325.984,604.305,351.416,604.305,382.677z\"></path>\r\n"
				+ "<path fill=\"#32526B\" d=\"M412.08,467.716h-65.795c-7.479-27.629-11.271-56.229-11.271-85.038c0-18.189,1.543-36.534,4.585-54.523  c0.155-0.918-0.15-1.854-0.818-2.504c-0.534-0.521-1.245-0.803-1.977-0.803c-0.183,0-0.366,0.018-0.55,0.054  c-3.634,0.718-7.339,1.082-11.014,1.082c-13.221,0-26.102-4.675-36.271-13.163c-0.518-0.433-1.163-0.658-1.817-0.658  c-0.324,0-0.652,0.056-0.968,0.17c-0.948,0.346-1.643,1.169-1.821,2.163c-4.01,22.307-6.042,45.246-6.042,68.183  c0,48.325,8.882,95.405,26.399,139.934c0.427,1.085,1.473,1.797,2.639,1.797H412.08c46.89,0,85.037-38.148,85.037-85.039  c0-44.427-41.607-76.112-46.352-79.579c-8.059-5.889-23.111-21.852-23.111-33.807c0-15.63,12.717-28.347,28.346-28.347h56.137  c1.002,0,1.928-0.527,2.438-1.39c11.653-19.665,26.876-36.552,45.245-50.192c0.981-0.729,1.385-2.005,1.002-3.166  c-0.385-1.16-1.469-1.944-2.691-1.944H456c-46.891,0-85.037,38.148-85.037,85.039c0,44.427,41.607,76.112,46.352,79.579  c8.059,5.89,23.11,21.852,23.11,33.807C440.425,455,427.709,467.716,412.08,467.716z\"></path>\r\n"
				+ "<path fill=\"#6D8A9F\" d=\"M431.09,723.469c-21.643-14.625-42.074-31.499-60.729-50.153c-15.839-15.838-30.473-33.064-43.498-51.202  c-0.236-0.331-0.543-0.606-0.896-0.809c-39.809-22.714-78.465-50.175-114.896-81.623C136.25,475.098,90.323,409.429,73.172,382.654  c16.885-26.42,62.148-91.264,136.453-155.731c36.849-31.97,75.993-59.854,116.348-82.881c0.354-0.202,0.66-0.478,0.896-0.809  c13.022-18.133,27.654-35.357,43.491-51.193c18.653-18.653,39.085-35.527,60.728-50.152c1.172-0.792,1.58-2.328,0.956-3.599  c-0.487-0.992-1.489-1.585-2.544-1.585c-0.296,0-0.596,0.047-0.891,0.145c-18.664,6.174-37.206,13.164-55.111,20.774  c-71.87,30.549-139.505,73.103-201.025,126.478C67.787,274.926,17.285,365.17,15.187,368.968l-6.818,12.339  c-0.472,0.854-0.472,1.89,0,2.742l6.818,12.34c2.099,3.797,52.601,94.041,157.286,184.865  c61.521,53.376,129.156,95.93,201.025,126.479c17.906,7.611,36.449,14.602,55.113,20.776c0.295,0.097,0.595,0.144,0.891,0.144  c1.055,0,2.057-0.593,2.544-1.584C432.67,725.798,432.262,724.262,431.09,723.469z\"></path>\r\n"
				+ "<path fill=\"#32526B\" d=\"M748.873,524.408h51.023c1.565,0,2.834-1.269,2.834-2.834V382.677c0-78.151-63.58-141.731-141.73-141.731  c-78.152,0-141.732,63.58-141.732,141.731S582.848,524.409,661,524.409h53.857c1.564,0,2.834-1.27,2.834-2.835v-51.023  c0-1.565-1.27-2.834-2.834-2.834H661c-46.892,0-85.041-38.149-85.041-85.04s38.149-85.039,85.041-85.039  c46.891,0,85.039,38.148,85.039,85.039v138.897C746.039,523.14,747.308,524.408,748.873,524.408z\"></path>\r\n"
				+ "<path fill=\"#6D8A9F\" d=\"M851.439,647.081c-1.124,0.813-1.495,2.318-0.878,3.56c0.489,0.984,1.485,1.573,2.538,1.573  c0.275,0,0.556-0.04,0.831-0.125c41.856-12.828,82.742-30.248,121.522-51.773c0.382-0.213,0.711-0.511,0.959-0.871  c14.622-21.233,27.131-44.046,37.18-67.803c19.963-47.197,30.085-97.315,30.085-148.964c0-51.647-10.122-101.766-30.085-148.964  c-10.043-23.745-22.545-46.548-37.159-67.773c-0.248-0.36-0.576-0.659-0.959-0.871c-38.779-21.525-79.66-38.946-121.509-51.779  c-0.275-0.084-0.556-0.125-0.831-0.125c-1.053,0-2.049,0.589-2.538,1.573c-0.617,1.241-0.246,2.748,0.878,3.56  c84.853,61.306,135.51,160.14,135.51,264.38C986.983,486.934,936.313,585.775,851.439,647.081z\"></path>\r\n"
				+ "<path fill=\"#32526B\" d=\"M1313.63,381.307l-6.818-12.339c-2.098-3.796-52.573-94.017-157.286-184.867  C1088.005,130.726,1020.37,88.173,948.5,57.623C858.546,19.388,761.816,0,660.998,0C609.35,0,559.23,10.122,512.034,30.084  c-45.572,19.276-86.495,46.865-121.63,82c-28.471,28.471-52.111,60.923-70.265,96.455c-0.458,0.898-0.405,1.973,0.14,2.821  c0.521,0.814,1.422,1.303,2.385,1.303l0.121-0.002c0.725-0.031,1.584-0.063,2.455-0.063c16.225,0,31.717,7.012,42.504,19.237  c0.542,0.614,1.318,0.959,2.125,0.959c0.112,0,0.225-0.006,0.338-0.02c0.925-0.111,1.736-0.669,2.171-1.494  c56.72-107.688,167.313-174.587,288.621-174.588c168.652,0,320.029,56.854,449.928,168.98  c74.812,64.577,120.733,130.234,137.883,157.004c-17.152,26.773-63.079,92.436-137.883,157.005  C981.029,651.809,829.651,708.661,660.999,708.661c-114.442,0-221.948-61.379-280.563-160.187c-0.51-0.86-1.437-1.389-2.438-1.389  h-58.113c-0.979,0-1.889,0.505-2.406,1.335c-0.518,0.831-0.57,1.869-0.14,2.748c18.537,37.805,43.12,72.156,73.065,102.102  c35.135,35.135,76.057,62.723,121.63,81.999c47.197,19.963,97.316,30.085,148.965,30.085c100.819,0,197.549-19.387,287.501-57.622  c71.868-30.549,139.503-73.103,201.025-126.478c104.713-90.851,155.188-181.07,157.286-184.866l6.818-12.34  C1314.101,383.196,1314.101,382.16,1313.63,381.307z\"></path>\r\n"
				+ "<path fill=\"#32526B\" d=\"M325.24,240.944c-15.63,0-28.346,12.716-28.346,28.347c0,15.63,12.716,28.346,28.346,28.346  c15.632,0,28.349-12.716,28.349-28.346C353.589,253.66,340.872,240.944,325.24,240.944z\"></path>\r\n"
				+ "</svg>");
		out.println("</html>");
		out.close();
		
		baseRequest.setHandled(true);
	}
    
}

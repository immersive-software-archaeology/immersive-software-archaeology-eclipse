package dk.itu.cs.isa.model.system.creation.java.visitors;

import org.eclipse.jdt.core.dom.*;

public final class CognitiveComplexityVisitor extends ASTVisitor {
	
	private MethodDeclaration rootNode;

	private int nesting = 0;
	private int structural = 0;
	private int fundamental = 0;
	private int hybrid = 0;
	
	

	public CognitiveComplexityVisitor(MethodDeclaration visitedNode) {
		rootNode = visitedNode;
	}
	
	

	public int getCalculatedComplexity() {
		return nesting + structural + fundamental + hybrid * 0;
	}
	
	private int calculateNestingLevel(ASTNode node) {
		if(node == rootNode) {
			return 0;
		}
		
		int additionalNesting;
		switch (node.getParent().getClass().getSimpleName()) {
			case "ForStatement":
			case "EnhancedForStatement":
			case "WhileStatement":
			case "DoStatement":
			case "IfStatement":
			case "ConditionalExpression":
			case "SwitchStatement":
			case "CatchClause":
			case "LambdaExpression":
//			case "AnonymousClassDeclaration":
			case "TypeDeclarationStatement":
				additionalNesting = 1;
				break;
			default:
				additionalNesting = 0;
				break;
		}
		
		return calculateNestingLevel(node.getParent()) + additionalNesting;
	}
	
	
	
	// Increment for breaks in the linear flow
	
	@Override
	public boolean visit(ForStatement node) {
		structural++;
		nesting += calculateNestingLevel(node);
		return super.visit(node);
	}
	
	@Override
	public boolean visit(EnhancedForStatement node) {
		structural++;
		nesting += calculateNestingLevel(node);
		return super.visit(node);
	}
	
	@Override
	public boolean visit(WhileStatement node) {
		structural++;
		nesting += calculateNestingLevel(node);
		return super.visit(node);
	}

	@Override
	public boolean visit(DoStatement node) {
		structural++;
		nesting += calculateNestingLevel(node);
		return super.visit(node);
	}
	
	@Override
	public boolean visit(IfStatement node) {
		if(node.getParent() instanceof IfStatement) {
			// if nested into another if statement, this if statement is an else-if
			hybrid++;
		}
		else {
			// is a regular first-level if
			structural++;
			nesting += calculateNestingLevel(node);
			
			if(node.getElseStatement() != null && !(node.getElseStatement() instanceof IfStatement)) {
				// else statement is not an if-else
				hybrid++;
			}
		}
		
		return super.visit(node);
	}
	
	@Override
	public boolean visit(ConditionalExpression node) {
		structural++;
		return super.visit(node);
	}
	
	@Override
	public boolean visit(SwitchStatement node) {
		structural++;
		nesting += calculateNestingLevel(node);
		return super.visit(node);
	}
	
	@Override
	public boolean visit(CatchClause node) {
		structural++;
		nesting += calculateNestingLevel(node);
		return super.visit(node);
	}
	


	@Override
	public boolean visit(InfixExpression node) {
		// This is a simplified version of Campbell's calculation
		if (node.getOperator().equals(InfixExpression.Operator.CONDITIONAL_AND)
				|| node.getOperator().equals(InfixExpression.Operator.CONDITIONAL_OR)
				|| node.getOperator().equals(InfixExpression.Operator.AND)
				|| node.getOperator().equals(InfixExpression.Operator.OR)) {
			fundamental++;
		}
		return super.visit(node);
	}
	
	

	// Ignore recursion for now...
//	@Override
//	public boolean visit(MethodInvocation node) {
//		fundamental++;
//		return super.visit(node);
//	}
	
	

	@Override
	public boolean visit(BreakStatement node) {
		fundamental++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ContinueStatement node) {
		fundamental++;
		return super.visit(node);
	}

}
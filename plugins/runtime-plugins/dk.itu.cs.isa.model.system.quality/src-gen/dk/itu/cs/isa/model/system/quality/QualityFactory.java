/**
 */
package dk.itu.cs.isa.model.system.quality;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.system.quality.QualityPackage
 * @generated
 */
public interface QualityFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	QualityFactory eINSTANCE = dk.itu.cs.isa.model.system.quality.impl.QualityFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ISA Software System Quality Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Software System Quality Model</em>'.
	 * @generated
	 */
	ISASoftwareSystemQualityModel createISASoftwareSystemQualityModel();

	/**
	 * Returns a new object of class '<em>ISA Classifier Quality Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Classifier Quality Entry</em>'.
	 * @generated
	 */
	ISAClassifierQualityEntry createISAClassifierQualityEntry();

	/**
	 * Returns a new object of class '<em>ISA Measure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Measure</em>'.
	 * @generated
	 */
	ISAMeasure createISAMeasure();

	/**
	 * Returns a new object of class '<em>ISA Issue</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Issue</em>'.
	 * @generated
	 */
	ISAIssue createISAIssue();

	/**
	 * Returns a new object of class '<em>ISA File Passage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA File Passage</em>'.
	 * @generated
	 */
	ISAFilePassage createISAFilePassage();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	QualityPackage getQualityPackage();

} //QualityFactory

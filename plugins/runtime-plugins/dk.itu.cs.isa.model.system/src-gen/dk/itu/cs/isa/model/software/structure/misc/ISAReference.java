/**
 */
package dk.itu.cs.isa.model.software.structure.misc;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAReference#getWeight <em>Weight</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAReference#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReference()
 * @model
 * @generated
 */
public interface ISAReference<T extends ISAReferenceableElement> extends EObject {
	/**
	 * Returns the value of the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Weight</em>' attribute.
	 * @see #setWeight(int)
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReference_Weight()
	 * @model required="true"
	 * @generated
	 */
	int getWeight();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReference#getWeight <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Weight</em>' attribute.
	 * @see #getWeight()
	 * @generated
	 */
	void setWeight(int value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement#getIncomingReferences <em>Incoming References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ISAReferenceableElement)
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReference_Target()
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement#getIncomingReferences
	 * @model opposite="incomingReferences"
	 * @generated
	 */
	T getTarget();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReference#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(T value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	String toString();

} // ISAReference

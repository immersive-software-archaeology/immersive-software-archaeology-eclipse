package dk.itu.cs.isa.model.quality.sonarqube;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.widgets.Display;

import dk.itu.cs.isa.analyzer.exceptions.UserAbortException;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelQualityAnalyzer;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.events.events.OutputEvent;
import dk.itu.cs.isa.model.quality.sonarqube.api.SonarQubeAPIClient;
import dk.itu.cs.isa.model.quality.sonarqube.hook.SonarQubeWebHookServer;
import dk.itu.cs.isa.model.quality.sonarqube.processes.SonarQubeScanProcessHandler;
import dk.itu.cs.isa.model.quality.sonarqube.processes.SonarQubeServerProcessHandler;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel;

public class SystemModelQualityAnalyzerSonarQube implements ISystemModelQualityAnalyzer {
	
	// see https://docs.sonarqube.org/latest/user-guide/metric-definitions/
	private String[] qualityMetrics = new String[] {
			"complexity", // Cyclomatic Complexity = calculated based on the number of paths through the code
			"cognitive_complexity", // Cognitive Complexity = Understandability of Code Control Flow
			"sqale_rating", // Maintainability Rating = formerly the SQALE rating
			"sqale_index", // Technical Debt = Effort to fix all Code Smells, in minutes
			"code_smells", "bugs", "vulnerabilities" // based on hundreds of SonarQube rules
	};
	
	private SonarQubeAPIClient sonarClient;

	private boolean analysisResultsAvailable;
	
	private boolean skipQualityAnalysisAfterServerWasNotReachable;

	

	@Override
	public void initializeInstance() {
		sonarClient = new SonarQubeAPIClient();
	}

	@Override
	public void setupBeforeRun() {
		SonarQubeWebHookServer.INSTANCE.init(this);
		SonarQubeWebHookServer.INSTANCE.startServer();
		skipQualityAnalysisAfterServerWasNotReachable = false;
	}

	@Override
	public void cleanUpAfterRun() {
		try {
			SonarQubeWebHookServer.INSTANCE.stopServer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	@Override
	public void run(ISASoftwareSystemModel structureModel, ISASoftwareSystemQualityModel qualityModel, List<IProject> analyzedProjects, IProgressMonitor monitor) throws Exception {
		monitor.beginTask("Analysing Code Quality", IProgressMonitor.UNKNOWN);
		monitor.subTask("Waiting for the SonarQube server to be ready for queries...");
		
		if(sonarClient.pingServer() == SonarQubeAPIClient.OFFLINE) {
			
			Display.getDefault().syncExec(() -> {
				OutputEvent output = ISAEventSystem.fireConfirmationDialog("SonarQube not reachable",
						"The SonarQube backend server is not reachable. Do you want to start it now?",
						"Start SonarQube and analyze quality", "Skip quality analysis");
				if(output.getReturnCode() != 0) {
					//monitor.setCanceled(true);
					skipQualityAnalysisAfterServerWasNotReachable = true;
					return;
				}
				if(!SonarQubeServerProcessHandler.INSTANCE.startServer()) {
					//monitor.setCanceled(true);
					skipQualityAnalysisAfterServerWasNotReachable = true;
				}
			});
			
			if(skipQualityAnalysisAfterServerWasNotReachable) {
//				monitor.setCanceled(false);
				return;
			}

			while(sonarClient.pingServer() != SonarQubeAPIClient.ONLINE_SANE) {
				if(monitor.isCanceled()) {
					throw new UserAbortException();
				}
				Thread.sleep(100);
			}
			Thread.sleep(5000);
		}
		
		monitor.subTask("Preparing Analysis: Creating new Authentication Token");
		sonarClient.revokePreviousToken();
		String token = sonarClient.generateToken();
		sonarClient.enableLocalWebHooks();
		
		if(monitor.isCanceled())
			throw new UserAbortException();
		
		monitor.subTask("Setting up a new sonar qube project");
		setupSonarQubeProject(structureModel.getName());
		
		if(monitor.isCanceled())
			throw new UserAbortException();

		monitor.subTask("Starting quality analysis");
		analysisResultsAvailable = false;
		SonarQubeScanProcessHandler scanner = new SonarQubeScanProcessHandler();
		scanner.scanProject(structureModel.getName(), analyzedProjects, token, true, monitor);
		
		monitor.subTask("Waiting for SonarQube to process analysis results (listening to web hook)");
		while(!analysisResultsAvailable) {
			Thread.sleep(100);
			if(monitor.isCanceled())
				throw new UserAbortException();
		}

		// At this point, all quality info is available in SonarQube
		ISASonarQubeQualityModelWriter writer = new ISASonarQubeQualityModelWriter(qualityModel, structureModel);
		
		monitor.subTask("Fetching measures from SonarQube server");
		writer.integrateMeasures(sonarClient.getAllMeasures(structureModel.getName(), qualityMetrics));
		
		if(monitor.isCanceled())
			throw new UserAbortException();

		writer.integrateIssues(sonarClient.getAllIssues(structureModel.getName(), monitor));
	}
	
	

	private void setupSonarQubeProject(String name) throws IOException, InterruptedException {
		if(!sonarClient.doesSonarQubeProjectExist(name))
			sonarClient.createSonarQubeProject(name);
		
		sonarClient.deleteAllWebHooks(name);
		sonarClient.setWebHook(name);
	}
	
	public void notifySonarScannerHasFinished() {
		analysisResultsAvailable = true;
	}

}







package dk.itu.cs.isa.model.software.structure.misc.impl;

import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;

public class ISAReferenceImplCustom<T extends ISAReferenceableElement> extends ISAReferenceImpl<T> {

	public String toString() {
		return this.getClass().getSimpleName() + ": "
				+ "[weight: " + (this.getWeight()) + "] "
				+ "[contained: " + (this.eContainer() != null) + "] "
				+ "[target: " + (this.getTarget().getName()) + "] "
				+ "@" + this.hashCode();
	}

}

/**
 */
package dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl;

import dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringFactory;
import dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringPackage;
import dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClusteringPackageImpl extends EPackageImpl implements ClusteringPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaSimpleClusterEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ClusteringPackageImpl() {
		super(eNS_URI, ClusteringFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ClusteringPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ClusteringPackage init() {
		if (isInited) return (ClusteringPackage)EPackage.Registry.INSTANCE.getEPackage(ClusteringPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredClusteringPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ClusteringPackageImpl theClusteringPackage = registeredClusteringPackage instanceof ClusteringPackageImpl ? (ClusteringPackageImpl)registeredClusteringPackage : new ClusteringPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theClusteringPackage.createPackageContents();

		// Initialize created meta-data
		theClusteringPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClusteringPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClusteringPackage.eNS_URI, theClusteringPackage);
		return theClusteringPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISASimpleCluster() {
		return isaSimpleClusterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASimpleCluster_ElementNames() {
		return (EAttribute)isaSimpleClusterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISASimpleCluster_SubClusters() {
		return (EReference)isaSimpleClusterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClusteringFactory getClusteringFactory() {
		return (ClusteringFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaSimpleClusterEClass = createEClass(ISA_SIMPLE_CLUSTER);
		createEAttribute(isaSimpleClusterEClass, ISA_SIMPLE_CLUSTER__ELEMENT_NAMES);
		createEReference(isaSimpleClusterEClass, ISA_SIMPLE_CLUSTER__SUB_CLUSTERS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(isaSimpleClusterEClass, ISASimpleCluster.class, "ISASimpleCluster", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISASimpleCluster_ElementNames(), ecorePackage.getEString(), "elementNames", null, 0, -1, ISASimpleCluster.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISASimpleCluster_SubClusters(), this.getISASimpleCluster(), null, "subClusters", null, 0, -1, ISASimpleCluster.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ClusteringPackageImpl

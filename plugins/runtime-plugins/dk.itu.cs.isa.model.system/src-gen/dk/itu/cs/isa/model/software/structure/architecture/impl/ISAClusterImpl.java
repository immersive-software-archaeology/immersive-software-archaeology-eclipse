/**
 */
package dk.itu.cs.isa.model.software.structure.architecture.impl;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;

import dk.itu.cs.isa.model.software.structure.misc.impl.ISANamedSoftwareElementImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Cluster</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.architecture.impl.ISAClusterImpl#getChildClusters <em>Child Clusters</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.architecture.impl.ISAClusterImpl#getClassifiers <em>Classifiers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.architecture.impl.ISAClusterImpl#getTopWordOccurrences <em>Top Word Occurrences</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAClusterImpl extends ISANamedSoftwareElementImpl implements ISACluster {
	/**
	 * The cached value of the '{@link #getChildClusters() <em>Child Clusters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildClusters()
	 * @generated
	 * @ordered
	 */
	protected EList<ISACluster> childClusters;

	/**
	 * The cached value of the '{@link #getClassifiers() <em>Classifiers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAClassifier> classifiers;

	/**
	 * The cached value of the '{@link #getTopWordOccurrences() <em>Top Word Occurrences</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopWordOccurrences()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAWordOccurrenceEntry> topWordOccurrences;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAClusterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArchitecturePackage.Literals.ISA_CLUSTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISACluster> getChildClusters() {
		if (childClusters == null) {
			childClusters = new EObjectContainmentEList<ISACluster>(ISACluster.class, this, ArchitecturePackage.ISA_CLUSTER__CHILD_CLUSTERS);
		}
		return childClusters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAClassifier> getClassifiers() {
		if (classifiers == null) {
			classifiers = new EObjectResolvingEList<ISAClassifier>(ISAClassifier.class, this, ArchitecturePackage.ISA_CLUSTER__CLASSIFIERS);
		}
		return classifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAWordOccurrenceEntry> getTopWordOccurrences() {
		if (topWordOccurrences == null) {
			topWordOccurrences = new EObjectContainmentEList<ISAWordOccurrenceEntry>(ISAWordOccurrenceEntry.class, this, ArchitecturePackage.ISA_CLUSTER__TOP_WORD_OCCURRENCES);
		}
		return topWordOccurrences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ArchitecturePackage.ISA_CLUSTER__CHILD_CLUSTERS:
				return ((InternalEList<?>)getChildClusters()).basicRemove(otherEnd, msgs);
			case ArchitecturePackage.ISA_CLUSTER__TOP_WORD_OCCURRENCES:
				return ((InternalEList<?>)getTopWordOccurrences()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArchitecturePackage.ISA_CLUSTER__CHILD_CLUSTERS:
				return getChildClusters();
			case ArchitecturePackage.ISA_CLUSTER__CLASSIFIERS:
				return getClassifiers();
			case ArchitecturePackage.ISA_CLUSTER__TOP_WORD_OCCURRENCES:
				return getTopWordOccurrences();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArchitecturePackage.ISA_CLUSTER__CHILD_CLUSTERS:
				getChildClusters().clear();
				getChildClusters().addAll((Collection<? extends ISACluster>)newValue);
				return;
			case ArchitecturePackage.ISA_CLUSTER__CLASSIFIERS:
				getClassifiers().clear();
				getClassifiers().addAll((Collection<? extends ISAClassifier>)newValue);
				return;
			case ArchitecturePackage.ISA_CLUSTER__TOP_WORD_OCCURRENCES:
				getTopWordOccurrences().clear();
				getTopWordOccurrences().addAll((Collection<? extends ISAWordOccurrenceEntry>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArchitecturePackage.ISA_CLUSTER__CHILD_CLUSTERS:
				getChildClusters().clear();
				return;
			case ArchitecturePackage.ISA_CLUSTER__CLASSIFIERS:
				getClassifiers().clear();
				return;
			case ArchitecturePackage.ISA_CLUSTER__TOP_WORD_OCCURRENCES:
				getTopWordOccurrences().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArchitecturePackage.ISA_CLUSTER__CHILD_CLUSTERS:
				return childClusters != null && !childClusters.isEmpty();
			case ArchitecturePackage.ISA_CLUSTER__CLASSIFIERS:
				return classifiers != null && !classifiers.isEmpty();
			case ArchitecturePackage.ISA_CLUSTER__TOP_WORD_OCCURRENCES:
				return topWordOccurrences != null && !topWordOccurrences.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ISAClusterImpl

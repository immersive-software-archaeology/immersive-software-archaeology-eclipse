package dk.itu.cs.isa.eclipse.ui.activation;

public interface GUIActivationListener {
	
	public void start();
	
	public void stop();
	
}

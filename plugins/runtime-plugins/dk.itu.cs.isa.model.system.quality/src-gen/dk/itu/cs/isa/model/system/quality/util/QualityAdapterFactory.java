/**
 */
package dk.itu.cs.isa.model.system.quality.util;

import dk.itu.cs.isa.model.system.quality.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.system.quality.QualityPackage
 * @generated
 */
public class QualityAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static QualityPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualityAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = QualityPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QualitySwitch<Adapter> modelSwitch =
		new QualitySwitch<Adapter>() {
			@Override
			public Adapter caseISASoftwareSystemQualityModel(ISASoftwareSystemQualityModel object) {
				return createISASoftwareSystemQualityModelAdapter();
			}
			@Override
			public Adapter caseISAClassifierQualityEntry(ISAClassifierQualityEntry object) {
				return createISAClassifierQualityEntryAdapter();
			}
			@Override
			public Adapter caseISAMeasure(ISAMeasure object) {
				return createISAMeasureAdapter();
			}
			@Override
			public Adapter caseISAIssue(ISAIssue object) {
				return createISAIssueAdapter();
			}
			@Override
			public Adapter caseISAFilePassage(ISAFilePassage object) {
				return createISAFilePassageAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel <em>ISA Software System Quality Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel
	 * @generated
	 */
	public Adapter createISASoftwareSystemQualityModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry <em>ISA Classifier Quality Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry
	 * @generated
	 */
	public Adapter createISAClassifierQualityEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure <em>ISA Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.system.quality.ISAMeasure
	 * @generated
	 */
	public Adapter createISAMeasureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.system.quality.ISAIssue <em>ISA Issue</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue
	 * @generated
	 */
	public Adapter createISAIssueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage <em>ISA File Passage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.system.quality.ISAFilePassage
	 * @generated
	 */
	public Adapter createISAFilePassageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //QualityAdapterFactory

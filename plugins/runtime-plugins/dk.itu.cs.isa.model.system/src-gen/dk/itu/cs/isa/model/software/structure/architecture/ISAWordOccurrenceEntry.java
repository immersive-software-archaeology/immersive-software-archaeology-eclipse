/**
 */
package dk.itu.cs.isa.model.software.structure.architecture;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Word Occurrence Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry#getWord <em>Word</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry#getCount <em>Count</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage#getISAWordOccurrenceEntry()
 * @model
 * @generated
 */
public interface ISAWordOccurrenceEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Word</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Word</em>' attribute.
	 * @see #setWord(String)
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage#getISAWordOccurrenceEntry_Word()
	 * @model required="true"
	 * @generated
	 */
	String getWord();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry#getWord <em>Word</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Word</em>' attribute.
	 * @see #getWord()
	 * @generated
	 */
	void setWord(String value);

	/**
	 * Returns the value of the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count</em>' attribute.
	 * @see #setCount(int)
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage#getISAWordOccurrenceEntry_Count()
	 * @model required="true"
	 * @generated
	 */
	int getCount();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry#getCount <em>Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count</em>' attribute.
	 * @see #getCount()
	 * @generated
	 */
	void setCount(int value);

} // ISAWordOccurrenceEntry

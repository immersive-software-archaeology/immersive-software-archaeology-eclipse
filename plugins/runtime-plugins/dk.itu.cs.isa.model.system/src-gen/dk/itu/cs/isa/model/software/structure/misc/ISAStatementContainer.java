/**
 */
package dk.itu.cs.isa.model.software.structure.misc;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Statement Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfStatements <em>Number Of Statements</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfExpressions <em>Number Of Expressions</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfControlFlowSplits <em>Number Of Control Flow Splits</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getCognitiveComplexity <em>Cognitive Complexity</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAStatementContainer()
 * @model abstract="true"
 * @generated
 */
public interface ISAStatementContainer extends ISAReferencingElement {
	/**
	 * Returns the value of the '<em><b>Number Of Statements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Statements</em>' attribute.
	 * @see #setNumberOfStatements(int)
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAStatementContainer_NumberOfStatements()
	 * @model required="true"
	 * @generated
	 */
	int getNumberOfStatements();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfStatements <em>Number Of Statements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Statements</em>' attribute.
	 * @see #getNumberOfStatements()
	 * @generated
	 */
	void setNumberOfStatements(int value);

	/**
	 * Returns the value of the '<em><b>Number Of Expressions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Expressions</em>' attribute.
	 * @see #setNumberOfExpressions(int)
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAStatementContainer_NumberOfExpressions()
	 * @model required="true"
	 * @generated
	 */
	int getNumberOfExpressions();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfExpressions <em>Number Of Expressions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Expressions</em>' attribute.
	 * @see #getNumberOfExpressions()
	 * @generated
	 */
	void setNumberOfExpressions(int value);

	/**
	 * Returns the value of the '<em><b>Number Of Control Flow Splits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Control Flow Splits</em>' attribute.
	 * @see #setNumberOfControlFlowSplits(int)
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAStatementContainer_NumberOfControlFlowSplits()
	 * @model required="true"
	 * @generated
	 */
	int getNumberOfControlFlowSplits();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfControlFlowSplits <em>Number Of Control Flow Splits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Control Flow Splits</em>' attribute.
	 * @see #getNumberOfControlFlowSplits()
	 * @generated
	 */
	void setNumberOfControlFlowSplits(int value);

	/**
	 * Returns the value of the '<em><b>Cognitive Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cognitive Complexity</em>' attribute.
	 * @see #setCognitiveComplexity(int)
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAStatementContainer_CognitiveComplexity()
	 * @model required="true"
	 * @generated
	 */
	int getCognitiveComplexity();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getCognitiveComplexity <em>Cognitive Complexity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cognitive Complexity</em>' attribute.
	 * @see #getCognitiveComplexity()
	 * @generated
	 */
	void setCognitiveComplexity(int value);

} // ISAStatementContainer

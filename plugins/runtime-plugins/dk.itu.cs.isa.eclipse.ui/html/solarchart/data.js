var rawData = [
    {
        "size": 5.2,
        "name": "Line",
        "links": [
            "Tasks",
            "Options",
            "Stream",
            "Source"
        ],
        "y_vis": -0.106556214,
        "x_vis": -3.043644
    },
    {
        "size": 10.360001,
        "name": "Execute",
        "links": [
            "Tasks",
            "Selector",
            "Source",
            "Status",
            "Stream",
            "Options",
            "Data",
            "Encode"
        ],
        "y_vis": 2.3408618,
        "x_vis": -1.3172226
    },
    {
        "size": 7.56,
        "name": "Selector",
        "links": [
            "Tasks",
            "Stream",
            "Source",
            "Options",
            "Status",
            "Data"
        ],
        "y_vis": -2.6689918,
        "x_vis": 0.0950365
    },
    {
        "size": 5.56,
        "name": "Source",
        "links": [
            "Execute",
            "Tasks",
            "Stream",
            "Options"
        ],
        "y_vis": 1.1915839,
        "x_vis": -2.383055
    },
    {
        "size": 5.4,
        "name": "Change",
        "links": [
            "Tasks",
            "Execute",
            "Source",
            "Selector",
            "Stream"
        ],
        "y_vis": -2.1677938,
        "x_vis": -1.4157442
    },
    {
        "size": 5.08,
        "name": "Src",
        "links": [
            "Tasks",
            "Source",
            "Selector"
        ],
        "y_vis": -1.7753735,
        "x_vis": 0.80977833
    },
    {
        "size": 20,
        "name": "Tasks",
        "links": [
            "Source",
            "Selector",
            "Options",
            "Execute",
            "Status",
            "Stream",
            "Data",
            "Encode",
            "Line"
        ],
        "y_vis": 0.5724801,
        "x_vis": 1.2095337
    },
    {
        "size": 5,
        "name": "Encode",
        "links": [],
        "y_vis": 2.1023886,
        "x_vis": -0.08595184
    },
    {
        "size": 5.4,
        "name": "Data",
        "links": ["Tasks"],
        "y_vis": -1.4866428,
        "x_vis": 2.6139495
    },
    {
        "size": 9.48,
        "name": "Stream",
        "links": [
            "Tasks",
            "Selector",
            "Options",
            "Source",
            "Execute",
            "Change"
        ],
        "y_vis": -1.0507959,
        "x_vis": -2.1657412
    },
    {
        "size": 5.48,
        "name": "Options",
        "links": ["Source"],
        "y_vis": 2.1710048,
        "x_vis": 1.2540761
    },
    {
        "size": 6.88,
        "name": "Status",
        "links": ["Tasks"],
        "y_vis": 0.17762779,
        "x_vis": 2.2723744
    }
]
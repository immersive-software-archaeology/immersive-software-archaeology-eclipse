package dk.itu.cs.isa.coalescence.server.requests;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import dk.itu.cs.isa.coalescence.server.ISACoalescenceServer;
import dk.itu.cs.isa.coalescence.server.extensions.ExtensionPointHandler;

public class ISABaseRequestHandler extends AbstractHandler {

    @Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
    	
    	for(ISAAbstractCoalescenceRequestHandler requestHandler : ExtensionPointHandler.INSTANCE.getRequestHandlers()) {
	    	if(pathsEqual(target, requestHandler.getTargetPath())) {
	    		requestHandler.handle(baseRequest, response);
	    		if(baseRequest.isHandled())
	    			return;
			}
    	}
    	
    	List<String> validPaths = ExtensionPointHandler.INSTANCE.getRegisteredPaths();
    	for(int i=0; i<validPaths.size(); i++)
    		validPaths.set(i, "http://" + baseRequest.getServerName() + ":" + ISACoalescenceServer.PORT + "/" + validPaths.get(i));
    	
		(new WelcomePageRequestHandler()).handle(baseRequest, response, target, validPaths.toArray(new String[0]));
		baseRequest.setHandled(true);
	}

	protected boolean pathsEqual(String target, String compare) {
		if(target.startsWith("/"))
			target = target.substring(1);
		if(compare.startsWith("/"))
			compare = compare.substring(1);

		if(target.endsWith("/"))
			target = target.substring(0, target.length()-1);
		if(compare.endsWith("/"))
			compare = compare.substring(0, target.length()-1);
			
		String[] targetSegments = target.trim().split("/");
		String[] compareSegments = compare.trim().split("/");
		
		if(targetSegments.length != compareSegments.length) {
			return false;
		}
		
		for(int i=0; i<targetSegments.length; i++) {
			if(!targetSegments[i].equalsIgnoreCase(compareSegments[i])) {
				return false;
			}
		}
		
		return true;
	}
	
}

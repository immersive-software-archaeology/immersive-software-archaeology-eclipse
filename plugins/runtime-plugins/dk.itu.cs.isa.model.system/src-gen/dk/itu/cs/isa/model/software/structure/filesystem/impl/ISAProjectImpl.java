/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem.impl;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;

import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

import dk.itu.cs.isa.model.software.structure.misc.impl.ISANamedSoftwareElementImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAProjectImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAProjectImpl#getPackageRoots <em>Package Roots</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAProjectImpl extends ISANamedSoftwareElementImpl implements ISAProject {
	/**
	 * The cached value of the '{@link #getPackageRoots() <em>Package Roots</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageRoots()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAPackageRoot> packageRoots;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FilesystemPackage.Literals.ISA_PROJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISASoftwareSystemModel getParent() {
		if (eContainerFeatureID() != FilesystemPackage.ISA_PROJECT__PARENT) return null;
		return (ISASoftwareSystemModel)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(ISASoftwareSystemModel newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, FilesystemPackage.ISA_PROJECT__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParent(ISASoftwareSystemModel newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != FilesystemPackage.ISA_PROJECT__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS, ISASoftwareSystemModel.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FilesystemPackage.ISA_PROJECT__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAPackageRoot> getPackageRoots() {
		if (packageRoots == null) {
			packageRoots = new EObjectContainmentWithInverseEList<ISAPackageRoot>(ISAPackageRoot.class, this, FilesystemPackage.ISA_PROJECT__PACKAGE_ROOTS, FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT);
		}
		return packageRoots;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAPackage getPackage(String packageName) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISANamedSoftwareElement getElement(String qualifiedElementName) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FilesystemPackage.ISA_PROJECT__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((ISASoftwareSystemModel)otherEnd, msgs);
			case FilesystemPackage.ISA_PROJECT__PACKAGE_ROOTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPackageRoots()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FilesystemPackage.ISA_PROJECT__PARENT:
				return basicSetParent(null, msgs);
			case FilesystemPackage.ISA_PROJECT__PACKAGE_ROOTS:
				return ((InternalEList<?>)getPackageRoots()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case FilesystemPackage.ISA_PROJECT__PARENT:
				return eInternalContainer().eInverseRemove(this, StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS, ISASoftwareSystemModel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FilesystemPackage.ISA_PROJECT__PARENT:
				return getParent();
			case FilesystemPackage.ISA_PROJECT__PACKAGE_ROOTS:
				return getPackageRoots();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FilesystemPackage.ISA_PROJECT__PARENT:
				setParent((ISASoftwareSystemModel)newValue);
				return;
			case FilesystemPackage.ISA_PROJECT__PACKAGE_ROOTS:
				getPackageRoots().clear();
				getPackageRoots().addAll((Collection<? extends ISAPackageRoot>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FilesystemPackage.ISA_PROJECT__PARENT:
				setParent((ISASoftwareSystemModel)null);
				return;
			case FilesystemPackage.ISA_PROJECT__PACKAGE_ROOTS:
				getPackageRoots().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FilesystemPackage.ISA_PROJECT__PARENT:
				return getParent() != null;
			case FilesystemPackage.ISA_PROJECT__PACKAGE_ROOTS:
				return packageRoots != null && !packageRoots.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case FilesystemPackage.ISA_PROJECT___GET_PACKAGE__STRING:
				return getPackage((String)arguments.get(0));
			case FilesystemPackage.ISA_PROJECT___GET_ELEMENT__STRING:
				return getElement((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ISAProjectImpl

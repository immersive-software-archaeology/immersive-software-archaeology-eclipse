/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Paint Clear All Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPaintClearAllOperation()
 * @model
 * @generated
 */
public interface PaintClearAllOperation extends AbstractDrawOperation {
} // PaintClearAllOperation

/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Package</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageImpl#getClassifiers <em>Classifiers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageImpl#getParentContainer <em>Parent Container</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAPackageImpl extends ISAPackageContainerImpl implements ISAPackage {
	/**
	 * The cached value of the '{@link #getClassifiers() <em>Classifiers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAClassifier> classifiers;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAPackageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FilesystemPackage.Literals.ISA_PACKAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAClassifier> getClassifiers() {
		if (classifiers == null) {
			classifiers = new EObjectContainmentWithInverseEList<ISAClassifier>(ISAClassifier.class, this, FilesystemPackage.ISA_PACKAGE__CLASSIFIERS, ClassifiersPackage.ISA_CLASSIFIER__PARENT);
		}
		return classifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAPackageContainer getParentContainer() {
		if (eContainerFeatureID() != FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER) return null;
		return (ISAPackageContainer)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentContainer(ISAPackageContainer newParentContainer, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentContainer, FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentContainer(ISAPackageContainer newParentContainer) {
		if (newParentContainer != eInternalContainer() || (eContainerFeatureID() != FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER && newParentContainer != null)) {
			if (EcoreUtil.isAncestor(this, newParentContainer))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentContainer != null)
				msgs = ((InternalEObject)newParentContainer).eInverseAdd(this, FilesystemPackage.ISA_PACKAGE_CONTAINER__SUB_PACKAGES, ISAPackageContainer.class, msgs);
			msgs = basicSetParentContainer(newParentContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER, newParentContainer, newParentContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE__CLASSIFIERS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getClassifiers()).basicAdd(otherEnd, msgs);
			case FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentContainer((ISAPackageContainer)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE__CLASSIFIERS:
				return ((InternalEList<?>)getClassifiers()).basicRemove(otherEnd, msgs);
			case FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER:
				return basicSetParentContainer(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER:
				return eInternalContainer().eInverseRemove(this, FilesystemPackage.ISA_PACKAGE_CONTAINER__SUB_PACKAGES, ISAPackageContainer.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE__CLASSIFIERS:
				return getClassifiers();
			case FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER:
				return getParentContainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE__CLASSIFIERS:
				getClassifiers().clear();
				getClassifiers().addAll((Collection<? extends ISAClassifier>)newValue);
				return;
			case FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER:
				setParentContainer((ISAPackageContainer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE__CLASSIFIERS:
				getClassifiers().clear();
				return;
			case FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER:
				setParentContainer((ISAPackageContainer)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE__CLASSIFIERS:
				return classifiers != null && !classifiers.isEmpty();
			case FilesystemPackage.ISA_PACKAGE__PARENT_CONTAINER:
				return getParentContainer() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAClassifierContainer.class) {
			switch (derivedFeatureID) {
				case FilesystemPackage.ISA_PACKAGE__CLASSIFIERS: return ClassifiersPackage.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAClassifierContainer.class) {
			switch (baseFeatureID) {
				case ClassifiersPackage.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS: return FilesystemPackage.ISA_PACKAGE__CLASSIFIERS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ISAPackageImpl

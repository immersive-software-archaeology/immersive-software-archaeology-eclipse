/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.RedoEvent;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Redo Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RedoEventImpl extends AbstractWhiteboardEventImpl implements RedoEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RedoEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.REDO_EVENT;
	}

} //RedoEventImpl

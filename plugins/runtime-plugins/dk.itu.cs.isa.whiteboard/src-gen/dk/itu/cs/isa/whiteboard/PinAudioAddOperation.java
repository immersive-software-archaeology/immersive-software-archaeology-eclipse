/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pin Audio Add Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PinAudioAddOperation#getSoundWaveBase64 <em>Sound Wave Base64</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PinAudioAddOperation#getSoundWaveFrequency <em>Sound Wave Frequency</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinAudioAddOperation()
 * @model
 * @generated
 */
public interface PinAudioAddOperation extends AbstractPinOperation {
	/**
	 * Returns the value of the '<em><b>Sound Wave Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sound Wave Base64</em>' attribute.
	 * @see #setSoundWaveBase64(String)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinAudioAddOperation_SoundWaveBase64()
	 * @model required="true"
	 * @generated
	 */
	String getSoundWaveBase64();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PinAudioAddOperation#getSoundWaveBase64 <em>Sound Wave Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sound Wave Base64</em>' attribute.
	 * @see #getSoundWaveBase64()
	 * @generated
	 */
	void setSoundWaveBase64(String value);

	/**
	 * Returns the value of the '<em><b>Sound Wave Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sound Wave Frequency</em>' attribute.
	 * @see #setSoundWaveFrequency(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinAudioAddOperation_SoundWaveFrequency()
	 * @model required="true"
	 * @generated
	 */
	int getSoundWaveFrequency();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PinAudioAddOperation#getSoundWaveFrequency <em>Sound Wave Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sound Wave Frequency</em>' attribute.
	 * @see #getSoundWaveFrequency()
	 * @generated
	 */
	void setSoundWaveFrequency(int value);

} // PinAudioAddOperation

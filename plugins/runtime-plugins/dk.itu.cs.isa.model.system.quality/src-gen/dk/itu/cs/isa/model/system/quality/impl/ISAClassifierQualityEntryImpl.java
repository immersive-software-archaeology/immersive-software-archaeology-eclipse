/**
 */
package dk.itu.cs.isa.model.system.quality.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;

import dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry;
import dk.itu.cs.isa.model.system.quality.ISAIssue;
import dk.itu.cs.isa.model.system.quality.ISAMeasure;
import dk.itu.cs.isa.model.system.quality.QualityPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Classifier Quality Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAClassifierQualityEntryImpl#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAClassifierQualityEntryImpl#getMeasures <em>Measures</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAClassifierQualityEntryImpl#getIssues <em>Issues</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAClassifierQualityEntryImpl extends MinimalEObjectImpl.Container implements ISAClassifierQualityEntry {
	/**
	 * The cached value of the '{@link #getClassifier() <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected ISAClassifier classifier;

	/**
	 * The cached value of the '{@link #getMeasures() <em>Measures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasures()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAMeasure> measures;

	/**
	 * The cached value of the '{@link #getIssues() <em>Issues</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIssues()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAIssue> issues;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAClassifierQualityEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QualityPackage.Literals.ISA_CLASSIFIER_QUALITY_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAClassifier getClassifier() {
		if (classifier != null && classifier.eIsProxy()) {
			InternalEObject oldClassifier = (InternalEObject)classifier;
			classifier = (ISAClassifier)eResolveProxy(oldClassifier);
			if (classifier != oldClassifier) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__CLASSIFIER, oldClassifier, classifier));
			}
		}
		return classifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAClassifier basicGetClassifier() {
		return classifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setClassifier(ISAClassifier newClassifier) {
		ISAClassifier oldClassifier = classifier;
		classifier = newClassifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__CLASSIFIER, oldClassifier, classifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAMeasure> getMeasures() {
		if (measures == null) {
			measures = new EObjectContainmentWithInverseEList<ISAMeasure>(ISAMeasure.class, this, QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES, QualityPackage.ISA_MEASURE__PARENT_ENTRY);
		}
		return measures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAIssue> getIssues() {
		if (issues == null) {
			issues = new EObjectContainmentWithInverseEList<ISAIssue>(ISAIssue.class, this, QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES, QualityPackage.ISA_ISSUE__PARENT_ENTRY);
		}
		return issues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMeasures()).basicAdd(otherEnd, msgs);
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIssues()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES:
				return ((InternalEList<?>)getMeasures()).basicRemove(otherEnd, msgs);
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES:
				return ((InternalEList<?>)getIssues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__CLASSIFIER:
				if (resolve) return getClassifier();
				return basicGetClassifier();
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES:
				return getMeasures();
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES:
				return getIssues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__CLASSIFIER:
				setClassifier((ISAClassifier)newValue);
				return;
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES:
				getMeasures().clear();
				getMeasures().addAll((Collection<? extends ISAMeasure>)newValue);
				return;
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES:
				getIssues().clear();
				getIssues().addAll((Collection<? extends ISAIssue>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__CLASSIFIER:
				setClassifier((ISAClassifier)null);
				return;
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES:
				getMeasures().clear();
				return;
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES:
				getIssues().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__CLASSIFIER:
				return classifier != null;
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES:
				return measures != null && !measures.isEmpty();
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES:
				return issues != null && !issues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ISAClassifierQualityEntryImpl

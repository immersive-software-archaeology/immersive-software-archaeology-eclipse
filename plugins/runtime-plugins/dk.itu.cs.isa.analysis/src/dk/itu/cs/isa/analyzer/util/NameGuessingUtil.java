package dk.itu.cs.isa.analyzer.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;

import dk.itu.cs.isa.preferences.ISAPreferences;
import dk.itu.cs.isa.util.StringUtil;

public class NameGuessingUtil {

	public static String guessName(List<IProject> selectedProjects) {
		List<String> projectNames = new ArrayList<String>(selectedProjects.size());
		for(IProject project : selectedProjects)
			projectNames.add(project.getName());
		
		String longestCommonSubString = StringUtil.calculateLongestCommonSubString(projectNames);
		
		while(longestCommonSubString.endsWith("."))
			longestCommonSubString = longestCommonSubString.substring(0, longestCommonSubString.length()-1);
		
		if(longestCommonSubString.isEmpty() || longestCommonSubString.isBlank())
			return ISAPreferences.getString(ISAPreferences.LAST_MODEL_NAME, "New Model");
		else
			return longestCommonSubString;
	}

}

/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pin Move Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PinMoveOperation#getPreTextureSpacePosition <em>Pre Texture Space Position</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinMoveOperation()
 * @model
 * @generated
 */
public interface PinMoveOperation extends AbstractPinOperation {
	/**
	 * Returns the value of the '<em><b>Pre Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Texture Space Position</em>' containment reference.
	 * @see #setPreTextureSpacePosition(ISAVector2)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinMoveOperation_PreTextureSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector2 getPreTextureSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PinMoveOperation#getPreTextureSpacePosition <em>Pre Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Texture Space Position</em>' containment reference.
	 * @see #getPreTextureSpacePosition()
	 * @generated
	 */
	void setPreTextureSpacePosition(ISAVector2 value);

} // PinMoveOperation

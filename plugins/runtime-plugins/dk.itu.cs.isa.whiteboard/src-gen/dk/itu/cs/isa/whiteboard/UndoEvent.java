/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Undo Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getUndoEvent()
 * @model
 * @generated
 */
public interface UndoEvent extends AbstractWhiteboardEvent {
} // UndoEvent

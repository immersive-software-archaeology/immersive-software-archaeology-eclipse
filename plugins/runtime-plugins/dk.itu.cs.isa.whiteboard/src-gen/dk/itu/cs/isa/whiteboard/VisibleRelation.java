/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visible Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.VisibleRelation#getElementName <em>Element Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.VisibleRelation#isForward <em>Forward</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.VisibleRelation#getRelationType <em>Relation Type</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getVisibleRelation()
 * @model
 * @generated
 */
public interface VisibleRelation extends EObject {
	/**
	 * Returns the value of the '<em><b>Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Name</em>' attribute.
	 * @see #setElementName(String)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getVisibleRelation_ElementName()
	 * @model required="true"
	 * @generated
	 */
	String getElementName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.VisibleRelation#getElementName <em>Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Name</em>' attribute.
	 * @see #getElementName()
	 * @generated
	 */
	void setElementName(String value);

	/**
	 * Returns the value of the '<em><b>Forward</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Forward</em>' attribute.
	 * @see #setForward(boolean)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getVisibleRelation_Forward()
	 * @model required="true"
	 * @generated
	 */
	boolean isForward();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.VisibleRelation#isForward <em>Forward</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Forward</em>' attribute.
	 * @see #isForward()
	 * @generated
	 */
	void setForward(boolean value);

	/**
	 * Returns the value of the '<em><b>Relation Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation Type</em>' attribute.
	 * @see #setRelationType(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getVisibleRelation_RelationType()
	 * @model required="true"
	 * @generated
	 */
	int getRelationType();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.VisibleRelation#getRelationType <em>Relation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relation Type</em>' attribute.
	 * @see #getRelationType()
	 * @generated
	 */
	void setRelationType(int value);

} // VisibleRelation

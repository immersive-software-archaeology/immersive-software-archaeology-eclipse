/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Draw Module Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getDrawModuleOperation()
 * @model
 * @generated
 */
public interface DrawModuleOperation extends AbstractDrawOperation {
} // DrawModuleOperation

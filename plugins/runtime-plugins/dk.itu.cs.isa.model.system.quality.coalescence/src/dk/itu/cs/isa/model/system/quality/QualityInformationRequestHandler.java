package dk.itu.cs.isa.model.system.quality;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.storage.ISAModelStorage;

public class QualityInformationRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		String elementName = baseRequest.getParameter("elementName");
		
		if(systemName == null || elementName == null) {
			String examplePath1 = "systemName=Ant&elementName=org.apache.tools.ant.AntClassLoader";
			String examplePath2 = "systemName=de.christophseidl.util&elementName=de.christophseidl.util.ecore.EcoreIOUtil";
			printInvalidParametersHTMLPage(baseRequest, response, null, examplePath1, examplePath2);
			return;
		}
		
		JSONObject qualityJSON;
		try {
			qualityJSON = gatherQualityInformation(systemName, elementName);
		}
		catch(QualityInformationRequestException e) {
			e.printStackTrace();
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_BAD_REQUEST, e.getTitle(), e);
			return;
		}
		
		response.setContentType("application/json; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
		PrintWriter out = response.getWriter();
		out.println(qualityJSON.toString(2));
		out.close();
		
		baseRequest.setHandled(true);
	}
	
	
	
	private JSONObject gatherQualityInformation(String systemName, String elementName) throws QualityInformationRequestException {
		ISASoftwareSystemModel structureModel = null;
		ISASoftwareSystemQualityModel qualityModel = null;
		try {
			structureModel = ISAModelStorage.INSTANCE.loadStructureModel(systemName);
			qualityModel = ISAModelStorage.INSTANCE.loadQualityModel(systemName);
		} catch (Exception e) {
			// Ignore exceptions, problems with loading models will become visible in the following check
		}
		
		if(structureModel == null || qualityModel == null)
			throw new QualityInformationRequestException("Empty Model", "A model for the system \""+ systemName +"\" is empty.<br>" +
					"Please run the analysis of the system again, using the ISA Eclipse plugins. Make sure to include the ISA quality analysis.<br><br>" +
					"Structure Model empty: " + (structureModel == null ? "Yes" : "No") + "<br>" +
					"Quality Model empty: " + (qualityModel == null ? "Yes" : "No"));
		
		ISANamedSoftwareElement element = structureModel.getSystemElementByName(elementName);
		if(element instanceof ISAClassifier) {
			ISAClassifier classifier = (ISAClassifier) element;
			for(ISAClassifierQualityEntry entry : qualityModel.getClassifierEntries()) {
				if(classifier.equals(entry.getClassifier())) {
					return buildJsonForQualityEntry(entry);
				}
			}
		}
		else {
			if(element != null)
				throw new QualityInformationRequestException("Unsupported kind of element",
					"You tried to access information on an element of type " + element.getClass().getSimpleName() + ".<br>" +
					"However, in its current version, the quality model only contains information on classifiers.");
			else
				throw new QualityInformationRequestException("Element not found",
					"An element with qualified name \""+ elementName +"\" does not exist in the system \""+ systemName +"\".");
		}

		throw new QualityInformationRequestException("Element not found",
				"The element \""+ elementName +"\" is not contained in the system \""+ systemName +"\".");
	}
	
	private JSONObject buildJsonForQualityEntry(ISAClassifierQualityEntry entry) {
		JSONObject qualityJSON = new JSONObject();
		qualityJSON.put("element", entry.getClassifier().getFullyQualifiedName());
		
		JSONArray measureJSONs = new JSONArray();
		for(ISAMeasure isaMeasure : entry.getMeasures()) {
			JSONObject measureJSON = new JSONObject();
			measureJSON.put("metric", isaMeasure.getMetric());
			measureJSON.put("value", isaMeasure.getValue());
			
			measureJSONs.put(measureJSON);
		}
		qualityJSON.put("measures", measureJSONs);
		
		JSONArray issuesJSONs = new JSONArray();
		for(ISAIssue isaIssue : entry.getIssues()) {
			JSONObject issueJSON = new JSONObject();
			
			issueJSON.put("effortToFixInMinutes", isaIssue.getEffortToFixInMinutes());
			issueJSON.put("hint", isaIssue.getHint());
			issueJSON.put("name", isaIssue.getName());
			issueJSON.put("severity", isaIssue.getSeverity().getName().toLowerCase());
			issueJSON.put("type", isaIssue.getType().getName().toLowerCase());
			
			if(isaIssue.getOccurence() != null) {
				JSONObject occurenceJSON = new JSONObject();
				{
					occurenceJSON.put("startLine", isaIssue.getOccurence().getStartLine());
					occurenceJSON.put("endLine", isaIssue.getOccurence().getEndLine());
					occurenceJSON.put("startChar", isaIssue.getOccurence().getStartChar());
					occurenceJSON.put("endChar", isaIssue.getOccurence().getEndChar());
				}
				issueJSON.put("occurence", occurenceJSON);
			}
			
			issuesJSONs.put(issueJSON);
		}
		qualityJSON.put("issues", issuesJSONs);
		return qualityJSON;
	}
	
	
	
}





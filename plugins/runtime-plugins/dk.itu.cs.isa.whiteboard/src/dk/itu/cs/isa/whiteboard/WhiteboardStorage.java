package dk.itu.cs.isa.whiteboard;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import dk.itu.cs.isa.storage.ISAModelStorage;
import dk.itu.cs.isa.util.EcoreUtilISA;
import dk.itu.cs.isa.whiteboard.events.WhiteboardEventHandler;

public class WhiteboardStorage {
	
	public static interface WhiteboardStorageObserver {
		public void notifyOnSave();
	}

	public static WhiteboardStorage INSTANCE = new WhiteboardStorage();
	

	
	private static final String WHITEBOARD_FILE_EXTENSION = WhiteboardPackage.eNAME;
	private ResourceSet resourceSet;

	private static final String WHITEBOARD_FOLDER_NAME = "whiteboards";
	private File whiteboardFolder;

	private Map<String, WhiteboardHistory> whiteboardHistoryCache;

	private Map<String, Map<Integer, WhiteboardEventHandler>> operationHandlers;
	private Set<WhiteboardStorageObserver> observers;
	
	

	private WhiteboardStorage() {
		observers = new HashSet<>();
		clearCacheAndResetHistories();
		
		try {
			startUp();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void startUp() throws IOException {
		File rootFolder = ISAModelStorage.INSTANCE.getRootFolder();
		String whiteboardFolderPath = rootFolder.getAbsolutePath() + File.separator + WHITEBOARD_FOLDER_NAME;
		
		whiteboardFolder = new File(whiteboardFolderPath);
		if(!whiteboardFolder.isDirectory()) {
			if(!whiteboardFolder.mkdirs()) {
				throw new IOException("Failed to create whiteboard folder at \"" + whiteboardFolderPath + "\"");
			}
		}
	}
	
	public void clearCacheAndResetHistories() {
		whiteboardHistoryCache = new HashMap<>();
		operationHandlers = new HashMap<>();
		
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(WHITEBOARD_FILE_EXTENSION, new EcoreResourceFactoryImpl());
	}
	
	public void addObserver(WhiteboardStorageObserver newObs) {
		observers.add(newObs);
	}
	
	public void removeObserver(WhiteboardStorageObserver obs) {
		observers.remove(obs);
	}
	


	public WhiteboardEventHandler getWhiteboardOperationHandler(String systemName, int whiteboardId) {
		Map<Integer, WhiteboardEventHandler> handlers = operationHandlers.get(systemName);
		if(handlers == null) {
			handlers = new HashMap<>();
			operationHandlers.put(systemName, handlers);
		}

		if(!handlers.containsKey(whiteboardId))
			handlers.put(whiteboardId, new WhiteboardEventHandler(systemName, whiteboardId));
		return handlers.get(whiteboardId);
	}

	public void removeWhiteboardOperationHandler(String systemName, int whiteboardId) {
		Map<Integer, WhiteboardEventHandler> handlers = operationHandlers.get(systemName);
		if(handlers == null)
			return;
		handlers.remove(whiteboardId);
	}
	
	

	public void save(WhiteboardHistory history, boolean notifyObservers) throws IOException {
		history.eResource().save(Collections.EMPTY_MAP);
		
		if(notifyObservers)
			for(WhiteboardStorageObserver obs : observers)
				obs.notifyOnSave();
	}
	
	public void save(Whiteboard board, boolean notifyObservers) throws IOException {
		board.eResource().save(Collections.EMPTY_MAP);
		
		if(notifyObservers)
			for(WhiteboardStorageObserver obs : observers)
				obs.notifyOnSave();
	}
	
	
	
	public List<Whiteboard> getAllWhiteboards(String systemName) {
		WhiteboardHistory history = getWhiteboardHistory(systemName);
		return new ArrayList<>(history.getWhiteboards());
	}
	
	public WhiteboardHistory getWhiteboardHistory(String systemName) {
		if(systemName == null)
			throw new NullPointerException();
		
		if(whiteboardHistoryCache.containsKey(systemName))
			return whiteboardHistoryCache.get(systemName);

		Resource resource = EcoreUtilISA.loadResource(resourceSet, whiteboardFolder.getAbsolutePath() + File.separator + systemName + "." + WHITEBOARD_FILE_EXTENSION, true);
		if(resource.getContents().size() == 0 || resource.getContents().get(0) == null)
			resource.getContents().add(WhiteboardFactory.eINSTANCE.createWhiteboardHistory());
		
		WhiteboardHistory history = (WhiteboardHistory) resource.getContents().get(0);
		if(history.getSystemName() == null)
			history.setSystemName(systemName);
		
		if(WhiteboardHistoryUtil.cleanUpObsoleteEntries(history, 0)) {
			try {
				save(history, false);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		whiteboardHistoryCache.put(systemName, history);
		return history;
	}
	
	public Whiteboard getWhiteboardByID(String systemName, int id) {
		WhiteboardHistory history = getWhiteboardHistory(systemName);
		for(Whiteboard board : history.getWhiteboards()) {
			if(board.getId() == id)
				return board;
		}
		return null;
	}
	
	public Whiteboard getWhiteboardByIndex(String systemName, int index) {
		WhiteboardHistory history = getWhiteboardHistory(systemName);
		if(history.getWhiteboards().size() == 0)
			return null;
		
		int currentIndex = 0;
		for(Whiteboard board : history.getWhiteboards()) {
			if(currentIndex == index)
				return board;
			currentIndex++;
		}
		return null;
	}

	public int getNumberOfWhiteboardsForSystem(String systemName) {
		return getWhiteboardHistory(systemName).getWhiteboards().size();
	}
	
	private static int runningWhiteboardId = 1;
	public int findAvailableWhiteboardId(String systemName) {
		idLoop:
		while(true) {
			int id = runningWhiteboardId++;
			
			for(Whiteboard board : getAllWhiteboards(systemName))
				if(board.getId() == id)
					continue idLoop;
			
			return id;
		}
	}

	private static int runningPinId = 1;
	public int findAvailablePinId(String systemName) {
		idLoop:
		while(true) {
			int id = runningPinId++;
			
			for(Whiteboard board : getAllWhiteboards(systemName))
				for(Pin pin : board.getPins())
					if(pin.getId() == id)
						continue idLoop;
			
			return id;
		}
	}

	private static int runningPhotoId = 1;
	public int findAvailablePhotoId(String systemName) {
		idLoop:
		while(true) {
			int id = runningPhotoId++;
			
			for(Whiteboard board : getAllWhiteboards(systemName))
				for(Photo photo : board.getPhotos())
					if(photo.getId() == id)
						continue idLoop;
			
			return id;
		}
	}
	


//	public long getLatestOperationTimestampInCurrentlyOpenSystem() {
//		String systemName = ISACoalescenceService.INSTANCE.getCurrentlyActiveSystem();
//		if(systemName == null)
//			return 0;
//		return getLatestOperationTimestamp(systemName);
//	}

	public long getLatestOperationTimestamp(String systemName) {
		long result = 0;
		WhiteboardHistory history = getWhiteboardHistory(systemName);
		
		for(int i=0; i<history.getEventLog().size(); i++) {
			AbstractWhiteboardEvent e = history.getEventLog().get(i);
			if(e == null)
				continue;
			if(e.getTimestamp() > result)
				result = e.getTimestamp();
		}
		return result;
	}

}


















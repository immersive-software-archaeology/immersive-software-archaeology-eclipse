/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem;

import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Package Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer#getSubPackages <em>Sub Packages</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAPackageContainer()
 * @model abstract="true"
 * @generated
 */
public interface ISAPackageContainer extends ISANamedSoftwareElement {
	/**
	 * Returns the value of the '<em><b>Sub Packages</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage#getParentContainer <em>Parent Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Packages</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAPackageContainer_SubPackages()
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage#getParentContainer
	 * @model opposite="parentContainer" containment="true"
	 * @generated
	 */
	EList<ISAPackage> getSubPackages();

} // ISAPackageContainer

/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.PinSplitOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pin Split Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinSplitOperationImpl#getPinnedElementName <em>Pinned Element Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinSplitOperationImpl#getPinId <em>Pin Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PinSplitOperationImpl extends AbstractCompoundOperationImpl implements PinSplitOperation {
	/**
	 * The default value of the '{@link #getPinnedElementName() <em>Pinned Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinnedElementName()
	 * @generated
	 * @ordered
	 */
	protected static final String PINNED_ELEMENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPinnedElementName() <em>Pinned Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinnedElementName()
	 * @generated
	 * @ordered
	 */
	protected String pinnedElementName = PINNED_ELEMENT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPinId() <em>Pin Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinId()
	 * @generated
	 * @ordered
	 */
	protected static final int PIN_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPinId() <em>Pin Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinId()
	 * @generated
	 * @ordered
	 */
	protected int pinId = PIN_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PinSplitOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PIN_SPLIT_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPinnedElementName() {
		return pinnedElementName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPinnedElementName(String newPinnedElementName) {
		String oldPinnedElementName = pinnedElementName;
		pinnedElementName = newPinnedElementName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN_SPLIT_OPERATION__PINNED_ELEMENT_NAME, oldPinnedElementName, pinnedElementName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPinId() {
		return pinId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPinId(int newPinId) {
		int oldPinId = pinId;
		pinId = newPinId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN_SPLIT_OPERATION__PIN_ID, oldPinId, pinId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.PIN_SPLIT_OPERATION__PINNED_ELEMENT_NAME:
				return getPinnedElementName();
			case WhiteboardPackage.PIN_SPLIT_OPERATION__PIN_ID:
				return getPinId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.PIN_SPLIT_OPERATION__PINNED_ELEMENT_NAME:
				setPinnedElementName((String)newValue);
				return;
			case WhiteboardPackage.PIN_SPLIT_OPERATION__PIN_ID:
				setPinId((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PIN_SPLIT_OPERATION__PINNED_ELEMENT_NAME:
				setPinnedElementName(PINNED_ELEMENT_NAME_EDEFAULT);
				return;
			case WhiteboardPackage.PIN_SPLIT_OPERATION__PIN_ID:
				setPinId(PIN_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PIN_SPLIT_OPERATION__PINNED_ELEMENT_NAME:
				return PINNED_ELEMENT_NAME_EDEFAULT == null ? pinnedElementName != null : !PINNED_ELEMENT_NAME_EDEFAULT.equals(pinnedElementName);
			case WhiteboardPackage.PIN_SPLIT_OPERATION__PIN_ID:
				return pinId != PIN_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (pinnedElementName: ");
		result.append(pinnedElementName);
		result.append(", pinId: ");
		result.append(pinId);
		result.append(')');
		return result.toString();
	}

} //PinSplitOperationImpl

/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.ISAColor;
import dk.itu.cs.isa.whiteboard.ISAVector2;
import dk.itu.cs.isa.whiteboard.ISAVector3;
import dk.itu.cs.isa.whiteboard.Pin;
import dk.itu.cs.isa.whiteboard.Whiteboard;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pin</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinImpl#getId <em>Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinImpl#getElementQualifiedName <em>Element Qualified Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinImpl#getTextureSpacePosition <em>Texture Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinImpl#getLocalSpacePosition <em>Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinImpl#getColor <em>Color</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinImpl#getWhiteboard <em>Whiteboard</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PinImpl extends MinimalEObjectImpl.Container implements Pin {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getElementQualifiedName() <em>Element Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String ELEMENT_QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getElementQualifiedName() <em>Element Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected String elementQualifiedName = ELEMENT_QUALIFIED_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTextureSpacePosition() <em>Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTextureSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector2 textureSpacePosition;

	/**
	 * The cached value of the '{@link #getLocalSpacePosition() <em>Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 localSpacePosition;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected ISAColor color;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PinImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getElementQualifiedName() {
		return elementQualifiedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementQualifiedName(String newElementQualifiedName) {
		String oldElementQualifiedName = elementQualifiedName;
		elementQualifiedName = newElementQualifiedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN__ELEMENT_QUALIFIED_NAME, oldElementQualifiedName, elementQualifiedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector2 getTextureSpacePosition() {
		return textureSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTextureSpacePosition(ISAVector2 newTextureSpacePosition, NotificationChain msgs) {
		ISAVector2 oldTextureSpacePosition = textureSpacePosition;
		textureSpacePosition = newTextureSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN__TEXTURE_SPACE_POSITION, oldTextureSpacePosition, newTextureSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTextureSpacePosition(ISAVector2 newTextureSpacePosition) {
		if (newTextureSpacePosition != textureSpacePosition) {
			NotificationChain msgs = null;
			if (textureSpacePosition != null)
				msgs = ((InternalEObject)textureSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PIN__TEXTURE_SPACE_POSITION, null, msgs);
			if (newTextureSpacePosition != null)
				msgs = ((InternalEObject)newTextureSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PIN__TEXTURE_SPACE_POSITION, null, msgs);
			msgs = basicSetTextureSpacePosition(newTextureSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN__TEXTURE_SPACE_POSITION, newTextureSpacePosition, newTextureSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getLocalSpacePosition() {
		return localSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocalSpacePosition(ISAVector3 newLocalSpacePosition, NotificationChain msgs) {
		ISAVector3 oldLocalSpacePosition = localSpacePosition;
		localSpacePosition = newLocalSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN__LOCAL_SPACE_POSITION, oldLocalSpacePosition, newLocalSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalSpacePosition(ISAVector3 newLocalSpacePosition) {
		if (newLocalSpacePosition != localSpacePosition) {
			NotificationChain msgs = null;
			if (localSpacePosition != null)
				msgs = ((InternalEObject)localSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PIN__LOCAL_SPACE_POSITION, null, msgs);
			if (newLocalSpacePosition != null)
				msgs = ((InternalEObject)newLocalSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PIN__LOCAL_SPACE_POSITION, null, msgs);
			msgs = basicSetLocalSpacePosition(newLocalSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN__LOCAL_SPACE_POSITION, newLocalSpacePosition, newLocalSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAColor getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetColor(ISAColor newColor, NotificationChain msgs) {
		ISAColor oldColor = color;
		color = newColor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN__COLOR, oldColor, newColor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(ISAColor newColor) {
		if (newColor != color) {
			NotificationChain msgs = null;
			if (color != null)
				msgs = ((InternalEObject)color).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PIN__COLOR, null, msgs);
			if (newColor != null)
				msgs = ((InternalEObject)newColor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PIN__COLOR, null, msgs);
			msgs = basicSetColor(newColor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN__COLOR, newColor, newColor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Whiteboard getWhiteboard() {
		if (eContainerFeatureID() != WhiteboardPackage.PIN__WHITEBOARD) return null;
		return (Whiteboard)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWhiteboard(Whiteboard newWhiteboard, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newWhiteboard, WhiteboardPackage.PIN__WHITEBOARD, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWhiteboard(Whiteboard newWhiteboard) {
		if (newWhiteboard != eInternalContainer() || (eContainerFeatureID() != WhiteboardPackage.PIN__WHITEBOARD && newWhiteboard != null)) {
			if (EcoreUtil.isAncestor(this, newWhiteboard))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newWhiteboard != null)
				msgs = ((InternalEObject)newWhiteboard).eInverseAdd(this, WhiteboardPackage.WHITEBOARD__PINS, Whiteboard.class, msgs);
			msgs = basicSetWhiteboard(newWhiteboard, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN__WHITEBOARD, newWhiteboard, newWhiteboard));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.PIN__WHITEBOARD:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetWhiteboard((Whiteboard)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.PIN__TEXTURE_SPACE_POSITION:
				return basicSetTextureSpacePosition(null, msgs);
			case WhiteboardPackage.PIN__LOCAL_SPACE_POSITION:
				return basicSetLocalSpacePosition(null, msgs);
			case WhiteboardPackage.PIN__COLOR:
				return basicSetColor(null, msgs);
			case WhiteboardPackage.PIN__WHITEBOARD:
				return basicSetWhiteboard(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case WhiteboardPackage.PIN__WHITEBOARD:
				return eInternalContainer().eInverseRemove(this, WhiteboardPackage.WHITEBOARD__PINS, Whiteboard.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.PIN__ID:
				return getId();
			case WhiteboardPackage.PIN__ELEMENT_QUALIFIED_NAME:
				return getElementQualifiedName();
			case WhiteboardPackage.PIN__TEXTURE_SPACE_POSITION:
				return getTextureSpacePosition();
			case WhiteboardPackage.PIN__LOCAL_SPACE_POSITION:
				return getLocalSpacePosition();
			case WhiteboardPackage.PIN__COLOR:
				return getColor();
			case WhiteboardPackage.PIN__WHITEBOARD:
				return getWhiteboard();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.PIN__ID:
				setId((Integer)newValue);
				return;
			case WhiteboardPackage.PIN__ELEMENT_QUALIFIED_NAME:
				setElementQualifiedName((String)newValue);
				return;
			case WhiteboardPackage.PIN__TEXTURE_SPACE_POSITION:
				setTextureSpacePosition((ISAVector2)newValue);
				return;
			case WhiteboardPackage.PIN__LOCAL_SPACE_POSITION:
				setLocalSpacePosition((ISAVector3)newValue);
				return;
			case WhiteboardPackage.PIN__COLOR:
				setColor((ISAColor)newValue);
				return;
			case WhiteboardPackage.PIN__WHITEBOARD:
				setWhiteboard((Whiteboard)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PIN__ID:
				setId(ID_EDEFAULT);
				return;
			case WhiteboardPackage.PIN__ELEMENT_QUALIFIED_NAME:
				setElementQualifiedName(ELEMENT_QUALIFIED_NAME_EDEFAULT);
				return;
			case WhiteboardPackage.PIN__TEXTURE_SPACE_POSITION:
				setTextureSpacePosition((ISAVector2)null);
				return;
			case WhiteboardPackage.PIN__LOCAL_SPACE_POSITION:
				setLocalSpacePosition((ISAVector3)null);
				return;
			case WhiteboardPackage.PIN__COLOR:
				setColor((ISAColor)null);
				return;
			case WhiteboardPackage.PIN__WHITEBOARD:
				setWhiteboard((Whiteboard)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PIN__ID:
				return id != ID_EDEFAULT;
			case WhiteboardPackage.PIN__ELEMENT_QUALIFIED_NAME:
				return ELEMENT_QUALIFIED_NAME_EDEFAULT == null ? elementQualifiedName != null : !ELEMENT_QUALIFIED_NAME_EDEFAULT.equals(elementQualifiedName);
			case WhiteboardPackage.PIN__TEXTURE_SPACE_POSITION:
				return textureSpacePosition != null;
			case WhiteboardPackage.PIN__LOCAL_SPACE_POSITION:
				return localSpacePosition != null;
			case WhiteboardPackage.PIN__COLOR:
				return color != null;
			case WhiteboardPackage.PIN__WHITEBOARD:
				return getWhiteboard() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", elementQualifiedName: ");
		result.append(elementQualifiedName);
		result.append(')');
		return result.toString();
	}

} //PinImpl

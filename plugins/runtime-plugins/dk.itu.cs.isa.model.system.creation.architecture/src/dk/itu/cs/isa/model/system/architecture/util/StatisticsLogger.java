package dk.itu.cs.isa.model.system.architecture.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.preferences.ISAPreferences;



/**
 * Logs different statistics of the dendrogram cutting process using R.<br>
 * This is for debugging only, it will only produce files if the current workspace
 * contains a project called "ISAStatistics".
 * @param isaSystem
 * @param cuttingPoints
 * @param performanceIndices
 */
public class StatisticsLogger {
	
	private String folderName;
	private ISASoftwareSystemModel isaSystem;
	
	
	
	public StatisticsLogger(ISASoftwareSystemModel isaSystem) {
		this.isaSystem = isaSystem;
		folderName = String.valueOf(System.currentTimeMillis() + " - " + isaSystem.getName());
	}



	private int dendrogramCreationPDFsCreated = 0;
	public void logDendrogramCreationStats(String overallDuration, List<String> singleStepDurations) {
		String rCode = buildSingleGraphCode(isaSystem.getName() + " (overall time: "+overallDuration+")", "%PLACEHOLDER%",
				"Calculation Step", null,
				"Calculation Time [ms]", singleStepDurations);
		createStatisticFiles(isaSystem.getName().replaceAll(" ", "_") + "_-_dendrogram_creation_speed_"+(++dendrogramCreationPDFsCreated), rCode);
	}
	
	

	private int dendrogramCuttingPDFsCreated = 0;
	public void logDendrogramCuttingStats(double optimalCuttingPoint, List<Double> cuttingPointStats, List<Double> performanceIndexStats, List<Double> clusterNumberStats) {
		List<String> cuttingPointStrings = new ArrayList<>(cuttingPointStats.size());
		for(Double point : cuttingPointStats)
			cuttingPointStrings.add("\""+String.valueOf(point)+"\"");
		
		List<String> performanceIndexStrings = new ArrayList<>(performanceIndexStats.size());
		for(Double index : performanceIndexStats)
			performanceIndexStrings.add(String.valueOf(index));
		
		List<String> clusterNumberStrings = new ArrayList<>(clusterNumberStats.size());
		for(Double number : clusterNumberStats)
			clusterNumberStrings.add(String.valueOf(number));
		
		String rCode;

		rCode = buildMultiGraphCode(isaSystem.getName()+" - best point: "+optimalCuttingPoint, "%PLACEHOLDER%",
				"Dendrogram cutting point (= distance value)", cuttingPointStrings,
				"Number of clusters formed", clusterNumberStrings, "blue",
				"Performance index (the lower the better)", performanceIndexStrings, "red");
		createStatisticFiles(isaSystem.getName().replaceAll(" ", "_") + "_-_dendrogram_cutting_"+(++dendrogramCuttingPDFsCreated)+"_-_"+optimalCuttingPoint, rCode);
	}
	
	

	private int DBSCANPerformanceStatsCreated = 0;
	public void logDBSCANPerformanceStats(int minimumClusterSize, List<Double> epsilonValues, List<Integer> numbersOfUnclusteredClassifiersValues, List<Integer> clusterAmountValues) {
		List<String> epsilonStrings = new ArrayList<>(epsilonValues.size());
		for(Double point : epsilonValues)
			epsilonStrings.add("\""+String.valueOf(point)+"\"");
		
		List<String> numbersOfUnclusteredClassifiersStrings = new ArrayList<>(numbersOfUnclusteredClassifiersValues.size());
		for(Integer index : numbersOfUnclusteredClassifiersValues)
			numbersOfUnclusteredClassifiersStrings.add(String.valueOf(index));
		
		List<String> clusterAmountStrings = new ArrayList<>(clusterAmountValues.size());
		for(Integer amount : clusterAmountValues)
			clusterAmountStrings.add(String.valueOf(amount));
		
		String rCode;

		rCode = buildMultiGraphCode(isaSystem.getName()+" - DBSCAN Performance - Minimum Cluster Size = "+minimumClusterSize, "%PLACEHOLDER%",
				"DBSCAN epsilon value (= maximum distance between cluster points)", epsilonStrings,
				"Amount of unclustered classifiers (noise)", numbersOfUnclusteredClassifiersStrings, "blue",
				"Amount of identified clusters - minimum size = "+minimumClusterSize, clusterAmountStrings, "red");
		String number = (++DBSCANPerformanceStatsCreated < 10 ? "0" : "") + DBSCANPerformanceStatsCreated;
		createStatisticFiles("run-"+number+"_-_"+isaSystem.getName().replaceAll(" ", "_") + "_-_density_based_clusters_performance", rCode);
	}
	
	

	private int DBSCANPerformanceIndexStatsCreated = 0;
	public void logDBSCANPerformanceIndexStats(int minimumClusterSize, List<Double> epsilonValues, List<Double> performanceValues, double bestEpsilon) {
		List<String> epsilonStrings = new ArrayList<>(epsilonValues.size());
		for(Double point : epsilonValues)
			epsilonStrings.add("\""+String.valueOf(point)+"\"");
		
		List<String> performanceStrings = new ArrayList<>(performanceValues.size());
		for(Double amount : performanceValues)
			performanceStrings.add(String.valueOf(amount));
		
		String rCode;

		rCode = buildSingleGraphCode(isaSystem.getName()+" - DBSCAN Performance - Minimum Cluster Size = "+minimumClusterSize+" - best epsilon = "+bestEpsilon, "%PLACEHOLDER%",
				"DBSCAN epsilon value (= maximum distance between cluster points)", epsilonStrings,
				"Clustering Performance Index", performanceStrings);
		String number = (++DBSCANPerformanceIndexStatsCreated < 10 ? "0" : "") + DBSCANPerformanceIndexStatsCreated;
		createStatisticFiles("run-"+number+"_-_"+isaSystem.getName().replaceAll(" ", "_") + "_-_density_based_clusters_performance_index", rCode);
	}
	
	

	public void logOPTICSPerformanceStats(int minimumClusterSize, List<Double> reachabilityDistancesValues) {
		List<String> reachabilityDistancesStrings = new ArrayList<>(reachabilityDistancesValues.size());
		for(Double index : reachabilityDistancesValues)
			reachabilityDistancesStrings.add(String.valueOf(index));
		
		String rCode;

		rCode = buildSingleGraphCode(isaSystem.getName()+" - OPTICS Performance - Minimum Cluster Size = "+minimumClusterSize, "%PLACEHOLDER%",
				"Classifiers", null,
				"Reachability Distance", reachabilityDistancesStrings);
		createStatisticFiles(isaSystem.getName().replaceAll(" ", "_") + "_-_density_based_clusters_performance", rCode);
	}
	
	

	private void createStatisticFiles(String name, String rCode) {
		IProject statsProject = ResourcesPlugin.getWorkspace().getRoot().getProject("ISAStatistics");
		if(!statsProject.exists() || !statsProject.isOpen())
			return;

		String rootLocation = statsProject.getLocation().makeAbsolute().toOSString() + File.separator + folderName + File.separator;
		File rFile = new File(rootLocation);
		rFile.mkdir();
		
		String rFileLocation = rootLocation + name + ".R";
		String errorFileLocation = rootLocation + name + "-errorlog.txt";
		String pdfFileLocation = rootLocation + name + ".pdf";
		pdfFileLocation = pdfFileLocation.replaceAll("\\\\", "/");
		
		rCode = rCode.replaceAll("%PLACEHOLDER%", pdfFileLocation);
		
		try {
			writeToFile(rFileLocation, rCode, true);
		} catch (IOException e1) {
			e1.printStackTrace();
			return;
		}
		
		ProcessBuilder rProcessBuilder = new ProcessBuilder(ISAPreferences.getString(ISAPreferences.R_EXECUTABLE_PATH, null), rFileLocation);
		try {
			Process rProcess = rProcessBuilder.start();
			if(rProcess.waitFor() != 0) {
			    StringBuilder textBuilder = new StringBuilder();
				try (Reader reader = new BufferedReader(new InputStreamReader(rProcess.getErrorStream(),
						Charset.forName(StandardCharsets.UTF_8.name())))) {
					int c = 0;
					while ((c = reader.read()) != -1) {
						textBuilder.append((char) c);
					}
				}
				writeToFile(errorFileLocation, textBuilder.toString(), true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			statsProject.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	
	
	private String buildSingleGraphCode(String name, String pdfTargetLocation,
			String xAxisLabel, List<String> xAxisValues,
			String yAxisLabel, List<String> yAxisValues) {
		
		String result =
				"pdf(file=\"" + pdfTargetLocation + "\", height=10, width=15)\r\n"
				+ "## set up data\r\n";
		
		if(xAxisValues != null)
			result += "xValues <- c("+ String.join(",", xAxisValues) +")\r\n";
		
		result +=
				"yValues <- c(" + String.join(", ", yAxisValues) + ")\r\n"
				+ "g_range <- range(0, yValues)\r\n";

		if(xAxisValues != null)
			result += "plot(xValues, yValues, type=\"l\", col=\"blue\", axes=FALSE, ann=FALSE)\r\n";
		else
			result += "plot(yValues, type=\"l\", col=\"blue\", axes=FALSE, ann=FALSE)\r\n";
		
		result +=
				"axis(1)\r\n"
				+ "axis(2, las=1)\r\n"
				+ "box()\r\n"
				+ "title(main=\"" + name + "\", col.main=\"red\", font.main=4)\r\n"
				+ "title(xlab=\""+ xAxisLabel +"\", col.lab=rgb(0,0,0))\r\n"
				+ "title(ylab=\""+ yAxisLabel +"\", col.lab=rgb(0,0,0))\r\n"
				+ "dev.off()";
		
		return result;
	}



	public String buildMultiGraphCode(String name, String pdfTargetLocation,
			String xAxisLabel, List<String> xAxisValues,
			String yAxisLabelLeft, List<String> yAxisValuesLeft, String colorLeft,
			String yAxisLabelRight, List<String> yAxisValuesRight, String colorRight) {
		return "pdf(file=\"" + pdfTargetLocation + "\", height=10, width=15)\r\n"
				+ "## set up data\r\n"
				+ "xAxis <- c("+ String.join(",", xAxisValues) +")\r\n"
				+ "\r\n"
				+ "yAxisLeft <- c("+ String.join(",", yAxisValuesLeft) +")\r\n"
				+ "yAxisLeft_range <- range(0, yAxisLeft)\r\n"
				+ "\r\n"
				+ "yAxisRight <- c("+ String.join(",", yAxisValuesRight) +")\r\n"
				+ "yAxisRight_range <- range(0, yAxisRight)\r\n"
				+ "\r\n"
				+ "\r\n"
				+ "## add extra space to right margin of plot within frame\r\n"
				+ "par(mar=c(5, 5, 5, 5) + 0.1)\r\n"
				+ "\r\n"
				+ "## Plot first set of data and draw its axis\r\n"
				+ "plot(xAxis, yAxisLeft, pch=16, axes=FALSE, #ylim=c(0,1),\r\n"
				+ "   xlab=\"\", ylab=\"\", \r\n"
				+ "   type=\"l\",col=\""+ colorLeft +"\")\r\n"
				+ "axis(2, col=\""+ colorLeft +"\", col.axis=\""+ colorLeft +"\", las=1)  ## las=1 makes horizontal labels\r\n"
				+ "mtext(\""+ yAxisLabelLeft +"\",side=2,col=\""+ colorLeft +"\",line=4)\r\n"
				+ "\r\n"
				+ "## Allow a second plot on the same graph\r\n"
				+ "par(new=TRUE)\r\n"
				+ "\r\n"
				+ "## Plot the second plot and put axis scale on right\r\n"
				+ "plot(xAxis, yAxisRight, pch=15,  xlab=\"\", ylab=\"\", #ylim=c(0,7000), \r\n"
				+ "    axes=FALSE, type=\"l\", col=\""+ colorRight +"\")\r\n"
				+ "## a little farther out (line=4) to make room for labels\r\n"
				+ "mtext(\""+ yAxisLabelRight +"\",side=4,col=\""+ colorRight +"\",line=4) \r\n"
				+ "axis(4, col=\""+ colorRight +"\",col.axis=\""+ colorRight +"\",las=1)\r\n"
				+ "\r\n"
				+ "## Draw the time axis\r\n"
				+ "#axis(1,pretty(range(time),10))\r\n"
				+ "axis(1)\r\n"
				+ "mtext(\""+ xAxisLabel +"\",side=1,col=\"black\",line=2.5)  \r\n"
				+ "\r\n"
				+ "box()\r\n"
				+ "\r\n"
				+ "title(main=\""+name+"\", col.main=\"black\", font.main=4)\r\n"
				+ "\r\n"
				+ "## Add Legend\r\n"
				+ "#legend(\"topleft\",legend=c(\"Beta Gal\",\"Cell Density\"),\r\n"
				+ "#  text.col=c(\""+ colorLeft +"\",\""+ colorRight +"\"),pch=c(16,15),col=c(\""+ colorLeft +"\",\""+ colorRight +"\"))"
				+ "dev.off()";
	}
	
	
	
	private void writeToFile(String fileLocation, String content, boolean overwrite) throws IOException {
		File rFile = new File(fileLocation);
		if (!rFile.createNewFile()) {
			if(!overwrite)
				return;
		}
		FileWriter myWriter = new FileWriter(rFile);
		myWriter.write(content);
		myWriter.close();
	}
	
	

}

















/**
 */
package dk.itu.cs.isa.model.system.quality.impl;

import dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry;
import dk.itu.cs.isa.model.system.quality.ISAFilePassage;
import dk.itu.cs.isa.model.system.quality.ISAIssue;
import dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity;
import dk.itu.cs.isa.model.system.quality.ISAQualityIssueType;
import dk.itu.cs.isa.model.system.quality.QualityPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Issue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl#getParentEntry <em>Parent Entry</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl#getName <em>Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl#getOccurence <em>Occurence</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl#getSeverity <em>Severity</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl#getHint <em>Hint</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl#getEffortToFixInMinutes <em>Effort To Fix In Minutes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAIssueImpl extends MinimalEObjectImpl.Container implements ISAIssue {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final ISAQualityIssueType TYPE_EDEFAULT = ISAQualityIssueType.CODE_SMELL;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ISAQualityIssueType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOccurence() <em>Occurence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOccurence()
	 * @generated
	 * @ordered
	 */
	protected ISAFilePassage occurence;

	/**
	 * The default value of the '{@link #getSeverity() <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeverity()
	 * @generated
	 * @ordered
	 */
	protected static final ISAQualityIssueSeverity SEVERITY_EDEFAULT = ISAQualityIssueSeverity.MINOR;

	/**
	 * The cached value of the '{@link #getSeverity() <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeverity()
	 * @generated
	 * @ordered
	 */
	protected ISAQualityIssueSeverity severity = SEVERITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getHint() <em>Hint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHint()
	 * @generated
	 * @ordered
	 */
	protected static final String HINT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHint() <em>Hint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHint()
	 * @generated
	 * @ordered
	 */
	protected String hint = HINT_EDEFAULT;

	/**
	 * The default value of the '{@link #getEffortToFixInMinutes() <em>Effort To Fix In Minutes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEffortToFixInMinutes()
	 * @generated
	 * @ordered
	 */
	protected static final int EFFORT_TO_FIX_IN_MINUTES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEffortToFixInMinutes() <em>Effort To Fix In Minutes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEffortToFixInMinutes()
	 * @generated
	 * @ordered
	 */
	protected int effortToFixInMinutes = EFFORT_TO_FIX_IN_MINUTES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAIssueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QualityPackage.Literals.ISA_ISSUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAClassifierQualityEntry getParentEntry() {
		if (eContainerFeatureID() != QualityPackage.ISA_ISSUE__PARENT_ENTRY) return null;
		return (ISAClassifierQualityEntry)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentEntry(ISAClassifierQualityEntry newParentEntry, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentEntry, QualityPackage.ISA_ISSUE__PARENT_ENTRY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentEntry(ISAClassifierQualityEntry newParentEntry) {
		if (newParentEntry != eInternalContainer() || (eContainerFeatureID() != QualityPackage.ISA_ISSUE__PARENT_ENTRY && newParentEntry != null)) {
			if (EcoreUtil.isAncestor(this, newParentEntry))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentEntry != null)
				msgs = ((InternalEObject)newParentEntry).eInverseAdd(this, QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES, ISAClassifierQualityEntry.class, msgs);
			msgs = basicSetParentEntry(newParentEntry, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_ISSUE__PARENT_ENTRY, newParentEntry, newParentEntry));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAQualityIssueType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(ISAQualityIssueType newType) {
		ISAQualityIssueType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_ISSUE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_ISSUE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAFilePassage getOccurence() {
		return occurence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOccurence(ISAFilePassage newOccurence, NotificationChain msgs) {
		ISAFilePassage oldOccurence = occurence;
		occurence = newOccurence;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_ISSUE__OCCURENCE, oldOccurence, newOccurence);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOccurence(ISAFilePassage newOccurence) {
		if (newOccurence != occurence) {
			NotificationChain msgs = null;
			if (occurence != null)
				msgs = ((InternalEObject)occurence).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - QualityPackage.ISA_ISSUE__OCCURENCE, null, msgs);
			if (newOccurence != null)
				msgs = ((InternalEObject)newOccurence).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - QualityPackage.ISA_ISSUE__OCCURENCE, null, msgs);
			msgs = basicSetOccurence(newOccurence, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_ISSUE__OCCURENCE, newOccurence, newOccurence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAQualityIssueSeverity getSeverity() {
		return severity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSeverity(ISAQualityIssueSeverity newSeverity) {
		ISAQualityIssueSeverity oldSeverity = severity;
		severity = newSeverity == null ? SEVERITY_EDEFAULT : newSeverity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_ISSUE__SEVERITY, oldSeverity, severity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getHint() {
		return hint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHint(String newHint) {
		String oldHint = hint;
		hint = newHint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_ISSUE__HINT, oldHint, hint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getEffortToFixInMinutes() {
		return effortToFixInMinutes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEffortToFixInMinutes(int newEffortToFixInMinutes) {
		int oldEffortToFixInMinutes = effortToFixInMinutes;
		effortToFixInMinutes = newEffortToFixInMinutes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_ISSUE__EFFORT_TO_FIX_IN_MINUTES, oldEffortToFixInMinutes, effortToFixInMinutes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QualityPackage.ISA_ISSUE__PARENT_ENTRY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentEntry((ISAClassifierQualityEntry)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QualityPackage.ISA_ISSUE__PARENT_ENTRY:
				return basicSetParentEntry(null, msgs);
			case QualityPackage.ISA_ISSUE__OCCURENCE:
				return basicSetOccurence(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case QualityPackage.ISA_ISSUE__PARENT_ENTRY:
				return eInternalContainer().eInverseRemove(this, QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES, ISAClassifierQualityEntry.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QualityPackage.ISA_ISSUE__PARENT_ENTRY:
				return getParentEntry();
			case QualityPackage.ISA_ISSUE__TYPE:
				return getType();
			case QualityPackage.ISA_ISSUE__NAME:
				return getName();
			case QualityPackage.ISA_ISSUE__OCCURENCE:
				return getOccurence();
			case QualityPackage.ISA_ISSUE__SEVERITY:
				return getSeverity();
			case QualityPackage.ISA_ISSUE__HINT:
				return getHint();
			case QualityPackage.ISA_ISSUE__EFFORT_TO_FIX_IN_MINUTES:
				return getEffortToFixInMinutes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QualityPackage.ISA_ISSUE__PARENT_ENTRY:
				setParentEntry((ISAClassifierQualityEntry)newValue);
				return;
			case QualityPackage.ISA_ISSUE__TYPE:
				setType((ISAQualityIssueType)newValue);
				return;
			case QualityPackage.ISA_ISSUE__NAME:
				setName((String)newValue);
				return;
			case QualityPackage.ISA_ISSUE__OCCURENCE:
				setOccurence((ISAFilePassage)newValue);
				return;
			case QualityPackage.ISA_ISSUE__SEVERITY:
				setSeverity((ISAQualityIssueSeverity)newValue);
				return;
			case QualityPackage.ISA_ISSUE__HINT:
				setHint((String)newValue);
				return;
			case QualityPackage.ISA_ISSUE__EFFORT_TO_FIX_IN_MINUTES:
				setEffortToFixInMinutes((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_ISSUE__PARENT_ENTRY:
				setParentEntry((ISAClassifierQualityEntry)null);
				return;
			case QualityPackage.ISA_ISSUE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case QualityPackage.ISA_ISSUE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case QualityPackage.ISA_ISSUE__OCCURENCE:
				setOccurence((ISAFilePassage)null);
				return;
			case QualityPackage.ISA_ISSUE__SEVERITY:
				setSeverity(SEVERITY_EDEFAULT);
				return;
			case QualityPackage.ISA_ISSUE__HINT:
				setHint(HINT_EDEFAULT);
				return;
			case QualityPackage.ISA_ISSUE__EFFORT_TO_FIX_IN_MINUTES:
				setEffortToFixInMinutes(EFFORT_TO_FIX_IN_MINUTES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_ISSUE__PARENT_ENTRY:
				return getParentEntry() != null;
			case QualityPackage.ISA_ISSUE__TYPE:
				return type != TYPE_EDEFAULT;
			case QualityPackage.ISA_ISSUE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case QualityPackage.ISA_ISSUE__OCCURENCE:
				return occurence != null;
			case QualityPackage.ISA_ISSUE__SEVERITY:
				return severity != SEVERITY_EDEFAULT;
			case QualityPackage.ISA_ISSUE__HINT:
				return HINT_EDEFAULT == null ? hint != null : !HINT_EDEFAULT.equals(hint);
			case QualityPackage.ISA_ISSUE__EFFORT_TO_FIX_IN_MINUTES:
				return effortToFixInMinutes != EFFORT_TO_FIX_IN_MINUTES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", name: ");
		result.append(name);
		result.append(", severity: ");
		result.append(severity);
		result.append(", hint: ");
		result.append(hint);
		result.append(", effortToFixInMinutes: ");
		result.append(effortToFixInMinutes);
		result.append(')');
		return result.toString();
	}

} //ISAIssueImpl

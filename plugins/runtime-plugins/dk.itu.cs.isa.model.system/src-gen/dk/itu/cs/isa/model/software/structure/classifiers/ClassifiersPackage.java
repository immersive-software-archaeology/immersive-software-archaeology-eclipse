/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersFactory
 * @model kind="package"
 * @generated
 */
public interface ClassifiersPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "classifiers";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/software/structure/classifiers";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "classifiers";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassifiersPackage eINSTANCE = dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierContainerImpl <em>ISA Classifier Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierContainerImpl
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAClassifierContainer()
	 * @generated
	 */
	int ISA_CLASSIFIER_CONTAINER = 0;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_CONTAINER__CLASSIFIERS = 0;

	/**
	 * The number of structural features of the '<em>ISA Classifier Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>ISA Classifier Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl <em>ISA Classifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAClassifier()
	 * @generated
	 */
	int ISA_CLASSIFIER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__NAME = MiscPackage.ISA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__INCOMING_REFERENCES = MiscPackage.ISA_TYPE__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__USAGES_AS_TYPE = MiscPackage.ISA_TYPE__USAGES_AS_TYPE;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__CLASSIFIERS = MiscPackage.ISA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__MODIFIERS = MiscPackage.ISA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION = MiscPackage.ISA_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT = MiscPackage.ISA_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT = MiscPackage.ISA_TYPE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__PARENT = MiscPackage.ISA_TYPE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__FIELDS = MiscPackage.ISA_TYPE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__METHODS = MiscPackage.ISA_TYPE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER__FILE_PATH = MiscPackage.ISA_TYPE_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>ISA Classifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_FEATURE_COUNT = MiscPackage.ISA_TYPE_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER___GET_FULLY_QUALIFIED_NAME = MiscPackage.ISA_TYPE___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER___TO_STRING = MiscPackage.ISA_TYPE___TO_STRING;

	/**
	 * The operation id for the '<em>Get Containing Package</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER___GET_CONTAINING_PACKAGE = MiscPackage.ISA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Contained Members</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER___GET_CONTAINED_MEMBERS = MiscPackage.ISA_TYPE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>ISA Classifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_OPERATION_COUNT = MiscPackage.ISA_TYPE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassImpl <em>ISA Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassImpl
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAClass()
	 * @generated
	 */
	int ISA_CLASS = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__NAME = ISA_CLASSIFIER__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__INCOMING_REFERENCES = ISA_CLASSIFIER__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__USAGES_AS_TYPE = ISA_CLASSIFIER__USAGES_AS_TYPE;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__CLASSIFIERS = ISA_CLASSIFIER__CLASSIFIERS;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__MODIFIERS = ISA_CLASSIFIER__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__NUMBER_IN_ORDER_OF_DECLARATION = ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__START_POSITION_IN_DOCUMENT = ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__END_POSITION_IN_DOCUMENT = ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__PARENT = ISA_CLASSIFIER__PARENT;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__FIELDS = ISA_CLASSIFIER__FIELDS;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__METHODS = ISA_CLASSIFIER__METHODS;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__FILE_PATH = ISA_CLASSIFIER__FILE_PATH;

	/**
	 * The feature id for the '<em><b>Constructors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__CONSTRUCTORS = ISA_CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Extended Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__EXTENDED_CLASS = ISA_CLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Implemented Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__IMPLEMENTED_INTERFACES = ISA_CLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Usages As Extended Class</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS__USAGES_AS_EXTENDED_CLASS = ISA_CLASSIFIER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>ISA Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS_FEATURE_COUNT = ISA_CLASSIFIER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS___GET_FULLY_QUALIFIED_NAME = ISA_CLASSIFIER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS___TO_STRING = ISA_CLASSIFIER___TO_STRING;

	/**
	 * The operation id for the '<em>Get Containing Package</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS___GET_CONTAINING_PACKAGE = ISA_CLASSIFIER___GET_CONTAINING_PACKAGE;

	/**
	 * The operation id for the '<em>Get Contained Members</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS___GET_CONTAINED_MEMBERS = ISA_CLASSIFIER___GET_CONTAINED_MEMBERS;

	/**
	 * The number of operations of the '<em>ISA Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASS_OPERATION_COUNT = ISA_CLASSIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAAbstractClassImpl <em>ISA Abstract Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAAbstractClassImpl
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAAbstractClass()
	 * @generated
	 */
	int ISA_ABSTRACT_CLASS = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__NAME = ISA_CLASS__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__INCOMING_REFERENCES = ISA_CLASS__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__USAGES_AS_TYPE = ISA_CLASS__USAGES_AS_TYPE;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__CLASSIFIERS = ISA_CLASS__CLASSIFIERS;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__MODIFIERS = ISA_CLASS__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__NUMBER_IN_ORDER_OF_DECLARATION = ISA_CLASS__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__START_POSITION_IN_DOCUMENT = ISA_CLASS__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__END_POSITION_IN_DOCUMENT = ISA_CLASS__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__PARENT = ISA_CLASS__PARENT;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__FIELDS = ISA_CLASS__FIELDS;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__METHODS = ISA_CLASS__METHODS;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__FILE_PATH = ISA_CLASS__FILE_PATH;

	/**
	 * The feature id for the '<em><b>Constructors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__CONSTRUCTORS = ISA_CLASS__CONSTRUCTORS;

	/**
	 * The feature id for the '<em><b>Extended Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__EXTENDED_CLASS = ISA_CLASS__EXTENDED_CLASS;

	/**
	 * The feature id for the '<em><b>Implemented Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__IMPLEMENTED_INTERFACES = ISA_CLASS__IMPLEMENTED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Usages As Extended Class</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS__USAGES_AS_EXTENDED_CLASS = ISA_CLASS__USAGES_AS_EXTENDED_CLASS;

	/**
	 * The number of structural features of the '<em>ISA Abstract Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS_FEATURE_COUNT = ISA_CLASS_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS___GET_FULLY_QUALIFIED_NAME = ISA_CLASS___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS___TO_STRING = ISA_CLASS___TO_STRING;

	/**
	 * The operation id for the '<em>Get Containing Package</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS___GET_CONTAINING_PACKAGE = ISA_CLASS___GET_CONTAINING_PACKAGE;

	/**
	 * The operation id for the '<em>Get Contained Members</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS___GET_CONTAINED_MEMBERS = ISA_CLASS___GET_CONTAINED_MEMBERS;

	/**
	 * The number of operations of the '<em>ISA Abstract Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_CLASS_OPERATION_COUNT = ISA_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAConcreteClassImpl <em>ISA Concrete Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAConcreteClassImpl
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAConcreteClass()
	 * @generated
	 */
	int ISA_CONCRETE_CLASS = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__NAME = ISA_CLASS__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__INCOMING_REFERENCES = ISA_CLASS__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__USAGES_AS_TYPE = ISA_CLASS__USAGES_AS_TYPE;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__CLASSIFIERS = ISA_CLASS__CLASSIFIERS;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__MODIFIERS = ISA_CLASS__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__NUMBER_IN_ORDER_OF_DECLARATION = ISA_CLASS__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__START_POSITION_IN_DOCUMENT = ISA_CLASS__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__END_POSITION_IN_DOCUMENT = ISA_CLASS__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__PARENT = ISA_CLASS__PARENT;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__FIELDS = ISA_CLASS__FIELDS;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__METHODS = ISA_CLASS__METHODS;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__FILE_PATH = ISA_CLASS__FILE_PATH;

	/**
	 * The feature id for the '<em><b>Constructors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__CONSTRUCTORS = ISA_CLASS__CONSTRUCTORS;

	/**
	 * The feature id for the '<em><b>Extended Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__EXTENDED_CLASS = ISA_CLASS__EXTENDED_CLASS;

	/**
	 * The feature id for the '<em><b>Implemented Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__IMPLEMENTED_INTERFACES = ISA_CLASS__IMPLEMENTED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Usages As Extended Class</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS__USAGES_AS_EXTENDED_CLASS = ISA_CLASS__USAGES_AS_EXTENDED_CLASS;

	/**
	 * The number of structural features of the '<em>ISA Concrete Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS_FEATURE_COUNT = ISA_CLASS_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS___GET_FULLY_QUALIFIED_NAME = ISA_CLASS___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS___TO_STRING = ISA_CLASS___TO_STRING;

	/**
	 * The operation id for the '<em>Get Containing Package</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS___GET_CONTAINING_PACKAGE = ISA_CLASS___GET_CONTAINING_PACKAGE;

	/**
	 * The operation id for the '<em>Get Contained Members</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS___GET_CONTAINED_MEMBERS = ISA_CLASS___GET_CONTAINED_MEMBERS;

	/**
	 * The number of operations of the '<em>ISA Concrete Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_CLASS_OPERATION_COUNT = ISA_CLASS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceImpl <em>ISA Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceImpl
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAInterface()
	 * @generated
	 */
	int ISA_INTERFACE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__NAME = ISA_CLASSIFIER__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__INCOMING_REFERENCES = ISA_CLASSIFIER__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__USAGES_AS_TYPE = ISA_CLASSIFIER__USAGES_AS_TYPE;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__CLASSIFIERS = ISA_CLASSIFIER__CLASSIFIERS;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__MODIFIERS = ISA_CLASSIFIER__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__NUMBER_IN_ORDER_OF_DECLARATION = ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__START_POSITION_IN_DOCUMENT = ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__END_POSITION_IN_DOCUMENT = ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__PARENT = ISA_CLASSIFIER__PARENT;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__FIELDS = ISA_CLASSIFIER__FIELDS;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__METHODS = ISA_CLASSIFIER__METHODS;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__FILE_PATH = ISA_CLASSIFIER__FILE_PATH;

	/**
	 * The feature id for the '<em><b>Extended Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__EXTENDED_INTERFACES = ISA_CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Usages As Implemented Interface</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE = ISA_CLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Usages As Extended Interface</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE = ISA_CLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ISA Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE_FEATURE_COUNT = ISA_CLASSIFIER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE___GET_FULLY_QUALIFIED_NAME = ISA_CLASSIFIER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE___TO_STRING = ISA_CLASSIFIER___TO_STRING;

	/**
	 * The operation id for the '<em>Get Containing Package</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE___GET_CONTAINING_PACKAGE = ISA_CLASSIFIER___GET_CONTAINING_PACKAGE;

	/**
	 * The operation id for the '<em>Get Contained Members</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE___GET_CONTAINED_MEMBERS = ISA_CLASSIFIER___GET_CONTAINED_MEMBERS;

	/**
	 * The number of operations of the '<em>ISA Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE_OPERATION_COUNT = ISA_CLASSIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAEnumImpl <em>ISA Enum</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAEnumImpl
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAEnum()
	 * @generated
	 */
	int ISA_ENUM = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__NAME = ISA_CLASSIFIER__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__INCOMING_REFERENCES = ISA_CLASSIFIER__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__USAGES_AS_TYPE = ISA_CLASSIFIER__USAGES_AS_TYPE;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__CLASSIFIERS = ISA_CLASSIFIER__CLASSIFIERS;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__MODIFIERS = ISA_CLASSIFIER__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__NUMBER_IN_ORDER_OF_DECLARATION = ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__START_POSITION_IN_DOCUMENT = ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__END_POSITION_IN_DOCUMENT = ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__PARENT = ISA_CLASSIFIER__PARENT;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__FIELDS = ISA_CLASSIFIER__FIELDS;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__METHODS = ISA_CLASSIFIER__METHODS;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__FILE_PATH = ISA_CLASSIFIER__FILE_PATH;

	/**
	 * The feature id for the '<em><b>Constructors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__CONSTRUCTORS = ISA_CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constants</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM__CONSTANTS = ISA_CLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>ISA Enum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_FEATURE_COUNT = ISA_CLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM___GET_FULLY_QUALIFIED_NAME = ISA_CLASSIFIER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM___TO_STRING = ISA_CLASSIFIER___TO_STRING;

	/**
	 * The operation id for the '<em>Get Containing Package</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM___GET_CONTAINING_PACKAGE = ISA_CLASSIFIER___GET_CONTAINING_PACKAGE;

	/**
	 * The operation id for the '<em>Get Contained Members</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM___GET_CONTAINED_MEMBERS = ISA_CLASSIFIER___GET_CONTAINED_MEMBERS;

	/**
	 * The number of operations of the '<em>ISA Enum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_OPERATION_COUNT = ISA_CLASSIFIER_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISARecordImpl <em>ISA Record</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISARecordImpl
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISARecord()
	 * @generated
	 */
	int ISA_RECORD = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__NAME = ISA_CLASSIFIER__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__INCOMING_REFERENCES = ISA_CLASSIFIER__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__USAGES_AS_TYPE = ISA_CLASSIFIER__USAGES_AS_TYPE;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__CLASSIFIERS = ISA_CLASSIFIER__CLASSIFIERS;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__MODIFIERS = ISA_CLASSIFIER__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__NUMBER_IN_ORDER_OF_DECLARATION = ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__START_POSITION_IN_DOCUMENT = ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__END_POSITION_IN_DOCUMENT = ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__PARENT = ISA_CLASSIFIER__PARENT;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__FIELDS = ISA_CLASSIFIER__FIELDS;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__METHODS = ISA_CLASSIFIER__METHODS;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__FILE_PATH = ISA_CLASSIFIER__FILE_PATH;

	/**
	 * The feature id for the '<em><b>Constructors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__CONSTRUCTORS = ISA_CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Extended Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__EXTENDED_INTERFACES = ISA_CLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD__COMPONENTS = ISA_CLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ISA Record</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_FEATURE_COUNT = ISA_CLASSIFIER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD___GET_FULLY_QUALIFIED_NAME = ISA_CLASSIFIER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD___TO_STRING = ISA_CLASSIFIER___TO_STRING;

	/**
	 * The operation id for the '<em>Get Containing Package</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD___GET_CONTAINING_PACKAGE = ISA_CLASSIFIER___GET_CONTAINING_PACKAGE;

	/**
	 * The operation id for the '<em>Get Contained Members</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD___GET_CONTAINED_MEMBERS = ISA_CLASSIFIER___GET_CONTAINED_MEMBERS;

	/**
	 * The number of operations of the '<em>ISA Record</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_OPERATION_COUNT = ISA_CLASSIFIER_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceExtendingElementImpl <em>ISA Interface Extending Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceExtendingElementImpl
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAInterfaceExtendingElement()
	 * @generated
	 */
	int ISA_INTERFACE_EXTENDING_ELEMENT = 8;

	/**
	 * The feature id for the '<em><b>Extended Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES = 0;

	/**
	 * The number of structural features of the '<em>ISA Interface Extending Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE_EXTENDING_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>ISA Interface Extending Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_INTERFACE_EXTENDING_ELEMENT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer <em>ISA Classifier Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Classifier Container</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer
	 * @generated
	 */
	EClass getISAClassifierContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer#getClassifiers <em>Classifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classifiers</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer#getClassifiers()
	 * @see #getISAClassifierContainer()
	 * @generated
	 */
	EReference getISAClassifierContainer_Classifiers();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier <em>ISA Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Classifier</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier
	 * @generated
	 */
	EClass getISAClassifier();

	/**
	 * Returns the meta object for the container reference '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getParent()
	 * @see #getISAClassifier()
	 * @generated
	 */
	EReference getISAClassifier_Parent();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getFields()
	 * @see #getISAClassifier()
	 * @generated
	 */
	EReference getISAClassifier_Fields();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getMethods <em>Methods</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Methods</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getMethods()
	 * @see #getISAClassifier()
	 * @generated
	 */
	EReference getISAClassifier_Methods();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getFilePath <em>File Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>File Path</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getFilePath()
	 * @see #getISAClassifier()
	 * @generated
	 */
	EAttribute getISAClassifier_FilePath();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getContainingPackage() <em>Get Containing Package</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Containing Package</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getContainingPackage()
	 * @generated
	 */
	EOperation getISAClassifier__GetContainingPackage();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getContainedMembers() <em>Get Contained Members</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Contained Members</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getContainedMembers()
	 * @generated
	 */
	EOperation getISAClassifier__GetContainedMembers();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass <em>ISA Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Class</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClass
	 * @generated
	 */
	EClass getISAClass();

	/**
	 * Returns the meta object for the reference '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getExtendedClass <em>Extended Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Extended Class</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getExtendedClass()
	 * @see #getISAClass()
	 * @generated
	 */
	EReference getISAClass_ExtendedClass();

	/**
	 * Returns the meta object for the reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getImplementedInterfaces <em>Implemented Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Implemented Interfaces</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getImplementedInterfaces()
	 * @see #getISAClass()
	 * @generated
	 */
	EReference getISAClass_ImplementedInterfaces();

	/**
	 * Returns the meta object for the reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getUsagesAsExtendedClass <em>Usages As Extended Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Usages As Extended Class</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getUsagesAsExtendedClass()
	 * @see #getISAClass()
	 * @generated
	 */
	EReference getISAClass_UsagesAsExtendedClass();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAAbstractClass <em>ISA Abstract Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Abstract Class</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAAbstractClass
	 * @generated
	 */
	EClass getISAAbstractClass();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAConcreteClass <em>ISA Concrete Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Concrete Class</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAConcreteClass
	 * @generated
	 */
	EClass getISAConcreteClass();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface <em>ISA Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Interface</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface
	 * @generated
	 */
	EClass getISAInterface();

	/**
	 * Returns the meta object for the reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsImplementedInterface <em>Usages As Implemented Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Usages As Implemented Interface</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsImplementedInterface()
	 * @see #getISAInterface()
	 * @generated
	 */
	EReference getISAInterface_UsagesAsImplementedInterface();

	/**
	 * Returns the meta object for the reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsExtendedInterface <em>Usages As Extended Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Usages As Extended Interface</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsExtendedInterface()
	 * @see #getISAInterface()
	 * @generated
	 */
	EReference getISAInterface_UsagesAsExtendedInterface();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum <em>ISA Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Enum</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum
	 * @generated
	 */
	EClass getISAEnum();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum#getConstants <em>Constants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constants</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum#getConstants()
	 * @see #getISAEnum()
	 * @generated
	 */
	EReference getISAEnum_Constants();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISARecord <em>ISA Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Record</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISARecord
	 * @generated
	 */
	EClass getISARecord();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISARecord#getComponents <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Components</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISARecord#getComponents()
	 * @see #getISARecord()
	 * @generated
	 */
	EReference getISARecord_Components();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement <em>ISA Interface Extending Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Interface Extending Element</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement
	 * @generated
	 */
	EClass getISAInterfaceExtendingElement();

	/**
	 * Returns the meta object for the reference list '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement#getExtendedInterfaces <em>Extended Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extended Interfaces</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement#getExtendedInterfaces()
	 * @see #getISAInterfaceExtendingElement()
	 * @generated
	 */
	EReference getISAInterfaceExtendingElement_ExtendedInterfaces();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClassifiersFactory getClassifiersFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierContainerImpl <em>ISA Classifier Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierContainerImpl
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAClassifierContainer()
		 * @generated
		 */
		EClass ISA_CLASSIFIER_CONTAINER = eINSTANCE.getISAClassifierContainer();

		/**
		 * The meta object literal for the '<em><b>Classifiers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASSIFIER_CONTAINER__CLASSIFIERS = eINSTANCE.getISAClassifierContainer_Classifiers();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl <em>ISA Classifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAClassifier()
		 * @generated
		 */
		EClass ISA_CLASSIFIER = eINSTANCE.getISAClassifier();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASSIFIER__PARENT = eINSTANCE.getISAClassifier_Parent();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASSIFIER__FIELDS = eINSTANCE.getISAClassifier_Fields();

		/**
		 * The meta object literal for the '<em><b>Methods</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASSIFIER__METHODS = eINSTANCE.getISAClassifier_Methods();

		/**
		 * The meta object literal for the '<em><b>File Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_CLASSIFIER__FILE_PATH = eINSTANCE.getISAClassifier_FilePath();

		/**
		 * The meta object literal for the '<em><b>Get Containing Package</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_CLASSIFIER___GET_CONTAINING_PACKAGE = eINSTANCE.getISAClassifier__GetContainingPackage();

		/**
		 * The meta object literal for the '<em><b>Get Contained Members</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_CLASSIFIER___GET_CONTAINED_MEMBERS = eINSTANCE.getISAClassifier__GetContainedMembers();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassImpl <em>ISA Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassImpl
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAClass()
		 * @generated
		 */
		EClass ISA_CLASS = eINSTANCE.getISAClass();

		/**
		 * The meta object literal for the '<em><b>Extended Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASS__EXTENDED_CLASS = eINSTANCE.getISAClass_ExtendedClass();

		/**
		 * The meta object literal for the '<em><b>Implemented Interfaces</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASS__IMPLEMENTED_INTERFACES = eINSTANCE.getISAClass_ImplementedInterfaces();

		/**
		 * The meta object literal for the '<em><b>Usages As Extended Class</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASS__USAGES_AS_EXTENDED_CLASS = eINSTANCE.getISAClass_UsagesAsExtendedClass();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAAbstractClassImpl <em>ISA Abstract Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAAbstractClassImpl
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAAbstractClass()
		 * @generated
		 */
		EClass ISA_ABSTRACT_CLASS = eINSTANCE.getISAAbstractClass();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAConcreteClassImpl <em>ISA Concrete Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAConcreteClassImpl
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAConcreteClass()
		 * @generated
		 */
		EClass ISA_CONCRETE_CLASS = eINSTANCE.getISAConcreteClass();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceImpl <em>ISA Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceImpl
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAInterface()
		 * @generated
		 */
		EClass ISA_INTERFACE = eINSTANCE.getISAInterface();

		/**
		 * The meta object literal for the '<em><b>Usages As Implemented Interface</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE = eINSTANCE.getISAInterface_UsagesAsImplementedInterface();

		/**
		 * The meta object literal for the '<em><b>Usages As Extended Interface</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE = eINSTANCE.getISAInterface_UsagesAsExtendedInterface();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAEnumImpl <em>ISA Enum</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAEnumImpl
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAEnum()
		 * @generated
		 */
		EClass ISA_ENUM = eINSTANCE.getISAEnum();

		/**
		 * The meta object literal for the '<em><b>Constants</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_ENUM__CONSTANTS = eINSTANCE.getISAEnum_Constants();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISARecordImpl <em>ISA Record</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISARecordImpl
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISARecord()
		 * @generated
		 */
		EClass ISA_RECORD = eINSTANCE.getISARecord();

		/**
		 * The meta object literal for the '<em><b>Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_RECORD__COMPONENTS = eINSTANCE.getISARecord_Components();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceExtendingElementImpl <em>ISA Interface Extending Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceExtendingElementImpl
		 * @see dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl#getISAInterfaceExtendingElement()
		 * @generated
		 */
		EClass ISA_INTERFACE_EXTENDING_ELEMENT = eINSTANCE.getISAInterfaceExtendingElement();

		/**
		 * The meta object literal for the '<em><b>Extended Interfaces</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES = eINSTANCE.getISAInterfaceExtendingElement_ExtendedInterfaces();

	}

} //ClassifiersPackage

/**
 */
package dk.itu.cs.isa.model.system.quality.impl;

import dk.itu.cs.isa.model.system.quality.ISAFilePassage;
import dk.itu.cs.isa.model.system.quality.QualityPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA File Passage</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAFilePassageImpl#getStartLine <em>Start Line</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAFilePassageImpl#getEndLine <em>End Line</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAFilePassageImpl#getStartChar <em>Start Char</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAFilePassageImpl#getEndChar <em>End Char</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAFilePassageImpl extends MinimalEObjectImpl.Container implements ISAFilePassage {
	/**
	 * The default value of the '{@link #getStartLine() <em>Start Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartLine()
	 * @generated
	 * @ordered
	 */
	protected static final int START_LINE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStartLine() <em>Start Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartLine()
	 * @generated
	 * @ordered
	 */
	protected int startLine = START_LINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndLine() <em>End Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndLine()
	 * @generated
	 * @ordered
	 */
	protected static final int END_LINE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEndLine() <em>End Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndLine()
	 * @generated
	 * @ordered
	 */
	protected int endLine = END_LINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartChar() <em>Start Char</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartChar()
	 * @generated
	 * @ordered
	 */
	protected static final int START_CHAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStartChar() <em>Start Char</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartChar()
	 * @generated
	 * @ordered
	 */
	protected int startChar = START_CHAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndChar() <em>End Char</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndChar()
	 * @generated
	 * @ordered
	 */
	protected static final int END_CHAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEndChar() <em>End Char</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndChar()
	 * @generated
	 * @ordered
	 */
	protected int endChar = END_CHAR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAFilePassageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QualityPackage.Literals.ISA_FILE_PASSAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getStartLine() {
		return startLine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartLine(int newStartLine) {
		int oldStartLine = startLine;
		startLine = newStartLine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_FILE_PASSAGE__START_LINE, oldStartLine, startLine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getEndLine() {
		return endLine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndLine(int newEndLine) {
		int oldEndLine = endLine;
		endLine = newEndLine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_FILE_PASSAGE__END_LINE, oldEndLine, endLine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getStartChar() {
		return startChar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartChar(int newStartChar) {
		int oldStartChar = startChar;
		startChar = newStartChar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_FILE_PASSAGE__START_CHAR, oldStartChar, startChar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getEndChar() {
		return endChar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndChar(int newEndChar) {
		int oldEndChar = endChar;
		endChar = newEndChar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_FILE_PASSAGE__END_CHAR, oldEndChar, endChar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QualityPackage.ISA_FILE_PASSAGE__START_LINE:
				return getStartLine();
			case QualityPackage.ISA_FILE_PASSAGE__END_LINE:
				return getEndLine();
			case QualityPackage.ISA_FILE_PASSAGE__START_CHAR:
				return getStartChar();
			case QualityPackage.ISA_FILE_PASSAGE__END_CHAR:
				return getEndChar();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QualityPackage.ISA_FILE_PASSAGE__START_LINE:
				setStartLine((Integer)newValue);
				return;
			case QualityPackage.ISA_FILE_PASSAGE__END_LINE:
				setEndLine((Integer)newValue);
				return;
			case QualityPackage.ISA_FILE_PASSAGE__START_CHAR:
				setStartChar((Integer)newValue);
				return;
			case QualityPackage.ISA_FILE_PASSAGE__END_CHAR:
				setEndChar((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_FILE_PASSAGE__START_LINE:
				setStartLine(START_LINE_EDEFAULT);
				return;
			case QualityPackage.ISA_FILE_PASSAGE__END_LINE:
				setEndLine(END_LINE_EDEFAULT);
				return;
			case QualityPackage.ISA_FILE_PASSAGE__START_CHAR:
				setStartChar(START_CHAR_EDEFAULT);
				return;
			case QualityPackage.ISA_FILE_PASSAGE__END_CHAR:
				setEndChar(END_CHAR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_FILE_PASSAGE__START_LINE:
				return startLine != START_LINE_EDEFAULT;
			case QualityPackage.ISA_FILE_PASSAGE__END_LINE:
				return endLine != END_LINE_EDEFAULT;
			case QualityPackage.ISA_FILE_PASSAGE__START_CHAR:
				return startChar != START_CHAR_EDEFAULT;
			case QualityPackage.ISA_FILE_PASSAGE__END_CHAR:
				return endChar != END_CHAR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (startLine: ");
		result.append(startLine);
		result.append(", endLine: ");
		result.append(endLine);
		result.append(", startChar: ");
		result.append(startChar);
		result.append(", endChar: ");
		result.append(endChar);
		result.append(')');
		return result.toString();
	}

} //ISAFilePassageImpl

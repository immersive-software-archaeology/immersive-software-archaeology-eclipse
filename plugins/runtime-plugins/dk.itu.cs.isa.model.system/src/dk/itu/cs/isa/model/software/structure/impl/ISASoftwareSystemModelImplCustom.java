package dk.itu.cs.isa.model.software.structure.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;
import dk.itu.cs.isa.model.software.structure.libraries.ISALibrary;
import dk.itu.cs.isa.model.software.structure.libraries.ISALibraryClassifier;
import dk.itu.cs.isa.model.software.structure.libraries.ISAPrimitiveType;
import dk.itu.cs.isa.model.software.structure.libraries.LibrariesFactory;
import dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.members.MembersFactory;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

public class ISASoftwareSystemModelImplCustom extends ISASoftwareSystemModelImpl {
	
	
	
	@Override
	public ISANamedSoftwareElement getSystemElementByName(String qualifiedName) {
		qualifiedName = cleanUpQualifiedName(qualifiedName);

		if(qualifiedName.startsWith("project:/")) {
			// A project was specified explicitly
			String[] split = qualifiedName.split("/");
			String projectName = split[1];
			
			// search for the element in said project and return the result
			for(ISAProject p : getProjects()) {
				if(p.getName().equals(projectName)) {
					ISANamedSoftwareElement element = p.getElement(qualifiedName);
					if(element != null)
						return element;
				}
			}
			return null;
		}

		// Try to find the element in the system
		for(ISAProject project : this.getProjects()) {
			ISANamedSoftwareElement element = project.getElement(qualifiedName);
			if(element == null)
				continue;
			
			return element;
		}
		return null;
	}
	
	@Override
	public ISANamedSoftwareElement getLibraryElementByName(String qualifiedName, int expectedType, boolean createOnDemand) {
		qualifiedName = cleanUpQualifiedName(qualifiedName);
		
		if(!qualifiedName.contains(".")) {
			// Given name is not qualified!
			// This is a primitive type or void
			
			for(ISAPrimitiveType primitive : getStandardLibrary().getPrimitiveTypes()) {
				if(primitive.getName().equals(qualifiedName))
					return primitive;
			}
			
			if(createOnDemand) {
				ISAPrimitiveType type = LibrariesFactory.eINSTANCE.createISAPrimitiveType();
				type.setName(qualifiedName);
				getStandardLibrary().getPrimitiveTypes().add(type);
				
				return type;
			}
		}
		
		// Try to find the element in the libraries
		ISANamedSoftwareElement libEntry = null;
		for(ISALibrary lib : this.getLibraries()) {
			libEntry = lib.getElement(qualifiedName);
			if(libEntry != null)
				break;
		}
		
		if(libEntry == null && createOnDemand)
			libEntry = createLibEntry(qualifiedName, expectedType);
		
		return libEntry;
	}
	
	
	
	private String cleanUpQualifiedName(String qualifiedName) {
//		qualifiedName = "java.util.Optional<Test<Test, Test<Test>>, Test<Test<Test<Test, Test<Test>>>>>";
		
		qualifiedName = qualifiedName.replaceAll("\\[\\]", "");
		
		qualifiedName = qualifiedName.replaceAll("\\$", ".");
		
//		qualifiedName = qualifiedName.replaceAll("\\s*\\<[^\\>]*\\>\\s*", "");
		String cleanQualifiedName = "";
		int currentDepth = 0;
		for(char c : qualifiedName.toCharArray()) {
			if(c == '<')
				currentDepth ++;
			else if(c == '>')
				currentDepth --;
			
			else if(currentDepth == 0)
				cleanQualifiedName += c;
		}
		return cleanQualifiedName;
	}


	
	private ISALibrary getStandardLibrary() {
		if(getLibraries().size() == 0) {
			ISALibrary lib = LibrariesFactory.eINSTANCE.createISALibrary();
			lib.setName("Libraries");
			getLibraries().add(lib);
			return lib;
		}
		return getLibraries().get(0);
	}
	
	

	private ISANamedSoftwareElement createLibEntry(String qualifiedName, int type) {
		ISALibrary lib = getStandardLibrary();

		if(!qualifiedName.contains(";")) {
			// supposed to create a classifier
			ISALibraryClassifier newClassifier = LibrariesFactory.eINSTANCE.createISALibraryClassifier();
			newClassifier.setName(qualifiedName);
			lib.getClassifiers().add(newClassifier);
			
			return newClassifier;
		}

		ISANamedSoftwareElement parentClassifier = getLibraryElementByName(qualifiedName.split(";")[0], ClassifiersPackage.ISA_CLASSIFIER, true);
		if(!(parentClassifier instanceof ISALibraryClassifier))
			throw new RuntimeException("Unexpected type of element in createLibEntry(..): " + parentClassifier.getClass().getSimpleName() + " - should be of type ISALibraryClassifier");
		ISALibraryClassifier parentLibClassifier = (ISALibraryClassifier) parentClassifier;
		
		if(type == MembersPackage.ISA_METHOD) {
			ISAConcreteMethod newMethod = MembersFactory.eINSTANCE.createISAConcreteMethod();
			newMethod.setName(qualifiedName.split(";")[1]);
			parentLibClassifier.getMethods().add(newMethod);
			return newMethod;
		}
		else if(type == MembersPackage.ISA_FIELD) {
			ISAField newField = MembersFactory.eINSTANCE.createISAField();
			newField.setName(qualifiedName.split(";")[1]);
			parentLibClassifier.getFields().add(newField);
			return newField;
		}
		else if(type == MembersPackage.ISA_CONSTRUCTOR) {
			ISAConstructor newConstructor = MembersFactory.eINSTANCE.createISAConstructor();
			newConstructor.setName(qualifiedName.split(";")[1]);
			parentLibClassifier.getConstructors().add(newConstructor);
			return newConstructor;
		}
		else if(type == MembersPackage.ISA_ENUM_CONSTANT) {
			ISAEnumConstant newConstant = MembersFactory.eINSTANCE.createISAEnumConstant();
			newConstant.setName(qualifiedName.split(";")[1]);
			parentLibClassifier.getConstants().add(newConstant);
			return newConstant;
		}
		else if(type == MembersPackage.ISA_RECORD_COMPONENT) {
			ISARecordComponent newConstant = MembersFactory.eINSTANCE.createISARecordComponent();
			newConstant.setName(qualifiedName.split(";")[1]);
			parentLibClassifier.getComponents().add(newConstant);
			return newConstant;
		}
		else throw new RuntimeException("Unexpected type of member: " + type + " (look up in generated class MembersPackage)");
	}
	
}

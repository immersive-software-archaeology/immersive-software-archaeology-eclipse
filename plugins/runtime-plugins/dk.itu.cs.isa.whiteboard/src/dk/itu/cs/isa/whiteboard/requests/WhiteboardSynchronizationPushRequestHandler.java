package dk.itu.cs.isa.whiteboard.requests;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jetty.server.Request;
import org.eclipse.swt.widgets.Display;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.util.EcoreUtilISA;
import dk.itu.cs.isa.whiteboard.AbstractPhotoOperation;
import dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent;
import dk.itu.cs.isa.whiteboard.BoardDestroyEvent;
import dk.itu.cs.isa.whiteboard.BoardDuplicateEvent;
import dk.itu.cs.isa.whiteboard.BoardSpawnEvent;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;
import dk.itu.cs.isa.whiteboard.WhiteboardStorage;
import dk.itu.cs.isa.whiteboard.events.WhiteboardEventHandler;

public class WhiteboardSynchronizationPushRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		if(systemName == null) {
			String examplePath1 = "systemName=JetUML";
			printInvalidParametersHTMLPage(baseRequest, response, null, examplePath1);
			return;
		}
		
		try {
			String requestBodyXML = "";
			while(true) {
				String line = baseRequest.getReader().readLine();
				if(line == null)
					break;
				requestBodyXML += line + System.lineSeparator();
			}
			
			EObject modelRoot = null;
			if(requestBodyXML != null && !requestBodyXML.isBlank())
				modelRoot = EcoreUtilISA.getRootModelObjectFromXMLString(requestBodyXML, WhiteboardPackage.eINSTANCE, "whiteboard").get(0);
			
			if(modelRoot == null || !(modelRoot instanceof AbstractWhiteboardEvent)) {
				printHTMLPage(baseRequest, response, HttpServletResponse.SC_BAD_REQUEST,
						"Your request body did not contain an operation", null,
						"The purpose of this HTTP interface is to send whiteboard operations (e.g., addition of a pin) "
						+ "to the Eclipse server backend.<br>That is done by writing XML encoded operations into the "
						+ "body of this HTTP request.");
				return;
			}
			
			AbstractWhiteboardEvent submittedOperation = (AbstractWhiteboardEvent) modelRoot;
			submittedOperation.setTimestamp(System.currentTimeMillis());
			submittedOperation.setConfirmationRequired(isClientConfirmationMessageRequired(submittedOperation));
			
			if(submittedOperation.getWhiteboardId() == 0) {
				 if(submittedOperation instanceof BoardSpawnEvent)
					 submittedOperation.setWhiteboardId(WhiteboardStorage.INSTANCE.findAvailableWhiteboardId(systemName));
				 else
					 throw new Exception("The submitted operation had a target whiteboard id set to 0 - that is only possible for whiteboard spawn events!");
			}
			
			WhiteboardHistory historyForSystem = WhiteboardStorage.INSTANCE.getWhiteboardHistory(systemName);
			
			WhiteboardEventHandler opHandler = WhiteboardStorage.INSTANCE.getWhiteboardOperationHandler(systemName, submittedOperation.getWhiteboardId());
			opHandler.handleEvent(systemName, historyForSystem, submittedOperation);
			
			if(!(submittedOperation instanceof BoardDuplicateEvent))
				historyForSystem.getEventLog().add(submittedOperation);
			WhiteboardStorage.INSTANCE.save(historyForSystem, true);
			
			Display.getDefault().asyncExec(() -> {
				ISAEventSystem.fireStandardConsoleMessage("Pushed " + submittedOperation.getClass().getSimpleName() + " for whiteboard with id=" + submittedOperation.getWhiteboardId());
			});
			
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_OK, "Whiteboard successfully synchronized", null, "Everything went fine.");
		}
		catch(Exception e) {
			e.printStackTrace();
			
	    	response.setContentType("text/plain; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_CONFLICT);
			
			PrintWriter out = response.getWriter();
			out.print(e.getMessage());
			out.close();
		}
		baseRequest.setHandled(true);
	}
	
	private boolean isClientConfirmationMessageRequired(AbstractWhiteboardEvent event) {
		if(event instanceof BoardDestroyEvent)
			return false;
		if(event instanceof AbstractPhotoOperation)
			return false;
		return true;
	}

}





















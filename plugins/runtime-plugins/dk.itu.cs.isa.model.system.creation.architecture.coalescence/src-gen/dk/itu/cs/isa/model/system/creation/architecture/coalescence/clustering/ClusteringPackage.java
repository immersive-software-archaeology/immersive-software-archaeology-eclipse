/**
 */
package dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringFactory
 * @model kind="package"
 * @generated
 */
public interface ClusteringPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "clustering";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/clustering";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "clustering";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClusteringPackage eINSTANCE = dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ClusteringPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ISASimpleClusterImpl <em>ISA Simple Cluster</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ISASimpleClusterImpl
	 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ClusteringPackageImpl#getISASimpleCluster()
	 * @generated
	 */
	int ISA_SIMPLE_CLUSTER = 0;

	/**
	 * The feature id for the '<em><b>Element Names</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SIMPLE_CLUSTER__ELEMENT_NAMES = 0;

	/**
	 * The feature id for the '<em><b>Sub Clusters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SIMPLE_CLUSTER__SUB_CLUSTERS = 1;

	/**
	 * The number of structural features of the '<em>ISA Simple Cluster</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SIMPLE_CLUSTER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>ISA Simple Cluster</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SIMPLE_CLUSTER_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster <em>ISA Simple Cluster</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Simple Cluster</em>'.
	 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster
	 * @generated
	 */
	EClass getISASimpleCluster();

	/**
	 * Returns the meta object for the attribute list '{@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster#getElementNames <em>Element Names</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Element Names</em>'.
	 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster#getElementNames()
	 * @see #getISASimpleCluster()
	 * @generated
	 */
	EAttribute getISASimpleCluster_ElementNames();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster#getSubClusters <em>Sub Clusters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Clusters</em>'.
	 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster#getSubClusters()
	 * @see #getISASimpleCluster()
	 * @generated
	 */
	EReference getISASimpleCluster_SubClusters();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClusteringFactory getClusteringFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ISASimpleClusterImpl <em>ISA Simple Cluster</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ISASimpleClusterImpl
		 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ClusteringPackageImpl#getISASimpleCluster()
		 * @generated
		 */
		EClass ISA_SIMPLE_CLUSTER = eINSTANCE.getISASimpleCluster();

		/**
		 * The meta object literal for the '<em><b>Element Names</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SIMPLE_CLUSTER__ELEMENT_NAMES = eINSTANCE.getISASimpleCluster_ElementNames();

		/**
		 * The meta object literal for the '<em><b>Sub Clusters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SIMPLE_CLUSTER__SUB_CLUSTERS = eINSTANCE.getISASimpleCluster_SubClusters();

	}

} //ClusteringPackage

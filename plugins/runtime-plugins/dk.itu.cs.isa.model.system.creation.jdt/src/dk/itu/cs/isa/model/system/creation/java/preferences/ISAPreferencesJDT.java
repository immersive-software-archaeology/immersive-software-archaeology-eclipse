package dk.itu.cs.isa.model.system.creation.java.preferences;

import dk.itu.cs.isa.preferences.ISAPreferences;

public class ISAPreferencesJDT extends ISAPreferences {

	public static final String JDT_COMPLIANCE_LEVEL = "JDT_COMPLIANCE_LEVEL";
	public static final int COMPLIANCE_LEVEL_DEFAULT = 8;
	
	@Override
    public void initializeDefaultPreferences() {
		ISAPreferencesJDT.setString(ISAPreferencesJDT.JDT_COMPLIANCE_LEVEL, String.valueOf(COMPLIANCE_LEVEL_DEFAULT));
	}
	
	public static int getComplianceLevel() {
		String level = getString(JDT_COMPLIANCE_LEVEL, String.valueOf(COMPLIANCE_LEVEL_DEFAULT));
		return Integer.valueOf(level);
	}

}

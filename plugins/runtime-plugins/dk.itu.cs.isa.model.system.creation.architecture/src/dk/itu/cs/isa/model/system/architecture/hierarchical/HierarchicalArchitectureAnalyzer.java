package dk.itu.cs.isa.model.system.architecture.hierarchical;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import dk.itu.cs.isa.analyzer.interfaces.ISystemModelArchitectureAnalyzer;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;
import dk.itu.cs.isa.model.system.architecture.util.ClusterHelper;
import dk.itu.cs.isa.model.system.architecture.util.ISAClusteringCachingUtil;
import dk.itu.cs.isa.model.system.architecture.util.StatisticsLogger;

public abstract class HierarchicalArchitectureAnalyzer implements ISystemModelArchitectureAnalyzer {
	
	public StatisticsLogger stats;
	
	protected ISASoftwareSystemModel isaSystem;
	protected List<ISACluster> initialSingletonClusters;
	
	
	
	@Override
	public void initializeInstance() {
	}
	
	@Override
	public void setupBeforeRun() {
		ISAClusteringCachingUtil.INSTANCE.reset();
		initialSingletonClusters = new LinkedList<>();
	}

	@Override
	public void cleanUpAfterRun() {
		stats = null;
		ISAClusteringCachingUtil.INSTANCE.reset();
		initialSingletonClusters = null;
	}

	@Override
	public void run(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) throws Exception {
		monitor.beginTask("Analyzing architecture", IProgressMonitor.UNKNOWN);
		this.isaSystem = isaSystem;
		this.stats = new StatisticsLogger(isaSystem);
		
		// Initialization - create a singleton component for each class
		monitor.subTask("Initializing...");
		for(ISAProject p : isaSystem.getProjects())
			ClusterHelper.initializeSetOfSingletonClustersRecursively(initialSingletonClusters, p);
	}
	
	public List<ISACluster> getInitialClusters() {
		return this.initialSingletonClusters;
	}
	
}

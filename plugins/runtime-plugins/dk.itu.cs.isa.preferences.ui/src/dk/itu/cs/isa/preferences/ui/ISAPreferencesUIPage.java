package dk.itu.cs.isa.preferences.ui;

import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;

import java.io.IOException;
import java.util.Map;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

import dk.itu.cs.isa.analyzer.SystemAnalysisExtensionHandler;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelArchitectureAnalyzer;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelCreator;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelQualityAnalyzer;
import dk.itu.cs.isa.analyzer.interfaces.IVisualiationModelCreator;
import dk.itu.cs.isa.eclipse.ui.SystemModelsObserver;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.events.events.GUIEvent;
import dk.itu.cs.isa.model.examples.ExampleSystemPopulator;
import dk.itu.cs.isa.preferences.ISAPreferences;
import dk.itu.cs.isa.storage.ISAModelStorage;
import dk.itu.cs.isa.storage.ModelStorageEntry;

public class ISAPreferencesUIPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	
	private List modelsList;
	private Button populateWithExampleModelsButton;
	private Button clearAllModelsButton;

	private FileFieldEditor isaExecutableEditor;
	private FileFieldEditor rExecutableEditor;

	private ComboFieldEditor designAnalyzerComboBox;
	private ComboFieldEditor qualityAnalyzerComboBox;
	private ComboFieldEditor architectureAnalyzerComboBox;
	private ComboFieldEditor modelTransformatorComboBox;

	private BooleanFieldEditor analyzeQualityButton;
	private BooleanFieldEditor analyzeArchitectureButton;

	public ISAPreferencesUIPage() {
		// TODO add fancy image!
		super(GRID);
	}
	
	
	
	@Override
	public void init(IWorkbench workbench) {
		setTitle("ISA - Immersive Software Archeology");
		setDescription("General settings page for the core ISA Plug-In Bundle");

		setPreferenceStore(new ScopedPreferenceStore(InstanceScope.INSTANCE, ISAPreferences.STORAGE_ID));
	}
	
	@Override
	public void applyData(Object data) {
		super.applyData(data);
	}
	
	
	
	@Override
	public void createFieldEditors() {
		addSpace();
		
		Group modelsGroup = new Group(getFieldEditorParent(), SWT.SHADOW_IN);
		modelsGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 2));
		GridLayout modelsGroupLayout = new GridLayout(3, false);
		modelsGroupLayout.marginHeight = 10;
		modelsGroup.setLayout(modelsGroupLayout);
		modelsGroup.setText("ISA Model Storage");
		
		Label storageLabel = new Label(modelsGroup, SWT.LEFT);
		storageLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
		storageLabel.setText("Currently stored System Models:");
		
		modelsList = new List(modelsGroup, SWT.BORDER);
		modelsList.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
//		modelsList.addSelectionListener(widgetSelectedAdapter(event -> {
//			Widget widget = event.widget;
//			System.out.println(widget);
//		}));
		
//		Button clearSelectedModelButton = new Button(modelsGroup, SWT.NONE);
//		clearSelectedModelButton.setEnabled(false);
//		clearSelectedModelButton.setText("Delete");
//		clearSelectedModelButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		
		populateWithExampleModelsButton = new Button(modelsGroup, SWT.NONE);
		populateWithExampleModelsButton.setEnabled(modelsList.getItemCount() == 0);
		populateWithExampleModelsButton.setText("Create example models");
		populateWithExampleModelsButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		populateWithExampleModelsButton.addSelectionListener(widgetSelectedAdapter(event -> {
			Widget widget = event.widget;
			if (widget == populateWithExampleModelsButton) {
				try {
					ExampleSystemPopulator.INSTANCE.populateExampleSystems();
				} catch (IOException e) {
					ISAEventSystem.fireErrorMessage("Something went wrong while creating the example system models.", e);
				}
				updateModelsList();
			}
		}));
		
		clearAllModelsButton = new Button(modelsGroup, SWT.NONE);
		clearAllModelsButton.setEnabled(modelsList.getItemCount() > 0);
		clearAllModelsButton.setText("Clear all");
		clearAllModelsButton.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, false, false, 1, 1));
		clearAllModelsButton.addSelectionListener(widgetSelectedAdapter(event -> {
			Widget widget = event.widget;
			if (widget == clearAllModelsButton) {
				GUIEvent dialogEvent = ISAEventSystem.fireConfirmationDialog("Do you really want to delete all stored models?");
				if(dialogEvent.getReturnCode() == 0) {
					try {
						ISAModelStorage.INSTANCE.clear();
						updateModelsList();
					} catch (IOException e) {
						ISAEventSystem.fireErrorMessage("Something went wrong while clearing the model storage.", e);
					}
				}
			}
		}));

		updateModelsList();
		
		
		
		addSeperator();
		

		isaExecutableEditor = new FileFieldEditor(ISAPreferences.ISA_EXECUTABLE_PATH, "&ISA executable location\n(optional, convenience for users):", getFieldEditorParent());
		addField(isaExecutableEditor);
		
		
		
		addSeperator();
		

		rExecutableEditor = new FileFieldEditor(ISAPreferences.R_EXECUTABLE_PATH, "&R Script executable location\n(optional, for debugging):", getFieldEditorParent());
		addField(rExecutableEditor);
		

		addSeperator();
		
		
		SystemAnalysisExtensionHandler analysisExtensionHandler = new SystemAnalysisExtensionHandler();
		
		Map<String, ISystemModelCreator> allSystemModelCreators = analysisExtensionHandler.getAllSystemModelCreatorsFromExtensions();
		String[][] systemModelCreators = new String[allSystemModelCreators.size()][2];
		int i = 0;
		for(String name : allSystemModelCreators.keySet()) {
			systemModelCreators[i] = new String[] {name, name};
			i++;
		}
		designAnalyzerComboBox = new ComboFieldEditor(ISAPreferences.SYSTEM_MODEL_CREATOR_DEFAULT, "&Default System Model Creator:", systemModelCreators, getFieldEditorParent());
		addField(designAnalyzerComboBox);

		addSpace();

		Map<String, ISystemModelQualityAnalyzer> allQualityAnalyzers = analysisExtensionHandler.getAllSystemModelQualityAnalyzersFromExtensions();
		String[][] systemQualityAnalyzers = new String[allQualityAnalyzers.size()][2];
		i = 0;
		for(String name : allQualityAnalyzers.keySet()) {
			systemQualityAnalyzers[i] = new String[] {name, name};
			i++;
		}
		qualityAnalyzerComboBox = new ComboFieldEditor(ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_DEFAULT, "&Default System Quality Analyzer:", systemQualityAnalyzers, getFieldEditorParent());
		addField(qualityAnalyzerComboBox);
		analyzeQualityButton = new BooleanFieldEditor(ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_EXECUTE, "&Perform Quality Analysis", getFieldEditorParent());
		addField(analyzeQualityButton);

		addSpace();

		Map<String, ISystemModelArchitectureAnalyzer> allArchitectureAnalyzers = analysisExtensionHandler.getAllSystemModelArchitectureAnalyzersFromExtensions();
		String[][] architectureAnalyzers = new String[allArchitectureAnalyzers.size()][2];
		i = 0;
		for(String name : allArchitectureAnalyzers.keySet()) {
			architectureAnalyzers[i] = new String[] {name, name};
			i++;
		}
		architectureAnalyzerComboBox = new ComboFieldEditor(ISAPreferences.SYSTEM_MODEL_SAR_DEFAULT, "&Default Architecture Recovery:", architectureAnalyzers, getFieldEditorParent());
		addField(architectureAnalyzerComboBox);
		analyzeArchitectureButton = new BooleanFieldEditor(ISAPreferences.SYSTEM_MODEL_SAR_EXECUTE, "&Perform Architecture Recovery", getFieldEditorParent());
		addField(analyzeArchitectureButton);

		addSpace();

		Map<String, IVisualiationModelCreator> allVisualizationModelCreators = analysisExtensionHandler.getAllVisualizationModelCreatorsFromExtensions();
		String[][] visualizationModelCreators = new String[allVisualizationModelCreators.size()][2];
		i = 0;
		for(String name : allVisualizationModelCreators.keySet()) {
			visualizationModelCreators[i] = new String[] {name, name};
			i++;
		}
		modelTransformatorComboBox = new ComboFieldEditor(ISAPreferences.VISUALIZATION_MODEL_CREATOR_DEFAULT, "&Default Visualization Model Creator:", visualizationModelCreators, getFieldEditorParent());
		addField(modelTransformatorComboBox);
	}

	private void updateModelsList() {
		modelsList.removeAll();
		for(ModelStorageEntry entry : ISAModelStorage.INSTANCE.getAllEntries()) {
			modelsList.add(entry.getSystemName());
		}
		clearAllModelsButton.setEnabled(modelsList.getItemCount() > 0);
		populateWithExampleModelsButton.setEnabled(modelsList.getItemCount() == 0);
		
		SystemModelsObserver.updateViews();
	}

	protected void addSeperator() {
		addSpace();
		new Label(getFieldEditorParent(), SWT.SEPARATOR | SWT.HORIZONTAL).setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		addSpace();
	}
	
	protected void addSpace() {
		new Label(getFieldEditorParent(), SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
	}
	
	
	
	@Override
	public void performDefaults() {
//		super.performDefaults();
		
		new ISAPreferences().initializeDefaultPreferences();
		isaExecutableEditor.setStringValue(ISAPreferences.getString(ISAPreferences.ISA_EXECUTABLE_PATH, ""));
		rExecutableEditor.setStringValue(ISAPreferences.getString(ISAPreferences.R_EXECUTABLE_PATH, ""));
		
		designAnalyzerComboBox.load();
		qualityAnalyzerComboBox.load();
		architectureAnalyzerComboBox.load();
		modelTransformatorComboBox.load();

		analyzeQualityButton.load();
		analyzeArchitectureButton.load();
		
		checkState();
		updateApplyButton();
	}

}

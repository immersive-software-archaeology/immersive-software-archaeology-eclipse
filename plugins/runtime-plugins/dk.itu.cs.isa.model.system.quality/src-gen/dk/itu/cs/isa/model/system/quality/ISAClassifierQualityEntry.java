/**
 */
package dk.itu.cs.isa.model.system.quality;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Classifier Quality Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getMeasures <em>Measures</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getIssues <em>Issues</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAClassifierQualityEntry()
 * @model
 * @generated
 */
public interface ISAClassifierQualityEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier</em>' reference.
	 * @see #setClassifier(ISAClassifier)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAClassifierQualityEntry_Classifier()
	 * @model required="true"
	 * @generated
	 */
	ISAClassifier getClassifier();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getClassifier <em>Classifier</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Classifier</em>' reference.
	 * @see #getClassifier()
	 * @generated
	 */
	void setClassifier(ISAClassifier value);

	/**
	 * Returns the value of the '<em><b>Measures</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.system.quality.ISAMeasure}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getParentEntry <em>Parent Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measures</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAClassifierQualityEntry_Measures()
	 * @see dk.itu.cs.isa.model.system.quality.ISAMeasure#getParentEntry
	 * @model opposite="parentEntry" containment="true"
	 * @generated
	 */
	EList<ISAMeasure> getMeasures();

	/**
	 * Returns the value of the '<em><b>Issues</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.system.quality.ISAIssue}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getParentEntry <em>Parent Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Issues</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAClassifierQualityEntry_Issues()
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue#getParentEntry
	 * @model opposite="parentEntry" containment="true"
	 * @generated
	 */
	EList<ISAIssue> getIssues();

} // ISAClassifierQualityEntry

/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo.impl;

import dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage;
import dk.itu.cs.isa.multiplayer.clientinfo.HandInfo;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Client Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getSystemName <em>System Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getId <em>Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getColor <em>Color</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getHead <em>Head</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getLeftHand <em>Left Hand</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getRightHand <em>Right Hand</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getFps <em>Fps</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getPing <em>Ping</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getConnectTimestamp <em>Connect Timestamp</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getLastServerTimestamp <em>Last Server Timestamp</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl#getLastClientTimestamp <em>Last Client Timestamp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClientInfoImpl extends MinimalEObjectImpl.Container implements ClientInfo {
	/**
	 * The default value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemName()
	 * @generated
	 * @ordered
	 */
	protected static final String SYSTEM_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemName()
	 * @generated
	 * @ordered
	 */
	protected String systemName = SYSTEM_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final byte ID_EDEFAULT = 0x00;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected byte id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final byte COLOR_EDEFAULT = 0x00;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected byte color = COLOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHead() <em>Head</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHead()
	 * @generated
	 * @ordered
	 */
	protected BodyPartInfo head;

	/**
	 * The cached value of the '{@link #getLeftHand() <em>Left Hand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftHand()
	 * @generated
	 * @ordered
	 */
	protected HandInfo leftHand;

	/**
	 * The cached value of the '{@link #getRightHand() <em>Right Hand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightHand()
	 * @generated
	 * @ordered
	 */
	protected HandInfo rightHand;

	/**
	 * The default value of the '{@link #getFps() <em>Fps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFps()
	 * @generated
	 * @ordered
	 */
	protected static final byte FPS_EDEFAULT = 0x00;

	/**
	 * The cached value of the '{@link #getFps() <em>Fps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFps()
	 * @generated
	 * @ordered
	 */
	protected byte fps = FPS_EDEFAULT;

	/**
	 * The default value of the '{@link #getPing() <em>Ping</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPing()
	 * @generated
	 * @ordered
	 */
	protected static final int PING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPing() <em>Ping</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPing()
	 * @generated
	 * @ordered
	 */
	protected int ping = PING_EDEFAULT;

	/**
	 * The default value of the '{@link #getConnectTimestamp() <em>Connect Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final long CONNECT_TIMESTAMP_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getConnectTimestamp() <em>Connect Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectTimestamp()
	 * @generated
	 * @ordered
	 */
	protected long connectTimestamp = CONNECT_TIMESTAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastServerTimestamp() <em>Last Server Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastServerTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final long LAST_SERVER_TIMESTAMP_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getLastServerTimestamp() <em>Last Server Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastServerTimestamp()
	 * @generated
	 * @ordered
	 */
	protected long lastServerTimestamp = LAST_SERVER_TIMESTAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastClientTimestamp() <em>Last Client Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastClientTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final long LAST_CLIENT_TIMESTAMP_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getLastClientTimestamp() <em>Last Client Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastClientTimestamp()
	 * @generated
	 * @ordered
	 */
	protected long lastClientTimestamp = LAST_CLIENT_TIMESTAMP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClientInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClientinfoPackage.Literals.CLIENT_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemName(String newSystemName) {
		String oldSystemName = systemName;
		systemName = newSystemName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__SYSTEM_NAME, oldSystemName, systemName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(byte newId) {
		byte oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(byte newColor) {
		byte oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__COLOR, oldColor, color));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BodyPartInfo getHead() {
		return head;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHead(BodyPartInfo newHead, NotificationChain msgs) {
		BodyPartInfo oldHead = head;
		head = newHead;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__HEAD, oldHead, newHead);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHead(BodyPartInfo newHead) {
		if (newHead != head) {
			NotificationChain msgs = null;
			if (head != null)
				msgs = ((InternalEObject)head).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.CLIENT_INFO__HEAD, null, msgs);
			if (newHead != null)
				msgs = ((InternalEObject)newHead).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.CLIENT_INFO__HEAD, null, msgs);
			msgs = basicSetHead(newHead, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__HEAD, newHead, newHead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HandInfo getLeftHand() {
		return leftHand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeftHand(HandInfo newLeftHand, NotificationChain msgs) {
		HandInfo oldLeftHand = leftHand;
		leftHand = newLeftHand;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__LEFT_HAND, oldLeftHand, newLeftHand);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftHand(HandInfo newLeftHand) {
		if (newLeftHand != leftHand) {
			NotificationChain msgs = null;
			if (leftHand != null)
				msgs = ((InternalEObject)leftHand).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.CLIENT_INFO__LEFT_HAND, null, msgs);
			if (newLeftHand != null)
				msgs = ((InternalEObject)newLeftHand).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.CLIENT_INFO__LEFT_HAND, null, msgs);
			msgs = basicSetLeftHand(newLeftHand, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__LEFT_HAND, newLeftHand, newLeftHand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HandInfo getRightHand() {
		return rightHand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRightHand(HandInfo newRightHand, NotificationChain msgs) {
		HandInfo oldRightHand = rightHand;
		rightHand = newRightHand;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__RIGHT_HAND, oldRightHand, newRightHand);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightHand(HandInfo newRightHand) {
		if (newRightHand != rightHand) {
			NotificationChain msgs = null;
			if (rightHand != null)
				msgs = ((InternalEObject)rightHand).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.CLIENT_INFO__RIGHT_HAND, null, msgs);
			if (newRightHand != null)
				msgs = ((InternalEObject)newRightHand).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.CLIENT_INFO__RIGHT_HAND, null, msgs);
			msgs = basicSetRightHand(newRightHand, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__RIGHT_HAND, newRightHand, newRightHand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public byte getFps() {
		return fps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFps(byte newFps) {
		byte oldFps = fps;
		fps = newFps;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__FPS, oldFps, fps));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPing() {
		return ping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPing(int newPing) {
		int oldPing = ping;
		ping = newPing;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__PING, oldPing, ping));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getConnectTimestamp() {
		return connectTimestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnectTimestamp(long newConnectTimestamp) {
		long oldConnectTimestamp = connectTimestamp;
		connectTimestamp = newConnectTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__CONNECT_TIMESTAMP, oldConnectTimestamp, connectTimestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getLastServerTimestamp() {
		return lastServerTimestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastServerTimestamp(long newLastServerTimestamp) {
		long oldLastServerTimestamp = lastServerTimestamp;
		lastServerTimestamp = newLastServerTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__LAST_SERVER_TIMESTAMP, oldLastServerTimestamp, lastServerTimestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getLastClientTimestamp() {
		return lastClientTimestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastClientTimestamp(long newLastClientTimestamp) {
		long oldLastClientTimestamp = lastClientTimestamp;
		lastClientTimestamp = newLastClientTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.CLIENT_INFO__LAST_CLIENT_TIMESTAMP, oldLastClientTimestamp, lastClientTimestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClientinfoPackage.CLIENT_INFO__HEAD:
				return basicSetHead(null, msgs);
			case ClientinfoPackage.CLIENT_INFO__LEFT_HAND:
				return basicSetLeftHand(null, msgs);
			case ClientinfoPackage.CLIENT_INFO__RIGHT_HAND:
				return basicSetRightHand(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClientinfoPackage.CLIENT_INFO__SYSTEM_NAME:
				return getSystemName();
			case ClientinfoPackage.CLIENT_INFO__ID:
				return getId();
			case ClientinfoPackage.CLIENT_INFO__COLOR:
				return getColor();
			case ClientinfoPackage.CLIENT_INFO__HEAD:
				return getHead();
			case ClientinfoPackage.CLIENT_INFO__LEFT_HAND:
				return getLeftHand();
			case ClientinfoPackage.CLIENT_INFO__RIGHT_HAND:
				return getRightHand();
			case ClientinfoPackage.CLIENT_INFO__FPS:
				return getFps();
			case ClientinfoPackage.CLIENT_INFO__PING:
				return getPing();
			case ClientinfoPackage.CLIENT_INFO__CONNECT_TIMESTAMP:
				return getConnectTimestamp();
			case ClientinfoPackage.CLIENT_INFO__LAST_SERVER_TIMESTAMP:
				return getLastServerTimestamp();
			case ClientinfoPackage.CLIENT_INFO__LAST_CLIENT_TIMESTAMP:
				return getLastClientTimestamp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClientinfoPackage.CLIENT_INFO__SYSTEM_NAME:
				setSystemName((String)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__ID:
				setId((Byte)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__COLOR:
				setColor((Byte)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__HEAD:
				setHead((BodyPartInfo)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__LEFT_HAND:
				setLeftHand((HandInfo)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__RIGHT_HAND:
				setRightHand((HandInfo)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__FPS:
				setFps((Byte)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__PING:
				setPing((Integer)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__CONNECT_TIMESTAMP:
				setConnectTimestamp((Long)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__LAST_SERVER_TIMESTAMP:
				setLastServerTimestamp((Long)newValue);
				return;
			case ClientinfoPackage.CLIENT_INFO__LAST_CLIENT_TIMESTAMP:
				setLastClientTimestamp((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClientinfoPackage.CLIENT_INFO__SYSTEM_NAME:
				setSystemName(SYSTEM_NAME_EDEFAULT);
				return;
			case ClientinfoPackage.CLIENT_INFO__ID:
				setId(ID_EDEFAULT);
				return;
			case ClientinfoPackage.CLIENT_INFO__COLOR:
				setColor(COLOR_EDEFAULT);
				return;
			case ClientinfoPackage.CLIENT_INFO__HEAD:
				setHead((BodyPartInfo)null);
				return;
			case ClientinfoPackage.CLIENT_INFO__LEFT_HAND:
				setLeftHand((HandInfo)null);
				return;
			case ClientinfoPackage.CLIENT_INFO__RIGHT_HAND:
				setRightHand((HandInfo)null);
				return;
			case ClientinfoPackage.CLIENT_INFO__FPS:
				setFps(FPS_EDEFAULT);
				return;
			case ClientinfoPackage.CLIENT_INFO__PING:
				setPing(PING_EDEFAULT);
				return;
			case ClientinfoPackage.CLIENT_INFO__CONNECT_TIMESTAMP:
				setConnectTimestamp(CONNECT_TIMESTAMP_EDEFAULT);
				return;
			case ClientinfoPackage.CLIENT_INFO__LAST_SERVER_TIMESTAMP:
				setLastServerTimestamp(LAST_SERVER_TIMESTAMP_EDEFAULT);
				return;
			case ClientinfoPackage.CLIENT_INFO__LAST_CLIENT_TIMESTAMP:
				setLastClientTimestamp(LAST_CLIENT_TIMESTAMP_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClientinfoPackage.CLIENT_INFO__SYSTEM_NAME:
				return SYSTEM_NAME_EDEFAULT == null ? systemName != null : !SYSTEM_NAME_EDEFAULT.equals(systemName);
			case ClientinfoPackage.CLIENT_INFO__ID:
				return id != ID_EDEFAULT;
			case ClientinfoPackage.CLIENT_INFO__COLOR:
				return color != COLOR_EDEFAULT;
			case ClientinfoPackage.CLIENT_INFO__HEAD:
				return head != null;
			case ClientinfoPackage.CLIENT_INFO__LEFT_HAND:
				return leftHand != null;
			case ClientinfoPackage.CLIENT_INFO__RIGHT_HAND:
				return rightHand != null;
			case ClientinfoPackage.CLIENT_INFO__FPS:
				return fps != FPS_EDEFAULT;
			case ClientinfoPackage.CLIENT_INFO__PING:
				return ping != PING_EDEFAULT;
			case ClientinfoPackage.CLIENT_INFO__CONNECT_TIMESTAMP:
				return connectTimestamp != CONNECT_TIMESTAMP_EDEFAULT;
			case ClientinfoPackage.CLIENT_INFO__LAST_SERVER_TIMESTAMP:
				return lastServerTimestamp != LAST_SERVER_TIMESTAMP_EDEFAULT;
			case ClientinfoPackage.CLIENT_INFO__LAST_CLIENT_TIMESTAMP:
				return lastClientTimestamp != LAST_CLIENT_TIMESTAMP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (systemName: ");
		result.append(systemName);
		result.append(", id: ");
		result.append(id);
		result.append(", color: ");
		result.append(color);
		result.append(", fps: ");
		result.append(fps);
		result.append(", ping: ");
		result.append(ping);
		result.append(", connectTimestamp: ");
		result.append(connectTimestamp);
		result.append(", lastServerTimestamp: ");
		result.append(lastServerTimestamp);
		result.append(", lastClientTimestamp: ");
		result.append(lastClientTimestamp);
		result.append(')');
		return result.toString();
	}

} //ClientInfoImpl

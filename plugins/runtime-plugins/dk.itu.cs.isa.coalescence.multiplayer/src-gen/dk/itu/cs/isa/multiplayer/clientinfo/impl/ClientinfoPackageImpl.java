/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo.impl;

import dk.itu.cs.isa.multiplayer.MultiplayerPackage;

import dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoFactory;
import dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage;
import dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.HandInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.HandPose;
import dk.itu.cs.isa.multiplayer.clientinfo.HeadInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3;

import dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClientinfoPackageImpl extends EPackageImpl implements ClientinfoPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clientInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bodyPartInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass headInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass handInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass handHeldObjectInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaVector3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum handPoseEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ClientinfoPackageImpl() {
		super(eNS_URI, ClientinfoFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ClientinfoPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ClientinfoPackage init() {
		if (isInited) return (ClientinfoPackage)EPackage.Registry.INSTANCE.getEPackage(ClientinfoPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredClientinfoPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ClientinfoPackageImpl theClientinfoPackage = registeredClientinfoPackage instanceof ClientinfoPackageImpl ? (ClientinfoPackageImpl)registeredClientinfoPackage : new ClientinfoPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MultiplayerPackage.eNS_URI);
		MultiplayerPackageImpl theMultiplayerPackage = (MultiplayerPackageImpl)(registeredPackage instanceof MultiplayerPackageImpl ? registeredPackage : MultiplayerPackage.eINSTANCE);

		// Create package meta-data objects
		theClientinfoPackage.createPackageContents();
		theMultiplayerPackage.createPackageContents();

		// Initialize created meta-data
		theClientinfoPackage.initializePackageContents();
		theMultiplayerPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClientinfoPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClientinfoPackage.eNS_URI, theClientinfoPackage);
		return theClientinfoPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClientInfo() {
		return clientInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClientInfo_SystemName() {
		return (EAttribute)clientInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClientInfo_Id() {
		return (EAttribute)clientInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClientInfo_Color() {
		return (EAttribute)clientInfoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClientInfo_Head() {
		return (EReference)clientInfoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClientInfo_LeftHand() {
		return (EReference)clientInfoEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClientInfo_RightHand() {
		return (EReference)clientInfoEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClientInfo_Fps() {
		return (EAttribute)clientInfoEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClientInfo_Ping() {
		return (EAttribute)clientInfoEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClientInfo_ConnectTimestamp() {
		return (EAttribute)clientInfoEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClientInfo_LastServerTimestamp() {
		return (EAttribute)clientInfoEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClientInfo_LastClientTimestamp() {
		return (EAttribute)clientInfoEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBodyPartInfo() {
		return bodyPartInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBodyPartInfo_Position() {
		return (EReference)bodyPartInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBodyPartInfo_Rotation() {
		return (EReference)bodyPartInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHeadInfo() {
		return headInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHandInfo() {
		return handInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHandInfo_IndexFingerTipPosition() {
		return (EReference)handInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHandInfo_Pose() {
		return (EAttribute)handInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHandInfo_HeldObject() {
		return (EReference)handInfoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHandHeldObjectInfo() {
		return handHeldObjectInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHandHeldObjectInfo_Id() {
		return (EAttribute)handHeldObjectInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAVector3() {
		return isaVector3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_X() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_Y() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_Z() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getHandPose() {
		return handPoseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClientinfoFactory getClientinfoFactory() {
		return (ClientinfoFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		clientInfoEClass = createEClass(CLIENT_INFO);
		createEAttribute(clientInfoEClass, CLIENT_INFO__SYSTEM_NAME);
		createEAttribute(clientInfoEClass, CLIENT_INFO__ID);
		createEAttribute(clientInfoEClass, CLIENT_INFO__COLOR);
		createEReference(clientInfoEClass, CLIENT_INFO__HEAD);
		createEReference(clientInfoEClass, CLIENT_INFO__LEFT_HAND);
		createEReference(clientInfoEClass, CLIENT_INFO__RIGHT_HAND);
		createEAttribute(clientInfoEClass, CLIENT_INFO__FPS);
		createEAttribute(clientInfoEClass, CLIENT_INFO__PING);
		createEAttribute(clientInfoEClass, CLIENT_INFO__CONNECT_TIMESTAMP);
		createEAttribute(clientInfoEClass, CLIENT_INFO__LAST_SERVER_TIMESTAMP);
		createEAttribute(clientInfoEClass, CLIENT_INFO__LAST_CLIENT_TIMESTAMP);

		bodyPartInfoEClass = createEClass(BODY_PART_INFO);
		createEReference(bodyPartInfoEClass, BODY_PART_INFO__POSITION);
		createEReference(bodyPartInfoEClass, BODY_PART_INFO__ROTATION);

		headInfoEClass = createEClass(HEAD_INFO);

		handInfoEClass = createEClass(HAND_INFO);
		createEReference(handInfoEClass, HAND_INFO__INDEX_FINGER_TIP_POSITION);
		createEAttribute(handInfoEClass, HAND_INFO__POSE);
		createEReference(handInfoEClass, HAND_INFO__HELD_OBJECT);

		handHeldObjectInfoEClass = createEClass(HAND_HELD_OBJECT_INFO);
		createEAttribute(handHeldObjectInfoEClass, HAND_HELD_OBJECT_INFO__ID);

		isaVector3EClass = createEClass(ISA_VECTOR3);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__X);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__Y);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__Z);

		// Create enums
		handPoseEEnum = createEEnum(HAND_POSE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		headInfoEClass.getESuperTypes().add(this.getBodyPartInfo());
		handInfoEClass.getESuperTypes().add(this.getBodyPartInfo());
		handHeldObjectInfoEClass.getESuperTypes().add(this.getBodyPartInfo());

		// Initialize classes, features, and operations; add parameters
		initEClass(clientInfoEClass, ClientInfo.class, "ClientInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClientInfo_SystemName(), ecorePackage.getEString(), "systemName", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClientInfo_Id(), ecorePackage.getEByte(), "id", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClientInfo_Color(), ecorePackage.getEByte(), "color", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClientInfo_Head(), this.getBodyPartInfo(), null, "head", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClientInfo_LeftHand(), this.getHandInfo(), null, "leftHand", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClientInfo_RightHand(), this.getHandInfo(), null, "rightHand", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClientInfo_Fps(), ecorePackage.getEByte(), "fps", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClientInfo_Ping(), ecorePackage.getEInt(), "ping", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClientInfo_ConnectTimestamp(), ecorePackage.getELong(), "connectTimestamp", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClientInfo_LastServerTimestamp(), ecorePackage.getELong(), "lastServerTimestamp", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClientInfo_LastClientTimestamp(), ecorePackage.getELong(), "lastClientTimestamp", null, 1, 1, ClientInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bodyPartInfoEClass, BodyPartInfo.class, "BodyPartInfo", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBodyPartInfo_Position(), this.getISAVector3(), null, "position", null, 1, 1, BodyPartInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBodyPartInfo_Rotation(), this.getISAVector3(), null, "rotation", null, 1, 1, BodyPartInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(headInfoEClass, HeadInfo.class, "HeadInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(handInfoEClass, HandInfo.class, "HandInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHandInfo_IndexFingerTipPosition(), this.getISAVector3(), null, "indexFingerTipPosition", null, 1, 1, HandInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHandInfo_Pose(), this.getHandPose(), "pose", null, 1, 1, HandInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHandInfo_HeldObject(), this.getHandHeldObjectInfo(), null, "heldObject", null, 1, 1, HandInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(handHeldObjectInfoEClass, HandHeldObjectInfo.class, "HandHeldObjectInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHandHeldObjectInfo_Id(), ecorePackage.getEInt(), "id", null, 1, 1, HandHeldObjectInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaVector3EClass, ISAVector3.class, "ISAVector3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAVector3_X(), ecorePackage.getEFloat(), "x", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVector3_Y(), ecorePackage.getEFloat(), "y", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVector3_Z(), ecorePackage.getEFloat(), "z", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(handPoseEEnum, HandPose.class, "HandPose");
		addEEnumLiteral(handPoseEEnum, HandPose.DEFAULT);
		addEEnumLiteral(handPoseEEnum, HandPose.FIST);
		addEEnumLiteral(handPoseEEnum, HandPose.POINTING);
		addEEnumLiteral(handPoseEEnum, HandPose.WHITEBOARD_SPAWN);
		addEEnumLiteral(handPoseEEnum, HandPose.WHITEBOARD);
		addEEnumLiteral(handPoseEEnum, HandPose.PIN);
		addEEnumLiteral(handPoseEEnum, HandPose.PEN);
		addEEnumLiteral(handPoseEEnum, HandPose.PREVIEW);
		addEEnumLiteral(handPoseEEnum, HandPose.CAPSULE);
		addEEnumLiteral(handPoseEEnum, HandPose.STATION);
		addEEnumLiteral(handPoseEEnum, HandPose.UI_SOFTWARE_CANVAS);
		addEEnumLiteral(handPoseEEnum, HandPose.CAMERA);
		addEEnumLiteral(handPoseEEnum, HandPose.PICTURE);
		addEEnumLiteral(handPoseEEnum, HandPose.SYSTEM);
		addEEnumLiteral(handPoseEEnum, HandPose.MICROPHONE);
		addEEnumLiteral(handPoseEEnum, HandPose.UI_SOFTWARE_CANVAS_SCROLL);
	}

} //ClientinfoPackageImpl

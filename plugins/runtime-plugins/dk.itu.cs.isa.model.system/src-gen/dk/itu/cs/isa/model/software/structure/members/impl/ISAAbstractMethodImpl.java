/**
 */
package dk.itu.cs.isa.model.software.structure.members.impl;

import dk.itu.cs.isa.model.software.structure.members.ISAAbstractMethod;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Abstract Method</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ISAAbstractMethodImpl extends ISAMethodImpl implements ISAAbstractMethod {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAAbstractMethodImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MembersPackage.Literals.ISA_ABSTRACT_METHOD;
	}

} //ISAAbstractMethodImpl

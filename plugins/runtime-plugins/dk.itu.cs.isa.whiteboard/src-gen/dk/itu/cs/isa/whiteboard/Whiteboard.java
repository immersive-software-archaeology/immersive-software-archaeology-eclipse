/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Whiteboard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Whiteboard#getId <em>Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Whiteboard#getContainingHistory <em>Containing History</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Whiteboard#getBitmapBase64 <em>Bitmap Base64</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Whiteboard#getPins <em>Pins</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Whiteboard#getPhotos <em>Photos</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Whiteboard#getPosition <em>Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Whiteboard#getRotation <em>Rotation</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Whiteboard#getSize <em>Size</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Whiteboard#getUndoDepth <em>Undo Depth</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard()
 * @model
 * @generated
 */
public interface Whiteboard extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Containing History</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory#getWhiteboards <em>Whiteboards</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing History</em>' container reference.
	 * @see #setContainingHistory(WhiteboardHistory)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard_ContainingHistory()
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardHistory#getWhiteboards
	 * @model opposite="whiteboards" required="true" transient="false"
	 * @generated
	 */
	WhiteboardHistory getContainingHistory();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getContainingHistory <em>Containing History</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containing History</em>' container reference.
	 * @see #getContainingHistory()
	 * @generated
	 */
	void setContainingHistory(WhiteboardHistory value);

	/**
	 * Returns the value of the '<em><b>Bitmap Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bitmap Base64</em>' attribute.
	 * @see #setBitmapBase64(String)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard_BitmapBase64()
	 * @model required="true"
	 * @generated
	 */
	String getBitmapBase64();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getBitmapBase64 <em>Bitmap Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bitmap Base64</em>' attribute.
	 * @see #getBitmapBase64()
	 * @generated
	 */
	void setBitmapBase64(String value);

	/**
	 * Returns the value of the '<em><b>Pins</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.whiteboard.Pin}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.whiteboard.Pin#getWhiteboard <em>Whiteboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pins</em>' containment reference list.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard_Pins()
	 * @see dk.itu.cs.isa.whiteboard.Pin#getWhiteboard
	 * @model opposite="whiteboard" containment="true"
	 * @generated
	 */
	EList<Pin> getPins();

	/**
	 * Returns the value of the '<em><b>Photos</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.whiteboard.Photo}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Photos</em>' containment reference list.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard_Photos()
	 * @model containment="true"
	 * @generated
	 */
	EList<Photo> getPhotos();

	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' containment reference.
	 * @see #setPosition(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard_Position()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getPosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getPosition <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' containment reference.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rotation</em>' containment reference.
	 * @see #setRotation(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard_Rotation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getRotation();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getRotation <em>Rotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rotation</em>' containment reference.
	 * @see #getRotation()
	 * @generated
	 */
	void setRotation(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' attribute.
	 * @see #setSize(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard_Size()
	 * @model required="true"
	 * @generated
	 */
	int getSize();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getSize <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' attribute.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(int value);

	/**
	 * Returns the value of the '<em><b>Undo Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Undo Depth</em>' attribute.
	 * @see #setUndoDepth(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getWhiteboard_UndoDepth()
	 * @model required="true"
	 * @generated
	 */
	int getUndoDepth();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getUndoDepth <em>Undo Depth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Undo Depth</em>' attribute.
	 * @see #getUndoDepth()
	 * @generated
	 */
	void setUndoDepth(int value);

} // Whiteboard

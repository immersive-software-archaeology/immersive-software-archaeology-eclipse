/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Classifier Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer#getClassifiers <em>Classifiers</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClassifierContainer()
 * @model abstract="true"
 * @generated
 */
public interface ISAClassifierContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Classifiers</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifiers</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClassifierContainer_Classifiers()
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<ISAClassifier> getClassifiers();

} // ISAClassifierContainer

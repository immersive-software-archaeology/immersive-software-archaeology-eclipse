/**
 */
package dk.itu.cs.isa.model.software.structure.members;

import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Member</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.ISAMember#getNumberInOrderOfDeclaration <em>Number In Order Of Declaration</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.ISAMember#getStartPositionInDocument <em>Start Position In Document</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.ISAMember#getEndPositionInDocument <em>End Position In Document</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAMember()
 * @model
 * @generated
 */
public interface ISAMember extends ISAReferenceableElement, ISAModifiableElement {
	/**
	 * Returns the value of the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number In Order Of Declaration</em>' attribute.
	 * @see #setNumberInOrderOfDeclaration(int)
	 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAMember_NumberInOrderOfDeclaration()
	 * @model required="true"
	 * @generated
	 */
	int getNumberInOrderOfDeclaration();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.members.ISAMember#getNumberInOrderOfDeclaration <em>Number In Order Of Declaration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number In Order Of Declaration</em>' attribute.
	 * @see #getNumberInOrderOfDeclaration()
	 * @generated
	 */
	void setNumberInOrderOfDeclaration(int value);

	/**
	 * Returns the value of the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Position In Document</em>' attribute.
	 * @see #setStartPositionInDocument(int)
	 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAMember_StartPositionInDocument()
	 * @model required="true"
	 * @generated
	 */
	int getStartPositionInDocument();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.members.ISAMember#getStartPositionInDocument <em>Start Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Position In Document</em>' attribute.
	 * @see #getStartPositionInDocument()
	 * @generated
	 */
	void setStartPositionInDocument(int value);

	/**
	 * Returns the value of the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Position In Document</em>' attribute.
	 * @see #setEndPositionInDocument(int)
	 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAMember_EndPositionInDocument()
	 * @model required="true"
	 * @generated
	 */
	int getEndPositionInDocument();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.members.ISAMember#getEndPositionInDocument <em>End Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Position In Document</em>' attribute.
	 * @see #getEndPositionInDocument()
	 * @generated
	 */
	void setEndPositionInDocument(int value);

} // ISAMember

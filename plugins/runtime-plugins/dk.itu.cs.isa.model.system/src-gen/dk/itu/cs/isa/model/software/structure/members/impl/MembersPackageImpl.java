/**
 */
package dk.itu.cs.isa.model.software.structure.members.impl;

import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage;

import dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;

import dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;

import dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl;

import dk.itu.cs.isa.model.software.structure.impl.StructurePackageImpl;

import dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage;

import dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl;

import dk.itu.cs.isa.model.software.structure.members.ISAAbstractMethod;
import dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.members.MembersFactory;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MembersPackageImpl extends EPackageImpl implements MembersPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaConstructorContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaMemberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaEnumConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaRecordComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaConstructorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaMethodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaConcreteMethodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaAbstractMethodEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MembersPackageImpl() {
		super(eNS_URI, MembersFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link MembersPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MembersPackage init() {
		if (isInited) return (MembersPackage)EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredMembersPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		MembersPackageImpl theMembersPackage = registeredMembersPackage instanceof MembersPackageImpl ? (MembersPackageImpl)registeredMembersPackage : new MembersPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);
		StructurePackageImpl theStructurePackage = (StructurePackageImpl)(registeredPackage instanceof StructurePackageImpl ? registeredPackage : StructurePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);
		ArchitecturePackageImpl theArchitecturePackage = (ArchitecturePackageImpl)(registeredPackage instanceof ArchitecturePackageImpl ? registeredPackage : ArchitecturePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FilesystemPackage.eNS_URI);
		FilesystemPackageImpl theFilesystemPackage = (FilesystemPackageImpl)(registeredPackage instanceof FilesystemPackageImpl ? registeredPackage : FilesystemPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LibrariesPackage.eNS_URI);
		LibrariesPackageImpl theLibrariesPackage = (LibrariesPackageImpl)(registeredPackage instanceof LibrariesPackageImpl ? registeredPackage : LibrariesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl)(registeredPackage instanceof ClassifiersPackageImpl ? registeredPackage : ClassifiersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);
		MiscPackageImpl theMiscPackage = (MiscPackageImpl)(registeredPackage instanceof MiscPackageImpl ? registeredPackage : MiscPackage.eINSTANCE);

		// Create package meta-data objects
		theMembersPackage.createPackageContents();
		theStructurePackage.createPackageContents();
		theArchitecturePackage.createPackageContents();
		theFilesystemPackage.createPackageContents();
		theLibrariesPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theMiscPackage.createPackageContents();

		// Initialize created meta-data
		theMembersPackage.initializePackageContents();
		theStructurePackage.initializePackageContents();
		theArchitecturePackage.initializePackageContents();
		theFilesystemPackage.initializePackageContents();
		theLibrariesPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theMiscPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMembersPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MembersPackage.eNS_URI, theMembersPackage);
		return theMembersPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAConstructorContainer() {
		return isaConstructorContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAConstructorContainer_Constructors() {
		return (EReference)isaConstructorContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAMember() {
		return isaMemberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAMember_NumberInOrderOfDeclaration() {
		return (EAttribute)isaMemberEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAMember_StartPositionInDocument() {
		return (EAttribute)isaMemberEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAMember_EndPositionInDocument() {
		return (EAttribute)isaMemberEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAEnumConstant() {
		return isaEnumConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISARecordComponent() {
		return isaRecordComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAField() {
		return isaFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAConstructor() {
		return isaConstructorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAMethod() {
		return isaMethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAConcreteMethod() {
		return isaConcreteMethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAAbstractMethod() {
		return isaAbstractMethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MembersFactory getMembersFactory() {
		return (MembersFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaConstructorContainerEClass = createEClass(ISA_CONSTRUCTOR_CONTAINER);
		createEReference(isaConstructorContainerEClass, ISA_CONSTRUCTOR_CONTAINER__CONSTRUCTORS);

		isaMemberEClass = createEClass(ISA_MEMBER);
		createEAttribute(isaMemberEClass, ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION);
		createEAttribute(isaMemberEClass, ISA_MEMBER__START_POSITION_IN_DOCUMENT);
		createEAttribute(isaMemberEClass, ISA_MEMBER__END_POSITION_IN_DOCUMENT);

		isaFieldEClass = createEClass(ISA_FIELD);

		isaEnumConstantEClass = createEClass(ISA_ENUM_CONSTANT);

		isaRecordComponentEClass = createEClass(ISA_RECORD_COMPONENT);

		isaConstructorEClass = createEClass(ISA_CONSTRUCTOR);

		isaMethodEClass = createEClass(ISA_METHOD);

		isaConcreteMethodEClass = createEClass(ISA_CONCRETE_METHOD);

		isaAbstractMethodEClass = createEClass(ISA_ABSTRACT_METHOD);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MiscPackage theMiscPackage = (MiscPackage)EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		isaMemberEClass.getESuperTypes().add(theMiscPackage.getISAReferenceableElement());
		isaMemberEClass.getESuperTypes().add(theMiscPackage.getISAModifiableElement());
		isaFieldEClass.getESuperTypes().add(this.getISAMember());
		isaFieldEClass.getESuperTypes().add(theMiscPackage.getISAReferencingElement());
		isaFieldEClass.getESuperTypes().add(theMiscPackage.getISATypedElement());
		isaEnumConstantEClass.getESuperTypes().add(this.getISAMember());
		isaRecordComponentEClass.getESuperTypes().add(this.getISAMember());
		isaRecordComponentEClass.getESuperTypes().add(theMiscPackage.getISATypedElement());
		isaConstructorEClass.getESuperTypes().add(this.getISAMember());
		isaConstructorEClass.getESuperTypes().add(theMiscPackage.getISAStatementContainer());
		isaConstructorEClass.getESuperTypes().add(theMiscPackage.getISAParametrizableElement());
		isaMethodEClass.getESuperTypes().add(this.getISAMember());
		isaMethodEClass.getESuperTypes().add(theMiscPackage.getISATypedElement());
		isaMethodEClass.getESuperTypes().add(theMiscPackage.getISAParametrizableElement());
		isaConcreteMethodEClass.getESuperTypes().add(theMiscPackage.getISAReferencingElement());
		isaConcreteMethodEClass.getESuperTypes().add(this.getISAMethod());
		isaConcreteMethodEClass.getESuperTypes().add(theMiscPackage.getISAStatementContainer());
		isaAbstractMethodEClass.getESuperTypes().add(this.getISAMethod());

		// Initialize classes, features, and operations; add parameters
		initEClass(isaConstructorContainerEClass, ISAConstructorContainer.class, "ISAConstructorContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAConstructorContainer_Constructors(), this.getISAConstructor(), null, "constructors", null, 0, -1, ISAConstructorContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaMemberEClass, ISAMember.class, "ISAMember", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAMember_NumberInOrderOfDeclaration(), ecorePackage.getEInt(), "numberInOrderOfDeclaration", null, 1, 1, ISAMember.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAMember_StartPositionInDocument(), ecorePackage.getEInt(), "startPositionInDocument", null, 1, 1, ISAMember.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAMember_EndPositionInDocument(), ecorePackage.getEInt(), "endPositionInDocument", null, 1, 1, ISAMember.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaFieldEClass, ISAField.class, "ISAField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isaEnumConstantEClass, ISAEnumConstant.class, "ISAEnumConstant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isaRecordComponentEClass, ISARecordComponent.class, "ISARecordComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isaConstructorEClass, ISAConstructor.class, "ISAConstructor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isaMethodEClass, ISAMethod.class, "ISAMethod", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isaConcreteMethodEClass, ISAConcreteMethod.class, "ISAConcreteMethod", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isaAbstractMethodEClass, ISAAbstractMethod.class, "ISAAbstractMethod", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //MembersPackageImpl

/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage
 * @generated
 */
public interface ClientinfoFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClientinfoFactory eINSTANCE = dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Client Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Client Info</em>'.
	 * @generated
	 */
	ClientInfo createClientInfo();

	/**
	 * Returns a new object of class '<em>Head Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Head Info</em>'.
	 * @generated
	 */
	HeadInfo createHeadInfo();

	/**
	 * Returns a new object of class '<em>Hand Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hand Info</em>'.
	 * @generated
	 */
	HandInfo createHandInfo();

	/**
	 * Returns a new object of class '<em>Hand Held Object Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hand Held Object Info</em>'.
	 * @generated
	 */
	HandHeldObjectInfo createHandHeldObjectInfo();

	/**
	 * Returns a new object of class '<em>ISA Vector3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Vector3</em>'.
	 * @generated
	 */
	ISAVector3 createISAVector3();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ClientinfoPackage getClientinfoPackage();

} //ClientinfoFactory

package dk.itu.cs.isa.model.system.creation.srcml.ui;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

import dk.itu.cs.isa.model.system.creation.srcml.preferences.ISAPreferencesSrcML;
import dk.itu.cs.isa.preferences.ISAPreferences;

public class SrcMLPreferencesUIPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	FileFieldEditor srcMlExecutableEditor;

	public SrcMLPreferencesUIPage() {
		// TODO add fancy image!
		super(GRID);
	}
	
	

	@Override
	public void createFieldEditors() {
		
		addSpace();
		
		srcMlExecutableEditor = new FileFieldEditor(ISAPreferencesSrcML.SRCML_EXECUTABLE_PATH, "&SrcML executable:", getFieldEditorParent());
		addField(srcMlExecutableEditor);

		addSpace();
		
	}

	private void addSpace() {
		new Label(getFieldEditorParent(), SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
	}
	
	

	@Override
	public void init(IWorkbench workbench) {
		setTitle("ISA - Immersive Software Archeology");
		setDescription("Settings page for system structure analysis with SrcML");

		setPreferenceStore(new ScopedPreferenceStore(InstanceScope.INSTANCE, ISAPreferencesSrcML.STORAGE_ID));
	}
	
	
	
	@Override
	public void performDefaults() {
		super.performDefaults();
		
		new ISAPreferencesSrcML().initializeDefaultPreferences();
		srcMlExecutableEditor.setStringValue(ISAPreferences.getString(ISAPreferencesSrcML.SRCML_EXECUTABLE_PATH, ""));
		
		checkState();
		updateApplyButton();
	}

}

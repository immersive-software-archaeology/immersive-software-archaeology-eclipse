/**
 */
package dk.itu.cs.isa.multiplayer.impl;

import dk.itu.cs.isa.multiplayer.ClearAllRelationsEvent;
import dk.itu.cs.isa.multiplayer.MultiplayerPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Clear All Relations Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClearAllRelationsEventImpl extends AbstractEventImpl implements ClearAllRelationsEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClearAllRelationsEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MultiplayerPackage.Literals.CLEAR_ALL_RELATIONS_EVENT;
	}

} //ClearAllRelationsEventImpl

/**
 */
package dk.itu.cs.isa.model.software.structure.misc;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Modifiable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement#getModifiers <em>Modifiers</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAModifiableElement()
 * @model abstract="true"
 * @generated
 */
public interface ISAModifiableElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Modifiers</b></em>' attribute list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISAModifier}.
	 * The literals are from the enumeration {@link dk.itu.cs.isa.model.software.structure.misc.ISAModifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modifiers</em>' attribute list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAModifier
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAModifiableElement_Modifiers()
	 * @model
	 * @generated
	 */
	EList<ISAModifier> getModifiers();

} // ISAModifiableElement

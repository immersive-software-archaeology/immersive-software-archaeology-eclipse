/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Whiteboard Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getSubmittingClientId <em>Submitting Client Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getWhiteboardId <em>Whiteboard Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#isConfirmationRequired <em>Confirmation Required</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractWhiteboardEvent()
 * @model
 * @generated
 */
public interface AbstractWhiteboardEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Submitting Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submitting Client Id</em>' attribute.
	 * @see #setSubmittingClientId(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractWhiteboardEvent_SubmittingClientId()
	 * @model required="true"
	 * @generated
	 */
	int getSubmittingClientId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getSubmittingClientId <em>Submitting Client Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Submitting Client Id</em>' attribute.
	 * @see #getSubmittingClientId()
	 * @generated
	 */
	void setSubmittingClientId(int value);

	/**
	 * Returns the value of the '<em><b>Whiteboard Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Whiteboard Id</em>' attribute.
	 * @see #setWhiteboardId(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractWhiteboardEvent_WhiteboardId()
	 * @model required="true"
	 * @generated
	 */
	int getWhiteboardId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getWhiteboardId <em>Whiteboard Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Whiteboard Id</em>' attribute.
	 * @see #getWhiteboardId()
	 * @generated
	 */
	void setWhiteboardId(int value);

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(long)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractWhiteboardEvent_Timestamp()
	 * @model id="true" required="true"
	 * @generated
	 */
	long getTimestamp();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(long value);

	/**
	 * Returns the value of the '<em><b>Confirmation Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Confirmation Required</em>' attribute.
	 * @see #setConfirmationRequired(boolean)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractWhiteboardEvent_ConfirmationRequired()
	 * @model required="true"
	 * @generated
	 */
	boolean isConfirmationRequired();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent#isConfirmationRequired <em>Confirmation Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Confirmation Required</em>' attribute.
	 * @see #isConfirmationRequired()
	 * @generated
	 */
	void setConfirmationRequired(boolean value);

} // AbstractWhiteboardEvent

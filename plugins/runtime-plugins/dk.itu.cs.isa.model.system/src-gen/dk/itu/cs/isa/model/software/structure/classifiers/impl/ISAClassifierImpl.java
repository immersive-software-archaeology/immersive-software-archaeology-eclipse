/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer;

import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;

import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;

import dk.itu.cs.isa.model.software.structure.members.MembersPackage;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifier;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.ISATypeImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Classifier</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl#getClassifiers <em>Classifiers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl#getModifiers <em>Modifiers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl#getNumberInOrderOfDeclaration <em>Number In Order Of Declaration</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl#getStartPositionInDocument <em>Start Position In Document</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl#getEndPositionInDocument <em>End Position In Document</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl#getFields <em>Fields</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl#getMethods <em>Methods</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl#getFilePath <em>File Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ISAClassifierImpl extends ISATypeImpl implements ISAClassifier {
	/**
	 * The cached value of the '{@link #getClassifiers() <em>Classifiers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAClassifier> classifiers;

	/**
	 * The cached value of the '{@link #getModifiers() <em>Modifiers</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAModifier> modifiers;

	/**
	 * The default value of the '{@link #getNumberInOrderOfDeclaration() <em>Number In Order Of Declaration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberInOrderOfDeclaration()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberInOrderOfDeclaration() <em>Number In Order Of Declaration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberInOrderOfDeclaration()
	 * @generated
	 * @ordered
	 */
	protected int numberInOrderOfDeclaration = NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartPositionInDocument() <em>Start Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected static final int START_POSITION_IN_DOCUMENT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStartPositionInDocument() <em>Start Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected int startPositionInDocument = START_POSITION_IN_DOCUMENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndPositionInDocument() <em>End Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected static final int END_POSITION_IN_DOCUMENT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEndPositionInDocument() <em>End Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected int endPositionInDocument = END_POSITION_IN_DOCUMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFields() <em>Fields</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFields()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAField> fields;

	/**
	 * The cached value of the '{@link #getMethods() <em>Methods</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethods()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAMethod> methods;

	/**
	 * The default value of the '{@link #getFilePath() <em>File Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilePath()
	 * @generated
	 * @ordered
	 */
	protected static final String FILE_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilePath() <em>File Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilePath()
	 * @generated
	 * @ordered
	 */
	protected String filePath = FILE_PATH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAClassifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.ISA_CLASSIFIER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAModifier> getModifiers() {
		if (modifiers == null) {
			modifiers = new EDataTypeUniqueEList<ISAModifier>(ISAModifier.class, this, ClassifiersPackage.ISA_CLASSIFIER__MODIFIERS);
		}
		return modifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberInOrderOfDeclaration() {
		return numberInOrderOfDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberInOrderOfDeclaration(int newNumberInOrderOfDeclaration) {
		int oldNumberInOrderOfDeclaration = numberInOrderOfDeclaration;
		numberInOrderOfDeclaration = newNumberInOrderOfDeclaration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassifiersPackage.ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION, oldNumberInOrderOfDeclaration, numberInOrderOfDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getStartPositionInDocument() {
		return startPositionInDocument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartPositionInDocument(int newStartPositionInDocument) {
		int oldStartPositionInDocument = startPositionInDocument;
		startPositionInDocument = newStartPositionInDocument;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassifiersPackage.ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT, oldStartPositionInDocument, startPositionInDocument));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getEndPositionInDocument() {
		return endPositionInDocument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndPositionInDocument(int newEndPositionInDocument) {
		int oldEndPositionInDocument = endPositionInDocument;
		endPositionInDocument = newEndPositionInDocument;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassifiersPackage.ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT, oldEndPositionInDocument, endPositionInDocument));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAClassifier> getClassifiers() {
		if (classifiers == null) {
			classifiers = new EObjectContainmentWithInverseEList<ISAClassifier>(ISAClassifier.class, this, ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS, ClassifiersPackage.ISA_CLASSIFIER__PARENT);
		}
		return classifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAClassifierContainer getParent() {
		if (eContainerFeatureID() != ClassifiersPackage.ISA_CLASSIFIER__PARENT) return null;
		return (ISAClassifierContainer)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(ISAClassifierContainer newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, ClassifiersPackage.ISA_CLASSIFIER__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParent(ISAClassifierContainer newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != ClassifiersPackage.ISA_CLASSIFIER__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, ClassifiersPackage.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS, ISAClassifierContainer.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassifiersPackage.ISA_CLASSIFIER__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAField> getFields() {
		if (fields == null) {
			fields = new EObjectContainmentEList<ISAField>(ISAField.class, this, ClassifiersPackage.ISA_CLASSIFIER__FIELDS);
		}
		return fields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAMethod> getMethods() {
		if (methods == null) {
			methods = new EObjectContainmentEList<ISAMethod>(ISAMethod.class, this, ClassifiersPackage.ISA_CLASSIFIER__METHODS);
		}
		return methods;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFilePath() {
		return filePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFilePath(String newFilePath) {
		String oldFilePath = filePath;
		filePath = newFilePath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClassifiersPackage.ISA_CLASSIFIER__FILE_PATH, oldFilePath, filePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAPackage getContainingPackage() {
		return dk.itu.cs.isa.model.software.structure.util.ISASystemModelHelper.getContainingPackage(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAMember> getContainedMembers() {
		return dk.itu.cs.isa.model.software.structure.util.ISASystemModelHelper.getAllContainedMembers(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getClassifiers()).basicAdd(otherEnd, msgs);
			case ClassifiersPackage.ISA_CLASSIFIER__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((ISAClassifierContainer)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS:
				return ((InternalEList<?>)getClassifiers()).basicRemove(otherEnd, msgs);
			case ClassifiersPackage.ISA_CLASSIFIER__PARENT:
				return basicSetParent(null, msgs);
			case ClassifiersPackage.ISA_CLASSIFIER__FIELDS:
				return ((InternalEList<?>)getFields()).basicRemove(otherEnd, msgs);
			case ClassifiersPackage.ISA_CLASSIFIER__METHODS:
				return ((InternalEList<?>)getMethods()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case ClassifiersPackage.ISA_CLASSIFIER__PARENT:
				return eInternalContainer().eInverseRemove(this, ClassifiersPackage.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS, ISAClassifierContainer.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS:
				return getClassifiers();
			case ClassifiersPackage.ISA_CLASSIFIER__MODIFIERS:
				return getModifiers();
			case ClassifiersPackage.ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION:
				return getNumberInOrderOfDeclaration();
			case ClassifiersPackage.ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT:
				return getStartPositionInDocument();
			case ClassifiersPackage.ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT:
				return getEndPositionInDocument();
			case ClassifiersPackage.ISA_CLASSIFIER__PARENT:
				return getParent();
			case ClassifiersPackage.ISA_CLASSIFIER__FIELDS:
				return getFields();
			case ClassifiersPackage.ISA_CLASSIFIER__METHODS:
				return getMethods();
			case ClassifiersPackage.ISA_CLASSIFIER__FILE_PATH:
				return getFilePath();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS:
				getClassifiers().clear();
				getClassifiers().addAll((Collection<? extends ISAClassifier>)newValue);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__MODIFIERS:
				getModifiers().clear();
				getModifiers().addAll((Collection<? extends ISAModifier>)newValue);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION:
				setNumberInOrderOfDeclaration((Integer)newValue);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT:
				setStartPositionInDocument((Integer)newValue);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT:
				setEndPositionInDocument((Integer)newValue);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__PARENT:
				setParent((ISAClassifierContainer)newValue);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__FIELDS:
				getFields().clear();
				getFields().addAll((Collection<? extends ISAField>)newValue);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__METHODS:
				getMethods().clear();
				getMethods().addAll((Collection<? extends ISAMethod>)newValue);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__FILE_PATH:
				setFilePath((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS:
				getClassifiers().clear();
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__MODIFIERS:
				getModifiers().clear();
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION:
				setNumberInOrderOfDeclaration(NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT:
				setStartPositionInDocument(START_POSITION_IN_DOCUMENT_EDEFAULT);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT:
				setEndPositionInDocument(END_POSITION_IN_DOCUMENT_EDEFAULT);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__PARENT:
				setParent((ISAClassifierContainer)null);
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__FIELDS:
				getFields().clear();
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__METHODS:
				getMethods().clear();
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__FILE_PATH:
				setFilePath(FILE_PATH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS:
				return classifiers != null && !classifiers.isEmpty();
			case ClassifiersPackage.ISA_CLASSIFIER__MODIFIERS:
				return modifiers != null && !modifiers.isEmpty();
			case ClassifiersPackage.ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION:
				return numberInOrderOfDeclaration != NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT;
			case ClassifiersPackage.ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT:
				return startPositionInDocument != START_POSITION_IN_DOCUMENT_EDEFAULT;
			case ClassifiersPackage.ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT:
				return endPositionInDocument != END_POSITION_IN_DOCUMENT_EDEFAULT;
			case ClassifiersPackage.ISA_CLASSIFIER__PARENT:
				return getParent() != null;
			case ClassifiersPackage.ISA_CLASSIFIER__FIELDS:
				return fields != null && !fields.isEmpty();
			case ClassifiersPackage.ISA_CLASSIFIER__METHODS:
				return methods != null && !methods.isEmpty();
			case ClassifiersPackage.ISA_CLASSIFIER__FILE_PATH:
				return FILE_PATH_EDEFAULT == null ? filePath != null : !FILE_PATH_EDEFAULT.equals(filePath);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAClassifierContainer.class) {
			switch (derivedFeatureID) {
				case ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS: return ClassifiersPackage.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS;
				default: return -1;
			}
		}
		if (baseClass == ISAModifiableElement.class) {
			switch (derivedFeatureID) {
				case ClassifiersPackage.ISA_CLASSIFIER__MODIFIERS: return MiscPackage.ISA_MODIFIABLE_ELEMENT__MODIFIERS;
				default: return -1;
			}
		}
		if (baseClass == ISAMember.class) {
			switch (derivedFeatureID) {
				case ClassifiersPackage.ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION: return MembersPackage.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION;
				case ClassifiersPackage.ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT: return MembersPackage.ISA_MEMBER__START_POSITION_IN_DOCUMENT;
				case ClassifiersPackage.ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT: return MembersPackage.ISA_MEMBER__END_POSITION_IN_DOCUMENT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAClassifierContainer.class) {
			switch (baseFeatureID) {
				case ClassifiersPackage.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS: return ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS;
				default: return -1;
			}
		}
		if (baseClass == ISAModifiableElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_MODIFIABLE_ELEMENT__MODIFIERS: return ClassifiersPackage.ISA_CLASSIFIER__MODIFIERS;
				default: return -1;
			}
		}
		if (baseClass == ISAMember.class) {
			switch (baseFeatureID) {
				case MembersPackage.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION: return ClassifiersPackage.ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION;
				case MembersPackage.ISA_MEMBER__START_POSITION_IN_DOCUMENT: return ClassifiersPackage.ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT;
				case MembersPackage.ISA_MEMBER__END_POSITION_IN_DOCUMENT: return ClassifiersPackage.ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ClassifiersPackage.ISA_CLASSIFIER___GET_CONTAINING_PACKAGE:
				return getContainingPackage();
			case ClassifiersPackage.ISA_CLASSIFIER___GET_CONTAINED_MEMBERS:
				return getContainedMembers();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (modifiers: ");
		result.append(modifiers);
		result.append(", numberInOrderOfDeclaration: ");
		result.append(numberInOrderOfDeclaration);
		result.append(", startPositionInDocument: ");
		result.append(startPositionInDocument);
		result.append(", endPositionInDocument: ");
		result.append(endPositionInDocument);
		result.append(", filePath: ");
		result.append(filePath);
		result.append(')');
		return result.toString();
	}

} //ISAClassifierImpl

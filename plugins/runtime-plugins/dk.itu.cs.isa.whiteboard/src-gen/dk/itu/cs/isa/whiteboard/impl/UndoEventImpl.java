/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.UndoEvent;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Undo Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UndoEventImpl extends AbstractWhiteboardEventImpl implements UndoEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UndoEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.UNDO_EVENT;
	}

} //UndoEventImpl

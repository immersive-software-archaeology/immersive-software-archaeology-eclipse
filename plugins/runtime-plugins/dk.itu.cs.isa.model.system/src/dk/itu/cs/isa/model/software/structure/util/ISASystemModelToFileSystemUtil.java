package dk.itu.cs.isa.model.software.structure.util;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;

public class ISASystemModelToFileSystemUtil {
	
	public static IFile getFileForClassifier(ISAClassifier isaClassifier) {
		if(isaClassifier == null)
			return null;
		
		List<ISAPackage> isaPackageFragments = ISASystemModelHelper.getPackageFragments(isaClassifier);
		ISAPackageRoot isaPackageRoot = ISASystemModelHelper.getContainingPackageRoot(isaClassifier);
		ISAProject project = isaPackageRoot.getParentProject();
		
		IProject eclipseProject = ResourcesPlugin.getWorkspace().getRoot().getProject(project.getName());
		IFolder packageRootFolder = eclipseProject.getFolder(isaPackageRoot.getName());
		if(!packageRootFolder.exists())
			return null;
		
		IFolder currentPackageFolder = packageRootFolder;
		for(ISAPackage packageFragment : isaPackageFragments) {
			currentPackageFolder = currentPackageFolder.getFolder(packageFragment.getName());
			if(!currentPackageFolder.exists())
				return null;
		}
		
		// TODO if byte code support is added, check whether or not
		//      the classifier is contained in a .class file!
		IFile classifierFile = currentPackageFolder.getFile(isaClassifier.getName() + ".java");
		if(classifierFile.exists())
			return classifierFile;
		
		// File could not be found by following the folder hierarchy according to the element's qualified name
		// -> See if the file was explicitly set
		
		if(isaClassifier.getFilePath() != null) {
			IPath path = new Path(isaClassifier.getFilePath());
			eclipseProject = ResourcesPlugin.getWorkspace().getRoot().getProject(path.segment(0));
			
			path = path.removeFirstSegments(1);
			classifierFile = eclipseProject.getFile(path);
			
			if(classifierFile.exists())
				return classifierFile;
		}
		
		return null;
	}
	
}

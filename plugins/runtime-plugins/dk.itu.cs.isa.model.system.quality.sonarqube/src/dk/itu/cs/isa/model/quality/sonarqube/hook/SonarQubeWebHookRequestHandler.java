package dk.itu.cs.isa.model.quality.sonarqube.hook;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

public class SonarQubeWebHookRequestHandler extends AbstractHandler {
	
    @Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		if(pathEquals(target, SonarQubeWebHookServer.WEB_HOOK_URL_SUFFIX)) {
			// notify scanner that data was received :-)
			SonarQubeWebHookServer.INSTANCE.getQualityAnalyzer().notifySonarScannerHasFinished();
			
			response.setContentType("application/json; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
		else {
			response.setContentType("text/html; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);

			PrintWriter out = response.getWriter();
			out.println("<h1>Hm? What are YOU doing here?</h1>");
			out.println("<p>This server is thought to act as web hook for SonarQube. "
					+ "It is launched within the context of ISA's Eclipse plugins and used "
					+ "to trigger further analysis as soon as SonarQube is finished with "
					+ "processing the scanning results!</p>");
			out.println("There is nothing interesting for you to see here...");
			out.close();
		}

		baseRequest.setHandled(true);
	}
	
	protected boolean pathEquals(String target, String compare) {
		if(target.startsWith("/"))
			target = target.substring(1);
		if(compare.startsWith("/"))
			compare = compare.substring(1);

		if(target.endsWith("/"))
			target = target.substring(0, target.length()-1);
		if(compare.endsWith("/"))
			compare = compare.substring(0, target.length()-1);
			
		String[] targetSegments = target.trim().split("/");
		String[] compareSegments = compare.trim().split("/");
		
		if(targetSegments.length != compareSegments.length) {
			return false;
		}
		
		for(int i=0; i<targetSegments.length; i++) {
			if(!targetSegments[i].equalsIgnoreCase(compareSegments[i])) {
				return false;
			}
		}
		
		return true;
	}

}
/**
 */
package dk.itu.cs.isa.model.software.structure.members;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.members.MembersFactory
 * @model kind="package"
 * @generated
 */
public interface MembersPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "members";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/software/structure/members";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "members";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MembersPackage eINSTANCE = dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorContainerImpl <em>ISA Constructor Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorContainerImpl
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAConstructorContainer()
	 * @generated
	 */
	int ISA_CONSTRUCTOR_CONTAINER = 0;

	/**
	 * The feature id for the '<em><b>Constructors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR_CONTAINER__CONSTRUCTORS = 0;

	/**
	 * The number of structural features of the '<em>ISA Constructor Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>ISA Constructor Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMemberImpl <em>ISA Member</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAMemberImpl
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAMember()
	 * @generated
	 */
	int ISA_MEMBER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER__NAME = MiscPackage.ISA_REFERENCEABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER__INCOMING_REFERENCES = MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER__MODIFIERS = MiscPackage.ISA_REFERENCEABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION = MiscPackage.ISA_REFERENCEABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER__START_POSITION_IN_DOCUMENT = MiscPackage.ISA_REFERENCEABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER__END_POSITION_IN_DOCUMENT = MiscPackage.ISA_REFERENCEABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>ISA Member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER_FEATURE_COUNT = MiscPackage.ISA_REFERENCEABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER___GET_FULLY_QUALIFIED_NAME = MiscPackage.ISA_REFERENCEABLE_ELEMENT___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER___TO_STRING = MiscPackage.ISA_REFERENCEABLE_ELEMENT___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEMBER_OPERATION_COUNT = MiscPackage.ISA_REFERENCEABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAEnumConstantImpl <em>ISA Enum Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAEnumConstantImpl
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAEnumConstant()
	 * @generated
	 */
	int ISA_ENUM_CONSTANT = 3;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISARecordComponentImpl <em>ISA Record Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISARecordComponentImpl
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISARecordComponent()
	 * @generated
	 */
	int ISA_RECORD_COMPONENT = 4;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl <em>ISA Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAField()
	 * @generated
	 */
	int ISA_FIELD = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__NAME = ISA_MEMBER__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__INCOMING_REFERENCES = ISA_MEMBER__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__MODIFIERS = ISA_MEMBER__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__NUMBER_IN_ORDER_OF_DECLARATION = ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__START_POSITION_IN_DOCUMENT = ISA_MEMBER__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__END_POSITION_IN_DOCUMENT = ISA_MEMBER__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Type References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__TYPE_REFERENCES = ISA_MEMBER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constructor Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__CONSTRUCTOR_CALLS = ISA_MEMBER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Method Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__METHOD_CALLS = ISA_MEMBER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Field Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__FIELD_ACCESSES = ISA_MEMBER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Constant Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__CONSTANT_ACCESSES = ISA_MEMBER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Component Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__COMPONENT_ACCESSES = ISA_MEMBER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__TYPE = ISA_MEMBER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Array Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD__ARRAY_TYPE = ISA_MEMBER_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>ISA Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD_FEATURE_COUNT = ISA_MEMBER_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD___GET_FULLY_QUALIFIED_NAME = ISA_MEMBER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD___TO_STRING = ISA_MEMBER___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FIELD_OPERATION_COUNT = ISA_MEMBER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT__NAME = ISA_MEMBER__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT__INCOMING_REFERENCES = ISA_MEMBER__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT__MODIFIERS = ISA_MEMBER__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT__NUMBER_IN_ORDER_OF_DECLARATION = ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT__START_POSITION_IN_DOCUMENT = ISA_MEMBER__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT__END_POSITION_IN_DOCUMENT = ISA_MEMBER__END_POSITION_IN_DOCUMENT;

	/**
	 * The number of structural features of the '<em>ISA Enum Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT_FEATURE_COUNT = ISA_MEMBER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT___GET_FULLY_QUALIFIED_NAME = ISA_MEMBER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT___TO_STRING = ISA_MEMBER___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Enum Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ENUM_CONSTANT_OPERATION_COUNT = ISA_MEMBER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT__NAME = ISA_MEMBER__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT__INCOMING_REFERENCES = ISA_MEMBER__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT__MODIFIERS = ISA_MEMBER__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT__NUMBER_IN_ORDER_OF_DECLARATION = ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT__START_POSITION_IN_DOCUMENT = ISA_MEMBER__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT__END_POSITION_IN_DOCUMENT = ISA_MEMBER__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT__TYPE = ISA_MEMBER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Array Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT__ARRAY_TYPE = ISA_MEMBER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>ISA Record Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT_FEATURE_COUNT = ISA_MEMBER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT___GET_FULLY_QUALIFIED_NAME = ISA_MEMBER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT___TO_STRING = ISA_MEMBER___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Record Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_RECORD_COMPONENT_OPERATION_COUNT = ISA_MEMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl <em>ISA Constructor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAConstructor()
	 * @generated
	 */
	int ISA_CONSTRUCTOR = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__NAME = ISA_MEMBER__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__INCOMING_REFERENCES = ISA_MEMBER__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__MODIFIERS = ISA_MEMBER__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__NUMBER_IN_ORDER_OF_DECLARATION = ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__START_POSITION_IN_DOCUMENT = ISA_MEMBER__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__END_POSITION_IN_DOCUMENT = ISA_MEMBER__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Type References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__TYPE_REFERENCES = ISA_MEMBER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constructor Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__CONSTRUCTOR_CALLS = ISA_MEMBER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Method Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__METHOD_CALLS = ISA_MEMBER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Field Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__FIELD_ACCESSES = ISA_MEMBER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Constant Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__CONSTANT_ACCESSES = ISA_MEMBER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Component Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__COMPONENT_ACCESSES = ISA_MEMBER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Number Of Statements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__NUMBER_OF_STATEMENTS = ISA_MEMBER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Number Of Expressions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__NUMBER_OF_EXPRESSIONS = ISA_MEMBER_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Number Of Control Flow Splits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__NUMBER_OF_CONTROL_FLOW_SPLITS = ISA_MEMBER_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Cognitive Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__COGNITIVE_COMPLEXITY = ISA_MEMBER_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR__PARAMETERS = ISA_MEMBER_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>ISA Constructor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR_FEATURE_COUNT = ISA_MEMBER_FEATURE_COUNT + 11;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR___GET_FULLY_QUALIFIED_NAME = ISA_MEMBER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR___TO_STRING = ISA_MEMBER___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Constructor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONSTRUCTOR_OPERATION_COUNT = ISA_MEMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMethodImpl <em>ISA Method</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAMethodImpl
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAMethod()
	 * @generated
	 */
	int ISA_METHOD = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD__NAME = ISA_MEMBER__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD__INCOMING_REFERENCES = ISA_MEMBER__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD__MODIFIERS = ISA_MEMBER__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD__NUMBER_IN_ORDER_OF_DECLARATION = ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD__START_POSITION_IN_DOCUMENT = ISA_MEMBER__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD__END_POSITION_IN_DOCUMENT = ISA_MEMBER__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD__TYPE = ISA_MEMBER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Array Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD__ARRAY_TYPE = ISA_MEMBER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD__PARAMETERS = ISA_MEMBER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ISA Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD_FEATURE_COUNT = ISA_MEMBER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD___GET_FULLY_QUALIFIED_NAME = ISA_MEMBER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD___TO_STRING = ISA_MEMBER___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_METHOD_OPERATION_COUNT = ISA_MEMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl <em>ISA Concrete Method</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAConcreteMethod()
	 * @generated
	 */
	int ISA_CONCRETE_METHOD = 7;

	/**
	 * The feature id for the '<em><b>Type References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__TYPE_REFERENCES = MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES;

	/**
	 * The feature id for the '<em><b>Constructor Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__CONSTRUCTOR_CALLS = MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS;

	/**
	 * The feature id for the '<em><b>Method Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__METHOD_CALLS = MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS;

	/**
	 * The feature id for the '<em><b>Field Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__FIELD_ACCESSES = MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES;

	/**
	 * The feature id for the '<em><b>Constant Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__CONSTANT_ACCESSES = MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES;

	/**
	 * The feature id for the '<em><b>Component Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__COMPONENT_ACCESSES = MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__NAME = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__INCOMING_REFERENCES = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__MODIFIERS = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__NUMBER_IN_ORDER_OF_DECLARATION = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__START_POSITION_IN_DOCUMENT = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__END_POSITION_IN_DOCUMENT = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__TYPE = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Array Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__ARRAY_TYPE = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__PARAMETERS = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Number Of Statements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__NUMBER_OF_STATEMENTS = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Number Of Expressions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__NUMBER_OF_EXPRESSIONS = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Number Of Control Flow Splits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__NUMBER_OF_CONTROL_FLOW_SPLITS = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Cognitive Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD__COGNITIVE_COMPLEXITY = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>ISA Concrete Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD_FEATURE_COUNT = MiscPackage.ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD___GET_FULLY_QUALIFIED_NAME = MiscPackage.ISA_REFERENCING_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD___TO_STRING = MiscPackage.ISA_REFERENCING_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>ISA Concrete Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CONCRETE_METHOD_OPERATION_COUNT = MiscPackage.ISA_REFERENCING_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAAbstractMethodImpl <em>ISA Abstract Method</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAAbstractMethodImpl
	 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAAbstractMethod()
	 * @generated
	 */
	int ISA_ABSTRACT_METHOD = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD__NAME = ISA_METHOD__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD__INCOMING_REFERENCES = ISA_METHOD__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD__MODIFIERS = ISA_METHOD__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD__NUMBER_IN_ORDER_OF_DECLARATION = ISA_METHOD__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD__START_POSITION_IN_DOCUMENT = ISA_METHOD__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD__END_POSITION_IN_DOCUMENT = ISA_METHOD__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD__TYPE = ISA_METHOD__TYPE;

	/**
	 * The feature id for the '<em><b>Array Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD__ARRAY_TYPE = ISA_METHOD__ARRAY_TYPE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD__PARAMETERS = ISA_METHOD__PARAMETERS;

	/**
	 * The number of structural features of the '<em>ISA Abstract Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD_FEATURE_COUNT = ISA_METHOD_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD___GET_FULLY_QUALIFIED_NAME = ISA_METHOD___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD___TO_STRING = ISA_METHOD___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Abstract Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_METHOD_OPERATION_COUNT = ISA_METHOD_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer <em>ISA Constructor Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Constructor Container</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer
	 * @generated
	 */
	EClass getISAConstructorContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer#getConstructors <em>Constructors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constructors</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer#getConstructors()
	 * @see #getISAConstructorContainer()
	 * @generated
	 */
	EReference getISAConstructorContainer_Constructors();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.members.ISAMember <em>ISA Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Member</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAMember
	 * @generated
	 */
	EClass getISAMember();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.members.ISAMember#getNumberInOrderOfDeclaration <em>Number In Order Of Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number In Order Of Declaration</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAMember#getNumberInOrderOfDeclaration()
	 * @see #getISAMember()
	 * @generated
	 */
	EAttribute getISAMember_NumberInOrderOfDeclaration();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.members.ISAMember#getStartPositionInDocument <em>Start Position In Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Position In Document</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAMember#getStartPositionInDocument()
	 * @see #getISAMember()
	 * @generated
	 */
	EAttribute getISAMember_StartPositionInDocument();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.members.ISAMember#getEndPositionInDocument <em>End Position In Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Position In Document</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAMember#getEndPositionInDocument()
	 * @see #getISAMember()
	 * @generated
	 */
	EAttribute getISAMember_EndPositionInDocument();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant <em>ISA Enum Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Enum Constant</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant
	 * @generated
	 */
	EClass getISAEnumConstant();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.members.ISARecordComponent <em>ISA Record Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Record Component</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISARecordComponent
	 * @generated
	 */
	EClass getISARecordComponent();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.members.ISAField <em>ISA Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Field</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAField
	 * @generated
	 */
	EClass getISAField();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.members.ISAConstructor <em>ISA Constructor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Constructor</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAConstructor
	 * @generated
	 */
	EClass getISAConstructor();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.members.ISAMethod <em>ISA Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Method</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAMethod
	 * @generated
	 */
	EClass getISAMethod();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod <em>ISA Concrete Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Concrete Method</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod
	 * @generated
	 */
	EClass getISAConcreteMethod();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.members.ISAAbstractMethod <em>ISA Abstract Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Abstract Method</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAAbstractMethod
	 * @generated
	 */
	EClass getISAAbstractMethod();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MembersFactory getMembersFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorContainerImpl <em>ISA Constructor Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorContainerImpl
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAConstructorContainer()
		 * @generated
		 */
		EClass ISA_CONSTRUCTOR_CONTAINER = eINSTANCE.getISAConstructorContainer();

		/**
		 * The meta object literal for the '<em><b>Constructors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CONSTRUCTOR_CONTAINER__CONSTRUCTORS = eINSTANCE.getISAConstructorContainer_Constructors();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMemberImpl <em>ISA Member</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAMemberImpl
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAMember()
		 * @generated
		 */
		EClass ISA_MEMBER = eINSTANCE.getISAMember();

		/**
		 * The meta object literal for the '<em><b>Number In Order Of Declaration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION = eINSTANCE.getISAMember_NumberInOrderOfDeclaration();

		/**
		 * The meta object literal for the '<em><b>Start Position In Document</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_MEMBER__START_POSITION_IN_DOCUMENT = eINSTANCE.getISAMember_StartPositionInDocument();

		/**
		 * The meta object literal for the '<em><b>End Position In Document</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_MEMBER__END_POSITION_IN_DOCUMENT = eINSTANCE.getISAMember_EndPositionInDocument();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAEnumConstantImpl <em>ISA Enum Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAEnumConstantImpl
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAEnumConstant()
		 * @generated
		 */
		EClass ISA_ENUM_CONSTANT = eINSTANCE.getISAEnumConstant();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISARecordComponentImpl <em>ISA Record Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISARecordComponentImpl
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISARecordComponent()
		 * @generated
		 */
		EClass ISA_RECORD_COMPONENT = eINSTANCE.getISARecordComponent();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl <em>ISA Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAField()
		 * @generated
		 */
		EClass ISA_FIELD = eINSTANCE.getISAField();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl <em>ISA Constructor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAConstructor()
		 * @generated
		 */
		EClass ISA_CONSTRUCTOR = eINSTANCE.getISAConstructor();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMethodImpl <em>ISA Method</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAMethodImpl
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAMethod()
		 * @generated
		 */
		EClass ISA_METHOD = eINSTANCE.getISAMethod();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl <em>ISA Concrete Method</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAConcreteMethod()
		 * @generated
		 */
		EClass ISA_CONCRETE_METHOD = eINSTANCE.getISAConcreteMethod();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAAbstractMethodImpl <em>ISA Abstract Method</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.ISAAbstractMethodImpl
		 * @see dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl#getISAAbstractMethod()
		 * @generated
		 */
		EClass ISA_ABSTRACT_METHOD = eINSTANCE.getISAAbstractMethod();

	}

} //MembersPackage

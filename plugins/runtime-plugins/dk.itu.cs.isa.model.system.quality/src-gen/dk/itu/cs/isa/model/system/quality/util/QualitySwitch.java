/**
 */
package dk.itu.cs.isa.model.system.quality.util;

import dk.itu.cs.isa.model.system.quality.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.system.quality.QualityPackage
 * @generated
 */
public class QualitySwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static QualityPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualitySwitch() {
		if (modelPackage == null) {
			modelPackage = QualityPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL: {
				ISASoftwareSystemQualityModel isaSoftwareSystemQualityModel = (ISASoftwareSystemQualityModel)theEObject;
				T result = caseISASoftwareSystemQualityModel(isaSoftwareSystemQualityModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY: {
				ISAClassifierQualityEntry isaClassifierQualityEntry = (ISAClassifierQualityEntry)theEObject;
				T result = caseISAClassifierQualityEntry(isaClassifierQualityEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case QualityPackage.ISA_MEASURE: {
				ISAMeasure isaMeasure = (ISAMeasure)theEObject;
				T result = caseISAMeasure(isaMeasure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case QualityPackage.ISA_ISSUE: {
				ISAIssue isaIssue = (ISAIssue)theEObject;
				T result = caseISAIssue(isaIssue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case QualityPackage.ISA_FILE_PASSAGE: {
				ISAFilePassage isaFilePassage = (ISAFilePassage)theEObject;
				T result = caseISAFilePassage(isaFilePassage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Software System Quality Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Software System Quality Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISASoftwareSystemQualityModel(ISASoftwareSystemQualityModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Classifier Quality Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Classifier Quality Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAClassifierQualityEntry(ISAClassifierQualityEntry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Measure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Measure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAMeasure(ISAMeasure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Issue</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Issue</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAIssue(ISAIssue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA File Passage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA File Passage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAFilePassage(ISAFilePassage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //QualitySwitch

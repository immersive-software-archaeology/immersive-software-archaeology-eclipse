/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Abstract Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAAbstractClass()
 * @model
 * @generated
 */
public interface ISAAbstractClass extends ISAClass {
} // ISAAbstractClass

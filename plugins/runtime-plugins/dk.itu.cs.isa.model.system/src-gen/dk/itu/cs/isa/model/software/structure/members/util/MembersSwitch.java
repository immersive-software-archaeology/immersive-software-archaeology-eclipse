/**
 */
package dk.itu.cs.isa.model.software.structure.members.util;

import dk.itu.cs.isa.model.software.structure.members.*;

import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage
 * @generated
 */
public class MembersSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MembersPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MembersSwitch() {
		if (modelPackage == null) {
			modelPackage = MembersPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MembersPackage.ISA_CONSTRUCTOR_CONTAINER: {
				ISAConstructorContainer isaConstructorContainer = (ISAConstructorContainer)theEObject;
				T result = caseISAConstructorContainer(isaConstructorContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MembersPackage.ISA_MEMBER: {
				ISAMember isaMember = (ISAMember)theEObject;
				T result = caseISAMember(isaMember);
				if (result == null) result = caseISAReferenceableElement(isaMember);
				if (result == null) result = caseISAModifiableElement(isaMember);
				if (result == null) result = caseISANamedSoftwareElement(isaMember);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MembersPackage.ISA_FIELD: {
				ISAField isaField = (ISAField)theEObject;
				T result = caseISAField(isaField);
				if (result == null) result = caseISAMember(isaField);
				if (result == null) result = caseISAReferencingElement(isaField);
				if (result == null) result = caseISATypedElement(isaField);
				if (result == null) result = caseISAReferenceableElement(isaField);
				if (result == null) result = caseISAModifiableElement(isaField);
				if (result == null) result = caseISANamedSoftwareElement(isaField);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MembersPackage.ISA_ENUM_CONSTANT: {
				ISAEnumConstant isaEnumConstant = (ISAEnumConstant)theEObject;
				T result = caseISAEnumConstant(isaEnumConstant);
				if (result == null) result = caseISAMember(isaEnumConstant);
				if (result == null) result = caseISAReferenceableElement(isaEnumConstant);
				if (result == null) result = caseISAModifiableElement(isaEnumConstant);
				if (result == null) result = caseISANamedSoftwareElement(isaEnumConstant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MembersPackage.ISA_RECORD_COMPONENT: {
				ISARecordComponent isaRecordComponent = (ISARecordComponent)theEObject;
				T result = caseISARecordComponent(isaRecordComponent);
				if (result == null) result = caseISAMember(isaRecordComponent);
				if (result == null) result = caseISATypedElement(isaRecordComponent);
				if (result == null) result = caseISAReferenceableElement(isaRecordComponent);
				if (result == null) result = caseISAModifiableElement(isaRecordComponent);
				if (result == null) result = caseISANamedSoftwareElement(isaRecordComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MembersPackage.ISA_CONSTRUCTOR: {
				ISAConstructor isaConstructor = (ISAConstructor)theEObject;
				T result = caseISAConstructor(isaConstructor);
				if (result == null) result = caseISAMember(isaConstructor);
				if (result == null) result = caseISAStatementContainer(isaConstructor);
				if (result == null) result = caseISAParametrizableElement(isaConstructor);
				if (result == null) result = caseISAReferenceableElement(isaConstructor);
				if (result == null) result = caseISAModifiableElement(isaConstructor);
				if (result == null) result = caseISAReferencingElement(isaConstructor);
				if (result == null) result = caseISANamedSoftwareElement(isaConstructor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MembersPackage.ISA_METHOD: {
				ISAMethod isaMethod = (ISAMethod)theEObject;
				T result = caseISAMethod(isaMethod);
				if (result == null) result = caseISAMember(isaMethod);
				if (result == null) result = caseISATypedElement(isaMethod);
				if (result == null) result = caseISAParametrizableElement(isaMethod);
				if (result == null) result = caseISAReferenceableElement(isaMethod);
				if (result == null) result = caseISAModifiableElement(isaMethod);
				if (result == null) result = caseISANamedSoftwareElement(isaMethod);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MembersPackage.ISA_CONCRETE_METHOD: {
				ISAConcreteMethod isaConcreteMethod = (ISAConcreteMethod)theEObject;
				T result = caseISAConcreteMethod(isaConcreteMethod);
				if (result == null) result = caseISAMethod(isaConcreteMethod);
				if (result == null) result = caseISAStatementContainer(isaConcreteMethod);
				if (result == null) result = caseISAReferencingElement(isaConcreteMethod);
				if (result == null) result = caseISAMember(isaConcreteMethod);
				if (result == null) result = caseISATypedElement(isaConcreteMethod);
				if (result == null) result = caseISAParametrizableElement(isaConcreteMethod);
				if (result == null) result = caseISAReferenceableElement(isaConcreteMethod);
				if (result == null) result = caseISAModifiableElement(isaConcreteMethod);
				if (result == null) result = caseISANamedSoftwareElement(isaConcreteMethod);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MembersPackage.ISA_ABSTRACT_METHOD: {
				ISAAbstractMethod isaAbstractMethod = (ISAAbstractMethod)theEObject;
				T result = caseISAAbstractMethod(isaAbstractMethod);
				if (result == null) result = caseISAMethod(isaAbstractMethod);
				if (result == null) result = caseISAMember(isaAbstractMethod);
				if (result == null) result = caseISATypedElement(isaAbstractMethod);
				if (result == null) result = caseISAParametrizableElement(isaAbstractMethod);
				if (result == null) result = caseISAReferenceableElement(isaAbstractMethod);
				if (result == null) result = caseISAModifiableElement(isaAbstractMethod);
				if (result == null) result = caseISANamedSoftwareElement(isaAbstractMethod);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Constructor Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Constructor Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAConstructorContainer(ISAConstructorContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Member</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Member</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAMember(ISAMember object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Enum Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Enum Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAEnumConstant(ISAEnumConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Record Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Record Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISARecordComponent(ISARecordComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAField(ISAField object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Constructor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Constructor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAConstructor(ISAConstructor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Method</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Method</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAMethod(ISAMethod object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Concrete Method</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Concrete Method</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAConcreteMethod(ISAConcreteMethod object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Abstract Method</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Abstract Method</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAAbstractMethod(ISAAbstractMethod object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Named Software Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Named Software Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISANamedSoftwareElement(ISANamedSoftwareElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Referenceable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Referenceable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAReferenceableElement(ISAReferenceableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Modifiable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Modifiable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAModifiableElement(ISAModifiableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Referencing Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Referencing Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAReferencingElement(ISAReferencingElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Typed Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Typed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISATypedElement(ISATypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Statement Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Statement Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAStatementContainer(ISAStatementContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Parametrizable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Parametrizable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAParametrizableElement(ISAParametrizableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MembersSwitch

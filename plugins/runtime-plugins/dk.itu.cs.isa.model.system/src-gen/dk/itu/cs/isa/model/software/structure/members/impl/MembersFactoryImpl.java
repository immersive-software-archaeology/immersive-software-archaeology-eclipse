/**
 */
package dk.itu.cs.isa.model.software.structure.members.impl;

import dk.itu.cs.isa.model.software.structure.members.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MembersFactoryImpl extends EFactoryImpl implements MembersFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MembersFactory init() {
		try {
			MembersFactory theMembersFactory = (MembersFactory)EPackage.Registry.INSTANCE.getEFactory(MembersPackage.eNS_URI);
			if (theMembersFactory != null) {
				return theMembersFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MembersFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MembersFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MembersPackage.ISA_MEMBER: return createISAMember();
			case MembersPackage.ISA_FIELD: return createISAField();
			case MembersPackage.ISA_ENUM_CONSTANT: return createISAEnumConstant();
			case MembersPackage.ISA_RECORD_COMPONENT: return createISARecordComponent();
			case MembersPackage.ISA_CONSTRUCTOR: return createISAConstructor();
			case MembersPackage.ISA_CONCRETE_METHOD: return createISAConcreteMethod();
			case MembersPackage.ISA_ABSTRACT_METHOD: return createISAAbstractMethod();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAMember createISAMember() {
		ISAMemberImpl isaMember = new ISAMemberImpl();
		return isaMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAEnumConstant createISAEnumConstant() {
		ISAEnumConstantImpl isaEnumConstant = new ISAEnumConstantImpl();
		return isaEnumConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISARecordComponent createISARecordComponent() {
		ISARecordComponentImpl isaRecordComponent = new ISARecordComponentImpl();
		return isaRecordComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAField createISAField() {
		ISAFieldImpl isaField = new ISAFieldImpl();
		return isaField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAConstructor createISAConstructor() {
		ISAConstructorImpl isaConstructor = new ISAConstructorImpl();
		return isaConstructor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAConcreteMethod createISAConcreteMethod() {
		ISAConcreteMethodImpl isaConcreteMethod = new ISAConcreteMethodImpl();
		return isaConcreteMethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAAbstractMethod createISAAbstractMethod() {
		ISAAbstractMethodImpl isaAbstractMethod = new ISAAbstractMethodImpl();
		return isaAbstractMethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MembersPackage getMembersPackage() {
		return (MembersPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MembersPackage getPackage() {
		return MembersPackage.eINSTANCE;
	}

} //MembersFactoryImpl

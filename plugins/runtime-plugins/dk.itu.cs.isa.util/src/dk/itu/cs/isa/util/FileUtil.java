package dk.itu.cs.isa.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;

public class FileUtil {
	
	public static File getFileFromPlugin(String pluginName, String relativePath) throws URISyntaxException, IOException {
		URL fileURL = Platform.getBundle(pluginName).getEntry(relativePath);
		
		// Solution 1:
		URL fileURLAbsolute = org.eclipse.core.runtime.FileLocator.toFileURL(fileURL);
		return new File(fileURLAbsolute.getPath());

		// Solution 2:
//		return new File(FileLocator.resolve(url).toURI());
	}
	
	public static List<String> readContentFromPluginFile(String pluginName, String relativePath) throws IOException, URISyntaxException {
		File file = getFileFromPlugin(pluginName, relativePath);
		List<String> content = new ArrayList<>();
		
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			content.add(line);
		}
		br.close();

		return content;
	}
	
	public static IFile getSiblingFile(IFile file, String siblingFileName) {
		return file.getParent().getFile(new Path(siblingFileName));
	}
	
	public static IFile createEmptyFileInFolder(IFolder targetSourceFolder, String pathToFacadeClass, boolean override, boolean force, IProgressMonitor monitor) throws CoreException {
		IFile facadeClassFile = targetSourceFolder.getFile(pathToFacadeClass);
		if(facadeClassFile.exists()) {
			if(override)
				facadeClassFile.delete(force, monitor);
			else
				return null;
		}
		
		IFolder currentFolder = targetSourceFolder;
		String[] segments = pathToFacadeClass.split("/");
		for(int i=0; i<segments.length-1 ; i++) {
			currentFolder = currentFolder.getFolder(segments[i]);
			if(!currentFolder.exists())
				currentFolder.create(force, true, monitor);
		}
		
		facadeClassFile.create(new ByteArrayInputStream(new byte[0]), force, monitor);
		return facadeClassFile;
	}

}





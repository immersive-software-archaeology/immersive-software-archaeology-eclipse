package dk.itu.cs.isa.coalescence.server;

import java.net.SocketException;
import java.util.List;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import dk.itu.cs.isa.coalescence.server.extensions.ExtensionPointHandler;
import dk.itu.cs.isa.coalescence.server.extensions.ISAAbstractSocketImplementation;
import dk.itu.cs.isa.coalescence.server.extensions.ISACoalescenceServerObserver;
import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.coalescence.server.requests.ISABaseRequestHandler;



public class ISACoalescenceServer extends ISAAbstractJettyHTTPServer implements BundleActivator {
	
	public static ISACoalescenceServer INSTANCE = null;
	public static final int PORT = 9005;
	
	private ISAStandardPortForwardServer forwardServer;
	
	
	
	/**
	 * Cannot be private, even though this is a singleton, because the class is
	 * registered as activator within the Eclipse framework and needs to be
	 * externally instantiated
	 */
	public ISACoalescenceServer() {
		if(INSTANCE != null)
			throw new RuntimeException("The ISACoalescenceServer is a singleton and it must be started via the Eclipse activation framework!");
		INSTANCE = this;
		init("ISA Coalescence Server", PORT, new ISABaseRequestHandler());
	}

	@Override
	public boolean startServer() {
		if(!super.startServer())
			return false;
		
		forwardServer = new ISAStandardPortForwardServer();
		forwardServer.startServer();
		
		for(ISACoalescenceServerObserver obs : ExtensionPointHandler.INSTANCE.getServerObservers())
			obs.notifyServerStart();

		for(ISAAbstractSocketImplementation socket : ExtensionPointHandler.INSTANCE.getSockets()) {
			for(int i=0; true; i++) {
				try {
					socket.init(ISACoalescenceServer.PORT + i);
					new Thread(socket, name).start();
					break;
				}
				catch(SocketException e) {
					// try again with another port
				}
			}
		}
		
		return true;
	}

	@Override
	public boolean stopServer() {
		if(!super.stopServer())
			return false;

		if(forwardServer != null)
			forwardServer.stopServer();
		
		for(ISACoalescenceServerObserver obs : ExtensionPointHandler.INSTANCE.getServerObservers())
			obs.notifyServerStop();

		for(ISAAbstractSocketImplementation socket : ExtensionPointHandler.INSTANCE.getSockets())
			socket.stop();
		
		return true;
	}
	
	public List<ISAAbstractCoalescenceRequestHandler> getRegisteredRequestHandlers() {
		return ExtensionPointHandler.INSTANCE.getRequestHandlers();
	}
	
	/**
	 * Triggered upon first usage of the plugin
	 */
	@Override
	public void start(BundleContext context) {
		ExtensionPointHandler.INSTANCE.readExtensionPoint();
//		try {
//			startServer();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}

	@Override
	public void stop(BundleContext context) {
		try {
			stopServer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
//	public void setCurrentlyActiveSystem(String systemName) {
//		this.currentlyActiveSystem = systemName;
//	}
//	
//	public String getCurrentlyActiveSystem() {
//		return currentlyActiveSystem;
//	}
	
	
	
}















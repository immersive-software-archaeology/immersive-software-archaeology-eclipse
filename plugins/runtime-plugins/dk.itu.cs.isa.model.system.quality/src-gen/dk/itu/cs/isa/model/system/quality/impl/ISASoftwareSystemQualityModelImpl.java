/**
 */
package dk.itu.cs.isa.model.system.quality.impl;

import dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry;
import dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel;
import dk.itu.cs.isa.model.system.quality.QualityPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Software System Quality Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISASoftwareSystemQualityModelImpl#getClassifierEntries <em>Classifier Entries</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISASoftwareSystemQualityModelImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISASoftwareSystemQualityModelImpl extends MinimalEObjectImpl.Container implements ISASoftwareSystemQualityModel {
	/**
	 * The cached value of the '{@link #getClassifierEntries() <em>Classifier Entries</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifierEntries()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAClassifierQualityEntry> classifierEntries;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISASoftwareSystemQualityModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QualityPackage.Literals.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAClassifierQualityEntry> getClassifierEntries() {
		if (classifierEntries == null) {
			classifierEntries = new EObjectContainmentEList<ISAClassifierQualityEntry>(ISAClassifierQualityEntry.class, this, QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__CLASSIFIER_ENTRIES);
		}
		return classifierEntries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__CLASSIFIER_ENTRIES:
				return ((InternalEList<?>)getClassifierEntries()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__CLASSIFIER_ENTRIES:
				return getClassifierEntries();
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__CLASSIFIER_ENTRIES:
				getClassifierEntries().clear();
				getClassifierEntries().addAll((Collection<? extends ISAClassifierQualityEntry>)newValue);
				return;
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__CLASSIFIER_ENTRIES:
				getClassifierEntries().clear();
				return;
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__CLASSIFIER_ENTRIES:
				return classifierEntries != null && !classifierEntries.isEmpty();
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ISASoftwareSystemQualityModelImpl

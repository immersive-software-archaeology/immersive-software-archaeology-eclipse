/**
 */
package dk.itu.cs.isa.model.visualization.impl;

import dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent;
import dk.itu.cs.isa.model.visualization.ISAAbstractVisualRelation;
import dk.itu.cs.isa.model.visualization.ISAAntennaRelation;
import dk.itu.cs.isa.model.visualization.ISABlockRelation;
import dk.itu.cs.isa.model.visualization.ISANamedVisualElement;
import dk.itu.cs.isa.model.visualization.ISASpaceStation;
import dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna;
import dk.itu.cs.isa.model.visualization.ISASpaceStationBlock;
import dk.itu.cs.isa.model.visualization.ISASpaceStationRelation;
import dk.itu.cs.isa.model.visualization.ISASpaceStationType;
import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.model.visualization.ISAVector3;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;
import dk.itu.cs.isa.model.visualization.VisualizationFactory;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VisualizationPackageImpl extends EPackageImpl implements VisualizationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaNamedVisualElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaVisualizationModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaVector3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaTransparentCapsuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaSpaceStationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaAbstractSpaceStationComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaSpaceStationBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaSpaceStationAntennaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaAbstractVisualRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaSpaceStationRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaBlockRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaAntennaRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum isaSpaceStationTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VisualizationPackageImpl() {
		super(eNS_URI, VisualizationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link VisualizationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static VisualizationPackage init() {
		if (isInited) return (VisualizationPackage)EPackage.Registry.INSTANCE.getEPackage(VisualizationPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredVisualizationPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		VisualizationPackageImpl theVisualizationPackage = registeredVisualizationPackage instanceof VisualizationPackageImpl ? (VisualizationPackageImpl)registeredVisualizationPackage : new VisualizationPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theVisualizationPackage.createPackageContents();

		// Initialize created meta-data
		theVisualizationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theVisualizationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VisualizationPackage.eNS_URI, theVisualizationPackage);
		return theVisualizationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISANamedVisualElement() {
		return isaNamedVisualElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISANamedVisualElement_Name() {
		return (EAttribute)isaNamedVisualElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISANamedVisualElement_QualifiedName() {
		return (EAttribute)isaNamedVisualElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAVisualizationModel() {
		return isaVisualizationModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISAVisualizationModel_SubjectSystemCapsule() {
		return (EReference)isaVisualizationModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISAVisualizationModel_LibraryCapsules() {
		return (EReference)isaVisualizationModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVisualizationModel_ModelCreatorName() {
		return (EAttribute)isaVisualizationModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAVector3() {
		return isaVector3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_X() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_Y() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_Z() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISATransparentCapsule() {
		return isaTransparentCapsuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISATransparentCapsule_SubCapsules() {
		return (EReference)isaTransparentCapsuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISATransparentCapsule_SpaceStations() {
		return (EReference)isaTransparentCapsuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISATransparentCapsule_Tags() {
		return (EAttribute)isaTransparentCapsuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISASpaceStation() {
		return isaSpaceStationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISASpaceStation_Position() {
		return (EReference)isaSpaceStationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISASpaceStation_SatelliteStations() {
		return (EReference)isaSpaceStationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISASpaceStation_Blocks() {
		return (EReference)isaSpaceStationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISASpaceStation_Antennas() {
		return (EReference)isaSpaceStationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStation_Type() {
		return (EAttribute)isaSpaceStationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStation_Public() {
		return (EAttribute)isaSpaceStationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISASpaceStation_InheritanceRelations() {
		return (EReference)isaSpaceStationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAAbstractSpaceStationComponent() {
		return isaAbstractSpaceStationComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISAAbstractSpaceStationComponent_Type() {
		return (EReference)isaAbstractSpaceStationComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISAAbstractSpaceStationComponent_RelationsToSpaceStationCenters() {
		return (EReference)isaAbstractSpaceStationComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISAAbstractSpaceStationComponent_RelationsToBlocks() {
		return (EReference)isaAbstractSpaceStationComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISAAbstractSpaceStationComponent_RelationsToAntennas() {
		return (EReference)isaAbstractSpaceStationComponentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISASpaceStationBlock() {
		return isaSpaceStationBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStationBlock_Height() {
		return (EAttribute)isaSpaceStationBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStationBlock_Diameter() {
		return (EAttribute)isaSpaceStationBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStationBlock_Constructor() {
		return (EAttribute)isaSpaceStationBlockEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStationBlock_Abstract() {
		return (EAttribute)isaSpaceStationBlockEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStationBlock_Public() {
		return (EAttribute)isaSpaceStationBlockEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStationBlock_SystemEntry() {
		return (EAttribute)isaSpaceStationBlockEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISASpaceStationAntenna() {
		return isaSpaceStationAntennaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStationAntenna_Public() {
		return (EAttribute)isaSpaceStationAntennaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStationAntenna_EnumConstant() {
		return (EAttribute)isaSpaceStationAntennaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISASpaceStationAntenna_RecordComponent() {
		return (EAttribute)isaSpaceStationAntennaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAAbstractVisualRelation() {
		return isaAbstractVisualRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAAbstractVisualRelation_Weight() {
		return (EAttribute)isaAbstractVisualRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISASpaceStationRelation() {
		return isaSpaceStationRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISASpaceStationRelation_Target() {
		return (EReference)isaSpaceStationRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISABlockRelation() {
		return isaBlockRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISABlockRelation_Target() {
		return (EReference)isaBlockRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAAntennaRelation() {
		return isaAntennaRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getISAAntennaRelation_Target() {
		return (EReference)isaAntennaRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getISASpaceStationType() {
		return isaSpaceStationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualizationFactory getVisualizationFactory() {
		return (VisualizationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaNamedVisualElementEClass = createEClass(ISA_NAMED_VISUAL_ELEMENT);
		createEAttribute(isaNamedVisualElementEClass, ISA_NAMED_VISUAL_ELEMENT__NAME);
		createEAttribute(isaNamedVisualElementEClass, ISA_NAMED_VISUAL_ELEMENT__QUALIFIED_NAME);

		isaVisualizationModelEClass = createEClass(ISA_VISUALIZATION_MODEL);
		createEReference(isaVisualizationModelEClass, ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE);
		createEReference(isaVisualizationModelEClass, ISA_VISUALIZATION_MODEL__LIBRARY_CAPSULES);
		createEAttribute(isaVisualizationModelEClass, ISA_VISUALIZATION_MODEL__MODEL_CREATOR_NAME);

		isaVector3EClass = createEClass(ISA_VECTOR3);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__X);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__Y);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__Z);

		isaTransparentCapsuleEClass = createEClass(ISA_TRANSPARENT_CAPSULE);
		createEReference(isaTransparentCapsuleEClass, ISA_TRANSPARENT_CAPSULE__SUB_CAPSULES);
		createEReference(isaTransparentCapsuleEClass, ISA_TRANSPARENT_CAPSULE__SPACE_STATIONS);
		createEAttribute(isaTransparentCapsuleEClass, ISA_TRANSPARENT_CAPSULE__TAGS);

		isaSpaceStationEClass = createEClass(ISA_SPACE_STATION);
		createEReference(isaSpaceStationEClass, ISA_SPACE_STATION__POSITION);
		createEReference(isaSpaceStationEClass, ISA_SPACE_STATION__SATELLITE_STATIONS);
		createEReference(isaSpaceStationEClass, ISA_SPACE_STATION__BLOCKS);
		createEReference(isaSpaceStationEClass, ISA_SPACE_STATION__ANTENNAS);
		createEAttribute(isaSpaceStationEClass, ISA_SPACE_STATION__TYPE);
		createEAttribute(isaSpaceStationEClass, ISA_SPACE_STATION__PUBLIC);
		createEReference(isaSpaceStationEClass, ISA_SPACE_STATION__INHERITANCE_RELATIONS);

		isaAbstractSpaceStationComponentEClass = createEClass(ISA_ABSTRACT_SPACE_STATION_COMPONENT);
		createEReference(isaAbstractSpaceStationComponentEClass, ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE);
		createEReference(isaAbstractSpaceStationComponentEClass, ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS);
		createEReference(isaAbstractSpaceStationComponentEClass, ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS);
		createEReference(isaAbstractSpaceStationComponentEClass, ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS);

		isaSpaceStationBlockEClass = createEClass(ISA_SPACE_STATION_BLOCK);
		createEAttribute(isaSpaceStationBlockEClass, ISA_SPACE_STATION_BLOCK__HEIGHT);
		createEAttribute(isaSpaceStationBlockEClass, ISA_SPACE_STATION_BLOCK__DIAMETER);
		createEAttribute(isaSpaceStationBlockEClass, ISA_SPACE_STATION_BLOCK__CONSTRUCTOR);
		createEAttribute(isaSpaceStationBlockEClass, ISA_SPACE_STATION_BLOCK__ABSTRACT);
		createEAttribute(isaSpaceStationBlockEClass, ISA_SPACE_STATION_BLOCK__PUBLIC);
		createEAttribute(isaSpaceStationBlockEClass, ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY);

		isaSpaceStationAntennaEClass = createEClass(ISA_SPACE_STATION_ANTENNA);
		createEAttribute(isaSpaceStationAntennaEClass, ISA_SPACE_STATION_ANTENNA__PUBLIC);
		createEAttribute(isaSpaceStationAntennaEClass, ISA_SPACE_STATION_ANTENNA__ENUM_CONSTANT);
		createEAttribute(isaSpaceStationAntennaEClass, ISA_SPACE_STATION_ANTENNA__RECORD_COMPONENT);

		isaAbstractVisualRelationEClass = createEClass(ISA_ABSTRACT_VISUAL_RELATION);
		createEAttribute(isaAbstractVisualRelationEClass, ISA_ABSTRACT_VISUAL_RELATION__WEIGHT);

		isaSpaceStationRelationEClass = createEClass(ISA_SPACE_STATION_RELATION);
		createEReference(isaSpaceStationRelationEClass, ISA_SPACE_STATION_RELATION__TARGET);

		isaBlockRelationEClass = createEClass(ISA_BLOCK_RELATION);
		createEReference(isaBlockRelationEClass, ISA_BLOCK_RELATION__TARGET);

		isaAntennaRelationEClass = createEClass(ISA_ANTENNA_RELATION);
		createEReference(isaAntennaRelationEClass, ISA_ANTENNA_RELATION__TARGET);

		// Create enums
		isaSpaceStationTypeEEnum = createEEnum(ISA_SPACE_STATION_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		isaVisualizationModelEClass.getESuperTypes().add(this.getISANamedVisualElement());
		isaTransparentCapsuleEClass.getESuperTypes().add(this.getISANamedVisualElement());
		isaSpaceStationEClass.getESuperTypes().add(this.getISANamedVisualElement());
		isaAbstractSpaceStationComponentEClass.getESuperTypes().add(this.getISANamedVisualElement());
		isaSpaceStationBlockEClass.getESuperTypes().add(this.getISAAbstractSpaceStationComponent());
		isaSpaceStationAntennaEClass.getESuperTypes().add(this.getISAAbstractSpaceStationComponent());
		isaSpaceStationRelationEClass.getESuperTypes().add(this.getISAAbstractVisualRelation());
		isaBlockRelationEClass.getESuperTypes().add(this.getISAAbstractVisualRelation());
		isaAntennaRelationEClass.getESuperTypes().add(this.getISAAbstractVisualRelation());

		// Initialize classes, features, and operations; add parameters
		initEClass(isaNamedVisualElementEClass, ISANamedVisualElement.class, "ISANamedVisualElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISANamedVisualElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, ISANamedVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISANamedVisualElement_QualifiedName(), ecorePackage.getEString(), "qualifiedName", null, 0, 1, ISANamedVisualElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaVisualizationModelEClass, ISAVisualizationModel.class, "ISAVisualizationModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAVisualizationModel_SubjectSystemCapsule(), this.getISATransparentCapsule(), null, "subjectSystemCapsule", null, 1, 1, ISAVisualizationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAVisualizationModel_LibraryCapsules(), this.getISATransparentCapsule(), null, "libraryCapsules", null, 0, -1, ISAVisualizationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVisualizationModel_ModelCreatorName(), ecorePackage.getEString(), "modelCreatorName", null, 1, 1, ISAVisualizationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaVector3EClass, ISAVector3.class, "ISAVector3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAVector3_X(), ecorePackage.getEFloat(), "x", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVector3_Y(), ecorePackage.getEFloat(), "y", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVector3_Z(), ecorePackage.getEFloat(), "z", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaTransparentCapsuleEClass, ISATransparentCapsule.class, "ISATransparentCapsule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISATransparentCapsule_SubCapsules(), this.getISATransparentCapsule(), null, "subCapsules", null, 0, -1, ISATransparentCapsule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISATransparentCapsule_SpaceStations(), this.getISASpaceStation(), null, "spaceStations", null, 0, -1, ISATransparentCapsule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISATransparentCapsule_Tags(), ecorePackage.getEString(), "tags", null, 0, -1, ISATransparentCapsule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaSpaceStationEClass, ISASpaceStation.class, "ISASpaceStation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISASpaceStation_Position(), this.getISAVector3(), null, "position", null, 1, 1, ISASpaceStation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISASpaceStation_SatelliteStations(), this.getISASpaceStation(), null, "satelliteStations", null, 0, -1, ISASpaceStation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISASpaceStation_Blocks(), this.getISASpaceStationBlock(), null, "blocks", null, 0, -1, ISASpaceStation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISASpaceStation_Antennas(), this.getISASpaceStationAntenna(), null, "antennas", null, 0, -1, ISASpaceStation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASpaceStation_Type(), this.getISASpaceStationType(), "type", null, 1, 1, ISASpaceStation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASpaceStation_Public(), ecorePackage.getEBoolean(), "public", null, 1, 1, ISASpaceStation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISASpaceStation_InheritanceRelations(), this.getISASpaceStationRelation(), null, "inheritanceRelations", null, 0, -1, ISASpaceStation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaAbstractSpaceStationComponentEClass, ISAAbstractSpaceStationComponent.class, "ISAAbstractSpaceStationComponent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAAbstractSpaceStationComponent_Type(), this.getISASpaceStationRelation(), null, "type", null, 0, 1, ISAAbstractSpaceStationComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAAbstractSpaceStationComponent_RelationsToSpaceStationCenters(), this.getISASpaceStationRelation(), null, "relationsToSpaceStationCenters", null, 0, -1, ISAAbstractSpaceStationComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAAbstractSpaceStationComponent_RelationsToBlocks(), this.getISABlockRelation(), null, "relationsToBlocks", null, 0, -1, ISAAbstractSpaceStationComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAAbstractSpaceStationComponent_RelationsToAntennas(), this.getISAAntennaRelation(), null, "relationsToAntennas", null, 0, -1, ISAAbstractSpaceStationComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaSpaceStationBlockEClass, ISASpaceStationBlock.class, "ISASpaceStationBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISASpaceStationBlock_Height(), ecorePackage.getEFloat(), "height", null, 1, 1, ISASpaceStationBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASpaceStationBlock_Diameter(), ecorePackage.getEFloat(), "diameter", null, 1, 1, ISASpaceStationBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASpaceStationBlock_Constructor(), ecorePackage.getEBoolean(), "constructor", null, 1, 1, ISASpaceStationBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASpaceStationBlock_Abstract(), ecorePackage.getEBoolean(), "abstract", null, 1, 1, ISASpaceStationBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASpaceStationBlock_Public(), ecorePackage.getEBoolean(), "public", null, 1, 1, ISASpaceStationBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASpaceStationBlock_SystemEntry(), ecorePackage.getEBoolean(), "systemEntry", null, 1, 1, ISASpaceStationBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaSpaceStationAntennaEClass, ISASpaceStationAntenna.class, "ISASpaceStationAntenna", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISASpaceStationAntenna_Public(), ecorePackage.getEBoolean(), "public", null, 1, 1, ISASpaceStationAntenna.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASpaceStationAntenna_EnumConstant(), ecorePackage.getEBoolean(), "enumConstant", null, 1, 1, ISASpaceStationAntenna.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASpaceStationAntenna_RecordComponent(), ecorePackage.getEBoolean(), "recordComponent", null, 1, 1, ISASpaceStationAntenna.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaAbstractVisualRelationEClass, ISAAbstractVisualRelation.class, "ISAAbstractVisualRelation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAAbstractVisualRelation_Weight(), ecorePackage.getEFloat(), "weight", null, 1, 1, ISAAbstractVisualRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaSpaceStationRelationEClass, ISASpaceStationRelation.class, "ISASpaceStationRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISASpaceStationRelation_Target(), this.getISASpaceStation(), null, "target", null, 1, 1, ISASpaceStationRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaBlockRelationEClass, ISABlockRelation.class, "ISABlockRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISABlockRelation_Target(), this.getISASpaceStationBlock(), null, "target", null, 1, 1, ISABlockRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaAntennaRelationEClass, ISAAntennaRelation.class, "ISAAntennaRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAAntennaRelation_Target(), this.getISASpaceStationAntenna(), null, "target", null, 1, 1, ISAAntennaRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(isaSpaceStationTypeEEnum, ISASpaceStationType.class, "ISASpaceStationType");
		addEEnumLiteral(isaSpaceStationTypeEEnum, ISASpaceStationType.UNKOWN);
		addEEnumLiteral(isaSpaceStationTypeEEnum, ISASpaceStationType.CLASS);
		addEEnumLiteral(isaSpaceStationTypeEEnum, ISASpaceStationType.INTERFACE);
		addEEnumLiteral(isaSpaceStationTypeEEnum, ISASpaceStationType.ENUM);
		addEEnumLiteral(isaSpaceStationTypeEEnum, ISASpaceStationType.RECORD);

		// Create resource
		createResource(eNS_URI);
	}

} //VisualizationPackageImpl

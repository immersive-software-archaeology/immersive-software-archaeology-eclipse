package dk.itu.cs.isa.model.system.creation.java;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

import dk.itu.cs.isa.analyzer.interfaces.ISystemModelCreator;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.StructureFactory;

public class SystemModelCreatorJDT implements ISystemModelCreator {

	private final StructureFactory softwareFactory = StructureFactory.eINSTANCE;

	
	
	@Override
	public void initializeInstance() {
	}
	
	@Override
	public void setupBeforeRun() {
	}
	
	@Override
	public void cleanUpAfterRun() {
	}
	
	

	public ISASoftwareSystemModel run(String name, List<IProject> selectedProjects, IProgressMonitor monitor) throws Exception {
		ISASoftwareSystemModel isaSystem = softwareFactory.createISASoftwareSystemModel();
		isaSystem.setName(name);
		
		DesignAnalyzer designAnalyzer = new DesignAnalyzer();
		for(IProject project : selectedProjects) {
			IJavaProject jdtProject = JavaCore.create(project);
			if (jdtProject == null || !jdtProject.exists()) {
				// project is not a Java project --> ignore
				continue;
			}
			designAnalyzer.analyzeJavaProject(isaSystem, jdtProject, monitor);
		}
		
		designAnalyzer.resolveAndRegisterReferences(isaSystem, monitor);
		designAnalyzer.clear();
		
		return isaSystem;
	}
	
	
	
}















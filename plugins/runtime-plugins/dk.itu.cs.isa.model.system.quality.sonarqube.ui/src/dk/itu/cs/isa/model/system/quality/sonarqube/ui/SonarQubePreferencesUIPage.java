package dk.itu.cs.isa.model.system.quality.sonarqube.ui;


import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

import dk.itu.cs.isa.model.quality.sonarqube.preferences.ISAPreferencesSonarQube;
import dk.itu.cs.isa.preferences.ui.ISAPreferencesUIPage;

public class SonarQubePreferencesUIPage extends ISAPreferencesUIPage {
	
	FileFieldEditor sonarQubeExecutableEditor;
	FileFieldEditor sonarScannerExecutableEditor;
	BooleanFieldEditor sonarScannerDebugModeEditor;
	StringFieldEditor loginEditor;
	StringFieldEditor passwordEditor;

	public SonarQubePreferencesUIPage() {
		// TODO add fancy image!
		super();
	}

	@Override
	public void createFieldEditors() {
		
		addSpace();
		
		sonarQubeExecutableEditor = new FileFieldEditor(ISAPreferencesSonarQube.SONAR_QUBE_EXECUTABLE_PATH, "&Sonar Qube executable:", getFieldEditorParent());
		new Label(getFieldEditorParent(), SWT.NONE); // Empty Label to achieve white space
		Label sonarQubePathHintLabel = new Label(getFieldEditorParent(), SWT.NONE);
		sonarQubePathHintLabel.setText("Example path for windows: C:\\sonarqube\\bin\\windows-x86-64\\StartSonar.bat");
		sonarQubePathHintLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		addField(sonarQubeExecutableEditor);
		
		addSpace();

		sonarScannerExecutableEditor = new FileFieldEditor(ISAPreferencesSonarQube.SONAR_SCANNER_EXECUTABLE_PATH, "&Sonar Scanner executable:", getFieldEditorParent());
		new Label(getFieldEditorParent(), SWT.NONE); // Empty Label to achieve white space
		Label sonarScannerPathHintLabel = new Label(getFieldEditorParent(), SWT.NONE);
		sonarScannerPathHintLabel.setText("Example path for windows: C:\\sonar-scanner\\bin\\sonar-scanner.bat");
		sonarScannerPathHintLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		addField(sonarScannerExecutableEditor);
		
		sonarScannerDebugModeEditor = new BooleanFieldEditor(ISAPreferencesSonarQube.SONAR_SCANNER_DEBUG_MODE, "&Scan in debug mode (produces significantly more console prints)", getFieldEditorParent());
		addField(sonarScannerDebugModeEditor);
		
		addSpace();
		addSeperator();
//		addSpace();
		
		Label credentialsheadingLabel = new Label(getFieldEditorParent(), SWT.NONE);
		credentialsheadingLabel.setText("SonarQube Credentials");
		credentialsheadingLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
		
		addSpace();
		
		loginEditor = new StringFieldEditor(ISAPreferencesSonarQube.SONAR_QUBE_LOGIN, "&Login Name", getFieldEditorParent());
		passwordEditor = new StringFieldEditor(ISAPreferencesSonarQube.SONAR_QUBE_PASSWORD, "&Password", getFieldEditorParent()) {
			@Override
			protected void doFillIntoGrid(Composite parent, int numColumns) {
				super.doFillIntoGrid(parent, numColumns);
				getTextControl().setEchoChar('*');
			}
		};

		new Label(getFieldEditorParent(), SWT.NONE); // Empty Label to achieve white space
		Label passwordHintLabel = new Label(getFieldEditorParent(), SWT.NONE);
		passwordHintLabel.setText("CAUTION: The password will not be encrypted within the Eclipse preference backend!");
		passwordHintLabel.setFont(
				JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT)
				);
		passwordHintLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		
		addField(loginEditor);
		addField(passwordEditor);
		
	}

	@Override
	public void init(IWorkbench workbench) {
		setTitle("ISA - Immersive Software Archeology");
		setDescription("Settings page for quality analysis with SonarQube");

		setPreferenceStore(new ScopedPreferenceStore(InstanceScope.INSTANCE, ISAPreferencesSonarQube.STORAGE_ID));
	}
	
	
	
	@Override
	public void performDefaults() {
//		super.performDefaults();
		
		(new ISAPreferencesSonarQube()).initializeDefaultPreferences();

		sonarQubeExecutableEditor.setStringValue(ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_QUBE_EXECUTABLE_PATH, ""));
		sonarScannerExecutableEditor.setStringValue(ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_SCANNER_EXECUTABLE_PATH, ""));
		sonarScannerDebugModeEditor.load();
		loginEditor.setStringValue(ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_QUBE_LOGIN, ""));
		passwordEditor.setStringValue(ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_QUBE_PASSWORD, ""));
		
		checkState();
		updateApplyButton();
	}

}

/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.BoardSpawnEvent;
import dk.itu.cs.isa.whiteboard.ISAVector3;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Board Spawn Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.BoardSpawnEventImpl#getWorldSpacePosition <em>World Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.BoardSpawnEventImpl#getWorldSpaceRotation <em>World Space Rotation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BoardSpawnEventImpl extends AbstractWhiteboardEventImpl implements BoardSpawnEvent {
	/**
	 * The cached value of the '{@link #getWorldSpacePosition() <em>World Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorldSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 worldSpacePosition;

	/**
	 * The cached value of the '{@link #getWorldSpaceRotation() <em>World Space Rotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorldSpaceRotation()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 worldSpaceRotation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BoardSpawnEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.BOARD_SPAWN_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getWorldSpacePosition() {
		return worldSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWorldSpacePosition(ISAVector3 newWorldSpacePosition, NotificationChain msgs) {
		ISAVector3 oldWorldSpacePosition = worldSpacePosition;
		worldSpacePosition = newWorldSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION, oldWorldSpacePosition, newWorldSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorldSpacePosition(ISAVector3 newWorldSpacePosition) {
		if (newWorldSpacePosition != worldSpacePosition) {
			NotificationChain msgs = null;
			if (worldSpacePosition != null)
				msgs = ((InternalEObject)worldSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION, null, msgs);
			if (newWorldSpacePosition != null)
				msgs = ((InternalEObject)newWorldSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION, null, msgs);
			msgs = basicSetWorldSpacePosition(newWorldSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION, newWorldSpacePosition, newWorldSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getWorldSpaceRotation() {
		return worldSpaceRotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWorldSpaceRotation(ISAVector3 newWorldSpaceRotation, NotificationChain msgs) {
		ISAVector3 oldWorldSpaceRotation = worldSpaceRotation;
		worldSpaceRotation = newWorldSpaceRotation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION, oldWorldSpaceRotation, newWorldSpaceRotation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorldSpaceRotation(ISAVector3 newWorldSpaceRotation) {
		if (newWorldSpaceRotation != worldSpaceRotation) {
			NotificationChain msgs = null;
			if (worldSpaceRotation != null)
				msgs = ((InternalEObject)worldSpaceRotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION, null, msgs);
			if (newWorldSpaceRotation != null)
				msgs = ((InternalEObject)newWorldSpaceRotation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION, null, msgs);
			msgs = basicSetWorldSpaceRotation(newWorldSpaceRotation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION, newWorldSpaceRotation, newWorldSpaceRotation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION:
				return basicSetWorldSpacePosition(null, msgs);
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION:
				return basicSetWorldSpaceRotation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION:
				return getWorldSpacePosition();
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION:
				return getWorldSpaceRotation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION:
				setWorldSpacePosition((ISAVector3)newValue);
				return;
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION:
				setWorldSpaceRotation((ISAVector3)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION:
				setWorldSpacePosition((ISAVector3)null);
				return;
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION:
				setWorldSpaceRotation((ISAVector3)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION:
				return worldSpacePosition != null;
			case WhiteboardPackage.BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION:
				return worldSpaceRotation != null;
		}
		return super.eIsSet(featureID);
	}

} //BoardSpawnEventImpl

/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Enum</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum#getConstants <em>Constants</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAEnum()
 * @model
 * @generated
 */
public interface ISAEnum extends ISAClassifier, ISAConstructorContainer, ISAClassifierContainer {
	/**
	 * Returns the value of the '<em><b>Constants</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constants</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAEnum_Constants()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAEnumConstant> getConstants();

} // ISAEnum

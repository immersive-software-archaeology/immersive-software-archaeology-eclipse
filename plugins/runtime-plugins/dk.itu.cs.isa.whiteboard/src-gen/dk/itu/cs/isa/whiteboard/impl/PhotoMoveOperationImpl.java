/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.ISAVector2;
import dk.itu.cs.isa.whiteboard.ISAVector3;
import dk.itu.cs.isa.whiteboard.PhotoMoveOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Photo Move Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoMoveOperationImpl#getPreLocalSpacePosition <em>Pre Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoMoveOperationImpl#getPreLocalSpaceForwardDirection <em>Pre Local Space Forward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoMoveOperationImpl#getPreLocalSpaceUpwardDirection <em>Pre Local Space Upward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoMoveOperationImpl#getPreTextureSpacePosition <em>Pre Texture Space Position</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhotoMoveOperationImpl extends AbstractPhotoOperationImpl implements PhotoMoveOperation {
	/**
	 * The cached value of the '{@link #getPreLocalSpacePosition() <em>Pre Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreLocalSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 preLocalSpacePosition;

	/**
	 * The cached value of the '{@link #getPreLocalSpaceForwardDirection() <em>Pre Local Space Forward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreLocalSpaceForwardDirection()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 preLocalSpaceForwardDirection;

	/**
	 * The cached value of the '{@link #getPreLocalSpaceUpwardDirection() <em>Pre Local Space Upward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreLocalSpaceUpwardDirection()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 preLocalSpaceUpwardDirection;

	/**
	 * The cached value of the '{@link #getPreTextureSpacePosition() <em>Pre Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreTextureSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector2 preTextureSpacePosition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhotoMoveOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PHOTO_MOVE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getPreLocalSpacePosition() {
		return preLocalSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreLocalSpacePosition(ISAVector3 newPreLocalSpacePosition, NotificationChain msgs) {
		ISAVector3 oldPreLocalSpacePosition = preLocalSpacePosition;
		preLocalSpacePosition = newPreLocalSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION, oldPreLocalSpacePosition, newPreLocalSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreLocalSpacePosition(ISAVector3 newPreLocalSpacePosition) {
		if (newPreLocalSpacePosition != preLocalSpacePosition) {
			NotificationChain msgs = null;
			if (preLocalSpacePosition != null)
				msgs = ((InternalEObject)preLocalSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION, null, msgs);
			if (newPreLocalSpacePosition != null)
				msgs = ((InternalEObject)newPreLocalSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION, null, msgs);
			msgs = basicSetPreLocalSpacePosition(newPreLocalSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION, newPreLocalSpacePosition, newPreLocalSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getPreLocalSpaceForwardDirection() {
		return preLocalSpaceForwardDirection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreLocalSpaceForwardDirection(ISAVector3 newPreLocalSpaceForwardDirection, NotificationChain msgs) {
		ISAVector3 oldPreLocalSpaceForwardDirection = preLocalSpaceForwardDirection;
		preLocalSpaceForwardDirection = newPreLocalSpaceForwardDirection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION, oldPreLocalSpaceForwardDirection, newPreLocalSpaceForwardDirection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreLocalSpaceForwardDirection(ISAVector3 newPreLocalSpaceForwardDirection) {
		if (newPreLocalSpaceForwardDirection != preLocalSpaceForwardDirection) {
			NotificationChain msgs = null;
			if (preLocalSpaceForwardDirection != null)
				msgs = ((InternalEObject)preLocalSpaceForwardDirection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION, null, msgs);
			if (newPreLocalSpaceForwardDirection != null)
				msgs = ((InternalEObject)newPreLocalSpaceForwardDirection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION, null, msgs);
			msgs = basicSetPreLocalSpaceForwardDirection(newPreLocalSpaceForwardDirection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION, newPreLocalSpaceForwardDirection, newPreLocalSpaceForwardDirection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getPreLocalSpaceUpwardDirection() {
		return preLocalSpaceUpwardDirection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreLocalSpaceUpwardDirection(ISAVector3 newPreLocalSpaceUpwardDirection, NotificationChain msgs) {
		ISAVector3 oldPreLocalSpaceUpwardDirection = preLocalSpaceUpwardDirection;
		preLocalSpaceUpwardDirection = newPreLocalSpaceUpwardDirection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION, oldPreLocalSpaceUpwardDirection, newPreLocalSpaceUpwardDirection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreLocalSpaceUpwardDirection(ISAVector3 newPreLocalSpaceUpwardDirection) {
		if (newPreLocalSpaceUpwardDirection != preLocalSpaceUpwardDirection) {
			NotificationChain msgs = null;
			if (preLocalSpaceUpwardDirection != null)
				msgs = ((InternalEObject)preLocalSpaceUpwardDirection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION, null, msgs);
			if (newPreLocalSpaceUpwardDirection != null)
				msgs = ((InternalEObject)newPreLocalSpaceUpwardDirection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION, null, msgs);
			msgs = basicSetPreLocalSpaceUpwardDirection(newPreLocalSpaceUpwardDirection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION, newPreLocalSpaceUpwardDirection, newPreLocalSpaceUpwardDirection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector2 getPreTextureSpacePosition() {
		return preTextureSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreTextureSpacePosition(ISAVector2 newPreTextureSpacePosition, NotificationChain msgs) {
		ISAVector2 oldPreTextureSpacePosition = preTextureSpacePosition;
		preTextureSpacePosition = newPreTextureSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION, oldPreTextureSpacePosition, newPreTextureSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreTextureSpacePosition(ISAVector2 newPreTextureSpacePosition) {
		if (newPreTextureSpacePosition != preTextureSpacePosition) {
			NotificationChain msgs = null;
			if (preTextureSpacePosition != null)
				msgs = ((InternalEObject)preTextureSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION, null, msgs);
			if (newPreTextureSpacePosition != null)
				msgs = ((InternalEObject)newPreTextureSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION, null, msgs);
			msgs = basicSetPreTextureSpacePosition(newPreTextureSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION, newPreTextureSpacePosition, newPreTextureSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION:
				return basicSetPreLocalSpacePosition(null, msgs);
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION:
				return basicSetPreLocalSpaceForwardDirection(null, msgs);
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION:
				return basicSetPreLocalSpaceUpwardDirection(null, msgs);
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				return basicSetPreTextureSpacePosition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION:
				return getPreLocalSpacePosition();
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION:
				return getPreLocalSpaceForwardDirection();
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION:
				return getPreLocalSpaceUpwardDirection();
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				return getPreTextureSpacePosition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION:
				setPreLocalSpacePosition((ISAVector3)newValue);
				return;
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION:
				setPreLocalSpaceForwardDirection((ISAVector3)newValue);
				return;
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION:
				setPreLocalSpaceUpwardDirection((ISAVector3)newValue);
				return;
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				setPreTextureSpacePosition((ISAVector2)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION:
				setPreLocalSpacePosition((ISAVector3)null);
				return;
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION:
				setPreLocalSpaceForwardDirection((ISAVector3)null);
				return;
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION:
				setPreLocalSpaceUpwardDirection((ISAVector3)null);
				return;
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				setPreTextureSpacePosition((ISAVector2)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION:
				return preLocalSpacePosition != null;
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION:
				return preLocalSpaceForwardDirection != null;
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION:
				return preLocalSpaceUpwardDirection != null;
			case WhiteboardPackage.PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				return preTextureSpacePosition != null;
		}
		return super.eIsSet(featureID);
	}

} //PhotoMoveOperationImpl

package dk.itu.cs.isa.events;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

import dk.itu.cs.isa.events.events.AnalysisSetupEvent;
import dk.itu.cs.isa.events.events.AnalysisSuccessEvent;
import dk.itu.cs.isa.events.events.ConsoleEvent;
import dk.itu.cs.isa.events.events.Event;
import dk.itu.cs.isa.events.events.EventType;
import dk.itu.cs.isa.events.events.EventsFactory;
import dk.itu.cs.isa.events.events.GUIEvent;
import dk.itu.cs.isa.events.events.OutputEvent;
import dk.itu.cs.isa.events.events.StringChoiceEvent;
import dk.itu.cs.isa.events.events.StringInputEvent;

public class ISAEventSystem {
	
	private static EventsFactory factory = EventsFactory.eINSTANCE;
	
	
	
	public static AnalysisSetupEvent fireAnalysisSetupDialog(String guessedName) {
		AnalysisSetupEvent event = factory.createAnalysisSetupEvent();
		event.setSystemName(guessedName);
		
		setupGUIEvent(event, null, null, EventType.QUESTION);
		event.getButtonLabels().clear();
		event.getButtonLabels().add("Start Analysis");
		event.getButtonLabels().add("Cancel");
		
		INSTANCE.fireEvent(event);
		return event;
	}
	
	public static AnalysisSuccessEvent fireAnalysisFinishedDialog(int millisToMinuteString) {
		AnalysisSuccessEvent event = factory.createAnalysisSuccessEvent();
		event.setCalculationTime(millisToMinuteString);

		INSTANCE.fireEvent(event);
		return event;
	}
	
	
	
	public static OutputEvent fireInformationMessage(String text) {
		return fireInformationMessage(null, text);
	}
	
	public static OutputEvent fireInformationMessage(String title, String text) {
		OutputEvent event = factory.createOutputEvent();
		setupGUIEvent(event, title, text, EventType.INFO);
		
		INSTANCE.fireEvent(event);
		return event;
	}
	
	
	
	public static OutputEvent fireConfirmationDialog(String text) {
		return fireConfirmationDialog(null, text);
	}
	
	public static OutputEvent fireConfirmationDialog(String title, String text) {
		return fireConfirmationDialog(title, text, "Continue", "Cancel");
	}
	
	public static OutputEvent fireConfirmationDialog(String title, String text, String ... buttons) {
		OutputEvent event = factory.createOutputEvent();
		setupGUIEvent(event, title, text, EventType.QUESTION);
		event.getButtonLabels().clear();
		for(String buttonLabel : buttons) {
			event.getButtonLabels().add(buttonLabel);
		}
		
		INSTANCE.fireEvent(event);
		return event;
	}
	
	
	
	public static OutputEvent fireWarningMessage(String text) {
		return fireWarningMessage(null, text, null);
	}
	
	public static OutputEvent fireWarningMessage(String title, String text) {
		return fireWarningMessage(title, text, null);
	}
	
	public static OutputEvent fireWarningMessage(String text, Throwable e) {
		return fireErrorMessage(null, text, e);
	}
	
	public static OutputEvent fireWarningMessage(String title, String text, Throwable e) {
		OutputEvent event = factory.createOutputEvent();
		setupGUIEvent(event, title, text, EventType.WARNING);
		
		if(e != null)
			event.setCause(e);
		
		INSTANCE.fireEvent(event);
		return event;
	}
	


	public static OutputEvent fireErrorMessage(String text) {
		return fireErrorMessage(null, text, null);
	}

	public static OutputEvent fireErrorMessage(String title, String text) {
		return fireErrorMessage(title, text, null);
	}
	
	public static OutputEvent fireErrorMessage(String text, Throwable e) {
		return fireErrorMessage(null, text, e);
	}
	
	public static OutputEvent fireErrorMessage(String title, String text, Throwable e) {
		if(title != null)
			fireStandardConsoleMessage(title + ": " + text);
		else
			fireStandardConsoleMessage(text);
		
		OutputEvent event = factory.createOutputEvent();
		setupGUIEvent(event, title, text, EventType.ERROR);
		
		if(e != null)
			event.setCause(e);
		
		INSTANCE.fireEvent(event);
		return event;
	}
	
	
	

	public static StringInputEvent fireStringInputDialog(String text) {
		return fireStringInputDialog(null, text);
	}
	
	public static StringInputEvent fireStringInputDialog(String title, String text) {
		return fireStringInputDialog(title, text, "");
	}
	
	public static StringInputEvent fireStringInputDialog(String title, String text, String initialValue) {
		StringInputEvent event = factory.createStringInputEvent();
		setupGUIEvent(event, title, text, EventType.QUESTION);
		event.setUserInput(initialValue);
		event.getButtonLabels().add("Cancel");
		
		INSTANCE.fireEvent(event);
		
		return event;
	}
	

	
	public static StringChoiceEvent fireStringChoiceDialog(String text, Collection<String> options) {
		return fireStringChoiceDialog(null, text, options);
	}
	
	public static StringChoiceEvent fireStringChoiceDialog(String text, Collection<String> options, String preSelection) {
		return fireStringChoiceDialog(null, text, options, preSelection);
	}
	
	public static StringChoiceEvent fireStringChoiceDialog(String title, String text, Collection<String> options) {
		return fireStringChoiceDialog(title, text, options, "");
	}
	
	public static StringChoiceEvent fireStringChoiceDialog(String title, String text, Collection<String> options, String preSelection) {
		StringChoiceEvent event = factory.createStringChoiceEvent();
		setupGUIEvent(event, title, text, EventType.QUESTION);
		event.getButtonLabels().add("Cancel");
		event.getOptions().addAll(options);
		if(preSelection != null)
			event.setSelection(preSelection);
		
		INSTANCE.fireEvent(event);
		
		return event;
	}
	
	
	
	private static void setupGUIEvent(GUIEvent event, String title, String text, EventType type) {
		if(title != null)
			event.setTitle(title);
		if(text != null)
			event.setText(text);
		if(type != null)
			event.setType(type);
		
		event.getButtonLabels().add("Okay");
	}



	public static void fireStandardConsoleMessage(String text) {
		ConsoleEvent event = factory.createConsoleEvent();
		event.setTimestamp(new Date());
		event.setText(text);
		
		INSTANCE.fireEvent(event);
	}
	
	
	
	

	private static final String EXTENSION_POINT_ID = "dk.itu.cs.isa.util.ISAEventListener";
	
	private static ISAEventSystem INSTANCE = new ISAEventSystem();
	private ISAEventSystem() { }
	
	
	
	private List<ISAEventListener> registeredListeners;
	
	
	
	private void fireEvent(Event event) {
		if(event instanceof OutputEvent) {
			Object cause = ((OutputEvent) event).getCause();
			if(cause instanceof Throwable) {
				((Throwable) cause).printStackTrace();
			}
		}

		if(event.getText() == null) {
			event.setText("");
		}
		if(event instanceof GUIEvent) {
			if(((GUIEvent)event).getTitle() == null) {
				((GUIEvent)event).setTitle("Immersive Software Archeology");
			}
			if(((GUIEvent)event).getButtonLabels().size() == 0) {
				((GUIEvent)event).getButtonLabels().add("Okay");
			}
		}
		
		if(registeredListeners == null) {
			setupListeners();
		}
		
		for(ISAEventListener listener : registeredListeners) {
			listener.notify(event);
		}
	}

	private void setupListeners() {
		registeredListeners = new LinkedList<>();

		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(EXTENSION_POINT_ID);
		for (IConfigurationElement configurationElement : config) {
    		String extensionName = configurationElement.getAttribute("name");
    		Object extensionImplementation;
        	
    		try {
    			extensionImplementation = configurationElement.createExecutableExtension("implementation");
    			registeredListeners.add((ISAEventListener) extensionImplementation);
    		} catch (Exception e) {
    			ISAEventSystem.fireWarningMessage("Could not add implementation of extension \"" + extensionName + "\" to ISA Event Listeners", e);
    			continue;
    		}
		}
	}

}

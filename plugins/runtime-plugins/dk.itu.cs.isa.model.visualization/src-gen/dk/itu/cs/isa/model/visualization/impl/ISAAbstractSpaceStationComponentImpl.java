/**
 */
package dk.itu.cs.isa.model.visualization.impl;

import dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent;
import dk.itu.cs.isa.model.visualization.ISAAntennaRelation;
import dk.itu.cs.isa.model.visualization.ISABlockRelation;
import dk.itu.cs.isa.model.visualization.ISASpaceStationRelation;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Abstract Space Station Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISAAbstractSpaceStationComponentImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISAAbstractSpaceStationComponentImpl#getRelationsToSpaceStationCenters <em>Relations To Space Station Centers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISAAbstractSpaceStationComponentImpl#getRelationsToBlocks <em>Relations To Blocks</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISAAbstractSpaceStationComponentImpl#getRelationsToAntennas <em>Relations To Antennas</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ISAAbstractSpaceStationComponentImpl extends ISANamedVisualElementImpl implements ISAAbstractSpaceStationComponent {
	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ISASpaceStationRelation type;

	/**
	 * The cached value of the '{@link #getRelationsToSpaceStationCenters() <em>Relations To Space Station Centers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationsToSpaceStationCenters()
	 * @generated
	 * @ordered
	 */
	protected EList<ISASpaceStationRelation> relationsToSpaceStationCenters;

	/**
	 * The cached value of the '{@link #getRelationsToBlocks() <em>Relations To Blocks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationsToBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<ISABlockRelation> relationsToBlocks;

	/**
	 * The cached value of the '{@link #getRelationsToAntennas() <em>Relations To Antennas</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationsToAntennas()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAAntennaRelation> relationsToAntennas;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAAbstractSpaceStationComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualizationPackage.Literals.ISA_ABSTRACT_SPACE_STATION_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASpaceStationRelation getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetType(ISASpaceStationRelation newType, NotificationChain msgs) {
		ISASpaceStationRelation oldType = type;
		type = newType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE, oldType, newType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(ISASpaceStationRelation newType) {
		if (newType != type) {
			NotificationChain msgs = null;
			if (type != null)
				msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE, null, msgs);
			if (newType != null)
				msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE, null, msgs);
			msgs = basicSetType(newType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE, newType, newType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISASpaceStationRelation> getRelationsToSpaceStationCenters() {
		if (relationsToSpaceStationCenters == null) {
			relationsToSpaceStationCenters = new EObjectContainmentEList<ISASpaceStationRelation>(ISASpaceStationRelation.class, this, VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS);
		}
		return relationsToSpaceStationCenters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISABlockRelation> getRelationsToBlocks() {
		if (relationsToBlocks == null) {
			relationsToBlocks = new EObjectContainmentEList<ISABlockRelation>(ISABlockRelation.class, this, VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS);
		}
		return relationsToBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISAAntennaRelation> getRelationsToAntennas() {
		if (relationsToAntennas == null) {
			relationsToAntennas = new EObjectContainmentEList<ISAAntennaRelation>(ISAAntennaRelation.class, this, VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS);
		}
		return relationsToAntennas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE:
				return basicSetType(null, msgs);
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS:
				return ((InternalEList<?>)getRelationsToSpaceStationCenters()).basicRemove(otherEnd, msgs);
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS:
				return ((InternalEList<?>)getRelationsToBlocks()).basicRemove(otherEnd, msgs);
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS:
				return ((InternalEList<?>)getRelationsToAntennas()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE:
				return getType();
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS:
				return getRelationsToSpaceStationCenters();
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS:
				return getRelationsToBlocks();
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS:
				return getRelationsToAntennas();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE:
				setType((ISASpaceStationRelation)newValue);
				return;
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS:
				getRelationsToSpaceStationCenters().clear();
				getRelationsToSpaceStationCenters().addAll((Collection<? extends ISASpaceStationRelation>)newValue);
				return;
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS:
				getRelationsToBlocks().clear();
				getRelationsToBlocks().addAll((Collection<? extends ISABlockRelation>)newValue);
				return;
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS:
				getRelationsToAntennas().clear();
				getRelationsToAntennas().addAll((Collection<? extends ISAAntennaRelation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE:
				setType((ISASpaceStationRelation)null);
				return;
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS:
				getRelationsToSpaceStationCenters().clear();
				return;
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS:
				getRelationsToBlocks().clear();
				return;
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS:
				getRelationsToAntennas().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE:
				return type != null;
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS:
				return relationsToSpaceStationCenters != null && !relationsToSpaceStationCenters.isEmpty();
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS:
				return relationsToBlocks != null && !relationsToBlocks.isEmpty();
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS:
				return relationsToAntennas != null && !relationsToAntennas.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ISAAbstractSpaceStationComponentImpl

/**
 */
package dk.itu.cs.isa.model.software.structure.misc.impl;

import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Referenceable Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceableElementImpl#getIncomingReferences <em>Incoming References</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ISAReferenceableElementImpl extends ISANamedSoftwareElementImpl implements ISAReferenceableElement {
	/**
	 * The cached value of the '{@link #getIncomingReferences() <em>Incoming References</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<?>> incomingReferences;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAReferenceableElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiscPackage.Literals.ISA_REFERENCEABLE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<?>> getIncomingReferences() {
		if (incomingReferences == null) {
			incomingReferences = new EObjectWithInverseResolvingEList<ISAReference<?>>(ISAReference.class, this, MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES, MiscPackage.ISA_REFERENCE__TARGET);
		}
		return incomingReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncomingReferences()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES:
				return ((InternalEList<?>)getIncomingReferences()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES:
				return getIncomingReferences();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES:
				getIncomingReferences().clear();
				getIncomingReferences().addAll((Collection<? extends ISAReference<?>>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES:
				getIncomingReferences().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES:
				return incomingReferences != null && !incomingReferences.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ISAReferenceableElementImpl

/**
 */
package dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl;

import dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClusteringFactoryImpl extends EFactoryImpl implements ClusteringFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ClusteringFactory init() {
		try {
			ClusteringFactory theClusteringFactory = (ClusteringFactory)EPackage.Registry.INSTANCE.getEFactory(ClusteringPackage.eNS_URI);
			if (theClusteringFactory != null) {
				return theClusteringFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ClusteringFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClusteringFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ClusteringPackage.ISA_SIMPLE_CLUSTER: return createISASimpleCluster();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASimpleCluster createISASimpleCluster() {
		ISASimpleClusterImpl isaSimpleCluster = new ISASimpleClusterImpl();
		return isaSimpleCluster;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClusteringPackage getClusteringPackage() {
		return (ClusteringPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ClusteringPackage getPackage() {
		return ClusteringPackage.eINSTANCE;
	}

} //ClusteringFactoryImpl

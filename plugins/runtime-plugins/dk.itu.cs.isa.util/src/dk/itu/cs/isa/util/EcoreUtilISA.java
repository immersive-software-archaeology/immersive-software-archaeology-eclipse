package dk.itu.cs.isa.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class EcoreUtilISA {

	
	
	/**
	 * Go through a model, starting from the specified start object, and look for object that have no container.<br>
	 * Specify the root class of a model to be ignored during the checks (because it is expected to have no container). 
	 * @param startObject object to start the search from. Will recursively step through its containments.
	 * @param rootClass 
	 */
	public static void scanForUncontainedElements(EObject startObject, Class<? extends Object> rootClass) {
//		Set<Integer> inspectedObjectHashes = new HashSet<>();
//		doFindUncontainedElements(inspectedObjectHashes, startObject, rootClass);
		// inspected object hashes are not needed anymore, the garbage collector should pick them up
	}
	
	@SuppressWarnings("unused")
	private static void doFindUncontainedElements(Set<Integer> inspectedObjectHashes, EObject e, Class<? extends Object> rootClass) {
		inspectedObjectHashes.add(e.hashCode());
		if(e.eContainer() == null) {
			if(!rootClass.isInstance(e))
				throw new RuntimeException("Found an uncontained element as part of the model structure (is referenced but not contained in the model): " + e.toString());
		}
		else {
			if(!inspectedObjectHashes.contains(e.eContainer().hashCode()))
				doFindUncontainedElements(inspectedObjectHashes, e.eContainer(), rootClass);
		}
		
		for(EObject child : e.eContents()) {
			if(!inspectedObjectHashes.contains(child.hashCode()))
				doFindUncontainedElements(inspectedObjectHashes, child, rootClass);
		}
		
		for(EObject child : e.eCrossReferences()) {
			if(!inspectedObjectHashes.contains(child.hashCode()))
				doFindUncontainedElements(inspectedObjectHashes, child, rootClass);
		}
	}
	
	

	public static String getXMLStringForModel(EObject model) throws IOException {
		if(model.eResource() == null) {			
			ResourceSet resSet = new ResourceSetImpl();
			resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
			
			Resource resource = resSet.createResource(URI.createURI("*"));
			resource.getContents().add(model);
		}
		
		return getXMLStringForModel(model.eResource());
	}
	
	public static String getXMLStringForModel(Resource res) throws IOException {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		res.save(stream, Collections.emptyMap());
		stream.close();
		return stream.toString();
	}
	
	public static EList<EObject> getRootModelObjectFromXMLString(String requestBodyXML, EPackage p, String fileExtension) throws IOException {
		if(requestBodyXML == null)
			return null;
		
		ResourceSet resSet = new ResourceSetImpl();
		// Resource.Factory.Registry.DEFAULT_EXTENSION
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(fileExtension, new XMIResourceFactoryImpl());
		resSet.getPackageRegistry().put(p.getNsURI(), p);
		
	    Resource resource = resSet.createResource(URI.createURI("*." + fileExtension));
	    resource.load(new URIConverter.ReadableInputStream(requestBodyXML), null);
	    
	    return resource.getContents();
	}
	
	
	
	public static Resource loadResource(ResourceSet resourceSet, String fullPath, boolean createOnDemand) {
		Resource resource = null;
		try {
			resource = resourceSet.getResource(URI.createFileURI(fullPath), true);
			if(resource != null) {
				if(resource.getContents().isEmpty()) {
					List<Resource> resourcesToDelete = new LinkedList<>();
					for(Resource res : resourceSet.getResources()) {
						if(res.getURI().equals(resource.getURI())) {
							resourcesToDelete.add(res);
						}
					}
					resourceSet.getResources().removeAll(resourcesToDelete);
					return resourceSet.getResource(URI.createFileURI(fullPath), true);
				}
			}
		}
		catch(Exception e) {
			if(createOnDemand) {
				resource = resourceSet.createResource(URI.createFileURI(fullPath));
			}
		}
		
		return resource;
	}
	
	

}

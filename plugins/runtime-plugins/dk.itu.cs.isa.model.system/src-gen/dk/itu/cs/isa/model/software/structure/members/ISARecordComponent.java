/**
 */
package dk.itu.cs.isa.model.software.structure.members;

import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Record Component</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISARecordComponent()
 * @model
 * @generated
 */
public interface ISARecordComponent extends ISAMember, ISATypedElement {
} // ISARecordComponent

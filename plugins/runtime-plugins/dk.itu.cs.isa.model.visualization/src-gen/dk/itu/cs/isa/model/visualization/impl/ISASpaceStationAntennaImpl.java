/**
 */
package dk.itu.cs.isa.model.visualization.impl;

import dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Space Station Antenna</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationAntennaImpl#isPublic <em>Public</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationAntennaImpl#isEnumConstant <em>Enum Constant</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationAntennaImpl#isRecordComponent <em>Record Component</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISASpaceStationAntennaImpl extends ISAAbstractSpaceStationComponentImpl implements ISASpaceStationAntenna {
	/**
	 * The default value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PUBLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected boolean public_ = PUBLIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isEnumConstant() <em>Enum Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnumConstant()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ENUM_CONSTANT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEnumConstant() <em>Enum Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnumConstant()
	 * @generated
	 * @ordered
	 */
	protected boolean enumConstant = ENUM_CONSTANT_EDEFAULT;

	/**
	 * The default value of the '{@link #isRecordComponent() <em>Record Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRecordComponent()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RECORD_COMPONENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRecordComponent() <em>Record Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRecordComponent()
	 * @generated
	 * @ordered
	 */
	protected boolean recordComponent = RECORD_COMPONENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISASpaceStationAntennaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualizationPackage.Literals.ISA_SPACE_STATION_ANTENNA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPublic() {
		return public_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublic(boolean newPublic) {
		boolean oldPublic = public_;
		public_ = newPublic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION_ANTENNA__PUBLIC, oldPublic, public_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnumConstant() {
		return enumConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumConstant(boolean newEnumConstant) {
		boolean oldEnumConstant = enumConstant;
		enumConstant = newEnumConstant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION_ANTENNA__ENUM_CONSTANT, oldEnumConstant, enumConstant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRecordComponent() {
		return recordComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordComponent(boolean newRecordComponent) {
		boolean oldRecordComponent = recordComponent;
		recordComponent = newRecordComponent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION_ANTENNA__RECORD_COMPONENT, oldRecordComponent, recordComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__PUBLIC:
				return isPublic();
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__ENUM_CONSTANT:
				return isEnumConstant();
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__RECORD_COMPONENT:
				return isRecordComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__PUBLIC:
				setPublic((Boolean)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__ENUM_CONSTANT:
				setEnumConstant((Boolean)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__RECORD_COMPONENT:
				setRecordComponent((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__PUBLIC:
				setPublic(PUBLIC_EDEFAULT);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__ENUM_CONSTANT:
				setEnumConstant(ENUM_CONSTANT_EDEFAULT);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__RECORD_COMPONENT:
				setRecordComponent(RECORD_COMPONENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__PUBLIC:
				return public_ != PUBLIC_EDEFAULT;
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__ENUM_CONSTANT:
				return enumConstant != ENUM_CONSTANT_EDEFAULT;
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA__RECORD_COMPONENT:
				return recordComponent != RECORD_COMPONENT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (public: ");
		result.append(public_);
		result.append(", enumConstant: ");
		result.append(enumConstant);
		result.append(", recordComponent: ");
		result.append(recordComponent);
		result.append(')');
		return result.toString();
	}

} //ISASpaceStationAntennaImpl

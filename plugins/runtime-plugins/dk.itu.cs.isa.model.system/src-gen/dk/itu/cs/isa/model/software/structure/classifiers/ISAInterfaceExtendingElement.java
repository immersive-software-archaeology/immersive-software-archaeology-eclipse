/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Interface Extending Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement#getExtendedInterfaces <em>Extended Interfaces</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAInterfaceExtendingElement()
 * @model abstract="true"
 * @generated
 */
public interface ISAInterfaceExtendingElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Extended Interfaces</b></em>' reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsExtendedInterface <em>Usages As Extended Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended Interfaces</em>' reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAInterfaceExtendingElement_ExtendedInterfaces()
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsExtendedInterface
	 * @model opposite="usagesAsExtendedInterface"
	 * @generated
	 */
	EList<ISAInterface> getExtendedInterfaces();

} // ISAInterfaceExtendingElement

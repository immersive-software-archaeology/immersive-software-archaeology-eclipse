/**
 */
package dk.itu.cs.isa.model.software.structure.libraries;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.libraries.LibrariesFactory
 * @model kind="package"
 * @generated
 */
public interface LibrariesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "libraries";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/software/structure/libraries";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "libraries";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LibrariesPackage eINSTANCE = dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryImpl <em>ISA Library</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryImpl
	 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl#getISALibrary()
	 * @generated
	 */
	int ISA_LIBRARY = 0;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY__CLASSIFIERS = ClassifiersPackage.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY__NAME = ClassifiersPackage.ISA_CLASSIFIER_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Primitive Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY__PRIMITIVE_TYPES = ClassifiersPackage.ISA_CLASSIFIER_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>ISA Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_FEATURE_COUNT = ClassifiersPackage.ISA_CLASSIFIER_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY___GET_ELEMENT__STRING = ClassifiersPackage.ISA_CLASSIFIER_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>ISA Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_OPERATION_COUNT = ClassifiersPackage.ISA_CLASSIFIER_CONTAINER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISAPrimitiveTypeImpl <em>ISA Primitive Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.ISAPrimitiveTypeImpl
	 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl#getISAPrimitiveType()
	 * @generated
	 */
	int ISA_PRIMITIVE_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PRIMITIVE_TYPE__NAME = MiscPackage.ISA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PRIMITIVE_TYPE__INCOMING_REFERENCES = MiscPackage.ISA_TYPE__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PRIMITIVE_TYPE__USAGES_AS_TYPE = MiscPackage.ISA_TYPE__USAGES_AS_TYPE;

	/**
	 * The number of structural features of the '<em>ISA Primitive Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PRIMITIVE_TYPE_FEATURE_COUNT = MiscPackage.ISA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PRIMITIVE_TYPE___GET_FULLY_QUALIFIED_NAME = MiscPackage.ISA_TYPE___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PRIMITIVE_TYPE___TO_STRING = MiscPackage.ISA_TYPE___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Primitive Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PRIMITIVE_TYPE_OPERATION_COUNT = MiscPackage.ISA_TYPE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryClassifierImpl <em>ISA Library Classifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryClassifierImpl
	 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl#getISALibraryClassifier()
	 * @generated
	 */
	int ISA_LIBRARY_CLASSIFIER = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__NAME = ClassifiersPackage.ISA_CLASS__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__INCOMING_REFERENCES = ClassifiersPackage.ISA_CLASS__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__USAGES_AS_TYPE = ClassifiersPackage.ISA_CLASS__USAGES_AS_TYPE;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__CLASSIFIERS = ClassifiersPackage.ISA_CLASS__CLASSIFIERS;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__MODIFIERS = ClassifiersPackage.ISA_CLASS__MODIFIERS;

	/**
	 * The feature id for the '<em><b>Number In Order Of Declaration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION = ClassifiersPackage.ISA_CLASS__NUMBER_IN_ORDER_OF_DECLARATION;

	/**
	 * The feature id for the '<em><b>Start Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__START_POSITION_IN_DOCUMENT = ClassifiersPackage.ISA_CLASS__START_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>End Position In Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__END_POSITION_IN_DOCUMENT = ClassifiersPackage.ISA_CLASS__END_POSITION_IN_DOCUMENT;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__PARENT = ClassifiersPackage.ISA_CLASS__PARENT;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__FIELDS = ClassifiersPackage.ISA_CLASS__FIELDS;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__METHODS = ClassifiersPackage.ISA_CLASS__METHODS;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__FILE_PATH = ClassifiersPackage.ISA_CLASS__FILE_PATH;

	/**
	 * The feature id for the '<em><b>Constructors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__CONSTRUCTORS = ClassifiersPackage.ISA_CLASS__CONSTRUCTORS;

	/**
	 * The feature id for the '<em><b>Extended Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__EXTENDED_CLASS = ClassifiersPackage.ISA_CLASS__EXTENDED_CLASS;

	/**
	 * The feature id for the '<em><b>Implemented Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__IMPLEMENTED_INTERFACES = ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Usages As Extended Class</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_CLASS = ClassifiersPackage.ISA_CLASS__USAGES_AS_EXTENDED_CLASS;

	/**
	 * The feature id for the '<em><b>Extended Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES = ClassifiersPackage.ISA_CLASS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Usages As Implemented Interface</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE = ClassifiersPackage.ISA_CLASS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Usages As Extended Interface</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE = ClassifiersPackage.ISA_CLASS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Constants</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__CONSTANTS = ClassifiersPackage.ISA_CLASS_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER__COMPONENTS = ClassifiersPackage.ISA_CLASS_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>ISA Library Classifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER_FEATURE_COUNT = ClassifiersPackage.ISA_CLASS_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER___GET_FULLY_QUALIFIED_NAME = ClassifiersPackage.ISA_CLASS___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER___TO_STRING = ClassifiersPackage.ISA_CLASS___TO_STRING;

	/**
	 * The operation id for the '<em>Get Containing Package</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER___GET_CONTAINING_PACKAGE = ClassifiersPackage.ISA_CLASS___GET_CONTAINING_PACKAGE;

	/**
	 * The operation id for the '<em>Get Contained Members</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER___GET_CONTAINED_MEMBERS = ClassifiersPackage.ISA_CLASS___GET_CONTAINED_MEMBERS;

	/**
	 * The number of operations of the '<em>ISA Library Classifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_LIBRARY_CLASSIFIER_OPERATION_COUNT = ClassifiersPackage.ISA_CLASS_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibrary <em>ISA Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Library</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.ISALibrary
	 * @generated
	 */
	EClass getISALibrary();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibrary#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.ISALibrary#getName()
	 * @see #getISALibrary()
	 * @generated
	 */
	EAttribute getISALibrary_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibrary#getPrimitiveTypes <em>Primitive Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Primitive Types</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.ISALibrary#getPrimitiveTypes()
	 * @see #getISALibrary()
	 * @generated
	 */
	EReference getISALibrary_PrimitiveTypes();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibrary#getElement(java.lang.String) <em>Get Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Element</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.ISALibrary#getElement(java.lang.String)
	 * @generated
	 */
	EOperation getISALibrary__GetElement__String();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.libraries.ISAPrimitiveType <em>ISA Primitive Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Primitive Type</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.ISAPrimitiveType
	 * @generated
	 */
	EClass getISAPrimitiveType();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibraryClassifier <em>ISA Library Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Library Classifier</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.ISALibraryClassifier
	 * @generated
	 */
	EClass getISALibraryClassifier();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LibrariesFactory getLibrariesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryImpl <em>ISA Library</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryImpl
		 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl#getISALibrary()
		 * @generated
		 */
		EClass ISA_LIBRARY = eINSTANCE.getISALibrary();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_LIBRARY__NAME = eINSTANCE.getISALibrary_Name();

		/**
		 * The meta object literal for the '<em><b>Primitive Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_LIBRARY__PRIMITIVE_TYPES = eINSTANCE.getISALibrary_PrimitiveTypes();

		/**
		 * The meta object literal for the '<em><b>Get Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_LIBRARY___GET_ELEMENT__STRING = eINSTANCE.getISALibrary__GetElement__String();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISAPrimitiveTypeImpl <em>ISA Primitive Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.ISAPrimitiveTypeImpl
		 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl#getISAPrimitiveType()
		 * @generated
		 */
		EClass ISA_PRIMITIVE_TYPE = eINSTANCE.getISAPrimitiveType();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryClassifierImpl <em>ISA Library Classifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryClassifierImpl
		 * @see dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl#getISALibraryClassifier()
		 * @generated
		 */
		EClass ISA_LIBRARY_CLASSIFIER = eINSTANCE.getISALibraryClassifier();

	}

} //LibrariesPackage

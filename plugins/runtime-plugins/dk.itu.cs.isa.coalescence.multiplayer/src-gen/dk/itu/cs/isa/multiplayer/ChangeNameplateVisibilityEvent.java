/**
 */
package dk.itu.cs.isa.multiplayer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Change Nameplate Visibility Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent#isVisible <em>Visible</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getChangeNameplateVisibilityEvent()
 * @model
 * @generated
 */
public interface ChangeNameplateVisibilityEvent extends AbstractEvent {
	/**
	 * Returns the value of the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visible</em>' attribute.
	 * @see #setVisible(boolean)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getChangeNameplateVisibilityEvent_Visible()
	 * @model required="true"
	 * @generated
	 */
	boolean isVisible();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent#isVisible <em>Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visible</em>' attribute.
	 * @see #isVisible()
	 * @generated
	 */
	void setVisible(boolean value);

} // ChangeNameplateVisibilityEvent

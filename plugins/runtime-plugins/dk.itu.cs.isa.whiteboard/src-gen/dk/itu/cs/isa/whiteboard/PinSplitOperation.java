/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pin Split Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PinSplitOperation#getPinnedElementName <em>Pinned Element Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PinSplitOperation#getPinId <em>Pin Id</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinSplitOperation()
 * @model
 * @generated
 */
public interface PinSplitOperation extends AbstractCompoundOperation {
	/**
	 * Returns the value of the '<em><b>Pinned Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pinned Element Name</em>' attribute.
	 * @see #setPinnedElementName(String)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinSplitOperation_PinnedElementName()
	 * @model required="true"
	 * @generated
	 */
	String getPinnedElementName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PinSplitOperation#getPinnedElementName <em>Pinned Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pinned Element Name</em>' attribute.
	 * @see #getPinnedElementName()
	 * @generated
	 */
	void setPinnedElementName(String value);

	/**
	 * Returns the value of the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pin Id</em>' attribute.
	 * @see #setPinId(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinSplitOperation_PinId()
	 * @model required="true"
	 * @generated
	 */
	int getPinId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PinSplitOperation#getPinId <em>Pin Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pin Id</em>' attribute.
	 * @see #getPinId()
	 * @generated
	 */
	void setPinId(int value);

} // PinSplitOperation

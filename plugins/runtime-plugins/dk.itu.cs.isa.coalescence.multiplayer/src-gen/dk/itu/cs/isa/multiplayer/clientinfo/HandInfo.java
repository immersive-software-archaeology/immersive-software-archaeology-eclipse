/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hand Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getIndexFingerTipPosition <em>Index Finger Tip Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getPose <em>Pose</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getHeldObject <em>Held Object</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getHandInfo()
 * @model
 * @generated
 */
public interface HandInfo extends BodyPartInfo {
	/**
	 * Returns the value of the '<em><b>Index Finger Tip Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Finger Tip Position</em>' containment reference.
	 * @see #setIndexFingerTipPosition(ISAVector3)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getHandInfo_IndexFingerTipPosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getIndexFingerTipPosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getIndexFingerTipPosition <em>Index Finger Tip Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Finger Tip Position</em>' containment reference.
	 * @see #getIndexFingerTipPosition()
	 * @generated
	 */
	void setIndexFingerTipPosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Pose</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.itu.cs.isa.multiplayer.clientinfo.HandPose}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pose</em>' attribute.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandPose
	 * @see #setPose(HandPose)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getHandInfo_Pose()
	 * @model required="true"
	 * @generated
	 */
	HandPose getPose();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getPose <em>Pose</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pose</em>' attribute.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandPose
	 * @see #getPose()
	 * @generated
	 */
	void setPose(HandPose value);

	/**
	 * Returns the value of the '<em><b>Held Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Held Object</em>' containment reference.
	 * @see #setHeldObject(HandHeldObjectInfo)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getHandInfo_HeldObject()
	 * @model containment="true" required="true"
	 * @generated
	 */
	HandHeldObjectInfo getHeldObject();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getHeldObject <em>Held Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Held Object</em>' containment reference.
	 * @see #getHeldObject()
	 * @generated
	 */
	void setHeldObject(HandHeldObjectInfo value);

} // HandInfo

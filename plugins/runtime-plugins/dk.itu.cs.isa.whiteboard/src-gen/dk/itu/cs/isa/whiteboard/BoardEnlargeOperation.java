/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Board Enlarge Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getBoardEnlargeOperation()
 * @model
 * @generated
 */
public interface BoardEnlargeOperation extends AbstractDrawOperation {
} // BoardEnlargeOperation

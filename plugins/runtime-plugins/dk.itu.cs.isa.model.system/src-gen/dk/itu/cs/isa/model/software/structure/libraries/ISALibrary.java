/**
 */
package dk.itu.cs.isa.model.software.structure.libraries;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer;

import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Library</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibrary#getName <em>Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibrary#getPrimitiveTypes <em>Primitive Types</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage#getISALibrary()
 * @model
 * @generated
 */
public interface ISALibrary extends ISAClassifierContainer {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage#getISALibrary_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibrary#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Primitive Types</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.libraries.ISAPrimitiveType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primitive Types</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage#getISALibrary_PrimitiveTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAPrimitiveType> getPrimitiveTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model qualifiedElementNameRequired="true"
	 * @generated
	 */
	ISANamedSoftwareElement getElement(String qualifiedElementName);

} // ISALibrary

/**
 */
package dk.itu.cs.isa.model.software.structure.misc.provider;


import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ISAStatementContainerItemProvider extends ISAReferencingElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAStatementContainerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNumberOfStatementsPropertyDescriptor(object);
			addNumberOfExpressionsPropertyDescriptor(object);
			addNumberOfControlFlowSplitsPropertyDescriptor(object);
			addCognitiveComplexityPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Number Of Statements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberOfStatementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAStatementContainer_numberOfStatements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAStatementContainer_numberOfStatements_feature", "_UI_ISAStatementContainer_type"),
				 MiscPackage.Literals.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number Of Expressions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberOfExpressionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAStatementContainer_numberOfExpressions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAStatementContainer_numberOfExpressions_feature", "_UI_ISAStatementContainer_type"),
				 MiscPackage.Literals.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number Of Control Flow Splits feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberOfControlFlowSplitsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAStatementContainer_numberOfControlFlowSplits_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAStatementContainer_numberOfControlFlowSplits_feature", "_UI_ISAStatementContainer_type"),
				 MiscPackage.Literals.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Cognitive Complexity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCognitiveComplexityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAStatementContainer_cognitiveComplexity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAStatementContainer_cognitiveComplexity_feature", "_UI_ISAStatementContainer_type"),
				 MiscPackage.Literals.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		ISAStatementContainer isaStatementContainer = (ISAStatementContainer)object;
		return getString("_UI_ISAStatementContainer_type") + " " + isaStatementContainer.getNumberOfStatements();
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ISAStatementContainer.class)) {
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS:
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS:
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS:
			case MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__METHOD_CALLS ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}

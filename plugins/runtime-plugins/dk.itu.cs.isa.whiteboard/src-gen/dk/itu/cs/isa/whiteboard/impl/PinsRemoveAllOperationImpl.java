/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.PinsRemoveAllOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pins Remove All Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PinsRemoveAllOperationImpl extends AbstractCompoundOperationImpl implements PinsRemoveAllOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PinsRemoveAllOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PINS_REMOVE_ALL_OPERATION;
	}

} //PinsRemoveAllOperationImpl

/**
 */
package dk.itu.cs.isa.model.software.structure.members.impl;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Constructor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getTypeReferences <em>Type References</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getConstructorCalls <em>Constructor Calls</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getMethodCalls <em>Method Calls</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getFieldAccesses <em>Field Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getConstantAccesses <em>Constant Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getComponentAccesses <em>Component Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getNumberOfStatements <em>Number Of Statements</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getNumberOfExpressions <em>Number Of Expressions</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getNumberOfControlFlowSplits <em>Number Of Control Flow Splits</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getCognitiveComplexity <em>Cognitive Complexity</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConstructorImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAConstructorImpl extends ISAMemberImpl implements ISAConstructor {
	/**
	 * The cached value of the '{@link #getTypeReferences() <em>Type References</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAType>> typeReferences;

	/**
	 * The cached value of the '{@link #getConstructorCalls() <em>Constructor Calls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstructorCalls()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAConstructor>> constructorCalls;

	/**
	 * The cached value of the '{@link #getMethodCalls() <em>Method Calls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethodCalls()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAMethod>> methodCalls;

	/**
	 * The cached value of the '{@link #getFieldAccesses() <em>Field Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFieldAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAField>> fieldAccesses;

	/**
	 * The cached value of the '{@link #getConstantAccesses() <em>Constant Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAEnumConstant>> constantAccesses;

	/**
	 * The cached value of the '{@link #getComponentAccesses() <em>Component Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISARecordComponent>> componentAccesses;

	/**
	 * The default value of the '{@link #getNumberOfStatements() <em>Number Of Statements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfStatements()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_STATEMENTS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfStatements() <em>Number Of Statements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfStatements()
	 * @generated
	 * @ordered
	 */
	protected int numberOfStatements = NUMBER_OF_STATEMENTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberOfExpressions() <em>Number Of Expressions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfExpressions()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_EXPRESSIONS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfExpressions() <em>Number Of Expressions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfExpressions()
	 * @generated
	 * @ordered
	 */
	protected int numberOfExpressions = NUMBER_OF_EXPRESSIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberOfControlFlowSplits() <em>Number Of Control Flow Splits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfControlFlowSplits()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfControlFlowSplits() <em>Number Of Control Flow Splits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfControlFlowSplits()
	 * @generated
	 * @ordered
	 */
	protected int numberOfControlFlowSplits = NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCognitiveComplexity() <em>Cognitive Complexity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCognitiveComplexity()
	 * @generated
	 * @ordered
	 */
	protected static final int COGNITIVE_COMPLEXITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCognitiveComplexity() <em>Cognitive Complexity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCognitiveComplexity()
	 * @generated
	 * @ordered
	 */
	protected int cognitiveComplexity = COGNITIVE_COMPLEXITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAParameter> parameters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAConstructorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MembersPackage.Literals.ISA_CONSTRUCTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAType>> getTypeReferences() {
		if (typeReferences == null) {
			typeReferences = new EObjectContainmentEList<ISAReference<ISAType>>(ISAReference.class, this, MembersPackage.ISA_CONSTRUCTOR__TYPE_REFERENCES);
		}
		return typeReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAConstructor>> getConstructorCalls() {
		if (constructorCalls == null) {
			constructorCalls = new EObjectContainmentEList<ISAReference<ISAConstructor>>(ISAReference.class, this, MembersPackage.ISA_CONSTRUCTOR__CONSTRUCTOR_CALLS);
		}
		return constructorCalls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAMethod>> getMethodCalls() {
		if (methodCalls == null) {
			methodCalls = new EObjectContainmentEList<ISAReference<ISAMethod>>(ISAReference.class, this, MembersPackage.ISA_CONSTRUCTOR__METHOD_CALLS);
		}
		return methodCalls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAField>> getFieldAccesses() {
		if (fieldAccesses == null) {
			fieldAccesses = new EObjectContainmentEList<ISAReference<ISAField>>(ISAReference.class, this, MembersPackage.ISA_CONSTRUCTOR__FIELD_ACCESSES);
		}
		return fieldAccesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAEnumConstant>> getConstantAccesses() {
		if (constantAccesses == null) {
			constantAccesses = new EObjectContainmentEList<ISAReference<ISAEnumConstant>>(ISAReference.class, this, MembersPackage.ISA_CONSTRUCTOR__CONSTANT_ACCESSES);
		}
		return constantAccesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISARecordComponent>> getComponentAccesses() {
		if (componentAccesses == null) {
			componentAccesses = new EObjectContainmentEList<ISAReference<ISARecordComponent>>(ISAReference.class, this, MembersPackage.ISA_CONSTRUCTOR__COMPONENT_ACCESSES);
		}
		return componentAccesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfStatements() {
		return numberOfStatements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfStatements(int newNumberOfStatements) {
		int oldNumberOfStatements = numberOfStatements;
		numberOfStatements = newNumberOfStatements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_STATEMENTS, oldNumberOfStatements, numberOfStatements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfExpressions() {
		return numberOfExpressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfExpressions(int newNumberOfExpressions) {
		int oldNumberOfExpressions = numberOfExpressions;
		numberOfExpressions = newNumberOfExpressions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_EXPRESSIONS, oldNumberOfExpressions, numberOfExpressions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfControlFlowSplits() {
		return numberOfControlFlowSplits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfControlFlowSplits(int newNumberOfControlFlowSplits) {
		int oldNumberOfControlFlowSplits = numberOfControlFlowSplits;
		numberOfControlFlowSplits = newNumberOfControlFlowSplits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_CONTROL_FLOW_SPLITS, oldNumberOfControlFlowSplits, numberOfControlFlowSplits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getCognitiveComplexity() {
		return cognitiveComplexity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCognitiveComplexity(int newCognitiveComplexity) {
		int oldCognitiveComplexity = cognitiveComplexity;
		cognitiveComplexity = newCognitiveComplexity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONSTRUCTOR__COGNITIVE_COMPLEXITY, oldCognitiveComplexity, cognitiveComplexity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAParameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<ISAParameter>(ISAParameter.class, this, MembersPackage.ISA_CONSTRUCTOR__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MembersPackage.ISA_CONSTRUCTOR__TYPE_REFERENCES:
				return ((InternalEList<?>)getTypeReferences()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_CONSTRUCTOR__CONSTRUCTOR_CALLS:
				return ((InternalEList<?>)getConstructorCalls()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_CONSTRUCTOR__METHOD_CALLS:
				return ((InternalEList<?>)getMethodCalls()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_CONSTRUCTOR__FIELD_ACCESSES:
				return ((InternalEList<?>)getFieldAccesses()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_CONSTRUCTOR__CONSTANT_ACCESSES:
				return ((InternalEList<?>)getConstantAccesses()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_CONSTRUCTOR__COMPONENT_ACCESSES:
				return ((InternalEList<?>)getComponentAccesses()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_CONSTRUCTOR__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MembersPackage.ISA_CONSTRUCTOR__TYPE_REFERENCES:
				return getTypeReferences();
			case MembersPackage.ISA_CONSTRUCTOR__CONSTRUCTOR_CALLS:
				return getConstructorCalls();
			case MembersPackage.ISA_CONSTRUCTOR__METHOD_CALLS:
				return getMethodCalls();
			case MembersPackage.ISA_CONSTRUCTOR__FIELD_ACCESSES:
				return getFieldAccesses();
			case MembersPackage.ISA_CONSTRUCTOR__CONSTANT_ACCESSES:
				return getConstantAccesses();
			case MembersPackage.ISA_CONSTRUCTOR__COMPONENT_ACCESSES:
				return getComponentAccesses();
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_STATEMENTS:
				return getNumberOfStatements();
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_EXPRESSIONS:
				return getNumberOfExpressions();
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_CONTROL_FLOW_SPLITS:
				return getNumberOfControlFlowSplits();
			case MembersPackage.ISA_CONSTRUCTOR__COGNITIVE_COMPLEXITY:
				return getCognitiveComplexity();
			case MembersPackage.ISA_CONSTRUCTOR__PARAMETERS:
				return getParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MembersPackage.ISA_CONSTRUCTOR__TYPE_REFERENCES:
				getTypeReferences().clear();
				getTypeReferences().addAll((Collection<? extends ISAReference<ISAType>>)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__CONSTRUCTOR_CALLS:
				getConstructorCalls().clear();
				getConstructorCalls().addAll((Collection<? extends ISAReference<ISAConstructor>>)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__METHOD_CALLS:
				getMethodCalls().clear();
				getMethodCalls().addAll((Collection<? extends ISAReference<ISAMethod>>)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__FIELD_ACCESSES:
				getFieldAccesses().clear();
				getFieldAccesses().addAll((Collection<? extends ISAReference<ISAField>>)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__CONSTANT_ACCESSES:
				getConstantAccesses().clear();
				getConstantAccesses().addAll((Collection<? extends ISAReference<ISAEnumConstant>>)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__COMPONENT_ACCESSES:
				getComponentAccesses().clear();
				getComponentAccesses().addAll((Collection<? extends ISAReference<ISARecordComponent>>)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_STATEMENTS:
				setNumberOfStatements((Integer)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_EXPRESSIONS:
				setNumberOfExpressions((Integer)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_CONTROL_FLOW_SPLITS:
				setNumberOfControlFlowSplits((Integer)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__COGNITIVE_COMPLEXITY:
				setCognitiveComplexity((Integer)newValue);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends ISAParameter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_CONSTRUCTOR__TYPE_REFERENCES:
				getTypeReferences().clear();
				return;
			case MembersPackage.ISA_CONSTRUCTOR__CONSTRUCTOR_CALLS:
				getConstructorCalls().clear();
				return;
			case MembersPackage.ISA_CONSTRUCTOR__METHOD_CALLS:
				getMethodCalls().clear();
				return;
			case MembersPackage.ISA_CONSTRUCTOR__FIELD_ACCESSES:
				getFieldAccesses().clear();
				return;
			case MembersPackage.ISA_CONSTRUCTOR__CONSTANT_ACCESSES:
				getConstantAccesses().clear();
				return;
			case MembersPackage.ISA_CONSTRUCTOR__COMPONENT_ACCESSES:
				getComponentAccesses().clear();
				return;
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_STATEMENTS:
				setNumberOfStatements(NUMBER_OF_STATEMENTS_EDEFAULT);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_EXPRESSIONS:
				setNumberOfExpressions(NUMBER_OF_EXPRESSIONS_EDEFAULT);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_CONTROL_FLOW_SPLITS:
				setNumberOfControlFlowSplits(NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__COGNITIVE_COMPLEXITY:
				setCognitiveComplexity(COGNITIVE_COMPLEXITY_EDEFAULT);
				return;
			case MembersPackage.ISA_CONSTRUCTOR__PARAMETERS:
				getParameters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_CONSTRUCTOR__TYPE_REFERENCES:
				return typeReferences != null && !typeReferences.isEmpty();
			case MembersPackage.ISA_CONSTRUCTOR__CONSTRUCTOR_CALLS:
				return constructorCalls != null && !constructorCalls.isEmpty();
			case MembersPackage.ISA_CONSTRUCTOR__METHOD_CALLS:
				return methodCalls != null && !methodCalls.isEmpty();
			case MembersPackage.ISA_CONSTRUCTOR__FIELD_ACCESSES:
				return fieldAccesses != null && !fieldAccesses.isEmpty();
			case MembersPackage.ISA_CONSTRUCTOR__CONSTANT_ACCESSES:
				return constantAccesses != null && !constantAccesses.isEmpty();
			case MembersPackage.ISA_CONSTRUCTOR__COMPONENT_ACCESSES:
				return componentAccesses != null && !componentAccesses.isEmpty();
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_STATEMENTS:
				return numberOfStatements != NUMBER_OF_STATEMENTS_EDEFAULT;
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_EXPRESSIONS:
				return numberOfExpressions != NUMBER_OF_EXPRESSIONS_EDEFAULT;
			case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_CONTROL_FLOW_SPLITS:
				return numberOfControlFlowSplits != NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT;
			case MembersPackage.ISA_CONSTRUCTOR__COGNITIVE_COMPLEXITY:
				return cognitiveComplexity != COGNITIVE_COMPLEXITY_EDEFAULT;
			case MembersPackage.ISA_CONSTRUCTOR__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAReferencingElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONSTRUCTOR__TYPE_REFERENCES: return MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES;
				case MembersPackage.ISA_CONSTRUCTOR__CONSTRUCTOR_CALLS: return MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS;
				case MembersPackage.ISA_CONSTRUCTOR__METHOD_CALLS: return MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS;
				case MembersPackage.ISA_CONSTRUCTOR__FIELD_ACCESSES: return MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES;
				case MembersPackage.ISA_CONSTRUCTOR__CONSTANT_ACCESSES: return MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES;
				case MembersPackage.ISA_CONSTRUCTOR__COMPONENT_ACCESSES: return MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES;
				default: return -1;
			}
		}
		if (baseClass == ISAStatementContainer.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_STATEMENTS: return MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS;
				case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_EXPRESSIONS: return MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS;
				case MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_CONTROL_FLOW_SPLITS: return MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS;
				case MembersPackage.ISA_CONSTRUCTOR__COGNITIVE_COMPLEXITY: return MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY;
				default: return -1;
			}
		}
		if (baseClass == ISAParametrizableElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONSTRUCTOR__PARAMETERS: return MiscPackage.ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAReferencingElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES: return MembersPackage.ISA_CONSTRUCTOR__TYPE_REFERENCES;
				case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS: return MembersPackage.ISA_CONSTRUCTOR__CONSTRUCTOR_CALLS;
				case MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS: return MembersPackage.ISA_CONSTRUCTOR__METHOD_CALLS;
				case MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES: return MembersPackage.ISA_CONSTRUCTOR__FIELD_ACCESSES;
				case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES: return MembersPackage.ISA_CONSTRUCTOR__CONSTANT_ACCESSES;
				case MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES: return MembersPackage.ISA_CONSTRUCTOR__COMPONENT_ACCESSES;
				default: return -1;
			}
		}
		if (baseClass == ISAStatementContainer.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS: return MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_STATEMENTS;
				case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS: return MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_EXPRESSIONS;
				case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS: return MembersPackage.ISA_CONSTRUCTOR__NUMBER_OF_CONTROL_FLOW_SPLITS;
				case MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY: return MembersPackage.ISA_CONSTRUCTOR__COGNITIVE_COMPLEXITY;
				default: return -1;
			}
		}
		if (baseClass == ISAParametrizableElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS: return MembersPackage.ISA_CONSTRUCTOR__PARAMETERS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (numberOfStatements: ");
		result.append(numberOfStatements);
		result.append(", numberOfExpressions: ");
		result.append(numberOfExpressions);
		result.append(", numberOfControlFlowSplits: ");
		result.append(numberOfControlFlowSplits);
		result.append(", cognitiveComplexity: ");
		result.append(cognitiveComplexity);
		result.append(')');
		return result.toString();
	}

} //ISAConstructorImpl

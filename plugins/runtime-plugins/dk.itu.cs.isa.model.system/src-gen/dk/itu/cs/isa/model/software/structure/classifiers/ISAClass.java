/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getExtendedClass <em>Extended Class</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getImplementedInterfaces <em>Implemented Interfaces</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getUsagesAsExtendedClass <em>Usages As Extended Class</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClass()
 * @model abstract="true"
 * @generated
 */
public interface ISAClass extends ISAClassifier, ISAConstructorContainer {
	/**
	 * Returns the value of the '<em><b>Extended Class</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getUsagesAsExtendedClass <em>Usages As Extended Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended Class</em>' reference.
	 * @see #setExtendedClass(ISAClass)
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClass_ExtendedClass()
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getUsagesAsExtendedClass
	 * @model opposite="usagesAsExtendedClass"
	 * @generated
	 */
	ISAClass getExtendedClass();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getExtendedClass <em>Extended Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extended Class</em>' reference.
	 * @see #getExtendedClass()
	 * @generated
	 */
	void setExtendedClass(ISAClass value);

	/**
	 * Returns the value of the '<em><b>Implemented Interfaces</b></em>' reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsImplementedInterface <em>Usages As Implemented Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implemented Interfaces</em>' reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClass_ImplementedInterfaces()
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsImplementedInterface
	 * @model opposite="usagesAsImplementedInterface"
	 * @generated
	 */
	EList<ISAInterface> getImplementedInterfaces();

	/**
	 * Returns the value of the '<em><b>Usages As Extended Class</b></em>' reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getExtendedClass <em>Extended Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usages As Extended Class</em>' reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClass_UsagesAsExtendedClass()
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getExtendedClass
	 * @model opposite="extendedClass"
	 * @generated
	 */
	EList<ISAClass> getUsagesAsExtendedClass();

} // ISAClass

/**
 */
package dk.itu.cs.isa.multiplayer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sphere Open Close Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent#getElementName <em>Element Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent#isNowOpen <em>Now Open</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getSphereOpenCloseEvent()
 * @model
 * @generated
 */
public interface SphereOpenCloseEvent extends AbstractEvent {
	/**
	 * Returns the value of the '<em><b>Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Name</em>' attribute.
	 * @see #setElementName(String)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getSphereOpenCloseEvent_ElementName()
	 * @model required="true"
	 * @generated
	 */
	String getElementName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent#getElementName <em>Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Name</em>' attribute.
	 * @see #getElementName()
	 * @generated
	 */
	void setElementName(String value);

	/**
	 * Returns the value of the '<em><b>Now Open</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Now Open</em>' attribute.
	 * @see #setNowOpen(boolean)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getSphereOpenCloseEvent_NowOpen()
	 * @model required="true"
	 * @generated
	 */
	boolean isNowOpen();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent#isNowOpen <em>Now Open</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Now Open</em>' attribute.
	 * @see #isNowOpen()
	 * @generated
	 */
	void setNowOpen(boolean value);

} // SphereOpenCloseEvent

package dk.itu.cs.isa.coalescence.server.ui.views;

import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;

import dk.itu.cs.isa.coalescence.server.ISACoalescenceServer;
import dk.itu.cs.isa.coalescence.server.ISAServer.ServerState;
import dk.itu.cs.isa.coalescence.server.extensions.ExtensionPointHandler;
import dk.itu.cs.isa.coalescence.server.extensions.ISAAbstractSocketImplementation;
import dk.itu.cs.isa.eclipse.ui.ImageHelper;
import dk.itu.cs.isa.eclipse.ui.views.server.ISAServerStatusView;
import dk.itu.cs.isa.multiplayer.udp.SynchronizationUdpServer;

public class ISACoalescenceServerStatusView extends ISAServerStatusView {
	
	public static FileOpenUtil FILE_OPENER;
	
	@Inject
	IWorkbench workbench;

	private Image serverOnlineBannerImage;
	private Image serverOfflineBannerImage;
	
	private Label statusBannerImageLabel;
	
	private Label clientTextLabel;
	
	
	
	public ISACoalescenceServerStatusView() {
		super();
		serverOnlineBannerImage = ImageHelper.getImage("images/connection_vr_ide_-_connected_-_transparent.png");
		serverOfflineBannerImage = ImageHelper.getImage("images/connection_vr_ide_-_disconnected_-_transparent.png");
	}

	@Override
	protected void setServer() {
		this.server = ISACoalescenceServer.INSTANCE;
	}
	
	@Override
	protected void refreshThreadSafe(ServerState serverState, String serverAddress, boolean canBeStarted, boolean canBeStopped) {
		super.refreshThreadSafe(serverState, serverAddress, canBeStarted, canBeStopped);
		if (serverState.equals(ServerState.ONLINE))
			statusBannerImageLabel.setImage(serverOnlineBannerImage);
		else
			statusBannerImageLabel.setImage(serverOfflineBannerImage);
		
		for(ISAAbstractSocketImplementation socket : ExtensionPointHandler.INSTANCE.getSockets())
			if(socket instanceof SynchronizationUdpServer)
				clientTextLabel.setText(((SynchronizationUdpServer) socket).toString());
	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		
		GridData layoutData = new GridData(SWT.CENTER, SWT.TOP, false, false, 5, 1);
		statusBannerImageLabel = new Label(parent, SWT.NONE);
		statusBannerImageLabel.setLayoutData(layoutData);
		
		FILE_OPENER = new FileOpenUtil(workbench);

		clientTextLabel = new Label(parent, SWT.LEFT);
		clientTextLabel.setText("none");
//		statusLabel.setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));
		clientTextLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 5, 1));
		clientTextLabel.setForeground(new Color(128, 128, 128));
	}
	
	@Override
	public void dispose() {
		super.dispose();
		serverOnlineBannerImage.dispose();
		serverOfflineBannerImage.dispose();
	}

}

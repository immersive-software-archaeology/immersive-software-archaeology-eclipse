package dk.itu.cs.isa.model.software.structure.classifiers.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAAbstractClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAConcreteClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;

public class ClassifiersFactoryImplCustom extends ClassifiersFactoryImpl {

	@Override
	public ISAAbstractClass createISAAbstractClass() {
		return super.createISAAbstractClass();
	}

	@Override
	public ISAConcreteClass createISAConcreteClass() {
		return super.createISAConcreteClass();
	}

	@Override
	public ISAInterface createISAInterface() {
		return super.createISAInterface();
	}

	@Override
	public ISAEnum createISAEnum() {
		return super.createISAEnum();
	}

}

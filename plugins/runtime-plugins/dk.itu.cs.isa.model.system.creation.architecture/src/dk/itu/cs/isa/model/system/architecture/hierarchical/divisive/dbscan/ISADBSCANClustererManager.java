package dk.itu.cs.isa.model.system.architecture.hierarchical.divisive.dbscan;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import dk.itu.cs.isa.analyzer.exceptions.UserAbortException;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.system.architecture.preferences.ISAPreferencesClustering;
import dk.itu.cs.isa.model.system.architecture.util.ClusterHelper;
import dk.itu.cs.isa.model.system.architecture.util.ISAClusteringCachingUtil;
import dk.itu.cs.isa.model.system.architecture.util.StatisticsLogger;

public class ISADBSCANClustererManager {
	
	private List<ISACluster> initialSingletonClusters;
	private StatisticsLogger stats;
	
	private int minimumClusterSize;
	private int maximumClusterSize;
	
	private List<Double> epsilonValues;
	private List<Integer> numbersOfUnclusteredClassifiers;
	private List<Integer> numbersOfIdentifiedClusters;
	private List<Double> performanceIndexList;
	
	private double bestPerformanceIndex;
	private double bestEpsilon;
	private List<ISACluster> bestClusters;
	private List<ISAClassifier> bestOutliers;
	
	private IProgressMonitor monitor;
	public IProgressMonitor getMonitor() {
		return this.monitor;
	}

	private int epsilonSamplingRate;
	private int availableCores;
	
	

	public ISADBSCANClustererManager(List<ISACluster> initialSingletonClusters, StatisticsLogger stats) {
		minimumClusterSize = ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MINIMUM_CLUSTER_SIZE, 15);
		maximumClusterSize = ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MAXIMUM_CLUSTER_SIZE, 50);
		
		init(initialSingletonClusters, stats);
	}
	
	public ISADBSCANClustererManager(List<ISACluster> initialSingletonClusters, StatisticsLogger stats, int minimumClusterSize, int maximumClusterSize) {
		this.minimumClusterSize = minimumClusterSize;
		this.maximumClusterSize = maximumClusterSize;
		
		init(initialSingletonClusters, stats);
	}
	
	private void init(List<ISACluster> initialSingletonClusters, StatisticsLogger stats) {
		this.initialSingletonClusters = initialSingletonClusters;
		this.stats = stats;
		
		availableCores = Runtime.getRuntime().availableProcessors();
		
		bestEpsilon = 0d;
		bestPerformanceIndex = 0d;
		
		if(minimumClusterSize >= maximumClusterSize)
			throw new IllegalArgumentException("Minimum cluster size ("+ minimumClusterSize +") "
					+ "must NOT be larger than the maximum cluster size("+ maximumClusterSize +")!");
	}



	public ISACluster startClustering(IProgressMonitor monitor) throws Exception {
		this.monitor = monitor;
		
		ISACluster rootCluster = ArchitectureFactory.eINSTANCE.createISACluster();
		
        ISAEventSystem.fireStandardConsoleMessage("Classifiers to cluster: " + initialSingletonClusters.size());
		for(ISACluster initialCluster : initialSingletonClusters)
			rootCluster.getClassifiers().addAll(initialCluster.getClassifiers());
		ClusterHelper.nameCluster(rootCluster);

		for(ISAClassifier classifier : rootCluster.getClassifiers())
			ISAClusteringCachingUtil.INSTANCE.cacheClusterInformation(classifier);
		
		// Do the actual clustering
		epsilonSamplingRate = ISAPreferencesClustering.getInt(ISAPreferencesClustering.EPSILON_SAMPLING_RATE, 64);
		ISAEventSystem.fireStandardConsoleMessage("Epsilon sampling rate: " + epsilonSamplingRate);
		recursivelySplitCluster(rootCluster, -1);
		
		int classifierCount = ClusterHelper.getElementCount(rootCluster);
		ISAEventSystem.fireStandardConsoleMessage("Classifiers in cluster structure: " + classifierCount);
		
		if(classifierCount != initialSingletonClusters.size())
			throw new RuntimeException("The clustering process lost classifiers along the way: Discarding results...");
		return rootCluster;
	}



	private void recursivelySplitCluster(ISACluster cluster, int recursiveStepsAvailable) throws Exception {
		if(epsilonSamplingRate <= 2)
			return;
		if(cluster.getClassifiers().size() <= maximumClusterSize)
			return;
		if(recursiveStepsAvailable == 0)
			return;

		printClusterStatsPre(cluster, recursiveStepsAvailable);
		boolean splitPerformed = performSplit(cluster);
		if(!splitPerformed)
			printIndented(" > Cluster cannot be broken down further.", recursiveStepsAvailable);
		printClusterStatsPost(cluster, recursiveStepsAvailable);
		
		// Try splitting the current cluster again (without running into endless recursion).
		// Will immediately return if the current cluster is small enough.
		if(splitPerformed)
			recursivelySplitCluster(cluster, recursiveStepsAvailable);
		
		// Try splitting child clusters that emerged from the just performed splits.
		for(ISACluster child : cluster.getChildClusters())
			recursivelySplitCluster(child, recursiveStepsAvailable-1);
	}
	
	private void printClusterStatsPre(ISACluster cluster, int indent) {
		printIndented("Cluster (pre): " + cluster.getName() + " - @" + cluster.hashCode(), indent);
		printIndented(" > calculating...", indent);
	}
	
	private void printClusterStatsPost(ISACluster cluster, int indent) {
		printIndented("Cluster (post): " + cluster.getName() + " - @" + cluster.hashCode(), indent);
		for(ISACluster childCluster : cluster.getChildClusters())
			printIndented("  | " + childCluster.getName() + " - @" + childCluster.hashCode(), indent);
		printIndented("", indent);
	}
	
	private void printIndented(String text, int indent) {
		if(indent < 0)
			indent *= -1;
		for(int i=0; i<indent; i++)
			text = "\t" + text;
		
		ISAEventSystem.fireStandardConsoleMessage(text);
	}
	
	

	/**
	 * Spawns a number of threads (depending on the number of available cores) and performs
	 * a DBSCAN step to split down the given cluster.
	 * @return true if the split was successful, false otherwise
	 */
	private boolean performSplit(ISACluster cluster) throws Exception {
		initializeSplit(cluster);

        for(int i=0; i<availableCores; i++) {
        	ISADBSCANClustererRunnable runnable = new ISADBSCANClustererRunnable(this, i, availableCores, epsilonValues, minimumClusterSize, cluster.getClassifiers());
        	(new Thread(null, runnable, "ISA DBSCAN Worker "+(i+1))).start();
        }
		
		while(numbersOfIdentifiedClusters.contains(-1)) {
			// Wait for all threads to return their results...!
			Thread.sleep(20);
			if(monitor.isCanceled())
				throw new UserAbortException();
		}
		
		if(stats != null) {
			stats.logDBSCANPerformanceStats(minimumClusterSize, epsilonValues, numbersOfUnclusteredClassifiers, numbersOfIdentifiedClusters);
			stats.logDBSCANPerformanceIndexStats(minimumClusterSize, epsilonValues, performanceIndexList, bestEpsilon);
		}
		
		if(bestClusters.size() == 0) {
			return false;
		}
		
		// Add new clusters
		cluster.getChildClusters().addAll(bestClusters);
		
		// Add remaining outliers as direct containment of this cluster
		cluster.getClassifiers().clear();
		cluster.getClassifiers().addAll(bestOutliers);
		
		// Set tentative names
		ClusterHelper.nameCluster(cluster);
		for(ISACluster childCluster : cluster.getChildClusters())
			ClusterHelper.nameCluster(childCluster);
		
		return true;
	}

	private void initializeSplit(ISACluster cluster) {
		bestEpsilon = 0d;
		bestPerformanceIndex = 0d;
		bestClusters = new ArrayList<>();
		bestOutliers = new ArrayList<>();
		
		epsilonValues = new ArrayList<>(epsilonSamplingRate);
		for(int i=0; i<epsilonSamplingRate; i++) {
//			epsilonValues.add(round(0.2d + 0.6d * (double) i / (double) accuracy, 1000d));
			epsilonValues.add(round((double) i / (double) epsilonSamplingRate, 1000d));
		}
		
		monitor.beginTask("Analyzing architecture", epsilonValues.size());
		monitor.subTask("Running density based clustering (DBSCAN) to identify clusters: Breaking down a cluster of "
				+ cluster.getClassifiers().size() + " classifiers.");
		
		numbersOfIdentifiedClusters = new ArrayList<>(epsilonValues.size());
		numbersOfUnclusteredClassifiers = new ArrayList<>(epsilonValues.size());
		performanceIndexList = new ArrayList<>(epsilonValues.size());
		for(int i=0; i<epsilonValues.size(); i++) {
			numbersOfIdentifiedClusters.add(-1);
			numbersOfUnclusteredClassifiers.add(-1);
			performanceIndexList.add(-1d);
		}
	}
	
	

	public synchronized void reportBack(Double eps, List<ISACluster> identifiedClusters, List<ISAClassifier> outliers, double ratioOfClusteredClassifiers) {
		int index = epsilonValues.indexOf(eps);
		int numberOfIdentifiedClusters = identifiedClusters.size();
		int numberOfUnclusteredClassifiers = outliers.size(); 
		
		numbersOfIdentifiedClusters.set(index, numberOfIdentifiedClusters);
		numbersOfUnclusteredClassifiers.set(index, numberOfUnclusteredClassifiers);
		
		int smallestClusterSize = Integer.MAX_VALUE;
		double clusterSizeAverage = 0;
		double clusterSizeVariance = 0;
		if(identifiedClusters.size() > 0) {
			for(ISACluster cl : identifiedClusters) {
				int clusterSize = cl.getClassifiers().size();
				if(clusterSize < smallestClusterSize)
					smallestClusterSize = clusterSize;
				clusterSizeAverage += clusterSize;
			}
			clusterSizeAverage /= identifiedClusters.size();
			
			for(ISACluster cl : identifiedClusters) {
			    clusterSizeVariance += Math.pow(cl.getClassifiers().size() - clusterSizeAverage, 2);
			}
			clusterSizeVariance = Math.sqrt(clusterSizeVariance / (double) identifiedClusters.size());
		}
		
		double performanceIndex;
		if(identifiedClusters.size() == 0 || (identifiedClusters.size() == 1 && ratioOfClusteredClassifiers > 0.999d) || numberOfUnclusteredClassifiers < smallestClusterSize) {
			// -> This result is useless and might even cause an endless recursion if assigned best result!
			performanceIndex = 0;
		}
		else {
//			performanceIndex = ratioOfClusteredClassifiers * Math.pow(clusterSizeAverage, 3) * numberOfIdentifiedClusters / clusterSizeVariance;
			performanceIndex = ratioOfClusteredClassifiers * numberOfIdentifiedClusters;
			if(Double.isInfinite(performanceIndex))
				performanceIndex = Double.MAX_VALUE;
		}
		performanceIndexList.set(index, performanceIndex);
		
		if(performanceIndex > bestPerformanceIndex || (performanceIndex == bestPerformanceIndex && eps < bestEpsilon)) {
			bestEpsilon = eps;
			bestClusters = identifiedClusters;
			bestPerformanceIndex = performanceIndex;
			bestOutliers = outliers;
		}
		
		monitor.worked(1);
	}
	
	
	private double round(double d, double precision) {
		return Math.round((d)*precision)/precision;
	}

}


# **Immersive Software Archaeology (ISA) - Eclipse Plugins**

The ISA Eclipse plugins are the analysis and information back-end of Immersive Software Archaeology.  
Their main purpose is:

- The plugins provide an automated analysis of subject Java systems along with a translation into a visualization model that serves as input for [ISA VR application clients](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-vr).
- The plugins encompass functionality for establishing continuous connections with the [ISA VR application clients](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-vr) to update them with live information.



&nbsp;



## **Publications**

- **[FSE'21 Vision Paper](https://pure.itu.dk/ws/portalfiles/portal/86190732/fse21_21_07_07_camera_ready_authors_version.pdf)** - short paper on our vision for the project
- **[VISSOFT'22 Technical Paper](https://pure.itu.dk/ws/portalfiles/portal/92854114/preprint_vissoft22.pdf)** - software exploration via solar system metaphor
- **[SANER'24 Tool Paper](#)** - tool demo paper on the solar system metaphor
- **[ICSME'23 Technical Paper](#)** - freehand sketching in virtual reality
- **[ICPC'24 Technical Paper](#)** - collaborative software exploration with note taking
- **[ICPC'24 Tool Paper](#)** - tool demo paper on collaborative software exploration with note taking



&nbsp;



## **Installation**

There are **three alternative ways** of installing the ISA Eclipse plugins into an Eclipse runtime.  
These are explained beneath.


#### Option A: Installation via a Pre-Configured **Binary Build** (Windows only)

This way of installing the ISA Eclipse plugins is the simplest and fastest.

- Download the ZIP-folder from the [release section](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-eclipse/-/releases) of this repository.
- Extract it
- Run Eclipse (via eclipse.exe)

The provided Eclipse installation (2023-09) contains the ISA plugins as well as a pre-configured reference workspace with ready-to-use example subject software systems.

#### Option B: Installation via the ISA Eclipse **Update Site**

This way of installing the ISA Eclipse plugins is convenient if you would like to install them to another Eclipse version than the one used in the pre-configured ZIP-folder above, regardless of your device's operating system.

The latest tested Eclipse version is **2023-09**, later versions might cause problems.

- Go to [eclipse.org/downloads/packages](https://www.eclipse.org/downloads/packages/), scroll down a little, and install the "**Eclipse Modeling Tools**" packages
	- *it is important to install the correct package, the "Eclipse IDE for Java Developers" will not work!*
- Open up Eclipse, go to "Help" > "Install new Software"
- Insert the following URL in the top input field (further instructions are given on the installation site, just follow the link):

> **<http://itu.dk/~adho/isa-updatesite/>**

- Install the plugins: confirm dialogs and restart eclipse
- Done, you can now use the ISA Eclipse plugins

#### Option C: Installation via **Source Code**

This way of installing the ISA Eclipse plugins is the most elaborate, yet necessary for invasive changes to the plugins beyond changing the settings, or for debugging purposes.

- Go to [eclipse.org/downloads/packages](https://www.eclipse.org/downloads/packages/), scroll down (all the way), and install the "**Eclipse Modeling Tools**" packages
	- *It is important to install this package, the "Eclipse IDE for Java Developers" will not work!*
- Check out this repository
- Import all plugins of this repository into your workspace
- [Set the target platform](https://www.ibm.com/docs/en/cics-ts/5.5?topic=java-setting-up-target-platform) to `target.target` contained in the `dk.itu.cs.isa.eclipse` project.
- [Launch an Eclipse runtime](https://www.ibm.com/docs/en/engineering-lifecycle-management-suite/design-rhapsody/8.2?topic=SSB2MU_8.2.0/com.sodius.mdw.rulescomposer.ui/doc/tutorial/tutorial_java_run_eclipse.htm) from within your original Eclipse runtime, containing all plugins of your workspace
- You can then use the ISA Eclipse plugins from within the launched runtime



&nbsp;



## **Usage**

The purpose of the ISA Eclipse plugins is two-fold:

- For one, they provide an automated analysis for software systems
- For another, they act as backend server for the [ISA VR application](https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-vr)

#### **ISA Perspective**

When using the ISA Eclipse plugins, consider using the ISA perspective.
It layouts the Eclipse user interface to be optimized for performing ISA related tasks.

![Opening the ISA Perspective](readme-resources/switching-perspective.png?raw=true "How to switch to the ISA perspective within Eclipse")

#### **Establishing a Connection to the VR application**

The comminucation between the Eclipse backend and the VR visualization front-end is established by starting the ISA Coalescence Server from within Eclipse.  
Simply click on the green play button to start the server.

![Starting the ISA Coalescence Server](readme-resources/usage-coalescence-server.png?raw=true "How to start the ISA Coalescence Server")

#### **Analyzing Own Systems**

The screenshot beneath shows how to start the automated analysis of own systems.  
This process can take some time, depending on the computation power of your system, especially the number of available CPU cores.
Reference values: A system with ~1.800 classes on a machine with 16 cores takes ~15 minutes when all analysis steps are executed.

![Analyzing own subject systems](readme-resources/usage-analysis.png?raw=true "How to trigger the analysis of own systems")

Specifically, the steps of the analysis & translation process are (in that order):
1. **Design Analysis** - analyzes package and class structure, as well as references between classes.
2. **~~Quality Analysis~~** (*results are not used in the current version*) - analyzes the subject system's quality on different levels, using SonarQube and its scanners. Quality analysis results are integrated with the results from the design analysis.
3. **Architecture Recovery** - extracts the subject system's architecture based on the results from the previous analysis steps. Uses software clustering techniques based on calls and references between classes.
4. **Visualization Model Transformation** - Transforms the results from all 3 above phases into a visualization model which is transmitted to the VR application to be rendered in 3D.

**Troubleshooting**: Should the analysis be unreasonably slow (and accompanied with not unresponsive UI?), a likely reason is that Eclipse does not have sufficient memory available.
One way to fix this is to edit the `eclipse.ini` file in your installation folder and increase the maximum allowed memory for the JVM (there is a respective entry in the list of JVM arguments).
After modification, that passage in `eclipse.ini` would look something like this (note the middle row):
````
...
-Xms256m
-Xmx8g
-XX:+UseG1GC
...
````



<!---
#### Installing SonarQube and Sonar Scanner

> In case you want to analyze the quality of a system, you need to install SonarQube and the Sonar Scanner:
>
> - Download [SonarQube](https://sonarqube.org/downloads/) (Community Edition)
> - Download [Sonar Scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/) from one of the links listed in the upper area of the page
> - Unpack the downloaded compressed folders
> - Open the ISA Eclipse plugin preference page (in Eclipse, go to Window > Preferences > ISA > Quality Analysis - SonarQube)
> 	- Set the path the the SonarQube and Sonar Scanner executables
> 	- Save your login credentials for the SonarQube backend (don't forget to update them here if you change them in SonarQube, otherwise the analysis will fail)
>
> *Note: if you unpack the downloaded compressed folders into the same folder as your ISA Eclipse workspace, the ISA plugins will detect the SonarQube and Sonar Scanner installations automatically*
-->

#### Configuring Suitable Java Environments

When analyzing legacy systems, it is important to use a compatible Java level.
For many projects, this is Java 1.8.

One way to do this is by configuring the project to use a new Java runtime in compatibility mode: right-click in the Project Explorer > "Properties". Then go to "Java Build Path" > "Libraries" and edit the JRE System Library to a suitable level. Also, check out "Java Compiler" and tick the box for "Use compliance from [..] Java Build Path".

If this is not sufficient and you still run into problems, you can always fall back to simply using an old Java runtime (instead of using a new one in compatibility mode as described above).
The screenshot below shows how to manually change the Java runtime.

![How to change the used JVM installation in Eclipse](readme-resources/configuring-jvm.png?raw=true "How to change the used JVM installation in Eclipse")



&nbsp;



## **Contact**

In case you have questions, please don't hesitate to contact me: [adho@itu.dk](adho@itu.dk).



&nbsp;

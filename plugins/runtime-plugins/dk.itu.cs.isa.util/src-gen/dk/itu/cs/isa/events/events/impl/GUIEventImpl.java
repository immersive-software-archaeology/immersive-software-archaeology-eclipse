/**
 */
package dk.itu.cs.isa.events.events.impl;

import dk.itu.cs.isa.events.events.EventsPackage;
import dk.itu.cs.isa.events.events.GUIEvent;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GUI Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.impl.GUIEventImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link dk.itu.cs.isa.events.events.impl.GUIEventImpl#getButtonLabels <em>Button Labels</em>}</li>
 *   <li>{@link dk.itu.cs.isa.events.events.impl.GUIEventImpl#getReturnCode <em>Return Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class GUIEventImpl extends EventImpl implements GUIEvent {
	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getButtonLabels() <em>Button Labels</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getButtonLabels()
	 * @generated
	 * @ordered
	 */
	protected EList<String> buttonLabels;

	/**
	 * The default value of the '{@link #getReturnCode() <em>Return Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnCode()
	 * @generated
	 * @ordered
	 */
	protected static final int RETURN_CODE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getReturnCode() <em>Return Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnCode()
	 * @generated
	 * @ordered
	 */
	protected int returnCode = RETURN_CODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GUIEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EventsPackage.Literals.GUI_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.GUI_EVENT__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getButtonLabels() {
		if (buttonLabels == null) {
			buttonLabels = new EDataTypeUniqueEList<String>(String.class, this, EventsPackage.GUI_EVENT__BUTTON_LABELS);
		}
		return buttonLabels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getReturnCode() {
		return returnCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReturnCode(int newReturnCode) {
		int oldReturnCode = returnCode;
		returnCode = newReturnCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EventsPackage.GUI_EVENT__RETURN_CODE, oldReturnCode, returnCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EventsPackage.GUI_EVENT__TITLE:
				return getTitle();
			case EventsPackage.GUI_EVENT__BUTTON_LABELS:
				return getButtonLabels();
			case EventsPackage.GUI_EVENT__RETURN_CODE:
				return getReturnCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EventsPackage.GUI_EVENT__TITLE:
				setTitle((String)newValue);
				return;
			case EventsPackage.GUI_EVENT__BUTTON_LABELS:
				getButtonLabels().clear();
				getButtonLabels().addAll((Collection<? extends String>)newValue);
				return;
			case EventsPackage.GUI_EVENT__RETURN_CODE:
				setReturnCode((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EventsPackage.GUI_EVENT__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
			case EventsPackage.GUI_EVENT__BUTTON_LABELS:
				getButtonLabels().clear();
				return;
			case EventsPackage.GUI_EVENT__RETURN_CODE:
				setReturnCode(RETURN_CODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EventsPackage.GUI_EVENT__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
			case EventsPackage.GUI_EVENT__BUTTON_LABELS:
				return buttonLabels != null && !buttonLabels.isEmpty();
			case EventsPackage.GUI_EVENT__RETURN_CODE:
				return returnCode != RETURN_CODE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (title: ");
		result.append(title);
		result.append(", buttonLabels: ");
		result.append(buttonLabels);
		result.append(", returnCode: ");
		result.append(returnCode);
		result.append(')');
		return result.toString();
	}

} //GUIEventImpl

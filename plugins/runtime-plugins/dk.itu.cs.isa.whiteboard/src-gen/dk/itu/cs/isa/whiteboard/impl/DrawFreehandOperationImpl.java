/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.DrawFreehandOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Draw Freehand Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DrawFreehandOperationImpl extends AbstractDrawOperationImpl implements DrawFreehandOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DrawFreehandOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.DRAW_FREEHAND_OPERATION;
	}

} //DrawFreehandOperationImpl

package dk.itu.cs.isa.eclipse.ui.dialogs;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.widgets.WidgetFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PreferencesUtil;

import dk.itu.cs.isa.analyzer.SystemAnalysisExtensionHandler;
import dk.itu.cs.isa.analyzer.exceptions.ISAException;
import dk.itu.cs.isa.eclipse.ui.ImageHelper;
import dk.itu.cs.isa.preferences.ISAPreferences;

public class ISAAnalysisSetupDialog extends Dialog {

	private static final GridData TWO_COLUMNS_LAYOUT_DATA = new GridData(SWT.LEFT, SWT.TOP, false, true, 2, 1);
	
	List<Image> registeredImages = new LinkedList<>();
	
	private Combo structureCombo;
	private Combo qualityCombo;
	private Combo sarCombo;
	private Combo visualizationCombo;

	private Label qualityExplanation;
	private Label sarExplanation;
	
	private Button performQualityAnalysisButton;
	private Button performSARButton;

	private Text systemNameText;
	private String systemName;

	private Button okButton;
	private Button settingsButton;


	
	public ISAAnalysisSetupDialog(Shell parentShell, String systemName) {
		super(parentShell);
		this.systemName = systemName;
		TWO_COLUMNS_LAYOUT_DATA.widthHint = 700;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Analysis Setup");
		Image iconImage = ImageHelper.getImage("icons/icon-16-analysis.png");
		registeredImages.add(iconImage);
		shell.setImage(iconImage);
	}

	@Override
	public boolean close() {
		for(Image image : registeredImages)
			image.dispose();
		return super.close();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		((GridLayout) composite.getLayout()).makeColumnsEqualWidth = false;
		((GridLayout) composite.getLayout()).numColumns = 2;
		
		createTextLabel(composite,
				"Specify the settings for the software analysis and translation to virtual reality world.",
				50, 0, false);
		
		createSpace(composite);

		createTextLabel(composite, "System Name", 0, 0, false);
		systemNameText = new Text(composite, getInputTextStyle());
		systemNameText.setLayoutData(TWO_COLUMNS_LAYOUT_DATA);
		systemNameText.addModifyListener(e -> validateInput());
		createTextLabel(composite,
				"The name will be used internally (to store generated models) and "
				+ "for you to chose from the list of displayable models in VR.",
				50, 10, true);
		
		createSpace(composite);
		
		createImageAndTextLabel(composite, "Static Structure Analysis", ImageHelper.getImage("icons/icon-16-settings-structure.png"));
		structureCombo = new Combo(composite, getInputTextStyle() | SWT.DROP_DOWN | SWT.READ_ONLY);
		structureCombo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
		createTextLabel(composite,
				"Analysis of a selection of Eclipse projects and creation of a structure model for further processing by the subsequent analyzers (beneath).",
				50, 10, true);
		
		createSpace(composite);

		Image qualityIcon = ImageHelper.getImage("icons/icon-16-settings-quality.png");
		registeredImages.add(qualityIcon);
		performQualityAnalysisButton = WidgetFactory.button(SWT.CHECK).text("Quality Analysis")
				.font(JFaceResources.getDialogFont())
				.data(IDialogConstants.CLIENT_ID+2)
				.image(qualityIcon)
				.layoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1))
				.onSelect(e -> validateInput())
				.create(composite);
		performQualityAnalysisButton.setSelection(ISAPreferences.getBoolean(ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_EXECUTE, true));
		qualityCombo = new Combo(composite, getInputTextStyle() | SWT.DROP_DOWN | SWT.READ_ONLY);
		qualityExplanation = createTextLabel(composite,
				"Optional source code quality analysis. If this step is skipped, advanced quality information will not be available in VR.",
				50, 10, true);
		
		createSpace(composite);
		
		Image sarIcon = ImageHelper.getImage("icons/icon-16-settings-architecture.png");
		registeredImages.add(sarIcon);
		performSARButton = WidgetFactory.button(SWT.CHECK).text("Architecture Recovery")
				.font(JFaceResources.getDialogFont())
				.data(IDialogConstants.CLIENT_ID+2)
				.image(sarIcon)
				.layoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1))
				.onSelect(e -> validateInput())
				.create(composite);
		performSARButton.setSelection(ISAPreferences.getBoolean(ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_EXECUTE, true));
		sarCombo = new Combo(composite, getInputTextStyle() | SWT.DROP_DOWN | SWT.READ_ONLY);
		sarExplanation = createTextLabel(composite,
				"Software architecture recovery analysis of the previous yielded structural analysis results.",
				50, 10, true);
		
		createSpace(composite);

		createImageAndTextLabel(composite, "Visualization Model Creation", ImageHelper.getImage("icons/icon-16-settings-visualization.png"));
		visualizationCombo = new Combo(composite, getInputTextStyle() | SWT.DROP_DOWN | SWT.READ_ONLY);
		createTextLabel(composite,
				"Translation of structure, quality, and architecture of the system to the visual metaphor, ready to be processed by the VR application.",
				50, 10, true);
		
		try {
			setupCombo(structureCombo, composite,
					SystemAnalysisExtensionHandler.INSTANCE.getAllSystemModelCreatorsFromExtensions(),
					SystemAnalysisExtensionHandler.INSTANCE.getDefaultSystemModelCreatorFromExtensions());
			setupCombo(qualityCombo, composite,
					SystemAnalysisExtensionHandler.INSTANCE.getAllSystemModelQualityAnalyzersFromExtensions(),
					SystemAnalysisExtensionHandler.INSTANCE.getDefaultSystemModelQualityAnalyzerFromExtensions());
			setupCombo(sarCombo, composite,
					SystemAnalysisExtensionHandler.INSTANCE.getAllSystemModelArchitectureAnalyzersFromExtensions(),
					SystemAnalysisExtensionHandler.INSTANCE.getDefaultArchitectureAnalyzerFromExtensions());
			setupCombo(visualizationCombo, composite,
					SystemAnalysisExtensionHandler.INSTANCE.getAllVisualizationModelCreatorsFromExtensions(),
					SystemAnalysisExtensionHandler.INSTANCE.getDefaultVisualizationModelCreatorFromExtensions());
			
			applyDialogFont(composite);
		} catch (ISAException e1) {
			e1.printStackTrace();
		}
		
		createSpace(composite);

//		GridData settingsButtonLayout = new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1);
//		settingsButtonLayout.widthHint = 300;
//		settingsButtonLayout.heightHint = 50;
//		Image iconImage = ImageHelper.getImage("icons/icon-16-settings.png");
//		registeredImages.add(iconImage);
//		WidgetFactory.button(SWT.PUSH).text("Open Detail Settings")
//				.font(JFaceResources.getDialogFont())
//				.data(IDialogConstants.CLIENT_ID+1)
//				.image(iconImage)
//				.layoutData(settingsButtonLayout)
//				.onSelect(event -> buttonPressed(((Integer) event.widget.getData()).intValue()))
//				.create(composite);
		
		return composite;
	}

	private void createSpace(Composite composite) {
		GridData layout = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		layout.widthHint = TWO_COLUMNS_LAYOUT_DATA.widthHint;
		layout.heightHint = 10;
		new Label(composite, SWT.NONE).setLayoutData(layout);
	}
	
	private Label createTextLabel(Composite composite, String text, int height, int indent, boolean italic) {
		if(text == null)
			return null;

		Label textLabel = new Label(composite, SWT.WRAP | SWT.LEFT);
		textLabel.setText(text);
		if(italic)
			textLabel.setFont(JFaceResources.getFontRegistry().getItalic(JFaceResources.DEFAULT_FONT));
		
		GridData layout = new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1);
		layout.widthHint = TWO_COLUMNS_LAYOUT_DATA.widthHint;
//		layout.minimumHeight = height;
//		layout.horizontalIndent = indent;
		textLabel.setLayoutData(layout);
		
		return textLabel;
	}
	
	private void createImageAndTextLabel(Composite composite, String text, Image image) {
		registeredImages.add(image);
		
		Label imageLabel = new Label(composite, SWT.RIGHT);
		imageLabel.setImage(image);
		imageLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
//		image.dispose();
		
		Label textLabel = new Label(composite, SWT.LEFT);
		textLabel.setText(text);
		textLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
	}
	
	
	
	private <T> void setupCombo(Combo combo, Composite composite, Map<String, T> extensionMap, T defaultExtension) {
		combo.setLayoutData(TWO_COLUMNS_LAYOUT_DATA);
		combo.addModifyListener(e -> validateInput());
		
		for(String option : extensionMap.keySet()) {
			combo.add(option);
		}

		if (defaultExtension != null) {
			for (String extensionName : extensionMap.keySet()) {
				T extension = extensionMap.get(extensionName);
				if (extension.equals(defaultExtension)) {
					combo.select(combo.indexOf(extensionName));
					break;
				}
			}
		}
	}

	protected void validateInput() {
		Control button = getButton(IDialogConstants.OK_ID);
		if (button != null) {
			boolean isOkay = true;
			if(systemNameText.getText().isBlank())
				isOkay = false;
			else if(structureCombo.getSelectionIndex() == -1)
				isOkay = false;
			else if(qualityCombo.getSelectionIndex() == -1)
				isOkay = false;
			else if(sarCombo.getSelectionIndex() == -1)
				isOkay = false;
			else if(visualizationCombo.getSelectionIndex() == -1)
				isOkay = false;
			
			button.setEnabled(isOkay);
		}

		qualityCombo.setEnabled(performQualityAnalysisButton.getSelection());
		qualityExplanation.setEnabled(performQualityAnalysisButton.getSelection());
		
		sarCombo.setEnabled(performSARButton.getSelection());
		sarExplanation.setEnabled(performSARButton.getSelection());
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		settingsButton = createButton(parent, IDialogConstants.DETAILS_ID, "Settings", false);
		Image settingsImage = ImageHelper.getImage("icons/icon-16-settings.png");
		registeredImages.add(settingsImage);
		settingsButton.setImage(settingsImage);
		
		okButton = createButton(parent, IDialogConstants.OK_ID, "Start", true);
		Image startImage = ImageHelper.getImage("icons/icon-16-server-start.png");
		registeredImages.add(startImage);
		okButton.setImage(startImage);
		
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
		
		// do this here because setting the text will set enablement on the ok button
		systemNameText.setFocus();
		if (systemName != null) {
			systemNameText.setText(systemName);
			systemNameText.selectAll();
		}
		
		validateInput();
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID || buttonId == IDialogConstants.CLIENT_ID) {
			systemName = systemNameText.getText();
			
			ISAPreferences.setString(ISAPreferences.SYSTEM_MODEL_CREATOR_DEFAULT, structureCombo.getItem(structureCombo.getSelectionIndex()));
			ISAPreferences.setString(ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_DEFAULT, qualityCombo.getItem(qualityCombo.getSelectionIndex()));
			ISAPreferences.setString(ISAPreferences.SYSTEM_MODEL_SAR_DEFAULT, sarCombo.getItem(sarCombo.getSelectionIndex()));
			ISAPreferences.setString(ISAPreferences.VISUALIZATION_MODEL_CREATOR_DEFAULT, visualizationCombo.getItem(visualizationCombo.getSelectionIndex()));

			ISAPreferences.setBoolean(ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_EXECUTE, performQualityAnalysisButton.getSelection());
			ISAPreferences.setBoolean(ISAPreferences.SYSTEM_MODEL_SAR_EXECUTE, performSARButton.getSelection());
		}
		
		if(buttonId == IDialogConstants.DETAILS_ID) {
			PreferencesUtil.createPreferenceDialogOn(this.getShell(),
					"dk.itu.cs.isa.preferences.page",
					null, // new String[] { "dk.itu.cs.isa.preferences.page" },
					systemName).open();
		}
		
		super.buttonPressed(buttonId);
	}

	/**
	 * Returns the ok button.
	 *
	 * @return the ok button
	 */
	protected Button getOkButton() {
		return okButton;
	}

	public String getSystemName() {
		return this.systemName;
	}

	/**
	 * Returns the style bits that should be used for the input text field.
	 * Defaults to a single line entry. Subclasses may override.
	 *
	 * @return the integer style bits that should be used when creating the
	 *         input text
	 *
	 * @since 3.4
	 */
	protected int getInputTextStyle() {
		return SWT.SINGLE | SWT.BORDER;
	}

}

/**
 */
package dk.itu.cs.isa.model.software.structure.architecture;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory
 * @model kind="package"
 * @generated
 */
public interface ArchitecturePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "architecture";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/software/structure/architecture";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "architecture";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArchitecturePackage eINSTANCE = dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.architecture.impl.ISAClusterImpl <em>ISA Cluster</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.architecture.impl.ISAClusterImpl
	 * @see dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl#getISACluster()
	 * @generated
	 */
	int ISA_CLUSTER = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLUSTER__NAME = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Child Clusters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLUSTER__CHILD_CLUSTERS = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLUSTER__CLASSIFIERS = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Top Word Occurrences</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLUSTER__TOP_WORD_OCCURRENCES = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ISA Cluster</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLUSTER_FEATURE_COUNT = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLUSTER___GET_FULLY_QUALIFIED_NAME = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLUSTER___TO_STRING = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Cluster</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLUSTER_OPERATION_COUNT = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.architecture.impl.ISAWordOccurrenceEntryImpl <em>ISA Word Occurrence Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.architecture.impl.ISAWordOccurrenceEntryImpl
	 * @see dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl#getISAWordOccurrenceEntry()
	 * @generated
	 */
	int ISA_WORD_OCCURRENCE_ENTRY = 1;

	/**
	 * The feature id for the '<em><b>Word</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_WORD_OCCURRENCE_ENTRY__WORD = 0;

	/**
	 * The feature id for the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_WORD_OCCURRENCE_ENTRY__COUNT = 1;

	/**
	 * The number of structural features of the '<em>ISA Word Occurrence Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_WORD_OCCURRENCE_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>ISA Word Occurrence Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_WORD_OCCURRENCE_ENTRY_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.architecture.ISACluster <em>ISA Cluster</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Cluster</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ISACluster
	 * @generated
	 */
	EClass getISACluster();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.architecture.ISACluster#getChildClusters <em>Child Clusters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Child Clusters</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ISACluster#getChildClusters()
	 * @see #getISACluster()
	 * @generated
	 */
	EReference getISACluster_ChildClusters();

	/**
	 * Returns the meta object for the reference list '{@link dk.itu.cs.isa.model.software.structure.architecture.ISACluster#getClassifiers <em>Classifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Classifiers</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ISACluster#getClassifiers()
	 * @see #getISACluster()
	 * @generated
	 */
	EReference getISACluster_Classifiers();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.architecture.ISACluster#getTopWordOccurrences <em>Top Word Occurrences</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Top Word Occurrences</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ISACluster#getTopWordOccurrences()
	 * @see #getISACluster()
	 * @generated
	 */
	EReference getISACluster_TopWordOccurrences();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry <em>ISA Word Occurrence Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Word Occurrence Entry</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry
	 * @generated
	 */
	EClass getISAWordOccurrenceEntry();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry#getWord <em>Word</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Word</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry#getWord()
	 * @see #getISAWordOccurrenceEntry()
	 * @generated
	 */
	EAttribute getISAWordOccurrenceEntry_Word();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry#getCount <em>Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry#getCount()
	 * @see #getISAWordOccurrenceEntry()
	 * @generated
	 */
	EAttribute getISAWordOccurrenceEntry_Count();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ArchitectureFactory getArchitectureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.architecture.impl.ISAClusterImpl <em>ISA Cluster</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.architecture.impl.ISAClusterImpl
		 * @see dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl#getISACluster()
		 * @generated
		 */
		EClass ISA_CLUSTER = eINSTANCE.getISACluster();

		/**
		 * The meta object literal for the '<em><b>Child Clusters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLUSTER__CHILD_CLUSTERS = eINSTANCE.getISACluster_ChildClusters();

		/**
		 * The meta object literal for the '<em><b>Classifiers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLUSTER__CLASSIFIERS = eINSTANCE.getISACluster_Classifiers();

		/**
		 * The meta object literal for the '<em><b>Top Word Occurrences</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLUSTER__TOP_WORD_OCCURRENCES = eINSTANCE.getISACluster_TopWordOccurrences();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.architecture.impl.ISAWordOccurrenceEntryImpl <em>ISA Word Occurrence Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.architecture.impl.ISAWordOccurrenceEntryImpl
		 * @see dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl#getISAWordOccurrenceEntry()
		 * @generated
		 */
		EClass ISA_WORD_OCCURRENCE_ENTRY = eINSTANCE.getISAWordOccurrenceEntry();

		/**
		 * The meta object literal for the '<em><b>Word</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_WORD_OCCURRENCE_ENTRY__WORD = eINSTANCE.getISAWordOccurrenceEntry_Word();

		/**
		 * The meta object literal for the '<em><b>Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_WORD_OCCURRENCE_ENTRY__COUNT = eINSTANCE.getISAWordOccurrenceEntry_Count();

	}

} //ArchitecturePackage

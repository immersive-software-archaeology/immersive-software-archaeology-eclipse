/**
 */
package dk.itu.cs.isa.model.software.structure.libraries.impl;

import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage;

import dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;

import dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;

import dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl;

import dk.itu.cs.isa.model.software.structure.impl.StructurePackageImpl;

import dk.itu.cs.isa.model.software.structure.libraries.ISALibrary;
import dk.itu.cs.isa.model.software.structure.libraries.ISALibraryClassifier;
import dk.itu.cs.isa.model.software.structure.libraries.ISAPrimitiveType;
import dk.itu.cs.isa.model.software.structure.libraries.LibrariesFactory;
import dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage;

import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LibrariesPackageImpl extends EPackageImpl implements LibrariesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaLibraryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaPrimitiveTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaLibraryClassifierEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private LibrariesPackageImpl() {
		super(eNS_URI, LibrariesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link LibrariesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static LibrariesPackage init() {
		if (isInited) return (LibrariesPackage)EPackage.Registry.INSTANCE.getEPackage(LibrariesPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredLibrariesPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		LibrariesPackageImpl theLibrariesPackage = registeredLibrariesPackage instanceof LibrariesPackageImpl ? (LibrariesPackageImpl)registeredLibrariesPackage : new LibrariesPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);
		StructurePackageImpl theStructurePackage = (StructurePackageImpl)(registeredPackage instanceof StructurePackageImpl ? registeredPackage : StructurePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);
		ArchitecturePackageImpl theArchitecturePackage = (ArchitecturePackageImpl)(registeredPackage instanceof ArchitecturePackageImpl ? registeredPackage : ArchitecturePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FilesystemPackage.eNS_URI);
		FilesystemPackageImpl theFilesystemPackage = (FilesystemPackageImpl)(registeredPackage instanceof FilesystemPackageImpl ? registeredPackage : FilesystemPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl)(registeredPackage instanceof ClassifiersPackageImpl ? registeredPackage : ClassifiersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);
		MembersPackageImpl theMembersPackage = (MembersPackageImpl)(registeredPackage instanceof MembersPackageImpl ? registeredPackage : MembersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);
		MiscPackageImpl theMiscPackage = (MiscPackageImpl)(registeredPackage instanceof MiscPackageImpl ? registeredPackage : MiscPackage.eINSTANCE);

		// Create package meta-data objects
		theLibrariesPackage.createPackageContents();
		theStructurePackage.createPackageContents();
		theArchitecturePackage.createPackageContents();
		theFilesystemPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theMembersPackage.createPackageContents();
		theMiscPackage.createPackageContents();

		// Initialize created meta-data
		theLibrariesPackage.initializePackageContents();
		theStructurePackage.initializePackageContents();
		theArchitecturePackage.initializePackageContents();
		theFilesystemPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theMembersPackage.initializePackageContents();
		theMiscPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theLibrariesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(LibrariesPackage.eNS_URI, theLibrariesPackage);
		return theLibrariesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISALibrary() {
		return isaLibraryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISALibrary_Name() {
		return (EAttribute)isaLibraryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISALibrary_PrimitiveTypes() {
		return (EReference)isaLibraryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISALibrary__GetElement__String() {
		return isaLibraryEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAPrimitiveType() {
		return isaPrimitiveTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISALibraryClassifier() {
		return isaLibraryClassifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LibrariesFactory getLibrariesFactory() {
		return (LibrariesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaLibraryEClass = createEClass(ISA_LIBRARY);
		createEAttribute(isaLibraryEClass, ISA_LIBRARY__NAME);
		createEReference(isaLibraryEClass, ISA_LIBRARY__PRIMITIVE_TYPES);
		createEOperation(isaLibraryEClass, ISA_LIBRARY___GET_ELEMENT__STRING);

		isaPrimitiveTypeEClass = createEClass(ISA_PRIMITIVE_TYPE);

		isaLibraryClassifierEClass = createEClass(ISA_LIBRARY_CLASSIFIER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage)EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);
		MiscPackage theMiscPackage = (MiscPackage)EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		isaLibraryEClass.getESuperTypes().add(theClassifiersPackage.getISAClassifierContainer());
		isaPrimitiveTypeEClass.getESuperTypes().add(theMiscPackage.getISAType());
		isaLibraryClassifierEClass.getESuperTypes().add(theClassifiersPackage.getISAClass());
		isaLibraryClassifierEClass.getESuperTypes().add(theClassifiersPackage.getISAInterface());
		isaLibraryClassifierEClass.getESuperTypes().add(theClassifiersPackage.getISAEnum());
		isaLibraryClassifierEClass.getESuperTypes().add(theClassifiersPackage.getISARecord());

		// Initialize classes, features, and operations; add parameters
		initEClass(isaLibraryEClass, ISALibrary.class, "ISALibrary", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISALibrary_Name(), ecorePackage.getEString(), "name", null, 1, 1, ISALibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISALibrary_PrimitiveTypes(), this.getISAPrimitiveType(), null, "primitiveTypes", null, 0, -1, ISALibrary.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getISALibrary__GetElement__String(), theMiscPackage.getISANamedSoftwareElement(), "getElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "qualifiedElementName", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(isaPrimitiveTypeEClass, ISAPrimitiveType.class, "ISAPrimitiveType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isaLibraryClassifierEClass, ISALibraryClassifier.class, "ISALibraryClassifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //LibrariesPackageImpl

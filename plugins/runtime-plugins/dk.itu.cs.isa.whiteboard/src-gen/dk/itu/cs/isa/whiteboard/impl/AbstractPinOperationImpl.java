/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.AbstractPinOperation;
import dk.itu.cs.isa.whiteboard.ISAColor;
import dk.itu.cs.isa.whiteboard.ISAVector2;
import dk.itu.cs.isa.whiteboard.ISAVector3;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Pin Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPinOperationImpl#getPinId <em>Pin Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPinOperationImpl#getPinnedElementName <em>Pinned Element Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPinOperationImpl#getTextureSpacePosition <em>Texture Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPinOperationImpl#getLocalSpacePosition <em>Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPinOperationImpl#getColor <em>Color</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractPinOperationImpl extends AbstractOperationImpl implements AbstractPinOperation {
	/**
	 * The default value of the '{@link #getPinId() <em>Pin Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinId()
	 * @generated
	 * @ordered
	 */
	protected static final int PIN_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPinId() <em>Pin Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinId()
	 * @generated
	 * @ordered
	 */
	protected int pinId = PIN_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getPinnedElementName() <em>Pinned Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinnedElementName()
	 * @generated
	 * @ordered
	 */
	protected static final String PINNED_ELEMENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPinnedElementName() <em>Pinned Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinnedElementName()
	 * @generated
	 * @ordered
	 */
	protected String pinnedElementName = PINNED_ELEMENT_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTextureSpacePosition() <em>Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTextureSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector2 textureSpacePosition;

	/**
	 * The cached value of the '{@link #getLocalSpacePosition() <em>Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 localSpacePosition;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected ISAColor color;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractPinOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.ABSTRACT_PIN_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPinId() {
		return pinId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPinId(int newPinId) {
		int oldPinId = pinId;
		pinId = newPinId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PIN_OPERATION__PIN_ID, oldPinId, pinId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPinnedElementName() {
		return pinnedElementName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPinnedElementName(String newPinnedElementName) {
		String oldPinnedElementName = pinnedElementName;
		pinnedElementName = newPinnedElementName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME, oldPinnedElementName, pinnedElementName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector2 getTextureSpacePosition() {
		return textureSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTextureSpacePosition(ISAVector2 newTextureSpacePosition, NotificationChain msgs) {
		ISAVector2 oldTextureSpacePosition = textureSpacePosition;
		textureSpacePosition = newTextureSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION, oldTextureSpacePosition, newTextureSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTextureSpacePosition(ISAVector2 newTextureSpacePosition) {
		if (newTextureSpacePosition != textureSpacePosition) {
			NotificationChain msgs = null;
			if (textureSpacePosition != null)
				msgs = ((InternalEObject)textureSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION, null, msgs);
			if (newTextureSpacePosition != null)
				msgs = ((InternalEObject)newTextureSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION, null, msgs);
			msgs = basicSetTextureSpacePosition(newTextureSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION, newTextureSpacePosition, newTextureSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getLocalSpacePosition() {
		return localSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocalSpacePosition(ISAVector3 newLocalSpacePosition, NotificationChain msgs) {
		ISAVector3 oldLocalSpacePosition = localSpacePosition;
		localSpacePosition = newLocalSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION, oldLocalSpacePosition, newLocalSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalSpacePosition(ISAVector3 newLocalSpacePosition) {
		if (newLocalSpacePosition != localSpacePosition) {
			NotificationChain msgs = null;
			if (localSpacePosition != null)
				msgs = ((InternalEObject)localSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION, null, msgs);
			if (newLocalSpacePosition != null)
				msgs = ((InternalEObject)newLocalSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION, null, msgs);
			msgs = basicSetLocalSpacePosition(newLocalSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION, newLocalSpacePosition, newLocalSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAColor getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetColor(ISAColor newColor, NotificationChain msgs) {
		ISAColor oldColor = color;
		color = newColor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PIN_OPERATION__COLOR, oldColor, newColor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(ISAColor newColor) {
		if (newColor != color) {
			NotificationChain msgs = null;
			if (color != null)
				msgs = ((InternalEObject)color).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PIN_OPERATION__COLOR, null, msgs);
			if (newColor != null)
				msgs = ((InternalEObject)newColor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PIN_OPERATION__COLOR, null, msgs);
			msgs = basicSetColor(newColor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PIN_OPERATION__COLOR, newColor, newColor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION:
				return basicSetTextureSpacePosition(null, msgs);
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION:
				return basicSetLocalSpacePosition(null, msgs);
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__COLOR:
				return basicSetColor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__PIN_ID:
				return getPinId();
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME:
				return getPinnedElementName();
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION:
				return getTextureSpacePosition();
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION:
				return getLocalSpacePosition();
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__COLOR:
				return getColor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__PIN_ID:
				setPinId((Integer)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME:
				setPinnedElementName((String)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION:
				setTextureSpacePosition((ISAVector2)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION:
				setLocalSpacePosition((ISAVector3)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__COLOR:
				setColor((ISAColor)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__PIN_ID:
				setPinId(PIN_ID_EDEFAULT);
				return;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME:
				setPinnedElementName(PINNED_ELEMENT_NAME_EDEFAULT);
				return;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION:
				setTextureSpacePosition((ISAVector2)null);
				return;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION:
				setLocalSpacePosition((ISAVector3)null);
				return;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__COLOR:
				setColor((ISAColor)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__PIN_ID:
				return pinId != PIN_ID_EDEFAULT;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME:
				return PINNED_ELEMENT_NAME_EDEFAULT == null ? pinnedElementName != null : !PINNED_ELEMENT_NAME_EDEFAULT.equals(pinnedElementName);
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION:
				return textureSpacePosition != null;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION:
				return localSpacePosition != null;
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION__COLOR:
				return color != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (pinId: ");
		result.append(pinId);
		result.append(", pinnedElementName: ");
		result.append(pinnedElementName);
		result.append(')');
		return result.toString();
	}

} //AbstractPinOperationImpl

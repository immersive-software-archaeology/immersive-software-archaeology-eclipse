/**
 */
package dk.itu.cs.isa.events.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getInputEvent()
 * @model abstract="true"
 * @generated
 */
public interface InputEvent extends GUIEvent {
} // InputEvent

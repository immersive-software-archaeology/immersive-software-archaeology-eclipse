/**
 */
package dk.itu.cs.isa.storage;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.storage.StoragePackage
 * @generated
 */
public interface StorageFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StorageFactory eINSTANCE = dk.itu.cs.isa.storage.impl.StorageFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model Storage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Storage</em>'.
	 * @generated
	 */
	ModelStorage createModelStorage();

	/**
	 * Returns a new object of class '<em>Model Storage Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Storage Entry</em>'.
	 * @generated
	 */
	ModelStorageEntry createModelStorageEntry();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StoragePackage getStoragePackage();

} //StorageFactory

package dk.itu.cs.isa.whiteboard.requests;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent;
import dk.itu.cs.isa.whiteboard.Whiteboard;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardStorage;

public class WhiteboardSynchronizationUpdateBitmapRequestHandler extends ISAAbstractCoalescenceRequestHandler {

	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		
		String whiteboardIdString = baseRequest.getParameter("whiteboardId");
		int whiteboardId;
		try {
			whiteboardId = Integer.parseInt(whiteboardIdString);
		} catch(NumberFormatException e) {
			whiteboardId = -1;
		}
		
		String operationTimestampString = baseRequest.getParameter("operationTimestamp");
		long operationUpdateTimestamp;
		try {
			operationUpdateTimestamp = Long.parseLong(operationTimestampString);
		} catch(NumberFormatException e) {
			operationUpdateTimestamp = -1;
		}
		
		if(systemName == null || operationUpdateTimestamp <= 0 || whiteboardId <= 0) {
			String examplePath1 = "systemName=JetUML&operationTimestamp=" + System.currentTimeMillis() + "&whiteboardId=" + 3;
			String explanation = "You need to specify the server-side timestamp for the operation you are updating the whiteboard bitmap for.";
			printInvalidParametersHTMLPage(baseRequest, response, explanation, examplePath1);
			return;
		}

		boolean requestArrivedInTime = false;
		WhiteboardHistory historyForSystem = WhiteboardStorage.INSTANCE.getWhiteboardHistory(systemName);
		for(AbstractWhiteboardEvent event : historyForSystem.getEventLog()) {
			if(event.getTimestamp() <= operationUpdateTimestamp && event.isConfirmationRequired()) {
				event.setConfirmationRequired(false);
				requestArrivedInTime = true;
			}
		}
		
		try {
			if(!requestArrivedInTime)
				throw new Exception("Update request is outdated and will be ignored.");
			
			Whiteboard whiteboard = WhiteboardStorage.INSTANCE.getWhiteboardByID(systemName, whiteboardId);
			if(whiteboard == null)
				throw new Exception("Whiteboard with id=" + whiteboardId + " cannot be found.");
			
			String requestBodyBitmapBase64 = "";
			while(true) {
				String line = baseRequest.getReader().readLine();
				if(line == null)
					break;
				requestBodyBitmapBase64 += line + System.lineSeparator();
			}
			
			whiteboard.setBitmapBase64(requestBodyBitmapBase64);
			WhiteboardStorage.INSTANCE.save(historyForSystem, true);
			
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_OK, "Whiteboard bitmap updated successfully", null, "Everything went fine.");
		}
		catch(Exception e) {
			e.printStackTrace();
			
	    	response.setContentType("text/plain; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_CONFLICT);
			
			PrintWriter out = response.getWriter();
			out.print(e.getMessage());
			out.close();
		}
	}

}

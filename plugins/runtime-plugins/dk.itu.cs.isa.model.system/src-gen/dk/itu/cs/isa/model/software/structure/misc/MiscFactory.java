/**
 */
package dk.itu.cs.isa.model.software.structure.misc;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage
 * @generated
 */
public interface MiscFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MiscFactory eINSTANCE = dk.itu.cs.isa.model.software.structure.misc.impl.MiscFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ISA Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Type</em>'.
	 * @generated
	 */
	ISAType createISAType();

	/**
	 * Returns a new object of class '<em>ISA Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Reference</em>'.
	 * @generated
	 */
	<T extends ISAReferenceableElement> ISAReference<T> createISAReference();

	/**
	 * Returns a new object of class '<em>ISA Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Parameter</em>'.
	 * @generated
	 */
	ISAParameter createISAParameter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MiscPackage getMiscPackage();

} //MiscFactory

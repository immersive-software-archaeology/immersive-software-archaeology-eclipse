/**
 */
package dk.itu.cs.isa.model.visualization;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Space Station</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getPosition <em>Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getSatelliteStations <em>Satellite Stations</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getBlocks <em>Blocks</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getAntennas <em>Antennas</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#isPublic <em>Public</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getInheritanceRelations <em>Inheritance Relations</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStation()
 * @model
 * @generated
 */
public interface ISASpaceStation extends ISANamedVisualElement {
	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' containment reference.
	 * @see #setPosition(ISAVector3)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStation_Position()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getPosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getPosition <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' containment reference.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Satellite Stations</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISASpaceStation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Satellite Stations</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStation_SatelliteStations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISASpaceStation> getSatelliteStations();

	/**
	 * Returns the value of the '<em><b>Blocks</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blocks</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStation_Blocks()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISASpaceStationBlock> getBlocks();

	/**
	 * Returns the value of the '<em><b>Antennas</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Antennas</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStation_Antennas()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISASpaceStationAntenna> getAntennas();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.itu.cs.isa.model.visualization.ISASpaceStationType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationType
	 * @see #setType(ISASpaceStationType)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStation_Type()
	 * @model required="true"
	 * @generated
	 */
	ISASpaceStationType getType();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationType
	 * @see #getType()
	 * @generated
	 */
	void setType(ISASpaceStationType value);

	/**
	 * Returns the value of the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Public</em>' attribute.
	 * @see #setPublic(boolean)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStation_Public()
	 * @model required="true"
	 * @generated
	 */
	boolean isPublic();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#isPublic <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Public</em>' attribute.
	 * @see #isPublic()
	 * @generated
	 */
	void setPublic(boolean value);

	/**
	 * Returns the value of the '<em><b>Inheritance Relations</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISASpaceStationRelation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inheritance Relations</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStation_InheritanceRelations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISASpaceStationRelation> getInheritanceRelations();

} // ISASpaceStation

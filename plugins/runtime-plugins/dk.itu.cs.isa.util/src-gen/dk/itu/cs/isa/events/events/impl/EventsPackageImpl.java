/**
 */
package dk.itu.cs.isa.events.events.impl;

import dk.itu.cs.isa.events.events.AnalysisSetupEvent;
import dk.itu.cs.isa.events.events.AnalysisSuccessEvent;
import dk.itu.cs.isa.events.events.BooleanInputEvent;
import dk.itu.cs.isa.events.events.ConsoleEvent;
import dk.itu.cs.isa.events.events.Event;
import dk.itu.cs.isa.events.events.EventType;
import dk.itu.cs.isa.events.events.EventsFactory;
import dk.itu.cs.isa.events.events.EventsPackage;
import dk.itu.cs.isa.events.events.GUIEvent;
import dk.itu.cs.isa.events.events.InputEvent;
import dk.itu.cs.isa.events.events.IntInputEvent;
import dk.itu.cs.isa.events.events.OutputEvent;
import dk.itu.cs.isa.events.events.StringChoiceEvent;
import dk.itu.cs.isa.events.events.StringInputEvent;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EventsPackageImpl extends EPackageImpl implements EventsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass consoleEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass guiEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass analysisSuccessEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass analysisSetupEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanInputEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intInputEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringInputEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringChoiceEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eventTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.events.events.EventsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EventsPackageImpl() {
		super(eNS_URI, EventsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link EventsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EventsPackage init() {
		if (isInited) return (EventsPackage)EPackage.Registry.INSTANCE.getEPackage(EventsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredEventsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		EventsPackageImpl theEventsPackage = registeredEventsPackage instanceof EventsPackageImpl ? (EventsPackageImpl)registeredEventsPackage : new EventsPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theEventsPackage.createPackageContents();

		// Initialize created meta-data
		theEventsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEventsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EventsPackage.eNS_URI, theEventsPackage);
		return theEventsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEvent() {
		return eventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEvent_Text() {
		return (EAttribute)eventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEvent_Type() {
		return (EAttribute)eventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConsoleEvent() {
		return consoleEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConsoleEvent_Timestamp() {
		return (EAttribute)consoleEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGUIEvent() {
		return guiEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGUIEvent_Title() {
		return (EAttribute)guiEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGUIEvent_ButtonLabels() {
		return (EAttribute)guiEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGUIEvent_ReturnCode() {
		return (EAttribute)guiEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOutputEvent() {
		return outputEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOutputEvent_Cause() {
		return (EAttribute)outputEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAnalysisSuccessEvent() {
		return analysisSuccessEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAnalysisSuccessEvent_CalculationTime() {
		return (EAttribute)analysisSuccessEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInputEvent() {
		return inputEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAnalysisSetupEvent() {
		return analysisSetupEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAnalysisSetupEvent_SystemName() {
		return (EAttribute)analysisSetupEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBooleanInputEvent() {
		return booleanInputEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBooleanInputEvent_UserInput() {
		return (EAttribute)booleanInputEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIntInputEvent() {
		return intInputEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIntInputEvent_UserInput() {
		return (EAttribute)intInputEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringInputEvent() {
		return stringInputEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringInputEvent_UserInput() {
		return (EAttribute)stringInputEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringChoiceEvent() {
		return stringChoiceEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringChoiceEvent_Options() {
		return (EAttribute)stringChoiceEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringChoiceEvent_Selection() {
		return (EAttribute)stringChoiceEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getEventType() {
		return eventTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EventsFactory getEventsFactory() {
		return (EventsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		eventEClass = createEClass(EVENT);
		createEAttribute(eventEClass, EVENT__TEXT);
		createEAttribute(eventEClass, EVENT__TYPE);

		consoleEventEClass = createEClass(CONSOLE_EVENT);
		createEAttribute(consoleEventEClass, CONSOLE_EVENT__TIMESTAMP);

		guiEventEClass = createEClass(GUI_EVENT);
		createEAttribute(guiEventEClass, GUI_EVENT__TITLE);
		createEAttribute(guiEventEClass, GUI_EVENT__BUTTON_LABELS);
		createEAttribute(guiEventEClass, GUI_EVENT__RETURN_CODE);

		outputEventEClass = createEClass(OUTPUT_EVENT);
		createEAttribute(outputEventEClass, OUTPUT_EVENT__CAUSE);

		analysisSuccessEventEClass = createEClass(ANALYSIS_SUCCESS_EVENT);
		createEAttribute(analysisSuccessEventEClass, ANALYSIS_SUCCESS_EVENT__CALCULATION_TIME);

		inputEventEClass = createEClass(INPUT_EVENT);

		analysisSetupEventEClass = createEClass(ANALYSIS_SETUP_EVENT);
		createEAttribute(analysisSetupEventEClass, ANALYSIS_SETUP_EVENT__SYSTEM_NAME);

		booleanInputEventEClass = createEClass(BOOLEAN_INPUT_EVENT);
		createEAttribute(booleanInputEventEClass, BOOLEAN_INPUT_EVENT__USER_INPUT);

		intInputEventEClass = createEClass(INT_INPUT_EVENT);
		createEAttribute(intInputEventEClass, INT_INPUT_EVENT__USER_INPUT);

		stringInputEventEClass = createEClass(STRING_INPUT_EVENT);
		createEAttribute(stringInputEventEClass, STRING_INPUT_EVENT__USER_INPUT);

		stringChoiceEventEClass = createEClass(STRING_CHOICE_EVENT);
		createEAttribute(stringChoiceEventEClass, STRING_CHOICE_EVENT__OPTIONS);
		createEAttribute(stringChoiceEventEClass, STRING_CHOICE_EVENT__SELECTION);

		// Create enums
		eventTypeEEnum = createEEnum(EVENT_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		consoleEventEClass.getESuperTypes().add(this.getEvent());
		guiEventEClass.getESuperTypes().add(this.getEvent());
		outputEventEClass.getESuperTypes().add(this.getGUIEvent());
		analysisSuccessEventEClass.getESuperTypes().add(this.getOutputEvent());
		inputEventEClass.getESuperTypes().add(this.getGUIEvent());
		analysisSetupEventEClass.getESuperTypes().add(this.getInputEvent());
		booleanInputEventEClass.getESuperTypes().add(this.getInputEvent());
		intInputEventEClass.getESuperTypes().add(this.getInputEvent());
		stringInputEventEClass.getESuperTypes().add(this.getInputEvent());
		stringChoiceEventEClass.getESuperTypes().add(this.getInputEvent());

		// Initialize classes, features, and operations; add parameters
		initEClass(eventEClass, Event.class, "Event", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEvent_Text(), ecorePackage.getEString(), "text", null, 1, 1, Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEvent_Type(), this.getEventType(), "type", null, 0, 1, Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(consoleEventEClass, ConsoleEvent.class, "ConsoleEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConsoleEvent_Timestamp(), ecorePackage.getEDate(), "timestamp", null, 1, 1, ConsoleEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(guiEventEClass, GUIEvent.class, "GUIEvent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGUIEvent_Title(), ecorePackage.getEString(), "title", null, 1, 1, GUIEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGUIEvent_ButtonLabels(), ecorePackage.getEString(), "buttonLabels", null, 1, -1, GUIEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGUIEvent_ReturnCode(), ecorePackage.getEInt(), "returnCode", null, 1, 1, GUIEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(outputEventEClass, OutputEvent.class, "OutputEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOutputEvent_Cause(), ecorePackage.getEJavaObject(), "cause", null, 0, 1, OutputEvent.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(analysisSuccessEventEClass, AnalysisSuccessEvent.class, "AnalysisSuccessEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAnalysisSuccessEvent_CalculationTime(), ecorePackage.getEInt(), "calculationTime", null, 1, 1, AnalysisSuccessEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inputEventEClass, InputEvent.class, "InputEvent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(analysisSetupEventEClass, AnalysisSetupEvent.class, "AnalysisSetupEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAnalysisSetupEvent_SystemName(), ecorePackage.getEString(), "systemName", null, 1, 1, AnalysisSetupEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanInputEventEClass, BooleanInputEvent.class, "BooleanInputEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanInputEvent_UserInput(), ecorePackage.getEBoolean(), "userInput", null, 1, 1, BooleanInputEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(intInputEventEClass, IntInputEvent.class, "IntInputEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntInputEvent_UserInput(), ecorePackage.getEInt(), "userInput", null, 1, 1, IntInputEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringInputEventEClass, StringInputEvent.class, "StringInputEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringInputEvent_UserInput(), ecorePackage.getEString(), "userInput", null, 1, 1, StringInputEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringChoiceEventEClass, StringChoiceEvent.class, "StringChoiceEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringChoiceEvent_Options(), ecorePackage.getEString(), "options", null, 1, -1, StringChoiceEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStringChoiceEvent_Selection(), ecorePackage.getEString(), "selection", null, 0, 1, StringChoiceEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(eventTypeEEnum, EventType.class, "EventType");
		addEEnumLiteral(eventTypeEEnum, EventType.INFO);
		addEEnumLiteral(eventTypeEEnum, EventType.WARNING);
		addEEnumLiteral(eventTypeEEnum, EventType.ERROR);
		addEEnumLiteral(eventTypeEEnum, EventType.QUESTION);

		// Create resource
		createResource(eNS_URI);
	}

} //EventsPackageImpl

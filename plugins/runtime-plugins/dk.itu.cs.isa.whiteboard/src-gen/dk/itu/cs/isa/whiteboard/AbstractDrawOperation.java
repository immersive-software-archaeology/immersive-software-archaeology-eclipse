/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Draw Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getColor <em>Color</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getPenSize <em>Pen Size</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getDrawnLines <em>Drawn Lines</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractDrawOperation()
 * @model abstract="true"
 * @generated
 */
public interface AbstractDrawOperation extends AbstractOperation {
	/**
	 * Returns the value of the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' containment reference.
	 * @see #setColor(ISAColor)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractDrawOperation_Color()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAColor getColor();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getColor <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' containment reference.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(ISAColor value);

	/**
	 * Returns the value of the '<em><b>Pen Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pen Size</em>' attribute.
	 * @see #setPenSize(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractDrawOperation_PenSize()
	 * @model required="true"
	 * @generated
	 */
	int getPenSize();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation#getPenSize <em>Pen Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pen Size</em>' attribute.
	 * @see #getPenSize()
	 * @generated
	 */
	void setPenSize(int value);

	/**
	 * Returns the value of the '<em><b>Drawn Lines</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Drawn Lines</em>' attribute list.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractDrawOperation_DrawnLines()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getDrawnLines();

} // AbstractDrawOperation

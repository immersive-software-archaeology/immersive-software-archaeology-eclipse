/**
 */
package dk.itu.cs.isa.model.software.structure.libraries;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;
import dk.itu.cs.isa.model.software.structure.classifiers.ISARecord;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Library Classifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage#getISALibraryClassifier()
 * @model
 * @generated
 */
public interface ISALibraryClassifier extends ISAClass, ISAInterface, ISAEnum, ISARecord {
} // ISALibraryClassifier

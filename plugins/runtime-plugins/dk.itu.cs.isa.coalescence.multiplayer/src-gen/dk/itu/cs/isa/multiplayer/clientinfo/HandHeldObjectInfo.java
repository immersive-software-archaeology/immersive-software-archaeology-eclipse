/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hand Held Object Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getHandHeldObjectInfo()
 * @model
 * @generated
 */
public interface HandHeldObjectInfo extends BodyPartInfo {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getHandHeldObjectInfo_Id()
	 * @model required="true"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

} // HandHeldObjectInfo

package dk.itu.cs.isa.util;

public class TimeUtil {

	public static String millisToMinuteWithMillisString(long millis) {
		return millisToMinuteString(millis)
				+ "." + withLeadingZero((int) (((float)(millis))%1000), 3);
	}

	public static String millisToMinuteString(long millis) {
		return withLeadingZero((int)Math.floor((double)(millis)/60d/1000d), 2)
				+ ":" + withLeadingZero((int) (((float)(millis)/1000f)%60), 2);
	}
	
	private static String withLeadingZero(int integer, int targetLength) {
		String asString = String.valueOf(integer);
		while(asString.length() < targetLength)
			asString = "0"+asString;
		return asString;
	}

}

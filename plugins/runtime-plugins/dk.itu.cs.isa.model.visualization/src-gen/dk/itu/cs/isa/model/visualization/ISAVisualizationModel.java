/**
 */
package dk.itu.cs.isa.model.visualization;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Visualization Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getSubjectSystemCapsule <em>Subject System Capsule</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getLibraryCapsules <em>Library Capsules</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getModelCreatorName <em>Model Creator Name</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAVisualizationModel()
 * @model
 * @generated
 */
public interface ISAVisualizationModel extends ISANamedVisualElement {
	/**
	 * Returns the value of the '<em><b>Subject System Capsule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subject System Capsule</em>' containment reference.
	 * @see #setSubjectSystemCapsule(ISATransparentCapsule)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAVisualizationModel_SubjectSystemCapsule()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISATransparentCapsule getSubjectSystemCapsule();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getSubjectSystemCapsule <em>Subject System Capsule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subject System Capsule</em>' containment reference.
	 * @see #getSubjectSystemCapsule()
	 * @generated
	 */
	void setSubjectSystemCapsule(ISATransparentCapsule value);

	/**
	 * Returns the value of the '<em><b>Library Capsules</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Library Capsules</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAVisualizationModel_LibraryCapsules()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISATransparentCapsule> getLibraryCapsules();

	/**
	 * Returns the value of the '<em><b>Model Creator Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Creator Name</em>' attribute.
	 * @see #setModelCreatorName(String)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAVisualizationModel_ModelCreatorName()
	 * @model required="true"
	 * @generated
	 */
	String getModelCreatorName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getModelCreatorName <em>Model Creator Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Creator Name</em>' attribute.
	 * @see #getModelCreatorName()
	 * @generated
	 */
	void setModelCreatorName(String value);

} // ISAVisualizationModel

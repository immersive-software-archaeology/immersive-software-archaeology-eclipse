package dk.itu.cs.isa.model.quality.sonarqube.hook;

import dk.itu.cs.isa.coalescence.server.ISAAbstractJettyHTTPServer;
import dk.itu.cs.isa.model.quality.sonarqube.SystemModelQualityAnalyzerSonarQube;



public class SonarQubeWebHookServer extends ISAAbstractJettyHTTPServer {
	
	public static final SonarQubeWebHookServer INSTANCE = new SonarQubeWebHookServer();
	
	public static final String WEB_HOOK_URL_SUFFIX = "sonarqube-hook";

	private SystemModelQualityAnalyzerSonarQube qualityAnalyzer;
	
	
	
	private SonarQubeWebHookServer() {
		init("ISA Sonar Qube Web Hook Server", 9002, new SonarQubeWebHookRequestHandler());
	}
	
	public void init(SystemModelQualityAnalyzerSonarQube qualityAnalyzer) {
		INSTANCE.qualityAnalyzer = qualityAnalyzer;
	}

	@Override
	public String getServerAddress() {
		return super.getServerAddress() + WEB_HOOK_URL_SUFFIX;
	}
	
	
	
	public SystemModelQualityAnalyzerSonarQube getQualityAnalyzer() {
		return qualityAnalyzer;
	}
	
}

/**
 */
package dk.itu.cs.isa.model.software.structure.misc;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Typed Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#isArrayType <em>Array Type</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISATypedElement()
 * @model abstract="true"
 * @generated
 */
public interface ISATypedElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.misc.ISAType#getUsagesAsType <em>Usages As Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(ISAType)
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISATypedElement_Type()
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAType#getUsagesAsType
	 * @model opposite="usagesAsType"
	 * @generated
	 */
	ISAType getType();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ISAType value);

	/**
	 * Returns the value of the '<em><b>Array Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array Type</em>' attribute.
	 * @see #setArrayType(boolean)
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISATypedElement_ArrayType()
	 * @model required="true"
	 * @generated
	 */
	boolean isArrayType();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#isArrayType <em>Array Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Array Type</em>' attribute.
	 * @see #isArrayType()
	 * @generated
	 */
	void setArrayType(boolean value);

} // ISATypedElement

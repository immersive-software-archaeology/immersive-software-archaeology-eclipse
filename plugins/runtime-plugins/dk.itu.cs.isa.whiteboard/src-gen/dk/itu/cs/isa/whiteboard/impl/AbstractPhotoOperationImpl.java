/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.AbstractPhotoOperation;
import dk.itu.cs.isa.whiteboard.ISAVector2;
import dk.itu.cs.isa.whiteboard.ISAVector3;
import dk.itu.cs.isa.whiteboard.VisibleRelation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Photo Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getPhotoId <em>Photo Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getBitmapBase64 <em>Bitmap Base64</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getLocalSpacePosition <em>Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getLocalSpaceForwardDirection <em>Local Space Forward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getLocalSpaceUpwardDirection <em>Local Space Upward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getTextureSpacePosition <em>Texture Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getCameraPositionWhenPhotoWasTaken <em>Camera Position When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getCameraRotationWhenPhotoWasTaken <em>Camera Rotation When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getSystemPositionWhenPhotoWasTaken <em>System Position When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getSystemSizeWhenPhotoWasTaken <em>System Size When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getRelationsVisibleWhenPhotoWasTaken <em>Relations Visible When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractPhotoOperationImpl#getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken <em>Open Capsules Element Qualified Names When Photo Was Taken</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractPhotoOperationImpl extends AbstractOperationImpl implements AbstractPhotoOperation {
	/**
	 * The default value of the '{@link #getPhotoId() <em>Photo Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhotoId()
	 * @generated
	 * @ordered
	 */
	protected static final int PHOTO_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPhotoId() <em>Photo Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhotoId()
	 * @generated
	 * @ordered
	 */
	protected int photoId = PHOTO_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getBitmapBase64() <em>Bitmap Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitmapBase64()
	 * @generated
	 * @ordered
	 */
	protected static final String BITMAP_BASE64_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBitmapBase64() <em>Bitmap Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitmapBase64()
	 * @generated
	 * @ordered
	 */
	protected String bitmapBase64 = BITMAP_BASE64_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLocalSpacePosition() <em>Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 localSpacePosition;

	/**
	 * The cached value of the '{@link #getLocalSpaceForwardDirection() <em>Local Space Forward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSpaceForwardDirection()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 localSpaceForwardDirection;

	/**
	 * The cached value of the '{@link #getLocalSpaceUpwardDirection() <em>Local Space Upward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSpaceUpwardDirection()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 localSpaceUpwardDirection;

	/**
	 * The cached value of the '{@link #getTextureSpacePosition() <em>Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTextureSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector2 textureSpacePosition;

	/**
	 * The cached value of the '{@link #getCameraPositionWhenPhotoWasTaken() <em>Camera Position When Photo Was Taken</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCameraPositionWhenPhotoWasTaken()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 cameraPositionWhenPhotoWasTaken;

	/**
	 * The cached value of the '{@link #getCameraRotationWhenPhotoWasTaken() <em>Camera Rotation When Photo Was Taken</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCameraRotationWhenPhotoWasTaken()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 cameraRotationWhenPhotoWasTaken;

	/**
	 * The cached value of the '{@link #getSystemPositionWhenPhotoWasTaken() <em>System Position When Photo Was Taken</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemPositionWhenPhotoWasTaken()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 systemPositionWhenPhotoWasTaken;

	/**
	 * The default value of the '{@link #getSystemSizeWhenPhotoWasTaken() <em>System Size When Photo Was Taken</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemSizeWhenPhotoWasTaken()
	 * @generated
	 * @ordered
	 */
	protected static final float SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getSystemSizeWhenPhotoWasTaken() <em>System Size When Photo Was Taken</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemSizeWhenPhotoWasTaken()
	 * @generated
	 * @ordered
	 */
	protected float systemSizeWhenPhotoWasTaken = SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRelationsVisibleWhenPhotoWasTaken() <em>Relations Visible When Photo Was Taken</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationsVisibleWhenPhotoWasTaken()
	 * @generated
	 * @ordered
	 */
	protected EList<VisibleRelation> relationsVisibleWhenPhotoWasTaken;

	/**
	 * The cached value of the '{@link #getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken() <em>Open Capsules Element Qualified Names When Photo Was Taken</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken()
	 * @generated
	 * @ordered
	 */
	protected EList<String> openCapsulesElementQualifiedNamesWhenPhotoWasTaken;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractPhotoOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.ABSTRACT_PHOTO_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPhotoId() {
		return photoId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhotoId(int newPhotoId) {
		int oldPhotoId = photoId;
		photoId = newPhotoId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__PHOTO_ID, oldPhotoId, photoId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBitmapBase64() {
		return bitmapBase64;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitmapBase64(String newBitmapBase64) {
		String oldBitmapBase64 = bitmapBase64;
		bitmapBase64 = newBitmapBase64;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64, oldBitmapBase64, bitmapBase64));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getLocalSpacePosition() {
		return localSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocalSpacePosition(ISAVector3 newLocalSpacePosition, NotificationChain msgs) {
		ISAVector3 oldLocalSpacePosition = localSpacePosition;
		localSpacePosition = newLocalSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION, oldLocalSpacePosition, newLocalSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalSpacePosition(ISAVector3 newLocalSpacePosition) {
		if (newLocalSpacePosition != localSpacePosition) {
			NotificationChain msgs = null;
			if (localSpacePosition != null)
				msgs = ((InternalEObject)localSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION, null, msgs);
			if (newLocalSpacePosition != null)
				msgs = ((InternalEObject)newLocalSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION, null, msgs);
			msgs = basicSetLocalSpacePosition(newLocalSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION, newLocalSpacePosition, newLocalSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getLocalSpaceForwardDirection() {
		return localSpaceForwardDirection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocalSpaceForwardDirection(ISAVector3 newLocalSpaceForwardDirection, NotificationChain msgs) {
		ISAVector3 oldLocalSpaceForwardDirection = localSpaceForwardDirection;
		localSpaceForwardDirection = newLocalSpaceForwardDirection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION, oldLocalSpaceForwardDirection, newLocalSpaceForwardDirection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalSpaceForwardDirection(ISAVector3 newLocalSpaceForwardDirection) {
		if (newLocalSpaceForwardDirection != localSpaceForwardDirection) {
			NotificationChain msgs = null;
			if (localSpaceForwardDirection != null)
				msgs = ((InternalEObject)localSpaceForwardDirection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION, null, msgs);
			if (newLocalSpaceForwardDirection != null)
				msgs = ((InternalEObject)newLocalSpaceForwardDirection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION, null, msgs);
			msgs = basicSetLocalSpaceForwardDirection(newLocalSpaceForwardDirection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION, newLocalSpaceForwardDirection, newLocalSpaceForwardDirection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getLocalSpaceUpwardDirection() {
		return localSpaceUpwardDirection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocalSpaceUpwardDirection(ISAVector3 newLocalSpaceUpwardDirection, NotificationChain msgs) {
		ISAVector3 oldLocalSpaceUpwardDirection = localSpaceUpwardDirection;
		localSpaceUpwardDirection = newLocalSpaceUpwardDirection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION, oldLocalSpaceUpwardDirection, newLocalSpaceUpwardDirection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalSpaceUpwardDirection(ISAVector3 newLocalSpaceUpwardDirection) {
		if (newLocalSpaceUpwardDirection != localSpaceUpwardDirection) {
			NotificationChain msgs = null;
			if (localSpaceUpwardDirection != null)
				msgs = ((InternalEObject)localSpaceUpwardDirection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION, null, msgs);
			if (newLocalSpaceUpwardDirection != null)
				msgs = ((InternalEObject)newLocalSpaceUpwardDirection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION, null, msgs);
			msgs = basicSetLocalSpaceUpwardDirection(newLocalSpaceUpwardDirection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION, newLocalSpaceUpwardDirection, newLocalSpaceUpwardDirection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector2 getTextureSpacePosition() {
		return textureSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTextureSpacePosition(ISAVector2 newTextureSpacePosition, NotificationChain msgs) {
		ISAVector2 oldTextureSpacePosition = textureSpacePosition;
		textureSpacePosition = newTextureSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION, oldTextureSpacePosition, newTextureSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTextureSpacePosition(ISAVector2 newTextureSpacePosition) {
		if (newTextureSpacePosition != textureSpacePosition) {
			NotificationChain msgs = null;
			if (textureSpacePosition != null)
				msgs = ((InternalEObject)textureSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION, null, msgs);
			if (newTextureSpacePosition != null)
				msgs = ((InternalEObject)newTextureSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION, null, msgs);
			msgs = basicSetTextureSpacePosition(newTextureSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION, newTextureSpacePosition, newTextureSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getCameraPositionWhenPhotoWasTaken() {
		return cameraPositionWhenPhotoWasTaken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCameraPositionWhenPhotoWasTaken(ISAVector3 newCameraPositionWhenPhotoWasTaken, NotificationChain msgs) {
		ISAVector3 oldCameraPositionWhenPhotoWasTaken = cameraPositionWhenPhotoWasTaken;
		cameraPositionWhenPhotoWasTaken = newCameraPositionWhenPhotoWasTaken;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN, oldCameraPositionWhenPhotoWasTaken, newCameraPositionWhenPhotoWasTaken);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCameraPositionWhenPhotoWasTaken(ISAVector3 newCameraPositionWhenPhotoWasTaken) {
		if (newCameraPositionWhenPhotoWasTaken != cameraPositionWhenPhotoWasTaken) {
			NotificationChain msgs = null;
			if (cameraPositionWhenPhotoWasTaken != null)
				msgs = ((InternalEObject)cameraPositionWhenPhotoWasTaken).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN, null, msgs);
			if (newCameraPositionWhenPhotoWasTaken != null)
				msgs = ((InternalEObject)newCameraPositionWhenPhotoWasTaken).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN, null, msgs);
			msgs = basicSetCameraPositionWhenPhotoWasTaken(newCameraPositionWhenPhotoWasTaken, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN, newCameraPositionWhenPhotoWasTaken, newCameraPositionWhenPhotoWasTaken));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getCameraRotationWhenPhotoWasTaken() {
		return cameraRotationWhenPhotoWasTaken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCameraRotationWhenPhotoWasTaken(ISAVector3 newCameraRotationWhenPhotoWasTaken, NotificationChain msgs) {
		ISAVector3 oldCameraRotationWhenPhotoWasTaken = cameraRotationWhenPhotoWasTaken;
		cameraRotationWhenPhotoWasTaken = newCameraRotationWhenPhotoWasTaken;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN, oldCameraRotationWhenPhotoWasTaken, newCameraRotationWhenPhotoWasTaken);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCameraRotationWhenPhotoWasTaken(ISAVector3 newCameraRotationWhenPhotoWasTaken) {
		if (newCameraRotationWhenPhotoWasTaken != cameraRotationWhenPhotoWasTaken) {
			NotificationChain msgs = null;
			if (cameraRotationWhenPhotoWasTaken != null)
				msgs = ((InternalEObject)cameraRotationWhenPhotoWasTaken).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN, null, msgs);
			if (newCameraRotationWhenPhotoWasTaken != null)
				msgs = ((InternalEObject)newCameraRotationWhenPhotoWasTaken).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN, null, msgs);
			msgs = basicSetCameraRotationWhenPhotoWasTaken(newCameraRotationWhenPhotoWasTaken, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN, newCameraRotationWhenPhotoWasTaken, newCameraRotationWhenPhotoWasTaken));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VisibleRelation> getRelationsVisibleWhenPhotoWasTaken() {
		if (relationsVisibleWhenPhotoWasTaken == null) {
			relationsVisibleWhenPhotoWasTaken = new EObjectContainmentEList<VisibleRelation>(VisibleRelation.class, this, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN);
		}
		return relationsVisibleWhenPhotoWasTaken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken() {
		if (openCapsulesElementQualifiedNamesWhenPhotoWasTaken == null) {
			openCapsulesElementQualifiedNamesWhenPhotoWasTaken = new EDataTypeUniqueEList<String>(String.class, this, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN);
		}
		return openCapsulesElementQualifiedNamesWhenPhotoWasTaken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getSystemSizeWhenPhotoWasTaken() {
		return systemSizeWhenPhotoWasTaken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemSizeWhenPhotoWasTaken(float newSystemSizeWhenPhotoWasTaken) {
		float oldSystemSizeWhenPhotoWasTaken = systemSizeWhenPhotoWasTaken;
		systemSizeWhenPhotoWasTaken = newSystemSizeWhenPhotoWasTaken;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN, oldSystemSizeWhenPhotoWasTaken, systemSizeWhenPhotoWasTaken));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getSystemPositionWhenPhotoWasTaken() {
		return systemPositionWhenPhotoWasTaken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSystemPositionWhenPhotoWasTaken(ISAVector3 newSystemPositionWhenPhotoWasTaken, NotificationChain msgs) {
		ISAVector3 oldSystemPositionWhenPhotoWasTaken = systemPositionWhenPhotoWasTaken;
		systemPositionWhenPhotoWasTaken = newSystemPositionWhenPhotoWasTaken;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN, oldSystemPositionWhenPhotoWasTaken, newSystemPositionWhenPhotoWasTaken);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemPositionWhenPhotoWasTaken(ISAVector3 newSystemPositionWhenPhotoWasTaken) {
		if (newSystemPositionWhenPhotoWasTaken != systemPositionWhenPhotoWasTaken) {
			NotificationChain msgs = null;
			if (systemPositionWhenPhotoWasTaken != null)
				msgs = ((InternalEObject)systemPositionWhenPhotoWasTaken).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN, null, msgs);
			if (newSystemPositionWhenPhotoWasTaken != null)
				msgs = ((InternalEObject)newSystemPositionWhenPhotoWasTaken).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN, null, msgs);
			msgs = basicSetSystemPositionWhenPhotoWasTaken(newSystemPositionWhenPhotoWasTaken, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN, newSystemPositionWhenPhotoWasTaken, newSystemPositionWhenPhotoWasTaken));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION:
				return basicSetLocalSpacePosition(null, msgs);
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION:
				return basicSetLocalSpaceForwardDirection(null, msgs);
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION:
				return basicSetLocalSpaceUpwardDirection(null, msgs);
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION:
				return basicSetTextureSpacePosition(null, msgs);
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN:
				return basicSetCameraPositionWhenPhotoWasTaken(null, msgs);
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN:
				return basicSetCameraRotationWhenPhotoWasTaken(null, msgs);
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN:
				return basicSetSystemPositionWhenPhotoWasTaken(null, msgs);
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN:
				return ((InternalEList<?>)getRelationsVisibleWhenPhotoWasTaken()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__PHOTO_ID:
				return getPhotoId();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64:
				return getBitmapBase64();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION:
				return getLocalSpacePosition();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION:
				return getLocalSpaceForwardDirection();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION:
				return getLocalSpaceUpwardDirection();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION:
				return getTextureSpacePosition();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN:
				return getCameraPositionWhenPhotoWasTaken();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN:
				return getCameraRotationWhenPhotoWasTaken();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN:
				return getSystemPositionWhenPhotoWasTaken();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN:
				return getSystemSizeWhenPhotoWasTaken();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN:
				return getRelationsVisibleWhenPhotoWasTaken();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN:
				return getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__PHOTO_ID:
				setPhotoId((Integer)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64:
				setBitmapBase64((String)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION:
				setLocalSpacePosition((ISAVector3)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION:
				setLocalSpaceForwardDirection((ISAVector3)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION:
				setLocalSpaceUpwardDirection((ISAVector3)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION:
				setTextureSpacePosition((ISAVector2)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN:
				setCameraPositionWhenPhotoWasTaken((ISAVector3)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN:
				setCameraRotationWhenPhotoWasTaken((ISAVector3)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN:
				setSystemPositionWhenPhotoWasTaken((ISAVector3)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN:
				setSystemSizeWhenPhotoWasTaken((Float)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN:
				getRelationsVisibleWhenPhotoWasTaken().clear();
				getRelationsVisibleWhenPhotoWasTaken().addAll((Collection<? extends VisibleRelation>)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN:
				getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken().clear();
				getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__PHOTO_ID:
				setPhotoId(PHOTO_ID_EDEFAULT);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64:
				setBitmapBase64(BITMAP_BASE64_EDEFAULT);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION:
				setLocalSpacePosition((ISAVector3)null);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION:
				setLocalSpaceForwardDirection((ISAVector3)null);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION:
				setLocalSpaceUpwardDirection((ISAVector3)null);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION:
				setTextureSpacePosition((ISAVector2)null);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN:
				setCameraPositionWhenPhotoWasTaken((ISAVector3)null);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN:
				setCameraRotationWhenPhotoWasTaken((ISAVector3)null);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN:
				setSystemPositionWhenPhotoWasTaken((ISAVector3)null);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN:
				setSystemSizeWhenPhotoWasTaken(SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN_EDEFAULT);
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN:
				getRelationsVisibleWhenPhotoWasTaken().clear();
				return;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN:
				getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__PHOTO_ID:
				return photoId != PHOTO_ID_EDEFAULT;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64:
				return BITMAP_BASE64_EDEFAULT == null ? bitmapBase64 != null : !BITMAP_BASE64_EDEFAULT.equals(bitmapBase64);
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION:
				return localSpacePosition != null;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION:
				return localSpaceForwardDirection != null;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION:
				return localSpaceUpwardDirection != null;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION:
				return textureSpacePosition != null;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN:
				return cameraPositionWhenPhotoWasTaken != null;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN:
				return cameraRotationWhenPhotoWasTaken != null;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN:
				return systemPositionWhenPhotoWasTaken != null;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN:
				return systemSizeWhenPhotoWasTaken != SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN_EDEFAULT;
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN:
				return relationsVisibleWhenPhotoWasTaken != null && !relationsVisibleWhenPhotoWasTaken.isEmpty();
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN:
				return openCapsulesElementQualifiedNamesWhenPhotoWasTaken != null && !openCapsulesElementQualifiedNamesWhenPhotoWasTaken.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (photoId: ");
		result.append(photoId);
		result.append(", bitmapBase64: ");
		result.append(bitmapBase64);
		result.append(", systemSizeWhenPhotoWasTaken: ");
		result.append(systemSizeWhenPhotoWasTaken);
		result.append(", openCapsulesElementQualifiedNamesWhenPhotoWasTaken: ");
		result.append(openCapsulesElementQualifiedNamesWhenPhotoWasTaken);
		result.append(')');
		return result.toString();
	}

} //AbstractPhotoOperationImpl

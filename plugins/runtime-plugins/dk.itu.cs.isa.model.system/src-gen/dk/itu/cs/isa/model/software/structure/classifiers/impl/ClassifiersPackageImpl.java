/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.impl;

import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage;

import dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersFactory;
import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAAbstractClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAConcreteClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement;
import dk.itu.cs.isa.model.software.structure.classifiers.ISARecord;
import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;

import dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl;

import dk.itu.cs.isa.model.software.structure.impl.StructurePackageImpl;

import dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage;

import dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl;

import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassifiersPackageImpl extends EPackageImpl implements ClassifiersPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaClassifierContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaClassifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaAbstractClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaConcreteClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaInterfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaEnumEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaRecordEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaInterfaceExtendingElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ClassifiersPackageImpl() {
		super(eNS_URI, ClassifiersFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ClassifiersPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ClassifiersPackage init() {
		if (isInited) return (ClassifiersPackage)EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredClassifiersPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ClassifiersPackageImpl theClassifiersPackage = registeredClassifiersPackage instanceof ClassifiersPackageImpl ? (ClassifiersPackageImpl)registeredClassifiersPackage : new ClassifiersPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);
		StructurePackageImpl theStructurePackage = (StructurePackageImpl)(registeredPackage instanceof StructurePackageImpl ? registeredPackage : StructurePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);
		ArchitecturePackageImpl theArchitecturePackage = (ArchitecturePackageImpl)(registeredPackage instanceof ArchitecturePackageImpl ? registeredPackage : ArchitecturePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FilesystemPackage.eNS_URI);
		FilesystemPackageImpl theFilesystemPackage = (FilesystemPackageImpl)(registeredPackage instanceof FilesystemPackageImpl ? registeredPackage : FilesystemPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LibrariesPackage.eNS_URI);
		LibrariesPackageImpl theLibrariesPackage = (LibrariesPackageImpl)(registeredPackage instanceof LibrariesPackageImpl ? registeredPackage : LibrariesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);
		MembersPackageImpl theMembersPackage = (MembersPackageImpl)(registeredPackage instanceof MembersPackageImpl ? registeredPackage : MembersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);
		MiscPackageImpl theMiscPackage = (MiscPackageImpl)(registeredPackage instanceof MiscPackageImpl ? registeredPackage : MiscPackage.eINSTANCE);

		// Create package meta-data objects
		theClassifiersPackage.createPackageContents();
		theStructurePackage.createPackageContents();
		theArchitecturePackage.createPackageContents();
		theFilesystemPackage.createPackageContents();
		theLibrariesPackage.createPackageContents();
		theMembersPackage.createPackageContents();
		theMiscPackage.createPackageContents();

		// Initialize created meta-data
		theClassifiersPackage.initializePackageContents();
		theStructurePackage.initializePackageContents();
		theArchitecturePackage.initializePackageContents();
		theFilesystemPackage.initializePackageContents();
		theLibrariesPackage.initializePackageContents();
		theMembersPackage.initializePackageContents();
		theMiscPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClassifiersPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClassifiersPackage.eNS_URI, theClassifiersPackage);
		return theClassifiersPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAClassifierContainer() {
		return isaClassifierContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClassifierContainer_Classifiers() {
		return (EReference)isaClassifierContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAClassifier() {
		return isaClassifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClassifier_Parent() {
		return (EReference)isaClassifierEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClassifier_Fields() {
		return (EReference)isaClassifierEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClassifier_Methods() {
		return (EReference)isaClassifierEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAClassifier_FilePath() {
		return (EAttribute)isaClassifierEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISAClassifier__GetContainingPackage() {
		return isaClassifierEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISAClassifier__GetContainedMembers() {
		return isaClassifierEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAClass() {
		return isaClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClass_ExtendedClass() {
		return (EReference)isaClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClass_ImplementedInterfaces() {
		return (EReference)isaClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClass_UsagesAsExtendedClass() {
		return (EReference)isaClassEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAAbstractClass() {
		return isaAbstractClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAConcreteClass() {
		return isaConcreteClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAInterface() {
		return isaInterfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAInterface_UsagesAsImplementedInterface() {
		return (EReference)isaInterfaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAInterface_UsagesAsExtendedInterface() {
		return (EReference)isaInterfaceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAEnum() {
		return isaEnumEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAEnum_Constants() {
		return (EReference)isaEnumEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISARecord() {
		return isaRecordEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISARecord_Components() {
		return (EReference)isaRecordEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAInterfaceExtendingElement() {
		return isaInterfaceExtendingElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAInterfaceExtendingElement_ExtendedInterfaces() {
		return (EReference)isaInterfaceExtendingElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ClassifiersFactory getClassifiersFactory() {
		return (ClassifiersFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaClassifierContainerEClass = createEClass(ISA_CLASSIFIER_CONTAINER);
		createEReference(isaClassifierContainerEClass, ISA_CLASSIFIER_CONTAINER__CLASSIFIERS);

		isaClassifierEClass = createEClass(ISA_CLASSIFIER);
		createEReference(isaClassifierEClass, ISA_CLASSIFIER__PARENT);
		createEReference(isaClassifierEClass, ISA_CLASSIFIER__FIELDS);
		createEReference(isaClassifierEClass, ISA_CLASSIFIER__METHODS);
		createEAttribute(isaClassifierEClass, ISA_CLASSIFIER__FILE_PATH);
		createEOperation(isaClassifierEClass, ISA_CLASSIFIER___GET_CONTAINING_PACKAGE);
		createEOperation(isaClassifierEClass, ISA_CLASSIFIER___GET_CONTAINED_MEMBERS);

		isaClassEClass = createEClass(ISA_CLASS);
		createEReference(isaClassEClass, ISA_CLASS__EXTENDED_CLASS);
		createEReference(isaClassEClass, ISA_CLASS__IMPLEMENTED_INTERFACES);
		createEReference(isaClassEClass, ISA_CLASS__USAGES_AS_EXTENDED_CLASS);

		isaAbstractClassEClass = createEClass(ISA_ABSTRACT_CLASS);

		isaConcreteClassEClass = createEClass(ISA_CONCRETE_CLASS);

		isaInterfaceEClass = createEClass(ISA_INTERFACE);
		createEReference(isaInterfaceEClass, ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE);
		createEReference(isaInterfaceEClass, ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE);

		isaEnumEClass = createEClass(ISA_ENUM);
		createEReference(isaEnumEClass, ISA_ENUM__CONSTANTS);

		isaRecordEClass = createEClass(ISA_RECORD);
		createEReference(isaRecordEClass, ISA_RECORD__COMPONENTS);

		isaInterfaceExtendingElementEClass = createEClass(ISA_INTERFACE_EXTENDING_ELEMENT);
		createEReference(isaInterfaceExtendingElementEClass, ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MiscPackage theMiscPackage = (MiscPackage)EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);
		MembersPackage theMembersPackage = (MembersPackage)EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);
		FilesystemPackage theFilesystemPackage = (FilesystemPackage)EPackage.Registry.INSTANCE.getEPackage(FilesystemPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		isaClassifierEClass.getESuperTypes().add(theMiscPackage.getISAType());
		isaClassifierEClass.getESuperTypes().add(this.getISAClassifierContainer());
		isaClassifierEClass.getESuperTypes().add(theMembersPackage.getISAMember());
		isaClassEClass.getESuperTypes().add(this.getISAClassifier());
		isaClassEClass.getESuperTypes().add(theMembersPackage.getISAConstructorContainer());
		isaAbstractClassEClass.getESuperTypes().add(this.getISAClass());
		isaConcreteClassEClass.getESuperTypes().add(this.getISAClass());
		isaInterfaceEClass.getESuperTypes().add(this.getISAClassifier());
		isaInterfaceEClass.getESuperTypes().add(this.getISAInterfaceExtendingElement());
		isaEnumEClass.getESuperTypes().add(this.getISAClassifier());
		isaEnumEClass.getESuperTypes().add(theMembersPackage.getISAConstructorContainer());
		isaEnumEClass.getESuperTypes().add(this.getISAClassifierContainer());
		isaRecordEClass.getESuperTypes().add(this.getISAClassifier());
		isaRecordEClass.getESuperTypes().add(theMembersPackage.getISAConstructorContainer());
		isaRecordEClass.getESuperTypes().add(this.getISAClassifierContainer());
		isaRecordEClass.getESuperTypes().add(this.getISAInterfaceExtendingElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(isaClassifierContainerEClass, ISAClassifierContainer.class, "ISAClassifierContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAClassifierContainer_Classifiers(), this.getISAClassifier(), this.getISAClassifier_Parent(), "classifiers", null, 0, -1, ISAClassifierContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaClassifierEClass, ISAClassifier.class, "ISAClassifier", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAClassifier_Parent(), this.getISAClassifierContainer(), this.getISAClassifierContainer_Classifiers(), "parent", null, 1, 1, ISAClassifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAClassifier_Fields(), theMembersPackage.getISAField(), null, "fields", null, 0, -1, ISAClassifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAClassifier_Methods(), theMembersPackage.getISAMethod(), null, "methods", null, 0, -1, ISAClassifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAClassifier_FilePath(), ecorePackage.getEString(), "filePath", null, 0, 1, ISAClassifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getISAClassifier__GetContainingPackage(), theFilesystemPackage.getISAPackage(), "getContainingPackage", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getISAClassifier__GetContainedMembers(), theMembersPackage.getISAMember(), "getContainedMembers", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(isaClassEClass, ISAClass.class, "ISAClass", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAClass_ExtendedClass(), this.getISAClass(), this.getISAClass_UsagesAsExtendedClass(), "extendedClass", null, 0, 1, ISAClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAClass_ImplementedInterfaces(), this.getISAInterface(), this.getISAInterface_UsagesAsImplementedInterface(), "implementedInterfaces", null, 0, -1, ISAClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAClass_UsagesAsExtendedClass(), this.getISAClass(), this.getISAClass_ExtendedClass(), "usagesAsExtendedClass", null, 0, -1, ISAClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaAbstractClassEClass, ISAAbstractClass.class, "ISAAbstractClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isaConcreteClassEClass, ISAConcreteClass.class, "ISAConcreteClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(isaInterfaceEClass, ISAInterface.class, "ISAInterface", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAInterface_UsagesAsImplementedInterface(), this.getISAClass(), this.getISAClass_ImplementedInterfaces(), "usagesAsImplementedInterface", null, 0, -1, ISAInterface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAInterface_UsagesAsExtendedInterface(), this.getISAInterfaceExtendingElement(), this.getISAInterfaceExtendingElement_ExtendedInterfaces(), "usagesAsExtendedInterface", null, 0, -1, ISAInterface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaEnumEClass, ISAEnum.class, "ISAEnum", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAEnum_Constants(), theMembersPackage.getISAEnumConstant(), null, "constants", null, 0, -1, ISAEnum.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaRecordEClass, ISARecord.class, "ISARecord", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISARecord_Components(), theMembersPackage.getISARecordComponent(), null, "components", null, 0, -1, ISARecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaInterfaceExtendingElementEClass, ISAInterfaceExtendingElement.class, "ISAInterfaceExtendingElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAInterfaceExtendingElement_ExtendedInterfaces(), this.getISAInterface(), this.getISAInterface_UsagesAsExtendedInterface(), "extendedInterfaces", null, 0, -1, ISAInterfaceExtendingElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //ClassifiersPackageImpl

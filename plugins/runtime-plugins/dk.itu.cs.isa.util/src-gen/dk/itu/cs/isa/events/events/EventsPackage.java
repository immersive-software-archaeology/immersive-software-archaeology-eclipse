/**
 */
package dk.itu.cs.isa.events.events;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.events.events.EventsFactory
 * @model kind="package"
 * @generated
 */
public interface EventsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "events";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/events";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "events";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EventsPackage eINSTANCE = dk.itu.cs.isa.events.events.impl.EventsPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.EventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 0;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TEXT = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.ConsoleEventImpl <em>Console Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.ConsoleEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getConsoleEvent()
	 * @generated
	 */
	int CONSOLE_EVENT = 1;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE_EVENT__TEXT = EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE_EVENT__TYPE = EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE_EVENT__TIMESTAMP = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Console Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Console Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSOLE_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.GUIEventImpl <em>GUI Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.GUIEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getGUIEvent()
	 * @generated
	 */
	int GUI_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUI_EVENT__TEXT = EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUI_EVENT__TYPE = EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUI_EVENT__TITLE = EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Button Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUI_EVENT__BUTTON_LABELS = EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUI_EVENT__RETURN_CODE = EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>GUI Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUI_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>GUI Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUI_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.OutputEventImpl <em>Output Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.OutputEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getOutputEvent()
	 * @generated
	 */
	int OUTPUT_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_EVENT__TEXT = GUI_EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_EVENT__TYPE = GUI_EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_EVENT__TITLE = GUI_EVENT__TITLE;

	/**
	 * The feature id for the '<em><b>Button Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_EVENT__BUTTON_LABELS = GUI_EVENT__BUTTON_LABELS;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_EVENT__RETURN_CODE = GUI_EVENT__RETURN_CODE;

	/**
	 * The feature id for the '<em><b>Cause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_EVENT__CAUSE = GUI_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Output Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_EVENT_FEATURE_COUNT = GUI_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Output Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_EVENT_OPERATION_COUNT = GUI_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.AnalysisSuccessEventImpl <em>Analysis Success Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.AnalysisSuccessEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getAnalysisSuccessEvent()
	 * @generated
	 */
	int ANALYSIS_SUCCESS_EVENT = 4;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SUCCESS_EVENT__TEXT = OUTPUT_EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SUCCESS_EVENT__TYPE = OUTPUT_EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SUCCESS_EVENT__TITLE = OUTPUT_EVENT__TITLE;

	/**
	 * The feature id for the '<em><b>Button Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SUCCESS_EVENT__BUTTON_LABELS = OUTPUT_EVENT__BUTTON_LABELS;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SUCCESS_EVENT__RETURN_CODE = OUTPUT_EVENT__RETURN_CODE;

	/**
	 * The feature id for the '<em><b>Cause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SUCCESS_EVENT__CAUSE = OUTPUT_EVENT__CAUSE;

	/**
	 * The feature id for the '<em><b>Calculation Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SUCCESS_EVENT__CALCULATION_TIME = OUTPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Analysis Success Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SUCCESS_EVENT_FEATURE_COUNT = OUTPUT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Analysis Success Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SUCCESS_EVENT_OPERATION_COUNT = OUTPUT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.InputEventImpl <em>Input Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.InputEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getInputEvent()
	 * @generated
	 */
	int INPUT_EVENT = 5;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT__TEXT = GUI_EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT__TYPE = GUI_EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT__TITLE = GUI_EVENT__TITLE;

	/**
	 * The feature id for the '<em><b>Button Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT__BUTTON_LABELS = GUI_EVENT__BUTTON_LABELS;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT__RETURN_CODE = GUI_EVENT__RETURN_CODE;

	/**
	 * The number of structural features of the '<em>Input Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT_FEATURE_COUNT = GUI_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Input Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_EVENT_OPERATION_COUNT = GUI_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.AnalysisSetupEventImpl <em>Analysis Setup Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.AnalysisSetupEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getAnalysisSetupEvent()
	 * @generated
	 */
	int ANALYSIS_SETUP_EVENT = 6;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SETUP_EVENT__TEXT = INPUT_EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SETUP_EVENT__TYPE = INPUT_EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SETUP_EVENT__TITLE = INPUT_EVENT__TITLE;

	/**
	 * The feature id for the '<em><b>Button Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SETUP_EVENT__BUTTON_LABELS = INPUT_EVENT__BUTTON_LABELS;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SETUP_EVENT__RETURN_CODE = INPUT_EVENT__RETURN_CODE;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SETUP_EVENT__SYSTEM_NAME = INPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Analysis Setup Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SETUP_EVENT_FEATURE_COUNT = INPUT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Analysis Setup Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANALYSIS_SETUP_EVENT_OPERATION_COUNT = INPUT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.BooleanInputEventImpl <em>Boolean Input Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.BooleanInputEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getBooleanInputEvent()
	 * @generated
	 */
	int BOOLEAN_INPUT_EVENT = 7;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_INPUT_EVENT__TEXT = INPUT_EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_INPUT_EVENT__TYPE = INPUT_EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_INPUT_EVENT__TITLE = INPUT_EVENT__TITLE;

	/**
	 * The feature id for the '<em><b>Button Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_INPUT_EVENT__BUTTON_LABELS = INPUT_EVENT__BUTTON_LABELS;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_INPUT_EVENT__RETURN_CODE = INPUT_EVENT__RETURN_CODE;

	/**
	 * The feature id for the '<em><b>User Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_INPUT_EVENT__USER_INPUT = INPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Input Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_INPUT_EVENT_FEATURE_COUNT = INPUT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Boolean Input Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_INPUT_EVENT_OPERATION_COUNT = INPUT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.IntInputEventImpl <em>Int Input Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.IntInputEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getIntInputEvent()
	 * @generated
	 */
	int INT_INPUT_EVENT = 8;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INPUT_EVENT__TEXT = INPUT_EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INPUT_EVENT__TYPE = INPUT_EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INPUT_EVENT__TITLE = INPUT_EVENT__TITLE;

	/**
	 * The feature id for the '<em><b>Button Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INPUT_EVENT__BUTTON_LABELS = INPUT_EVENT__BUTTON_LABELS;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INPUT_EVENT__RETURN_CODE = INPUT_EVENT__RETURN_CODE;

	/**
	 * The feature id for the '<em><b>User Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INPUT_EVENT__USER_INPUT = INPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Input Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INPUT_EVENT_FEATURE_COUNT = INPUT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Int Input Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INPUT_EVENT_OPERATION_COUNT = INPUT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.StringInputEventImpl <em>String Input Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.StringInputEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getStringInputEvent()
	 * @generated
	 */
	int STRING_INPUT_EVENT = 9;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INPUT_EVENT__TEXT = INPUT_EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INPUT_EVENT__TYPE = INPUT_EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INPUT_EVENT__TITLE = INPUT_EVENT__TITLE;

	/**
	 * The feature id for the '<em><b>Button Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INPUT_EVENT__BUTTON_LABELS = INPUT_EVENT__BUTTON_LABELS;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INPUT_EVENT__RETURN_CODE = INPUT_EVENT__RETURN_CODE;

	/**
	 * The feature id for the '<em><b>User Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INPUT_EVENT__USER_INPUT = INPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Input Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INPUT_EVENT_FEATURE_COUNT = INPUT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>String Input Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INPUT_EVENT_OPERATION_COUNT = INPUT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.impl.StringChoiceEventImpl <em>String Choice Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.impl.StringChoiceEventImpl
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getStringChoiceEvent()
	 * @generated
	 */
	int STRING_CHOICE_EVENT = 10;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CHOICE_EVENT__TEXT = INPUT_EVENT__TEXT;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CHOICE_EVENT__TYPE = INPUT_EVENT__TYPE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CHOICE_EVENT__TITLE = INPUT_EVENT__TITLE;

	/**
	 * The feature id for the '<em><b>Button Labels</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CHOICE_EVENT__BUTTON_LABELS = INPUT_EVENT__BUTTON_LABELS;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CHOICE_EVENT__RETURN_CODE = INPUT_EVENT__RETURN_CODE;

	/**
	 * The feature id for the '<em><b>Options</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CHOICE_EVENT__OPTIONS = INPUT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CHOICE_EVENT__SELECTION = INPUT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>String Choice Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CHOICE_EVENT_FEATURE_COUNT = INPUT_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>String Choice Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_CHOICE_EVENT_OPERATION_COUNT = INPUT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.events.events.EventType <em>Event Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.events.events.EventType
	 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getEventType()
	 * @generated
	 */
	int EVENT_TYPE = 11;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see dk.itu.cs.isa.events.events.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.Event#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see dk.itu.cs.isa.events.events.Event#getText()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_Text();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.Event#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dk.itu.cs.isa.events.events.Event#getType()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_Type();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.ConsoleEvent <em>Console Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Console Event</em>'.
	 * @see dk.itu.cs.isa.events.events.ConsoleEvent
	 * @generated
	 */
	EClass getConsoleEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.ConsoleEvent#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see dk.itu.cs.isa.events.events.ConsoleEvent#getTimestamp()
	 * @see #getConsoleEvent()
	 * @generated
	 */
	EAttribute getConsoleEvent_Timestamp();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.GUIEvent <em>GUI Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>GUI Event</em>'.
	 * @see dk.itu.cs.isa.events.events.GUIEvent
	 * @generated
	 */
	EClass getGUIEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.GUIEvent#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see dk.itu.cs.isa.events.events.GUIEvent#getTitle()
	 * @see #getGUIEvent()
	 * @generated
	 */
	EAttribute getGUIEvent_Title();

	/**
	 * Returns the meta object for the attribute list '{@link dk.itu.cs.isa.events.events.GUIEvent#getButtonLabels <em>Button Labels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Button Labels</em>'.
	 * @see dk.itu.cs.isa.events.events.GUIEvent#getButtonLabels()
	 * @see #getGUIEvent()
	 * @generated
	 */
	EAttribute getGUIEvent_ButtonLabels();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.GUIEvent#getReturnCode <em>Return Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Return Code</em>'.
	 * @see dk.itu.cs.isa.events.events.GUIEvent#getReturnCode()
	 * @see #getGUIEvent()
	 * @generated
	 */
	EAttribute getGUIEvent_ReturnCode();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.OutputEvent <em>Output Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Output Event</em>'.
	 * @see dk.itu.cs.isa.events.events.OutputEvent
	 * @generated
	 */
	EClass getOutputEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.OutputEvent#getCause <em>Cause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cause</em>'.
	 * @see dk.itu.cs.isa.events.events.OutputEvent#getCause()
	 * @see #getOutputEvent()
	 * @generated
	 */
	EAttribute getOutputEvent_Cause();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.AnalysisSuccessEvent <em>Analysis Success Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Analysis Success Event</em>'.
	 * @see dk.itu.cs.isa.events.events.AnalysisSuccessEvent
	 * @generated
	 */
	EClass getAnalysisSuccessEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.AnalysisSuccessEvent#getCalculationTime <em>Calculation Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calculation Time</em>'.
	 * @see dk.itu.cs.isa.events.events.AnalysisSuccessEvent#getCalculationTime()
	 * @see #getAnalysisSuccessEvent()
	 * @generated
	 */
	EAttribute getAnalysisSuccessEvent_CalculationTime();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.InputEvent <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Input Event</em>'.
	 * @see dk.itu.cs.isa.events.events.InputEvent
	 * @generated
	 */
	EClass getInputEvent();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.AnalysisSetupEvent <em>Analysis Setup Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Analysis Setup Event</em>'.
	 * @see dk.itu.cs.isa.events.events.AnalysisSetupEvent
	 * @generated
	 */
	EClass getAnalysisSetupEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.AnalysisSetupEvent#getSystemName <em>System Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Name</em>'.
	 * @see dk.itu.cs.isa.events.events.AnalysisSetupEvent#getSystemName()
	 * @see #getAnalysisSetupEvent()
	 * @generated
	 */
	EAttribute getAnalysisSetupEvent_SystemName();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.BooleanInputEvent <em>Boolean Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Input Event</em>'.
	 * @see dk.itu.cs.isa.events.events.BooleanInputEvent
	 * @generated
	 */
	EClass getBooleanInputEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.BooleanInputEvent#isUserInput <em>User Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>User Input</em>'.
	 * @see dk.itu.cs.isa.events.events.BooleanInputEvent#isUserInput()
	 * @see #getBooleanInputEvent()
	 * @generated
	 */
	EAttribute getBooleanInputEvent_UserInput();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.IntInputEvent <em>Int Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Input Event</em>'.
	 * @see dk.itu.cs.isa.events.events.IntInputEvent
	 * @generated
	 */
	EClass getIntInputEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.IntInputEvent#getUserInput <em>User Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>User Input</em>'.
	 * @see dk.itu.cs.isa.events.events.IntInputEvent#getUserInput()
	 * @see #getIntInputEvent()
	 * @generated
	 */
	EAttribute getIntInputEvent_UserInput();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.StringInputEvent <em>String Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Input Event</em>'.
	 * @see dk.itu.cs.isa.events.events.StringInputEvent
	 * @generated
	 */
	EClass getStringInputEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.StringInputEvent#getUserInput <em>User Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>User Input</em>'.
	 * @see dk.itu.cs.isa.events.events.StringInputEvent#getUserInput()
	 * @see #getStringInputEvent()
	 * @generated
	 */
	EAttribute getStringInputEvent_UserInput();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.events.events.StringChoiceEvent <em>String Choice Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Choice Event</em>'.
	 * @see dk.itu.cs.isa.events.events.StringChoiceEvent
	 * @generated
	 */
	EClass getStringChoiceEvent();

	/**
	 * Returns the meta object for the attribute list '{@link dk.itu.cs.isa.events.events.StringChoiceEvent#getOptions <em>Options</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Options</em>'.
	 * @see dk.itu.cs.isa.events.events.StringChoiceEvent#getOptions()
	 * @see #getStringChoiceEvent()
	 * @generated
	 */
	EAttribute getStringChoiceEvent_Options();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.events.events.StringChoiceEvent#getSelection <em>Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selection</em>'.
	 * @see dk.itu.cs.isa.events.events.StringChoiceEvent#getSelection()
	 * @see #getStringChoiceEvent()
	 * @generated
	 */
	EAttribute getStringChoiceEvent_Selection();

	/**
	 * Returns the meta object for enum '{@link dk.itu.cs.isa.events.events.EventType <em>Event Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Event Type</em>'.
	 * @see dk.itu.cs.isa.events.events.EventType
	 * @generated
	 */
	EEnum getEventType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EventsFactory getEventsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.EventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__TEXT = eINSTANCE.getEvent_Text();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__TYPE = eINSTANCE.getEvent_Type();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.ConsoleEventImpl <em>Console Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.ConsoleEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getConsoleEvent()
		 * @generated
		 */
		EClass CONSOLE_EVENT = eINSTANCE.getConsoleEvent();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSOLE_EVENT__TIMESTAMP = eINSTANCE.getConsoleEvent_Timestamp();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.GUIEventImpl <em>GUI Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.GUIEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getGUIEvent()
		 * @generated
		 */
		EClass GUI_EVENT = eINSTANCE.getGUIEvent();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUI_EVENT__TITLE = eINSTANCE.getGUIEvent_Title();

		/**
		 * The meta object literal for the '<em><b>Button Labels</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUI_EVENT__BUTTON_LABELS = eINSTANCE.getGUIEvent_ButtonLabels();

		/**
		 * The meta object literal for the '<em><b>Return Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUI_EVENT__RETURN_CODE = eINSTANCE.getGUIEvent_ReturnCode();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.OutputEventImpl <em>Output Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.OutputEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getOutputEvent()
		 * @generated
		 */
		EClass OUTPUT_EVENT = eINSTANCE.getOutputEvent();

		/**
		 * The meta object literal for the '<em><b>Cause</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT_EVENT__CAUSE = eINSTANCE.getOutputEvent_Cause();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.AnalysisSuccessEventImpl <em>Analysis Success Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.AnalysisSuccessEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getAnalysisSuccessEvent()
		 * @generated
		 */
		EClass ANALYSIS_SUCCESS_EVENT = eINSTANCE.getAnalysisSuccessEvent();

		/**
		 * The meta object literal for the '<em><b>Calculation Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_SUCCESS_EVENT__CALCULATION_TIME = eINSTANCE.getAnalysisSuccessEvent_CalculationTime();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.InputEventImpl <em>Input Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.InputEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getInputEvent()
		 * @generated
		 */
		EClass INPUT_EVENT = eINSTANCE.getInputEvent();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.AnalysisSetupEventImpl <em>Analysis Setup Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.AnalysisSetupEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getAnalysisSetupEvent()
		 * @generated
		 */
		EClass ANALYSIS_SETUP_EVENT = eINSTANCE.getAnalysisSetupEvent();

		/**
		 * The meta object literal for the '<em><b>System Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANALYSIS_SETUP_EVENT__SYSTEM_NAME = eINSTANCE.getAnalysisSetupEvent_SystemName();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.BooleanInputEventImpl <em>Boolean Input Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.BooleanInputEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getBooleanInputEvent()
		 * @generated
		 */
		EClass BOOLEAN_INPUT_EVENT = eINSTANCE.getBooleanInputEvent();

		/**
		 * The meta object literal for the '<em><b>User Input</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_INPUT_EVENT__USER_INPUT = eINSTANCE.getBooleanInputEvent_UserInput();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.IntInputEventImpl <em>Int Input Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.IntInputEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getIntInputEvent()
		 * @generated
		 */
		EClass INT_INPUT_EVENT = eINSTANCE.getIntInputEvent();

		/**
		 * The meta object literal for the '<em><b>User Input</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_INPUT_EVENT__USER_INPUT = eINSTANCE.getIntInputEvent_UserInput();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.StringInputEventImpl <em>String Input Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.StringInputEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getStringInputEvent()
		 * @generated
		 */
		EClass STRING_INPUT_EVENT = eINSTANCE.getStringInputEvent();

		/**
		 * The meta object literal for the '<em><b>User Input</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_INPUT_EVENT__USER_INPUT = eINSTANCE.getStringInputEvent_UserInput();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.impl.StringChoiceEventImpl <em>String Choice Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.impl.StringChoiceEventImpl
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getStringChoiceEvent()
		 * @generated
		 */
		EClass STRING_CHOICE_EVENT = eINSTANCE.getStringChoiceEvent();

		/**
		 * The meta object literal for the '<em><b>Options</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_CHOICE_EVENT__OPTIONS = eINSTANCE.getStringChoiceEvent_Options();

		/**
		 * The meta object literal for the '<em><b>Selection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_CHOICE_EVENT__SELECTION = eINSTANCE.getStringChoiceEvent_Selection();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.events.events.EventType <em>Event Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.events.events.EventType
		 * @see dk.itu.cs.isa.events.events.impl.EventsPackageImpl#getEventType()
		 * @generated
		 */
		EEnum EVENT_TYPE = eINSTANCE.getEventType();

	}

} //EventsPackage

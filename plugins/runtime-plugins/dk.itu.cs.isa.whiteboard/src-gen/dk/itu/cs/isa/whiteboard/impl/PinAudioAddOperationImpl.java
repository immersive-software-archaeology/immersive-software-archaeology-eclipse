/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.PinAudioAddOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pin Audio Add Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinAudioAddOperationImpl#getSoundWaveBase64 <em>Sound Wave Base64</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinAudioAddOperationImpl#getSoundWaveFrequency <em>Sound Wave Frequency</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PinAudioAddOperationImpl extends AbstractPinOperationImpl implements PinAudioAddOperation {
	/**
	 * The default value of the '{@link #getSoundWaveBase64() <em>Sound Wave Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoundWaveBase64()
	 * @generated
	 * @ordered
	 */
	protected static final String SOUND_WAVE_BASE64_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSoundWaveBase64() <em>Sound Wave Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoundWaveBase64()
	 * @generated
	 * @ordered
	 */
	protected String soundWaveBase64 = SOUND_WAVE_BASE64_EDEFAULT;

	/**
	 * The default value of the '{@link #getSoundWaveFrequency() <em>Sound Wave Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoundWaveFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final int SOUND_WAVE_FREQUENCY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSoundWaveFrequency() <em>Sound Wave Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoundWaveFrequency()
	 * @generated
	 * @ordered
	 */
	protected int soundWaveFrequency = SOUND_WAVE_FREQUENCY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PinAudioAddOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PIN_AUDIO_ADD_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSoundWaveBase64() {
		return soundWaveBase64;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoundWaveBase64(String newSoundWaveBase64) {
		String oldSoundWaveBase64 = soundWaveBase64;
		soundWaveBase64 = newSoundWaveBase64;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_BASE64, oldSoundWaveBase64, soundWaveBase64));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSoundWaveFrequency() {
		return soundWaveFrequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoundWaveFrequency(int newSoundWaveFrequency) {
		int oldSoundWaveFrequency = soundWaveFrequency;
		soundWaveFrequency = newSoundWaveFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_FREQUENCY, oldSoundWaveFrequency, soundWaveFrequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_BASE64:
				return getSoundWaveBase64();
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_FREQUENCY:
				return getSoundWaveFrequency();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_BASE64:
				setSoundWaveBase64((String)newValue);
				return;
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_FREQUENCY:
				setSoundWaveFrequency((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_BASE64:
				setSoundWaveBase64(SOUND_WAVE_BASE64_EDEFAULT);
				return;
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_FREQUENCY:
				setSoundWaveFrequency(SOUND_WAVE_FREQUENCY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_BASE64:
				return SOUND_WAVE_BASE64_EDEFAULT == null ? soundWaveBase64 != null : !SOUND_WAVE_BASE64_EDEFAULT.equals(soundWaveBase64);
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_FREQUENCY:
				return soundWaveFrequency != SOUND_WAVE_FREQUENCY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (soundWaveBase64: ");
		result.append(soundWaveBase64);
		result.append(", soundWaveFrequency: ");
		result.append(soundWaveFrequency);
		result.append(')');
		return result.toString();
	}

} //PinAudioAddOperationImpl

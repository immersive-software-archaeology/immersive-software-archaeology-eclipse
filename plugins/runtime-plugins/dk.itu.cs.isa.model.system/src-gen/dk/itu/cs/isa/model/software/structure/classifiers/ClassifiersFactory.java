/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage
 * @generated
 */
public interface ClassifiersFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassifiersFactory eINSTANCE = dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ISA Abstract Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Abstract Class</em>'.
	 * @generated
	 */
	ISAAbstractClass createISAAbstractClass();

	/**
	 * Returns a new object of class '<em>ISA Concrete Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Concrete Class</em>'.
	 * @generated
	 */
	ISAConcreteClass createISAConcreteClass();

	/**
	 * Returns a new object of class '<em>ISA Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Interface</em>'.
	 * @generated
	 */
	ISAInterface createISAInterface();

	/**
	 * Returns a new object of class '<em>ISA Enum</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Enum</em>'.
	 * @generated
	 */
	ISAEnum createISAEnum();

	/**
	 * Returns a new object of class '<em>ISA Record</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Record</em>'.
	 * @generated
	 */
	ISARecord createISARecord();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ClassifiersPackage getClassifiersPackage();

} //ClassifiersFactory

/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.PhotoRemoveOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Photo Remove Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoRemoveOperationImpl#isPlayDissolveAnimation <em>Play Dissolve Animation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhotoRemoveOperationImpl extends AbstractPhotoOperationImpl implements PhotoRemoveOperation {
	/**
	 * The default value of the '{@link #isPlayDissolveAnimation() <em>Play Dissolve Animation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPlayDissolveAnimation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PLAY_DISSOLVE_ANIMATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPlayDissolveAnimation() <em>Play Dissolve Animation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPlayDissolveAnimation()
	 * @generated
	 * @ordered
	 */
	protected boolean playDissolveAnimation = PLAY_DISSOLVE_ANIMATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhotoRemoveOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PHOTO_REMOVE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPlayDissolveAnimation() {
		return playDissolveAnimation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlayDissolveAnimation(boolean newPlayDissolveAnimation) {
		boolean oldPlayDissolveAnimation = playDissolveAnimation;
		playDissolveAnimation = newPlayDissolveAnimation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO_REMOVE_OPERATION__PLAY_DISSOLVE_ANIMATION, oldPlayDissolveAnimation, playDissolveAnimation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO_REMOVE_OPERATION__PLAY_DISSOLVE_ANIMATION:
				return isPlayDissolveAnimation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO_REMOVE_OPERATION__PLAY_DISSOLVE_ANIMATION:
				setPlayDissolveAnimation((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO_REMOVE_OPERATION__PLAY_DISSOLVE_ANIMATION:
				setPlayDissolveAnimation(PLAY_DISSOLVE_ANIMATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO_REMOVE_OPERATION__PLAY_DISSOLVE_ANIMATION:
				return playDissolveAnimation != PLAY_DISSOLVE_ANIMATION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (playDissolveAnimation: ");
		result.append(playDissolveAnimation);
		result.append(')');
		return result.toString();
	}

} //PhotoRemoveOperationImpl

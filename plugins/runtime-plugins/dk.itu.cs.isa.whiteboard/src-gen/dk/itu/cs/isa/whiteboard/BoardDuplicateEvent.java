/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Board Duplicate Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.BoardDuplicateEvent#getWorldSpacePosition <em>World Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.BoardDuplicateEvent#getWorldSpaceRotation <em>World Space Rotation</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getBoardDuplicateEvent()
 * @model
 * @generated
 */
public interface BoardDuplicateEvent extends AbstractWhiteboardEvent {
	/**
	 * Returns the value of the '<em><b>World Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>World Space Position</em>' containment reference.
	 * @see #setWorldSpacePosition(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getBoardDuplicateEvent_WorldSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getWorldSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.BoardDuplicateEvent#getWorldSpacePosition <em>World Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>World Space Position</em>' containment reference.
	 * @see #getWorldSpacePosition()
	 * @generated
	 */
	void setWorldSpacePosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>World Space Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>World Space Rotation</em>' containment reference.
	 * @see #setWorldSpaceRotation(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getBoardDuplicateEvent_WorldSpaceRotation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getWorldSpaceRotation();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.BoardDuplicateEvent#getWorldSpaceRotation <em>World Space Rotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>World Space Rotation</em>' containment reference.
	 * @see #getWorldSpaceRotation()
	 * @generated
	 */
	void setWorldSpaceRotation(ISAVector3 value);

} // BoardDuplicateEvent

/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pin</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Pin#getId <em>Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Pin#getElementQualifiedName <em>Element Qualified Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Pin#getTextureSpacePosition <em>Texture Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Pin#getLocalSpacePosition <em>Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Pin#getColor <em>Color</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Pin#getWhiteboard <em>Whiteboard</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPin()
 * @model
 * @generated
 */
public interface Pin extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPin_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Pin#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Element Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Qualified Name</em>' attribute.
	 * @see #setElementQualifiedName(String)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPin_ElementQualifiedName()
	 * @model required="true"
	 * @generated
	 */
	String getElementQualifiedName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Pin#getElementQualifiedName <em>Element Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Qualified Name</em>' attribute.
	 * @see #getElementQualifiedName()
	 * @generated
	 */
	void setElementQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Texture Space Position</em>' containment reference.
	 * @see #setTextureSpacePosition(ISAVector2)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPin_TextureSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector2 getTextureSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Pin#getTextureSpacePosition <em>Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Texture Space Position</em>' containment reference.
	 * @see #getTextureSpacePosition()
	 * @generated
	 */
	void setTextureSpacePosition(ISAVector2 value);

	/**
	 * Returns the value of the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Space Position</em>' containment reference.
	 * @see #setLocalSpacePosition(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPin_LocalSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getLocalSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Pin#getLocalSpacePosition <em>Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Space Position</em>' containment reference.
	 * @see #getLocalSpacePosition()
	 * @generated
	 */
	void setLocalSpacePosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' containment reference.
	 * @see #setColor(ISAColor)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPin_Color()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAColor getColor();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Pin#getColor <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' containment reference.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(ISAColor value);

	/**
	 * Returns the value of the '<em><b>Whiteboard</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.whiteboard.Whiteboard#getPins <em>Pins</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Whiteboard</em>' container reference.
	 * @see #setWhiteboard(Whiteboard)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPin_Whiteboard()
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard#getPins
	 * @model opposite="pins" required="true" transient="false"
	 * @generated
	 */
	Whiteboard getWhiteboard();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Pin#getWhiteboard <em>Whiteboard</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Whiteboard</em>' container reference.
	 * @see #getWhiteboard()
	 * @generated
	 */
	void setWhiteboard(Whiteboard value);

} // Pin

/**
 */
package dk.itu.cs.isa.model.software.structure.misc;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAParameter#isVarArg <em>Var Arg</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAParameter()
 * @model
 * @generated
 */
public interface ISAParameter extends ISANamedSoftwareElement, ISATypedElement {

	/**
	 * Returns the value of the '<em><b>Var Arg</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Arg</em>' attribute.
	 * @see #setVarArg(boolean)
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAParameter_VarArg()
	 * @model required="true"
	 * @generated
	 */
	boolean isVarArg();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAParameter#isVarArg <em>Var Arg</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Arg</em>' attribute.
	 * @see #isVarArg()
	 * @generated
	 */
	void setVarArg(boolean value);
} // ISAParameter

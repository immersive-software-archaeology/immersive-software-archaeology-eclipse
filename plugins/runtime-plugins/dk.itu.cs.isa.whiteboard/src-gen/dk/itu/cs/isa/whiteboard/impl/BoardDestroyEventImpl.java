/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.BoardDestroyEvent;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Board Destroy Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BoardDestroyEventImpl extends AbstractWhiteboardEventImpl implements BoardDestroyEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BoardDestroyEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.BOARD_DESTROY_EVENT;
	}

} //BoardDestroyEventImpl

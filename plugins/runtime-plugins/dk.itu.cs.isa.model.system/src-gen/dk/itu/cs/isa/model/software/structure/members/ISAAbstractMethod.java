/**
 */
package dk.itu.cs.isa.model.software.structure.members;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Abstract Method</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAAbstractMethod()
 * @model
 * @generated
 */
public interface ISAAbstractMethod extends ISAMethod {
} // ISAAbstractMethod

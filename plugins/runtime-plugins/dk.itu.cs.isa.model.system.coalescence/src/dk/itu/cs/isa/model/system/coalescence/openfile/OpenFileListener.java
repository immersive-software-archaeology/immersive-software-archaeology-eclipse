package dk.itu.cs.isa.model.system.coalescence.openfile;

import org.eclipse.core.resources.IFile;

public interface OpenFileListener {
	
	public boolean openFileInEditor(IFile file);
	
}

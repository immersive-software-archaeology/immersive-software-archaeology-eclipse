/**
 */
package dk.itu.cs.isa.multiplayer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Clear Relations Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ClearRelationsEvent#getElementName <em>Element Name</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getClearRelationsEvent()
 * @model
 * @generated
 */
public interface ClearRelationsEvent extends AbstractEvent {
	/**
	 * Returns the value of the '<em><b>Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Name</em>' attribute.
	 * @see #setElementName(String)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getClearRelationsEvent_ElementName()
	 * @model required="true"
	 * @generated
	 */
	String getElementName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ClearRelationsEvent#getElementName <em>Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Name</em>' attribute.
	 * @see #getElementName()
	 * @generated
	 */
	void setElementName(String value);

} // ClearRelationsEvent

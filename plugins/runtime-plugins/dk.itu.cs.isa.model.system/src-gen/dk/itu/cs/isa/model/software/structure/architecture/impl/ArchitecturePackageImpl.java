/**
 */
package dk.itu.cs.isa.model.software.structure.architecture.impl;

import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory;
import dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;

import dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;

import dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl;

import dk.itu.cs.isa.model.software.structure.impl.StructurePackageImpl;

import dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage;

import dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl;

import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ArchitecturePackageImpl extends EPackageImpl implements ArchitecturePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaClusterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaWordOccurrenceEntryEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ArchitecturePackageImpl() {
		super(eNS_URI, ArchitectureFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ArchitecturePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ArchitecturePackage init() {
		if (isInited) return (ArchitecturePackage)EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredArchitecturePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ArchitecturePackageImpl theArchitecturePackage = registeredArchitecturePackage instanceof ArchitecturePackageImpl ? (ArchitecturePackageImpl)registeredArchitecturePackage : new ArchitecturePackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);
		StructurePackageImpl theStructurePackage = (StructurePackageImpl)(registeredPackage instanceof StructurePackageImpl ? registeredPackage : StructurePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FilesystemPackage.eNS_URI);
		FilesystemPackageImpl theFilesystemPackage = (FilesystemPackageImpl)(registeredPackage instanceof FilesystemPackageImpl ? registeredPackage : FilesystemPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LibrariesPackage.eNS_URI);
		LibrariesPackageImpl theLibrariesPackage = (LibrariesPackageImpl)(registeredPackage instanceof LibrariesPackageImpl ? registeredPackage : LibrariesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl)(registeredPackage instanceof ClassifiersPackageImpl ? registeredPackage : ClassifiersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);
		MembersPackageImpl theMembersPackage = (MembersPackageImpl)(registeredPackage instanceof MembersPackageImpl ? registeredPackage : MembersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);
		MiscPackageImpl theMiscPackage = (MiscPackageImpl)(registeredPackage instanceof MiscPackageImpl ? registeredPackage : MiscPackage.eINSTANCE);

		// Create package meta-data objects
		theArchitecturePackage.createPackageContents();
		theStructurePackage.createPackageContents();
		theFilesystemPackage.createPackageContents();
		theLibrariesPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theMembersPackage.createPackageContents();
		theMiscPackage.createPackageContents();

		// Initialize created meta-data
		theArchitecturePackage.initializePackageContents();
		theStructurePackage.initializePackageContents();
		theFilesystemPackage.initializePackageContents();
		theLibrariesPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theMembersPackage.initializePackageContents();
		theMiscPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theArchitecturePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ArchitecturePackage.eNS_URI, theArchitecturePackage);
		return theArchitecturePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISACluster() {
		return isaClusterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISACluster_ChildClusters() {
		return (EReference)isaClusterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISACluster_Classifiers() {
		return (EReference)isaClusterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISACluster_TopWordOccurrences() {
		return (EReference)isaClusterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAWordOccurrenceEntry() {
		return isaWordOccurrenceEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAWordOccurrenceEntry_Word() {
		return (EAttribute)isaWordOccurrenceEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAWordOccurrenceEntry_Count() {
		return (EAttribute)isaWordOccurrenceEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ArchitectureFactory getArchitectureFactory() {
		return (ArchitectureFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaClusterEClass = createEClass(ISA_CLUSTER);
		createEReference(isaClusterEClass, ISA_CLUSTER__CHILD_CLUSTERS);
		createEReference(isaClusterEClass, ISA_CLUSTER__CLASSIFIERS);
		createEReference(isaClusterEClass, ISA_CLUSTER__TOP_WORD_OCCURRENCES);

		isaWordOccurrenceEntryEClass = createEClass(ISA_WORD_OCCURRENCE_ENTRY);
		createEAttribute(isaWordOccurrenceEntryEClass, ISA_WORD_OCCURRENCE_ENTRY__WORD);
		createEAttribute(isaWordOccurrenceEntryEClass, ISA_WORD_OCCURRENCE_ENTRY__COUNT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MiscPackage theMiscPackage = (MiscPackage)EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage)EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		isaClusterEClass.getESuperTypes().add(theMiscPackage.getISANamedSoftwareElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(isaClusterEClass, ISACluster.class, "ISACluster", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISACluster_ChildClusters(), this.getISACluster(), null, "childClusters", null, 0, -1, ISACluster.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISACluster_Classifiers(), theClassifiersPackage.getISAClassifier(), null, "classifiers", null, 0, -1, ISACluster.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISACluster_TopWordOccurrences(), this.getISAWordOccurrenceEntry(), null, "topWordOccurrences", null, 0, -1, ISACluster.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaWordOccurrenceEntryEClass, ISAWordOccurrenceEntry.class, "ISAWordOccurrenceEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAWordOccurrenceEntry_Word(), ecorePackage.getEString(), "word", null, 1, 1, ISAWordOccurrenceEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAWordOccurrenceEntry_Count(), ecorePackage.getEInt(), "count", null, 1, 1, ISAWordOccurrenceEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //ArchitecturePackageImpl

/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.PinSoftwareAddOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pin Software Add Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PinSoftwareAddOperationImpl extends AbstractPinOperationImpl implements PinSoftwareAddOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PinSoftwareAddOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PIN_SOFTWARE_ADD_OPERATION;
	}

} //PinSoftwareAddOperationImpl

package dk.itu.cs.isa.whiteboard.requests;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.util.EcoreUtilISA;
import dk.itu.cs.isa.whiteboard.AbstractPhotoOperation;
import dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent;
import dk.itu.cs.isa.whiteboard.PhotoMoveOperation;
import dk.itu.cs.isa.whiteboard.PhotoRemoveOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardHistoryUtil;
import dk.itu.cs.isa.whiteboard.WhiteboardStorage;

public class WhiteboardSynchronizationFetchRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		String latestUpdateTimestampString = baseRequest.getParameter("latestUpdateTimestamp");
		long latestUpdateTimestamp;
		try {
			latestUpdateTimestamp = Long.parseLong(latestUpdateTimestampString);
		} catch(NumberFormatException e) {
			latestUpdateTimestamp = 0;
		}
		
		if(systemName == null) {
			String examplePath1 = "systemName=JetUML&latestUpdateTimestamp=" + System.currentTimeMillis();
			String explanation = "You can specify a timestamp to pull only events that happened after the time point it specifies";
			printInvalidParametersHTMLPage(baseRequest, response, explanation, examplePath1);
			return;
		}

		PrintWriter out = response.getWriter();
		try {
			response.setContentType("application/xml; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_OK);
			
			WhiteboardHistory history = WhiteboardStorage.INSTANCE.getWhiteboardHistory(systemName);
			WhiteboardHistoryUtil.updateWhiteboardPositionEvents(history);
			
			WhiteboardHistory copyOfHistory = EcoreUtil.copy(history);
			WhiteboardHistoryUtil.cleanUpObsoleteEntries(copyOfHistory, latestUpdateTimestamp);
			
			removeInformationNotRequiredByClients(copyOfHistory);

			out.println(EcoreUtilISA.getXMLStringForModel(copyOfHistory));
			baseRequest.setHandled(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_CONFLICT, "Error", e, "The whiteboard could not be synchronized");
			baseRequest.setHandled(true);
		}
		finally {
			out.close();
		}
	}

	private void removeInformationNotRequiredByClients(WhiteboardHistory history) {
		// Remove the whiteboard information, it is not needed
		history.getWhiteboards().clear();
		
		for(AbstractWhiteboardEvent event : history.getEventLog()) {
			if(event instanceof PhotoMoveOperation) {
				((PhotoMoveOperation) event).setBitmapBase64(null);
				((PhotoMoveOperation) event).setPreTextureSpacePosition(null);
				((PhotoMoveOperation) event).setPreLocalSpacePosition(null);
				((PhotoMoveOperation) event).setPreLocalSpaceForwardDirection(null);
				((PhotoMoveOperation) event).setPreLocalSpaceUpwardDirection(null);
			}
			else if(event instanceof PhotoRemoveOperation) {
				((AbstractPhotoOperation) event).setBitmapBase64(null);
			}
		}
	}

}










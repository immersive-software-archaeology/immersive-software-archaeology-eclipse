package dk.itu.cs.isa.whiteboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.eclipse.emf.ecore.util.EcoreUtil;

public class WhiteboardHistoryUtil {
	
	public static void updateWhiteboardPositionEvents(WhiteboardHistory history) {
		for(AbstractWhiteboardEvent ev : history.getEventLog()) {
			if(ev instanceof BoardSpawnEvent)
				updateSpawnEvent(history, (BoardSpawnEvent) ev);
		}
	}
	
	private static void updateSpawnEvent(WhiteboardHistory history, BoardSpawnEvent event) {
		Whiteboard board = WhiteboardHistoryUtil.getWhiteboardFromHistory(history, event.getWhiteboardId());
		if(board == null)
			// Could not find board for this operation.
			// It might have been deleted in the meantime.
			return;

		((BoardSpawnEvent)event).setWorldSpacePosition(EcoreUtil.copy(board.getPosition()));
		((BoardSpawnEvent)event).setWorldSpaceRotation(EcoreUtil.copy(board.getRotation()));
	}
	
	
	
	public static boolean cleanUpObsoleteEntries(WhiteboardHistory history, long latestUpdateTimestamp) {
		boolean dirtyFlag = false;
		
		// Get rid of obsolete entries for destroyed boards
		{
			// Gather all deleted boards and mark them so that we can later remove all events on them
			List<Integer> deletedBoards = new ArrayList<>();
			for(AbstractWhiteboardEvent event : history.getEventLog())
				if(event instanceof BoardDestroyEvent)
					deletedBoards.add(event.getWhiteboardId());
			
			// However, do not remove events for boards that were synced with a client already
			for(AbstractWhiteboardEvent event : history.getEventLog())
				if(event instanceof BoardSpawnEvent)
					if(wasSyncedWithClientAlready(event, latestUpdateTimestamp))
						deletedBoards.remove((Object) event.getWhiteboardId());
			
			// Now, the list of board to remove events for is set up, delete the events 
			for(int i=0; i<history.getEventLog().size(); i++) {
				AbstractWhiteboardEvent event = history.getEventLog().get(i);
				if(deletedBoards.contains(event.getWhiteboardId())) {
					history.getEventLog().remove(i--);
					dirtyFlag = true;
				}
			}
			
			for(int id : deletedBoards) {
				Whiteboard board = WhiteboardHistoryUtil.getWhiteboardFromHistory(history, id);
				history.getWhiteboards().remove(board);
				dirtyFlag = true;
			}
		}
		
		// Get rid of obsolete undo/redo events
		{
			int undoDepth = 0;
			Map<Integer, Stack<UndoEvent>> undoEventStacks = new HashMap<>();
			List<AbstractWhiteboardEvent> eventsToRemove = new ArrayList<>();
			
			for(AbstractWhiteboardEvent event : history.getEventLog()) {
				Stack<UndoEvent> stackOfCurrentlyOpenUndoOperations = undoEventStacks.get(event.getWhiteboardId());
				if(stackOfCurrentlyOpenUndoOperations == null) {
					stackOfCurrentlyOpenUndoOperations = new Stack<>();
					undoEventStacks.put(event.getWhiteboardId(), stackOfCurrentlyOpenUndoOperations);
				}
				
				if(event instanceof UndoEvent) {
					undoDepth ++;
					stackOfCurrentlyOpenUndoOperations.add((UndoEvent)event);
				}
				else if(event instanceof RedoEvent) {
					undoDepth --;
					if(undoDepth < 0)
						throw new RuntimeException("Something is wrong with the undo stack! "
								+ "More redo operations were scheduled than undo operations were perviously performed.");
					
					UndoEvent undoEvent = stackOfCurrentlyOpenUndoOperations.pop();
					if(!wasSyncedWithClientAlready(event, latestUpdateTimestamp) && !wasSyncedWithClientAlready(undoEvent, latestUpdateTimestamp)) {
						eventsToRemove.add(event);
						eventsToRemove.add(undoEvent);
					}
				}
				else if(event instanceof AbstractOperation) {
					undoDepth = 0;
					int numberOfOperationsToRemove = stackOfCurrentlyOpenUndoOperations.size();
					if(numberOfOperationsToRemove > 0) {
						int overwritingOperationIndex = history.getEventLog().indexOf(event);
						for (int i = overwritingOperationIndex - numberOfOperationsToRemove - 1; i >= overwritingOperationIndex - numberOfOperationsToRemove * 2; i--) {
							if(!wasSyncedWithClientAlready(history.getEventLog().get(i), latestUpdateTimestamp)) {
								eventsToRemove.add(history.getEventLog().get(i));
							}
						}
					}
					while(!stackOfCurrentlyOpenUndoOperations.isEmpty()) {
						UndoEvent undoEvent = stackOfCurrentlyOpenUndoOperations.pop();
						if(!wasSyncedWithClientAlready(undoEvent, latestUpdateTimestamp))
							eventsToRemove.add(undoEvent);
					}
				}
			}
			
			if(history.getEventLog().removeAll(eventsToRemove))
				dirtyFlag = true;
		}
		
		// remove all events that were synced already
		for(int i=0; i<history.getEventLog().size(); i++) {
			AbstractWhiteboardEvent event = history.getEventLog().get(i);
			if(wasSyncedWithClientAlready(event, latestUpdateTimestamp)) {
				history.getEventLog().remove(i--);
				dirtyFlag = true;
			}
		}
		
		return dirtyFlag;
	}
	
	public static boolean wasSyncedWithClientAlready(AbstractWhiteboardEvent event, long latestUpdateTimestamp) {
		return event.getTimestamp() <= latestUpdateTimestamp;
	}
	

	
	public static Whiteboard getWhiteboardFromHistory(WhiteboardHistory history, int whiteboardId) {
		for(Whiteboard b : history.getWhiteboards())
			if(b.getId() == whiteboardId)
				return b;
		return null;
	}



	public static Pin getPinOnWhiteboard(Whiteboard board, String pinnedElementName) {
		for(Pin p : board.getPins())
			if(p.getElementQualifiedName() != null)
				if(p.getElementQualifiedName().equals(pinnedElementName))
					return p;
		return null;
	}

	public static Pin getPinOnWhiteboard(Whiteboard board, int pinId) {
		for(Pin p : board.getPins())
			if(p.getId() == pinId)
				return p;
		return null;
	}
	

	
	public static Pin addPin(Whiteboard whiteboard, int pinId, String pinnedElementName, ISAColor color, ISAVector2 textureSpacePosition, ISAVector3 localSpacePosition) {
		if(pinId <= 0)
			throw new RuntimeException("No pin id provided!");
		
		Pin newPin = WhiteboardFactory.eINSTANCE.createPin();
		newPin.setId(pinId);
		newPin.setElementQualifiedName(pinnedElementName);
		newPin.setTextureSpacePosition(textureSpacePosition);
		newPin.setLocalSpacePosition(localSpacePosition);
		newPin.setColor(color);
		whiteboard.getPins().add(newPin);
		return newPin;
	}
	
	public static Pin addPin(Whiteboard whiteboard, int pinId, String pinnedElementName, ISAColor color, ISAVector2 textureSpacePosition, ISAVector3 localSpacePosition, String audioBase64) {
		Pin newPin = addPin(whiteboard, pinId, pinnedElementName, color, textureSpacePosition, localSpacePosition);
		//newPin.setSoundWaveBase64(audioBase64);
		return newPin;
	}
	
	public static boolean removePin(Whiteboard whiteboard, int pinId) {
		Pin pin = getPinOnWhiteboard(whiteboard, pinId);
		return whiteboard.getPins().remove(pin);
	}



	public static Photo getPhotoOnWhiteboard(Whiteboard board, int photoId) {
		for(Photo p : board.getPhotos())
			if(p.getId() == photoId)
				return p;
		return null;
	}
	
	public static Photo addPhoto(Whiteboard whiteboard, int potoId, String bitmapBase64, ISAVector2 textureSpacePosition,
			ISAVector3 localSpacePosition, ISAVector3 localSpaceForward, ISAVector3 localSpaceUp/*,
			ISAVector3 systemPosition, float systemSize, Collection<VisibleRelation> visibleRelations*/) {
		if(potoId <= 0)
			throw new RuntimeException("No photo id provided!");
		
		Photo newPhoto = WhiteboardFactory.eINSTANCE.createPhoto();
		newPhoto.setId(potoId);
		newPhoto.setBitmapBase64(bitmapBase64);
		
		newPhoto.setTextureSpacePosition(textureSpacePosition);
		newPhoto.setLocalSpacePosition(localSpacePosition);
		newPhoto.setLocalSpaceForwardDirection(localSpaceForward);
		newPhoto.setLocalSpaceUpwardDirection(localSpaceUp);
		
//		newPhoto.setSystemPositionWhenPhotoWasTaken(systemPosition);
//		newPhoto.setSystemSizeWhenPhotoWasTaken(systemSize);
//		
//		newPhoto.getRelationsVisibleWhenPhotoWasTaken().addAll(visibleRelations);
		
		whiteboard.getPhotos().add(newPhoto);
		return newPhoto;
	}
	
	public static boolean removePhoto(Whiteboard whiteboard, int photoId) {
		Photo photo = getPhotoOnWhiteboard(whiteboard, photoId);
		return whiteboard.getPhotos().remove(photo);
	}

}
















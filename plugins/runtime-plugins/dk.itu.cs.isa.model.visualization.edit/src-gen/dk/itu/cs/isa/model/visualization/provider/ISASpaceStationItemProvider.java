/**
 */
package dk.itu.cs.isa.model.visualization.provider;


import dk.itu.cs.isa.model.visualization.ISASpaceStation;
import dk.itu.cs.isa.model.visualization.VisualizationFactory;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.itu.cs.isa.model.visualization.ISASpaceStation} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ISASpaceStationItemProvider extends ISANamedVisualElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASpaceStationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTypePropertyDescriptor(object);
			addPublicPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISASpaceStation_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISASpaceStation_type_feature", "_UI_ISASpaceStation_type"),
				 VisualizationPackage.Literals.ISA_SPACE_STATION__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Public feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPublicPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISASpaceStation_public_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISASpaceStation_public_feature", "_UI_ISASpaceStation_type"),
				 VisualizationPackage.Literals.ISA_SPACE_STATION__PUBLIC,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VisualizationPackage.Literals.ISA_SPACE_STATION__POSITION);
			childrenFeatures.add(VisualizationPackage.Literals.ISA_SPACE_STATION__SATELLITE_STATIONS);
			childrenFeatures.add(VisualizationPackage.Literals.ISA_SPACE_STATION__BLOCKS);
			childrenFeatures.add(VisualizationPackage.Literals.ISA_SPACE_STATION__ANTENNAS);
			childrenFeatures.add(VisualizationPackage.Literals.ISA_SPACE_STATION__INHERITANCE_RELATIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ISASpaceStation.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ISASpaceStation"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ISASpaceStation)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ISASpaceStation_type") :
			getString("_UI_ISASpaceStation_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ISASpaceStation.class)) {
			case VisualizationPackage.ISA_SPACE_STATION__TYPE:
			case VisualizationPackage.ISA_SPACE_STATION__PUBLIC:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case VisualizationPackage.ISA_SPACE_STATION__POSITION:
			case VisualizationPackage.ISA_SPACE_STATION__SATELLITE_STATIONS:
			case VisualizationPackage.ISA_SPACE_STATION__BLOCKS:
			case VisualizationPackage.ISA_SPACE_STATION__ANTENNAS:
			case VisualizationPackage.ISA_SPACE_STATION__INHERITANCE_RELATIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VisualizationPackage.Literals.ISA_SPACE_STATION__POSITION,
				 VisualizationFactory.eINSTANCE.createISAVector3()));

		newChildDescriptors.add
			(createChildParameter
				(VisualizationPackage.Literals.ISA_SPACE_STATION__SATELLITE_STATIONS,
				 VisualizationFactory.eINSTANCE.createISASpaceStation()));

		newChildDescriptors.add
			(createChildParameter
				(VisualizationPackage.Literals.ISA_SPACE_STATION__BLOCKS,
				 VisualizationFactory.eINSTANCE.createISASpaceStationBlock()));

		newChildDescriptors.add
			(createChildParameter
				(VisualizationPackage.Literals.ISA_SPACE_STATION__ANTENNAS,
				 VisualizationFactory.eINSTANCE.createISASpaceStationAntenna()));

		newChildDescriptors.add
			(createChildParameter
				(VisualizationPackage.Literals.ISA_SPACE_STATION__INHERITANCE_RELATIONS,
				 VisualizationFactory.eINSTANCE.createISASpaceStationRelation()));
	}

}

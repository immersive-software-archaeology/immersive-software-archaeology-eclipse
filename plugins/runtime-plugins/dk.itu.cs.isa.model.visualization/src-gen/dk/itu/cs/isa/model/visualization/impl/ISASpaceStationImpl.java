/**
 */
package dk.itu.cs.isa.model.visualization.impl;

import dk.itu.cs.isa.model.visualization.ISASpaceStation;
import dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna;
import dk.itu.cs.isa.model.visualization.ISASpaceStationBlock;
import dk.itu.cs.isa.model.visualization.ISASpaceStationRelation;
import dk.itu.cs.isa.model.visualization.ISASpaceStationType;
import dk.itu.cs.isa.model.visualization.ISAVector3;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Space Station</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl#getSatelliteStations <em>Satellite Stations</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl#getBlocks <em>Blocks</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl#getAntennas <em>Antennas</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl#isPublic <em>Public</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl#getInheritanceRelations <em>Inheritance Relations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISASpaceStationImpl extends ISANamedVisualElementImpl implements ISASpaceStation {
	/**
	 * The cached value of the '{@link #getPosition() <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 position;

	/**
	 * The cached value of the '{@link #getSatelliteStations() <em>Satellite Stations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSatelliteStations()
	 * @generated
	 * @ordered
	 */
	protected EList<ISASpaceStation> satelliteStations;

	/**
	 * The cached value of the '{@link #getBlocks() <em>Blocks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<ISASpaceStationBlock> blocks;

	/**
	 * The cached value of the '{@link #getAntennas() <em>Antennas</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAntennas()
	 * @generated
	 * @ordered
	 */
	protected EList<ISASpaceStationAntenna> antennas;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final ISASpaceStationType TYPE_EDEFAULT = ISASpaceStationType.UNKOWN;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ISASpaceStationType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PUBLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected boolean public_ = PUBLIC_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInheritanceRelations() <em>Inheritance Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInheritanceRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<ISASpaceStationRelation> inheritanceRelations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISASpaceStationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualizationPackage.Literals.ISA_SPACE_STATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getPosition() {
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPosition(ISAVector3 newPosition, NotificationChain msgs) {
		ISAVector3 oldPosition = position;
		position = newPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION__POSITION, oldPosition, newPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(ISAVector3 newPosition) {
		if (newPosition != position) {
			NotificationChain msgs = null;
			if (position != null)
				msgs = ((InternalEObject)position).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisualizationPackage.ISA_SPACE_STATION__POSITION, null, msgs);
			if (newPosition != null)
				msgs = ((InternalEObject)newPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisualizationPackage.ISA_SPACE_STATION__POSITION, null, msgs);
			msgs = basicSetPosition(newPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION__POSITION, newPosition, newPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISASpaceStation> getSatelliteStations() {
		if (satelliteStations == null) {
			satelliteStations = new EObjectContainmentEList<ISASpaceStation>(ISASpaceStation.class, this, VisualizationPackage.ISA_SPACE_STATION__SATELLITE_STATIONS);
		}
		return satelliteStations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISASpaceStationBlock> getBlocks() {
		if (blocks == null) {
			blocks = new EObjectContainmentEList<ISASpaceStationBlock>(ISASpaceStationBlock.class, this, VisualizationPackage.ISA_SPACE_STATION__BLOCKS);
		}
		return blocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISASpaceStationAntenna> getAntennas() {
		if (antennas == null) {
			antennas = new EObjectContainmentEList<ISASpaceStationAntenna>(ISASpaceStationAntenna.class, this, VisualizationPackage.ISA_SPACE_STATION__ANTENNAS);
		}
		return antennas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASpaceStationType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(ISASpaceStationType newType) {
		ISASpaceStationType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPublic() {
		return public_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublic(boolean newPublic) {
		boolean oldPublic = public_;
		public_ = newPublic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION__PUBLIC, oldPublic, public_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISASpaceStationRelation> getInheritanceRelations() {
		if (inheritanceRelations == null) {
			inheritanceRelations = new EObjectContainmentEList<ISASpaceStationRelation>(ISASpaceStationRelation.class, this, VisualizationPackage.ISA_SPACE_STATION__INHERITANCE_RELATIONS);
		}
		return inheritanceRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION__POSITION:
				return basicSetPosition(null, msgs);
			case VisualizationPackage.ISA_SPACE_STATION__SATELLITE_STATIONS:
				return ((InternalEList<?>)getSatelliteStations()).basicRemove(otherEnd, msgs);
			case VisualizationPackage.ISA_SPACE_STATION__BLOCKS:
				return ((InternalEList<?>)getBlocks()).basicRemove(otherEnd, msgs);
			case VisualizationPackage.ISA_SPACE_STATION__ANTENNAS:
				return ((InternalEList<?>)getAntennas()).basicRemove(otherEnd, msgs);
			case VisualizationPackage.ISA_SPACE_STATION__INHERITANCE_RELATIONS:
				return ((InternalEList<?>)getInheritanceRelations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION__POSITION:
				return getPosition();
			case VisualizationPackage.ISA_SPACE_STATION__SATELLITE_STATIONS:
				return getSatelliteStations();
			case VisualizationPackage.ISA_SPACE_STATION__BLOCKS:
				return getBlocks();
			case VisualizationPackage.ISA_SPACE_STATION__ANTENNAS:
				return getAntennas();
			case VisualizationPackage.ISA_SPACE_STATION__TYPE:
				return getType();
			case VisualizationPackage.ISA_SPACE_STATION__PUBLIC:
				return isPublic();
			case VisualizationPackage.ISA_SPACE_STATION__INHERITANCE_RELATIONS:
				return getInheritanceRelations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION__POSITION:
				setPosition((ISAVector3)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION__SATELLITE_STATIONS:
				getSatelliteStations().clear();
				getSatelliteStations().addAll((Collection<? extends ISASpaceStation>)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION__BLOCKS:
				getBlocks().clear();
				getBlocks().addAll((Collection<? extends ISASpaceStationBlock>)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION__ANTENNAS:
				getAntennas().clear();
				getAntennas().addAll((Collection<? extends ISASpaceStationAntenna>)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION__TYPE:
				setType((ISASpaceStationType)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION__PUBLIC:
				setPublic((Boolean)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION__INHERITANCE_RELATIONS:
				getInheritanceRelations().clear();
				getInheritanceRelations().addAll((Collection<? extends ISASpaceStationRelation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION__POSITION:
				setPosition((ISAVector3)null);
				return;
			case VisualizationPackage.ISA_SPACE_STATION__SATELLITE_STATIONS:
				getSatelliteStations().clear();
				return;
			case VisualizationPackage.ISA_SPACE_STATION__BLOCKS:
				getBlocks().clear();
				return;
			case VisualizationPackage.ISA_SPACE_STATION__ANTENNAS:
				getAntennas().clear();
				return;
			case VisualizationPackage.ISA_SPACE_STATION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case VisualizationPackage.ISA_SPACE_STATION__PUBLIC:
				setPublic(PUBLIC_EDEFAULT);
				return;
			case VisualizationPackage.ISA_SPACE_STATION__INHERITANCE_RELATIONS:
				getInheritanceRelations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION__POSITION:
				return position != null;
			case VisualizationPackage.ISA_SPACE_STATION__SATELLITE_STATIONS:
				return satelliteStations != null && !satelliteStations.isEmpty();
			case VisualizationPackage.ISA_SPACE_STATION__BLOCKS:
				return blocks != null && !blocks.isEmpty();
			case VisualizationPackage.ISA_SPACE_STATION__ANTENNAS:
				return antennas != null && !antennas.isEmpty();
			case VisualizationPackage.ISA_SPACE_STATION__TYPE:
				return type != TYPE_EDEFAULT;
			case VisualizationPackage.ISA_SPACE_STATION__PUBLIC:
				return public_ != PUBLIC_EDEFAULT;
			case VisualizationPackage.ISA_SPACE_STATION__INHERITANCE_RELATIONS:
				return inheritanceRelations != null && !inheritanceRelations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", public: ");
		result.append(public_);
		result.append(')');
		return result.toString();
	}

} //ISASpaceStationImpl

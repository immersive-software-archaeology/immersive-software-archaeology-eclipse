/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.ISAVector2;
import dk.itu.cs.isa.whiteboard.PinMoveOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pin Move Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PinMoveOperationImpl#getPreTextureSpacePosition <em>Pre Texture Space Position</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PinMoveOperationImpl extends AbstractPinOperationImpl implements PinMoveOperation {
	/**
	 * The cached value of the '{@link #getPreTextureSpacePosition() <em>Pre Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreTextureSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector2 preTextureSpacePosition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PinMoveOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PIN_MOVE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector2 getPreTextureSpacePosition() {
		return preTextureSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreTextureSpacePosition(ISAVector2 newPreTextureSpacePosition, NotificationChain msgs) {
		ISAVector2 oldPreTextureSpacePosition = preTextureSpacePosition;
		preTextureSpacePosition = newPreTextureSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION, oldPreTextureSpacePosition, newPreTextureSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreTextureSpacePosition(ISAVector2 newPreTextureSpacePosition) {
		if (newPreTextureSpacePosition != preTextureSpacePosition) {
			NotificationChain msgs = null;
			if (preTextureSpacePosition != null)
				msgs = ((InternalEObject)preTextureSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION, null, msgs);
			if (newPreTextureSpacePosition != null)
				msgs = ((InternalEObject)newPreTextureSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION, null, msgs);
			msgs = basicSetPreTextureSpacePosition(newPreTextureSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION, newPreTextureSpacePosition, newPreTextureSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				return basicSetPreTextureSpacePosition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				return getPreTextureSpacePosition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				setPreTextureSpacePosition((ISAVector2)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				setPreTextureSpacePosition((ISAVector2)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION:
				return preTextureSpacePosition != null;
		}
		return super.eIsSet(featureID);
	}

} //PinMoveOperationImpl

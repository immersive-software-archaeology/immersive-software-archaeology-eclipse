﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.9174
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=2.0.50727.3038.
// 


namespace ISA.Modelling.Visualization
{
/// <remarks/>
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISAAbstractSpaceStationComponent))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISASpaceStationAntenna))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISASpaceStationBlock))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISASpaceStation))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISATransparentCapsule))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISAVisualizationModel))]
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISANamedVisualElement {
    
    private string nameField;
    
    private string qualifiedNameField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string name {
        get {
            return this.nameField;
        }
        set {
            this.nameField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string qualifiedName {
        get {
            return this.qualifiedNameField;
        }
        set {
            this.qualifiedNameField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISAVisualizationModel : ISANamedVisualElement {
    
    private ISATransparentCapsule subjectSystemCapsuleField;
    
    private ISATransparentCapsule[] libraryCapsulesField;
    
    private string modelCreatorNameField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISATransparentCapsule subjectSystemCapsule {
        get {
            return this.subjectSystemCapsuleField;
        }
        set {
            this.subjectSystemCapsuleField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("libraryCapsules", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISATransparentCapsule[] libraryCapsules {
        get {
            return this.libraryCapsulesField;
        }
        set {
            this.libraryCapsulesField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string modelCreatorName {
        get {
            return this.modelCreatorNameField;
        }
        set {
            this.modelCreatorNameField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISATransparentCapsule : ISANamedVisualElement {
    
    private ISATransparentCapsule[] subCapsulesField;
    
    private ISASpaceStation[] spaceStationsField;
    
    private string[] tagsField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("subCapsules", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISATransparentCapsule[] subCapsules {
        get {
            return this.subCapsulesField;
        }
        set {
            this.subCapsulesField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("spaceStations", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISASpaceStation[] spaceStations {
        get {
            return this.spaceStationsField;
        }
        set {
            this.spaceStationsField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("tags", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=true)]
    public string[] tags {
        get {
            return this.tagsField;
        }
        set {
            this.tagsField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISASpaceStation : ISANamedVisualElement {
    
    private ISAVector3 positionField;
    
    private ISASpaceStation[] satelliteStationsField;
    
    private ISASpaceStationBlock[] blocksField;
    
    private ISASpaceStationAntenna[] antennasField;
    
    private ISASpaceStationRelation[] inheritanceRelationsField;
    
    private ISASpaceStationType typeField;
    
    private bool publicField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISAVector3 position {
        get {
            return this.positionField;
        }
        set {
            this.positionField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("satelliteStations", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISASpaceStation[] satelliteStations {
        get {
            return this.satelliteStationsField;
        }
        set {
            this.satelliteStationsField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("blocks", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISASpaceStationBlock[] blocks {
        get {
            return this.blocksField;
        }
        set {
            this.blocksField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("antennas", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISASpaceStationAntenna[] antennas {
        get {
            return this.antennasField;
        }
        set {
            this.antennasField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("inheritanceRelations", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISASpaceStationRelation[] inheritanceRelations {
        get {
            return this.inheritanceRelationsField;
        }
        set {
            this.inheritanceRelationsField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public ISASpaceStationType type {
        get {
            return this.typeField;
        }
        set {
            this.typeField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool @public {
        get {
            return this.publicField;
        }
        set {
            this.publicField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISAVector3 {
    
    private float xField;
    
    private float yField;
    
    private float zField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public float x {
        get {
            return this.xField;
        }
        set {
            this.xField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public float y {
        get {
            return this.yField;
        }
        set {
            this.yField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public float z {
        get {
            return this.zField;
        }
        set {
            this.zField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISASpaceStationBlock : ISAAbstractSpaceStationComponent {
    
    private float heightField;
    
    private float diameterField;
    
    private bool constructorField;
    
    private bool abstractField;
    
    private bool publicField;
    
    private bool systemEntryField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public float height {
        get {
            return this.heightField;
        }
        set {
            this.heightField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public float diameter {
        get {
            return this.diameterField;
        }
        set {
            this.diameterField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool constructor {
        get {
            return this.constructorField;
        }
        set {
            this.constructorField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool @abstract {
        get {
            return this.abstractField;
        }
        set {
            this.abstractField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool @public {
        get {
            return this.publicField;
        }
        set {
            this.publicField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool systemEntry {
        get {
            return this.systemEntryField;
        }
        set {
            this.systemEntryField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISASpaceStationAntenna))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISASpaceStationBlock))]
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public abstract partial class ISAAbstractSpaceStationComponent : ISANamedVisualElement {
    
    private ISASpaceStationRelation typeField;
    
    private ISASpaceStationRelation[] relationsToSpaceStationCentersField;
    
    private ISABlockRelation[] relationsToBlocksField;
    
    private ISAAntennaRelation[] relationsToAntennasField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISASpaceStationRelation type {
        get {
            return this.typeField;
        }
        set {
            this.typeField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("relationsToSpaceStationCenters", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISASpaceStationRelation[] relationsToSpaceStationCenters {
        get {
            return this.relationsToSpaceStationCentersField;
        }
        set {
            this.relationsToSpaceStationCentersField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("relationsToBlocks", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISABlockRelation[] relationsToBlocks {
        get {
            return this.relationsToBlocksField;
        }
        set {
            this.relationsToBlocksField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("relationsToAntennas", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public ISAAntennaRelation[] relationsToAntennas {
        get {
            return this.relationsToAntennasField;
        }
        set {
            this.relationsToAntennasField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISASpaceStationRelation : ISAAbstractVisualRelation {
    
    private string targetField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="anyURI")]
    public string target {
        get {
            return this.targetField;
        }
        set {
            this.targetField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISAAntennaRelation))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISABlockRelation))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ISASpaceStationRelation))]
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public abstract partial class ISAAbstractVisualRelation {
    
    private float weightField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public float weight {
        get {
            return this.weightField;
        }
        set {
            this.weightField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISABlockRelation : ISAAbstractVisualRelation {
    
    private string targetField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="anyURI")]
    public string target {
        get {
            return this.targetField;
        }
        set {
            this.targetField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISAAntennaRelation : ISAAbstractVisualRelation {
    
    private string targetField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute(DataType="anyURI")]
    public string target {
        get {
            return this.targetField;
        }
        set {
            this.targetField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
[System.Xml.Serialization.XmlRootAttribute(Namespace="http://itu.dk/isa/visualization", IsNullable=false)]
public partial class ISASpaceStationAntenna : ISAAbstractSpaceStationComponent {
    
    private bool publicField;
    
    private bool enumConstantField;
    
    private bool recordComponentField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool @public {
        get {
            return this.publicField;
        }
        set {
            this.publicField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool enumConstant {
        get {
            return this.enumConstantField;
        }
        set {
            this.enumConstantField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool recordComponent {
        get {
            return this.recordComponentField;
        }
        set {
            this.recordComponentField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038")]
[System.SerializableAttribute()]
[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://itu.dk/isa/visualization")]
public enum ISASpaceStationType {
    
    /// <remarks/>
    UNKOWN,
    
    /// <remarks/>
    CLASS,
    
    /// <remarks/>
    INTERFACE,
    
    /// <remarks/>
    ENUM,
    
    /// <remarks/>
    RECORD,
}
}
/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage
 * @generated
 */
public interface FilesystemFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FilesystemFactory eINSTANCE = dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ISA Project</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Project</em>'.
	 * @generated
	 */
	ISAProject createISAProject();

	/**
	 * Returns a new object of class '<em>ISA Package Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Package Root</em>'.
	 * @generated
	 */
	ISAPackageRoot createISAPackageRoot();

	/**
	 * Returns a new object of class '<em>ISA Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Package</em>'.
	 * @generated
	 */
	ISAPackage createISAPackage();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FilesystemPackage getFilesystemPackage();

} //FilesystemFactory

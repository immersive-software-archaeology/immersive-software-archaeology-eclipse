package dk.itu.cs.isa.eclipse.ui.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;

import dk.itu.cs.isa.eclipse.ui.ImageHelper;
import dk.itu.cs.isa.events.ISAEventListener;
import dk.itu.cs.isa.events.events.AnalysisSetupEvent;
import dk.itu.cs.isa.events.events.AnalysisSuccessEvent;
import dk.itu.cs.isa.events.events.BooleanInputEvent;
import dk.itu.cs.isa.events.events.Event;
import dk.itu.cs.isa.events.events.EventType;
import dk.itu.cs.isa.events.events.GUIEvent;
import dk.itu.cs.isa.events.events.InputEvent;
import dk.itu.cs.isa.events.events.IntInputEvent;
import dk.itu.cs.isa.events.events.OutputEvent;
import dk.itu.cs.isa.events.events.StringChoiceEvent;
import dk.itu.cs.isa.events.events.StringInputEvent;
import dk.itu.cs.isa.util.TimeUtil;

public class EclipseDialogGUIHandler implements ISAEventListener {
	
	private static Shell SHELL;
	
	public static void setShell(Shell shell) {
		SHELL = shell;
	}



	@Override
	public void notify(Event e) {
		if(!(e instanceof GUIEvent))
			return;
		
		GUIEvent event = (GUIEvent) e;
		
		String title = event.getTitle();
		String text = event.getText();
		
		if(event instanceof OutputEvent) {
			OutputEvent outEvent = (OutputEvent) event;
			Object cause = outEvent.getCause();
			
			if(cause != null) {
				Exception ex = (Exception) cause;
	            MultiStatus status = createMultiStatus(ex);
	            
	            int returnCode = ErrorDialog.openError(SHELL, title, text, status);
				event.setReturnCode(returnCode);
			}
			
			if(event instanceof AnalysisSuccessEvent) {
				ISAAnalysisSuccessDialog dialog = new ISAAnalysisSuccessDialog(SHELL);
				dialog.setTimeInMillis(TimeUtil.millisToMinuteString(((AnalysisSuccessEvent) event).getCalculationTime()));
				event.setReturnCode(dialog.open());
			}
			
			else {
				int dialogType;
				
				switch(outEvent.getType().getValue()) {
				case EventType.QUESTION_VALUE:
					dialogType = MessageDialog.QUESTION;
					break;
				case EventType.WARNING_VALUE:
					dialogType = MessageDialog.WARNING;
					break;
				case EventType.ERROR_VALUE:
					dialogType = MessageDialog.ERROR;
					break;
				default:
					dialogType = MessageDialog.INFORMATION;
					break;
				}
				
				Image iconImage = ImageHelper.getImage("icons/icon-16.png");
				
				MessageDialog dialog = new MessageDialog(SHELL, title, iconImage, text, dialogType, event.getButtonLabels().toArray(new String[] {}), 0);
				int returnCode = dialog.open();
				event.setReturnCode(returnCode);
				
				iconImage.dispose();
			}
		}
		
		else if(event instanceof InputEvent) {
			
			if(event instanceof AnalysisSetupEvent) {
				ISAAnalysisSetupDialog dialog = new ISAAnalysisSetupDialog(SHELL, ((AnalysisSetupEvent) event).getSystemName());
				int returnCode = dialog.open();
				if(returnCode == 0) {
					((AnalysisSetupEvent) event).setSystemName(dialog.getSystemName());
				}
				dialog.close();
				event.setReturnCode(returnCode);
			}
			
			else if(event instanceof StringInputEvent) {
				InputDialog dialog = new InputDialog(SHELL, title, text, ((StringInputEvent) event).getUserInput(), new IInputValidator() {
					@Override
					public String isValid(String newText) {
						if(newText == null || newText.trim().length() == 0) {
							return "The input must not be empty";
						}
						return null;
					}
				});
				int returnCode = dialog.open();
				if(returnCode == 0) {
					((StringInputEvent) event).setUserInput(dialog.getValue());
				}
				event.setReturnCode(returnCode);
			}
			
			if(event instanceof StringChoiceEvent) {
				ISAChoiceDialog dialog = new ISAChoiceDialog(SHELL, title, text, ((StringChoiceEvent) event).getSelection(), ((StringChoiceEvent) event).getOptions());
				int returnCode = dialog.open();
				if(returnCode == 0) {
					((StringChoiceEvent) event).setSelection(dialog.getSelection());
				}
				event.setReturnCode(returnCode);
			}
			
			else if(event instanceof IntInputEvent) {
				InputDialog dialog = new InputDialog(SHELL, title, text, String.valueOf(((IntInputEvent) event).getUserInput()), new IInputValidator() {
					@Override
					public String isValid(String newText) {
						if(newText == null || newText.trim().length() == 0) {
							return "The input must not be empty";
						}
						try {
							Integer.parseInt(newText);
						} catch(Exception e) {
							return "Please insert a number";
						}
						return null;
					}
				});
				int returnCode = dialog.open();
				if(returnCode == 0) {
					((IntInputEvent) event).setUserInput(Integer.parseInt(dialog.getValue()));
				}
				event.setReturnCode(returnCode);
			}

			else if(event instanceof BooleanInputEvent) {
				throw new UnsupportedOperationException("Boolean dialogs were not yet implemented!");
			}
			
		}
		
	}



	private MultiStatus createMultiStatus(Exception e) {
		List<Status> childStatuses = new ArrayList<>();
        StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();

        for (StackTraceElement stackTrace: stackTraces) {
            Status status = new Status(IStatus.ERROR, "dk.itu.cs.isa.ui", stackTrace.toString());
            childStatuses.add(status);
        }

        MultiStatus ms = new MultiStatus("dk.itu.cs.isa.ui", IStatus.ERROR, childStatuses.toArray(new Status[] {}), e.toString(), e);
        return ms;
	}

}

package dk.itu.cs.isa.model.system.creation.java;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.dom.ASTNode;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.software.structure.misc.MiscFactory;
import dk.itu.cs.isa.model.system.creation.java.visitors.ReferenceTrackingVisitor;
import dk.itu.cs.isa.model.system.creation.java.visitors.ReferenceTrackingVisitor.Entry;

public class ReferenceResolvingWorker implements Runnable {
	private final ReferenceHandler callingHandler;
	
	private final ISASoftwareSystemModel isaSystem;
	private final Map<ASTNode, ISAReferencingElement> elementMapping;

	private final int startIndex;
	private final int chunkSize;
	
	private final IProgressMonitor monitor;
	
	

	public ReferenceResolvingWorker(ReferenceHandler callingHandler, int workerIndex, int numberOfWorkers, ISASoftwareSystemModel isaSystem, Map<ASTNode, ISAReferencingElement> elementMapping, IProgressMonitor monitor) {
		this.callingHandler = callingHandler;
		
		this.elementMapping = elementMapping;
		this.monitor = monitor;
		this.isaSystem = isaSystem;
		
		chunkSize = (int) Math.ceil((double) elementMapping.size() / (double) numberOfWorkers);
		startIndex = workerIndex * chunkSize;
		
//		System.out.println("Worker Thread: startIndex="+startIndex + " chunkSize="+chunkSize + " totalElements="+elementMapping.size());
	}
	
	
	
	@Override
	public void run() {
		try {			
			int index = 0;
			for (ASTNode jdtNode : elementMapping.keySet()) {
				if(index < startIndex) {
					index++;
					continue;
				}
				if(index == startIndex+chunkSize)
					break;
				
				ISAReferencingElement isaElement = elementMapping.get(jdtNode);
				
				ReferenceTrackingVisitor referenceVisitor = new ReferenceTrackingVisitor();
				jdtNode.accept(referenceVisitor);
				
				writeToModel(isaSystem, isaElement, referenceVisitor);
				
				monitor.worked(1);
				if (monitor.isCanceled())
					return;
				
				index++;
			}
			
			callingHandler.callBackForReferenceResolvingWorkers(startIndex, chunkSize);
		}
		catch(Exception e) {
			callingHandler.callBackForReferenceResolvingWorkers(e);
//			throw e;
		}
	}
	
	private synchronized static void writeToModel(ISASoftwareSystemModel isaSystem, ISAReferencingElement isaElement, ReferenceTrackingVisitor referenceVisitor) {
		// The uncertainty value counts occurrences in which references could not be
		// resolved due to errors in the workspace, e.g., a method call could not be
		// traced back to the class that contains the resp. method's implementation.
		for (String referencedElementName : referenceVisitor.getReferencedElements().keySet()) {
			Entry entry = referenceVisitor.getReferencedElements().get(referencedElementName);
			ISANamedSoftwareElement target = isaSystem.getSystemElementByName(entry.qualifiedName);
			if(target == null)
				target = isaSystem.getLibraryElementByName(entry.qualifiedName, entry.type, true);
			
			if(target instanceof ISAType) {
				ISAReference<ISAType> ref = MiscFactory.eINSTANCE.createISAReference();
				ref.setTarget((ISAType) target);
				
				isaElement.getTypeReferences().add(ref);
				ref.setWeight(entry.weight);
			}
			else if(target instanceof ISAConstructor) {
				ISAReference<ISAConstructor> ref = MiscFactory.eINSTANCE.createISAReference();
				ref.setTarget((ISAConstructor) target);
				
				isaElement.getConstructorCalls().add(ref);
				ref.setWeight(entry.weight);
			}
			else if(target instanceof ISAMethod) {
				ISAReference<ISAMethod> ref = MiscFactory.eINSTANCE.createISAReference();
				ref.setTarget((ISAMethod) target);
				
				isaElement.getMethodCalls().add(ref);
				ref.setWeight(entry.weight);
			}
			else if(target instanceof ISAField) {
				ISAReference<ISAField> ref = MiscFactory.eINSTANCE.createISAReference();
				ref.setTarget((ISAField) target);
				
				isaElement.getFieldAccesses().add(ref);
				ref.setWeight(entry.weight);
			}
			else if(target instanceof ISAEnumConstant) {
				ISAReference<ISAEnumConstant> ref = MiscFactory.eINSTANCE.createISAReference();
				ref.setTarget((ISAEnumConstant) target);
				
				isaElement.getConstantAccesses().add(ref);
				ref.setWeight(entry.weight);
			}
			else if(target instanceof ISARecordComponent) {
				ISAReference<ISARecordComponent> ref = MiscFactory.eINSTANCE.createISAReference();
				ref.setTarget((ISARecordComponent) target);
				
				isaElement.getComponentAccesses().add(ref);
				ref.setWeight(entry.weight);
			}
			else throw new RuntimeException("Unhandled kind of reference target: " + target.getClass().getName());
		}
	}


}



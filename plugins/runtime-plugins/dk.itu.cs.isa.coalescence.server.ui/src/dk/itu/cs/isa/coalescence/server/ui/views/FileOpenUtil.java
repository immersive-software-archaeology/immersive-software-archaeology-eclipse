package dk.itu.cs.isa.coalescence.server.ui.views;

import org.eclipse.core.resources.IFile;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

public class FileOpenUtil {
	
	private IWorkbench workbench;

	public FileOpenUtil(IWorkbench workbench) {
		if(workbench == null)
			throw new NullPointerException();
		
		this.workbench = workbench;
	}
	
	
	
	private boolean success;

	public boolean showFileInEditor(IFile file) {
		success = true;
		
		Display.getDefault().asyncExec(() -> {
			if(file == null || workbench.getActiveWorkbenchWindow() == null) {
				success = false;
				return;
			}
			
			IWorkbenchPage page = workbench.getActiveWorkbenchWindow().getActivePage();
			if(page == null) {
				success = false;
				return;
			}
			
			IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.getName());
			try {
				page.openEditor(new FileEditorInput(file), desc.getId());
			} catch (PartInitException e) {
				e.printStackTrace();
				success = false;
				return;
			}
		});
		
		return success;
	}

}

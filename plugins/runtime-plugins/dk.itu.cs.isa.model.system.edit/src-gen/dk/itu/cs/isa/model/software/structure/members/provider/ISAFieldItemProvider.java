/**
 */
package dk.itu.cs.isa.model.software.structure.members.provider;


import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.misc.MiscFactory;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.itu.cs.isa.model.software.structure.members.ISAField} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ISAFieldItemProvider extends ISAMemberItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAFieldItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTypePropertyDescriptor(object);
			addArrayTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISATypedElement_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISATypedElement_type_feature", "_UI_ISATypedElement_type"),
				 MiscPackage.Literals.ISA_TYPED_ELEMENT__TYPE,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Array Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addArrayTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISATypedElement_arrayType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISATypedElement_arrayType_feature", "_UI_ISATypedElement_type"),
				 MiscPackage.Literals.ISA_TYPED_ELEMENT__ARRAY_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES);
			childrenFeatures.add(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS);
			childrenFeatures.add(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__METHOD_CALLS);
			childrenFeatures.add(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES);
			childrenFeatures.add(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES);
			childrenFeatures.add(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ISAField.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ISAField"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ISAField)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ISAField_type") :
			getString("_UI_ISAField_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ISAField.class)) {
			case MembersPackage.ISA_FIELD__TYPE:
			case MembersPackage.ISA_FIELD__ARRAY_TYPE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case MembersPackage.ISA_FIELD__TYPE_REFERENCES:
			case MembersPackage.ISA_FIELD__CONSTRUCTOR_CALLS:
			case MembersPackage.ISA_FIELD__METHOD_CALLS:
			case MembersPackage.ISA_FIELD__FIELD_ACCESSES:
			case MembersPackage.ISA_FIELD__CONSTANT_ACCESSES:
			case MembersPackage.ISA_FIELD__COMPONENT_ACCESSES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES,
				 MiscFactory.eINSTANCE.createISAReference()));

		newChildDescriptors.add
			(createChildParameter
				(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS,
				 MiscFactory.eINSTANCE.createISAReference()));

		newChildDescriptors.add
			(createChildParameter
				(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__METHOD_CALLS,
				 MiscFactory.eINSTANCE.createISAReference()));

		newChildDescriptors.add
			(createChildParameter
				(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES,
				 MiscFactory.eINSTANCE.createISAReference()));

		newChildDescriptors.add
			(createChildParameter
				(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES,
				 MiscFactory.eINSTANCE.createISAReference()));

		newChildDescriptors.add
			(createChildParameter
				(MiscPackage.Literals.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES,
				 MiscFactory.eINSTANCE.createISAReference()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__METHOD_CALLS ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}

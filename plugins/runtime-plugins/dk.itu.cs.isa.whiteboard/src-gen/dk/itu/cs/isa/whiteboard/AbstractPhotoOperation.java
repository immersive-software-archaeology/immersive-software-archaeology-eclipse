/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Photo Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getPhotoId <em>Photo Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getBitmapBase64 <em>Bitmap Base64</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpacePosition <em>Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpaceForwardDirection <em>Local Space Forward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpaceUpwardDirection <em>Local Space Upward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getTextureSpacePosition <em>Texture Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getCameraPositionWhenPhotoWasTaken <em>Camera Position When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getCameraRotationWhenPhotoWasTaken <em>Camera Rotation When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getSystemPositionWhenPhotoWasTaken <em>System Position When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getSystemSizeWhenPhotoWasTaken <em>System Size When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getRelationsVisibleWhenPhotoWasTaken <em>Relations Visible When Photo Was Taken</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken <em>Open Capsules Element Qualified Names When Photo Was Taken</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation()
 * @model abstract="true"
 * @generated
 */
public interface AbstractPhotoOperation extends AbstractOperation {
	/**
	 * Returns the value of the '<em><b>Photo Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Photo Id</em>' attribute.
	 * @see #setPhotoId(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_PhotoId()
	 * @model required="true"
	 * @generated
	 */
	int getPhotoId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getPhotoId <em>Photo Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Photo Id</em>' attribute.
	 * @see #getPhotoId()
	 * @generated
	 */
	void setPhotoId(int value);

	/**
	 * Returns the value of the '<em><b>Bitmap Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bitmap Base64</em>' attribute.
	 * @see #setBitmapBase64(String)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_BitmapBase64()
	 * @model required="true"
	 * @generated
	 */
	String getBitmapBase64();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getBitmapBase64 <em>Bitmap Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bitmap Base64</em>' attribute.
	 * @see #getBitmapBase64()
	 * @generated
	 */
	void setBitmapBase64(String value);

	/**
	 * Returns the value of the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Space Position</em>' containment reference.
	 * @see #setLocalSpacePosition(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_LocalSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getLocalSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpacePosition <em>Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Space Position</em>' containment reference.
	 * @see #getLocalSpacePosition()
	 * @generated
	 */
	void setLocalSpacePosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Local Space Forward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Space Forward Direction</em>' containment reference.
	 * @see #setLocalSpaceForwardDirection(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_LocalSpaceForwardDirection()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getLocalSpaceForwardDirection();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpaceForwardDirection <em>Local Space Forward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Space Forward Direction</em>' containment reference.
	 * @see #getLocalSpaceForwardDirection()
	 * @generated
	 */
	void setLocalSpaceForwardDirection(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Local Space Upward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Space Upward Direction</em>' containment reference.
	 * @see #setLocalSpaceUpwardDirection(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_LocalSpaceUpwardDirection()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getLocalSpaceUpwardDirection();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getLocalSpaceUpwardDirection <em>Local Space Upward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Space Upward Direction</em>' containment reference.
	 * @see #getLocalSpaceUpwardDirection()
	 * @generated
	 */
	void setLocalSpaceUpwardDirection(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Texture Space Position</em>' containment reference.
	 * @see #setTextureSpacePosition(ISAVector2)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_TextureSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector2 getTextureSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getTextureSpacePosition <em>Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Texture Space Position</em>' containment reference.
	 * @see #getTextureSpacePosition()
	 * @generated
	 */
	void setTextureSpacePosition(ISAVector2 value);

	/**
	 * Returns the value of the '<em><b>Camera Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Camera Position When Photo Was Taken</em>' containment reference.
	 * @see #setCameraPositionWhenPhotoWasTaken(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_CameraPositionWhenPhotoWasTaken()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getCameraPositionWhenPhotoWasTaken();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getCameraPositionWhenPhotoWasTaken <em>Camera Position When Photo Was Taken</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Camera Position When Photo Was Taken</em>' containment reference.
	 * @see #getCameraPositionWhenPhotoWasTaken()
	 * @generated
	 */
	void setCameraPositionWhenPhotoWasTaken(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Camera Rotation When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Camera Rotation When Photo Was Taken</em>' containment reference.
	 * @see #setCameraRotationWhenPhotoWasTaken(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_CameraRotationWhenPhotoWasTaken()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getCameraRotationWhenPhotoWasTaken();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getCameraRotationWhenPhotoWasTaken <em>Camera Rotation When Photo Was Taken</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Camera Rotation When Photo Was Taken</em>' containment reference.
	 * @see #getCameraRotationWhenPhotoWasTaken()
	 * @generated
	 */
	void setCameraRotationWhenPhotoWasTaken(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Relations Visible When Photo Was Taken</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.whiteboard.VisibleRelation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relations Visible When Photo Was Taken</em>' containment reference list.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_RelationsVisibleWhenPhotoWasTaken()
	 * @model containment="true"
	 * @generated
	 */
	EList<VisibleRelation> getRelationsVisibleWhenPhotoWasTaken();

	/**
	 * Returns the value of the '<em><b>Open Capsules Element Qualified Names When Photo Was Taken</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Open Capsules Element Qualified Names When Photo Was Taken</em>' attribute list.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_OpenCapsulesElementQualifiedNamesWhenPhotoWasTaken()
	 * @model
	 * @generated
	 */
	EList<String> getOpenCapsulesElementQualifiedNamesWhenPhotoWasTaken();

	/**
	 * Returns the value of the '<em><b>System Size When Photo Was Taken</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Size When Photo Was Taken</em>' attribute.
	 * @see #setSystemSizeWhenPhotoWasTaken(float)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_SystemSizeWhenPhotoWasTaken()
	 * @model required="true"
	 * @generated
	 */
	float getSystemSizeWhenPhotoWasTaken();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getSystemSizeWhenPhotoWasTaken <em>System Size When Photo Was Taken</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Size When Photo Was Taken</em>' attribute.
	 * @see #getSystemSizeWhenPhotoWasTaken()
	 * @generated
	 */
	void setSystemSizeWhenPhotoWasTaken(float value);

	/**
	 * Returns the value of the '<em><b>System Position When Photo Was Taken</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Position When Photo Was Taken</em>' containment reference.
	 * @see #setSystemPositionWhenPhotoWasTaken(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPhotoOperation_SystemPositionWhenPhotoWasTaken()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getSystemPositionWhenPhotoWasTaken();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation#getSystemPositionWhenPhotoWasTaken <em>System Position When Photo Was Taken</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Position When Photo Was Taken</em>' containment reference.
	 * @see #getSystemPositionWhenPhotoWasTaken()
	 * @generated
	 */
	void setSystemPositionWhenPhotoWasTaken(ISAVector3 value);

} // AbstractPhotoOperation

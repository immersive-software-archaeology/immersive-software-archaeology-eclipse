package dk.itu.cs.isa.util;

import java.util.List;

public class StringUtil {
	
	public static String calculateLongestCommonSubString(List<String> strings) {
		String longestCommonSubString = strings.get(0);
		while(strings.size() > 1) {
			String name1 = strings.get(0);
			String name2 = strings.get(1);
			
			longestCommonSubString = StringUtil.calculateLongestCommonSubString(name1, name2);
			
			strings.remove(0);
			strings.remove(0);
			strings.add(0, longestCommonSubString);
		}
		return longestCommonSubString;
	}
	
	
	
	/**
	 * cf. https://www.geeksforgeeks.org/print-longest-common-substring/
	 */
	public static String calculateLongestCommonSubString(String string1, String string2) {
		int m = string1.length();
		int n = string2.length();
		
        // Create a table to store lengths of longest common
        // suffixes of substrings.   Note that LCSuff[i][j]
        // contains length of longest common suffix of X[0..i-1]
        // and Y[0..j-1]. The first row and first column entries
        // have no logical meaning, they are used only for
        // simplicity of program
        int[][] LCSuff = new int[m + 1][n + 1];
 
        // To store length of the longest common substring
        int len = 0;
 
        // To store the index of the cell which contains the
        // maximum value. This cell's index helps in building
        // up the longest common substring from right to left.
        int row = 0, col = 0;
 
        /* Following steps build LCSuff[m+1][n+1] in bottom
           up fashion. */
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 || j == 0)
                    LCSuff[i][j] = 0;
 
                else if (string1.charAt(i - 1) == string2.charAt(j - 1)) {
                    LCSuff[i][j] = LCSuff[i - 1][j - 1] + 1;
                    if (len < LCSuff[i][j]) {
                        len = LCSuff[i][j];
                        row = i;
                        col = j;
                    }
                }
                else
                    LCSuff[i][j] = 0;
            }
        }
 
        // if true, then no common substring exists
        if (len == 0) {
            return "";
        }
 
        // allocate space for the longest common substring
        String resultStr = "";
 
        // traverse up diagonally form the (row, col) cell
        // until LCSuff[row][col] != 0
        while (LCSuff[row][col] != 0) {
            resultStr = string1.charAt(row - 1) + resultStr; // or Y[col-1]
            --len;
 
            // move diagonally up to previous cell
            row--;
            col--;
        }
 
        // required longest common substring
        return resultStr;
    }

}

/**
 */
package dk.itu.cs.isa.events.events.util;

import dk.itu.cs.isa.events.events.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.events.events.EventsPackage
 * @generated
 */
public class EventsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EventsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventsSwitch() {
		if (modelPackage == null) {
			modelPackage = EventsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case EventsPackage.EVENT: {
				Event event = (Event)theEObject;
				T result = caseEvent(event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.CONSOLE_EVENT: {
				ConsoleEvent consoleEvent = (ConsoleEvent)theEObject;
				T result = caseConsoleEvent(consoleEvent);
				if (result == null) result = caseEvent(consoleEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.GUI_EVENT: {
				GUIEvent guiEvent = (GUIEvent)theEObject;
				T result = caseGUIEvent(guiEvent);
				if (result == null) result = caseEvent(guiEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.OUTPUT_EVENT: {
				OutputEvent outputEvent = (OutputEvent)theEObject;
				T result = caseOutputEvent(outputEvent);
				if (result == null) result = caseGUIEvent(outputEvent);
				if (result == null) result = caseEvent(outputEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.ANALYSIS_SUCCESS_EVENT: {
				AnalysisSuccessEvent analysisSuccessEvent = (AnalysisSuccessEvent)theEObject;
				T result = caseAnalysisSuccessEvent(analysisSuccessEvent);
				if (result == null) result = caseOutputEvent(analysisSuccessEvent);
				if (result == null) result = caseGUIEvent(analysisSuccessEvent);
				if (result == null) result = caseEvent(analysisSuccessEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.INPUT_EVENT: {
				InputEvent inputEvent = (InputEvent)theEObject;
				T result = caseInputEvent(inputEvent);
				if (result == null) result = caseGUIEvent(inputEvent);
				if (result == null) result = caseEvent(inputEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.ANALYSIS_SETUP_EVENT: {
				AnalysisSetupEvent analysisSetupEvent = (AnalysisSetupEvent)theEObject;
				T result = caseAnalysisSetupEvent(analysisSetupEvent);
				if (result == null) result = caseInputEvent(analysisSetupEvent);
				if (result == null) result = caseGUIEvent(analysisSetupEvent);
				if (result == null) result = caseEvent(analysisSetupEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.BOOLEAN_INPUT_EVENT: {
				BooleanInputEvent booleanInputEvent = (BooleanInputEvent)theEObject;
				T result = caseBooleanInputEvent(booleanInputEvent);
				if (result == null) result = caseInputEvent(booleanInputEvent);
				if (result == null) result = caseGUIEvent(booleanInputEvent);
				if (result == null) result = caseEvent(booleanInputEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.INT_INPUT_EVENT: {
				IntInputEvent intInputEvent = (IntInputEvent)theEObject;
				T result = caseIntInputEvent(intInputEvent);
				if (result == null) result = caseInputEvent(intInputEvent);
				if (result == null) result = caseGUIEvent(intInputEvent);
				if (result == null) result = caseEvent(intInputEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.STRING_INPUT_EVENT: {
				StringInputEvent stringInputEvent = (StringInputEvent)theEObject;
				T result = caseStringInputEvent(stringInputEvent);
				if (result == null) result = caseInputEvent(stringInputEvent);
				if (result == null) result = caseGUIEvent(stringInputEvent);
				if (result == null) result = caseEvent(stringInputEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EventsPackage.STRING_CHOICE_EVENT: {
				StringChoiceEvent stringChoiceEvent = (StringChoiceEvent)theEObject;
				T result = caseStringChoiceEvent(stringChoiceEvent);
				if (result == null) result = caseInputEvent(stringChoiceEvent);
				if (result == null) result = caseGUIEvent(stringChoiceEvent);
				if (result == null) result = caseEvent(stringChoiceEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvent(Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Console Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Console Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConsoleEvent(ConsoleEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GUI Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GUI Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGUIEvent(GUIEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Output Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Output Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutputEvent(OutputEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Analysis Success Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Analysis Success Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnalysisSuccessEvent(AnalysisSuccessEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Input Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Input Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInputEvent(InputEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Analysis Setup Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Analysis Setup Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnalysisSetupEvent(AnalysisSetupEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Input Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Input Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanInputEvent(BooleanInputEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Input Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Input Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntInputEvent(IntInputEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Input Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Input Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringInputEvent(StringInputEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Choice Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Choice Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringChoiceEvent(StringChoiceEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //EventsSwitch

/**
 */
package dk.itu.cs.isa.model.software.structure.members;

import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Concrete Method</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAConcreteMethod()
 * @model
 * @generated
 */
public interface ISAConcreteMethod extends ISAReferencingElement, ISAMethod, ISAStatementContainer {
} // ISAConcreteMethod

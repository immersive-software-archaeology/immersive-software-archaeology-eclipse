package dk.itu.cs.isa.model.system.creation.srcml;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class XMLHelper {

	public static Element parseXML(InputStream inputStream) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(inputStream);

		// Normalize the XML Structure; It's just too important !!
		document.getDocumentElement().normalize();

		Element root = document.getDocumentElement();

		// Get a specific kind of tag
//		NodeList nList = document.getElementsByTagName("unit");

		return root;
	}

//	private static void visitChildNodes(NodeList nList) {
//		for (int temp = 0; temp < nList.getLength(); temp++) {
//			Node node = nList.item(temp);
//			
//			if (node.getNodeType() == Node.ELEMENT_NODE) {
//				
//				if (node.hasAttributes()) {
//					NamedNodeMap nodeMap = node.getAttributes();
//					
//					for (int i = 0; i < nodeMap.getLength(); i++) {
//						Node tempNode = nodeMap.item(i);
//					}
//					
//					if (node.hasChildNodes()) {
//						visitChildNodes(node.getChildNodes());
//					}
//				}
//			}
//		}
//	}

}

/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsImplementedInterface <em>Usages As Implemented Interface</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface#getUsagesAsExtendedInterface <em>Usages As Extended Interface</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAInterface()
 * @model
 * @generated
 */
public interface ISAInterface extends ISAClassifier, ISAInterfaceExtendingElement {
	/**
	 * Returns the value of the '<em><b>Usages As Implemented Interface</b></em>' reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getImplementedInterfaces <em>Implemented Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usages As Implemented Interface</em>' reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAInterface_UsagesAsImplementedInterface()
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClass#getImplementedInterfaces
	 * @model opposite="implementedInterfaces"
	 * @generated
	 */
	EList<ISAClass> getUsagesAsImplementedInterface();

	/**
	 * Returns the value of the '<em><b>Usages As Extended Interface</b></em>' reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement#getExtendedInterfaces <em>Extended Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usages As Extended Interface</em>' reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAInterface_UsagesAsExtendedInterface()
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement#getExtendedInterfaces
	 * @model opposite="extendedInterfaces"
	 * @generated
	 */
	EList<ISAInterfaceExtendingElement> getUsagesAsExtendedInterface();

} // ISAInterface

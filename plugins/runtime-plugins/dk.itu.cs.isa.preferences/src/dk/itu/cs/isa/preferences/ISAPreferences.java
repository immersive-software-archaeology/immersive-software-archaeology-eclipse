package dk.itu.cs.isa.preferences;


import dk.itu.cs.isa.events.ISAEventSystem;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.BackingStoreException;

public class ISAPreferences extends AbstractPreferenceInitializer {

	public static final String LAST_MODEL_NAME = "LAST_MODEL_NAME";

	public static final String ISA_EXECUTABLE_PATH = "ISA_EXECUTABLE_PATH";

	public static final String R_EXECUTABLE_PATH = "R_EXECUTABLE_PATH";

	public static final String SYSTEM_MODEL_CREATOR_DEFAULT = "SYSTEM_MODEL_CREATOR_DEFAULT";
	public static final String SYSTEM_MODEL_QUALITY_ANALYZER_DEFAULT = "SYSTEM_MODEL_QUALITY_ANALYZER_DEFAULT";
	public static final String SYSTEM_MODEL_SAR_DEFAULT = "SYSTEM_MODEL_ARCHITECTURE_ANALYZER_DEFAULT";
	public static final String VISUALIZATION_MODEL_CREATOR_DEFAULT = "VISUALIZATION_MODEL_CREATOR_DEFAULT";

	public static final String SYSTEM_MODEL_QUALITY_ANALYZER_EXECUTE = "SYSTEM_MODEL_QUALITY_ANALYZER_EXECUTE";
	public static final String SYSTEM_MODEL_SAR_EXECUTE = "SYSTEM_MODEL_SAR_EXECUTE";
	
	
	
	@Override
    public void initializeDefaultPreferences() {
    	ISAPreferences.setString(ISAPreferences.SYSTEM_MODEL_CREATOR_DEFAULT, "");
    	ISAPreferences.setString(ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_DEFAULT, "");
    	ISAPreferences.setString(ISAPreferences.SYSTEM_MODEL_SAR_DEFAULT, "");
    	ISAPreferences.setString(ISAPreferences.VISUALIZATION_MODEL_CREATOR_DEFAULT, "");

    	ISAPreferences.setBoolean(ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_EXECUTE, false);
    	ISAPreferences.setBoolean(ISAPreferences.SYSTEM_MODEL_SAR_EXECUTE, false);
    }
	
	

	public static final String STORAGE_ID = "dk.itu.cs.isa.preferences";
	public static final String PAGE_ID = STORAGE_ID + ".page";
	
	
	
	private static IEclipsePreferences pref;
	
	private static IEclipsePreferences getPref() {
		if(pref == null)
			pref = InstanceScope.INSTANCE.getNode(STORAGE_ID);
		return pref;
	}
	

	
	public static String getString(String key, String def) {
		return getPref().get(key, def);
	}
	
	public static int getInt(String key, int def) {
		return getPref().getInt(key, def);
	}
	
	public static float getFloat(String key, float def) {
		return getPref().getFloat(key, def);
	}
	
	public static boolean getBoolean(String key, boolean def) {
		return getPref().getBoolean(key, def);
	}
	

	
	public static void setString(String key, String value) {
		getPref().put(key, value);
		flush();
	}
	
	public static void setInt(String key, int value) {
		getPref().putInt(key, value);
		flush();
	}
	
	public static void setFloat(String key, float value) {
		getPref().putFloat(key, value);
		flush();
	}

	public static void setBoolean(String key, boolean value) {
		getPref().putBoolean(key, value);
		flush();
	}
	
	private static void flush() {
		try {
			getPref().flush();
		} catch (BackingStoreException e) {
			ISAEventSystem.fireErrorMessage("Could not write to Eclipse preference system!", e);
		}
	}

}

/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Draw Arrow Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.DrawArrowOperation#isAutoDrawArrowHead <em>Auto Draw Arrow Head</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getDrawArrowOperation()
 * @model
 * @generated
 */
public interface DrawArrowOperation extends AbstractDrawOperation {
	/**
	 * Returns the value of the '<em><b>Auto Draw Arrow Head</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Auto Draw Arrow Head</em>' attribute.
	 * @see #setAutoDrawArrowHead(boolean)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getDrawArrowOperation_AutoDrawArrowHead()
	 * @model required="true"
	 * @generated
	 */
	boolean isAutoDrawArrowHead();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.DrawArrowOperation#isAutoDrawArrowHead <em>Auto Draw Arrow Head</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Auto Draw Arrow Head</em>' attribute.
	 * @see #isAutoDrawArrowHead()
	 * @generated
	 */
	void setAutoDrawArrowHead(boolean value);

} // DrawArrowOperation

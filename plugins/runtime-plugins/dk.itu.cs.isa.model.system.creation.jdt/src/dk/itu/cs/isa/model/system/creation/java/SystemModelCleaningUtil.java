package dk.itu.cs.isa.model.system.creation.java;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;

public class SystemModelCleaningUtil {
	
	public static void removeEmptyPackages(ISAProject project) {
		List<ISAPackage> emptyPackages = new ArrayList<>();
		findEmptyPackages(project, emptyPackages);
		
		for(ISAPackage emptyPackage : emptyPackages)
			emptyPackage.getParentContainer().getSubPackages().remove(emptyPackage);
	}
	
	private static void findEmptyPackages(EObject e, List<ISAPackage> emptyPackages) {
		if(e instanceof ISAPackage) {
			ISAPackage pack = (ISAPackage) e;
			if(pack.getClassifiers().isEmpty() && pack.getSubPackages().isEmpty())
				emptyPackages.add(pack);
		}
		
		for(EObject child : e.eContents())
			findEmptyPackages(child, emptyPackages);
	}

}

package dk.itu.cs.isa.eclipse.ui.dialogs;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.widgets.WidgetFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import dk.itu.cs.isa.coalescence.server.ISACoalescenceServer;
import dk.itu.cs.isa.coalescence.server.ISAServer.ServerState;
import dk.itu.cs.isa.eclipse.ui.ImageHelper;

public class ISAAnalysisSuccessDialog extends TitleAreaDialog {

	private boolean disposed = false;
	private String timeInMillis;
	private List<Image> registeredImages = new LinkedList<>();
	
	private Button startServerButton;
	private Label serverStateImageLabel;
	private Label serverStateTextLabel;
	private Image serverOnlineImage;
	private Image serverOfflineImage;
	
	

	public ISAAnalysisSuccessDialog(Shell parentShell) {
		super(parentShell);
		serverOnlineImage = ImageHelper.getImage("images/connection_vr_ide_-_connected.png");
		serverOfflineImage = ImageHelper.getImage("images/connection_vr_ide_-_disconnected.png");
		registeredImages.add(serverOnlineImage);
		registeredImages.add(serverOfflineImage);
	}

	public void setTimeInMillis(String timeInMillis) {
		this.timeInMillis = timeInMillis;
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		Image iconImage = ImageHelper.getImage("icons/icon-16-analysis.png");
		shell.setImage(iconImage);
		registeredImages.add(iconImage);
	}

	@Override
	public boolean close() {
		for(Image image : registeredImages)
			image.dispose();
		disposed = true;
		return super.close();
	}

	@Override
	public void create() {
		super.create();
		Image headerImage = ImageHelper.getImage("images/analysis_success_dialog_head.png");
		setTitleImage(headerImage);
		registeredImages.add(headerImage);
		
		setTitle("System analysis finished successfully");
		setMessage("Total exection duration: " + timeInMillis + " min", IMessageProvider.INFORMATION);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite contentContainerParent = (Composite) super.createDialogArea(parent);
		Composite contentContainer = new Composite(contentContainerParent, SWT.NONE);
		contentContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, false);
		contentContainer.setLayout(layout);
		
		new Label(contentContainer, SWT.NONE);
		
		createCentredTextLabel(contentContainer, "You can start digging into the system in VR now!")
		.setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));

		new Label(contentContainer, SWT.NONE);
		new Label(contentContainer, SWT.NONE);
		
		serverStateImageLabel = new Label(contentContainer, SWT.CENTER);
		serverStateImageLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		serverStateTextLabel = createCentredTextLabel(contentContainer, "");
		serverStateTextLabel.setFont(JFaceResources.getFontRegistry().getItalic(JFaceResources.DEFAULT_FONT));

		if (ISACoalescenceServer.INSTANCE.getServerState().equals(ServerState.ONLINE)) {
			serverStateImageLabel.setImage(serverOnlineImage);
			serverStateTextLabel.setText("Everything is up and running!");
		}
		else {
			serverStateImageLabel.setImage(serverOfflineImage);
			serverStateTextLabel.setText("However, that requires to run the ISA Coalescence Server which is used for\n"
					+ "communication between the ISA VR application and the ISA Eclipse plugins.");

			new Label(contentContainer, SWT.NONE);
			
			GridData startServerButtonLayout = new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1);
			startServerButtonLayout.heightHint = 30;
			startServerButton = WidgetFactory.button(SWT.PUSH | SWT.CENTER)
					.layoutData(startServerButtonLayout)
					.text("Start ISA Coalescence Server")
					.font(JFaceResources.getDialogFont())
					.data(Integer.valueOf(IDialogConstants.CLIENT_ID))
					.onSelect(event -> startServer()).
					create(contentContainer);
			Image startServerIcon = ImageHelper.getImage("icons/icon-16-server.png");
			startServerButton.setImage(startServerIcon);
			registeredImages.add(startServerIcon);
		}
		
		new Label(contentContainer, SWT.NONE);
		
		return contentContainerParent;
	}
	
	private void startServer() {
		if(ISACoalescenceServer.INSTANCE.canBeStarted()) {
			startServerButton.setEnabled(false);
			ISACoalescenceServer.INSTANCE.startServer();
			
			new Thread(new Runnable() {
				@Override
				public void run() {
					while(!disposed && !ISACoalescenceServer.INSTANCE.getServerState().equals(ServerState.ONLINE)) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					serverStateImageLabel.setImage(serverOnlineImage);
					serverStateTextLabel.setText("Everything is up and running!");
				}
				
			}).run();
		}
	}
	
	private Label createCentredTextLabel(Composite container, String text) {
		Label textLabel = new Label(container, SWT.CENTER);
		textLabel.setText(text);
		textLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		return textLabel;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, "Open ISA Perspective", false);
	}

}

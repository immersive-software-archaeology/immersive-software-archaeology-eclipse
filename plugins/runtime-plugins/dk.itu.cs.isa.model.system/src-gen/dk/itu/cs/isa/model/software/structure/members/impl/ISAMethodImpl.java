/**
 */
package dk.itu.cs.isa.model.software.structure.members.impl;

import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Method</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMethodImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMethodImpl#isArrayType <em>Array Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAMethodImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ISAMethodImpl extends ISAMemberImpl implements ISAMethod {
	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ISAType type;

	/**
	 * The default value of the '{@link #isArrayType() <em>Array Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArrayType()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ARRAY_TYPE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isArrayType() <em>Array Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArrayType()
	 * @generated
	 * @ordered
	 */
	protected boolean arrayType = ARRAY_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAParameter> parameters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAMethodImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MembersPackage.Literals.ISA_METHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (ISAType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MembersPackage.ISA_METHOD__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetType(ISAType newType, NotificationChain msgs) {
		ISAType oldType = type;
		type = newType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_METHOD__TYPE, oldType, newType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(ISAType newType) {
		if (newType != type) {
			NotificationChain msgs = null;
			if (type != null)
				msgs = ((InternalEObject)type).eInverseRemove(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
			if (newType != null)
				msgs = ((InternalEObject)newType).eInverseAdd(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
			msgs = basicSetType(newType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_METHOD__TYPE, newType, newType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isArrayType() {
		return arrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArrayType(boolean newArrayType) {
		boolean oldArrayType = arrayType;
		arrayType = newArrayType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_METHOD__ARRAY_TYPE, oldArrayType, arrayType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAParameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<ISAParameter>(ISAParameter.class, this, MembersPackage.ISA_METHOD__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MembersPackage.ISA_METHOD__TYPE:
				if (type != null)
					msgs = ((InternalEObject)type).eInverseRemove(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
				return basicSetType((ISAType)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MembersPackage.ISA_METHOD__TYPE:
				return basicSetType(null, msgs);
			case MembersPackage.ISA_METHOD__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MembersPackage.ISA_METHOD__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case MembersPackage.ISA_METHOD__ARRAY_TYPE:
				return isArrayType();
			case MembersPackage.ISA_METHOD__PARAMETERS:
				return getParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MembersPackage.ISA_METHOD__TYPE:
				setType((ISAType)newValue);
				return;
			case MembersPackage.ISA_METHOD__ARRAY_TYPE:
				setArrayType((Boolean)newValue);
				return;
			case MembersPackage.ISA_METHOD__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends ISAParameter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_METHOD__TYPE:
				setType((ISAType)null);
				return;
			case MembersPackage.ISA_METHOD__ARRAY_TYPE:
				setArrayType(ARRAY_TYPE_EDEFAULT);
				return;
			case MembersPackage.ISA_METHOD__PARAMETERS:
				getParameters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_METHOD__TYPE:
				return type != null;
			case MembersPackage.ISA_METHOD__ARRAY_TYPE:
				return arrayType != ARRAY_TYPE_EDEFAULT;
			case MembersPackage.ISA_METHOD__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISATypedElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_METHOD__TYPE: return MiscPackage.ISA_TYPED_ELEMENT__TYPE;
				case MembersPackage.ISA_METHOD__ARRAY_TYPE: return MiscPackage.ISA_TYPED_ELEMENT__ARRAY_TYPE;
				default: return -1;
			}
		}
		if (baseClass == ISAParametrizableElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_METHOD__PARAMETERS: return MiscPackage.ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISATypedElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_TYPED_ELEMENT__TYPE: return MembersPackage.ISA_METHOD__TYPE;
				case MiscPackage.ISA_TYPED_ELEMENT__ARRAY_TYPE: return MembersPackage.ISA_METHOD__ARRAY_TYPE;
				default: return -1;
			}
		}
		if (baseClass == ISAParametrizableElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS: return MembersPackage.ISA_METHOD__PARAMETERS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (arrayType: ");
		result.append(arrayType);
		result.append(')');
		return result.toString();
	}

} //ISAMethodImpl

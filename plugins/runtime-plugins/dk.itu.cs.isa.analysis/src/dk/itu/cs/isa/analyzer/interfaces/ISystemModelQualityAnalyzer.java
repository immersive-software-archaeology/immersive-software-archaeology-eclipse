package dk.itu.cs.isa.analyzer.interfaces;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel;

public interface ISystemModelQualityAnalyzer extends ISAModelHandler {

	public void run(ISASoftwareSystemModel systemModel, ISASoftwareSystemQualityModel qualityModel, List<IProject> analyzedProjects, IProgressMonitor monitor) throws Exception;
	
}

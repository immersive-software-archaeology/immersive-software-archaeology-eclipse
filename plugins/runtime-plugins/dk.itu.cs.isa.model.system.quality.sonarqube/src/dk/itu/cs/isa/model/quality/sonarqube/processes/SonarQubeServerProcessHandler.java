package dk.itu.cs.isa.model.quality.sonarqube.processes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import dk.itu.cs.isa.coalescence.server.ISAServer;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.quality.sonarqube.api.SonarQubeAPIClient;
import dk.itu.cs.isa.model.quality.sonarqube.preferences.ISAPreferencesSonarQube;
import dk.itu.cs.isa.model.quality.sonarqube.processes.io.SonarProcessesOutputHandler;

public class SonarQubeServerProcessHandler implements ISAServer {
	
	public static final SonarQubeServerProcessHandler INSTANCE = new SonarQubeServerProcessHandler();
	public static final String NAME = "SONAR_QUBE_SERVER_PROCESS";
	
	private final int PORT = 9000;
	
	private SonarQubeAPIClient apiClient;
	private Process sonarQubeWrapperProcess;
	
	
	
	private SonarQubeServerProcessHandler() {}
	
	
	
	@Override
	public boolean canBeStarted() {
		return getServerState().equals(ServerState.OFFLINE);
	}

	@Override
	public boolean canBeStopped() {
		return false;
	}

	@Override
	public ServerState getServerState() {
		try {
			if(apiClient == null)
				apiClient = new SonarQubeAPIClient();
			if(apiClient.pingServer() == SonarQubeAPIClient.OFFLINE) {
				if(sonarQubeWrapperProcess != null)
					return ServerState.STARTING;
				else return ServerState.OFFLINE;
			}
			else return ServerState.ONLINE;
		} catch (Exception e) {
			return ServerState.UNKOWN;
		}
	}

	@Override
	public boolean startServer() {
		String sonarQubeExecutablePath = ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_QUBE_EXECUTABLE_PATH, null);
		if(sonarQubeExecutablePath == null) {
			new ISAPreferencesSonarQube().initializeDefaultSonarQubePath();
			sonarQubeExecutablePath = ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_QUBE_EXECUTABLE_PATH, null);
		}
		
		if(sonarQubeExecutablePath == null || sonarQubeExecutablePath.isEmpty()) {
			ISAEventSystem.fireErrorMessage("SonarQube Executable not specified",
					"Please have a look at the ISA preference page (Window > Preferences > ISA) and specify "
					+ "a valid path to the SonarQube executable.");
			return false;
		}
		
		try {
			if(getServerState().equals(ServerState.ONLINE)) {
				ISAEventSystem.fireConfirmationDialog(
						"SonarQube is already running",
						"SonarQube (or another process) is already running and listening to port "+ PORT);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			sonarQubeWrapperProcess = new ProcessBuilder(sonarQubeExecutablePath).start();
		} catch (IOException eIgnore) {
			// Maybe the path is faulty, try again!
			new ISAPreferencesSonarQube().initializeDefaultSonarQubePath();
			sonarQubeExecutablePath = ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_QUBE_EXECUTABLE_PATH, null);

			try {
				sonarQubeWrapperProcess = new ProcessBuilder(sonarQubeExecutablePath).start();
			} catch (IOException eFire) {
				ISAEventSystem.fireErrorMessage("Could not start the Server Batch Process", eFire);
				return false;
			}
		}
		
		(new Thread(new Runnable() {
			@Override
			public void run() {
				SonarProcessesOutputHandler.notifyStart(NAME);
				while(sonarQubeWrapperProcess.isAlive()) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
						return;
					}
					
					try {
						BufferedReader errorReader = new BufferedReader(new InputStreamReader(sonarQubeWrapperProcess.getInputStream()));
						String errorLine;
						while(errorReader.ready() && (errorLine = errorReader.readLine()) != null) {
							System.err.println(errorLine);
							SonarProcessesOutputHandler.notifyError(NAME, errorLine);
						}
						
						BufferedReader outputReader = new BufferedReader(new InputStreamReader(sonarQubeWrapperProcess.getInputStream()));
						String outputLine;
						while(outputReader.ready() && (outputLine = outputReader.readLine()) != null) {
							System.out.println(outputLine);
							SonarProcessesOutputHandler.notifyOutput(NAME, outputLine);
						}
					}
					catch(IOException e) {
						e.printStackTrace();
						return;
					}
				}
			}
		})).start();
		return true;
	}

	@Override
	public boolean stopServer() {
		return false;
	}

	@Override
	public String getServerAddress() {
		return "http://localhost:" + PORT + "/";
	}
	
	
	
	
	
}

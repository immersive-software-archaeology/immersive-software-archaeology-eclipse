package dk.itu.cs.isa.model.system.coalescence.sourcecode;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SourceCodeFormatter {
	
	private final static String[] KEYWORDS = new String[] {
			"abstract",
			"assert",
			"boolean",
			"break",
			"byte",
			"case",
			"catch",
			"char",
			"class",
			"const",
			"continue",
			"default",
			"do",
			"double",
			"else",
			"enum",
			"extends",
			"final",
			"finally",
			"float",
			"for",
			"goto",
			"if",
			"implements",
			"import",
			"instanceof",
			"int",
			"interface",
			"long",
			"native",
			"new",
			"package",
			"private",
			"protected",
			"public",
			"return",
			"short",
			"static",
			"super",
			"switch",
			"synchronized",
			"this",
			"throw",
			"throws",
			"transient",
			"try",
			"void",
			"volatile",
			"while",
			
			"true",
			"false",
			"null",
			
			"strictfp"
	};
	
	public static String formatCode(String code) {
		List<CodeSequence> stringsMatches = new LinkedList<>();
		List<CodeSequence> commentMatches = new LinkedList<>();
		List<CodeSequence> keywordMatches = new LinkedList<>();
        
		// First, get all occurrences of strings, comments, and keywords
		{
			collectAllMatches(stringsMatches, code, CodeType.string, Pattern.compile("([\"\'].*?(?<!\\\\)[\"\'])"), 0, 0);
			collectAllMatches(commentMatches, code, CodeType.comment, Pattern.compile("(//.*?$)|(/\\*.*?\\*/)", Pattern.MULTILINE | Pattern.DOTALL), 0, 0);
			
			// Problem with that:	strings that are contained within a comment
			// 						should not be highlighted as strings (and vice versa).
			// Therefore:			remove occurrences of strings contained in comments
			// 						and comments contained in strings
			removeSequencesInBoundsOfOtherSequences(commentMatches, stringsMatches);
			removeSequencesInBoundsOfOtherSequences(stringsMatches, commentMatches);

			for(String keyword : KEYWORDS) {
				collectAllMatches(keywordMatches, code, CodeType.keyword, Pattern.compile("[\\W]" + keyword + "[\\W]", Pattern.DOTALL), 1, 1);
				collectAllMatches(keywordMatches, code, CodeType.keyword, Pattern.compile("^" + keyword + "[\\W]", Pattern.DOTALL), 0, 1);
			}
			
			removeSequencesInBoundsOfOtherSequences(keywordMatches, stringsMatches);
			removeSequencesInBoundsOfOtherSequences(keywordMatches, commentMatches);
		}
		
		// Now, construct an array of tagged code fragments (strings, comments, etc.)
		// which subdivides the code into different constituents.
		List<CodeSequence> sortedListOfCodeFragments = new LinkedList<>();
		sortedListOfCodeFragments.addAll(stringsMatches);
		sortedListOfCodeFragments.addAll(commentMatches);
		sortedListOfCodeFragments.addAll(keywordMatches);
		sortedListOfCodeFragments.sort(new Comparator<CodeSequence>() {
			@Override
			public int compare(CodeSequence o1, CodeSequence o2) {
				return o1.start - o2.start;
			}
		});
		
		// Construct initial array with marked strings
		{
			int currentPositionInText = 0;
			for(int i=0; i<sortedListOfCodeFragments.size(); i++) {
				CodeSequence stringMatch = sortedListOfCodeFragments.get(i);
				if(stringMatch.start > currentPositionInText) {
					CodeSequence newFragment = new CodeSequence(CodeType.unmarked, currentPositionInText, stringMatch.start);
					newFragment.text = code.substring(currentPositionInText, stringMatch.start);
					sortedListOfCodeFragments.add(i, newFragment);
				}
				currentPositionInText = stringMatch.end;
			}
			if(currentPositionInText < code.length()-1) {
				CodeSequence newFragment = new CodeSequence(CodeType.unmarked, currentPositionInText, code.length()-1);
				newFragment.text = code.substring(currentPositionInText);
				sortedListOfCodeFragments.add(newFragment);
			}
		}
		
        return constructEnrichedCodeString(sortedListOfCodeFragments);
	}
	
	private static String constructEnrichedCodeString(List<CodeSequence> codeFragments) {
		StringBuilder builder = new StringBuilder();
		
		for(CodeSequence fragment : codeFragments) {
			if(fragment.type == CodeType.unmarked) {
				builder.append(fragment.text);
			}
			else if(fragment.type == CodeType.comment) {
				builder.append("<color=#7ACC96><i>");
				builder.append(fragment.text);
				builder.append("</i></color>");
			}
			else if(fragment.type == CodeType.string) {
				builder.append("<color=#7AABCC>");
				builder.append(fragment.text);
				builder.append("</color>");
			}
			else if(fragment.type == CodeType.keyword) {
				builder.append("<color=#FFFFFF><b>");
				builder.append(fragment.text);
				builder.append("</b></color>");
			}
			else throw new RuntimeException("Unknown type of code fragment: " + fragment.type);
		}
		
		return builder.toString();
	}

	private static void removeSequencesInBoundsOfOtherSequences(List<CodeSequence> toBeCleanedSequences, List<CodeSequence> otherSequences) {
		Set<CodeSequence> sequencesToRemove = new HashSet<CodeSequence>();
		
		for(CodeSequence outerSequence : otherSequences) {
			for(CodeSequence toBeCheckedSequence : toBeCleanedSequences) {
				if (toBeCheckedSequence.start > outerSequence.start && toBeCheckedSequence.start < outerSequence.end)
                	sequencesToRemove.add(toBeCheckedSequence);
			}
		}
        
        for (CodeSequence comment : sequencesToRemove)
        	toBeCleanedSequences.remove(comment);
	}

	private static void collectAllMatches(List<CodeSequence> matches, String code, CodeType type, Pattern pattern, int offsetStart, int offsetEnd) {
		Matcher matcher = pattern.matcher(code);
        while (matcher.find()) {
            CodeSequence match = new CodeSequence(type, matcher.start() + offsetStart, matcher.end() - offsetEnd);
            match.text = matcher.group();
            match.text = match.text.substring(offsetStart, match.text.length() - offsetEnd);
            matches.add(match);
        }
	}
	

	
	private static enum CodeType {
		unmarked, string, comment, keyword; 
	}

	private static class CodeSequence {
		private CodeType type;
		private int start;
		private int end;
		public String text;
		
		private CodeSequence(CodeType type, int start, int end) {
			this.start = start;
			this.end = end;
			this.type = type;
		}
	}

}






























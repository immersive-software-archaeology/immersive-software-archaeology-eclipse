/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Color</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.ISAColor#getR <em>R</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.ISAColor#getG <em>G</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.ISAColor#getB <em>B</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.ISAColor#getA <em>A</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getISAColor()
 * @model
 * @generated
 */
public interface ISAColor extends EObject {
	/**
	 * Returns the value of the '<em><b>R</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>R</em>' attribute.
	 * @see #setR(float)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getISAColor_R()
	 * @model required="true"
	 * @generated
	 */
	float getR();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.ISAColor#getR <em>R</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>R</em>' attribute.
	 * @see #getR()
	 * @generated
	 */
	void setR(float value);

	/**
	 * Returns the value of the '<em><b>G</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>G</em>' attribute.
	 * @see #setG(float)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getISAColor_G()
	 * @model required="true"
	 * @generated
	 */
	float getG();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.ISAColor#getG <em>G</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>G</em>' attribute.
	 * @see #getG()
	 * @generated
	 */
	void setG(float value);

	/**
	 * Returns the value of the '<em><b>B</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>B</em>' attribute.
	 * @see #setB(float)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getISAColor_B()
	 * @model required="true"
	 * @generated
	 */
	float getB();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.ISAColor#getB <em>B</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>B</em>' attribute.
	 * @see #getB()
	 * @generated
	 */
	void setB(float value);

	/**
	 * Returns the value of the '<em><b>A</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>A</em>' attribute.
	 * @see #setA(float)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getISAColor_A()
	 * @model required="true"
	 * @generated
	 */
	float getA();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.ISAColor#getA <em>A</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>A</em>' attribute.
	 * @see #getA()
	 * @generated
	 */
	void setA(float value);

} // ISAColor

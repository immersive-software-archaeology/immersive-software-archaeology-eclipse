/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.provider;


import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersFactory;
import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;

import dk.itu.cs.isa.model.software.structure.libraries.LibrariesFactory;

import dk.itu.cs.isa.model.software.structure.members.MembersFactory;

import dk.itu.cs.isa.model.software.structure.members.MembersPackage;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.provider.ISATypeItemProvider;

import dk.itu.cs.isa.model.software.structure.provider.SoftwareSystemStructuralModelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ISAClassifierItemProvider extends ISATypeItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAClassifierItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addModifiersPropertyDescriptor(object);
			addNumberInOrderOfDeclarationPropertyDescriptor(object);
			addStartPositionInDocumentPropertyDescriptor(object);
			addEndPositionInDocumentPropertyDescriptor(object);
			addFilePathPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Modifiers feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModifiersPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAModifiableElement_modifiers_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAModifiableElement_modifiers_feature", "_UI_ISAModifiableElement_type"),
				 MiscPackage.Literals.ISA_MODIFIABLE_ELEMENT__MODIFIERS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number In Order Of Declaration feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberInOrderOfDeclarationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAMember_numberInOrderOfDeclaration_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAMember_numberInOrderOfDeclaration_feature", "_UI_ISAMember_type"),
				 MembersPackage.Literals.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Start Position In Document feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStartPositionInDocumentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAMember_startPositionInDocument_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAMember_startPositionInDocument_feature", "_UI_ISAMember_type"),
				 MembersPackage.Literals.ISA_MEMBER__START_POSITION_IN_DOCUMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the End Position In Document feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEndPositionInDocumentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAMember_endPositionInDocument_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAMember_endPositionInDocument_feature", "_UI_ISAMember_type"),
				 MembersPackage.Literals.ISA_MEMBER__END_POSITION_IN_DOCUMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the File Path feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFilePathPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAClassifier_filePath_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAClassifier_filePath_feature", "_UI_ISAClassifier_type"),
				 ClassifiersPackage.Literals.ISA_CLASSIFIER__FILE_PATH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ClassifiersPackage.Literals.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS);
			childrenFeatures.add(ClassifiersPackage.Literals.ISA_CLASSIFIER__FIELDS);
			childrenFeatures.add(ClassifiersPackage.Literals.ISA_CLASSIFIER__METHODS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ISAClassifier)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ISAClassifier_type") :
			getString("_UI_ISAClassifier_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ISAClassifier.class)) {
			case ClassifiersPackage.ISA_CLASSIFIER__MODIFIERS:
			case ClassifiersPackage.ISA_CLASSIFIER__NUMBER_IN_ORDER_OF_DECLARATION:
			case ClassifiersPackage.ISA_CLASSIFIER__START_POSITION_IN_DOCUMENT:
			case ClassifiersPackage.ISA_CLASSIFIER__END_POSITION_IN_DOCUMENT:
			case ClassifiersPackage.ISA_CLASSIFIER__FILE_PATH:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ClassifiersPackage.ISA_CLASSIFIER__CLASSIFIERS:
			case ClassifiersPackage.ISA_CLASSIFIER__FIELDS:
			case ClassifiersPackage.ISA_CLASSIFIER__METHODS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ClassifiersPackage.Literals.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS,
				 ClassifiersFactory.eINSTANCE.createISAAbstractClass()));

		newChildDescriptors.add
			(createChildParameter
				(ClassifiersPackage.Literals.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS,
				 ClassifiersFactory.eINSTANCE.createISAConcreteClass()));

		newChildDescriptors.add
			(createChildParameter
				(ClassifiersPackage.Literals.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS,
				 ClassifiersFactory.eINSTANCE.createISAInterface()));

		newChildDescriptors.add
			(createChildParameter
				(ClassifiersPackage.Literals.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS,
				 ClassifiersFactory.eINSTANCE.createISAEnum()));

		newChildDescriptors.add
			(createChildParameter
				(ClassifiersPackage.Literals.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS,
				 ClassifiersFactory.eINSTANCE.createISARecord()));

		newChildDescriptors.add
			(createChildParameter
				(ClassifiersPackage.Literals.ISA_CLASSIFIER_CONTAINER__CLASSIFIERS,
				 LibrariesFactory.eINSTANCE.createISALibraryClassifier()));

		newChildDescriptors.add
			(createChildParameter
				(ClassifiersPackage.Literals.ISA_CLASSIFIER__FIELDS,
				 MembersFactory.eINSTANCE.createISAField()));

		newChildDescriptors.add
			(createChildParameter
				(ClassifiersPackage.Literals.ISA_CLASSIFIER__METHODS,
				 MembersFactory.eINSTANCE.createISAConcreteMethod()));

		newChildDescriptors.add
			(createChildParameter
				(ClassifiersPackage.Literals.ISA_CLASSIFIER__METHODS,
				 MembersFactory.eINSTANCE.createISAAbstractMethod()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return SoftwareSystemStructuralModelEditPlugin.INSTANCE;
	}

}

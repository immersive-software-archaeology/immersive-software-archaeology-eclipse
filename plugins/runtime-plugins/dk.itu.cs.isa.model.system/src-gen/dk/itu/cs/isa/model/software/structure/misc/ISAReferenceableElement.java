/**
 */
package dk.itu.cs.isa.model.software.structure.misc;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Referenceable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement#getIncomingReferences <em>Incoming References</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReferenceableElement()
 * @model abstract="true"
 * @generated
 */
public interface ISAReferenceableElement extends ISANamedSoftwareElement {
	/**
	 * Returns the value of the '<em><b>Incoming References</b></em>' reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISAReference}<code>&lt;?&gt;</code>.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReference#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming References</em>' reference list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReferenceableElement_IncomingReferences()
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReference#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<ISAReference<?>> getIncomingReferences();

} // ISAReferenceableElement

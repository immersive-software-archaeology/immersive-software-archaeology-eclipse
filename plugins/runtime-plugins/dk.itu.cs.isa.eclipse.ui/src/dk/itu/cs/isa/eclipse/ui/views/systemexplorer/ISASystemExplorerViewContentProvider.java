package dk.itu.cs.isa.eclipse.ui.views.systemexplorer;

import org.eclipse.jface.viewers.ITreeContentProvider;

import dk.itu.cs.isa.model.visualization.ISASpaceStation;
import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;
import dk.itu.cs.isa.preferences.ISAPreferences;
import dk.itu.cs.isa.storage.ISAModelStorage;

public class ISASystemExplorerViewContentProvider implements ITreeContentProvider {
	
	private ISASystemExplorerView parentView;
	private ISASystemExplorerNode invisibleRoot;
	
	

	public ISASystemExplorerViewContentProvider(ISASystemExplorerView parentView) {
		this.parentView = parentView;
	}
	
	

	@Override
	public Object[] getElements(Object parent) {
		if (parent.equals(parentView.getViewSite())) {
			if (invisibleRoot == null)
				initialize();
			return getChildren(invisibleRoot);
		}
		return getChildren(parent);
	}

	@Override
	public Object getParent(Object child) {
		if (child instanceof ISASystemExplorerNode) {
			return ((ISASystemExplorerNode) child).getParent();
		}
		return null;
	}

	@Override
	public Object[] getChildren(Object parent) {
		if (parent instanceof ISASystemExplorerNode) {
			return ((ISASystemExplorerNode) parent).getChildren();
		}
		return new Object[0];
	}

	@Override
	public boolean hasChildren(Object parent) {
		if (parent instanceof ISASystemExplorerNode)
			return ((ISASystemExplorerNode) parent).hasChildren();
		return false;
	}
	
	
	

	public void initialize() {
		if(!doInitialize())
			invisibleRoot.addChild(new ISASystemExplorerNode("Nothing to display"));
	}

	private boolean doInitialize() {
		invisibleRoot = new ISASystemExplorerNode("");
		
		String nameOfModelToLoad = ISAPreferences.getString(ISAPreferences.LAST_MODEL_NAME, null);
		ISAVisualizationModel model;
		try {
			model = ISAModelStorage.INSTANCE.loadVisualizationModel(nameOfModelToLoad);
			if(model == null)
				return false;
		} catch (Exception e) {
			return false;
		}
		
		ISASystemExplorerNode systemNode = new ISASystemExplorerNode(model);
		invisibleRoot.addChild(systemNode);
		
		doInitialize(systemNode, model.getSubjectSystemCapsule());
		return true;
	}
	
	private void doInitialize(ISASystemExplorerNode parentNode, ISATransparentCapsule capsule) {
		ISASystemExplorerNode capsuleNode = new ISASystemExplorerNode(capsule);
		parentNode.addChild(capsuleNode);

		for(ISATransparentCapsule childCapsule : capsule.getSubCapsules()) {
			doInitialize(capsuleNode, childCapsule);
		}
		
		for(ISASpaceStation station : capsule.getSpaceStations()) {
			doInitialize(capsuleNode, station);
		}
	}
	
	private void doInitialize(ISASystemExplorerNode parentNode, ISASpaceStation station) {
		ISASystemExplorerNode stationNode = new ISASystemExplorerNode(station);
		parentNode.addChild(stationNode);
		
		for(ISASpaceStation satelliteStation : station.getSatelliteStations()) {
			doInitialize(stationNode, satelliteStation);
		}
	}
}















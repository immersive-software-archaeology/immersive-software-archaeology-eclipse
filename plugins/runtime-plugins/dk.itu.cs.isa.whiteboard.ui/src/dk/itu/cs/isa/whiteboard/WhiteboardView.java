package dk.itu.cs.isa.whiteboard;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IActionBars;

import dk.itu.cs.isa.coalescence.server.ISACoalescenceServer;
import dk.itu.cs.isa.coalescence.server.ISAServer.ServerState;
import dk.itu.cs.isa.eclipse.ui.ISAUpdatableView;
import dk.itu.cs.isa.eclipse.ui.ImageHelper;
import dk.itu.cs.isa.eclipse.ui.SystemModelsObserver;
import dk.itu.cs.isa.eclipse.ui.views.ISAHTMLView;
import dk.itu.cs.isa.eclipse.ui.views.systemexplorer.ISASystemExplorerView;
import dk.itu.cs.isa.preferences.ISAPreferences;
import dk.itu.cs.isa.whiteboard.WhiteboardStorage.WhiteboardStorageObserver;

public class WhiteboardView extends ISAHTMLView implements ISAUpdatableView, WhiteboardStorageObserver {

	private Action nextWhiteboardAction;
	private Action previousWhiteboardAction;
	
	private int currentWhiteboardIndex = 0;
	
	
	
	public WhiteboardView() {
		SystemModelsObserver.registerView(this);
		ISASystemExplorerView.registerSystemSelectionListener(this);
		WhiteboardStorage.INSTANCE.addObserver(this);
	}
	


	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		makeActions();
		contributeToActionBar();
		update();
	}

	@Override
	public void dispose() {
		super.dispose();
		WhiteboardStorage.INSTANCE.removeObserver(this);
	}



	@Override
	public void update() {
		if(browser == null)
			return;
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(100);
					
					Display.getDefault().asyncExec(() -> {
						if(ISACoalescenceServer.INSTANCE.getServerState().equals(ServerState.ONLINE)
								|| ISACoalescenceServer.INSTANCE.getServerState().equals(ServerState.STARTING)) {
							String url = "http://localhost:9005/whiteboard/display/"
									+ "?systemName=" + ISAPreferences.getString(ISAPreferences.LAST_MODEL_NAME, null)
									+ "&index=" + currentWhiteboardIndex;
							browser.setUrl(url);
						}
						else {
							browser.setText(getHTMLCode());
						}
					});
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).run();
	}

	@Override
	protected String getHTMLCode() {
		return "<div style=\"font-family: 'Open Sans', Sans-serif; padding: 50px;\">"
					+ "Whiteboards cannot be displayed at the moment "
					+ "because <strong>the ISA coalescence server is currently offline</strong>"
				+ "</div>";
	}
	
	
	
	private void makeActions() {
		nextWhiteboardAction = new Action() {
			public void run() {
				String currentSystem = ISAPreferences.getString(ISAPreferences.LAST_MODEL_NAME, null);
				
				currentWhiteboardIndex ++;
				if(currentWhiteboardIndex >= WhiteboardStorage.INSTANCE.getNumberOfWhiteboardsForSystem(currentSystem))
					currentWhiteboardIndex = 0;
				
				update();
			}
		};
		nextWhiteboardAction.setText("Next");
		nextWhiteboardAction.setToolTipText("Switch to the next whiteboard");
		nextWhiteboardAction.setImageDescriptor(ImageHelper.getImageDescriptor("dk.itu.cs.isa.whiteboard.ui", "icons/icon-16-whiteboard-next.png"));

		previousWhiteboardAction = new Action() {
			public void run() {
				String currentSystem = ISAPreferences.getString(ISAPreferences.LAST_MODEL_NAME, null);
				
				currentWhiteboardIndex --;
				if(currentWhiteboardIndex < 0)
					currentWhiteboardIndex = WhiteboardStorage.INSTANCE.getNumberOfWhiteboardsForSystem(currentSystem) - 1;
				if(currentWhiteboardIndex < 0)
					currentWhiteboardIndex = 0;
				
				update();
			}
		};
		previousWhiteboardAction.setText("Previous");
		previousWhiteboardAction.setToolTipText("Switch to the previous whiteboard");
		previousWhiteboardAction.setImageDescriptor(ImageHelper.getImageDescriptor("dk.itu.cs.isa.whiteboard.ui", "icons/icon-16-whiteboard-previous.png"));
	}

	private void contributeToActionBar() {
		IActionBars bars = getViewSite().getActionBars();
		IToolBarManager manager = bars.getToolBarManager();

		manager.add(previousWhiteboardAction);
		manager.add(nextWhiteboardAction);
		manager.add(new Separator());
	}
	
	
	
	@Override
	public void notifyOnSave() {
		update();
	}
	
}
















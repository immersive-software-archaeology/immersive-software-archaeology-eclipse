package dk.itu.cs.isa.model.software.structure.libraries.impl;

import dk.itu.cs.isa.model.software.structure.libraries.ISALibrary;

public class LibrariesFactoryImplCustom extends LibrariesFactoryImpl {

	@Override
	public ISALibrary createISALibrary() {
		return new ISALibraryImplCustom();
	}
	
}

/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.AbstractCompoundOperation;
import dk.itu.cs.isa.whiteboard.AbstractOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Compound Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractCompoundOperationImpl#getSubOperations <em>Sub Operations</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractCompoundOperationImpl extends AbstractOperationImpl implements AbstractCompoundOperation {
	/**
	 * The cached value of the '{@link #getSubOperations() <em>Sub Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractOperation> subOperations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractCompoundOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.ABSTRACT_COMPOUND_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractOperation> getSubOperations() {
		if (subOperations == null) {
			subOperations = new EObjectContainmentEList<AbstractOperation>(AbstractOperation.class, this, WhiteboardPackage.ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS);
		}
		return subOperations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS:
				return ((InternalEList<?>)getSubOperations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS:
				return getSubOperations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS:
				getSubOperations().clear();
				getSubOperations().addAll((Collection<? extends AbstractOperation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS:
				getSubOperations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS:
				return subOperations != null && !subOperations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AbstractCompoundOperationImpl

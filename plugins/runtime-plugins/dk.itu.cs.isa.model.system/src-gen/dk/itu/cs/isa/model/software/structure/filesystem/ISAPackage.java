/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage#getParentContainer <em>Parent Container</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAPackage()
 * @model
 * @generated
 */
public interface ISAPackage extends ISAPackageContainer, ISAClassifierContainer {
	/**
	 * Returns the value of the '<em><b>Parent Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer#getSubPackages <em>Sub Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Container</em>' container reference.
	 * @see #setParentContainer(ISAPackageContainer)
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAPackage_ParentContainer()
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer#getSubPackages
	 * @model opposite="subPackages" required="true" transient="false"
	 * @generated
	 */
	ISAPackageContainer getParentContainer();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage#getParentContainer <em>Parent Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Container</em>' container reference.
	 * @see #getParentContainer()
	 * @generated
	 */
	void setParentContainer(ISAPackageContainer value);

} // ISAPackage

/**
 */
package dk.itu.cs.isa.model.visualization;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Space Station Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#getHeight <em>Height</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#getDiameter <em>Diameter</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isConstructor <em>Constructor</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isPublic <em>Public</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isSystemEntry <em>System Entry</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationBlock()
 * @model
 * @generated
 */
public interface ISASpaceStationBlock extends ISAAbstractSpaceStationComponent {
	/**
	 * Returns the value of the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Height</em>' attribute.
	 * @see #setHeight(float)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationBlock_Height()
	 * @model required="true"
	 * @generated
	 */
	float getHeight();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#getHeight <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Height</em>' attribute.
	 * @see #getHeight()
	 * @generated
	 */
	void setHeight(float value);

	/**
	 * Returns the value of the '<em><b>Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diameter</em>' attribute.
	 * @see #setDiameter(float)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationBlock_Diameter()
	 * @model required="true"
	 * @generated
	 */
	float getDiameter();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#getDiameter <em>Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diameter</em>' attribute.
	 * @see #getDiameter()
	 * @generated
	 */
	void setDiameter(float value);

	/**
	 * Returns the value of the '<em><b>Constructor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constructor</em>' attribute.
	 * @see #setConstructor(boolean)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationBlock_Constructor()
	 * @model required="true"
	 * @generated
	 */
	boolean isConstructor();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isConstructor <em>Constructor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constructor</em>' attribute.
	 * @see #isConstructor()
	 * @generated
	 */
	void setConstructor(boolean value);

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationBlock_Abstract()
	 * @model required="true"
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Public</em>' attribute.
	 * @see #setPublic(boolean)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationBlock_Public()
	 * @model required="true"
	 * @generated
	 */
	boolean isPublic();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isPublic <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Public</em>' attribute.
	 * @see #isPublic()
	 * @generated
	 */
	void setPublic(boolean value);

	/**
	 * Returns the value of the '<em><b>System Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Entry</em>' attribute.
	 * @see #setSystemEntry(boolean)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISASpaceStationBlock_SystemEntry()
	 * @model required="true"
	 * @generated
	 */
	boolean isSystemEntry();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isSystemEntry <em>System Entry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Entry</em>' attribute.
	 * @see #isSystemEntry()
	 * @generated
	 */
	void setSystemEntry(boolean value);

} // ISASpaceStationBlock

/**
 */
package dk.itu.cs.isa.model.system.quality;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.system.quality.QualityFactory
 * @model kind="package"
 * @generated
 */
public interface QualityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "quality";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/software/quality";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "quality";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	QualityPackage eINSTANCE = dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISASoftwareSystemQualityModelImpl <em>ISA Software System Quality Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.system.quality.impl.ISASoftwareSystemQualityModelImpl
	 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISASoftwareSystemQualityModel()
	 * @generated
	 */
	int ISA_SOFTWARE_SYSTEM_QUALITY_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Classifier Entries</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__CLASSIFIER_ENTRIES = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__NAME = 1;

	/**
	 * The number of structural features of the '<em>ISA Software System Quality Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_QUALITY_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>ISA Software System Quality Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_QUALITY_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISAClassifierQualityEntryImpl <em>ISA Classifier Quality Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.system.quality.impl.ISAClassifierQualityEntryImpl
	 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAClassifierQualityEntry()
	 * @generated
	 */
	int ISA_CLASSIFIER_QUALITY_ENTRY = 1;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_QUALITY_ENTRY__CLASSIFIER = 0;

	/**
	 * The feature id for the '<em><b>Measures</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES = 1;

	/**
	 * The feature id for the '<em><b>Issues</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES = 2;

	/**
	 * The number of structural features of the '<em>ISA Classifier Quality Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_QUALITY_ENTRY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>ISA Classifier Quality Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_CLASSIFIER_QUALITY_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISAMeasureImpl <em>ISA Measure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.system.quality.impl.ISAMeasureImpl
	 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAMeasure()
	 * @generated
	 */
	int ISA_MEASURE = 2;

	/**
	 * The feature id for the '<em><b>Parent Entry</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEASURE__PARENT_ENTRY = 0;

	/**
	 * The feature id for the '<em><b>Metric</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEASURE__METRIC = 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEASURE__VALUE = 2;

	/**
	 * The feature id for the '<em><b>Best Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEASURE__BEST_VALUE = 3;

	/**
	 * The number of structural features of the '<em>ISA Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEASURE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>ISA Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MEASURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl <em>ISA Issue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl
	 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAIssue()
	 * @generated
	 */
	int ISA_ISSUE = 3;

	/**
	 * The feature id for the '<em><b>Parent Entry</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ISSUE__PARENT_ENTRY = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ISSUE__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ISSUE__NAME = 2;

	/**
	 * The feature id for the '<em><b>Occurence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ISSUE__OCCURENCE = 3;

	/**
	 * The feature id for the '<em><b>Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ISSUE__SEVERITY = 4;

	/**
	 * The feature id for the '<em><b>Hint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ISSUE__HINT = 5;

	/**
	 * The feature id for the '<em><b>Effort To Fix In Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ISSUE__EFFORT_TO_FIX_IN_MINUTES = 6;

	/**
	 * The number of structural features of the '<em>ISA Issue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ISSUE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>ISA Issue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ISSUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISAFilePassageImpl <em>ISA File Passage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.system.quality.impl.ISAFilePassageImpl
	 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAFilePassage()
	 * @generated
	 */
	int ISA_FILE_PASSAGE = 4;

	/**
	 * The feature id for the '<em><b>Start Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FILE_PASSAGE__START_LINE = 0;

	/**
	 * The feature id for the '<em><b>End Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FILE_PASSAGE__END_LINE = 1;

	/**
	 * The feature id for the '<em><b>Start Char</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FILE_PASSAGE__START_CHAR = 2;

	/**
	 * The feature id for the '<em><b>End Char</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FILE_PASSAGE__END_CHAR = 3;

	/**
	 * The number of structural features of the '<em>ISA File Passage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FILE_PASSAGE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>ISA File Passage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_FILE_PASSAGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.system.quality.ISAQualityIssueType <em>ISA Quality Issue Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueType
	 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAQualityIssueType()
	 * @generated
	 */
	int ISA_QUALITY_ISSUE_TYPE = 5;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity <em>ISA Quality Issue Severity</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity
	 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAQualityIssueSeverity()
	 * @generated
	 */
	int ISA_QUALITY_ISSUE_SEVERITY = 6;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel <em>ISA Software System Quality Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Software System Quality Model</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel
	 * @generated
	 */
	EClass getISASoftwareSystemQualityModel();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel#getClassifierEntries <em>Classifier Entries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classifier Entries</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel#getClassifierEntries()
	 * @see #getISASoftwareSystemQualityModel()
	 * @generated
	 */
	EReference getISASoftwareSystemQualityModel_ClassifierEntries();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel#getName()
	 * @see #getISASoftwareSystemQualityModel()
	 * @generated
	 */
	EAttribute getISASoftwareSystemQualityModel_Name();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry <em>ISA Classifier Quality Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Classifier Quality Entry</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry
	 * @generated
	 */
	EClass getISAClassifierQualityEntry();

	/**
	 * Returns the meta object for the reference '{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Classifier</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getClassifier()
	 * @see #getISAClassifierQualityEntry()
	 * @generated
	 */
	EReference getISAClassifierQualityEntry_Classifier();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getMeasures <em>Measures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Measures</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getMeasures()
	 * @see #getISAClassifierQualityEntry()
	 * @generated
	 */
	EReference getISAClassifierQualityEntry_Measures();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getIssues <em>Issues</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Issues</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getIssues()
	 * @see #getISAClassifierQualityEntry()
	 * @generated
	 */
	EReference getISAClassifierQualityEntry_Issues();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure <em>ISA Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Measure</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAMeasure
	 * @generated
	 */
	EClass getISAMeasure();

	/**
	 * Returns the meta object for the container reference '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getParentEntry <em>Parent Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Entry</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAMeasure#getParentEntry()
	 * @see #getISAMeasure()
	 * @generated
	 */
	EReference getISAMeasure_ParentEntry();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getMetric <em>Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metric</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAMeasure#getMetric()
	 * @see #getISAMeasure()
	 * @generated
	 */
	EAttribute getISAMeasure_Metric();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAMeasure#getValue()
	 * @see #getISAMeasure()
	 * @generated
	 */
	EAttribute getISAMeasure_Value();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#isBestValue <em>Best Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Value</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAMeasure#isBestValue()
	 * @see #getISAMeasure()
	 * @generated
	 */
	EAttribute getISAMeasure_BestValue();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.system.quality.ISAIssue <em>ISA Issue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Issue</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue
	 * @generated
	 */
	EClass getISAIssue();

	/**
	 * Returns the meta object for the container reference '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getParentEntry <em>Parent Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Entry</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue#getParentEntry()
	 * @see #getISAIssue()
	 * @generated
	 */
	EReference getISAIssue_ParentEntry();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue#getType()
	 * @see #getISAIssue()
	 * @generated
	 */
	EAttribute getISAIssue_Type();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue#getName()
	 * @see #getISAIssue()
	 * @generated
	 */
	EAttribute getISAIssue_Name();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getOccurence <em>Occurence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Occurence</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue#getOccurence()
	 * @see #getISAIssue()
	 * @generated
	 */
	EReference getISAIssue_Occurence();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getSeverity <em>Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Severity</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue#getSeverity()
	 * @see #getISAIssue()
	 * @generated
	 */
	EAttribute getISAIssue_Severity();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getHint <em>Hint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hint</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue#getHint()
	 * @see #getISAIssue()
	 * @generated
	 */
	EAttribute getISAIssue_Hint();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getEffortToFixInMinutes <em>Effort To Fix In Minutes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Effort To Fix In Minutes</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAIssue#getEffortToFixInMinutes()
	 * @see #getISAIssue()
	 * @generated
	 */
	EAttribute getISAIssue_EffortToFixInMinutes();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage <em>ISA File Passage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA File Passage</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAFilePassage
	 * @generated
	 */
	EClass getISAFilePassage();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getStartLine <em>Start Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Line</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAFilePassage#getStartLine()
	 * @see #getISAFilePassage()
	 * @generated
	 */
	EAttribute getISAFilePassage_StartLine();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getEndLine <em>End Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Line</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAFilePassage#getEndLine()
	 * @see #getISAFilePassage()
	 * @generated
	 */
	EAttribute getISAFilePassage_EndLine();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getStartChar <em>Start Char</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Char</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAFilePassage#getStartChar()
	 * @see #getISAFilePassage()
	 * @generated
	 */
	EAttribute getISAFilePassage_StartChar();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getEndChar <em>End Char</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Char</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAFilePassage#getEndChar()
	 * @see #getISAFilePassage()
	 * @generated
	 */
	EAttribute getISAFilePassage_EndChar();

	/**
	 * Returns the meta object for enum '{@link dk.itu.cs.isa.model.system.quality.ISAQualityIssueType <em>ISA Quality Issue Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ISA Quality Issue Type</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueType
	 * @generated
	 */
	EEnum getISAQualityIssueType();

	/**
	 * Returns the meta object for enum '{@link dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity <em>ISA Quality Issue Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ISA Quality Issue Severity</em>'.
	 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity
	 * @generated
	 */
	EEnum getISAQualityIssueSeverity();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	QualityFactory getQualityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISASoftwareSystemQualityModelImpl <em>ISA Software System Quality Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.system.quality.impl.ISASoftwareSystemQualityModelImpl
		 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISASoftwareSystemQualityModel()
		 * @generated
		 */
		EClass ISA_SOFTWARE_SYSTEM_QUALITY_MODEL = eINSTANCE.getISASoftwareSystemQualityModel();

		/**
		 * The meta object literal for the '<em><b>Classifier Entries</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__CLASSIFIER_ENTRIES = eINSTANCE.getISASoftwareSystemQualityModel_ClassifierEntries();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__NAME = eINSTANCE.getISASoftwareSystemQualityModel_Name();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISAClassifierQualityEntryImpl <em>ISA Classifier Quality Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.system.quality.impl.ISAClassifierQualityEntryImpl
		 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAClassifierQualityEntry()
		 * @generated
		 */
		EClass ISA_CLASSIFIER_QUALITY_ENTRY = eINSTANCE.getISAClassifierQualityEntry();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASSIFIER_QUALITY_ENTRY__CLASSIFIER = eINSTANCE.getISAClassifierQualityEntry_Classifier();

		/**
		 * The meta object literal for the '<em><b>Measures</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES = eINSTANCE.getISAClassifierQualityEntry_Measures();

		/**
		 * The meta object literal for the '<em><b>Issues</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES = eINSTANCE.getISAClassifierQualityEntry_Issues();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISAMeasureImpl <em>ISA Measure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.system.quality.impl.ISAMeasureImpl
		 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAMeasure()
		 * @generated
		 */
		EClass ISA_MEASURE = eINSTANCE.getISAMeasure();

		/**
		 * The meta object literal for the '<em><b>Parent Entry</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_MEASURE__PARENT_ENTRY = eINSTANCE.getISAMeasure_ParentEntry();

		/**
		 * The meta object literal for the '<em><b>Metric</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_MEASURE__METRIC = eINSTANCE.getISAMeasure_Metric();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_MEASURE__VALUE = eINSTANCE.getISAMeasure_Value();

		/**
		 * The meta object literal for the '<em><b>Best Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_MEASURE__BEST_VALUE = eINSTANCE.getISAMeasure_BestValue();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl <em>ISA Issue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.system.quality.impl.ISAIssueImpl
		 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAIssue()
		 * @generated
		 */
		EClass ISA_ISSUE = eINSTANCE.getISAIssue();

		/**
		 * The meta object literal for the '<em><b>Parent Entry</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_ISSUE__PARENT_ENTRY = eINSTANCE.getISAIssue_ParentEntry();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_ISSUE__TYPE = eINSTANCE.getISAIssue_Type();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_ISSUE__NAME = eINSTANCE.getISAIssue_Name();

		/**
		 * The meta object literal for the '<em><b>Occurence</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_ISSUE__OCCURENCE = eINSTANCE.getISAIssue_Occurence();

		/**
		 * The meta object literal for the '<em><b>Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_ISSUE__SEVERITY = eINSTANCE.getISAIssue_Severity();

		/**
		 * The meta object literal for the '<em><b>Hint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_ISSUE__HINT = eINSTANCE.getISAIssue_Hint();

		/**
		 * The meta object literal for the '<em><b>Effort To Fix In Minutes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_ISSUE__EFFORT_TO_FIX_IN_MINUTES = eINSTANCE.getISAIssue_EffortToFixInMinutes();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.system.quality.impl.ISAFilePassageImpl <em>ISA File Passage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.system.quality.impl.ISAFilePassageImpl
		 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAFilePassage()
		 * @generated
		 */
		EClass ISA_FILE_PASSAGE = eINSTANCE.getISAFilePassage();

		/**
		 * The meta object literal for the '<em><b>Start Line</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_FILE_PASSAGE__START_LINE = eINSTANCE.getISAFilePassage_StartLine();

		/**
		 * The meta object literal for the '<em><b>End Line</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_FILE_PASSAGE__END_LINE = eINSTANCE.getISAFilePassage_EndLine();

		/**
		 * The meta object literal for the '<em><b>Start Char</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_FILE_PASSAGE__START_CHAR = eINSTANCE.getISAFilePassage_StartChar();

		/**
		 * The meta object literal for the '<em><b>End Char</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_FILE_PASSAGE__END_CHAR = eINSTANCE.getISAFilePassage_EndChar();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.system.quality.ISAQualityIssueType <em>ISA Quality Issue Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueType
		 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAQualityIssueType()
		 * @generated
		 */
		EEnum ISA_QUALITY_ISSUE_TYPE = eINSTANCE.getISAQualityIssueType();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity <em>ISA Quality Issue Severity</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity
		 * @see dk.itu.cs.isa.model.system.quality.impl.QualityPackageImpl#getISAQualityIssueSeverity()
		 * @generated
		 */
		EEnum ISA_QUALITY_ISSUE_SEVERITY = eINSTANCE.getISAQualityIssueSeverity();

	}

} //QualityPackage

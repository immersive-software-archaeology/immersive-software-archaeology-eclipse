package dk.itu.cs.isa.model.system.quality.sonarqube.ui.terminal;

import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import dk.itu.cs.isa.eclipse.ui.teminal.ISAConsoleUtil;
import dk.itu.cs.isa.model.quality.sonarqube.processes.io.ISonarProcessOutputConsumer;

public class SonarScannerConsole implements ISonarProcessOutputConsumer {
	
	private MessageConsole console;
	private MessageConsoleStream consoleStream;
	
	
	
	public SonarScannerConsole(String consoleName) {
		this.console = ISAConsoleUtil.getConsole(consoleName);
		this.consoleStream = console.newMessageStream();
	}
	
	
	
	@Override
	public void startNewRun() {
		console.clearConsole();
	}

	@Override
	public void processOutputLine(String line) {
		console.activate();
		consoleStream.println(line);
	}

	@Override
	public void processErrorLine(String line) {
		console.activate();
		consoleStream.println(line);
	}
	
}

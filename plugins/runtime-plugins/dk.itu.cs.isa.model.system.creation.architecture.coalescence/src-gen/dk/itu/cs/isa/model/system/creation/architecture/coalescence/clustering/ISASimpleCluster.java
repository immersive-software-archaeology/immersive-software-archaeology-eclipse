/**
 */
package dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Simple Cluster</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster#getElementNames <em>Element Names</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster#getSubClusters <em>Sub Clusters</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringPackage#getISASimpleCluster()
 * @model
 * @generated
 */
public interface ISASimpleCluster extends EObject {
	/**
	 * Returns the value of the '<em><b>Element Names</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Names</em>' attribute list.
	 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringPackage#getISASimpleCluster_ElementNames()
	 * @model
	 * @generated
	 */
	EList<String> getElementNames();

	/**
	 * Returns the value of the '<em><b>Sub Clusters</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Clusters</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringPackage#getISASimpleCluster_SubClusters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISASimpleCluster> getSubClusters();

} // ISASimpleCluster

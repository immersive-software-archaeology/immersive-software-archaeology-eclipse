/**
 */
package dk.itu.cs.isa.model.software.structure.members;

import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Constructor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAConstructor()
 * @model
 * @generated
 */
public interface ISAConstructor extends ISAMember, ISAStatementContainer, ISAParametrizableElement {
} // ISAConstructor

/**
 */
package dk.itu.cs.isa.multiplayer;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.multiplayer.MultiplayerFactory
 * @model kind="package"
 * @generated
 */
public interface MultiplayerPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "multiplayer";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/multiplayer";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "multiplayer";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MultiplayerPackage eINSTANCE = dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.EventLogImpl <em>Event Log</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.EventLogImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getEventLog()
	 * @generated
	 */
	int EVENT_LOG = 0;

	/**
	 * The feature id for the '<em><b>Log</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_LOG__LOG = 0;

	/**
	 * The number of structural features of the '<em>Event Log</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_LOG_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Event Log</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_LOG_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.AbstractEventImpl <em>Abstract Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.AbstractEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getAbstractEvent()
	 * @generated
	 */
	int ABSTRACT_EVENT = 1;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_EVENT__TIMESTAMP = 0;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_EVENT__SYSTEM_NAME = 1;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_EVENT__CLIENT_ID = 2;

	/**
	 * The number of structural features of the '<em>Abstract Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_EVENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Abstract Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.ShowRelationEventImpl <em>Show Relation Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.ShowRelationEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getShowRelationEvent()
	 * @generated
	 */
	int SHOW_RELATION_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_RELATION_EVENT__TIMESTAMP = ABSTRACT_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_RELATION_EVENT__SYSTEM_NAME = ABSTRACT_EVENT__SYSTEM_NAME;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_RELATION_EVENT__CLIENT_ID = ABSTRACT_EVENT__CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_RELATION_EVENT__ELEMENT_NAME = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Relation Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_RELATION_EVENT__RELATION_TYPE = ABSTRACT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_RELATION_EVENT__DIRECTION = ABSTRACT_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Show Relation Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_RELATION_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Show Relation Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_RELATION_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.ClearOneRelationEventImpl <em>Clear One Relation Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.ClearOneRelationEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getClearOneRelationEvent()
	 * @generated
	 */
	int CLEAR_ONE_RELATION_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ONE_RELATION_EVENT__TIMESTAMP = ABSTRACT_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ONE_RELATION_EVENT__SYSTEM_NAME = ABSTRACT_EVENT__SYSTEM_NAME;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ONE_RELATION_EVENT__CLIENT_ID = ABSTRACT_EVENT__CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ONE_RELATION_EVENT__ELEMENT_NAME = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Relation Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ONE_RELATION_EVENT__RELATION_TYPE = ABSTRACT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ONE_RELATION_EVENT__DIRECTION = ABSTRACT_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Clear One Relation Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ONE_RELATION_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Clear One Relation Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ONE_RELATION_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.ClearRelationsEventImpl <em>Clear Relations Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.ClearRelationsEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getClearRelationsEvent()
	 * @generated
	 */
	int CLEAR_RELATIONS_EVENT = 4;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_RELATIONS_EVENT__TIMESTAMP = ABSTRACT_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_RELATIONS_EVENT__SYSTEM_NAME = ABSTRACT_EVENT__SYSTEM_NAME;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_RELATIONS_EVENT__CLIENT_ID = ABSTRACT_EVENT__CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_RELATIONS_EVENT__ELEMENT_NAME = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Clear Relations Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_RELATIONS_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Clear Relations Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_RELATIONS_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.ClearAllRelationsEventImpl <em>Clear All Relations Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.ClearAllRelationsEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getClearAllRelationsEvent()
	 * @generated
	 */
	int CLEAR_ALL_RELATIONS_EVENT = 5;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ALL_RELATIONS_EVENT__TIMESTAMP = ABSTRACT_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ALL_RELATIONS_EVENT__SYSTEM_NAME = ABSTRACT_EVENT__SYSTEM_NAME;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ALL_RELATIONS_EVENT__CLIENT_ID = ABSTRACT_EVENT__CLIENT_ID;

	/**
	 * The number of structural features of the '<em>Clear All Relations Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ALL_RELATIONS_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Clear All Relations Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLEAR_ALL_RELATIONS_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.ChangeElementActiveStateEventImpl <em>Change Element Active State Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.ChangeElementActiveStateEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getChangeElementActiveStateEvent()
	 * @generated
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT = 6;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT__TIMESTAMP = ABSTRACT_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT__SYSTEM_NAME = ABSTRACT_EVENT__SYSTEM_NAME;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT__CLIENT_ID = ABSTRACT_EVENT__CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Element Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_TYPE = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Element Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_ID = ABSTRACT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>New State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT__NEW_STATE = ABSTRACT_EVENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION = ABSTRACT_EVENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION = ABSTRACT_EVENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Change Element Active State Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Change Element Active State Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_ELEMENT_ACTIVE_STATE_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.SystemMoveEventImpl <em>System Move Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.SystemMoveEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getSystemMoveEvent()
	 * @generated
	 */
	int SYSTEM_MOVE_EVENT = 7;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MOVE_EVENT__TIMESTAMP = ABSTRACT_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MOVE_EVENT__SYSTEM_NAME = ABSTRACT_EVENT__SYSTEM_NAME;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MOVE_EVENT__CLIENT_ID = ABSTRACT_EVENT__CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MOVE_EVENT__POSITION = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MOVE_EVENT__ROTATION = ABSTRACT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Scale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MOVE_EVENT__SCALE = ABSTRACT_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>System Move Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MOVE_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>System Move Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_MOVE_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.ChangeNameplateVisibilityEventImpl <em>Change Nameplate Visibility Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.ChangeNameplateVisibilityEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getChangeNameplateVisibilityEvent()
	 * @generated
	 */
	int CHANGE_NAMEPLATE_VISIBILITY_EVENT = 8;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_NAMEPLATE_VISIBILITY_EVENT__TIMESTAMP = ABSTRACT_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_NAMEPLATE_VISIBILITY_EVENT__SYSTEM_NAME = ABSTRACT_EVENT__SYSTEM_NAME;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_NAMEPLATE_VISIBILITY_EVENT__CLIENT_ID = ABSTRACT_EVENT__CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_NAMEPLATE_VISIBILITY_EVENT__VISIBLE = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Change Nameplate Visibility Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_NAMEPLATE_VISIBILITY_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Change Nameplate Visibility Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_NAMEPLATE_VISIBILITY_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.SphereOpenCloseEventImpl <em>Sphere Open Close Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.SphereOpenCloseEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getSphereOpenCloseEvent()
	 * @generated
	 */
	int SPHERE_OPEN_CLOSE_EVENT = 9;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPHERE_OPEN_CLOSE_EVENT__TIMESTAMP = ABSTRACT_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPHERE_OPEN_CLOSE_EVENT__SYSTEM_NAME = ABSTRACT_EVENT__SYSTEM_NAME;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPHERE_OPEN_CLOSE_EVENT__CLIENT_ID = ABSTRACT_EVENT__CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPHERE_OPEN_CLOSE_EVENT__ELEMENT_NAME = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Now Open</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPHERE_OPEN_CLOSE_EVENT__NOW_OPEN = ABSTRACT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sphere Open Close Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPHERE_OPEN_CLOSE_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Sphere Open Close Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPHERE_OPEN_CLOSE_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.AudioEventImpl <em>Audio Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.AudioEventImpl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getAudioEvent()
	 * @generated
	 */
	int AUDIO_EVENT = 10;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIO_EVENT__TIMESTAMP = ABSTRACT_EVENT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIO_EVENT__SYSTEM_NAME = ABSTRACT_EVENT__SYSTEM_NAME;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIO_EVENT__CLIENT_ID = ABSTRACT_EVENT__CLIENT_ID;

	/**
	 * The feature id for the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIO_EVENT__PIN_ID = ABSTRACT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Play</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIO_EVENT__PLAY = ABSTRACT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Playback Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIO_EVENT__PLAYBACK_TIME = ABSTRACT_EVENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Audio Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIO_EVENT_FEATURE_COUNT = ABSTRACT_EVENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Audio Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUDIO_EVENT_OPERATION_COUNT = ABSTRACT_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.impl.ISAVector3Impl <em>ISA Vector3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.impl.ISAVector3Impl
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getISAVector3()
	 * @generated
	 */
	int ISA_VECTOR3 = 11;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__Z = 2;

	/**
	 * The number of structural features of the '<em>ISA Vector3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>ISA Vector3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.RelationType <em>Relation Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.RelationType
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getRelationType()
	 * @generated
	 */
	int RELATION_TYPE = 12;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.Direction <em>Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.Direction
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getDirection()
	 * @generated
	 */
	int DIRECTION = 13;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.ElementType <em>Element Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.ElementType
	 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getElementType()
	 * @generated
	 */
	int ELEMENT_TYPE = 14;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.EventLog <em>Event Log</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Log</em>'.
	 * @see dk.itu.cs.isa.multiplayer.EventLog
	 * @generated
	 */
	EClass getEventLog();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.multiplayer.EventLog#getLog <em>Log</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Log</em>'.
	 * @see dk.itu.cs.isa.multiplayer.EventLog#getLog()
	 * @see #getEventLog()
	 * @generated
	 */
	EReference getEventLog_Log();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.AbstractEvent <em>Abstract Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.AbstractEvent
	 * @generated
	 */
	EClass getAbstractEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.AbstractEvent#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see dk.itu.cs.isa.multiplayer.AbstractEvent#getTimestamp()
	 * @see #getAbstractEvent()
	 * @generated
	 */
	EAttribute getAbstractEvent_Timestamp();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.AbstractEvent#getSystemName <em>System Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Name</em>'.
	 * @see dk.itu.cs.isa.multiplayer.AbstractEvent#getSystemName()
	 * @see #getAbstractEvent()
	 * @generated
	 */
	EAttribute getAbstractEvent_SystemName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.AbstractEvent#getClientId <em>Client Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Client Id</em>'.
	 * @see dk.itu.cs.isa.multiplayer.AbstractEvent#getClientId()
	 * @see #getAbstractEvent()
	 * @generated
	 */
	EAttribute getAbstractEvent_ClientId();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent <em>Show Relation Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Show Relation Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ShowRelationEvent
	 * @generated
	 */
	EClass getShowRelationEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent#getElementName <em>Element Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Name</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ShowRelationEvent#getElementName()
	 * @see #getShowRelationEvent()
	 * @generated
	 */
	EAttribute getShowRelationEvent_ElementName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent#getRelationType <em>Relation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Relation Type</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ShowRelationEvent#getRelationType()
	 * @see #getShowRelationEvent()
	 * @generated
	 */
	EAttribute getShowRelationEvent_RelationType();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ShowRelationEvent#getDirection()
	 * @see #getShowRelationEvent()
	 * @generated
	 */
	EAttribute getShowRelationEvent_Direction();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.ClearOneRelationEvent <em>Clear One Relation Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clear One Relation Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ClearOneRelationEvent
	 * @generated
	 */
	EClass getClearOneRelationEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ClearOneRelationEvent#getElementName <em>Element Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Name</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ClearOneRelationEvent#getElementName()
	 * @see #getClearOneRelationEvent()
	 * @generated
	 */
	EAttribute getClearOneRelationEvent_ElementName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ClearOneRelationEvent#getRelationType <em>Relation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Relation Type</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ClearOneRelationEvent#getRelationType()
	 * @see #getClearOneRelationEvent()
	 * @generated
	 */
	EAttribute getClearOneRelationEvent_RelationType();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ClearOneRelationEvent#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ClearOneRelationEvent#getDirection()
	 * @see #getClearOneRelationEvent()
	 * @generated
	 */
	EAttribute getClearOneRelationEvent_Direction();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.ClearRelationsEvent <em>Clear Relations Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clear Relations Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ClearRelationsEvent
	 * @generated
	 */
	EClass getClearRelationsEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ClearRelationsEvent#getElementName <em>Element Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Name</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ClearRelationsEvent#getElementName()
	 * @see #getClearRelationsEvent()
	 * @generated
	 */
	EAttribute getClearRelationsEvent_ElementName();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.ClearAllRelationsEvent <em>Clear All Relations Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clear All Relations Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ClearAllRelationsEvent
	 * @generated
	 */
	EClass getClearAllRelationsEvent();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent <em>Change Element Active State Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Change Element Active State Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent
	 * @generated
	 */
	EClass getChangeElementActiveStateEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getElementType <em>Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Type</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getElementType()
	 * @see #getChangeElementActiveStateEvent()
	 * @generated
	 */
	EAttribute getChangeElementActiveStateEvent_ElementType();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getElementId <em>Element Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Id</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getElementId()
	 * @see #getChangeElementActiveStateEvent()
	 * @generated
	 */
	EAttribute getChangeElementActiveStateEvent_ElementId();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#isNewState <em>New State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>New State</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#isNewState()
	 * @see #getChangeElementActiveStateEvent()
	 * @generated
	 */
	EAttribute getChangeElementActiveStateEvent_NewState();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Position</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getPosition()
	 * @see #getChangeElementActiveStateEvent()
	 * @generated
	 */
	EReference getChangeElementActiveStateEvent_Position();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getRotation <em>Rotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rotation</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent#getRotation()
	 * @see #getChangeElementActiveStateEvent()
	 * @generated
	 */
	EReference getChangeElementActiveStateEvent_Rotation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent <em>System Move Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Move Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.SystemMoveEvent
	 * @generated
	 */
	EClass getSystemMoveEvent();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Position</em>'.
	 * @see dk.itu.cs.isa.multiplayer.SystemMoveEvent#getPosition()
	 * @see #getSystemMoveEvent()
	 * @generated
	 */
	EReference getSystemMoveEvent_Position();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent#getRotation <em>Rotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rotation</em>'.
	 * @see dk.itu.cs.isa.multiplayer.SystemMoveEvent#getRotation()
	 * @see #getSystemMoveEvent()
	 * @generated
	 */
	EReference getSystemMoveEvent_Rotation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent#getScale <em>Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scale</em>'.
	 * @see dk.itu.cs.isa.multiplayer.SystemMoveEvent#getScale()
	 * @see #getSystemMoveEvent()
	 * @generated
	 */
	EAttribute getSystemMoveEvent_Scale();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent <em>Change Nameplate Visibility Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Change Nameplate Visibility Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent
	 * @generated
	 */
	EClass getChangeNameplateVisibilityEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent#isVisible <em>Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visible</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent#isVisible()
	 * @see #getChangeNameplateVisibilityEvent()
	 * @generated
	 */
	EAttribute getChangeNameplateVisibilityEvent_Visible();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent <em>Sphere Open Close Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sphere Open Close Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent
	 * @generated
	 */
	EClass getSphereOpenCloseEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent#getElementName <em>Element Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element Name</em>'.
	 * @see dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent#getElementName()
	 * @see #getSphereOpenCloseEvent()
	 * @generated
	 */
	EAttribute getSphereOpenCloseEvent_ElementName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent#isNowOpen <em>Now Open</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Now Open</em>'.
	 * @see dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent#isNowOpen()
	 * @see #getSphereOpenCloseEvent()
	 * @generated
	 */
	EAttribute getSphereOpenCloseEvent_NowOpen();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.AudioEvent <em>Audio Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Audio Event</em>'.
	 * @see dk.itu.cs.isa.multiplayer.AudioEvent
	 * @generated
	 */
	EClass getAudioEvent();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.AudioEvent#getPinId <em>Pin Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pin Id</em>'.
	 * @see dk.itu.cs.isa.multiplayer.AudioEvent#getPinId()
	 * @see #getAudioEvent()
	 * @generated
	 */
	EAttribute getAudioEvent_PinId();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.AudioEvent#isPlay <em>Play</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Play</em>'.
	 * @see dk.itu.cs.isa.multiplayer.AudioEvent#isPlay()
	 * @see #getAudioEvent()
	 * @generated
	 */
	EAttribute getAudioEvent_Play();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.AudioEvent#getPlaybackTime <em>Playback Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Playback Time</em>'.
	 * @see dk.itu.cs.isa.multiplayer.AudioEvent#getPlaybackTime()
	 * @see #getAudioEvent()
	 * @generated
	 */
	EAttribute getAudioEvent_PlaybackTime();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.ISAVector3 <em>ISA Vector3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Vector3</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ISAVector3
	 * @generated
	 */
	EClass getISAVector3();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ISAVector3#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ISAVector3#getX()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_X();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ISAVector3#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ISAVector3#getY()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_Y();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.ISAVector3#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ISAVector3#getZ()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_Z();

	/**
	 * Returns the meta object for enum '{@link dk.itu.cs.isa.multiplayer.RelationType <em>Relation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Relation Type</em>'.
	 * @see dk.itu.cs.isa.multiplayer.RelationType
	 * @generated
	 */
	EEnum getRelationType();

	/**
	 * Returns the meta object for enum '{@link dk.itu.cs.isa.multiplayer.Direction <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Direction</em>'.
	 * @see dk.itu.cs.isa.multiplayer.Direction
	 * @generated
	 */
	EEnum getDirection();

	/**
	 * Returns the meta object for enum '{@link dk.itu.cs.isa.multiplayer.ElementType <em>Element Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Element Type</em>'.
	 * @see dk.itu.cs.isa.multiplayer.ElementType
	 * @generated
	 */
	EEnum getElementType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MultiplayerFactory getMultiplayerFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.EventLogImpl <em>Event Log</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.EventLogImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getEventLog()
		 * @generated
		 */
		EClass EVENT_LOG = eINSTANCE.getEventLog();

		/**
		 * The meta object literal for the '<em><b>Log</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_LOG__LOG = eINSTANCE.getEventLog_Log();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.AbstractEventImpl <em>Abstract Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.AbstractEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getAbstractEvent()
		 * @generated
		 */
		EClass ABSTRACT_EVENT = eINSTANCE.getAbstractEvent();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_EVENT__TIMESTAMP = eINSTANCE.getAbstractEvent_Timestamp();

		/**
		 * The meta object literal for the '<em><b>System Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_EVENT__SYSTEM_NAME = eINSTANCE.getAbstractEvent_SystemName();

		/**
		 * The meta object literal for the '<em><b>Client Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_EVENT__CLIENT_ID = eINSTANCE.getAbstractEvent_ClientId();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.ShowRelationEventImpl <em>Show Relation Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.ShowRelationEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getShowRelationEvent()
		 * @generated
		 */
		EClass SHOW_RELATION_EVENT = eINSTANCE.getShowRelationEvent();

		/**
		 * The meta object literal for the '<em><b>Element Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHOW_RELATION_EVENT__ELEMENT_NAME = eINSTANCE.getShowRelationEvent_ElementName();

		/**
		 * The meta object literal for the '<em><b>Relation Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHOW_RELATION_EVENT__RELATION_TYPE = eINSTANCE.getShowRelationEvent_RelationType();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHOW_RELATION_EVENT__DIRECTION = eINSTANCE.getShowRelationEvent_Direction();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.ClearOneRelationEventImpl <em>Clear One Relation Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.ClearOneRelationEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getClearOneRelationEvent()
		 * @generated
		 */
		EClass CLEAR_ONE_RELATION_EVENT = eINSTANCE.getClearOneRelationEvent();

		/**
		 * The meta object literal for the '<em><b>Element Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLEAR_ONE_RELATION_EVENT__ELEMENT_NAME = eINSTANCE.getClearOneRelationEvent_ElementName();

		/**
		 * The meta object literal for the '<em><b>Relation Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLEAR_ONE_RELATION_EVENT__RELATION_TYPE = eINSTANCE.getClearOneRelationEvent_RelationType();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLEAR_ONE_RELATION_EVENT__DIRECTION = eINSTANCE.getClearOneRelationEvent_Direction();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.ClearRelationsEventImpl <em>Clear Relations Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.ClearRelationsEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getClearRelationsEvent()
		 * @generated
		 */
		EClass CLEAR_RELATIONS_EVENT = eINSTANCE.getClearRelationsEvent();

		/**
		 * The meta object literal for the '<em><b>Element Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLEAR_RELATIONS_EVENT__ELEMENT_NAME = eINSTANCE.getClearRelationsEvent_ElementName();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.ClearAllRelationsEventImpl <em>Clear All Relations Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.ClearAllRelationsEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getClearAllRelationsEvent()
		 * @generated
		 */
		EClass CLEAR_ALL_RELATIONS_EVENT = eINSTANCE.getClearAllRelationsEvent();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.ChangeElementActiveStateEventImpl <em>Change Element Active State Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.ChangeElementActiveStateEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getChangeElementActiveStateEvent()
		 * @generated
		 */
		EClass CHANGE_ELEMENT_ACTIVE_STATE_EVENT = eINSTANCE.getChangeElementActiveStateEvent();

		/**
		 * The meta object literal for the '<em><b>Element Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_TYPE = eINSTANCE.getChangeElementActiveStateEvent_ElementType();

		/**
		 * The meta object literal for the '<em><b>Element Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_ID = eINSTANCE.getChangeElementActiveStateEvent_ElementId();

		/**
		 * The meta object literal for the '<em><b>New State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHANGE_ELEMENT_ACTIVE_STATE_EVENT__NEW_STATE = eINSTANCE.getChangeElementActiveStateEvent_NewState();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION = eINSTANCE.getChangeElementActiveStateEvent_Position();

		/**
		 * The meta object literal for the '<em><b>Rotation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION = eINSTANCE.getChangeElementActiveStateEvent_Rotation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.SystemMoveEventImpl <em>System Move Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.SystemMoveEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getSystemMoveEvent()
		 * @generated
		 */
		EClass SYSTEM_MOVE_EVENT = eINSTANCE.getSystemMoveEvent();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_MOVE_EVENT__POSITION = eINSTANCE.getSystemMoveEvent_Position();

		/**
		 * The meta object literal for the '<em><b>Rotation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_MOVE_EVENT__ROTATION = eINSTANCE.getSystemMoveEvent_Rotation();

		/**
		 * The meta object literal for the '<em><b>Scale</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM_MOVE_EVENT__SCALE = eINSTANCE.getSystemMoveEvent_Scale();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.ChangeNameplateVisibilityEventImpl <em>Change Nameplate Visibility Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.ChangeNameplateVisibilityEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getChangeNameplateVisibilityEvent()
		 * @generated
		 */
		EClass CHANGE_NAMEPLATE_VISIBILITY_EVENT = eINSTANCE.getChangeNameplateVisibilityEvent();

		/**
		 * The meta object literal for the '<em><b>Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHANGE_NAMEPLATE_VISIBILITY_EVENT__VISIBLE = eINSTANCE.getChangeNameplateVisibilityEvent_Visible();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.SphereOpenCloseEventImpl <em>Sphere Open Close Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.SphereOpenCloseEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getSphereOpenCloseEvent()
		 * @generated
		 */
		EClass SPHERE_OPEN_CLOSE_EVENT = eINSTANCE.getSphereOpenCloseEvent();

		/**
		 * The meta object literal for the '<em><b>Element Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPHERE_OPEN_CLOSE_EVENT__ELEMENT_NAME = eINSTANCE.getSphereOpenCloseEvent_ElementName();

		/**
		 * The meta object literal for the '<em><b>Now Open</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPHERE_OPEN_CLOSE_EVENT__NOW_OPEN = eINSTANCE.getSphereOpenCloseEvent_NowOpen();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.AudioEventImpl <em>Audio Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.AudioEventImpl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getAudioEvent()
		 * @generated
		 */
		EClass AUDIO_EVENT = eINSTANCE.getAudioEvent();

		/**
		 * The meta object literal for the '<em><b>Pin Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AUDIO_EVENT__PIN_ID = eINSTANCE.getAudioEvent_PinId();

		/**
		 * The meta object literal for the '<em><b>Play</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AUDIO_EVENT__PLAY = eINSTANCE.getAudioEvent_Play();

		/**
		 * The meta object literal for the '<em><b>Playback Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AUDIO_EVENT__PLAYBACK_TIME = eINSTANCE.getAudioEvent_PlaybackTime();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.impl.ISAVector3Impl <em>ISA Vector3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.impl.ISAVector3Impl
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getISAVector3()
		 * @generated
		 */
		EClass ISA_VECTOR3 = eINSTANCE.getISAVector3();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__X = eINSTANCE.getISAVector3_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__Y = eINSTANCE.getISAVector3_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__Z = eINSTANCE.getISAVector3_Z();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.RelationType <em>Relation Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.RelationType
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getRelationType()
		 * @generated
		 */
		EEnum RELATION_TYPE = eINSTANCE.getRelationType();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.Direction <em>Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.Direction
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getDirection()
		 * @generated
		 */
		EEnum DIRECTION = eINSTANCE.getDirection();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.ElementType <em>Element Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.ElementType
		 * @see dk.itu.cs.isa.multiplayer.impl.MultiplayerPackageImpl#getElementType()
		 * @generated
		 */
		EEnum ELEMENT_TYPE = eINSTANCE.getElementType();

	}

} //MultiplayerPackage

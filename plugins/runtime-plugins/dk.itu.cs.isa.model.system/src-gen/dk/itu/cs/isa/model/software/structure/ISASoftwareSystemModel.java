/**
 */
package dk.itu.cs.isa.model.software.structure;

import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;

import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;

import dk.itu.cs.isa.model.software.structure.libraries.ISALibrary;

import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Software System Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getProjects <em>Projects</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getRootCluster <em>Root Cluster</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getLibraries <em>Libraries</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getMetaInfo <em>Meta Info</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.StructurePackage#getISASoftwareSystemModel()
 * @model
 * @generated
 */
public interface ISASoftwareSystemModel extends ISANamedSoftwareElement {
	/**
	 * Returns the value of the '<em><b>Projects</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Projects</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.StructurePackage#getISASoftwareSystemModel_Projects()
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getParent
	 * @model opposite="parent" containment="true" required="true"
	 * @generated
	 */
	EList<ISAProject> getProjects();

	/**
	 * Returns the value of the '<em><b>Root Cluster</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Cluster</em>' containment reference.
	 * @see #setRootCluster(ISACluster)
	 * @see dk.itu.cs.isa.model.software.structure.StructurePackage#getISASoftwareSystemModel_RootCluster()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISACluster getRootCluster();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getRootCluster <em>Root Cluster</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Cluster</em>' containment reference.
	 * @see #getRootCluster()
	 * @generated
	 */
	void setRootCluster(ISACluster value);

	/**
	 * Returns the value of the '<em><b>Libraries</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.libraries.ISALibrary}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Libraries</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.StructurePackage#getISASoftwareSystemModel_Libraries()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISALibrary> getLibraries();

	/**
	 * Returns the value of the '<em><b>Meta Info</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Meta Info</em>' attribute list.
	 * @see dk.itu.cs.isa.model.software.structure.StructurePackage#getISASoftwareSystemModel_MetaInfo()
	 * @model
	 * @generated
	 */
	EList<String> getMetaInfo();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" qualifiedNameRequired="true"
	 * @generated
	 */
	ISANamedSoftwareElement getSystemElementByName(String qualifiedName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" qualifiedNameRequired="true"
	 * @generated
	 */
	ISANamedSoftwareElement getLibraryElementByName(String qualifiedName, int expectedType, boolean createOnDemand);

} // ISASoftwareSystemModel

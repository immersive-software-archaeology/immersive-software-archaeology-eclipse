/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Redo Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getRedoEvent()
 * @model
 * @generated
 */
public interface RedoEvent extends AbstractWhiteboardEvent {
} // RedoEvent

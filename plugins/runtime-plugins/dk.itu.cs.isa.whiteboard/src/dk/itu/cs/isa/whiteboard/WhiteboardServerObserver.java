package dk.itu.cs.isa.whiteboard;

import dk.itu.cs.isa.coalescence.server.extensions.ISACoalescenceServerObserver;

public class WhiteboardServerObserver implements ISACoalescenceServerObserver {
	
	@Override
	public void notifyServerStart() {
	}

	@Override
	public void notifyServerStop() {
		WhiteboardStorage.INSTANCE.clearCacheAndResetHistories();
	}

}

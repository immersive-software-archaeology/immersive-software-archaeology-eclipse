package dk.itu.cs.isa.model.system.coalescence.codegeneration;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.PrimitiveType;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.formatter.CodeFormatter;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;

public class CodeGenerationUtil {

	public static IFolder createNewSourceFolder(IProject eclipseProject, IJavaProject javaProject, String sourceFolderName) throws CoreException {
		IFolder newSrcFolder = eclipseProject.getFolder(sourceFolderName);
		
		List<IClasspathEntry> classPathsEntriesList = new ArrayList<>(java.util.Arrays.asList(javaProject.getRawClasspath()));
		boolean containsNewSourceFolderEntry = false;
		for(IClasspathEntry entry : classPathsEntriesList) {
			if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
				String thisEntryFolderName = entry.getPath().lastSegment();
				if(thisEntryFolderName.equals(sourceFolderName)) {
					containsNewSourceFolderEntry = true;
					break;
				}
			}
		}
		if(!containsNewSourceFolderEntry) {
			IClasspathEntry newJarEntry = JavaCore.newSourceEntry(newSrcFolder.getFullPath());
			classPathsEntriesList.add(newJarEntry);
			
			IClasspathEntry[] classPathEntriesArray = (IClasspathEntry[]) classPathsEntriesList.toArray(new IClasspathEntry[0]);
			javaProject.setRawClasspath(classPathEntriesArray, null);
		}
		
		if(!newSrcFolder.exists())
			newSrcFolder.create(true, true, null);
		return newSrcFolder;
	}
	
	public static String formatCode(String contents, CodeFormatter codeFormatter) throws MalformedTreeException, BadLocationException {
		IDocument doc = new Document(contents);
		TextEdit edit = ((CodeFormatter) codeFormatter).format(CodeFormatter.K_COMPILATION_UNIT, doc.get(), 0, doc.get().length(), 0, null);
		if (edit != null) {
			edit.apply(doc);
			contents = doc.get();
		}
		return contents;
	}

	public static Type getTypeForQualifiedName(AST ast, String fullyQualifiedName) {
		if(PrimitiveType.toCode(fullyQualifiedName) != null)
			return ast.newPrimitiveType(PrimitiveType.toCode(fullyQualifiedName));
		else
			return ast.newSimpleType(ast.newName(fullyQualifiedName));
	}
	
}

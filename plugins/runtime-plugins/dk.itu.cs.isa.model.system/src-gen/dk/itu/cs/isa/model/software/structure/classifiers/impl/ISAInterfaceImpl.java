/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Interface</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceImpl#getExtendedInterfaces <em>Extended Interfaces</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceImpl#getUsagesAsImplementedInterface <em>Usages As Implemented Interface</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAInterfaceImpl#getUsagesAsExtendedInterface <em>Usages As Extended Interface</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAInterfaceImpl extends ISAClassifierImpl implements ISAInterface {
	/**
	 * The cached value of the '{@link #getExtendedInterfaces() <em>Extended Interfaces</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendedInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAInterface> extendedInterfaces;

	/**
	 * The cached value of the '{@link #getUsagesAsImplementedInterface() <em>Usages As Implemented Interface</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsagesAsImplementedInterface()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAClass> usagesAsImplementedInterface;
	/**
	 * The cached value of the '{@link #getUsagesAsExtendedInterface() <em>Usages As Extended Interface</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsagesAsExtendedInterface()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAInterfaceExtendingElement> usagesAsExtendedInterface;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAInterfaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.ISA_INTERFACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAInterface> getExtendedInterfaces() {
		if (extendedInterfaces == null) {
			extendedInterfaces = new EObjectWithInverseResolvingEList.ManyInverse<ISAInterface>(ISAInterface.class, this, ClassifiersPackage.ISA_INTERFACE__EXTENDED_INTERFACES, ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE);
		}
		return extendedInterfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAClass> getUsagesAsImplementedInterface() {
		if (usagesAsImplementedInterface == null) {
			usagesAsImplementedInterface = new EObjectWithInverseResolvingEList.ManyInverse<ISAClass>(ISAClass.class, this, ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE, ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES);
		}
		return usagesAsImplementedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAInterfaceExtendingElement> getUsagesAsExtendedInterface() {
		if (usagesAsExtendedInterface == null) {
			usagesAsExtendedInterface = new EObjectWithInverseResolvingEList.ManyInverse<ISAInterfaceExtendingElement>(ISAInterfaceExtendingElement.class, this, ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE, ClassifiersPackage.ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES);
		}
		return usagesAsExtendedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassifiersPackage.ISA_INTERFACE__EXTENDED_INTERFACES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtendedInterfaces()).basicAdd(otherEnd, msgs);
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getUsagesAsImplementedInterface()).basicAdd(otherEnd, msgs);
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getUsagesAsExtendedInterface()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassifiersPackage.ISA_INTERFACE__EXTENDED_INTERFACES:
				return ((InternalEList<?>)getExtendedInterfaces()).basicRemove(otherEnd, msgs);
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE:
				return ((InternalEList<?>)getUsagesAsImplementedInterface()).basicRemove(otherEnd, msgs);
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE:
				return ((InternalEList<?>)getUsagesAsExtendedInterface()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassifiersPackage.ISA_INTERFACE__EXTENDED_INTERFACES:
				return getExtendedInterfaces();
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE:
				return getUsagesAsImplementedInterface();
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE:
				return getUsagesAsExtendedInterface();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassifiersPackage.ISA_INTERFACE__EXTENDED_INTERFACES:
				getExtendedInterfaces().clear();
				getExtendedInterfaces().addAll((Collection<? extends ISAInterface>)newValue);
				return;
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE:
				getUsagesAsImplementedInterface().clear();
				getUsagesAsImplementedInterface().addAll((Collection<? extends ISAClass>)newValue);
				return;
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE:
				getUsagesAsExtendedInterface().clear();
				getUsagesAsExtendedInterface().addAll((Collection<? extends ISAInterfaceExtendingElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_INTERFACE__EXTENDED_INTERFACES:
				getExtendedInterfaces().clear();
				return;
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE:
				getUsagesAsImplementedInterface().clear();
				return;
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE:
				getUsagesAsExtendedInterface().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_INTERFACE__EXTENDED_INTERFACES:
				return extendedInterfaces != null && !extendedInterfaces.isEmpty();
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE:
				return usagesAsImplementedInterface != null && !usagesAsImplementedInterface.isEmpty();
			case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE:
				return usagesAsExtendedInterface != null && !usagesAsExtendedInterface.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAInterfaceExtendingElement.class) {
			switch (derivedFeatureID) {
				case ClassifiersPackage.ISA_INTERFACE__EXTENDED_INTERFACES: return ClassifiersPackage.ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAInterfaceExtendingElement.class) {
			switch (baseFeatureID) {
				case ClassifiersPackage.ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES: return ClassifiersPackage.ISA_INTERFACE__EXTENDED_INTERFACES;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ISAInterfaceImpl

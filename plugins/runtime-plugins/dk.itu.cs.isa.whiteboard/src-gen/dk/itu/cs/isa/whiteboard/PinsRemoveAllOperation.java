/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pins Remove All Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinsRemoveAllOperation()
 * @model
 * @generated
 */
public interface PinsRemoveAllOperation extends AbstractCompoundOperation {
} // PinsRemoveAllOperation

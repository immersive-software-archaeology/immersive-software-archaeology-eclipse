package dk.itu.cs.isa.whiteboard;

import dk.itu.cs.isa.coalescence.server.extensions.ISACoalescenceServerObserver;
import dk.itu.cs.isa.eclipse.ui.SystemModelsObserver;

public class WhiteboardViewServerObserver implements ISACoalescenceServerObserver {

	@Override
	public void notifyServerStart() {
		SystemModelsObserver.updateViews();
	}

	@Override
	public void notifyServerStop() {
		SystemModelsObserver.updateViews();
	}

}

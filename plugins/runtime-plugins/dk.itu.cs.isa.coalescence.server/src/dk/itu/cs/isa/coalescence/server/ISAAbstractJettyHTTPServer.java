package dk.itu.cs.isa.coalescence.server;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.swt.widgets.Display;

import dk.itu.cs.isa.events.ISAEventSystem;

public abstract class ISAAbstractJettyHTTPServer implements ISAServer {

	protected String name;
	protected int port;
	
	protected Server jettyServer;
	protected Runnable serverRunnable;
	
	protected boolean starting, stopping;
	
	
	
	public void init(String name, int port, Handler requestHandler) {
		this.name = name;
		this.port = port;
		this.serverRunnable = new Runnable() {
			@Override
			public void run() {
				starting = true;
				jettyServer = new Server(port);
				try {
					jettyServer.getConnectors()[0].getConnectionFactory(HttpConnectionFactory.class);
					jettyServer.setHandler(requestHandler);
					
					jettyServer.start();
					starting = false;
					jettyServer.join();
				} catch (Exception e) {
					Display.getDefault().syncExec(() -> {
						ISAEventSystem.fireErrorMessage("Exception during startup of " + name, e);
					});
				}
			}
		};
	}
	
	
	
	@Override
	public boolean canBeStarted() {
		return getServerState().equals(ServerState.OFFLINE);
	}

	@Override
	public boolean canBeStopped() {
		return getServerState().equals(ServerState.ONLINE);
	}
	
	
	
	@Override
	public ServerState getServerState() {
		if(jettyServer != null) {
			if(jettyServer.isRunning())
				return ServerState.ONLINE;
			else if(starting)
				return ServerState.STARTING;
			else if(stopping)
				return ServerState.STOPPING;
			else return ServerState.OFFLINE;
		}
		else return ServerState.OFFLINE;
	}
	
	

	@Override
	public boolean startServer() {
		new Thread(serverRunnable).start();
		return true;
	}

	@Override
	public boolean stopServer() {
		stopping = true;
		try {
			if(jettyServer != null) {
				jettyServer.stop();
			}
		} catch (Exception e) {
			ISAEventSystem.fireErrorMessage("Exception during shutdown of " + name, e);
			return false;
		}
		finally {
			stopping = false;
		}
		return true;
	}
	
	

	@Override
	public String getServerAddress() {
		return "http://localhost:" + port + "/";
	}

}

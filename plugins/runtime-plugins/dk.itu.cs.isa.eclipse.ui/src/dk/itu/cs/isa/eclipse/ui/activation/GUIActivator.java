package dk.itu.cs.isa.eclipse.ui.activation;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import dk.itu.cs.isa.eclipse.ui.dialogs.EclipseDialogGUIHandler;

public class GUIActivator implements BundleActivator {
	
	private static final String EXTENSION_POINT_ID = "dk.itu.cs.isa.eclipse.ui.ISAGUIActivation";
	
	private List<GUIActivationListener> listeners = new LinkedList<>();

	@Override
	public void start(BundleContext context) throws Exception {
//		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
//		    public void run() {
//		        Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
//		    }
//		});
		
		Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		EclipseDialogGUIHandler.setShell(activeShell);
		
		IConfigurationElement[] config = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(EXTENSION_POINT_ID);
		for (IConfigurationElement configurationElement : config) {
			String extensionPointName = configurationElement.getName();
			
			if (extensionPointName.equals("gui_activation")) {
				try {
//					String extensionName = configurationElement.getAttribute("name");
					GUIActivationListener extensionImplementation = (GUIActivationListener) configurationElement.createExecutableExtension("implementation");
					extensionImplementation.start();
					
					listeners.add(extensionImplementation);
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
			}
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		for(GUIActivationListener listener : listeners) {
			listener.stop();
		}
	}

}

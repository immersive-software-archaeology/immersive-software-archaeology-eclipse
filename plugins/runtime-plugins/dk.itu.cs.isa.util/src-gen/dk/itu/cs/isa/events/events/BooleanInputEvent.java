/**
 */
package dk.itu.cs.isa.events.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Input Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.BooleanInputEvent#isUserInput <em>User Input</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getBooleanInputEvent()
 * @model
 * @generated
 */
public interface BooleanInputEvent extends InputEvent {
	/**
	 * Returns the value of the '<em><b>User Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Input</em>' attribute.
	 * @see #setUserInput(boolean)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getBooleanInputEvent_UserInput()
	 * @model required="true"
	 * @generated
	 */
	boolean isUserInput();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.BooleanInputEvent#isUserInput <em>User Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Input</em>' attribute.
	 * @see #isUserInput()
	 * @generated
	 */
	void setUserInput(boolean value);

} // BooleanInputEvent

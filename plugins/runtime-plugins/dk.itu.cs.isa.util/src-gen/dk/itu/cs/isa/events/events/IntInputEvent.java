/**
 */
package dk.itu.cs.isa.events.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Input Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.IntInputEvent#getUserInput <em>User Input</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getIntInputEvent()
 * @model
 * @generated
 */
public interface IntInputEvent extends InputEvent {
	/**
	 * Returns the value of the '<em><b>User Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Input</em>' attribute.
	 * @see #setUserInput(int)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getIntInputEvent_UserInput()
	 * @model required="true"
	 * @generated
	 */
	int getUserInput();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.IntInputEvent#getUserInput <em>User Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Input</em>' attribute.
	 * @see #getUserInput()
	 * @generated
	 */
	void setUserInput(int value);

} // IntInputEvent

/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo.impl;

import dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage;
import dk.itu.cs.isa.multiplayer.clientinfo.HeadInfo;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Head Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HeadInfoImpl extends BodyPartInfoImpl implements HeadInfo {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HeadInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClientinfoPackage.Literals.HEAD_INFO;
	}

} //HeadInfoImpl

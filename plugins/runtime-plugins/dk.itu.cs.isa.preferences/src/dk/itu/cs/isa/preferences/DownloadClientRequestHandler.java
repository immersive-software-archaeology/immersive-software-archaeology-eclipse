package dk.itu.cs.isa.preferences;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.ISACoalescenceServer;
import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;

public class DownloadClientRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		InputStream in = null;
		ServletOutputStream out = null;
		try {
			String pathToISA = ISAPreferences.getString(ISAPreferences.ISA_EXECUTABLE_PATH, null);
			String isaRepo = "<a target=\"_blank\" href=\"https://gitlab.com/immersive-software-archaeology/immersive-software-archaeology-vr\">Git repository</a>";
			String alternative = "Alternatively, you can download the ISA client from its " + isaRepo + ".";
			String furtherInfo = "<strong>Important</strong>: The ISA VR client builds on <a target=\"_blank\" href=\"https://store.steampowered.com/app/250820/SteamVR/\">SteamVR</a>. "
					+ "More information on the installation can be found in the ISA VR client " + isaRepo + ".";
			
			if(pathToISA == null) {
				printHTMLPage(baseRequest, response, HttpServletResponse.SC_NOT_FOUND, "ISA VR Client Download - Not Available", null,
						"The server did not specify an internal path to a location from which you can download the ISA client - sorry!<br>",
						alternative, furtherInfo);
				return;
			}
			
			File zipFile = new File(pathToISA);
			if(zipFile == null || !zipFile.exists()) {
				printHTMLPage(baseRequest, response, HttpServletResponse.SC_NOT_FOUND, "ISA VR Client Download - Not Available", null,
						"The client ZIP file does currently not exist on the server - sorry!<br>",
						alternative, furtherInfo);
				return;
			}
			
			String fileName = zipFile.getName();
			String fileSize = zipFile.length() / 1000 / 1000 + " MB";
			Date lastModifiedDate = new Date(zipFile.lastModified());
			String lastModifiedString = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(lastModifiedDate);

			String sha256String;
			String md5String;
			byte[] zipData = Files.readAllBytes(zipFile.toPath());
			{
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				byte[] hashBytes = digest.digest(zipData);
				
				StringBuilder hexString = new StringBuilder(2 * hashBytes.length);
			    for (int i = 0; i < hashBytes.length; i++) {
			        String hex = Integer.toHexString(0xff & hashBytes[i]);
			        if(hex.length() == 1) {
			            hexString.append('0');
			        }
			        hexString.append(hex);
			    }
			    sha256String = hexString.toString();
			}
			{
				MessageDigest digest = MessageDigest.getInstance("MD5");
				byte[] hashBytes = digest.digest(zipData);
				md5String = new BigInteger(1, hashBytes).toString(16);
			}

			if(baseRequest.getParameter("requestClient") == null) {
				printHTMLPage(baseRequest, response, HttpServletResponse.SC_OK, "ISA VR Client Download", null,
						"The client comes as a packed ZIP-folder. After downloading, unpack it and start the executable.",
						"<form method=\"get\" action=\"http://" + baseRequest.getServerName() + ":" + ISACoalescenceServer.PORT + "/download\" style=\"\">"
							+ "     <input type=\"hidden\" name=\"requestClient\">"
							// nice downward facing arrow: &darr;
							+ "    <input type=\"submit\" value=\"" + fileName
							+ "\" style=\"padding: 16px 32px\"/>"
							+ "</form>",
						"<ul>"
							+ "<li>Size:<ul><li>" + fileSize + "</li></ul></li>"
							+ "<li>Timestamp:<ul><li>" + lastModifiedString + "</li></ul></li>"
							+ "<li>SHA-256 hash:<ul><li>" + sha256String + "</li></ul></li>"
							+ "<li>MD-5 hash:<ul><li>" + md5String + "</li></ul></li>"
						+ "</ul>",
						alternative + " (However, the version above might be more up-to-date).", furtherInfo);
				return;
			}
			
			in = new FileInputStream(zipFile);
			out = response.getOutputStream();

			response.setStatus(HttpServletResponse.SC_OK);
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			response.setHeader("Content-Length", "" + zipFile.length());
			
			byte[] bytes = new byte[1024];
			int bytesRead;
			while ((bytesRead = in.read(bytes)) != -1) {
				out.write(bytes, 0, bytesRead);
//				for(int i=0; i<bytesRead; i++)
//					out.write(bytes[i]);
			}
			response.flushBuffer();
			
			baseRequest.setHandled(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_FORBIDDEN, "Error", e, "Download page could not be built.");
			baseRequest.setHandled(true);
		}
		finally {
			if(in != null)
				in.close();
			if(out != null)
				out.close();
		}
	}

}










package dk.itu.cs.isa.model.system.architecture.ui;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.ScaleFieldEditor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

import dk.itu.cs.isa.model.system.architecture.preferences.ISAPreferencesClustering;


public class ClusteringPreferencesUIPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	ScaleFieldEditor dbscanMinimumScale;
	ScaleFieldEditor dbscanMaximumScale;
	Label dbscanMinimumLabel;
	Label dbscanMaximumLabel;
	
	ScaleFieldEditor epsilonScale;
	Label epsilonLabel;
	
	ScaleFieldEditor linkageScale;
	Label linkageLabel;

	ScaleFieldEditor packageScale;
	Label packageLabel;
	
	
	
	public ClusteringPreferencesUIPage() {
		// TODO add fancy image!
		super(GRID);
	}



	@Override
	public void createFieldEditors() {
		if(ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MINIMUM_CLUSTER_SIZE, -1) == -1
				|| ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MAXIMUM_CLUSTER_SIZE, -1) == -1
				|| ISAPreferencesClustering.getInt(ISAPreferencesClustering.EPSILON_SAMPLING_RATE, -1) == -1) {
			performDefaults();
		}
		
		
		addSpace();
		

		createLabel("Similarity Measure").setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));
		createLabel("Determines the influence of the measures that calculate similarity between elements (i.e., clusters and classifiers).\n"
				+ "Direct Linkage = similarity between elements based on strength of their interconnection.\n"
				+ "Sibling Linkage = similarity between elements based on how many references they share.",
				true);
		linkageScale = new ScaleFieldEditor(ISAPreferencesClustering.CLUSTERING_SIBLING_LINKAGE_INFLUENCE,
				"&Similarity measure interpolation:", getFieldEditorParent(),
				0, 100, 10, 20) {
			protected void valueChanged() {
				super.valueChanged();
				updateTexts();
			}
		};
		addField(linkageScale);
		new Label(getFieldEditorParent(), SWT.WRAP).setText("");
		linkageLabel = new Label(getFieldEditorParent(), SWT.CENTER);
		linkageLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		int siblingLinkageInfluence = ISAPreferencesClustering.getInt(ISAPreferencesClustering.CLUSTERING_SIBLING_LINKAGE_INFLUENCE, 0); 
		linkageLabel.setText(siblingLinkageInfluence+"% sibling   -   " + (100-siblingLinkageInfluence)+"% direct");
		
		
		addSpace();
		

		createLabel("Influence of the package structure on clustering results").setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));
		createLabel("Determines the influence of a system's package/folder structure during the clustering process.\n"
				+ "0% means no influence at all.\n"
				+ "100% means that the extracted cluster structure is influenced only by the package/folder hierarchy.",
				true);
		packageScale = new ScaleFieldEditor(ISAPreferencesClustering.CLUSTERING_PACKAGE_INFLUENCE,
				"&Package structure influence:", getFieldEditorParent(),
				0, 100, 10, 20) {
			protected void valueChanged() {
				super.valueChanged();
				updateTexts();
			}
		};
		addField(packageScale);
		new Label(getFieldEditorParent(), SWT.WRAP).setText("");
		packageLabel = new Label(getFieldEditorParent(), SWT.CENTER);
		packageLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		packageLabel.setText(ISAPreferencesClustering.getInt(ISAPreferencesClustering.CLUSTERING_PACKAGE_INFLUENCE, 0)+"%");
		
		
		addSpace();

		
		createLabel("Epsilon Sampling Rate").setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));
		createLabel("Specify the effort that should go into finding the optimal epsilon for the DBSCAN algorithm."
				+ "\nA smaller epsilon sampling rate (= a larger divisor) results in a finer and better, yet slower analysis."
				+ "\nThe divisor should be a multiple of the number of available cores of your system (cores available: "+ Runtime.getRuntime().availableProcessors() +")",
				true);
		
		epsilonScale = new ScaleFieldEditor(ISAPreferencesClustering.EPSILON_SAMPLING_RATE,
				"&Epsilon Sampling Rate:", getFieldEditorParent(),
				16, 512, 16, 32) {
			protected void valueChanged() {
				super.valueChanged();
				updateTexts();
			}
		};
		addField(epsilonScale);
		new Label(getFieldEditorParent(), SWT.WRAP).setText("");
		epsilonLabel = new Label(getFieldEditorParent(), SWT.CENTER);
		epsilonLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		epsilonLabel.setText("1/" + ISAPreferencesClustering.getInt(ISAPreferencesClustering.EPSILON_SAMPLING_RATE, 64));
		
		
		addSpace();

		
		createLabel("Targeted cluster size thresholds").setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));
		createLabel("Specify the desired lower and upper bounds for the sizes of clusters that will be converted into cities."
				+ "\nPlease note that the upper bound can sometimes be exceeded.",
				true);
		
		dbscanMinimumScale = new ScaleFieldEditor(ISAPreferencesClustering.DBSCAN_MINIMUM_CLUSTER_SIZE,
				"&Minimum size:", getFieldEditorParent(),
				1, 100, 10, 20) {
			protected void valueChanged() {
				super.valueChanged();
				updateTexts();
			}
		};
		addField(dbscanMinimumScale);
		new Label(getFieldEditorParent(), SWT.WRAP).setText("");
		dbscanMinimumLabel = new Label(getFieldEditorParent(), SWT.CENTER);
		dbscanMinimumLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		dbscanMinimumLabel.setText(ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MINIMUM_CLUSTER_SIZE, 8)+"");

		dbscanMaximumScale = new ScaleFieldEditor(ISAPreferencesClustering.DBSCAN_MAXIMUM_CLUSTER_SIZE,
				"&Maximum size:", getFieldEditorParent(),
				1, 100, 10, 20) {
			protected void valueChanged() {
				super.valueChanged();
				updateTexts();
			}
		};
		addField(dbscanMaximumScale);
		new Label(getFieldEditorParent(), SWT.WRAP).setText("");
		dbscanMaximumLabel = new Label(getFieldEditorParent(), SWT.CENTER);
		dbscanMaximumLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		dbscanMaximumLabel.setText(ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MAXIMUM_CLUSTER_SIZE, 50)+"");
		
		addSpace();
	}

	private void updateTexts() {
		int siblingLinkageInfluence = linkageScale.getScaleControl().getSelection(); 
		linkageLabel.setText(siblingLinkageInfluence+"% sibling   -   " + (100-siblingLinkageInfluence)+"% direct");
		packageLabel.setText(packageScale.getScaleControl().getSelection()+"%");
		dbscanMinimumLabel.setText(dbscanMinimumScale.getScaleControl().getSelection()+"");
		dbscanMaximumLabel.setText(dbscanMaximumScale.getScaleControl().getSelection()+"");
		epsilonLabel.setText("1/" + epsilonScale.getScaleControl().getSelection());
		checkState();
	}

	private Label createLabel(String text) {
		return createLabel(text, false);
	}

	private Label createLabel(String text, boolean italic) {
		Label label = new Label(getFieldEditorParent(), SWT.WRAP);
		label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		label.setText(text);
		if(italic) {
			label.setFont(JFaceResources.getFontRegistry().getItalic(JFaceResources.DEFAULT_FONT));
		}
		return label;
	}

	private void addSpace() {
		new Label(getFieldEditorParent(), SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
	}
	
	@Override
	public void checkState() {
		super.checkState();
		this.setValid(isValid());
	}
	
	@Override
	public boolean isValid() {
		if(dbscanMinimumScale != null && dbscanMaximumScale != null) {
			if(dbscanMinimumScale.getScaleControl().getSelection() > dbscanMaximumScale.getScaleControl().getSelection()) {
				setErrorMessage("The minimum cluster size must not be greater than the maximum cluster size.");
				return false;
			}
		}
		return super.isValid();
	}
	
	
	
	@Override
	public void init(IWorkbench workbench) {
		setTitle("ISA - Immersive Software Archeology");
		setDescription("Settings page for system architecture analysis based on hierarchical clustering.");

		setPreferenceStore(new ScopedPreferenceStore(InstanceScope.INSTANCE, ISAPreferencesClustering.STORAGE_ID));
	}
	
	@Override
	public void dispose() {
		linkageScale.dispose();
		packageScale.dispose();
		epsilonScale.dispose();
		dbscanMinimumScale.dispose();
		dbscanMaximumScale.dispose();
		
		dbscanMinimumLabel.dispose();
		dbscanMaximumLabel.dispose();
		epsilonLabel.dispose();
		linkageLabel.dispose();
		packageLabel.dispose();
		
		super.dispose();
	}
	
	
	
	@Override
	public void performDefaults() {
		super.performDefaults();
		
		new ISAPreferencesClustering().initializeDefaultPreferences();

		if(linkageScale != null && packageScale != null && epsilonScale != null && dbscanMinimumScale != null && dbscanMaximumScale != null) {
			linkageScale.load();
			packageScale.load();
			epsilonScale.load();
			dbscanMinimumScale.load();
			dbscanMaximumScale.load();
			updateTexts();
		}
		
		checkState();
		updateApplyButton();
	}

}

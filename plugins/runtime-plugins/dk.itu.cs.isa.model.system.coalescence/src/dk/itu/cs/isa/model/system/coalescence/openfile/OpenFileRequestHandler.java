package dk.itu.cs.isa.model.system.coalescence.openfile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.core.resources.IFile;
import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.util.ISASystemModelToFileSystemUtil;
import dk.itu.cs.isa.storage.ISAModelStorage;

public class OpenFileRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	private List<OpenFileListener> listeners = new ArrayList<>();

	public void registerListener(OpenFileListener newListener) {
		listeners.add(newListener);
	}
	
	
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		String classifierQualifiedName = baseRequest.getParameter("classifierQualifiedName");
		
		if(systemName == null || classifierQualifiedName == null) {
			String examplePath1 = "systemName=Ant&classifierQualifiedName=org.apache.tools.ant.taskdefs.PropertyHelperTask";
			String examplePath2 = "systemName=ArgoUML&classifierQualifiedName=org.argouml.uml.ui.behavior.state_machines.ActionNewCallEvent";
			String examplePath3 = "systemName=JetUML&classifierQualifiedName=org.jetuml.rendering.DiagramRenderer";
			printInvalidParametersHTMLPage(baseRequest, response, null, examplePath1, examplePath2, examplePath3);
			return;
		}
		
		try {
			ISASoftwareSystemModel structureModel = ISAModelStorage.INSTANCE.loadStructureModel(systemName);
			if(structureModel == null)
				throw new NullPointerException("Cannot load system model for \"" + systemName + "\". Did the meta model change?");

			ISANamedSoftwareElement foundElement = structureModel.getSystemElementByName(classifierQualifiedName);
			if(!(foundElement instanceof ISAClassifier)) {
				printHTMLPage(baseRequest, response, HttpServletResponse.SC_OK, "Tried to open a package", null,
						"You tried to open a package. That is not possible.");				
				return;
			}
			
			ISAClassifier isaClassifier = (ISAClassifier) foundElement;
			IFile classifierFile = ISASystemModelToFileSystemUtil.getFileForClassifier((ISAClassifier) isaClassifier);
			
			boolean wasAbleToOpenInEditor = false;
			for(OpenFileListener listener : listeners) {
				if(listener.openFileInEditor(classifierFile))
					wasAbleToOpenInEditor = true;
			}
			
			if(wasAbleToOpenInEditor)
				printHTMLPage(baseRequest, response, HttpServletResponse.SC_OK, "File was opened",  null);
			else
				printHTMLPage(baseRequest, response, HttpServletResponse.SC_FORBIDDEN, "File cannot be opened",  null, "The given element could not be opened.");
		}
		catch(Exception e) {
			e.printStackTrace();

			printHTMLPage(baseRequest, response, HttpServletResponse.SC_NOT_FOUND, "Could not find source file", e,
					"The ISA Eclipse plugins were not able to find a file for that classifier.<br>"
					+ "Please check whether your Eclipse runtime contains projects with respective source files. "
					+ "These must match the qualified names of your loaded ISA models!");
		}
	}

}
























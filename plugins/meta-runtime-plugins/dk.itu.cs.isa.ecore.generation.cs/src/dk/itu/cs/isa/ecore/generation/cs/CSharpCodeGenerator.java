package dk.itu.cs.isa.ecore.generation.cs;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.internal.ObjectPluginAction;

public class CSharpCodeGenerator implements IObjectActionDelegate {

	private Shell shell;

	
	
	@Override
	public void run(IAction action) {
		if (action instanceof ObjectPluginAction) {
			ISelection selection = ((ObjectPluginAction) action).getSelection();

			if (selection instanceof TreeSelection) {
				TreePath treePath = ((TreeSelection) selection).getPaths()[0];
				Object file = treePath.getSegment(treePath.getSegmentCount() - 1);

				if (file instanceof IFile) {
					IFile xsdFile = (IFile) file;
					List<String> rawXSDLines;
					
					try {
						rawXSDLines = readFile(xsdFile);
						String fixedXSD = fixXSD(rawXSDLines);
						writeFile(xsdFile, fixedXSD);
					} catch (Exception e) {
						e.printStackTrace();
						openFailDialog(e.getMessage());
						return;
					}
					
					String xsdExeLocation = "C:\\Program Files (x86)\\Microsoft SDKs\\Windows\\v10.0A\\bin\\NETFX 4.8 Tools\\xsd.exe";
					xsdExeLocation = "C:\\Program Files (x86)\\Microsoft SDKs\\Windows\\v7.0A\\bin\\x64\\xsd.exe";
					String absolutePathXSD = xsdFile.getRawLocation().toString();
					String parentFolderabsolutePath = xsdFile.getParent().getRawLocation().toString();
					
					// xsd "C:\Unity\ISA-prototype-1\ISA_1\Assets\Scripts\metamodel\Visualization.xsd" /classes
					
					Runtime run = Runtime.getRuntime();
					Process pr;
					String result = "";
					try {
						pr = run.exec(new String[] {xsdExeLocation, "\""+absolutePathXSD+"\"", "/classes", "/out:\""+parentFolderabsolutePath+"\""});
						pr.waitFor();
						BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
						String line = "";
						while ((line=buf.readLine())!=null) {
							System.out.println(line);
							result += line;
						}
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
						openFailDialog(e.getMessage());
						return;
					}
					
					try {
						xsdFile.getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
					} catch (CoreException e) {
						e.printStackTrace();
						openFailDialog(e.getMessage());
						return;
					}

					String projectSpecificXSDPath = xsdFile.getProjectRelativePath().toString();
					String projectSpecificCSPath = projectSpecificXSDPath.substring(0, projectSpecificXSDPath.length()-4);
					projectSpecificCSPath = projectSpecificCSPath.replaceAll("\\.", "_");
					projectSpecificCSPath += ".cs";
					IFile csFile = xsdFile.getProject().getFile(projectSpecificCSPath);
					
					String csFileName = csFile.getFullPath().lastSegment();
					String namespace = "ISA.Modelling." + csFileName.substring(0, csFileName.lastIndexOf('.'));
					
					try {
						List<String> rawCSLines = readFile(csFile);
						String fixedCS = fixCS(rawCSLines, namespace);
						writeFile(csFile, fixedCS);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					try {
						xsdFile.getProject().refreshLocal(IResource.DEPTH_INFINITE, null);
					} catch (CoreException e) {
						e.printStackTrace();
						openFailDialog(e.getMessage());
						return;
					}
					
					openSuccessDialog(result);
				}
			}
		}
	}
	
	

	private List<String> readFile(IFile file) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(file.getContents()));

		List<String> result = new ArrayList<String>();
		String line = reader.readLine();
		while (line != null) {
			result.add(line);
			line = reader.readLine();
		}
		
		reader.close();
		System.out.println("Successfully read out file.");
		return result;
	}

	private void writeFile(IFile file, String fixedXSD) throws Exception {
		byte[] bytes = fixedXSD.getBytes(file.getCharset());
		InputStream source = new ByteArrayInputStream(bytes);
		file.setContents(source, IResource.FORCE, null);
		
		System.out.println("Successfully wrote to the file.");
	}
	
	

	private String fixXSD(List<String> rawXSDLines) {
		for(int i=0; i<rawXSDLines.size(); i++) {
			String line = rawXSDLines.get(i);
			
			if(line.contains("type=\"ecore:EResource\""))
				line = "";
			
			line = line.replaceAll("type=\"ecore:EString\"", "type=\"xsd:string\"");
			line = line.replaceAll("type=\"ecore:EInt\"", "type=\"xsd:int\"");
			line = line.replaceAll("type=\"ecore:ELong\"", "type=\"xsd:long\"");
			line = line.replaceAll("type=\"ecore:EFloat\"", "type=\"xsd:float\"");
			line = line.replaceAll("type=\"ecore:EBoolean\"", "type=\"xsd:boolean\"");
			line = line.replaceAll("type=\"ecore:EByte\"", "type=\"xsd:byte\"");
			// line = line.replaceAll(....);
			
			if(line.trim().startsWith("<xsd:import")) {
				line = "";
			}
			rawXSDLines.set(i, line);
		}
		return String.join(System.lineSeparator(), rawXSDLines);
	}

	private String fixCS(List<String> rawCSLines, String namespace) {
		for(int i=0; i<rawCSLines.size(); i++) {
			String line = rawCSLines.get(i);
			
			if(line.contains("/// <remarks/>")) {
				line = "namespace " + namespace + System.lineSeparator()
						+ "{" + System.lineSeparator()
						+ "/// <remarks/>";
				rawCSLines.set(i, line);
				break;
			}
		}
		return String.join(System.lineSeparator(), rawCSLines) + System.lineSeparator() + "}";
	}
	
	
	
	private void openSuccessDialog(String message) {
		openDialog("C# Code Generation", "C# code generation result:\n\n" + message, MessageDialog.INFORMATION, new String[] {"Okay"});
	}
	
	private void openFailDialog(String errorMessage) {
		openDialog("C# Code Generation", "C# code could not be generated:\n\n" + errorMessage, MessageDialog.ERROR, new String[] {"Okay"});
	}
	
	private int openDialog(String title, String message, int type, String ... buttonLabels) {
		if(shell == null) {
			shell = Display.getDefault().getActiveShell();
		}
		
		MessageDialog dialog = new MessageDialog(shell, title, null, message, type, buttonLabels, 0);
		return dialog.open();
	}
	
	

	@Override
	public void selectionChanged(IAction action, ISelection selection) {

	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

}
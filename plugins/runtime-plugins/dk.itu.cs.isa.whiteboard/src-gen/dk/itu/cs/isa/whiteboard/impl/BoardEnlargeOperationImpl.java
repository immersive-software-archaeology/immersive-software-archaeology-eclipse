/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.BoardEnlargeOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Board Enlarge Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BoardEnlargeOperationImpl extends AbstractDrawOperationImpl implements BoardEnlargeOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BoardEnlargeOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.BOARD_ENLARGE_OPERATION;
	}

} //BoardEnlargeOperationImpl

/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement;
import dk.itu.cs.isa.model.software.structure.classifiers.ISARecord;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Record</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISARecordImpl#getConstructors <em>Constructors</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISARecordImpl#getExtendedInterfaces <em>Extended Interfaces</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISARecordImpl#getComponents <em>Components</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISARecordImpl extends ISAClassifierImpl implements ISARecord {
	/**
	 * The cached value of the '{@link #getConstructors() <em>Constructors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstructors()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAConstructor> constructors;

	/**
	 * The cached value of the '{@link #getExtendedInterfaces() <em>Extended Interfaces</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendedInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAInterface> extendedInterfaces;

	/**
	 * The cached value of the '{@link #getComponents() <em>Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<ISARecordComponent> components;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISARecordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.ISA_RECORD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAConstructor> getConstructors() {
		if (constructors == null) {
			constructors = new EObjectContainmentEList<ISAConstructor>(ISAConstructor.class, this, ClassifiersPackage.ISA_RECORD__CONSTRUCTORS);
		}
		return constructors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAInterface> getExtendedInterfaces() {
		if (extendedInterfaces == null) {
			extendedInterfaces = new EObjectWithInverseResolvingEList.ManyInverse<ISAInterface>(ISAInterface.class, this, ClassifiersPackage.ISA_RECORD__EXTENDED_INTERFACES, ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE);
		}
		return extendedInterfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISARecordComponent> getComponents() {
		if (components == null) {
			components = new EObjectContainmentEList<ISARecordComponent>(ISARecordComponent.class, this, ClassifiersPackage.ISA_RECORD__COMPONENTS);
		}
		return components;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassifiersPackage.ISA_RECORD__EXTENDED_INTERFACES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtendedInterfaces()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassifiersPackage.ISA_RECORD__CONSTRUCTORS:
				return ((InternalEList<?>)getConstructors()).basicRemove(otherEnd, msgs);
			case ClassifiersPackage.ISA_RECORD__EXTENDED_INTERFACES:
				return ((InternalEList<?>)getExtendedInterfaces()).basicRemove(otherEnd, msgs);
			case ClassifiersPackage.ISA_RECORD__COMPONENTS:
				return ((InternalEList<?>)getComponents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassifiersPackage.ISA_RECORD__CONSTRUCTORS:
				return getConstructors();
			case ClassifiersPackage.ISA_RECORD__EXTENDED_INTERFACES:
				return getExtendedInterfaces();
			case ClassifiersPackage.ISA_RECORD__COMPONENTS:
				return getComponents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassifiersPackage.ISA_RECORD__CONSTRUCTORS:
				getConstructors().clear();
				getConstructors().addAll((Collection<? extends ISAConstructor>)newValue);
				return;
			case ClassifiersPackage.ISA_RECORD__EXTENDED_INTERFACES:
				getExtendedInterfaces().clear();
				getExtendedInterfaces().addAll((Collection<? extends ISAInterface>)newValue);
				return;
			case ClassifiersPackage.ISA_RECORD__COMPONENTS:
				getComponents().clear();
				getComponents().addAll((Collection<? extends ISARecordComponent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_RECORD__CONSTRUCTORS:
				getConstructors().clear();
				return;
			case ClassifiersPackage.ISA_RECORD__EXTENDED_INTERFACES:
				getExtendedInterfaces().clear();
				return;
			case ClassifiersPackage.ISA_RECORD__COMPONENTS:
				getComponents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_RECORD__CONSTRUCTORS:
				return constructors != null && !constructors.isEmpty();
			case ClassifiersPackage.ISA_RECORD__EXTENDED_INTERFACES:
				return extendedInterfaces != null && !extendedInterfaces.isEmpty();
			case ClassifiersPackage.ISA_RECORD__COMPONENTS:
				return components != null && !components.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAConstructorContainer.class) {
			switch (derivedFeatureID) {
				case ClassifiersPackage.ISA_RECORD__CONSTRUCTORS: return MembersPackage.ISA_CONSTRUCTOR_CONTAINER__CONSTRUCTORS;
				default: return -1;
			}
		}
		if (baseClass == ISAInterfaceExtendingElement.class) {
			switch (derivedFeatureID) {
				case ClassifiersPackage.ISA_RECORD__EXTENDED_INTERFACES: return ClassifiersPackage.ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAConstructorContainer.class) {
			switch (baseFeatureID) {
				case MembersPackage.ISA_CONSTRUCTOR_CONTAINER__CONSTRUCTORS: return ClassifiersPackage.ISA_RECORD__CONSTRUCTORS;
				default: return -1;
			}
		}
		if (baseClass == ISAInterfaceExtendingElement.class) {
			switch (baseFeatureID) {
				case ClassifiersPackage.ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES: return ClassifiersPackage.ISA_RECORD__EXTENDED_INTERFACES;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ISARecordImpl

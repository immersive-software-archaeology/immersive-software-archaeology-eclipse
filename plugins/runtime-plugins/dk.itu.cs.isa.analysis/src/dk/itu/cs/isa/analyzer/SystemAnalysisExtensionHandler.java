package dk.itu.cs.isa.analyzer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

import dk.itu.cs.isa.analyzer.exceptions.ISAException;
import dk.itu.cs.isa.analyzer.exceptions.SystemAnalysisException;
import dk.itu.cs.isa.analyzer.interfaces.ISAModelHandler;
import dk.itu.cs.isa.analyzer.interfaces.ISystemAnalysisObserver;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelArchitectureAnalyzer;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelCreator;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelQualityAnalyzer;
import dk.itu.cs.isa.analyzer.interfaces.IVisualiationModelCreator;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.preferences.ISAPreferences;

public class SystemAnalysisExtensionHandler {

	private static final String EXTENSION_POINT_ID = "dk.itu.cs.isa.analysis.ISAModelCreation";

	public static final SystemAnalysisExtensionHandler INSTANCE = new SystemAnalysisExtensionHandler();
	
	

	private List<ISystemAnalysisObserver> systemAnalysisObservers;
	
	private Map<String, ISystemModelCreator> systemModelCreators;
	private Map<String, ISystemModelQualityAnalyzer> qualityAnalyzers;
	private Map<String, ISystemModelArchitectureAnalyzer> systemModelArchitectureAnalyzers;
	private Map<String, IVisualiationModelCreator> visualizationModelCreators;
	
	

	private void init() {
		systemAnalysisObservers = new ArrayList<>();
		systemModelCreators = new HashMap<>();
		qualityAnalyzers = new HashMap<>();
		systemModelArchitectureAnalyzers = new HashMap<>();
		visualizationModelCreators = new HashMap<>();

		IConfigurationElement[] config = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(EXTENSION_POINT_ID);
		for (IConfigurationElement configurationElement : config) {
			String extensionPointName = configurationElement.getName();

			String extensionName = configurationElement.getAttribute("name");

			if (extensionPointName.equals("system_analysis_observer")) {
				try {
					ISystemAnalysisObserver extensionImplementation = (ISystemAnalysisObserver) configurationElement.createExecutableExtension("implementation");
					systemAnalysisObservers.add(extensionImplementation);
				} catch (Exception e) {
					ISAEventSystem.fireWarningMessage(
							"Could not instantiate Implementation of System Model Creation Extension \"" + extensionName + "\"", e);
					continue;
				}
			}
			else {
				ISAModelHandler extensionImplementation;
				if (extensionPointName.equals("system_model_creator")) {
					try {
						extensionImplementation = (ISAModelHandler) configurationElement.createExecutableExtension("implementation");
						systemModelCreators.put(extensionName, (ISystemModelCreator) extensionImplementation);
					} catch (Exception e) {
						ISAEventSystem.fireWarningMessage(
								"Could not instantiate Implementation of System Model Creation Extension \"" + extensionName + "\"", e);
						continue;
					}
				}
				else if (extensionPointName.equals("system_model_quality_analyzer")) {
					try {
						extensionImplementation = (ISAModelHandler) configurationElement.createExecutableExtension("implementation");
						qualityAnalyzers.put(extensionName, (ISystemModelQualityAnalyzer) extensionImplementation);
					} catch (Exception e) {
						ISAEventSystem.fireWarningMessage(
								"Could not instantiate Implementation of System Model Quality Analysis Extension \"" + extensionName + "\"", e);
						continue;
					}
				} else if (extensionPointName.equals("system_model_architecture_analyzer")) {
					try {
						extensionImplementation = (ISAModelHandler) configurationElement.createExecutableExtension("implementation");
						systemModelArchitectureAnalyzers.put(extensionName, (ISystemModelArchitectureAnalyzer) extensionImplementation);
					} catch (Exception e) {
						ISAEventSystem.fireWarningMessage(
								"Could not instantiate Implementation of System Model Architecture Analysis Extension \"" + extensionName + "\"", e);
						continue;
					}
				} else if (extensionPointName.equals("visualization_model_creator")) {
					try {
						extensionImplementation = (ISAModelHandler) configurationElement.createExecutableExtension("implementation");
						visualizationModelCreators.put(extensionName, (IVisualiationModelCreator) extensionImplementation);
					} catch (Exception e) {
						ISAEventSystem.fireWarningMessage(
								"Could not instantiate Implementation of Visualization Model Creation Extension \"" + extensionName + "\"", e);
						continue;
					}
				}
				else throw new RuntimeException("Could not evaluate extension point name \"" +extensionPointName+ "\"");
				
				extensionImplementation.initializeInstance();
			}
		}
	}
	
	

	public List<ISystemAnalysisObserver> getAllSystemAnalysisObserversFromExtensions() {
		if (systemAnalysisObservers == null)
			init();
		return systemAnalysisObservers;
	}

	public Map<String, ISystemModelCreator> getAllSystemModelCreatorsFromExtensions() {
		if (systemModelCreators == null)
			init();
		return systemModelCreators;
	}

	public Map<String, ISystemModelQualityAnalyzer> getAllSystemModelQualityAnalyzersFromExtensions() {
		if (qualityAnalyzers == null)
			init();
		return qualityAnalyzers;
	}

	public Map<String, ISystemModelArchitectureAnalyzer> getAllSystemModelArchitectureAnalyzersFromExtensions() {
		if (systemModelArchitectureAnalyzers == null)
			init();
		return systemModelArchitectureAnalyzers;
	}

	public Map<String, IVisualiationModelCreator> getAllVisualizationModelCreatorsFromExtensions() {
		if (visualizationModelCreators == null)
			init();
		return visualizationModelCreators;
	}
	
	

	public ISystemModelCreator getDefaultSystemModelCreatorFromExtensions() throws ISAException {
		if (systemModelCreators == null)
			init();

		return getModelCreatorFromExtensions(systemModelCreators,
				ISAPreferences.SYSTEM_MODEL_CREATOR_DEFAULT);
	}

	public ISystemModelQualityAnalyzer getDefaultSystemModelQualityAnalyzerFromExtensions() throws ISAException {
		if (qualityAnalyzers == null)
			init();

		return getModelCreatorFromExtensions(qualityAnalyzers,
				ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_DEFAULT);
	}

	public ISystemModelArchitectureAnalyzer getDefaultArchitectureAnalyzerFromExtensions() throws ISAException {
		if (systemModelArchitectureAnalyzers == null)
			init();

		return getModelCreatorFromExtensions(systemModelArchitectureAnalyzers,
				ISAPreferences.SYSTEM_MODEL_SAR_DEFAULT);
	}

	public IVisualiationModelCreator getDefaultVisualizationModelCreatorFromExtensions() throws ISAException {
		if (visualizationModelCreators == null)
			init();

		return getModelCreatorFromExtensions(visualizationModelCreators,
				ISAPreferences.VISUALIZATION_MODEL_CREATOR_DEFAULT);
	}
	
	

	private <T> T getModelCreatorFromExtensions(Map<String, T> allModelCreators, String defaultCreatorID) throws ISAException {
		T modelCreator = null;

		if (allModelCreators.size() == 0) {
			throw new SystemAnalysisException(
					"Could not find a plugin that provides functionality for an analysis part.\n\n"
					+ "Please make sure to install plugins for all analysis steps:\n"
					+ "  - structure model creation (mandatory)\n"
					+ "  - quality analysis (optional)\n"
					+ "  - architecture analysis (mandatory)\n"
					+ "  - visualization model creation (mandatory)");
		} else if (allModelCreators.size() == 1) {
			modelCreator = allModelCreators.values().iterator().next();
		} else {
//			boolean alwaysAsk = !ISAPreferences.getBoolean(dontAskID, false);
			String modelCreatorName = ISAPreferences.getString(defaultCreatorID, "");
			
//			if(alwaysAsk || modelCreatorName.isBlank()) {
//				StringChoiceEvent event = ISAEventSystem.fireStringChoiceDialog(
//						"Please choose from the list of registered solutions", allModelCreators.keySet(), modelCreatorName);
//				if(event.getReturnCode() != 0)
//					throw new UserAbortException();
//				
//				modelCreatorName = event.getSelection();
//				ISAPreferences.setString(defaultCreatorID, modelCreatorName);
//			}

			modelCreator = allModelCreators.get(modelCreatorName);
		}

		return modelCreator;
	}

}















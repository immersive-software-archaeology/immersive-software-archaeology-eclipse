/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.PaintClearAllOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Paint Clear All Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PaintClearAllOperationImpl extends AbstractDrawOperationImpl implements PaintClearAllOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PaintClearAllOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PAINT_CLEAR_ALL_OPERATION;
	}

} //PaintClearAllOperationImpl

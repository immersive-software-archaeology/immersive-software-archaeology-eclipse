package dk.itu.cs.isa.model.quality.sonarqube.processes.io;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SonarProcessesOutputHandler {

	private SonarProcessesOutputHandler() {}

	private static Map<String, List<ISonarProcessOutputConsumer>> outputConsumers = new HashMap<>();
	
	
	
	public static void registerOutputHandler(String consumerName, ISonarProcessOutputConsumer newHandler) {
		List<ISonarProcessOutputConsumer> consumers = outputConsumers.get(consumerName);
		if(consumers == null) {
			consumers = new LinkedList<>();
			outputConsumers.put(consumerName, consumers);
		}
		consumers.add(newHandler);
	}
	
	
	
	public static void notifyStart(String consumerName) {
		for (ISonarProcessOutputConsumer consumer : outputConsumers.get(consumerName)) {
			consumer.startNewRun();
		}
	}

	public static void notifyOutput(String consumerName, String outputLine) {
		for (ISonarProcessOutputConsumer consumer : outputConsumers.get(consumerName)) {
			consumer.processOutputLine(outputLine);
		}
	}
	
	public static void notifyError(String consumerName, String errorLine) {
		for (ISonarProcessOutputConsumer consumer : outputConsumers.get(consumerName)) {
			consumer.processErrorLine(errorLine);
		}
	}
	
}

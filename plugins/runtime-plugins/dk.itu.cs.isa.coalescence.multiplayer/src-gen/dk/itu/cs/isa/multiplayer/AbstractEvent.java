/**
 */
package dk.itu.cs.isa.multiplayer;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.AbstractEvent#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.AbstractEvent#getSystemName <em>System Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.AbstractEvent#getClientId <em>Client Id</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getAbstractEvent()
 * @model abstract="true"
 * @generated
 */
public interface AbstractEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(long)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getAbstractEvent_Timestamp()
	 * @model id="true" required="true"
	 * @generated
	 */
	long getTimestamp();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.AbstractEvent#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(long value);

	/**
	 * Returns the value of the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Name</em>' attribute.
	 * @see #setSystemName(String)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getAbstractEvent_SystemName()
	 * @model required="true"
	 * @generated
	 */
	String getSystemName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.AbstractEvent#getSystemName <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Name</em>' attribute.
	 * @see #getSystemName()
	 * @generated
	 */
	void setSystemName(String value);

	/**
	 * Returns the value of the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Client Id</em>' attribute.
	 * @see #setClientId(int)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getAbstractEvent_ClientId()
	 * @model required="true"
	 * @generated
	 */
	int getClientId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.AbstractEvent#getClientId <em>Client Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Client Id</em>' attribute.
	 * @see #getClientId()
	 * @generated
	 */
	void setClientId(int value);

} // AbstractEvent

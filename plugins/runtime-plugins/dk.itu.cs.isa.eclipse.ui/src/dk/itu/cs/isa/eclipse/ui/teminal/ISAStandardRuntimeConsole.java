package dk.itu.cs.isa.eclipse.ui.teminal;

import java.text.SimpleDateFormat;

import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import dk.itu.cs.isa.events.ISAEventListener;
import dk.itu.cs.isa.events.events.ConsoleEvent;
import dk.itu.cs.isa.events.events.Event;

public class ISAStandardRuntimeConsole implements ISAEventListener {
	
	private MessageConsole console;
	private MessageConsoleStream consoleStream;

	
	
	@Override
	public void notify(Event event) {
		if(event instanceof ConsoleEvent) {
			if(console == null) {
				console = ISAConsoleUtil.getConsole("ISA Standard Console");
				consoleStream = console.newMessageStream();
			}
			console.activate();
			
			String date = (new SimpleDateFormat("HH:mm:ss.SSS")).format(((ConsoleEvent) event).getTimestamp());
			String textToPrint = "[" + date + "]: " + event.getText();
			
			consoleStream.println(textToPrint);
			System.out.println(textToPrint);
		}
	}

}

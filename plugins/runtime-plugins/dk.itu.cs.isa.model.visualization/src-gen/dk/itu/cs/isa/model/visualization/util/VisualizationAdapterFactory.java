/**
 */
package dk.itu.cs.isa.model.visualization.util;

import dk.itu.cs.isa.model.visualization.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage
 * @generated
 */
public class VisualizationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VisualizationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualizationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = VisualizationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VisualizationSwitch<Adapter> modelSwitch =
		new VisualizationSwitch<Adapter>() {
			@Override
			public Adapter caseISANamedVisualElement(ISANamedVisualElement object) {
				return createISANamedVisualElementAdapter();
			}
			@Override
			public Adapter caseISAVisualizationModel(ISAVisualizationModel object) {
				return createISAVisualizationModelAdapter();
			}
			@Override
			public Adapter caseISAVector3(ISAVector3 object) {
				return createISAVector3Adapter();
			}
			@Override
			public Adapter caseISATransparentCapsule(ISATransparentCapsule object) {
				return createISATransparentCapsuleAdapter();
			}
			@Override
			public Adapter caseISASpaceStation(ISASpaceStation object) {
				return createISASpaceStationAdapter();
			}
			@Override
			public Adapter caseISAAbstractSpaceStationComponent(ISAAbstractSpaceStationComponent object) {
				return createISAAbstractSpaceStationComponentAdapter();
			}
			@Override
			public Adapter caseISASpaceStationBlock(ISASpaceStationBlock object) {
				return createISASpaceStationBlockAdapter();
			}
			@Override
			public Adapter caseISASpaceStationAntenna(ISASpaceStationAntenna object) {
				return createISASpaceStationAntennaAdapter();
			}
			@Override
			public Adapter caseISAAbstractVisualRelation(ISAAbstractVisualRelation object) {
				return createISAAbstractVisualRelationAdapter();
			}
			@Override
			public Adapter caseISASpaceStationRelation(ISASpaceStationRelation object) {
				return createISASpaceStationRelationAdapter();
			}
			@Override
			public Adapter caseISABlockRelation(ISABlockRelation object) {
				return createISABlockRelationAdapter();
			}
			@Override
			public Adapter caseISAAntennaRelation(ISAAntennaRelation object) {
				return createISAAntennaRelationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISANamedVisualElement <em>ISA Named Visual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISANamedVisualElement
	 * @generated
	 */
	public Adapter createISANamedVisualElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel <em>ISA Visualization Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISAVisualizationModel
	 * @generated
	 */
	public Adapter createISAVisualizationModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISAVector3 <em>ISA Vector3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISAVector3
	 * @generated
	 */
	public Adapter createISAVector3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule <em>ISA Transparent Capsule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISATransparentCapsule
	 * @generated
	 */
	public Adapter createISATransparentCapsuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation <em>ISA Space Station</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStation
	 * @generated
	 */
	public Adapter createISASpaceStationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent <em>ISA Abstract Space Station Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent
	 * @generated
	 */
	public Adapter createISAAbstractSpaceStationComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock <em>ISA Space Station Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationBlock
	 * @generated
	 */
	public Adapter createISASpaceStationBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna <em>ISA Space Station Antenna</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna
	 * @generated
	 */
	public Adapter createISASpaceStationAntennaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISAAbstractVisualRelation <em>ISA Abstract Visual Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISAAbstractVisualRelation
	 * @generated
	 */
	public Adapter createISAAbstractVisualRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationRelation <em>ISA Space Station Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationRelation
	 * @generated
	 */
	public Adapter createISASpaceStationRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISABlockRelation <em>ISA Block Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISABlockRelation
	 * @generated
	 */
	public Adapter createISABlockRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.visualization.ISAAntennaRelation <em>ISA Antenna Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.visualization.ISAAntennaRelation
	 * @generated
	 */
	public Adapter createISAAntennaRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //VisualizationAdapterFactory

package dk.itu.cs.isa.model.quality.sonarqube.api;

public class SonarQubeServerError extends RuntimeException {

	public SonarQubeServerError(String message) {
		super(message);
	}

	private static final long serialVersionUID = -7954397893786322700L;

}

package dk.itu.cs.isa.model.quality.sonarqube;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry;
import dk.itu.cs.isa.model.system.quality.ISAFilePassage;
import dk.itu.cs.isa.model.system.quality.ISAIssue;
import dk.itu.cs.isa.model.system.quality.ISAMeasure;
import dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity;
import dk.itu.cs.isa.model.system.quality.ISAQualityIssueType;
import dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel;
import dk.itu.cs.isa.model.system.quality.QualityFactory;

public class ISASonarQubeQualityModelWriter {
	
	private QualityFactory factory = QualityFactory.eINSTANCE;
	
	private ISASoftwareSystemQualityModel qualityModel;
	private ISASoftwareSystemModel structureModel;
	
	public ISASonarQubeQualityModelWriter(ISASoftwareSystemQualityModel qualityModel, ISASoftwareSystemModel structureModel) {
		this.qualityModel = qualityModel;
		this.structureModel = structureModel;
	}
	
	
	
	public void integrateMeasures(List<JsonElement> allMeasures) {
		for (JsonElement measure : allMeasures) {
			String path = measure.getAsJsonObject().get("path").getAsString();
			if (!path.endsWith(".java"))
				// Ignore xml html etc.
				// Should not occur, as scanner arguments are set to ignore everything except
				// .java files
				continue;

			ISAClassifier resolvedClassifier = findClassifier(path);
			if (resolvedClassifier == null) {
				ISAEventSystem.fireStandardConsoleMessage("Could not resolve a classifier for SonarQube file path: " + path);
				continue;
			}
			
			ISAClassifierQualityEntry entry = getOrCreateEntryForClassifier(resolvedClassifier);

			JsonArray sonarqubeMeasures = measure.getAsJsonObject().get("measures").getAsJsonArray();
			for (int i = 0; i < sonarqubeMeasures.size(); i++) {
				JsonObject sonarqubeMeasure = sonarqubeMeasures.get(i).getAsJsonObject();
				String metric = sonarqubeMeasure.get("metric").getAsString();
				float value = sonarqubeMeasure.get("value").getAsFloat();
				boolean bestValue = false;
				if (sonarqubeMeasure.get("bestValue") != null && !sonarqubeMeasure.get("bestValue").isJsonNull())
					bestValue = sonarqubeMeasure.get("bestValue").getAsBoolean();

				ISAMeasure newISAMeasure = factory.createISAMeasure();
				newISAMeasure.setParentEntry(entry);
				newISAMeasure.setMetric(metric);
				newISAMeasure.setValue(value);
				newISAMeasure.setBestValue(bestValue);
			}
		}
	}



	public void integrateIssues(JsonObject inputJson) {
		for (JsonElement jsonIssue : inputJson.get("issues").getAsJsonArray()) {
			String component = jsonIssue.getAsJsonObject().get("component").getAsString();
			ISAClassifier resolvedClassifier = findClassifier(component.split(":")[1]);
			if (resolvedClassifier == null) {
				ISAEventSystem.fireStandardConsoleMessage("Could not resolve a classifier for SonarQube file path: " + component);
				continue;
			}
			
			ISAClassifierQualityEntry entry = getOrCreateEntryForClassifier(resolvedClassifier);
			
			ISAIssue issue = factory.createISAIssue();
			issue.setParentEntry(entry);
			
			if(jsonIssue.getAsJsonObject().get("message") != null)
				issue.setHint(jsonIssue.getAsJsonObject().get("message").getAsString());
			
			if(jsonIssue.getAsJsonObject().get("effort") != null) {
				String effort = jsonIssue.getAsJsonObject().get("effort").getAsString();
				int effortInMinutes = 0;
				if(effort.contains("d")) {
					String[] split = effort.split("d");
					int days = Integer.valueOf(split[0]);
					effortInMinutes += days * 24 * 60;
					if(split.length > 1)
						effort = split[1];
				}
				if(effort.contains("h")) {
					String[] split = effort.split("h");
					int hrs = Integer.valueOf(split[0]);
					effortInMinutes += hrs * 60;
					if(split.length > 1)
						effort = split[1];
				}
				if(effort.contains("min")) {
					effort = effort.replace("min", "");
					effortInMinutes += Integer.valueOf(effort);
				}
				issue.setEffortToFixInMinutes(effortInMinutes);
			}
			
			if(jsonIssue.getAsJsonObject().get("severity") != null) {
				String severity = jsonIssue.getAsJsonObject().get("severity").getAsString();
				if(severity.equals("INFO") || severity.equals("MINOR"))
					issue.setSeverity(ISAQualityIssueSeverity.MINOR);
				else if(severity.equals("MAJOR"))
					issue.setSeverity(ISAQualityIssueSeverity.MAJOR);
				else if(severity.equals("CRITICAL") || severity.equals("BLOCKER"))
					issue.setSeverity(ISAQualityIssueSeverity.CRITICAL);
				else throw new UnsupportedOperationException("Unknown kind of issue severity: " + severity);
			}
			
			if(jsonIssue.getAsJsonObject().get("type") != null) {
				String type = jsonIssue.getAsJsonObject().get("type").getAsString();
				issue.setType(ISAQualityIssueType.get(type));
			}
			
			if(jsonIssue.getAsJsonObject().get("textRange") != null) {
				JsonObject textRange = jsonIssue.getAsJsonObject().get("textRange").getAsJsonObject();
				ISAFilePassage occurence = factory.createISAFilePassage();
				occurence.setStartLine(textRange.get("startLine").getAsInt());
				occurence.setEndLine(textRange.get("endLine").getAsInt());
				occurence.setStartChar(textRange.get("startOffset").getAsInt());
				occurence.setEndChar(textRange.get("endOffset").getAsInt());
				issue.setOccurence(occurence);
			}
			
			if(jsonIssue.getAsJsonObject().get("rule") != null) {
				String ruleKey = jsonIssue.getAsJsonObject().get("rule").getAsString();
				JsonObject rule = getRule(inputJson, ruleKey);
				if(rule.get("name") != null)
					issue.setName(rule.get("name").getAsString());
			}
		}
	}
	
	private JsonObject getRule(JsonObject inputJson, String ruleKey) {
		for(JsonElement rule : inputJson.get("rules").getAsJsonArray()) {
			if(ruleKey.equals(rule.getAsJsonObject().get("key").getAsString())) {
				return rule.getAsJsonObject();
			}
		}
		throw new RuntimeException("Could not find a rule for " + ruleKey);
	}
	
	
	
	/**
	 * Tries to find an quality information entry for the given Classifier.
	 * If one exists, it is returned.
	 * If none exists, a new entry is created, registered and then returned. 
	 * @param resolvedClassifier
	 * @return a quality information entry for the given classifier
	 */
	private ISAClassifierQualityEntry getOrCreateEntryForClassifier(ISAClassifier classifier) {
		if(classifier == null)
			throw new NullPointerException("Given classifier was null");
		
		for(ISAClassifierQualityEntry entry : qualityModel.getClassifierEntries()) {
			if(entry.getClassifier().equals(classifier))
				return entry;
		}
		
		ISAClassifierQualityEntry newEntry = factory.createISAClassifierQualityEntry();
		newEntry.setClassifier(classifier);
		qualityModel.getClassifierEntries().add(newEntry);
		return newEntry;
	}



	/**
	 * Searches the system structure model for a classifier described by the given SonarQube path.<br>
	 * Is implemented in this complicated method because<br>
	 * (i) the SonarQube path can contain folders that parent projects<br>
	 * (ii) package root folders can be nested or contained directly inside a project
	 * @param structureModel
	 * @param sonarqubePath
	 * @return the resp. classifier within the system structure model
	 */
	private ISAClassifier findClassifier(String sonarqubePath) {
		List<String> nameSegments = new ArrayList<>(Arrays.asList(sonarqubePath.split("/")));
		if(nameSegments.get(nameSegments.size()-1).endsWith(".java"))
			nameSegments.set(nameSegments.size()-1, nameSegments.get(nameSegments.size()-1).substring(0, nameSegments.get(nameSegments.size()-1).length()-5));
		
		ISAProject project = null;
		ISAPackageRoot packageRoot = null;
		int currentSegment = 0;
		
		// Find the right project, the resp. name segment can be nested inside
		// some additional parent/root folders
		{
			while(project == null && currentSegment < nameSegments.size()-1) {
				String projectName = nameSegments.get(currentSegment++);
				for(ISAProject currentProject : structureModel.getProjects()) {
					if(currentProject.getName().equals(projectName)) {
						project = currentProject;
						break;
					}
				}
			}
			if(project == null && structureModel.getProjects().size() == 1) {
				project = structureModel.getProjects().get(0);
				currentSegment = 0;
			}
			if(project == null)
				return null;
		}
		
		// Find the package root, could potentially be nested inside some project
		// sub folder, or be contained directly inside the project
		{
			while(packageRoot == null && currentSegment < nameSegments.size()) {
				String packageRootName = nameSegments.get(currentSegment++);
				for(ISAPackageRoot currentPackageRoot : project.getPackageRoots()) {
					if(currentPackageRoot.getName().equals(packageRootName)) {
						packageRoot = currentPackageRoot;
						break;
					}
				}
			}
			if(packageRoot == null)
				return null;
		}
		
		// At this points, nameSegments contains
		for(int i=0; i<currentSegment; i++) {
			nameSegments.remove(0);
		}
		ISANamedSoftwareElement classifier = project.getElement(String.join(".", nameSegments));
		if(classifier != null && classifier instanceof ISAClassifier)
			return (ISAClassifier) classifier;
		return null;
	}
	
}








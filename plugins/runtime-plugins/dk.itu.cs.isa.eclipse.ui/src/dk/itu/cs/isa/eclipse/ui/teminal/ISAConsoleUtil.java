package dk.itu.cs.isa.eclipse.ui.teminal;

import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;

import dk.itu.cs.isa.eclipse.ui.ImageHelper;

public class ISAConsoleUtil {

	public static MessageConsole getConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if (name.equals(existing[i].getName()))
				return (MessageConsole) existing[i];

		// no console found, so create a new one
		MessageConsole newConsole = new MessageConsole(name, ImageHelper.getImageDescriptor("icons/icon-16.png"));
		conMan.addConsoles(new IConsole[] { newConsole });
		return newConsole;
	}
	
	
	
	private static IConsole lastShownConsole;
	
	public static boolean showConsole(String name) {
		return showConsole(getConsole(name));
	}

	public static boolean showConsole(IConsole console) {
		if(console.equals(lastShownConsole))
			return true;
		console = lastShownConsole;
		
		try {
			IWorkbenchPage page = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage();
			String id = IConsoleConstants.ID_CONSOLE_VIEW;
			
			IConsoleView view = (IConsoleView) page.showView(id);
			view.display(console);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}

/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Whiteboard Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractWhiteboardEventImpl#getSubmittingClientId <em>Submitting Client Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractWhiteboardEventImpl#getWhiteboardId <em>Whiteboard Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractWhiteboardEventImpl#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractWhiteboardEventImpl#isConfirmationRequired <em>Confirmation Required</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AbstractWhiteboardEventImpl extends MinimalEObjectImpl.Container implements AbstractWhiteboardEvent {
	/**
	 * The default value of the '{@link #getSubmittingClientId() <em>Submitting Client Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmittingClientId()
	 * @generated
	 * @ordered
	 */
	protected static final int SUBMITTING_CLIENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSubmittingClientId() <em>Submitting Client Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmittingClientId()
	 * @generated
	 * @ordered
	 */
	protected int submittingClientId = SUBMITTING_CLIENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getWhiteboardId() <em>Whiteboard Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWhiteboardId()
	 * @generated
	 * @ordered
	 */
	protected static final int WHITEBOARD_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWhiteboardId() <em>Whiteboard Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWhiteboardId()
	 * @generated
	 * @ordered
	 */
	protected int whiteboardId = WHITEBOARD_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final long TIMESTAMP_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected long timestamp = TIMESTAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #isConfirmationRequired() <em>Confirmation Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConfirmationRequired()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONFIRMATION_REQUIRED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConfirmationRequired() <em>Confirmation Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConfirmationRequired()
	 * @generated
	 * @ordered
	 */
	protected boolean confirmationRequired = CONFIRMATION_REQUIRED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractWhiteboardEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.ABSTRACT_WHITEBOARD_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSubmittingClientId() {
		return submittingClientId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubmittingClientId(int newSubmittingClientId) {
		int oldSubmittingClientId = submittingClientId;
		submittingClientId = newSubmittingClientId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID, oldSubmittingClientId, submittingClientId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWhiteboardId() {
		return whiteboardId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWhiteboardId(int newWhiteboardId) {
		int oldWhiteboardId = whiteboardId;
		whiteboardId = newWhiteboardId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID, oldWhiteboardId, whiteboardId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimestamp(long newTimestamp) {
		long oldTimestamp = timestamp;
		timestamp = newTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP, oldTimestamp, timestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConfirmationRequired() {
		return confirmationRequired;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfirmationRequired(boolean newConfirmationRequired) {
		boolean oldConfirmationRequired = confirmationRequired;
		confirmationRequired = newConfirmationRequired;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED, oldConfirmationRequired, confirmationRequired));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID:
				return getSubmittingClientId();
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID:
				return getWhiteboardId();
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP:
				return getTimestamp();
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED:
				return isConfirmationRequired();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID:
				setSubmittingClientId((Integer)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID:
				setWhiteboardId((Integer)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP:
				setTimestamp((Long)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED:
				setConfirmationRequired((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID:
				setSubmittingClientId(SUBMITTING_CLIENT_ID_EDEFAULT);
				return;
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID:
				setWhiteboardId(WHITEBOARD_ID_EDEFAULT);
				return;
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP:
				setTimestamp(TIMESTAMP_EDEFAULT);
				return;
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED:
				setConfirmationRequired(CONFIRMATION_REQUIRED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID:
				return submittingClientId != SUBMITTING_CLIENT_ID_EDEFAULT;
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID:
				return whiteboardId != WHITEBOARD_ID_EDEFAULT;
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP:
				return timestamp != TIMESTAMP_EDEFAULT;
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED:
				return confirmationRequired != CONFIRMATION_REQUIRED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (submittingClientId: ");
		result.append(submittingClientId);
		result.append(", whiteboardId: ");
		result.append(whiteboardId);
		result.append(", timestamp: ");
		result.append(timestamp);
		result.append(", confirmationRequired: ");
		result.append(confirmationRequired);
		result.append(')');
		return result.toString();
	}

} //AbstractWhiteboardEventImpl

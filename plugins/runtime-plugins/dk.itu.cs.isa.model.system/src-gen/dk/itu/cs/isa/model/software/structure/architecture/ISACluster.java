/**
 */
package dk.itu.cs.isa.model.software.structure.architecture;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;

import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Cluster</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.architecture.ISACluster#getChildClusters <em>Child Clusters</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.architecture.ISACluster#getClassifiers <em>Classifiers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.architecture.ISACluster#getTopWordOccurrences <em>Top Word Occurrences</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage#getISACluster()
 * @model
 * @generated
 */
public interface ISACluster extends ISANamedSoftwareElement {
	/**
	 * Returns the value of the '<em><b>Child Clusters</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.architecture.ISACluster}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Clusters</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage#getISACluster_ChildClusters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISACluster> getChildClusters();

	/**
	 * Returns the value of the '<em><b>Classifiers</b></em>' reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifiers</em>' reference list.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage#getISACluster_Classifiers()
	 * @model
	 * @generated
	 */
	EList<ISAClassifier> getClassifiers();

	/**
	 * Returns the value of the '<em><b>Top Word Occurrences</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Top Word Occurrences</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage#getISACluster_TopWordOccurrences()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAWordOccurrenceEntry> getTopWordOccurrences();

} // ISACluster

/**
 */
package dk.itu.cs.isa.model.software.structure;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.StructureFactory
 * @model kind="package"
 * @generated
 */
public interface StructurePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "structure";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/software/structure";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "structure";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StructurePackage eINSTANCE = dk.itu.cs.isa.model.software.structure.impl.StructurePackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.impl.ISASoftwareSystemModelImpl <em>ISA Software System Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.impl.ISASoftwareSystemModelImpl
	 * @see dk.itu.cs.isa.model.software.structure.impl.StructurePackageImpl#getISASoftwareSystemModel()
	 * @generated
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL__NAME = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Projects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Root Cluster</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Libraries</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Meta Info</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL__META_INFO = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>ISA Software System Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL_FEATURE_COUNT = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL___GET_FULLY_QUALIFIED_NAME = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL___TO_STRING = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING;

	/**
	 * The operation id for the '<em>Get System Element By Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL___GET_SYSTEM_ELEMENT_BY_NAME__STRING = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Library Element By Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL___GET_LIBRARY_ELEMENT_BY_NAME__STRING_INT_BOOLEAN = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>ISA Software System Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SOFTWARE_SYSTEM_MODEL_OPERATION_COUNT = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel <em>ISA Software System Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Software System Model</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel
	 * @generated
	 */
	EClass getISASoftwareSystemModel();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getProjects <em>Projects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Projects</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getProjects()
	 * @see #getISASoftwareSystemModel()
	 * @generated
	 */
	EReference getISASoftwareSystemModel_Projects();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getRootCluster <em>Root Cluster</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root Cluster</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getRootCluster()
	 * @see #getISASoftwareSystemModel()
	 * @generated
	 */
	EReference getISASoftwareSystemModel_RootCluster();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getLibraries <em>Libraries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Libraries</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getLibraries()
	 * @see #getISASoftwareSystemModel()
	 * @generated
	 */
	EReference getISASoftwareSystemModel_Libraries();

	/**
	 * Returns the meta object for the attribute list '{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getMetaInfo <em>Meta Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Meta Info</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getMetaInfo()
	 * @see #getISASoftwareSystemModel()
	 * @generated
	 */
	EAttribute getISASoftwareSystemModel_MetaInfo();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getSystemElementByName(java.lang.String) <em>Get System Element By Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get System Element By Name</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getSystemElementByName(java.lang.String)
	 * @generated
	 */
	EOperation getISASoftwareSystemModel__GetSystemElementByName__String();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getLibraryElementByName(java.lang.String, int, boolean) <em>Get Library Element By Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Library Element By Name</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getLibraryElementByName(java.lang.String, int, boolean)
	 * @generated
	 */
	EOperation getISASoftwareSystemModel__GetLibraryElementByName__String_int_boolean();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StructureFactory getStructureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.impl.ISASoftwareSystemModelImpl <em>ISA Software System Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.impl.ISASoftwareSystemModelImpl
		 * @see dk.itu.cs.isa.model.software.structure.impl.StructurePackageImpl#getISASoftwareSystemModel()
		 * @generated
		 */
		EClass ISA_SOFTWARE_SYSTEM_MODEL = eINSTANCE.getISASoftwareSystemModel();

		/**
		 * The meta object literal for the '<em><b>Projects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS = eINSTANCE.getISASoftwareSystemModel_Projects();

		/**
		 * The meta object literal for the '<em><b>Root Cluster</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER = eINSTANCE.getISASoftwareSystemModel_RootCluster();

		/**
		 * The meta object literal for the '<em><b>Libraries</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES = eINSTANCE.getISASoftwareSystemModel_Libraries();

		/**
		 * The meta object literal for the '<em><b>Meta Info</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SOFTWARE_SYSTEM_MODEL__META_INFO = eINSTANCE.getISASoftwareSystemModel_MetaInfo();

		/**
		 * The meta object literal for the '<em><b>Get System Element By Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_SOFTWARE_SYSTEM_MODEL___GET_SYSTEM_ELEMENT_BY_NAME__STRING = eINSTANCE.getISASoftwareSystemModel__GetSystemElementByName__String();

		/**
		 * The meta object literal for the '<em><b>Get Library Element By Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_SOFTWARE_SYSTEM_MODEL___GET_LIBRARY_ELEMENT_BY_NAME__STRING_INT_BOOLEAN = eINSTANCE.getISASoftwareSystemModel__GetLibraryElementByName__String_int_boolean();

	}

} //StructurePackage

package dk.itu.cs.isa.model.system.architecture.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.system.architecture.preferences.ISAPreferencesClustering;

public class SimilarityCalculator {
	
	private int packageInfluencePercentage;
	private int siblingLinkageInfluence;
	

	
	public SimilarityCalculator() {
		packageInfluencePercentage = ISAPreferencesClustering.getInt(ISAPreferencesClustering.CLUSTERING_PACKAGE_INFLUENCE, 0);
		if(packageInfluencePercentage < 0 || packageInfluencePercentage > 100) {
			new ISAPreferencesClustering().initializeDefaultPreferences();
			packageInfluencePercentage = ISAPreferencesClustering.getInt(ISAPreferencesClustering.CLUSTERING_PACKAGE_INFLUENCE, 0);
		}
		siblingLinkageInfluence = ISAPreferencesClustering.getInt(ISAPreferencesClustering.CLUSTERING_SIBLING_LINKAGE_INFLUENCE, 0);
	}
	
	
	
	


	public double calculateSimilarity(ISACluster c1, ISACluster c2) {
    	ClusterInformation info1 = ISAClusteringCachingUtil.INSTANCE.getCachedClusterInformation(c1);
    	ClusterInformation info2 = ISAClusteringCachingUtil.INSTANCE.getCachedClusterInformation(c2);
		return calculateSimilarity(info1, info2);
	}
	
	public double calculateSimilarity(ISAClassifier cl1, ISAClassifier cl2) {
    	ClusterInformation info1 = ISAClusteringCachingUtil.INSTANCE.getCachedClusterInformation(cl1);
    	ClusterInformation info2 = ISAClusteringCachingUtil.INSTANCE.getCachedClusterInformation(cl2);
		return calculateSimilarity(info1, info2);
	}
	
	public double calculateSimilarity(ClusterInformation info1, ClusterInformation info2) {
		double cachedResult = ISAClusteringCachingUtil.INSTANCE.getCachedSimilarity(info1, info2);
		if(cachedResult > 0)
			return cachedResult;
		
		double similarity;
		if(packageInfluencePercentage == 100) {
			// use only the package similarity
			similarity = calculatePackageSimilarity(info1.packages, info2.packages);
		}
		else if(packageInfluencePercentage == 0) {
			// use only the class similarity
			similarity = calculateClassSimilarity(info1, info2);
		}
		else {
			// linearly interpolate between the measures
			double similarityOfPackages = calculatePackageSimilarity(info1.packages, info2.packages);
			double similarityOfClasses = calculateClassSimilarity(info1, info2);
			
			double packageInfluence = (double) packageInfluencePercentage / 100d;
			similarity = packageInfluence * similarityOfPackages + (1d-packageInfluence) * similarityOfClasses;
		}
		
		ISAClusteringCachingUtil.INSTANCE.cacheSimilarity(info1, info2, similarity);
		return similarity;
	}



	/**
	 * This is some form of complete linkage
	 */
	private double calculatePackageSimilarity(List<String> packages1, List<String> packages2) {
		List<String> allPackages = new ArrayList<>(packages1.size() + packages2.size());
		allPackages.addAll(packages1);
		allPackages.addAll(packages2);
		
		List<String[]> allPackageFragments = getPackageFragments(allPackages);
		int shortestPackageLength = Integer.MAX_VALUE;
		int longestPackageLength = 0;
		for(String[] currentPackageFragments : allPackageFragments) {
			if(currentPackageFragments.length < shortestPackageLength)
				shortestPackageLength = currentPackageFragments.length;
			if(currentPackageFragments.length > longestPackageLength)
				longestPackageLength = currentPackageFragments.length;
		}
		
		int matchingSegments;
		outerLoop:
		for(matchingSegments=0; matchingSegments<shortestPackageLength; matchingSegments++) {
			String packageFragmentToMatch = allPackageFragments.get(0)[matchingSegments];
			
			for(String[] currentPackageFragments : allPackageFragments) {
				if(!currentPackageFragments[matchingSegments].equals(packageFragmentToMatch))
					break outerLoop;
			}
		}
		
//		return (double) matchingSegments / (double) longestPackageLength;
		return Math.max(0, 1d-(longestPackageLength-matchingSegments)/2d);
	}
	
	private List<String[]> getPackageFragments(List<String> packages) {
		List<String[]> packageFragments = new ArrayList<>(packages.size());
		for(String pack : packages) {
			packageFragments.add(pack.split("\\."));
		}
		return packageFragments;
	}
	
	
	
	private double calculateClassSimilarity(ClusterInformation info1, ClusterInformation info2) {
		if(siblingLinkageInfluence == 100) {
			// use only the package similarity
			return calculateSiblingLinkSimilarity(info1, info2);
		}
		else if(siblingLinkageInfluence == 0) {
			// use only the class similarity
			return calculateDirectLinkSimilarity(info1, info2);
		}
		else {
			// linearly interpolate between the measures
			double siblingLinkSimilarity = calculateSiblingLinkSimilarity(info1, info2);
			double directLinkSimilarity = calculateDirectLinkSimilarity(info1, info2);
			
			double siblingLinkInfluence = (double) siblingLinkageInfluence / 100d;
			double interpolatedSimilarity = siblingLinkInfluence * siblingLinkSimilarity + (1d-siblingLinkInfluence) * directLinkSimilarity;
			return interpolatedSimilarity;
		}
	}
	
	private double calculateSummarizedWeight(Map<ISAReferenceableElement, Integer> references) {
		double result = 0;
		for(Integer weight : references.values()) {
			result += weight;
		}
		return result;
	}
	
	
	
	private double calculateDirectLinkSimilarity(ClusterInformation info1, ClusterInformation info2) {
		int referencesFrom1To2 = calculateDirectLinksBetweenClusters(info1, info2);
		int referencesFrom2To1 = calculateDirectLinksBetweenClusters(info2, info1);
		return Math.pow((double) (referencesFrom1To2 + referencesFrom2To1)
				/ (double) (calculateSummarizedWeight(info1.classifierLevelReferences) + calculateSummarizedWeight(info2.classifierLevelReferences)), 0.25);
	}
	
	private int calculateDirectLinksBetweenClusters(ClusterInformation info1, ClusterInformation info2) {
		if(info1.classifierLevelReferences.size() == 0)
			return 0;
		
		int referencesCount = 0;
		for(ISAReferenceableElement refTarget : info1.classifierLevelReferences.keySet()) {
			String targetClassifierName = refTarget.getFullyQualifiedName();
			for(String classifierQualifiedName : info2.classifierQualifiedNames) {
				if(targetClassifierName.equals(classifierQualifiedName)) {
					referencesCount += info1.classifierLevelReferences.get(refTarget);
					break;
				}
			}
		}
		return referencesCount;
	}
	
	
	
	/**
	 * Calculate the unbiased Gleason measure between two components.<br>
	 * The higher the value, the more similar are the components.
	 * @return the unbiased Gleason measure between component1 and component2
	 */
	private double calculateSiblingLinkSimilarity(ClusterInformation info1, ClusterInformation info2) {
		double summarizedMutualReferenceWeight1 = 0;
		double summarizedMutualReferenceWeight2 = 0;
		
		// This check becomes REALLY slow for large sets of references
		for(ISAReferenceableElement refTarget1 : info1.classifierLevelReferences.keySet()) {
			String targetTargetName1 = refTarget1.getFullyQualifiedName();
			for(ISAReferenceableElement refTarget2 : info2.classifierLevelReferences.keySet()) {
				if(targetTargetName1.equals(refTarget2.getFullyQualifiedName())) {
					summarizedMutualReferenceWeight1 += info1.classifierLevelReferences.get(refTarget1);
					summarizedMutualReferenceWeight2 += info2.classifierLevelReferences.get(refTarget2);
				}
			}
		}

		// Ma
		double mutualReferencesSum = summarizedMutualReferenceWeight1 / (double)info1.classifierQualifiedNames.size() + summarizedMutualReferenceWeight2 / (double)info2.classifierQualifiedNames.size();
		// b
		double distinctReferencesCount1 = calculateSummarizedWeight(info1.classifierLevelReferences) - summarizedMutualReferenceWeight1;
		// c
		double distinctReferencesCount2 = calculateSummarizedWeight(info2.classifierLevelReferences) - summarizedMutualReferenceWeight2;

		// for binary features
//		float unbiasedEllenbergMeasure = 0.5f * mutualReferencesSum / (0.5f * mutualReferencesSum + (float)distinctReferencesCount1 + (float)distinctReferencesCount2);
		// for non-binary features (such as the weighted references)
		double allMatchingReferencesCount = mutualReferencesSum + distinctReferencesCount1 + distinctReferencesCount2;
		if(allMatchingReferencesCount == 0)
			return 0;
		return mutualReferencesSum / allMatchingReferencesCount;
	}
	
}

/**
 */
package dk.itu.cs.isa.model.visualization.util;

import dk.itu.cs.isa.model.visualization.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage
 * @generated
 */
public class VisualizationSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VisualizationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualizationSwitch() {
		if (modelPackage == null) {
			modelPackage = VisualizationPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case VisualizationPackage.ISA_NAMED_VISUAL_ELEMENT: {
				ISANamedVisualElement isaNamedVisualElement = (ISANamedVisualElement)theEObject;
				T result = caseISANamedVisualElement(isaNamedVisualElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_VISUALIZATION_MODEL: {
				ISAVisualizationModel isaVisualizationModel = (ISAVisualizationModel)theEObject;
				T result = caseISAVisualizationModel(isaVisualizationModel);
				if (result == null) result = caseISANamedVisualElement(isaVisualizationModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_VECTOR3: {
				ISAVector3 isaVector3 = (ISAVector3)theEObject;
				T result = caseISAVector3(isaVector3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE: {
				ISATransparentCapsule isaTransparentCapsule = (ISATransparentCapsule)theEObject;
				T result = caseISATransparentCapsule(isaTransparentCapsule);
				if (result == null) result = caseISANamedVisualElement(isaTransparentCapsule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_SPACE_STATION: {
				ISASpaceStation isaSpaceStation = (ISASpaceStation)theEObject;
				T result = caseISASpaceStation(isaSpaceStation);
				if (result == null) result = caseISANamedVisualElement(isaSpaceStation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_ABSTRACT_SPACE_STATION_COMPONENT: {
				ISAAbstractSpaceStationComponent isaAbstractSpaceStationComponent = (ISAAbstractSpaceStationComponent)theEObject;
				T result = caseISAAbstractSpaceStationComponent(isaAbstractSpaceStationComponent);
				if (result == null) result = caseISANamedVisualElement(isaAbstractSpaceStationComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK: {
				ISASpaceStationBlock isaSpaceStationBlock = (ISASpaceStationBlock)theEObject;
				T result = caseISASpaceStationBlock(isaSpaceStationBlock);
				if (result == null) result = caseISAAbstractSpaceStationComponent(isaSpaceStationBlock);
				if (result == null) result = caseISANamedVisualElement(isaSpaceStationBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA: {
				ISASpaceStationAntenna isaSpaceStationAntenna = (ISASpaceStationAntenna)theEObject;
				T result = caseISASpaceStationAntenna(isaSpaceStationAntenna);
				if (result == null) result = caseISAAbstractSpaceStationComponent(isaSpaceStationAntenna);
				if (result == null) result = caseISANamedVisualElement(isaSpaceStationAntenna);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_ABSTRACT_VISUAL_RELATION: {
				ISAAbstractVisualRelation isaAbstractVisualRelation = (ISAAbstractVisualRelation)theEObject;
				T result = caseISAAbstractVisualRelation(isaAbstractVisualRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_SPACE_STATION_RELATION: {
				ISASpaceStationRelation isaSpaceStationRelation = (ISASpaceStationRelation)theEObject;
				T result = caseISASpaceStationRelation(isaSpaceStationRelation);
				if (result == null) result = caseISAAbstractVisualRelation(isaSpaceStationRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_BLOCK_RELATION: {
				ISABlockRelation isaBlockRelation = (ISABlockRelation)theEObject;
				T result = caseISABlockRelation(isaBlockRelation);
				if (result == null) result = caseISAAbstractVisualRelation(isaBlockRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VisualizationPackage.ISA_ANTENNA_RELATION: {
				ISAAntennaRelation isaAntennaRelation = (ISAAntennaRelation)theEObject;
				T result = caseISAAntennaRelation(isaAntennaRelation);
				if (result == null) result = caseISAAbstractVisualRelation(isaAntennaRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Named Visual Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Named Visual Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISANamedVisualElement(ISANamedVisualElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Visualization Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Visualization Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAVisualizationModel(ISAVisualizationModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Vector3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Vector3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAVector3(ISAVector3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Transparent Capsule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Transparent Capsule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISATransparentCapsule(ISATransparentCapsule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Space Station</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Space Station</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISASpaceStation(ISASpaceStation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Abstract Space Station Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Abstract Space Station Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAAbstractSpaceStationComponent(ISAAbstractSpaceStationComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Space Station Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Space Station Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISASpaceStationBlock(ISASpaceStationBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Space Station Antenna</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Space Station Antenna</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISASpaceStationAntenna(ISASpaceStationAntenna object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Abstract Visual Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Abstract Visual Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAAbstractVisualRelation(ISAAbstractVisualRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Space Station Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Space Station Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISASpaceStationRelation(ISASpaceStationRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Block Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Block Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISABlockRelation(ISABlockRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Antenna Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Antenna Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAAntennaRelation(ISAAntennaRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //VisualizationSwitch

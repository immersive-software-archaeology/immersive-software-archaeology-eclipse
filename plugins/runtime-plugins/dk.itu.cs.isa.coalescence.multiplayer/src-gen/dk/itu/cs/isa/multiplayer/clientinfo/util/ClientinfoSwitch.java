/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo.util;

import dk.itu.cs.isa.multiplayer.clientinfo.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage
 * @generated
 */
public class ClientinfoSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ClientinfoPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClientinfoSwitch() {
		if (modelPackage == null) {
			modelPackage = ClientinfoPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ClientinfoPackage.CLIENT_INFO: {
				ClientInfo clientInfo = (ClientInfo)theEObject;
				T result = caseClientInfo(clientInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClientinfoPackage.BODY_PART_INFO: {
				BodyPartInfo bodyPartInfo = (BodyPartInfo)theEObject;
				T result = caseBodyPartInfo(bodyPartInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClientinfoPackage.HEAD_INFO: {
				HeadInfo headInfo = (HeadInfo)theEObject;
				T result = caseHeadInfo(headInfo);
				if (result == null) result = caseBodyPartInfo(headInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClientinfoPackage.HAND_INFO: {
				HandInfo handInfo = (HandInfo)theEObject;
				T result = caseHandInfo(handInfo);
				if (result == null) result = caseBodyPartInfo(handInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClientinfoPackage.HAND_HELD_OBJECT_INFO: {
				HandHeldObjectInfo handHeldObjectInfo = (HandHeldObjectInfo)theEObject;
				T result = caseHandHeldObjectInfo(handHeldObjectInfo);
				if (result == null) result = caseBodyPartInfo(handHeldObjectInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClientinfoPackage.ISA_VECTOR3: {
				ISAVector3 isaVector3 = (ISAVector3)theEObject;
				T result = caseISAVector3(isaVector3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Client Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Client Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClientInfo(ClientInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Body Part Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Body Part Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBodyPartInfo(BodyPartInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Head Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Head Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHeadInfo(HeadInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hand Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hand Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHandInfo(HandInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hand Held Object Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hand Held Object Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHandHeldObjectInfo(HandHeldObjectInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Vector3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Vector3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAVector3(ISAVector3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ClientinfoSwitch

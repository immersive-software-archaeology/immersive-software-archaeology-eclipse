package dk.itu.cs.isa.model.visualization.creation.java.multiview;

import org.eclipse.core.runtime.IProgressMonitor;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;
import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;

public class MultiViewVisualiationModelCreatorPackages extends AbstractMultiViewVisualiationModelCreator {
	
	@Override
	public ISAVisualizationModel run(ISASoftwareSystemModel softwareModel, IProgressMonitor monitor) throws Exception {
		monitor.beginTask("Creating Visualization Model", 4);
		monitor.subTask("Transforming software model to visualization model");
		
		ISAVisualizationModel model = factory.createISAVisualizationModel();
		model.setName(softwareModel.getName());

		ISATransparentCapsule subjectRootCapsule = factory.createISATransparentCapsule();
		model.setSubjectSystemCapsule(subjectRootCapsule);
		subjectRootCapsule.setName(softwareModel.getName());
		
		for(ISAProject project : softwareModel.getProjects()) {
			if(project.getPackageRoots().isEmpty())
				continue;
			
			ISATransparentCapsule projectCapsule = factory.createISATransparentCapsule();
			subjectRootCapsule.getSubCapsules().add(projectCapsule);
			projectCapsule.setName(project.getName());
			projectCapsule.setQualifiedName("project:/" + project.getName());
			
			for(ISAPackageRoot packageRoot : project.getPackageRoots()) {
				ISATransparentCapsule packageRootCapsule = factory.createISATransparentCapsule();
				projectCapsule.getSubCapsules().add(packageRootCapsule);
				
				packageRootCapsule.setName(packageRoot.getName());
				packageRootCapsule.setQualifiedName(packageRoot.getFullyQualifiedName());
				
				for(ISAPackage containedPackage : packageRoot.getSubPackages()) {
					ISATransparentCapsule packageCapsule = createCapsule(containedPackage);
					packageRootCapsule.getSubCapsules().add(packageCapsule);
					MultiViewModelCleanupUtil.deleteEmptyCapsules(packageCapsule);
					MultiViewModelCleanupUtil.collapseCapsules(packageCapsule, ".");
				}
				
				MultiViewModelCleanupUtil.deleteEmptyCapsules(packageRootCapsule);
				MultiViewModelCleanupUtil.collapseCapsules(packageRootCapsule, "/");
			}
		}
		
		model.getLibraryCapsules().addAll(createLibraryCapsules(softwareModel.getLibraries()));
		
		monitor.worked(1);
		monitor.subTask("Cleaning up model structure");
		
		MultiViewModelCleanupUtil.deleteEmptyCapsules(subjectRootCapsule);
		MultiViewModelCleanupUtil.collapseCapsules(subjectRootCapsule, "/");
		
		monitor.worked(1);
		monitor.subTask("Initializing references");
		
		referenceInitializer.initializeRelations();
		
		monitor.worked(1);
		return model;
	}
	

	private ISATransparentCapsule createCapsule(ISAPackage pack) {
		ISATransparentCapsule capsule = factory.createISATransparentCapsule();
		capsule.setName(pack.getName());
		capsule.setQualifiedName(pack.getFullyQualifiedName());

		for(ISAPackage subPackage : pack.getSubPackages()) {
			ISATransparentCapsule subCapsule = createCapsule(subPackage);
			capsule.getSubCapsules().add(subCapsule);
		}
		
		for(ISAClassifier classifier : pack.getClassifiers()) {
			capsule.getSpaceStations().add(createSpaceStation(classifier));
		}
		
		return capsule;
	}
	
}















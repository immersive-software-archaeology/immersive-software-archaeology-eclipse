/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Client Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getSystemName <em>System Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getId <em>Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getColor <em>Color</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getHead <em>Head</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLeftHand <em>Left Hand</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getRightHand <em>Right Hand</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getFps <em>Fps</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getPing <em>Ping</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getConnectTimestamp <em>Connect Timestamp</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLastServerTimestamp <em>Last Server Timestamp</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLastClientTimestamp <em>Last Client Timestamp</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo()
 * @model
 * @generated
 */
public interface ClientInfo extends EObject {
	/**
	 * Returns the value of the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Name</em>' attribute.
	 * @see #setSystemName(String)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_SystemName()
	 * @model required="true"
	 * @generated
	 */
	String getSystemName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getSystemName <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Name</em>' attribute.
	 * @see #getSystemName()
	 * @generated
	 */
	void setSystemName(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(byte)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	byte getId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(byte value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(byte)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_Color()
	 * @model required="true"
	 * @generated
	 */
	byte getColor();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(byte value);

	/**
	 * Returns the value of the '<em><b>Head</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Head</em>' containment reference.
	 * @see #setHead(BodyPartInfo)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_Head()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BodyPartInfo getHead();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getHead <em>Head</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Head</em>' containment reference.
	 * @see #getHead()
	 * @generated
	 */
	void setHead(BodyPartInfo value);

	/**
	 * Returns the value of the '<em><b>Left Hand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Hand</em>' containment reference.
	 * @see #setLeftHand(HandInfo)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_LeftHand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	HandInfo getLeftHand();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLeftHand <em>Left Hand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Hand</em>' containment reference.
	 * @see #getLeftHand()
	 * @generated
	 */
	void setLeftHand(HandInfo value);

	/**
	 * Returns the value of the '<em><b>Right Hand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Hand</em>' containment reference.
	 * @see #setRightHand(HandInfo)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_RightHand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	HandInfo getRightHand();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getRightHand <em>Right Hand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Hand</em>' containment reference.
	 * @see #getRightHand()
	 * @generated
	 */
	void setRightHand(HandInfo value);

	/**
	 * Returns the value of the '<em><b>Fps</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fps</em>' attribute.
	 * @see #setFps(byte)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_Fps()
	 * @model required="true"
	 * @generated
	 */
	byte getFps();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getFps <em>Fps</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fps</em>' attribute.
	 * @see #getFps()
	 * @generated
	 */
	void setFps(byte value);

	/**
	 * Returns the value of the '<em><b>Ping</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ping</em>' attribute.
	 * @see #setPing(int)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_Ping()
	 * @model required="true"
	 * @generated
	 */
	int getPing();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getPing <em>Ping</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ping</em>' attribute.
	 * @see #getPing()
	 * @generated
	 */
	void setPing(int value);

	/**
	 * Returns the value of the '<em><b>Connect Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connect Timestamp</em>' attribute.
	 * @see #setConnectTimestamp(long)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_ConnectTimestamp()
	 * @model required="true"
	 * @generated
	 */
	long getConnectTimestamp();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getConnectTimestamp <em>Connect Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connect Timestamp</em>' attribute.
	 * @see #getConnectTimestamp()
	 * @generated
	 */
	void setConnectTimestamp(long value);

	/**
	 * Returns the value of the '<em><b>Last Server Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Server Timestamp</em>' attribute.
	 * @see #setLastServerTimestamp(long)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_LastServerTimestamp()
	 * @model required="true"
	 * @generated
	 */
	long getLastServerTimestamp();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLastServerTimestamp <em>Last Server Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Server Timestamp</em>' attribute.
	 * @see #getLastServerTimestamp()
	 * @generated
	 */
	void setLastServerTimestamp(long value);

	/**
	 * Returns the value of the '<em><b>Last Client Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Client Timestamp</em>' attribute.
	 * @see #setLastClientTimestamp(long)
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getClientInfo_LastClientTimestamp()
	 * @model required="true"
	 * @generated
	 */
	long getLastClientTimestamp();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLastClientTimestamp <em>Last Client Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Client Timestamp</em>' attribute.
	 * @see #getLastClientTimestamp()
	 * @generated
	 */
	void setLastClientTimestamp(long value);

} // ClientInfo

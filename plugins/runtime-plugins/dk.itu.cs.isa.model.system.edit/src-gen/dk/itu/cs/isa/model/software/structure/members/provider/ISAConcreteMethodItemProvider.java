/**
 */
package dk.itu.cs.isa.model.software.structure.members.provider;


import dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.misc.MiscFactory;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.provider.ISAReferencingElementItemProvider;

import dk.itu.cs.isa.model.software.structure.provider.SoftwareSystemStructuralModelEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ISAConcreteMethodItemProvider extends ISAReferencingElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAConcreteMethodItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addIncomingReferencesPropertyDescriptor(object);
			addModifiersPropertyDescriptor(object);
			addNumberInOrderOfDeclarationPropertyDescriptor(object);
			addStartPositionInDocumentPropertyDescriptor(object);
			addEndPositionInDocumentPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addArrayTypePropertyDescriptor(object);
			addNumberOfStatementsPropertyDescriptor(object);
			addNumberOfExpressionsPropertyDescriptor(object);
			addNumberOfControlFlowSplitsPropertyDescriptor(object);
			addCognitiveComplexityPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISANamedSoftwareElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISANamedSoftwareElement_name_feature", "_UI_ISANamedSoftwareElement_type"),
				 MiscPackage.Literals.ISA_NAMED_SOFTWARE_ELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Incoming References feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIncomingReferencesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAReferenceableElement_incomingReferences_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAReferenceableElement_incomingReferences_feature", "_UI_ISAReferenceableElement_type"),
				 MiscPackage.Literals.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Modifiers feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModifiersPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAModifiableElement_modifiers_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAModifiableElement_modifiers_feature", "_UI_ISAModifiableElement_type"),
				 MiscPackage.Literals.ISA_MODIFIABLE_ELEMENT__MODIFIERS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number In Order Of Declaration feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberInOrderOfDeclarationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAMember_numberInOrderOfDeclaration_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAMember_numberInOrderOfDeclaration_feature", "_UI_ISAMember_type"),
				 MembersPackage.Literals.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Start Position In Document feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStartPositionInDocumentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAMember_startPositionInDocument_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAMember_startPositionInDocument_feature", "_UI_ISAMember_type"),
				 MembersPackage.Literals.ISA_MEMBER__START_POSITION_IN_DOCUMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the End Position In Document feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEndPositionInDocumentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAMember_endPositionInDocument_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAMember_endPositionInDocument_feature", "_UI_ISAMember_type"),
				 MembersPackage.Literals.ISA_MEMBER__END_POSITION_IN_DOCUMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISATypedElement_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISATypedElement_type_feature", "_UI_ISATypedElement_type"),
				 MiscPackage.Literals.ISA_TYPED_ELEMENT__TYPE,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Array Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addArrayTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISATypedElement_arrayType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISATypedElement_arrayType_feature", "_UI_ISATypedElement_type"),
				 MiscPackage.Literals.ISA_TYPED_ELEMENT__ARRAY_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number Of Statements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberOfStatementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAStatementContainer_numberOfStatements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAStatementContainer_numberOfStatements_feature", "_UI_ISAStatementContainer_type"),
				 MiscPackage.Literals.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number Of Expressions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberOfExpressionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAStatementContainer_numberOfExpressions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAStatementContainer_numberOfExpressions_feature", "_UI_ISAStatementContainer_type"),
				 MiscPackage.Literals.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number Of Control Flow Splits feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberOfControlFlowSplitsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAStatementContainer_numberOfControlFlowSplits_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAStatementContainer_numberOfControlFlowSplits_feature", "_UI_ISAStatementContainer_type"),
				 MiscPackage.Literals.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Cognitive Complexity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCognitiveComplexityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISAStatementContainer_cognitiveComplexity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISAStatementContainer_cognitiveComplexity_feature", "_UI_ISAStatementContainer_type"),
				 MiscPackage.Literals.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MiscPackage.Literals.ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ISAConcreteMethod.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ISAConcreteMethod"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ISAConcreteMethod)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ISAConcreteMethod_type") :
			getString("_UI_ISAConcreteMethod_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ISAConcreteMethod.class)) {
			case MembersPackage.ISA_CONCRETE_METHOD__NAME:
			case MembersPackage.ISA_CONCRETE_METHOD__MODIFIERS:
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_IN_ORDER_OF_DECLARATION:
			case MembersPackage.ISA_CONCRETE_METHOD__START_POSITION_IN_DOCUMENT:
			case MembersPackage.ISA_CONCRETE_METHOD__END_POSITION_IN_DOCUMENT:
			case MembersPackage.ISA_CONCRETE_METHOD__TYPE:
			case MembersPackage.ISA_CONCRETE_METHOD__ARRAY_TYPE:
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_STATEMENTS:
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_EXPRESSIONS:
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_CONTROL_FLOW_SPLITS:
			case MembersPackage.ISA_CONCRETE_METHOD__COGNITIVE_COMPLEXITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__PARAMETERS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(MiscPackage.Literals.ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS,
				 MiscFactory.eINSTANCE.createISAParameter()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__METHOD_CALLS ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES ||
			childFeature == MiscPackage.Literals.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return SoftwareSystemStructuralModelEditPlugin.INSTANCE;
	}

}

package dk.itu.cs.isa.model.software.structure.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;
import dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassifierImpl;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;
import dk.itu.cs.isa.model.software.structure.libraries.ISALibrary;
import dk.itu.cs.isa.model.software.structure.libraries.ISAPrimitiveType;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;

public class ISASystemModelHelper {
	
	public static String getFullyQualifiedName(ISANamedSoftwareElement e) {
		if(e instanceof ISAPrimitiveType) {
			return e.getName();
		}
		
		if(isPartOfLibrary(e)) {
			// requires to store qualified names in the name field of elements in libraries.
			return e.getName();
		}

		if(e instanceof ISAProject) {
			return "project:/" + e.getName();
		}
		if(e instanceof ISAPackageRoot) {
			return getFullyQualifiedName((ISANamedSoftwareElement) e.eContainer()) + "/" + e.getName();
		}
		if(e instanceof ISAPackage) {
			if(e.eContainer() instanceof ISAPackageRoot)
				return getFullyQualifiedName((ISANamedSoftwareElement) e.eContainer()) + "/" + e.getName();
			else
				return getFullyQualifiedName((ISANamedSoftwareElement) e.eContainer()) + "." + e.getName();
		}
		if(e instanceof ISAClassifier) {
			return getFullyQualifiedName((ISANamedSoftwareElement) e.eContainer()) + "." + e.getName();
		}
		if(e instanceof ISAMember) {
			return getFullyQualifiedName((ISANamedSoftwareElement) e.eContainer()) + ";" + e.getName();
		}
		
		throw new UnsupportedOperationException("getFullyQualifiedName() is not yet implement for " + e.getClass().getSimpleName());
	}

	private static boolean isPartOfLibrary(EObject e) {
		if(e instanceof ISAPackageRoot || e instanceof ISAProject)
			return false;
		
		EObject parent = e.eContainer();
		if(parent == null)
			throw new RuntimeException("Cannot check containment of element because it is uncontained");
		if(parent instanceof ISAPackage || parent instanceof ISAPackageRoot || parent instanceof ISAProject)
			return false;
		else if(parent instanceof ISALibrary)
			return true;
		else
			return isPartOfLibrary(parent);
	}

//	private static String buildPackageName(ISAPackage e, String name) {
//		ISAPackageContainer parent = e.getParentContainer();
//		if(parent instanceof ISAPackage) {
//			name = parent.getName() + "." + name;
//			return buildPackageName((ISAPackage) parent, name);
//		}
//		return name;
//	}
	
	
	
	public static ISAProject getContainingProject(ISAClassifier classifier) {
		ISAPackageRoot packRoot = getContainingPackageRoot(classifier);
		return packRoot.getParentProject();
	}

	public static ISAPackageRoot getContainingPackageRoot(ISAClassifier classifier) {
		ISAPackage pack = getContainingPackage(classifier);
		return getContainingPackageRoot(pack);
	}

	public static ISAPackageRoot getContainingPackageRoot(ISAPackage pack) {
		EObject parent = pack.eContainer();
		if(parent instanceof ISAPackage)
			return getContainingPackageRoot((ISAPackage) parent);
		if(parent instanceof ISAPackageRoot)
			return (ISAPackageRoot) parent;
		return null;
	}

	public static List<ISAPackage> getPackageFragments(ISAClassifier isaClassifier) {
		List<ISAPackage> result = new LinkedList<>();
		ISAPackage currentPackageFragment = getContainingPackage(isaClassifier);
		while(true) {
			result.add(currentPackageFragment);
			if(!(currentPackageFragment.eContainer() instanceof ISAPackage))
				break;
			currentPackageFragment = (ISAPackage) currentPackageFragment.eContainer();
		}
		Collections.reverse(result);
		return result;
	}

	public static ISAPackage getContainingPackage(ISAClassifier classifier) {
		EObject container = classifier.getParent();
		while(!(container instanceof ISAPackage)) {
			container = container.eContainer();
			if(container == null)
				return null;
		}
		return (ISAPackage) container;
	}

	public static ISAClassifier getContainingClassifier(EObject element) {
		if(element == null)
			return null;
		if(element instanceof ISAClassifier)
			return (ISAClassifier) element;
		else if(element.eContainer() != null)
			return getContainingClassifier(element.eContainer());
		else
			return null;
	}

	public static EList<ISAMember> getAllContainedMembers(ISAClassifierImpl classifier) {
		EList<ISAMember> members = new BasicEList<>();
		// this solution is a bit generic, but robust against changes in the meta model
		for(EObject child : classifier.eContents()) {
			if(child instanceof ISAMember) {
				members.add((ISAMember) child);
			}
		}
		
		members.sort(new Comparator<ISAMember>() {
			@Override
			public int compare(ISAMember m1, ISAMember m2) {
				return m1.getStartPositionInDocument() - m2.getStartPositionInDocument();
			}
		});
		
		return members;
	}
	
	

	public static Map<ISAReferenceableElement, Integer> getAllContainedReferences(ISACluster cluster) {
		Map<ISAReferenceableElement, Integer> result = new HashMap<>();

		for(ISAClassifier classifier : cluster.getClassifiers()) {
			Map<ISAReferenceableElement, Integer> childReferences = getAllContainedReferences(classifier);
			extendReferenceMap(result, childReferences);
		}
		for(ISACluster subCluster : cluster.getChildClusters()) {
			Map<ISAReferenceableElement, Integer> childReferences = getAllContainedReferences(subCluster);
			extendReferenceMap(result, childReferences);
		}
		
		return result;
	}

	public static Map<ISAReferenceableElement, Integer> getAllContainedReferences(ISAClassifier classifier) {
		Map<ISAReferenceableElement, Integer> result = new HashMap<>();
		
		for(ISAMember member : classifier.getContainedMembers()) {
			if(member instanceof ISAReferencingElement) {
				ISAReferencingElement referencingElement = (ISAReferencingElement) member;
				
				List<ISAReference<?>> refs = new ArrayList<>();
				refs.addAll(referencingElement.getConstantAccesses());
				refs.addAll(referencingElement.getConstructorCalls());
				refs.addAll(referencingElement.getFieldAccesses());
				refs.addAll(referencingElement.getMethodCalls());
				refs.addAll(referencingElement.getTypeReferences());
				
				for(ISAReference<?> ref : refs) {
					extendReferenceMap(result, ref.getTarget(), ref.getWeight());
				}
			}
		}
		
		if(classifier instanceof ISAClass) {
			ISAClassifier extendedClass = ((ISAClass) classifier).getExtendedClass();
			if(extendedClass != null)
				extendReferenceMap(result, extendedClass, 1);
				
			for(ISAInterface implementedInterface : ((ISAClass) classifier).getImplementedInterfaces())
				extendReferenceMap(result, implementedInterface, 1);
		}
		else if(classifier instanceof ISAInterface) {
			for(ISAInterface extendedInterface : ((ISAInterface) classifier).getExtendedInterfaces())
				extendReferenceMap(result, extendedInterface, 1);
		}
		
		return result;
	}
	
	private static void extendReferenceMap(Map<ISAReferenceableElement, Integer> referenceMapToExtend, Map<ISAReferenceableElement, Integer> referenceMapToTakeEntriesFrom) {
		for(ISAReferenceableElement target : referenceMapToTakeEntriesFrom.keySet()) {
			ISAClassifier containingClassifier = getContainingClassifier(target);
			extendReferenceMap(referenceMapToExtend, containingClassifier, referenceMapToTakeEntriesFrom.get(target));
		}
	}
	
	private static void extendReferenceMap(Map<ISAReferenceableElement, Integer> references, ISAReferenceableElement target, int weight) {
		if(references.containsKey(target))
			references.put(target, references.get(target) + weight);
		else
			references.put(target, weight);
	}
	
	

	public static Map<ISAReferenceableElement, Integer> pullReferencesUpToClassifierLevel(Map<ISAReferenceableElement, Integer> refs) {
		Map<ISAReferenceableElement, Integer> result = new HashMap<>();
		for(ISAReferenceableElement target : refs.keySet()) {
			ISAClassifier containingClassifier = getContainingClassifier(target);
			extendReferenceMap(result, containingClassifier, refs.get(target));
		}
		return result;
	}

}













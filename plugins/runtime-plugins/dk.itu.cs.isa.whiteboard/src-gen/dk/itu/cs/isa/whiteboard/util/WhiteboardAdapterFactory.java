/**
 */
package dk.itu.cs.isa.whiteboard.util;

import dk.itu.cs.isa.whiteboard.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage
 * @generated
 */
public class WhiteboardAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static WhiteboardPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhiteboardAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = WhiteboardPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WhiteboardSwitch<Adapter> modelSwitch =
		new WhiteboardSwitch<Adapter>() {
			@Override
			public Adapter caseWhiteboardHistory(WhiteboardHistory object) {
				return createWhiteboardHistoryAdapter();
			}
			@Override
			public Adapter caseWhiteboard(Whiteboard object) {
				return createWhiteboardAdapter();
			}
			@Override
			public Adapter casePin(Pin object) {
				return createPinAdapter();
			}
			@Override
			public Adapter casePhoto(Photo object) {
				return createPhotoAdapter();
			}
			@Override
			public Adapter caseISAVector3(ISAVector3 object) {
				return createISAVector3Adapter();
			}
			@Override
			public Adapter caseISAVector2(ISAVector2 object) {
				return createISAVector2Adapter();
			}
			@Override
			public Adapter caseISAColor(ISAColor object) {
				return createISAColorAdapter();
			}
			@Override
			public Adapter caseAbstractWhiteboardEvent(AbstractWhiteboardEvent object) {
				return createAbstractWhiteboardEventAdapter();
			}
			@Override
			public Adapter caseBoardSpawnEvent(BoardSpawnEvent object) {
				return createBoardSpawnEventAdapter();
			}
			@Override
			public Adapter caseBoardDuplicateEvent(BoardDuplicateEvent object) {
				return createBoardDuplicateEventAdapter();
			}
			@Override
			public Adapter caseBoardDestroyEvent(BoardDestroyEvent object) {
				return createBoardDestroyEventAdapter();
			}
			@Override
			public Adapter caseUndoEvent(UndoEvent object) {
				return createUndoEventAdapter();
			}
			@Override
			public Adapter caseRedoEvent(RedoEvent object) {
				return createRedoEventAdapter();
			}
			@Override
			public Adapter caseAbstractOperation(AbstractOperation object) {
				return createAbstractOperationAdapter();
			}
			@Override
			public Adapter caseAbstractCompoundOperation(AbstractCompoundOperation object) {
				return createAbstractCompoundOperationAdapter();
			}
			@Override
			public Adapter casePinSplitOperation(PinSplitOperation object) {
				return createPinSplitOperationAdapter();
			}
			@Override
			public Adapter casePinsRemoveAllOperation(PinsRemoveAllOperation object) {
				return createPinsRemoveAllOperationAdapter();
			}
			@Override
			public Adapter caseAbstractPinOperation(AbstractPinOperation object) {
				return createAbstractPinOperationAdapter();
			}
			@Override
			public Adapter casePinSoftwareAddOperation(PinSoftwareAddOperation object) {
				return createPinSoftwareAddOperationAdapter();
			}
			@Override
			public Adapter casePinAudioAddOperation(PinAudioAddOperation object) {
				return createPinAudioAddOperationAdapter();
			}
			@Override
			public Adapter casePinRemoveOperation(PinRemoveOperation object) {
				return createPinRemoveOperationAdapter();
			}
			@Override
			public Adapter casePinMoveOperation(PinMoveOperation object) {
				return createPinMoveOperationAdapter();
			}
			@Override
			public Adapter caseAbstractDrawOperation(AbstractDrawOperation object) {
				return createAbstractDrawOperationAdapter();
			}
			@Override
			public Adapter caseDrawFreehandOperation(DrawFreehandOperation object) {
				return createDrawFreehandOperationAdapter();
			}
			@Override
			public Adapter caseDrawModuleOperation(DrawModuleOperation object) {
				return createDrawModuleOperationAdapter();
			}
			@Override
			public Adapter caseDrawArrowOperation(DrawArrowOperation object) {
				return createDrawArrowOperationAdapter();
			}
			@Override
			public Adapter casePaintClearAllOperation(PaintClearAllOperation object) {
				return createPaintClearAllOperationAdapter();
			}
			@Override
			public Adapter casePaintEraseOperation(PaintEraseOperation object) {
				return createPaintEraseOperationAdapter();
			}
			@Override
			public Adapter caseBoardEnlargeOperation(BoardEnlargeOperation object) {
				return createBoardEnlargeOperationAdapter();
			}
			@Override
			public Adapter caseAbstractPhotoOperation(AbstractPhotoOperation object) {
				return createAbstractPhotoOperationAdapter();
			}
			@Override
			public Adapter casePhotoAddOperation(PhotoAddOperation object) {
				return createPhotoAddOperationAdapter();
			}
			@Override
			public Adapter casePhotoMoveOperation(PhotoMoveOperation object) {
				return createPhotoMoveOperationAdapter();
			}
			@Override
			public Adapter casePhotoRemoveOperation(PhotoRemoveOperation object) {
				return createPhotoRemoveOperationAdapter();
			}
			@Override
			public Adapter caseVisibleRelation(VisibleRelation object) {
				return createVisibleRelationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.WhiteboardHistory <em>History</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardHistory
	 * @generated
	 */
	public Adapter createWhiteboardHistoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.Whiteboard <em>Whiteboard</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.Whiteboard
	 * @generated
	 */
	public Adapter createWhiteboardAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.Pin <em>Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.Pin
	 * @generated
	 */
	public Adapter createPinAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.Photo <em>Photo</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.Photo
	 * @generated
	 */
	public Adapter createPhotoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.ISAVector3 <em>ISA Vector3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.ISAVector3
	 * @generated
	 */
	public Adapter createISAVector3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.ISAVector2 <em>ISA Vector2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.ISAVector2
	 * @generated
	 */
	public Adapter createISAVector2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.ISAColor <em>ISA Color</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.ISAColor
	 * @generated
	 */
	public Adapter createISAColorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent <em>Abstract Whiteboard Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent
	 * @generated
	 */
	public Adapter createAbstractWhiteboardEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.BoardSpawnEvent <em>Board Spawn Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.BoardSpawnEvent
	 * @generated
	 */
	public Adapter createBoardSpawnEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.BoardDuplicateEvent <em>Board Duplicate Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.BoardDuplicateEvent
	 * @generated
	 */
	public Adapter createBoardDuplicateEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.BoardDestroyEvent <em>Board Destroy Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.BoardDestroyEvent
	 * @generated
	 */
	public Adapter createBoardDestroyEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.UndoEvent <em>Undo Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.UndoEvent
	 * @generated
	 */
	public Adapter createUndoEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.RedoEvent <em>Redo Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.RedoEvent
	 * @generated
	 */
	public Adapter createRedoEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.AbstractOperation <em>Abstract Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.AbstractOperation
	 * @generated
	 */
	public Adapter createAbstractOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.AbstractCompoundOperation <em>Abstract Compound Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.AbstractCompoundOperation
	 * @generated
	 */
	public Adapter createAbstractCompoundOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PinSplitOperation <em>Pin Split Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PinSplitOperation
	 * @generated
	 */
	public Adapter createPinSplitOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PinsRemoveAllOperation <em>Pins Remove All Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PinsRemoveAllOperation
	 * @generated
	 */
	public Adapter createPinsRemoveAllOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation <em>Abstract Pin Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPinOperation
	 * @generated
	 */
	public Adapter createAbstractPinOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PinSoftwareAddOperation <em>Pin Software Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PinSoftwareAddOperation
	 * @generated
	 */
	public Adapter createPinSoftwareAddOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PinAudioAddOperation <em>Pin Audio Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PinAudioAddOperation
	 * @generated
	 */
	public Adapter createPinAudioAddOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PinRemoveOperation <em>Pin Remove Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PinRemoveOperation
	 * @generated
	 */
	public Adapter createPinRemoveOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PinMoveOperation <em>Pin Move Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PinMoveOperation
	 * @generated
	 */
	public Adapter createPinMoveOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.AbstractDrawOperation <em>Abstract Draw Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.AbstractDrawOperation
	 * @generated
	 */
	public Adapter createAbstractDrawOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.DrawFreehandOperation <em>Draw Freehand Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.DrawFreehandOperation
	 * @generated
	 */
	public Adapter createDrawFreehandOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.DrawModuleOperation <em>Draw Module Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.DrawModuleOperation
	 * @generated
	 */
	public Adapter createDrawModuleOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.DrawArrowOperation <em>Draw Arrow Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.DrawArrowOperation
	 * @generated
	 */
	public Adapter createDrawArrowOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PaintClearAllOperation <em>Paint Clear All Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PaintClearAllOperation
	 * @generated
	 */
	public Adapter createPaintClearAllOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PaintEraseOperation <em>Paint Erase Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PaintEraseOperation
	 * @generated
	 */
	public Adapter createPaintEraseOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.BoardEnlargeOperation <em>Board Enlarge Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.BoardEnlargeOperation
	 * @generated
	 */
	public Adapter createBoardEnlargeOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.AbstractPhotoOperation <em>Abstract Photo Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.AbstractPhotoOperation
	 * @generated
	 */
	public Adapter createAbstractPhotoOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PhotoAddOperation <em>Photo Add Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PhotoAddOperation
	 * @generated
	 */
	public Adapter createPhotoAddOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PhotoMoveOperation <em>Photo Move Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PhotoMoveOperation
	 * @generated
	 */
	public Adapter createPhotoMoveOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.PhotoRemoveOperation <em>Photo Remove Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.PhotoRemoveOperation
	 * @generated
	 */
	public Adapter createPhotoRemoveOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.whiteboard.VisibleRelation <em>Visible Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.whiteboard.VisibleRelation
	 * @generated
	 */
	public Adapter createVisibleRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //WhiteboardAdapterFactory

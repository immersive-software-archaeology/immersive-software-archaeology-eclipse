package dk.itu.cs.isa.analyzer.interfaces;

import org.eclipse.core.runtime.IProgressMonitor;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;

public interface ISystemModelArchitectureAnalyzer extends ISAModelHandler {

	public void run(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) throws Exception;

}

/**
 */
package dk.itu.cs.isa.model.system.quality;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Software System Quality Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel#getClassifierEntries <em>Classifier Entries</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISASoftwareSystemQualityModel()
 * @model
 * @generated
 */
public interface ISASoftwareSystemQualityModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Classifier Entries</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier Entries</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISASoftwareSystemQualityModel_ClassifierEntries()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAClassifierQualityEntry> getClassifierEntries();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISASoftwareSystemQualityModel_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // ISASoftwareSystemQualityModel

/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Board Destroy Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getBoardDestroyEvent()
 * @model
 * @generated
 */
public interface BoardDestroyEvent extends AbstractWhiteboardEvent {
} // BoardDestroyEvent

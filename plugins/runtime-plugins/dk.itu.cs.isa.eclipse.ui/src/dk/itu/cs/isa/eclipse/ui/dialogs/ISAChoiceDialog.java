package dk.itu.cs.isa.eclipse.ui.dialogs;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import dk.itu.cs.isa.eclipse.ui.ImageHelper;

public class ISAChoiceDialog extends Dialog {

	private Combo combo;
	private List<String> options;
	private String selection;

	private String title;

	private String message;

	private Button okButton;


	
	public ISAChoiceDialog(Shell parentShell, String dialogTitle, String dialogMessage, String initialValue, List<String> options) {
		super(parentShell);
		this.title = dialogTitle;
		this.options = options;
		message = dialogMessage;
		if (initialValue == null && !options.isEmpty()) {
			selection = options.get(0);
		} else {
			selection = initialValue;
		}
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			selection = combo.getItem(combo.getSelectionIndex());
		} else {
			selection = null;
		}
		super.buttonPressed(buttonId);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		if (title != null) {
			shell.setText(title);
		}
		Image iconImage = ImageHelper.getImage("icons/icon-16.png");
		shell.setImage(iconImage);
		iconImage.dispose();
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons by default
		okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		//do this here because setting the text will set enablement on the ok
		// button
		combo.setFocus();
		if (selection != null) {
			combo.select(combo.indexOf(selection));
		}
		validateInput();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		// create composite
		Composite composite = (Composite) super.createDialogArea(parent);
		// create message
		if (message != null) {
			Label label = new Label(composite, SWT.WRAP);
			label.setText(message);
			GridData data = new GridData(GridData.GRAB_HORIZONTAL
					| GridData.GRAB_VERTICAL | GridData.HORIZONTAL_ALIGN_FILL
					| GridData.VERTICAL_ALIGN_CENTER);
			data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
			label.setLayoutData(data);
			label.setFont(parent.getFont());
		}
		
		combo = new Combo(composite, getInputTextStyle() | SWT.DROP_DOWN | SWT.READ_ONLY);
		combo.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		combo.addModifyListener(e -> validateInput());
		
		for(String option : options) {
			combo.add(option);
		}

		applyDialogFont(composite);
		
		return composite;
	}

	protected void validateInput() {
		Control button = getButton(IDialogConstants.OK_ID);
		if (button != null) {
			button.setEnabled(combo.getSelectionIndex() != -1);
		}
	}

	/**
	 * Returns the ok button.
	 *
	 * @return the ok button
	 */
	protected Button getOkButton() {
		return okButton;
	}

	/**
	 * Returns the string typed into this input dialog.
	 *
	 * @return the input string
	 */
	public String getSelection() {
		return selection;
	}

	/**
	 * Returns the style bits that should be used for the input text field.
	 * Defaults to a single line entry. Subclasses may override.
	 *
	 * @return the integer style bits that should be used when creating the
	 *         input text
	 *
	 * @since 3.4
	 */
	protected int getInputTextStyle() {
		return SWT.SINGLE | SWT.BORDER;
	}

}

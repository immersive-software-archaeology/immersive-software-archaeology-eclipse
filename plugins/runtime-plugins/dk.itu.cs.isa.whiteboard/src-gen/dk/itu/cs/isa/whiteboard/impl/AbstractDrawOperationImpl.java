/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.AbstractDrawOperation;
import dk.itu.cs.isa.whiteboard.ISAColor;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Draw Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractDrawOperationImpl#getColor <em>Color</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractDrawOperationImpl#getPenSize <em>Pen Size</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.AbstractDrawOperationImpl#getDrawnLines <em>Drawn Lines</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractDrawOperationImpl extends AbstractOperationImpl implements AbstractDrawOperation {
	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected ISAColor color;

	/**
	 * The default value of the '{@link #getPenSize() <em>Pen Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPenSize()
	 * @generated
	 * @ordered
	 */
	protected static final int PEN_SIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPenSize() <em>Pen Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPenSize()
	 * @generated
	 * @ordered
	 */
	protected int penSize = PEN_SIZE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDrawnLines() <em>Drawn Lines</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDrawnLines()
	 * @generated
	 * @ordered
	 */
	protected EList<String> drawnLines;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractDrawOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.ABSTRACT_DRAW_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAColor getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetColor(ISAColor newColor, NotificationChain msgs) {
		ISAColor oldColor = color;
		color = newColor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_DRAW_OPERATION__COLOR, oldColor, newColor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(ISAColor newColor) {
		if (newColor != color) {
			NotificationChain msgs = null;
			if (color != null)
				msgs = ((InternalEObject)color).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_DRAW_OPERATION__COLOR, null, msgs);
			if (newColor != null)
				msgs = ((InternalEObject)newColor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.ABSTRACT_DRAW_OPERATION__COLOR, null, msgs);
			msgs = basicSetColor(newColor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_DRAW_OPERATION__COLOR, newColor, newColor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPenSize() {
		return penSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPenSize(int newPenSize) {
		int oldPenSize = penSize;
		penSize = newPenSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.ABSTRACT_DRAW_OPERATION__PEN_SIZE, oldPenSize, penSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getDrawnLines() {
		if (drawnLines == null) {
			drawnLines = new EDataTypeUniqueEList<String>(String.class, this, WhiteboardPackage.ABSTRACT_DRAW_OPERATION__DRAWN_LINES);
		}
		return drawnLines;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__COLOR:
				return basicSetColor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__COLOR:
				return getColor();
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__PEN_SIZE:
				return getPenSize();
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__DRAWN_LINES:
				return getDrawnLines();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__COLOR:
				setColor((ISAColor)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__PEN_SIZE:
				setPenSize((Integer)newValue);
				return;
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__DRAWN_LINES:
				getDrawnLines().clear();
				getDrawnLines().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__COLOR:
				setColor((ISAColor)null);
				return;
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__PEN_SIZE:
				setPenSize(PEN_SIZE_EDEFAULT);
				return;
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__DRAWN_LINES:
				getDrawnLines().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__COLOR:
				return color != null;
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__PEN_SIZE:
				return penSize != PEN_SIZE_EDEFAULT;
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION__DRAWN_LINES:
				return drawnLines != null && !drawnLines.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (penSize: ");
		result.append(penSize);
		result.append(", drawnLines: ");
		result.append(drawnLines);
		result.append(')');
		return result.toString();
	}

} //AbstractDrawOperationImpl

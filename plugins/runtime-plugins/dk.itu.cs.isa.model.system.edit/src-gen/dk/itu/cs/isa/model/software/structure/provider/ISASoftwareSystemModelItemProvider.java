/**
 */
package dk.itu.cs.isa.model.software.structure.provider;


import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemFactory;

import dk.itu.cs.isa.model.software.structure.libraries.LibrariesFactory;

import dk.itu.cs.isa.model.software.structure.misc.provider.ISANamedSoftwareElementItemProvider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ISASoftwareSystemModelItemProvider extends ISANamedSoftwareElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASoftwareSystemModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addMetaInfoPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Meta Info feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMetaInfoPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ISASoftwareSystemModel_metaInfo_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ISASoftwareSystemModel_metaInfo_feature", "_UI_ISASoftwareSystemModel_type"),
				 StructurePackage.Literals.ISA_SOFTWARE_SYSTEM_MODEL__META_INFO,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(StructurePackage.Literals.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS);
			childrenFeatures.add(StructurePackage.Literals.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER);
			childrenFeatures.add(StructurePackage.Literals.ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ISASoftwareSystemModel.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ISASoftwareSystemModel"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ISASoftwareSystemModel)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ISASoftwareSystemModel_type") :
			getString("_UI_ISASoftwareSystemModel_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ISASoftwareSystemModel.class)) {
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__META_INFO:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS:
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER:
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(StructurePackage.Literals.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS,
				 FilesystemFactory.eINSTANCE.createISAProject()));

		newChildDescriptors.add
			(createChildParameter
				(StructurePackage.Literals.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER,
				 ArchitectureFactory.eINSTANCE.createISACluster()));

		newChildDescriptors.add
			(createChildParameter
				(StructurePackage.Literals.ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES,
				 LibrariesFactory.eINSTANCE.createISALibrary()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return SoftwareSystemStructuralModelEditPlugin.INSTANCE;
	}

}

package dk.itu.cs.isa.coalescence.server.requests;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;
import dk.itu.cs.isa.storage.ISAModelStorage;
import dk.itu.cs.isa.storage.ModelStorageEntry;
import dk.itu.cs.isa.util.EcoreUtilISA;

public class ListModelsRequestHandler extends ISAAbstractCoalescenceRequestHandler {

	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = null; //ISACoalescenceService.INSTANCE.getCurrentlyActiveSystem();
		
		if(systemName == null)
			systemName = baseRequest.getParameter("systemName");
		
		if(systemName == null)
			handleListAllModels(baseRequest, response);
		else
			handleModelRequest(baseRequest, response, systemName);
	}
	
	
    
	private void handleModelRequest(Request baseRequest, HttpServletResponse response, String systemName) throws IOException {
    	response.setContentType("application/xml; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
//		ISACoalescenceService.INSTANCE.setCurrentlyActiveSystem(systemName);
		
		ISAVisualizationModel model = ISAModelStorage.INSTANCE.loadVisualizationModel(systemName);
		
		PrintWriter out = response.getWriter();
		out.print(EcoreUtilISA.getXMLStringForModel(model));
		out.close();
		
		baseRequest.setHandled(true);
	}
	
	
    
	private void handleListAllModels(Request baseRequest, HttpServletResponse response) throws IOException {
    	response.setContentType("application/json; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
		PrintWriter out = response.getWriter();
		out.print(printModelStorageAsJSON());
		out.close();
		
		baseRequest.setHandled(true);
	}

	private String printModelStorageAsJSON() {
		JSONObject responseJSON = new JSONObject();
		responseJSON.put("modelStorageXmlLocation", prettyPrintPath(ISAModelStorage.INSTANCE.getStorageMetaDataPath()));
		responseJSON.put("baseFolderLocation", prettyPrintPath(ISAModelStorage.INSTANCE.getStorageMetaData().getBaseFolder()));

		
		JSONArray entryArray = new JSONArray();
		for(ModelStorageEntry entry : ISAModelStorage.INSTANCE.getAllEntries()) {
			JSONObject entryJSON = new JSONObject();
			entryJSON.put("systemName", entry.getSystemName());
			entryJSON.put("numberOfClassifiers", entry.getNumberOfClassifiers());
			
			entryJSON.put("structureModel", prettyPrintPath(entry.getStructureModelFileName()));
			entryJSON.put("qualityModel", prettyPrintPath(entry.getQualityModelFileName()));
			entryJSON.put("visualizationModel", prettyPrintPath(entry.getVisualizationModelFileName()));
			
			entryArray.put(entryJSON);
		}
		responseJSON.put("entries", entryArray);
    	return responseJSON.toString(2);
    }
	
	private String prettyPrintPath(String path) {
		return path.replace("\\", "/");
	}
    
}

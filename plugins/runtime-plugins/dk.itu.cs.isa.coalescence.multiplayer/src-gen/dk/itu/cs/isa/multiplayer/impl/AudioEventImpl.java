/**
 */
package dk.itu.cs.isa.multiplayer.impl;

import dk.itu.cs.isa.multiplayer.AudioEvent;
import dk.itu.cs.isa.multiplayer.MultiplayerPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Audio Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.AudioEventImpl#getPinId <em>Pin Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.AudioEventImpl#isPlay <em>Play</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.AudioEventImpl#getPlaybackTime <em>Playback Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AudioEventImpl extends AbstractEventImpl implements AudioEvent {
	/**
	 * The default value of the '{@link #getPinId() <em>Pin Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinId()
	 * @generated
	 * @ordered
	 */
	protected static final int PIN_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPinId() <em>Pin Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPinId()
	 * @generated
	 * @ordered
	 */
	protected int pinId = PIN_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isPlay() <em>Play</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPlay()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PLAY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPlay() <em>Play</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPlay()
	 * @generated
	 * @ordered
	 */
	protected boolean play = PLAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPlaybackTime() <em>Playback Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlaybackTime()
	 * @generated
	 * @ordered
	 */
	protected static final float PLAYBACK_TIME_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getPlaybackTime() <em>Playback Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlaybackTime()
	 * @generated
	 * @ordered
	 */
	protected float playbackTime = PLAYBACK_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AudioEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MultiplayerPackage.Literals.AUDIO_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPinId() {
		return pinId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPinId(int newPinId) {
		int oldPinId = pinId;
		pinId = newPinId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.AUDIO_EVENT__PIN_ID, oldPinId, pinId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPlay() {
		return play;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlay(boolean newPlay) {
		boolean oldPlay = play;
		play = newPlay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.AUDIO_EVENT__PLAY, oldPlay, play));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getPlaybackTime() {
		return playbackTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlaybackTime(float newPlaybackTime) {
		float oldPlaybackTime = playbackTime;
		playbackTime = newPlaybackTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.AUDIO_EVENT__PLAYBACK_TIME, oldPlaybackTime, playbackTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MultiplayerPackage.AUDIO_EVENT__PIN_ID:
				return getPinId();
			case MultiplayerPackage.AUDIO_EVENT__PLAY:
				return isPlay();
			case MultiplayerPackage.AUDIO_EVENT__PLAYBACK_TIME:
				return getPlaybackTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MultiplayerPackage.AUDIO_EVENT__PIN_ID:
				setPinId((Integer)newValue);
				return;
			case MultiplayerPackage.AUDIO_EVENT__PLAY:
				setPlay((Boolean)newValue);
				return;
			case MultiplayerPackage.AUDIO_EVENT__PLAYBACK_TIME:
				setPlaybackTime((Float)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MultiplayerPackage.AUDIO_EVENT__PIN_ID:
				setPinId(PIN_ID_EDEFAULT);
				return;
			case MultiplayerPackage.AUDIO_EVENT__PLAY:
				setPlay(PLAY_EDEFAULT);
				return;
			case MultiplayerPackage.AUDIO_EVENT__PLAYBACK_TIME:
				setPlaybackTime(PLAYBACK_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MultiplayerPackage.AUDIO_EVENT__PIN_ID:
				return pinId != PIN_ID_EDEFAULT;
			case MultiplayerPackage.AUDIO_EVENT__PLAY:
				return play != PLAY_EDEFAULT;
			case MultiplayerPackage.AUDIO_EVENT__PLAYBACK_TIME:
				return playbackTime != PLAYBACK_TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (pinId: ");
		result.append(pinId);
		result.append(", play: ");
		result.append(play);
		result.append(", playbackTime: ");
		result.append(playbackTime);
		result.append(')');
		return result.toString();
	}

} //AudioEventImpl

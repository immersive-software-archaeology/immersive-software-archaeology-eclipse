/**
 */
package dk.itu.cs.isa.model.visualization.impl;

import dk.itu.cs.isa.model.visualization.ISASpaceStationBlock;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Space Station Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl#getHeight <em>Height</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl#getDiameter <em>Diameter</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl#isConstructor <em>Constructor</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl#isPublic <em>Public</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl#isSystemEntry <em>System Entry</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISASpaceStationBlockImpl extends ISAAbstractSpaceStationComponentImpl implements ISASpaceStationBlock {
	/**
	 * The default value of the '{@link #getHeight() <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeight()
	 * @generated
	 * @ordered
	 */
	protected static final float HEIGHT_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getHeight() <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeight()
	 * @generated
	 * @ordered
	 */
	protected float height = HEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDiameter() <em>Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiameter()
	 * @generated
	 * @ordered
	 */
	protected static final float DIAMETER_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getDiameter() <em>Diameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiameter()
	 * @generated
	 * @ordered
	 */
	protected float diameter = DIAMETER_EDEFAULT;

	/**
	 * The default value of the '{@link #isConstructor() <em>Constructor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConstructor()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONSTRUCTOR_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isConstructor() <em>Constructor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConstructor()
	 * @generated
	 * @ordered
	 */
	protected boolean constructor = CONSTRUCTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The default value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PUBLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected boolean public_ = PUBLIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isSystemEntry() <em>System Entry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSystemEntry()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SYSTEM_ENTRY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSystemEntry() <em>System Entry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSystemEntry()
	 * @generated
	 * @ordered
	 */
	protected boolean systemEntry = SYSTEM_ENTRY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISASpaceStationBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualizationPackage.Literals.ISA_SPACE_STATION_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getHeight() {
		return height;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeight(float newHeight) {
		float oldHeight = height;
		height = newHeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION_BLOCK__HEIGHT, oldHeight, height));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getDiameter() {
		return diameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiameter(float newDiameter) {
		float oldDiameter = diameter;
		diameter = newDiameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION_BLOCK__DIAMETER, oldDiameter, diameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConstructor() {
		return constructor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstructor(boolean newConstructor) {
		boolean oldConstructor = constructor;
		constructor = newConstructor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION_BLOCK__CONSTRUCTOR, oldConstructor, constructor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION_BLOCK__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPublic() {
		return public_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublic(boolean newPublic) {
		boolean oldPublic = public_;
		public_ = newPublic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION_BLOCK__PUBLIC, oldPublic, public_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSystemEntry() {
		return systemEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystemEntry(boolean newSystemEntry) {
		boolean oldSystemEntry = systemEntry;
		systemEntry = newSystemEntry;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY, oldSystemEntry, systemEntry));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__HEIGHT:
				return getHeight();
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__DIAMETER:
				return getDiameter();
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__CONSTRUCTOR:
				return isConstructor();
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__ABSTRACT:
				return isAbstract();
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__PUBLIC:
				return isPublic();
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY:
				return isSystemEntry();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__HEIGHT:
				setHeight((Float)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__DIAMETER:
				setDiameter((Float)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__CONSTRUCTOR:
				setConstructor((Boolean)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__ABSTRACT:
				setAbstract((Boolean)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__PUBLIC:
				setPublic((Boolean)newValue);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY:
				setSystemEntry((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__HEIGHT:
				setHeight(HEIGHT_EDEFAULT);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__DIAMETER:
				setDiameter(DIAMETER_EDEFAULT);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__CONSTRUCTOR:
				setConstructor(CONSTRUCTOR_EDEFAULT);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__PUBLIC:
				setPublic(PUBLIC_EDEFAULT);
				return;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY:
				setSystemEntry(SYSTEM_ENTRY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__HEIGHT:
				return height != HEIGHT_EDEFAULT;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__DIAMETER:
				return diameter != DIAMETER_EDEFAULT;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__CONSTRUCTOR:
				return constructor != CONSTRUCTOR_EDEFAULT;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__ABSTRACT:
				return abstract_ != ABSTRACT_EDEFAULT;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__PUBLIC:
				return public_ != PUBLIC_EDEFAULT;
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY:
				return systemEntry != SYSTEM_ENTRY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (height: ");
		result.append(height);
		result.append(", diameter: ");
		result.append(diameter);
		result.append(", constructor: ");
		result.append(constructor);
		result.append(", abstract: ");
		result.append(abstract_);
		result.append(", public: ");
		result.append(public_);
		result.append(", systemEntry: ");
		result.append(systemEntry);
		result.append(')');
		return result.toString();
	}

} //ISASpaceStationBlockImpl

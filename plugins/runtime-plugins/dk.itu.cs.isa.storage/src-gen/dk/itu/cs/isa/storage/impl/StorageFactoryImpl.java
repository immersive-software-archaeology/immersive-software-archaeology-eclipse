/**
 */
package dk.itu.cs.isa.storage.impl;

import dk.itu.cs.isa.storage.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StorageFactoryImpl extends EFactoryImpl implements StorageFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StorageFactory init() {
		try {
			StorageFactory theStorageFactory = (StorageFactory)EPackage.Registry.INSTANCE.getEFactory(StoragePackage.eNS_URI);
			if (theStorageFactory != null) {
				return theStorageFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StorageFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StoragePackage.MODEL_STORAGE: return createModelStorage();
			case StoragePackage.MODEL_STORAGE_ENTRY: return createModelStorageEntry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModelStorage createModelStorage() {
		ModelStorageImpl modelStorage = new ModelStorageImpl();
		return modelStorage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ModelStorageEntry createModelStorageEntry() {
		ModelStorageEntryImpl modelStorageEntry = new ModelStorageEntryImpl();
		return modelStorageEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StoragePackage getStoragePackage() {
		return (StoragePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StoragePackage getPackage() {
		return StoragePackage.eINSTANCE;
	}

} //StorageFactoryImpl

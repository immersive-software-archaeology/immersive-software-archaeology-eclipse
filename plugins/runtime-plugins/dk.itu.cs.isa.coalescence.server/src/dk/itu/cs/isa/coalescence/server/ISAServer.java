package dk.itu.cs.isa.coalescence.server;

public interface ISAServer {
	
	public static enum ServerState {
		ONLINE,
		STARTING,
		STOPPING,
		OFFLINE,
		UNKOWN;
	}
	
	
	
	public boolean canBeStarted();
	public boolean canBeStopped();
	
	public ServerState getServerState();

	/**
	 * @return true if the startup was TRIGGERED (not completed!) successfully
	 */
	public boolean startServer();
	/**
	 * @return true if the shutdown was TRIGGERED (not completed!) successfully
	 */
	public boolean stopServer();

	public String getServerAddress();
	
}

/**
 */
package dk.itu.cs.isa.model.software.structure.libraries;

import dk.itu.cs.isa.model.software.structure.misc.ISAType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Primitive Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage#getISAPrimitiveType()
 * @model
 * @generated
 */
public interface ISAPrimitiveType extends ISAType {
} // ISAPrimitiveType

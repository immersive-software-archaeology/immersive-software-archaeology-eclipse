package dk.itu.cs.isa.model.visualization.creation.java.util;

import java.util.Random;

import dk.itu.cs.isa.model.visualization.ISAVector3;
import dk.itu.cs.isa.model.visualization.VisualizationFactory;

public class Vector3DUtil {
	
	private static final VisualizationFactory factory = VisualizationFactory.eINSTANCE;
	
	
	
	// align along X and Z axes
	public static ISAVector3 calculateVertexPositionIn01Circle(int vertexIndex, int circleResolution) {
		float angle = (float) vertexIndex * 360f / (float) circleResolution;
		return createVector(Math.cos(Math.toRadians(angle)), 0d, Math.sin(Math.toRadians(angle)));
	}

	public static ISAVector3 createRandomVector(Random rand, float minMagnitude, float maxMagnitude) {
		ISAVector3 randomVector = createRandomNormalizedVector(rand);
		float randomMagnitude = minMagnitude + rand.nextFloat() * (maxMagnitude - minMagnitude);
		multiplyVector(randomVector, randomMagnitude);
		return randomVector;
	}

	public static ISAVector3 createRandomNormalizedVector(Random rand) {
		float x = (float) Math.sin(Math.toRadians(rand.nextFloat() * 360f));
		float y = (float) Math.sin(Math.toRadians(rand.nextFloat() * 360f));
		float z = (float) Math.sin(Math.toRadians(rand.nextFloat() * 360f));
		return createVector(x, y, z);
	}
	
	
	
	public static float calculateDistance(ISAVector3 v1, ISAVector3 v2) {
		return (float) Math.sqrt(Math.pow(v1.getX()-v2.getX(), 2) + Math.pow(v1.getY()-v2.getY(), 2) + Math.pow(v1.getZ()-v2.getZ(), 2));
	}
	
	public static void addVector(ISAVector3 base, ISAVector3 toAdd) {
		base.setX(base.getX() + toAdd.getX());
		base.setY(base.getY() + toAdd.getY());
		base.setZ(base.getZ() + toAdd.getZ());
	}

	public static void multiplyVector(ISAVector3 base, float scalar) {
		base.setX(base.getX() * scalar);
		base.setY(base.getY() * scalar);
		base.setZ(base.getZ() * scalar);
	}
	
	public static ISAVector3 createVector(float x, float y, float z) {
		ISAVector3 vec = factory.createISAVector3();
		vec.setX(x);
		vec.setY(y);
		vec.setZ(z);
		return vec;
	}
	
	public static ISAVector3 createVector(double x, double y, double z) {
		return createVector((float) x, (float) y, (float) z);
	}
	
}

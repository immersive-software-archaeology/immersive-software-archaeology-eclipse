/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemFactory
 * @model kind="package"
 * @generated
 */
public interface FilesystemPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "filesystem";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/software/structure/filesystem";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "filesystem";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FilesystemPackage eINSTANCE = dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAProjectImpl <em>ISA Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAProjectImpl
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl#getISAProject()
	 * @generated
	 */
	int ISA_PROJECT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PROJECT__NAME = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PROJECT__PARENT = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Package Roots</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PROJECT__PACKAGE_ROOTS = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>ISA Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PROJECT_FEATURE_COUNT = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PROJECT___GET_FULLY_QUALIFIED_NAME = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PROJECT___TO_STRING = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING;

	/**
	 * The operation id for the '<em>Get Package</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PROJECT___GET_PACKAGE__STRING = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PROJECT___GET_ELEMENT__STRING = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>ISA Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PROJECT_OPERATION_COUNT = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageContainerImpl <em>ISA Package Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageContainerImpl
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl#getISAPackageContainer()
	 * @generated
	 */
	int ISA_PACKAGE_CONTAINER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_CONTAINER__NAME = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Sub Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_CONTAINER__SUB_PACKAGES = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>ISA Package Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_CONTAINER_FEATURE_COUNT = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_CONTAINER___GET_FULLY_QUALIFIED_NAME = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_CONTAINER___TO_STRING = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Package Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_CONTAINER_OPERATION_COUNT = MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageRootImpl <em>ISA Package Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageRootImpl
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl#getISAPackageRoot()
	 * @generated
	 */
	int ISA_PACKAGE_ROOT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_ROOT__NAME = ISA_PACKAGE_CONTAINER__NAME;

	/**
	 * The feature id for the '<em><b>Sub Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_ROOT__SUB_PACKAGES = ISA_PACKAGE_CONTAINER__SUB_PACKAGES;

	/**
	 * The feature id for the '<em><b>Parent Project</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_ROOT__PARENT_PROJECT = ISA_PACKAGE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Binary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_ROOT__BINARY = ISA_PACKAGE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>ISA Package Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_ROOT_FEATURE_COUNT = ISA_PACKAGE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_ROOT___GET_FULLY_QUALIFIED_NAME = ISA_PACKAGE_CONTAINER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_ROOT___TO_STRING = ISA_PACKAGE_CONTAINER___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Package Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_ROOT_OPERATION_COUNT = ISA_PACKAGE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageImpl <em>ISA Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageImpl
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl#getISAPackage()
	 * @generated
	 */
	int ISA_PACKAGE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE__NAME = ISA_PACKAGE_CONTAINER__NAME;

	/**
	 * The feature id for the '<em><b>Sub Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE__SUB_PACKAGES = ISA_PACKAGE_CONTAINER__SUB_PACKAGES;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE__CLASSIFIERS = ISA_PACKAGE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parent Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE__PARENT_CONTAINER = ISA_PACKAGE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>ISA Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_FEATURE_COUNT = ISA_PACKAGE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE___GET_FULLY_QUALIFIED_NAME = ISA_PACKAGE_CONTAINER___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE___TO_STRING = ISA_PACKAGE_CONTAINER___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PACKAGE_OPERATION_COUNT = ISA_PACKAGE_CONTAINER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject <em>ISA Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Project</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAProject
	 * @generated
	 */
	EClass getISAProject();

	/**
	 * Returns the meta object for the container reference '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getParent()
	 * @see #getISAProject()
	 * @generated
	 */
	EReference getISAProject_Parent();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getPackageRoots <em>Package Roots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Package Roots</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getPackageRoots()
	 * @see #getISAProject()
	 * @generated
	 */
	EReference getISAProject_PackageRoots();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getPackage(java.lang.String) <em>Get Package</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Package</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getPackage(java.lang.String)
	 * @generated
	 */
	EOperation getISAProject__GetPackage__String();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getElement(java.lang.String) <em>Get Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Element</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getElement(java.lang.String)
	 * @generated
	 */
	EOperation getISAProject__GetElement__String();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer <em>ISA Package Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Package Container</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer
	 * @generated
	 */
	EClass getISAPackageContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer#getSubPackages <em>Sub Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Packages</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer#getSubPackages()
	 * @see #getISAPackageContainer()
	 * @generated
	 */
	EReference getISAPackageContainer_SubPackages();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot <em>ISA Package Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Package Root</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot
	 * @generated
	 */
	EClass getISAPackageRoot();

	/**
	 * Returns the meta object for the container reference '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#getParentProject <em>Parent Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Project</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#getParentProject()
	 * @see #getISAPackageRoot()
	 * @generated
	 */
	EReference getISAPackageRoot_ParentProject();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#isBinary <em>Binary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Binary</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#isBinary()
	 * @see #getISAPackageRoot()
	 * @generated
	 */
	EAttribute getISAPackageRoot_Binary();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage <em>ISA Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Package</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage
	 * @generated
	 */
	EClass getISAPackage();

	/**
	 * Returns the meta object for the container reference '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage#getParentContainer <em>Parent Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Container</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage#getParentContainer()
	 * @see #getISAPackage()
	 * @generated
	 */
	EReference getISAPackage_ParentContainer();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FilesystemFactory getFilesystemFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAProjectImpl <em>ISA Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAProjectImpl
		 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl#getISAProject()
		 * @generated
		 */
		EClass ISA_PROJECT = eINSTANCE.getISAProject();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_PROJECT__PARENT = eINSTANCE.getISAProject_Parent();

		/**
		 * The meta object literal for the '<em><b>Package Roots</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_PROJECT__PACKAGE_ROOTS = eINSTANCE.getISAProject_PackageRoots();

		/**
		 * The meta object literal for the '<em><b>Get Package</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_PROJECT___GET_PACKAGE__STRING = eINSTANCE.getISAProject__GetPackage__String();

		/**
		 * The meta object literal for the '<em><b>Get Element</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_PROJECT___GET_ELEMENT__STRING = eINSTANCE.getISAProject__GetElement__String();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageContainerImpl <em>ISA Package Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageContainerImpl
		 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl#getISAPackageContainer()
		 * @generated
		 */
		EClass ISA_PACKAGE_CONTAINER = eINSTANCE.getISAPackageContainer();

		/**
		 * The meta object literal for the '<em><b>Sub Packages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_PACKAGE_CONTAINER__SUB_PACKAGES = eINSTANCE.getISAPackageContainer_SubPackages();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageRootImpl <em>ISA Package Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageRootImpl
		 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl#getISAPackageRoot()
		 * @generated
		 */
		EClass ISA_PACKAGE_ROOT = eINSTANCE.getISAPackageRoot();

		/**
		 * The meta object literal for the '<em><b>Parent Project</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_PACKAGE_ROOT__PARENT_PROJECT = eINSTANCE.getISAPackageRoot_ParentProject();

		/**
		 * The meta object literal for the '<em><b>Binary</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_PACKAGE_ROOT__BINARY = eINSTANCE.getISAPackageRoot_Binary();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageImpl <em>ISA Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageImpl
		 * @see dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl#getISAPackage()
		 * @generated
		 */
		EClass ISA_PACKAGE = eINSTANCE.getISAPackage();

		/**
		 * The meta object literal for the '<em><b>Parent Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_PACKAGE__PARENT_CONTAINER = eINSTANCE.getISAPackage_ParentContainer();

	}

} //FilesystemPackage

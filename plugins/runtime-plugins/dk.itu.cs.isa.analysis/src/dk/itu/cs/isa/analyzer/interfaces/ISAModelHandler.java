package dk.itu.cs.isa.analyzer.interfaces;

public interface ISAModelHandler {
	
	/**
	 * Method that is called once upon first run of the analysis
	 */
	public void initializeInstance();
	
	/**
	 * Method that is called before each analysis
	 */
	public void setupBeforeRun();
	
	/**
	 * Method that is called after each analysis
	 */
	public void cleanUpAfterRun();

}

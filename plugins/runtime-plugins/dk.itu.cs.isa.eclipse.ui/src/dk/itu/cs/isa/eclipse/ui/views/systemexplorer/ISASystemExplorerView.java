package dk.itu.cs.isa.eclipse.ui.views.systemexplorer;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.ISetSelectionTarget;
import org.eclipse.ui.part.ViewPart;

import dk.itu.cs.isa.eclipse.ui.ISAUpdatableView;
import dk.itu.cs.isa.eclipse.ui.ImageHelper;
import dk.itu.cs.isa.eclipse.ui.SystemModelsObserver;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.events.events.StringChoiceEvent;
import dk.itu.cs.isa.preferences.ISAPreferences;
import dk.itu.cs.isa.storage.ISAModelStorage;



public class ISASystemExplorerView extends ViewPart implements ISAUpdatableView {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "dk.itu.cs.isa.eclipse.ui.views.ISASolarSystemView";
	private final int INITIAL_EXPAND_LEVEL = 3;

	@Inject
	IWorkbench workbench;

	private TreeViewer viewer;
	private ISASystemExplorerViewContentProvider contentProvider;
//	private DrillDownAdapter drillDownAdapter;
	
	private Action selectSystemAction;
	private Action showInEditorAction;
	private Action copyQualifiedNameAction;
	private Action showInProjectExplorerAction;
	private Action expandAllAction;
	private Action collapseAllAction;
	
	
	
	private static List<ISAUpdatableView> systemSelectionListeners = new LinkedList<>();
	
	public static void registerSystemSelectionListener(ISAUpdatableView newListener) {
		systemSelectionListeners.add(newListener);
	}
	
	
	
	public ISASystemExplorerView() {
		SystemModelsObserver.registerView(this);
	}



	@Override
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = viewer.getStructuredSelection();
				Object selectedElement = selection.getFirstElement();
				if(selectedElement instanceof ISASystemExplorerNode) {
					showInEditorAction.setEnabled(((ISASystemExplorerNode) selectedElement).isClassifierNode());
					showInProjectExplorerAction.setEnabled(((ISASystemExplorerNode) selectedElement).isClassifierNode());
				}
			}
		});
//		drillDownAdapter = new DrillDownAdapter(viewer);
		
		contentProvider = new ISASystemExplorerViewContentProvider(this);
		viewer.setContentProvider(contentProvider);
		viewer.setInput(getViewSite());
//		viewer.setLabelProvider(new LabelProvider() {
//			public String getText(Object obj) {
//				return obj.toString();
//			}
//
//			public Image getImage(Object obj) {
////				String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
////				if (obj instanceof ISASolarSystemParentNode)
////					imageKey = ISharedImages.IMG_OBJ_FOLDER;
////				return workbench.getSharedImages().getImage(imageKey);
//				return ((ISASolarSystemNode) obj).getImage();
//			}
//		});
		ColumnViewerToolTipSupport.enableFor(viewer);
		viewer.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public void update(ViewerCell cell) {
				cell.setText(cell.getElement().toString());
				cell.setImage(((ISASystemExplorerNode) cell.getElement()).getImage());
			}
			
			@Override
			public String getToolTipText(Object element) {
				return ((ISASystemExplorerNode) element).getToolTipMessage();
			}

			@Override
			public int getToolTipDisplayDelayTime(Object object) {
				return 100;
			}

			@Override
			public int getToolTipTimeDisplayed(Object object) {
				return 20000;
			}
		});

		// Create the help context id for the viewer's control
		if(workbench == null)
			workbench = getSite().getWorkbenchWindow().getWorkbench();
		workbench.getHelpSystem().setHelp(viewer.getControl(), ID);
		getSite().setSelectionProvider(viewer);
		makeActions();
		hookContextMenu();
		contributeToActionBars();
		
		viewer.expandToLevel(INITIAL_EXPAND_LEVEL);
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				ISASystemExplorerView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
//		manager.add(selectSystemAction);
//		manager.add(new Separator());
//		manager.add(showInPackageExplorerAction);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(showInEditorAction);
		manager.add(showInProjectExplorerAction);
		manager.add(copyQualifiedNameAction);
		manager.add(new Separator());
		manager.add(selectSystemAction);
		manager.add(new Separator());
//		drillDownAdapter.addNavigationActions(manager);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(selectSystemAction);
		manager.add(new Separator());
		manager.add(expandAllAction);
		manager.add(collapseAllAction);
		manager.add(new Separator());
//		drillDownAdapter.addNavigationActions(manager);
	}

	@Override
	public void update() {
		contentProvider.initialize();
		viewer.refresh();
		viewer.expandToLevel(INITIAL_EXPAND_LEVEL);
	}
	
	private void makeActions() {
		selectSystemAction = new Action() {
			public void run() {
				if(ISAModelStorage.INSTANCE.getAllEntryNames().isEmpty()) {
					ISAEventSystem.fireInformationMessage("No Models Found",
							"There are currently no models of analyzed systems available to display here.\n\n"
							+ "Please analyze a selection of Eclipse projects via the ISA System Analysis functionality.\n"
							+ "These can then be inspected in the System Explorer.");
					return;
				}
				
				StringChoiceEvent result = ISAEventSystem.fireStringChoiceDialog(
						"ISA System Explorer View",
						"Select a model to inspect",
						ISAModelStorage.INSTANCE.getAllEntryNames(),
						ISAPreferences.getString(ISAPreferences.LAST_MODEL_NAME, null)
				);
				if(result.getReturnCode() == 0) {
					String selectedModelName = result.getSelection();
					if(selectedModelName != null) {
						ISAPreferences.setString(ISAPreferences.LAST_MODEL_NAME, selectedModelName);
						
						update();
						for(ISAUpdatableView listener : systemSelectionListeners) {
							listener.update();
						}
					}
				}
			}
		};
		selectSystemAction.setText("Select Different System");
		selectSystemAction.setToolTipText("Select a system to display from the list of all systems");
		selectSystemAction.setImageDescriptor(ImageHelper.getImageDescriptor("icons/icon-16-reload.png"));

		showInEditorAction = new Action() {
			public void run() {
				TreeSelection selection = (TreeSelection) viewer.getStructuredSelection();
				for(TreePath path : selection.getPaths()) {
					Object selectedElement = path.getLastSegment();
					if(!(selectedElement instanceof ISASystemExplorerNode))
						continue;
//					if((((ISASystemExplorerNode) selectedElement).getVisualizationModelElement())
//						continue;
					if(showInEditor(((ISASystemExplorerNode) selectedElement).getFile()))
						continue;
					
					// Did not open the editor, because user double clicked on something that is not a building
					if(!viewer.getExpandedState(selectedElement))
						viewer.expandToLevel(selectedElement, 1);
					else
						viewer.collapseToLevel(selectedElement, AbstractTreeViewer.ALL_LEVELS);
				}
			}
		};
		showInEditorAction.setEnabled(false);
		showInEditorAction.setText("Show in Editor");
		showInEditorAction.setToolTipText("Show the selected element in the Editor");
		showInEditorAction.setImageDescriptor(ImageHelper.getImageDescriptor("platform:/plugin/org.eclipse.jdt.ui/icons/full/obj16/jcu_obj.png"));
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				showInEditorAction.run();
			}
		});
		
		showInProjectExplorerAction = new Action() {
			public void run() {
				List<IFile> selectedFiles = new ArrayList<>();
				TreeSelection selection = (TreeSelection) viewer.getStructuredSelection();
				for(TreePath path : selection.getPaths()) {
					Object selectedElement = path.getLastSegment();
					if(selectedElement instanceof ISASystemExplorerNode) {
						selectedFiles.add(((ISASystemExplorerNode) selectedElement).getFile());
					}
				}
				showInPackageExplorer(selectedFiles);
			}
		};
		showInProjectExplorerAction.setText("Show in Project Explorer");
		showInProjectExplorerAction.setToolTipText("Show the selected element in the Project Explorer view");
		showInProjectExplorerAction.setImageDescriptor(ImageHelper.getImageDescriptor("platform:/plugin/org.eclipse.ui.navigator.resources/icons/full/obj16/nested_projects.png"));
		
		expandAllAction = new Action() {
			public void run() {
				viewer.expandAll();
			}
		};
		expandAllAction.setText("Expand All");
		expandAllAction.setToolTipText("Expand all entries of the solar system view");
		expandAllAction.setImageDescriptor(ImageHelper.getImageDescriptor("platform:/plugin/org.eclipse.ui/icons/full/elcl16/expandall.png"));

		copyQualifiedNameAction = new Action() {
			public void run() {
				IStructuredSelection selection = viewer.getStructuredSelection();
				Object selectedElement = selection.getFirstElement();
				if(selectedElement instanceof ISASystemExplorerNode) {
					StringSelection stringSelection = new StringSelection(((ISASystemExplorerNode) selectedElement).getQualifiedName());
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard.setContents(stringSelection, null);
				}
			}
		};
		copyQualifiedNameAction.setText("Copy Qualified Name");
		copyQualifiedNameAction.setToolTipText("Copy the qualified name of the selected element to your clipboard");
		copyQualifiedNameAction.setImageDescriptor(ImageHelper.getImageDescriptor("platform:/plugin/org.eclipse.ui/icons/full/etool16/copy_edit.png"));

		collapseAllAction = new Action() {
			public void run() {
				viewer.collapseAll();
			}
		};
		collapseAllAction.setText("Collapse All");
		collapseAllAction.setToolTipText("Collapse all entries of the solar system view");
		collapseAllAction.setImageDescriptor(ImageHelper.getImageDescriptor("platform:/plugin/org.eclipse.ui/icons/full/elcl16/collapseall.png"));
	}

	private void showInPackageExplorer(List<IFile> files) {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IViewPart view = page.findView(IPageLayout.ID_PROJECT_EXPLORER);
		if(view == null) {
			try {
				page.showView(IPageLayout.ID_PROJECT_EXPLORER);
				view = page.findView(IPageLayout.ID_PROJECT_EXPLORER);
			} catch (PartInitException e) {
				ISAEventSystem.fireErrorMessage("Cannot open Project Explorer", "The project explorer is not available.", e);
				return;
			}
		}
		((ISetSelectionTarget) view).selectReveal(new StructuredSelection(files));
		page.activate(view);
	}

	private boolean showInEditor(IFile file) {
		if(file == null || workbench.getActiveWorkbenchWindow() == null)
			return false;
		
		IWorkbenchPage page = workbench.getActiveWorkbenchWindow().getActivePage();
		if(page == null)
			return false;
		
		IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.getName());
		try {
			page.openEditor(new FileEditorInput(file), desc.getId());
		} catch (PartInitException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	@Override
	public void dispose() {
		SystemModelsObserver.unregisterView(this);
		viewer.getLabelProvider().dispose();
		super.dispose();
	}
	
}

















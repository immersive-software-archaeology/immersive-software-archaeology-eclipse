/**
 */
package dk.itu.cs.isa.model.system.quality.impl;

import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;

import dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry;
import dk.itu.cs.isa.model.system.quality.ISAFilePassage;
import dk.itu.cs.isa.model.system.quality.ISAIssue;
import dk.itu.cs.isa.model.system.quality.ISAMeasure;
import dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity;
import dk.itu.cs.isa.model.system.quality.ISAQualityIssueType;
import dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel;
import dk.itu.cs.isa.model.system.quality.QualityFactory;
import dk.itu.cs.isa.model.system.quality.QualityPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class QualityPackageImpl extends EPackageImpl implements QualityPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaSoftwareSystemQualityModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaClassifierQualityEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaMeasureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaIssueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaFilePassageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum isaQualityIssueTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum isaQualityIssueSeverityEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private QualityPackageImpl() {
		super(eNS_URI, QualityFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link QualityPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static QualityPackage init() {
		if (isInited) return (QualityPackage)EPackage.Registry.INSTANCE.getEPackage(QualityPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredQualityPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		QualityPackageImpl theQualityPackage = registeredQualityPackage instanceof QualityPackageImpl ? (QualityPackageImpl)registeredQualityPackage : new QualityPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		StructurePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theQualityPackage.createPackageContents();

		// Initialize created meta-data
		theQualityPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theQualityPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(QualityPackage.eNS_URI, theQualityPackage);
		return theQualityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISASoftwareSystemQualityModel() {
		return isaSoftwareSystemQualityModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISASoftwareSystemQualityModel_ClassifierEntries() {
		return (EReference)isaSoftwareSystemQualityModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISASoftwareSystemQualityModel_Name() {
		return (EAttribute)isaSoftwareSystemQualityModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAClassifierQualityEntry() {
		return isaClassifierQualityEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClassifierQualityEntry_Classifier() {
		return (EReference)isaClassifierQualityEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClassifierQualityEntry_Measures() {
		return (EReference)isaClassifierQualityEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAClassifierQualityEntry_Issues() {
		return (EReference)isaClassifierQualityEntryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAMeasure() {
		return isaMeasureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAMeasure_ParentEntry() {
		return (EReference)isaMeasureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAMeasure_Metric() {
		return (EAttribute)isaMeasureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAMeasure_Value() {
		return (EAttribute)isaMeasureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAMeasure_BestValue() {
		return (EAttribute)isaMeasureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAIssue() {
		return isaIssueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAIssue_ParentEntry() {
		return (EReference)isaIssueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAIssue_Type() {
		return (EAttribute)isaIssueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAIssue_Name() {
		return (EAttribute)isaIssueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAIssue_Occurence() {
		return (EReference)isaIssueEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAIssue_Severity() {
		return (EAttribute)isaIssueEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAIssue_Hint() {
		return (EAttribute)isaIssueEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAIssue_EffortToFixInMinutes() {
		return (EAttribute)isaIssueEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAFilePassage() {
		return isaFilePassageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAFilePassage_StartLine() {
		return (EAttribute)isaFilePassageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAFilePassage_EndLine() {
		return (EAttribute)isaFilePassageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAFilePassage_StartChar() {
		return (EAttribute)isaFilePassageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAFilePassage_EndChar() {
		return (EAttribute)isaFilePassageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getISAQualityIssueType() {
		return isaQualityIssueTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getISAQualityIssueSeverity() {
		return isaQualityIssueSeverityEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public QualityFactory getQualityFactory() {
		return (QualityFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaSoftwareSystemQualityModelEClass = createEClass(ISA_SOFTWARE_SYSTEM_QUALITY_MODEL);
		createEReference(isaSoftwareSystemQualityModelEClass, ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__CLASSIFIER_ENTRIES);
		createEAttribute(isaSoftwareSystemQualityModelEClass, ISA_SOFTWARE_SYSTEM_QUALITY_MODEL__NAME);

		isaClassifierQualityEntryEClass = createEClass(ISA_CLASSIFIER_QUALITY_ENTRY);
		createEReference(isaClassifierQualityEntryEClass, ISA_CLASSIFIER_QUALITY_ENTRY__CLASSIFIER);
		createEReference(isaClassifierQualityEntryEClass, ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES);
		createEReference(isaClassifierQualityEntryEClass, ISA_CLASSIFIER_QUALITY_ENTRY__ISSUES);

		isaMeasureEClass = createEClass(ISA_MEASURE);
		createEReference(isaMeasureEClass, ISA_MEASURE__PARENT_ENTRY);
		createEAttribute(isaMeasureEClass, ISA_MEASURE__METRIC);
		createEAttribute(isaMeasureEClass, ISA_MEASURE__VALUE);
		createEAttribute(isaMeasureEClass, ISA_MEASURE__BEST_VALUE);

		isaIssueEClass = createEClass(ISA_ISSUE);
		createEReference(isaIssueEClass, ISA_ISSUE__PARENT_ENTRY);
		createEAttribute(isaIssueEClass, ISA_ISSUE__TYPE);
		createEAttribute(isaIssueEClass, ISA_ISSUE__NAME);
		createEReference(isaIssueEClass, ISA_ISSUE__OCCURENCE);
		createEAttribute(isaIssueEClass, ISA_ISSUE__SEVERITY);
		createEAttribute(isaIssueEClass, ISA_ISSUE__HINT);
		createEAttribute(isaIssueEClass, ISA_ISSUE__EFFORT_TO_FIX_IN_MINUTES);

		isaFilePassageEClass = createEClass(ISA_FILE_PASSAGE);
		createEAttribute(isaFilePassageEClass, ISA_FILE_PASSAGE__START_LINE);
		createEAttribute(isaFilePassageEClass, ISA_FILE_PASSAGE__END_LINE);
		createEAttribute(isaFilePassageEClass, ISA_FILE_PASSAGE__START_CHAR);
		createEAttribute(isaFilePassageEClass, ISA_FILE_PASSAGE__END_CHAR);

		// Create enums
		isaQualityIssueTypeEEnum = createEEnum(ISA_QUALITY_ISSUE_TYPE);
		isaQualityIssueSeverityEEnum = createEEnum(ISA_QUALITY_ISSUE_SEVERITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage)EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(isaSoftwareSystemQualityModelEClass, ISASoftwareSystemQualityModel.class, "ISASoftwareSystemQualityModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISASoftwareSystemQualityModel_ClassifierEntries(), this.getISAClassifierQualityEntry(), null, "classifierEntries", null, 0, -1, ISASoftwareSystemQualityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASoftwareSystemQualityModel_Name(), ecorePackage.getEString(), "name", null, 1, 1, ISASoftwareSystemQualityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaClassifierQualityEntryEClass, ISAClassifierQualityEntry.class, "ISAClassifierQualityEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAClassifierQualityEntry_Classifier(), theClassifiersPackage.getISAClassifier(), null, "classifier", null, 1, 1, ISAClassifierQualityEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAClassifierQualityEntry_Measures(), this.getISAMeasure(), this.getISAMeasure_ParentEntry(), "measures", null, 0, -1, ISAClassifierQualityEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAClassifierQualityEntry_Issues(), this.getISAIssue(), this.getISAIssue_ParentEntry(), "issues", null, 0, -1, ISAClassifierQualityEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaMeasureEClass, ISAMeasure.class, "ISAMeasure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAMeasure_ParentEntry(), this.getISAClassifierQualityEntry(), this.getISAClassifierQualityEntry_Measures(), "parentEntry", null, 1, 1, ISAMeasure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAMeasure_Metric(), ecorePackage.getEString(), "metric", null, 1, 1, ISAMeasure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAMeasure_Value(), ecorePackage.getEFloat(), "value", null, 1, 1, ISAMeasure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAMeasure_BestValue(), ecorePackage.getEBoolean(), "bestValue", "false", 1, 1, ISAMeasure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaIssueEClass, ISAIssue.class, "ISAIssue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAIssue_ParentEntry(), this.getISAClassifierQualityEntry(), this.getISAClassifierQualityEntry_Issues(), "parentEntry", null, 1, 1, ISAIssue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAIssue_Type(), this.getISAQualityIssueType(), "type", null, 1, 1, ISAIssue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAIssue_Name(), ecorePackage.getEString(), "name", null, 0, 1, ISAIssue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAIssue_Occurence(), this.getISAFilePassage(), null, "occurence", null, 0, 1, ISAIssue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAIssue_Severity(), this.getISAQualityIssueSeverity(), "severity", null, 1, 1, ISAIssue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAIssue_Hint(), ecorePackage.getEString(), "hint", null, 0, 1, ISAIssue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAIssue_EffortToFixInMinutes(), ecorePackage.getEInt(), "effortToFixInMinutes", null, 0, 1, ISAIssue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaFilePassageEClass, ISAFilePassage.class, "ISAFilePassage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAFilePassage_StartLine(), ecorePackage.getEInt(), "startLine", null, 1, 1, ISAFilePassage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAFilePassage_EndLine(), ecorePackage.getEInt(), "endLine", null, 1, 1, ISAFilePassage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAFilePassage_StartChar(), ecorePackage.getEInt(), "startChar", null, 1, 1, ISAFilePassage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAFilePassage_EndChar(), ecorePackage.getEInt(), "endChar", null, 1, 1, ISAFilePassage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(isaQualityIssueTypeEEnum, ISAQualityIssueType.class, "ISAQualityIssueType");
		addEEnumLiteral(isaQualityIssueTypeEEnum, ISAQualityIssueType.CODE_SMELL);
		addEEnumLiteral(isaQualityIssueTypeEEnum, ISAQualityIssueType.BUG);
		addEEnumLiteral(isaQualityIssueTypeEEnum, ISAQualityIssueType.VULNERABILITY);

		initEEnum(isaQualityIssueSeverityEEnum, ISAQualityIssueSeverity.class, "ISAQualityIssueSeverity");
		addEEnumLiteral(isaQualityIssueSeverityEEnum, ISAQualityIssueSeverity.MINOR);
		addEEnumLiteral(isaQualityIssueSeverityEEnum, ISAQualityIssueSeverity.MAJOR);
		addEEnumLiteral(isaQualityIssueSeverityEEnum, ISAQualityIssueSeverity.CRITICAL);

		// Create resource
		createResource(eNS_URI);
	}

} //QualityPackageImpl

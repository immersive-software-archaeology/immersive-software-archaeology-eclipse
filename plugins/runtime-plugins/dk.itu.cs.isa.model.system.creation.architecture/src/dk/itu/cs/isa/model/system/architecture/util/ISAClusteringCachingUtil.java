package dk.itu.cs.isa.model.system.architecture.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;

public class ISAClusteringCachingUtil {
	
	public static ISAClusteringCachingUtil INSTANCE = new ISAClusteringCachingUtil();
	
	private ISAClusteringCachingUtil() {}
	


	private Map<ISAClassifier, ClusterInformation> classifierClusteringInfoMap;
	private Map<ISACluster, ClusterInformation> clusterClusteringInfoMap;
	
	private Map<ClusterInformation, Map<ClusterInformation, Double>> similarityMap;

	private Map<ISAReference<? extends ISAReferenceableElement>, String> referenceToTargetNameHashMap;
	private Map<ISACluster, List<ISAReference<? extends ISAReferenceableElement>>> componentReferenceHashMap;
	private Map<ISACluster, List<String>> componentPackageNamesHashMap;
	private Map<ISACluster, List<String>> componentClassifierNamesHashMap;
	private Map<ISACluster, Integer> componentContentCountHashMap;
	

	
	public void reset() {
		classifierClusteringInfoMap = new HashMap<>();
		clusterClusteringInfoMap = new HashMap<>();
		similarityMap = new HashMap<>();

		referenceToTargetNameHashMap = new HashMap<>();
		componentReferenceHashMap = new HashMap<>();
		componentPackageNamesHashMap = new HashMap<>();
		componentClassifierNamesHashMap = new HashMap<>();
		componentContentCountHashMap = new HashMap<>();
	}
	


	public ClusterInformation getCachedClusterInformation(ISAClassifier cl) {
		ClusterInformation cachedInfo = classifierClusteringInfoMap.get(cl);
		if(cachedInfo == null)
			cacheClusterInformation(cl);
		return classifierClusteringInfoMap.get(cl);
	}

	public synchronized void cacheClusterInformation(ISAClassifier cl) {
		classifierClusteringInfoMap.put(cl, new ClusterInformation(cl));
	}

	public ClusterInformation getCachedClusterInformation(ISACluster cl) {
		ClusterInformation cachedInfo = clusterClusteringInfoMap.get(cl);
		if(cachedInfo == null)
			cacheClusterInformation(cl);
		return clusterClusteringInfoMap.get(cl);
	}

	public synchronized void cacheClusterInformation(ISACluster cl) {
		clusterClusteringInfoMap.put(cl, new ClusterInformation(cl));
	}
	
	

	public double getCachedSimilarity(ClusterInformation info1, ClusterInformation info2) {
		Map<ClusterInformation, Double> entry1 = similarityMap.get(info1);
		Map<ClusterInformation, Double> entry2 = similarityMap.get(info2);
		
		if(entry1 != null) {
			Double result = entry1.get(info2);
			if(result != null)
				return result;
		}
		if(entry2 != null) {
			Double result = entry2.get(info1);
			if(result != null)
				return result;
		}
		return -1;
	}

	public synchronized void cacheSimilarity(ClusterInformation info1, ClusterInformation info2, double similarity) {
		Map<ClusterInformation, Double> entry = similarityMap.get(info1);
		if(entry == null) {
			entry = new HashMap<>();
			similarityMap.put(info1, entry);
		}
//		System.out.println("caching: " + System.currentTimeMillis());
		
		entry.put(info2, similarity);
	}
	


	public String getCachedReferenceTargetName(ISAReference<? extends ISAReferenceableElement> ref) {
		return referenceToTargetNameHashMap.get(ref);
	}

	public synchronized void cacheReferenceTargetName(ISAReference<? extends ISAReferenceableElement> ref, String name) {
		referenceToTargetNameHashMap.put(ref, name);
	}
	
	public List<ISAReference<? extends ISAReferenceableElement>> getCachedReferences(ISACluster c) {
		return componentReferenceHashMap.get(c);
	}

	public synchronized void cacheReferences(ISACluster c, List<ISAReference<? extends ISAReferenceableElement>> l) {
		componentReferenceHashMap.put(c, l);
	}

	public List<String> getCachedClassifierNames(ISACluster c) {
		return componentClassifierNamesHashMap.get(c);
	}

	public synchronized void cacheClassifierNames(ISACluster c, List<String> cl) {
		componentClassifierNamesHashMap.put(c, cl);
	}

	public List<String> getCachedPackageNames(ISACluster c) {
		return componentPackageNamesHashMap.get(c);
	}

	public synchronized void cachePackageNames(ISACluster c, List<String> p) {
		componentPackageNamesHashMap.put(c, p);
	}

	public Integer getCachedContentCount(ISACluster c) {
		return componentContentCountHashMap.get(c);
	}

	public synchronized void cacheContentCount(ISACluster c, int i) {
		componentContentCountHashMap.put(c, i);
	}

}

/**
 */
package dk.itu.cs.isa.model.visualization.impl;

import dk.itu.cs.isa.model.visualization.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VisualizationFactoryImpl extends EFactoryImpl implements VisualizationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VisualizationFactory init() {
		try {
			VisualizationFactory theVisualizationFactory = (VisualizationFactory)EPackage.Registry.INSTANCE.getEFactory(VisualizationPackage.eNS_URI);
			if (theVisualizationFactory != null) {
				return theVisualizationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VisualizationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualizationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case VisualizationPackage.ISA_NAMED_VISUAL_ELEMENT: return createISANamedVisualElement();
			case VisualizationPackage.ISA_VISUALIZATION_MODEL: return createISAVisualizationModel();
			case VisualizationPackage.ISA_VECTOR3: return createISAVector3();
			case VisualizationPackage.ISA_TRANSPARENT_CAPSULE: return createISATransparentCapsule();
			case VisualizationPackage.ISA_SPACE_STATION: return createISASpaceStation();
			case VisualizationPackage.ISA_SPACE_STATION_BLOCK: return createISASpaceStationBlock();
			case VisualizationPackage.ISA_SPACE_STATION_ANTENNA: return createISASpaceStationAntenna();
			case VisualizationPackage.ISA_SPACE_STATION_RELATION: return createISASpaceStationRelation();
			case VisualizationPackage.ISA_BLOCK_RELATION: return createISABlockRelation();
			case VisualizationPackage.ISA_ANTENNA_RELATION: return createISAAntennaRelation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case VisualizationPackage.ISA_SPACE_STATION_TYPE:
				return createISASpaceStationTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case VisualizationPackage.ISA_SPACE_STATION_TYPE:
				return convertISASpaceStationTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISANamedVisualElement createISANamedVisualElement() {
		ISANamedVisualElementImpl isaNamedVisualElement = new ISANamedVisualElementImpl();
		return isaNamedVisualElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVisualizationModel createISAVisualizationModel() {
		ISAVisualizationModelImpl isaVisualizationModel = new ISAVisualizationModelImpl();
		return isaVisualizationModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 createISAVector3() {
		ISAVector3Impl isaVector3 = new ISAVector3Impl();
		return isaVector3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISATransparentCapsule createISATransparentCapsule() {
		ISATransparentCapsuleImpl isaTransparentCapsule = new ISATransparentCapsuleImpl();
		return isaTransparentCapsule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASpaceStation createISASpaceStation() {
		ISASpaceStationImpl isaSpaceStation = new ISASpaceStationImpl();
		return isaSpaceStation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASpaceStationBlock createISASpaceStationBlock() {
		ISASpaceStationBlockImpl isaSpaceStationBlock = new ISASpaceStationBlockImpl();
		return isaSpaceStationBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASpaceStationAntenna createISASpaceStationAntenna() {
		ISASpaceStationAntennaImpl isaSpaceStationAntenna = new ISASpaceStationAntennaImpl();
		return isaSpaceStationAntenna;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASpaceStationRelation createISASpaceStationRelation() {
		ISASpaceStationRelationImpl isaSpaceStationRelation = new ISASpaceStationRelationImpl();
		return isaSpaceStationRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISABlockRelation createISABlockRelation() {
		ISABlockRelationImpl isaBlockRelation = new ISABlockRelationImpl();
		return isaBlockRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAAntennaRelation createISAAntennaRelation() {
		ISAAntennaRelationImpl isaAntennaRelation = new ISAAntennaRelationImpl();
		return isaAntennaRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISASpaceStationType createISASpaceStationTypeFromString(EDataType eDataType, String initialValue) {
		ISASpaceStationType result = ISASpaceStationType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertISASpaceStationTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisualizationPackage getVisualizationPackage() {
		return (VisualizationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static VisualizationPackage getPackage() {
		return VisualizationPackage.eINSTANCE;
	}

} //VisualizationFactoryImpl

package dk.itu.cs.isa.whiteboard.events;

import dk.itu.cs.isa.whiteboard.AbstractOperation;
import dk.itu.cs.isa.whiteboard.AbstractPinOperation;
import dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent;
import dk.itu.cs.isa.whiteboard.BoardDestroyEvent;
import dk.itu.cs.isa.whiteboard.BoardDuplicateEvent;
import dk.itu.cs.isa.whiteboard.BoardEnlargeOperation;
import dk.itu.cs.isa.whiteboard.BoardSpawnEvent;
import dk.itu.cs.isa.whiteboard.DrawArrowOperation;
import dk.itu.cs.isa.whiteboard.DrawFreehandOperation;
import dk.itu.cs.isa.whiteboard.DrawModuleOperation;
import dk.itu.cs.isa.whiteboard.PaintClearAllOperation;
import dk.itu.cs.isa.whiteboard.PaintEraseOperation;
import dk.itu.cs.isa.whiteboard.Photo;
import dk.itu.cs.isa.whiteboard.PhotoAddOperation;
import dk.itu.cs.isa.whiteboard.PhotoMoveOperation;
import dk.itu.cs.isa.whiteboard.PhotoRemoveOperation;
import dk.itu.cs.isa.whiteboard.Pin;
import dk.itu.cs.isa.whiteboard.PinAudioAddOperation;
import dk.itu.cs.isa.whiteboard.PinSoftwareAddOperation;
import dk.itu.cs.isa.whiteboard.PinMoveOperation;
import dk.itu.cs.isa.whiteboard.PinRemoveOperation;
import dk.itu.cs.isa.whiteboard.PinSplitOperation;
import dk.itu.cs.isa.whiteboard.PinsRemoveAllOperation;
import dk.itu.cs.isa.whiteboard.RedoEvent;
import dk.itu.cs.isa.whiteboard.UndoEvent;
import dk.itu.cs.isa.whiteboard.Whiteboard;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardHistoryUtil;

public class WhiteboardOperationConsistencyChecker {
	
	private WhiteboardEventHandler operationHandler;
	private static final int OPERATION_STACK_SIZE = 16;

	public WhiteboardOperationConsistencyChecker(WhiteboardEventHandler operationHandler) {
		this.operationHandler = operationHandler;
	}
	
	

	public void check(WhiteboardHistory history, AbstractWhiteboardEvent event) throws WhiteboardOperationInconsistencyException {
		Whiteboard board = operationHandler.getWhiteboard();
		
		if(event.getWhiteboardId() == 0)
			throw new WhiteboardOperationInconsistencyException(event, "An event with target whiteboard id set to 0 is not valid.");

		if(event instanceof UndoEvent || event instanceof RedoEvent) {
			checkUndoRedoConsistency(history, event);
		}
		else if(event instanceof BoardSpawnEvent) {
			if(board != null)
				throw new WhiteboardOperationInconsistencyException(event, "Whiteboard with id=" + event.getWhiteboardId() + " already exists.");
		}
		else {
			if(board == null)
				throw new WhiteboardOperationInconsistencyException(event, "Whiteboard with id=" + event.getWhiteboardId() + " not found.");
			
			if(event instanceof BoardDestroyEvent) {
				if(board.eContainer() == null)
					throw new WhiteboardOperationInconsistencyException(event, "Whiteboard with id=" + event.getWhiteboardId() + " was destroyed already.");
			}
			else if(event instanceof BoardDuplicateEvent) {
				// Nothing to do here, it should always be allowed to duplicate a whiteboard.
			}
			
			else if(event instanceof PinSplitOperation) {
				if(((PinSplitOperation) event).getPinId() <= 0)
					throw new WhiteboardOperationInconsistencyException(event, "Operation did not provide a pin id, id=" + ((PinSplitOperation) event).getPinId());
				
				Pin pin = WhiteboardHistoryUtil.getPinOnWhiteboard(board, ((PinSplitOperation) event).getPinnedElementName());
				if(pin == null)
					throw new WhiteboardOperationInconsistencyException(event, "Pin for element \"" + ((PinSplitOperation) event).getPinnedElementName() + "\" does not exist.");
				
				for(AbstractOperation subOperation : ((PinSplitOperation) event).getSubOperations())
					check(history, subOperation);
			}
			else if(event instanceof PinsRemoveAllOperation) {
				for(AbstractOperation subOperation : ((PinsRemoveAllOperation) event).getSubOperations())
					check(history, subOperation);
			}
			else if(event instanceof PinSoftwareAddOperation) {
				Pin pin = WhiteboardHistoryUtil.getPinOnWhiteboard(board, ((AbstractPinOperation) event).getPinnedElementName());
				if(pin != null)
					throw new WhiteboardOperationInconsistencyException(event, "Pin for element \"" + ((AbstractPinOperation) event).getPinnedElementName() + "\" already on the board.");
			}
			else if(event instanceof PinAudioAddOperation) {
				// Nothing to do here, it should always be allowed to add audio pins.
			}
			else if(event instanceof PinRemoveOperation) {
				int pinId = ((PinRemoveOperation) event).getPinId();
				if(pinId <= 0)
					throw new WhiteboardOperationInconsistencyException(event, "Operation did not provide a pin id, id=" + pinId);
				
				Pin pin = WhiteboardHistoryUtil.getPinOnWhiteboard(board, pinId);
				if(pin == null)
					throw new WhiteboardOperationInconsistencyException(event, "Pin for element \"" + ((AbstractPinOperation) event).getPinnedElementName() + "\" does not exist.");
			}
			else if(event instanceof PinMoveOperation) {
				int pinId = ((PinMoveOperation) event).getPinId();
				if(pinId <= 0)
					throw new WhiteboardOperationInconsistencyException(event, "Operation did not provide a pin id, id=" + pinId);
				
				Pin pin = WhiteboardHistoryUtil.getPinOnWhiteboard(board, pinId);
				if(pin == null)
					throw new WhiteboardOperationInconsistencyException(event, "Pin for element \"" + ((AbstractPinOperation) event).getPinnedElementName() + "\" does not exist.");
			}
			
			else if(event instanceof DrawFreehandOperation) {
				// Nothing to do here, it should always be allowed to draw.
				// Operations will be executed in the order they arrive on the server. 
			}
			else if(event instanceof DrawModuleOperation) {
				// Nothing to do here, it should always be allowed to draw.
				// Operations will be executed in the order they arrive on the server. 
			}
			else if(event instanceof DrawArrowOperation) {
				// Nothing to do here, it should always be allowed to draw.
				// Operations will be executed in the order they arrive on the server. 
			}
			else if(event instanceof PaintClearAllOperation) {
				// Nothing to do here, it should always be allowed to clear all paint.
			}
			else if(event instanceof PaintEraseOperation) {
				// Nothing to do here, it should always be allowed to erase paint.
			}
			
			else if(event instanceof BoardEnlargeOperation) {
				if(board.getSize() >= 3)
					throw new WhiteboardOperationInconsistencyException(event, "Whiteboard with id=" + event.getWhiteboardId() + " cannot be enlarged further.");
			}
			
			else if(event instanceof PhotoAddOperation) {
				Photo photo = WhiteboardHistoryUtil.getPhotoOnWhiteboard(board, ((PhotoAddOperation) event).getPhotoId());
				if(photo != null)
					throw new WhiteboardOperationInconsistencyException(event, "Photo with id \"" + ((PhotoAddOperation) event).getPhotoId() + "\" already on the board.");
			}
			else if(event instanceof PhotoMoveOperation) {
				if(((PhotoMoveOperation) event).getPhotoId() <= 0)
					throw new WhiteboardOperationInconsistencyException(event, "Operation did not provide a photo id, id=" + ((PhotoMoveOperation) event).getPhotoId());
				
				Photo photo = WhiteboardHistoryUtil.getPhotoOnWhiteboard(board, ((PhotoMoveOperation) event).getPhotoId());
				if(photo == null)
					throw new WhiteboardOperationInconsistencyException(event, "Photo with id \"" + ((PhotoMoveOperation) event).getPhotoId() + "\" does not exist.");
			}
			else if(event instanceof PhotoRemoveOperation) {
				if(((PhotoRemoveOperation) event).getPhotoId() <= 0)
					throw new WhiteboardOperationInconsistencyException(event, "Operation did not provide a photo id, id=" + ((PhotoRemoveOperation) event).getPhotoId());
				
				Photo photo = WhiteboardHistoryUtil.getPhotoOnWhiteboard(board, ((PhotoRemoveOperation) event).getPhotoId());
				if(photo == null)
					throw new WhiteboardOperationInconsistencyException(event, "Photo with id \"" + ((PhotoRemoveOperation) event).getPhotoId() + "\" does not exist.");
			}
			
			else {
				throw new UnsupportedOperationException("Unknown kind of event: " + event.getClass().getSimpleName());
			}
		}
	}
	
	
	
	private void checkUndoRedoConsistency(WhiteboardHistory history, AbstractWhiteboardEvent undoRedoEvent) throws WhiteboardOperationInconsistencyException {
		int operationDepth = 0;
		int undoDepth = 0;
		
		for(AbstractWhiteboardEvent event : history.getEventLog()) {
			if(event instanceof UndoEvent) {
				undoDepth ++;
			}
			else if(event instanceof RedoEvent) {
				undoDepth --;
			}
			else if(event instanceof AbstractOperation) {
				operationDepth ++;
				undoDepth = 0;
			}
			throwExceptionIfInvalidStack(undoRedoEvent, undoDepth, operationDepth);
		}
		
		int additionalUndoStackStep = undoRedoEvent instanceof UndoEvent ? +1 : -1;
		throwExceptionIfInvalidStack(undoRedoEvent, undoDepth + additionalUndoStackStep, operationDepth);
	}
	
	private void throwExceptionIfInvalidStack(AbstractWhiteboardEvent undoRedoEvent, int undoDepth, int maxAllowedOperationDepth) throws WhiteboardOperationInconsistencyException {
		if(undoDepth > OPERATION_STACK_SIZE)
			throw new WhiteboardOperationInconsistencyException(undoRedoEvent, "Too many undo operations were executed: The maximum allowed operation stack size was exceeded.");
		if(undoDepth > maxAllowedOperationDepth)
			throw new WhiteboardOperationInconsistencyException(undoRedoEvent, "Too many undo operations were executed: There is nothing to undo.");
		if(undoDepth < 0)
			throw new WhiteboardOperationInconsistencyException(undoRedoEvent, "Too many redo operations were executed: There is nothing to redo.");
	}
	
	
	
	public class WhiteboardOperationInconsistencyException extends Exception {
		private static final long serialVersionUID = 4881835074668685527L;
		
		public WhiteboardOperationInconsistencyException(AbstractWhiteboardEvent event, String message) {
			super(event.getClass().getSimpleName().substring(0, event.getClass().getSimpleName().length()-4) + " could not be executed: " + message);
		}
	}

}

/**
 */
package dk.itu.cs.isa.model.software.structure.members.impl;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Field</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl#getTypeReferences <em>Type References</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl#getConstructorCalls <em>Constructor Calls</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl#getMethodCalls <em>Method Calls</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl#getFieldAccesses <em>Field Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl#getConstantAccesses <em>Constant Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl#getComponentAccesses <em>Component Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAFieldImpl#isArrayType <em>Array Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAFieldImpl extends ISAMemberImpl implements ISAField {
	/**
	 * The cached value of the '{@link #getTypeReferences() <em>Type References</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAType>> typeReferences;

	/**
	 * The cached value of the '{@link #getConstructorCalls() <em>Constructor Calls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstructorCalls()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAConstructor>> constructorCalls;

	/**
	 * The cached value of the '{@link #getMethodCalls() <em>Method Calls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethodCalls()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAMethod>> methodCalls;

	/**
	 * The cached value of the '{@link #getFieldAccesses() <em>Field Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFieldAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAField>> fieldAccesses;

	/**
	 * The cached value of the '{@link #getConstantAccesses() <em>Constant Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAEnumConstant>> constantAccesses;

	/**
	 * The cached value of the '{@link #getComponentAccesses() <em>Component Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISARecordComponent>> componentAccesses;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ISAType type;

	/**
	 * The default value of the '{@link #isArrayType() <em>Array Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArrayType()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ARRAY_TYPE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isArrayType() <em>Array Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArrayType()
	 * @generated
	 * @ordered
	 */
	protected boolean arrayType = ARRAY_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAFieldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MembersPackage.Literals.ISA_FIELD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAType>> getTypeReferences() {
		if (typeReferences == null) {
			typeReferences = new EObjectContainmentEList<ISAReference<ISAType>>(ISAReference.class, this, MembersPackage.ISA_FIELD__TYPE_REFERENCES);
		}
		return typeReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAConstructor>> getConstructorCalls() {
		if (constructorCalls == null) {
			constructorCalls = new EObjectContainmentEList<ISAReference<ISAConstructor>>(ISAReference.class, this, MembersPackage.ISA_FIELD__CONSTRUCTOR_CALLS);
		}
		return constructorCalls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAMethod>> getMethodCalls() {
		if (methodCalls == null) {
			methodCalls = new EObjectContainmentEList<ISAReference<ISAMethod>>(ISAReference.class, this, MembersPackage.ISA_FIELD__METHOD_CALLS);
		}
		return methodCalls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAField>> getFieldAccesses() {
		if (fieldAccesses == null) {
			fieldAccesses = new EObjectContainmentEList<ISAReference<ISAField>>(ISAReference.class, this, MembersPackage.ISA_FIELD__FIELD_ACCESSES);
		}
		return fieldAccesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAEnumConstant>> getConstantAccesses() {
		if (constantAccesses == null) {
			constantAccesses = new EObjectContainmentEList<ISAReference<ISAEnumConstant>>(ISAReference.class, this, MembersPackage.ISA_FIELD__CONSTANT_ACCESSES);
		}
		return constantAccesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISARecordComponent>> getComponentAccesses() {
		if (componentAccesses == null) {
			componentAccesses = new EObjectContainmentEList<ISAReference<ISARecordComponent>>(ISAReference.class, this, MembersPackage.ISA_FIELD__COMPONENT_ACCESSES);
		}
		return componentAccesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (ISAType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MembersPackage.ISA_FIELD__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetType(ISAType newType, NotificationChain msgs) {
		ISAType oldType = type;
		type = newType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_FIELD__TYPE, oldType, newType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(ISAType newType) {
		if (newType != type) {
			NotificationChain msgs = null;
			if (type != null)
				msgs = ((InternalEObject)type).eInverseRemove(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
			if (newType != null)
				msgs = ((InternalEObject)newType).eInverseAdd(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
			msgs = basicSetType(newType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_FIELD__TYPE, newType, newType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isArrayType() {
		return arrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArrayType(boolean newArrayType) {
		boolean oldArrayType = arrayType;
		arrayType = newArrayType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_FIELD__ARRAY_TYPE, oldArrayType, arrayType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MembersPackage.ISA_FIELD__TYPE:
				if (type != null)
					msgs = ((InternalEObject)type).eInverseRemove(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
				return basicSetType((ISAType)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MembersPackage.ISA_FIELD__TYPE_REFERENCES:
				return ((InternalEList<?>)getTypeReferences()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_FIELD__CONSTRUCTOR_CALLS:
				return ((InternalEList<?>)getConstructorCalls()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_FIELD__METHOD_CALLS:
				return ((InternalEList<?>)getMethodCalls()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_FIELD__FIELD_ACCESSES:
				return ((InternalEList<?>)getFieldAccesses()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_FIELD__CONSTANT_ACCESSES:
				return ((InternalEList<?>)getConstantAccesses()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_FIELD__COMPONENT_ACCESSES:
				return ((InternalEList<?>)getComponentAccesses()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_FIELD__TYPE:
				return basicSetType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MembersPackage.ISA_FIELD__TYPE_REFERENCES:
				return getTypeReferences();
			case MembersPackage.ISA_FIELD__CONSTRUCTOR_CALLS:
				return getConstructorCalls();
			case MembersPackage.ISA_FIELD__METHOD_CALLS:
				return getMethodCalls();
			case MembersPackage.ISA_FIELD__FIELD_ACCESSES:
				return getFieldAccesses();
			case MembersPackage.ISA_FIELD__CONSTANT_ACCESSES:
				return getConstantAccesses();
			case MembersPackage.ISA_FIELD__COMPONENT_ACCESSES:
				return getComponentAccesses();
			case MembersPackage.ISA_FIELD__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case MembersPackage.ISA_FIELD__ARRAY_TYPE:
				return isArrayType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MembersPackage.ISA_FIELD__TYPE_REFERENCES:
				getTypeReferences().clear();
				getTypeReferences().addAll((Collection<? extends ISAReference<ISAType>>)newValue);
				return;
			case MembersPackage.ISA_FIELD__CONSTRUCTOR_CALLS:
				getConstructorCalls().clear();
				getConstructorCalls().addAll((Collection<? extends ISAReference<ISAConstructor>>)newValue);
				return;
			case MembersPackage.ISA_FIELD__METHOD_CALLS:
				getMethodCalls().clear();
				getMethodCalls().addAll((Collection<? extends ISAReference<ISAMethod>>)newValue);
				return;
			case MembersPackage.ISA_FIELD__FIELD_ACCESSES:
				getFieldAccesses().clear();
				getFieldAccesses().addAll((Collection<? extends ISAReference<ISAField>>)newValue);
				return;
			case MembersPackage.ISA_FIELD__CONSTANT_ACCESSES:
				getConstantAccesses().clear();
				getConstantAccesses().addAll((Collection<? extends ISAReference<ISAEnumConstant>>)newValue);
				return;
			case MembersPackage.ISA_FIELD__COMPONENT_ACCESSES:
				getComponentAccesses().clear();
				getComponentAccesses().addAll((Collection<? extends ISAReference<ISARecordComponent>>)newValue);
				return;
			case MembersPackage.ISA_FIELD__TYPE:
				setType((ISAType)newValue);
				return;
			case MembersPackage.ISA_FIELD__ARRAY_TYPE:
				setArrayType((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_FIELD__TYPE_REFERENCES:
				getTypeReferences().clear();
				return;
			case MembersPackage.ISA_FIELD__CONSTRUCTOR_CALLS:
				getConstructorCalls().clear();
				return;
			case MembersPackage.ISA_FIELD__METHOD_CALLS:
				getMethodCalls().clear();
				return;
			case MembersPackage.ISA_FIELD__FIELD_ACCESSES:
				getFieldAccesses().clear();
				return;
			case MembersPackage.ISA_FIELD__CONSTANT_ACCESSES:
				getConstantAccesses().clear();
				return;
			case MembersPackage.ISA_FIELD__COMPONENT_ACCESSES:
				getComponentAccesses().clear();
				return;
			case MembersPackage.ISA_FIELD__TYPE:
				setType((ISAType)null);
				return;
			case MembersPackage.ISA_FIELD__ARRAY_TYPE:
				setArrayType(ARRAY_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_FIELD__TYPE_REFERENCES:
				return typeReferences != null && !typeReferences.isEmpty();
			case MembersPackage.ISA_FIELD__CONSTRUCTOR_CALLS:
				return constructorCalls != null && !constructorCalls.isEmpty();
			case MembersPackage.ISA_FIELD__METHOD_CALLS:
				return methodCalls != null && !methodCalls.isEmpty();
			case MembersPackage.ISA_FIELD__FIELD_ACCESSES:
				return fieldAccesses != null && !fieldAccesses.isEmpty();
			case MembersPackage.ISA_FIELD__CONSTANT_ACCESSES:
				return constantAccesses != null && !constantAccesses.isEmpty();
			case MembersPackage.ISA_FIELD__COMPONENT_ACCESSES:
				return componentAccesses != null && !componentAccesses.isEmpty();
			case MembersPackage.ISA_FIELD__TYPE:
				return type != null;
			case MembersPackage.ISA_FIELD__ARRAY_TYPE:
				return arrayType != ARRAY_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAReferencingElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_FIELD__TYPE_REFERENCES: return MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES;
				case MembersPackage.ISA_FIELD__CONSTRUCTOR_CALLS: return MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS;
				case MembersPackage.ISA_FIELD__METHOD_CALLS: return MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS;
				case MembersPackage.ISA_FIELD__FIELD_ACCESSES: return MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES;
				case MembersPackage.ISA_FIELD__CONSTANT_ACCESSES: return MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES;
				case MembersPackage.ISA_FIELD__COMPONENT_ACCESSES: return MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES;
				default: return -1;
			}
		}
		if (baseClass == ISATypedElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_FIELD__TYPE: return MiscPackage.ISA_TYPED_ELEMENT__TYPE;
				case MembersPackage.ISA_FIELD__ARRAY_TYPE: return MiscPackage.ISA_TYPED_ELEMENT__ARRAY_TYPE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAReferencingElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES: return MembersPackage.ISA_FIELD__TYPE_REFERENCES;
				case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS: return MembersPackage.ISA_FIELD__CONSTRUCTOR_CALLS;
				case MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS: return MembersPackage.ISA_FIELD__METHOD_CALLS;
				case MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES: return MembersPackage.ISA_FIELD__FIELD_ACCESSES;
				case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES: return MembersPackage.ISA_FIELD__CONSTANT_ACCESSES;
				case MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES: return MembersPackage.ISA_FIELD__COMPONENT_ACCESSES;
				default: return -1;
			}
		}
		if (baseClass == ISATypedElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_TYPED_ELEMENT__TYPE: return MembersPackage.ISA_FIELD__TYPE;
				case MiscPackage.ISA_TYPED_ELEMENT__ARRAY_TYPE: return MembersPackage.ISA_FIELD__ARRAY_TYPE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (arrayType: ");
		result.append(arrayType);
		result.append(')');
		return result.toString();
	}

} //ISAFieldImpl

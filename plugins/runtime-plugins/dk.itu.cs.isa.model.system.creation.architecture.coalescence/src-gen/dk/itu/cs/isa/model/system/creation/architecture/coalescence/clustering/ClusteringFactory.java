/**
 */
package dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringPackage
 * @generated
 */
public interface ClusteringFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClusteringFactory eINSTANCE = dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ClusteringFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ISA Simple Cluster</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Simple Cluster</em>'.
	 * @generated
	 */
	ISASimpleCluster createISASimpleCluster();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ClusteringPackage getClusteringPackage();

} //ClusteringFactory

package dk.itu.cs.isa.model.system.quality.sonarqube.ui;

import dk.itu.cs.isa.eclipse.ui.activation.GUIActivationListener;
import dk.itu.cs.isa.model.quality.sonarqube.processes.SonarQubeScanProcessHandler;
import dk.itu.cs.isa.model.quality.sonarqube.processes.SonarQubeServerProcessHandler;
import dk.itu.cs.isa.model.quality.sonarqube.processes.io.SonarProcessesOutputHandler;
import dk.itu.cs.isa.model.system.quality.sonarqube.ui.terminal.SonarScannerConsole;

public class BundleActivator implements GUIActivationListener {
	
	@Override
	public void start() {
		SonarProcessesOutputHandler.registerOutputHandler(
				SonarQubeScanProcessHandler.NAME,
				new SonarScannerConsole("Sonar Scanner Console"));
		
		SonarProcessesOutputHandler.registerOutputHandler(
				SonarQubeServerProcessHandler.NAME,
				new SonarScannerConsole("Sonar Server Console"));
	}

	@Override
	public void stop() {
		
	}
	
}

package dk.itu.cs.isa.eclipse.ui.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

public abstract class ISAHTMLView extends ViewPart {

	protected Browser browser;

	public ISAHTMLView() {
		super();
	}

	@Override
	public void createPartControl(Composite parent) {
		browser = new Browser(parent, SWT.EDGE);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		browser.setLayoutData(gridData);
		browser.setText(getHTMLCode());

		setFocus();
	}

	@Override
	public void setFocus() {
		browser.setFocus();
	}

	@Override
	public void dispose() {
		browser.dispose();
		super.dispose();
	}

	protected abstract String getHTMLCode();
	
}

package dk.itu.cs.isa.model.system.creation.srcml.preferences;

import dk.itu.cs.isa.preferences.ISAPreferences;

public class ISAPreferencesSrcML extends ISAPreferences {

	public static final String SRCML_EXECUTABLE_PATH = "SRCML_EXECUTABLE_PATH";

	@Override
    public void initializeDefaultPreferences() {
		ISAPreferencesSrcML.setString(ISAPreferencesSrcML.SRCML_EXECUTABLE_PATH, "");
	}

}

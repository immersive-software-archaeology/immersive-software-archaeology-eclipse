package dk.itu.cs.isa.eclipse.ui;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

public class ImageHelper {
	
	public static ImageDescriptor getImageDescriptor(String path) {
		return getImageDescriptor("dk.itu.cs.isa.eclipse.ui", path);
	}
	
	public static ImageDescriptor getImageDescriptor(String bundleName, String path) {
		try {
			if(!path.startsWith("platform:/"))
				return ImageDescriptor.createFromURL(new URL("platform:/plugin/" + bundleName + "/" + path));
			else
				return ImageDescriptor.createFromURL(new URL(path));
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Image getImage(String path) {
		return getImageDescriptor(path).createImage();
	}

}

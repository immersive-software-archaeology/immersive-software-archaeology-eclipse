package dk.itu.cs.isa.analyzer.exceptions;

public class SystemAnalysisException extends ISAException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5966047426563274848L;
	


	public SystemAnalysisException(String message) {
		super(message);
	}
	
}

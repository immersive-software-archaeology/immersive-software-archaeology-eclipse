/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo.impl;

import dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage;
import dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.HandInfo;
import dk.itu.cs.isa.multiplayer.clientinfo.HandPose;

import dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hand Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.HandInfoImpl#getIndexFingerTipPosition <em>Index Finger Tip Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.HandInfoImpl#getPose <em>Pose</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.HandInfoImpl#getHeldObject <em>Held Object</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HandInfoImpl extends BodyPartInfoImpl implements HandInfo {
	/**
	 * The cached value of the '{@link #getIndexFingerTipPosition() <em>Index Finger Tip Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexFingerTipPosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 indexFingerTipPosition;

	/**
	 * The default value of the '{@link #getPose() <em>Pose</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPose()
	 * @generated
	 * @ordered
	 */
	protected static final HandPose POSE_EDEFAULT = HandPose.DEFAULT;

	/**
	 * The cached value of the '{@link #getPose() <em>Pose</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPose()
	 * @generated
	 * @ordered
	 */
	protected HandPose pose = POSE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHeldObject() <em>Held Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeldObject()
	 * @generated
	 * @ordered
	 */
	protected HandHeldObjectInfo heldObject;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HandInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClientinfoPackage.Literals.HAND_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getIndexFingerTipPosition() {
		return indexFingerTipPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndexFingerTipPosition(ISAVector3 newIndexFingerTipPosition, NotificationChain msgs) {
		ISAVector3 oldIndexFingerTipPosition = indexFingerTipPosition;
		indexFingerTipPosition = newIndexFingerTipPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClientinfoPackage.HAND_INFO__INDEX_FINGER_TIP_POSITION, oldIndexFingerTipPosition, newIndexFingerTipPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexFingerTipPosition(ISAVector3 newIndexFingerTipPosition) {
		if (newIndexFingerTipPosition != indexFingerTipPosition) {
			NotificationChain msgs = null;
			if (indexFingerTipPosition != null)
				msgs = ((InternalEObject)indexFingerTipPosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.HAND_INFO__INDEX_FINGER_TIP_POSITION, null, msgs);
			if (newIndexFingerTipPosition != null)
				msgs = ((InternalEObject)newIndexFingerTipPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.HAND_INFO__INDEX_FINGER_TIP_POSITION, null, msgs);
			msgs = basicSetIndexFingerTipPosition(newIndexFingerTipPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.HAND_INFO__INDEX_FINGER_TIP_POSITION, newIndexFingerTipPosition, newIndexFingerTipPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HandPose getPose() {
		return pose;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPose(HandPose newPose) {
		HandPose oldPose = pose;
		pose = newPose == null ? POSE_EDEFAULT : newPose;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.HAND_INFO__POSE, oldPose, pose));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HandHeldObjectInfo getHeldObject() {
		return heldObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHeldObject(HandHeldObjectInfo newHeldObject, NotificationChain msgs) {
		HandHeldObjectInfo oldHeldObject = heldObject;
		heldObject = newHeldObject;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ClientinfoPackage.HAND_INFO__HELD_OBJECT, oldHeldObject, newHeldObject);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeldObject(HandHeldObjectInfo newHeldObject) {
		if (newHeldObject != heldObject) {
			NotificationChain msgs = null;
			if (heldObject != null)
				msgs = ((InternalEObject)heldObject).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.HAND_INFO__HELD_OBJECT, null, msgs);
			if (newHeldObject != null)
				msgs = ((InternalEObject)newHeldObject).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ClientinfoPackage.HAND_INFO__HELD_OBJECT, null, msgs);
			msgs = basicSetHeldObject(newHeldObject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ClientinfoPackage.HAND_INFO__HELD_OBJECT, newHeldObject, newHeldObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClientinfoPackage.HAND_INFO__INDEX_FINGER_TIP_POSITION:
				return basicSetIndexFingerTipPosition(null, msgs);
			case ClientinfoPackage.HAND_INFO__HELD_OBJECT:
				return basicSetHeldObject(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClientinfoPackage.HAND_INFO__INDEX_FINGER_TIP_POSITION:
				return getIndexFingerTipPosition();
			case ClientinfoPackage.HAND_INFO__POSE:
				return getPose();
			case ClientinfoPackage.HAND_INFO__HELD_OBJECT:
				return getHeldObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClientinfoPackage.HAND_INFO__INDEX_FINGER_TIP_POSITION:
				setIndexFingerTipPosition((ISAVector3)newValue);
				return;
			case ClientinfoPackage.HAND_INFO__POSE:
				setPose((HandPose)newValue);
				return;
			case ClientinfoPackage.HAND_INFO__HELD_OBJECT:
				setHeldObject((HandHeldObjectInfo)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClientinfoPackage.HAND_INFO__INDEX_FINGER_TIP_POSITION:
				setIndexFingerTipPosition((ISAVector3)null);
				return;
			case ClientinfoPackage.HAND_INFO__POSE:
				setPose(POSE_EDEFAULT);
				return;
			case ClientinfoPackage.HAND_INFO__HELD_OBJECT:
				setHeldObject((HandHeldObjectInfo)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClientinfoPackage.HAND_INFO__INDEX_FINGER_TIP_POSITION:
				return indexFingerTipPosition != null;
			case ClientinfoPackage.HAND_INFO__POSE:
				return pose != POSE_EDEFAULT;
			case ClientinfoPackage.HAND_INFO__HELD_OBJECT:
				return heldObject != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (pose: ");
		result.append(pose);
		result.append(')');
		return result.toString();
	}

} //HandInfoImpl

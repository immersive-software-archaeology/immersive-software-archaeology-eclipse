package dk.itu.cs.isa.analyzer.util;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;

public class ResourceDebugUtil {
	
	private static final String DEBUG_PROJECT_NAME = "ISADebugModels";
	
	

	public static ISASoftwareSystemModel loadStructureDebugResources(String name) {
		IProject debugProject = ResourcesPlugin.getWorkspace().getRoot().getProject(DEBUG_PROJECT_NAME);
		if(!debugProject.exists() || !debugProject.isOpen())
			return null;
		
		try {
			Resource debugResource = (new ResourceSetImpl()).createResource(URI.createPlatformResourceURI(debugProject.getName()+"/"+name+".structure", true));
			debugResource.load(Collections.EMPTY_MAP);
			for(EObject e : debugResource.getContents()) {
				if(e instanceof ISASoftwareSystemModel)
					return (ISASoftwareSystemModel) e;
			}
		} catch (IOException e) {
			// Do nothing
		}
		return null;
	}
	
	public static ISASoftwareSystemQualityModel loadQualityDebugResources(String name) {
		IProject debugProject = ResourcesPlugin.getWorkspace().getRoot().getProject(DEBUG_PROJECT_NAME);
		if(!debugProject.exists() || !debugProject.isOpen())
			return null;
		
		try {
			Resource debugResource = (new ResourceSetImpl()).createResource(URI.createPlatformResourceURI(debugProject.getName()+"/"+name+".quality", true));
			debugResource.load(Collections.EMPTY_MAP);
			for(EObject e : debugResource.getContents()) {
				if(e instanceof ISASoftwareSystemQualityModel)
					return (ISASoftwareSystemQualityModel) e;
			}
		} catch (IOException e) {
			// Do nothing
		}
		return null;
	}



	public static void storeStructureModelDebugResource(ISASoftwareSystemModel structureModel) throws IOException {
		if(structureModel != null)
			storeDebugResource(structureModel, structureModel.getName(), null, "structure");
	}
	
	public static void storeSystemQualityDebugResource(ISASoftwareSystemQualityModel qualityModel) throws IOException {
		if(qualityModel != null)
			storeDebugResource(qualityModel, qualityModel.getName(), null, "quality");
	}
	
	public static void storeVisualizationModelDebugResource(ISAVisualizationModel visModel) throws IOException {
		if(visModel != null)
			storeDebugResource(visModel, visModel.getName(), null, "visualization");
	}
	
	public static void storeDebugResource(EObject rootObject, String name, String suffix, String extension) throws IOException {
		IProject debugProject = ResourcesPlugin.getWorkspace().getRoot().getProject("ISADebugModels");
		if(!debugProject.exists() || !debugProject.isOpen())
			return;
		
		Resource debugResource = (new ResourceSetImpl()).createResource(URI.createPlatformResourceURI("ISADebugModels/"+name+(suffix==null?"":"_"+suffix)+"."+extension, true));
		debugResource.getContents().add(rootObject);
		debugResource.save(Collections.EMPTY_MAP);
	}
	
	

}

/**
 */
package dk.itu.cs.isa.multiplayer.util;

import dk.itu.cs.isa.multiplayer.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage
 * @generated
 */
public class MultiplayerAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MultiplayerPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplayerAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MultiplayerPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiplayerSwitch<Adapter> modelSwitch =
		new MultiplayerSwitch<Adapter>() {
			@Override
			public Adapter caseEventLog(EventLog object) {
				return createEventLogAdapter();
			}
			@Override
			public Adapter caseAbstractEvent(AbstractEvent object) {
				return createAbstractEventAdapter();
			}
			@Override
			public Adapter caseShowRelationEvent(ShowRelationEvent object) {
				return createShowRelationEventAdapter();
			}
			@Override
			public Adapter caseClearOneRelationEvent(ClearOneRelationEvent object) {
				return createClearOneRelationEventAdapter();
			}
			@Override
			public Adapter caseClearRelationsEvent(ClearRelationsEvent object) {
				return createClearRelationsEventAdapter();
			}
			@Override
			public Adapter caseClearAllRelationsEvent(ClearAllRelationsEvent object) {
				return createClearAllRelationsEventAdapter();
			}
			@Override
			public Adapter caseChangeElementActiveStateEvent(ChangeElementActiveStateEvent object) {
				return createChangeElementActiveStateEventAdapter();
			}
			@Override
			public Adapter caseSystemMoveEvent(SystemMoveEvent object) {
				return createSystemMoveEventAdapter();
			}
			@Override
			public Adapter caseChangeNameplateVisibilityEvent(ChangeNameplateVisibilityEvent object) {
				return createChangeNameplateVisibilityEventAdapter();
			}
			@Override
			public Adapter caseSphereOpenCloseEvent(SphereOpenCloseEvent object) {
				return createSphereOpenCloseEventAdapter();
			}
			@Override
			public Adapter caseAudioEvent(AudioEvent object) {
				return createAudioEventAdapter();
			}
			@Override
			public Adapter caseISAVector3(ISAVector3 object) {
				return createISAVector3Adapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.EventLog <em>Event Log</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.EventLog
	 * @generated
	 */
	public Adapter createEventLogAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.AbstractEvent <em>Abstract Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.AbstractEvent
	 * @generated
	 */
	public Adapter createAbstractEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent <em>Show Relation Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.ShowRelationEvent
	 * @generated
	 */
	public Adapter createShowRelationEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.ClearOneRelationEvent <em>Clear One Relation Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.ClearOneRelationEvent
	 * @generated
	 */
	public Adapter createClearOneRelationEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.ClearRelationsEvent <em>Clear Relations Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.ClearRelationsEvent
	 * @generated
	 */
	public Adapter createClearRelationsEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.ClearAllRelationsEvent <em>Clear All Relations Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.ClearAllRelationsEvent
	 * @generated
	 */
	public Adapter createClearAllRelationsEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent <em>Change Element Active State Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent
	 * @generated
	 */
	public Adapter createChangeElementActiveStateEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent <em>System Move Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.SystemMoveEvent
	 * @generated
	 */
	public Adapter createSystemMoveEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent <em>Change Nameplate Visibility Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent
	 * @generated
	 */
	public Adapter createChangeNameplateVisibilityEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent <em>Sphere Open Close Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent
	 * @generated
	 */
	public Adapter createSphereOpenCloseEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.AudioEvent <em>Audio Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.AudioEvent
	 * @generated
	 */
	public Adapter createAudioEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.multiplayer.ISAVector3 <em>ISA Vector3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.multiplayer.ISAVector3
	 * @generated
	 */
	public Adapter createISAVector3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //MultiplayerAdapterFactory

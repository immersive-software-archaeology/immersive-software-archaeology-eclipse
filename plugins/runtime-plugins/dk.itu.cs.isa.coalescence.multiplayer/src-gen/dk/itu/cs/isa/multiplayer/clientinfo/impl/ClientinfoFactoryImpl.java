/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo.impl;

import dk.itu.cs.isa.multiplayer.clientinfo.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClientinfoFactoryImpl extends EFactoryImpl implements ClientinfoFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ClientinfoFactory init() {
		try {
			ClientinfoFactory theClientinfoFactory = (ClientinfoFactory)EPackage.Registry.INSTANCE.getEFactory(ClientinfoPackage.eNS_URI);
			if (theClientinfoFactory != null) {
				return theClientinfoFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ClientinfoFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClientinfoFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ClientinfoPackage.CLIENT_INFO: return createClientInfo();
			case ClientinfoPackage.HEAD_INFO: return createHeadInfo();
			case ClientinfoPackage.HAND_INFO: return createHandInfo();
			case ClientinfoPackage.HAND_HELD_OBJECT_INFO: return createHandHeldObjectInfo();
			case ClientinfoPackage.ISA_VECTOR3: return createISAVector3();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ClientinfoPackage.HAND_POSE:
				return createHandPoseFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ClientinfoPackage.HAND_POSE:
				return convertHandPoseToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClientInfo createClientInfo() {
		ClientInfoImpl clientInfo = new ClientInfoImpl();
		return clientInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HeadInfo createHeadInfo() {
		HeadInfoImpl headInfo = new HeadInfoImpl();
		return headInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HandInfo createHandInfo() {
		HandInfoImpl handInfo = new HandInfoImpl();
		return handInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HandHeldObjectInfo createHandHeldObjectInfo() {
		HandHeldObjectInfoImpl handHeldObjectInfo = new HandHeldObjectInfoImpl();
		return handHeldObjectInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 createISAVector3() {
		ISAVector3Impl isaVector3 = new ISAVector3Impl();
		return isaVector3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HandPose createHandPoseFromString(EDataType eDataType, String initialValue) {
		HandPose result = HandPose.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertHandPoseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClientinfoPackage getClientinfoPackage() {
		return (ClientinfoPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ClientinfoPackage getPackage() {
		return ClientinfoPackage.eINSTANCE;
	}

} //ClientinfoFactoryImpl

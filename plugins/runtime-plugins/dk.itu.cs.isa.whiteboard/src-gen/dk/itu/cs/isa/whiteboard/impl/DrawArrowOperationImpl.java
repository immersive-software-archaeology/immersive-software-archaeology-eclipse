/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.DrawArrowOperation;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Draw Arrow Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.DrawArrowOperationImpl#isAutoDrawArrowHead <em>Auto Draw Arrow Head</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DrawArrowOperationImpl extends AbstractDrawOperationImpl implements DrawArrowOperation {
	/**
	 * The default value of the '{@link #isAutoDrawArrowHead() <em>Auto Draw Arrow Head</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAutoDrawArrowHead()
	 * @generated
	 * @ordered
	 */
	protected static final boolean AUTO_DRAW_ARROW_HEAD_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAutoDrawArrowHead() <em>Auto Draw Arrow Head</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAutoDrawArrowHead()
	 * @generated
	 * @ordered
	 */
	protected boolean autoDrawArrowHead = AUTO_DRAW_ARROW_HEAD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DrawArrowOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.DRAW_ARROW_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAutoDrawArrowHead() {
		return autoDrawArrowHead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAutoDrawArrowHead(boolean newAutoDrawArrowHead) {
		boolean oldAutoDrawArrowHead = autoDrawArrowHead;
		autoDrawArrowHead = newAutoDrawArrowHead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.DRAW_ARROW_OPERATION__AUTO_DRAW_ARROW_HEAD, oldAutoDrawArrowHead, autoDrawArrowHead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.DRAW_ARROW_OPERATION__AUTO_DRAW_ARROW_HEAD:
				return isAutoDrawArrowHead();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.DRAW_ARROW_OPERATION__AUTO_DRAW_ARROW_HEAD:
				setAutoDrawArrowHead((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.DRAW_ARROW_OPERATION__AUTO_DRAW_ARROW_HEAD:
				setAutoDrawArrowHead(AUTO_DRAW_ARROW_HEAD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.DRAW_ARROW_OPERATION__AUTO_DRAW_ARROW_HEAD:
				return autoDrawArrowHead != AUTO_DRAW_ARROW_HEAD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (autoDrawArrowHead: ");
		result.append(autoDrawArrowHead);
		result.append(')');
		return result.toString();
	}

} //DrawArrowOperationImpl

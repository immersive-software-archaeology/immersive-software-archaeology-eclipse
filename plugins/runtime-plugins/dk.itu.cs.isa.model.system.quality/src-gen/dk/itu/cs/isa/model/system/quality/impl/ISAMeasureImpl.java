/**
 */
package dk.itu.cs.isa.model.system.quality.impl;

import dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry;
import dk.itu.cs.isa.model.system.quality.ISAMeasure;
import dk.itu.cs.isa.model.system.quality.QualityPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Measure</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAMeasureImpl#getParentEntry <em>Parent Entry</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAMeasureImpl#getMetric <em>Metric</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAMeasureImpl#getValue <em>Value</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.impl.ISAMeasureImpl#isBestValue <em>Best Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAMeasureImpl extends MinimalEObjectImpl.Container implements ISAMeasure {
	/**
	 * The default value of the '{@link #getMetric() <em>Metric</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetric()
	 * @generated
	 * @ordered
	 */
	protected static final String METRIC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMetric() <em>Metric</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetric()
	 * @generated
	 * @ordered
	 */
	protected String metric = METRIC_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final float VALUE_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected float value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #isBestValue() <em>Best Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBestValue()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BEST_VALUE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBestValue() <em>Best Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBestValue()
	 * @generated
	 * @ordered
	 */
	protected boolean bestValue = BEST_VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAMeasureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return QualityPackage.Literals.ISA_MEASURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAClassifierQualityEntry getParentEntry() {
		if (eContainerFeatureID() != QualityPackage.ISA_MEASURE__PARENT_ENTRY) return null;
		return (ISAClassifierQualityEntry)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentEntry(ISAClassifierQualityEntry newParentEntry, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentEntry, QualityPackage.ISA_MEASURE__PARENT_ENTRY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentEntry(ISAClassifierQualityEntry newParentEntry) {
		if (newParentEntry != eInternalContainer() || (eContainerFeatureID() != QualityPackage.ISA_MEASURE__PARENT_ENTRY && newParentEntry != null)) {
			if (EcoreUtil.isAncestor(this, newParentEntry))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentEntry != null)
				msgs = ((InternalEObject)newParentEntry).eInverseAdd(this, QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES, ISAClassifierQualityEntry.class, msgs);
			msgs = basicSetParentEntry(newParentEntry, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_MEASURE__PARENT_ENTRY, newParentEntry, newParentEntry));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMetric() {
		return metric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMetric(String newMetric) {
		String oldMetric = metric;
		metric = newMetric;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_MEASURE__METRIC, oldMetric, metric));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public float getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(float newValue) {
		float oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_MEASURE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isBestValue() {
		return bestValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBestValue(boolean newBestValue) {
		boolean oldBestValue = bestValue;
		bestValue = newBestValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, QualityPackage.ISA_MEASURE__BEST_VALUE, oldBestValue, bestValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QualityPackage.ISA_MEASURE__PARENT_ENTRY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentEntry((ISAClassifierQualityEntry)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case QualityPackage.ISA_MEASURE__PARENT_ENTRY:
				return basicSetParentEntry(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case QualityPackage.ISA_MEASURE__PARENT_ENTRY:
				return eInternalContainer().eInverseRemove(this, QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY__MEASURES, ISAClassifierQualityEntry.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case QualityPackage.ISA_MEASURE__PARENT_ENTRY:
				return getParentEntry();
			case QualityPackage.ISA_MEASURE__METRIC:
				return getMetric();
			case QualityPackage.ISA_MEASURE__VALUE:
				return getValue();
			case QualityPackage.ISA_MEASURE__BEST_VALUE:
				return isBestValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case QualityPackage.ISA_MEASURE__PARENT_ENTRY:
				setParentEntry((ISAClassifierQualityEntry)newValue);
				return;
			case QualityPackage.ISA_MEASURE__METRIC:
				setMetric((String)newValue);
				return;
			case QualityPackage.ISA_MEASURE__VALUE:
				setValue((Float)newValue);
				return;
			case QualityPackage.ISA_MEASURE__BEST_VALUE:
				setBestValue((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_MEASURE__PARENT_ENTRY:
				setParentEntry((ISAClassifierQualityEntry)null);
				return;
			case QualityPackage.ISA_MEASURE__METRIC:
				setMetric(METRIC_EDEFAULT);
				return;
			case QualityPackage.ISA_MEASURE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case QualityPackage.ISA_MEASURE__BEST_VALUE:
				setBestValue(BEST_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case QualityPackage.ISA_MEASURE__PARENT_ENTRY:
				return getParentEntry() != null;
			case QualityPackage.ISA_MEASURE__METRIC:
				return METRIC_EDEFAULT == null ? metric != null : !METRIC_EDEFAULT.equals(metric);
			case QualityPackage.ISA_MEASURE__VALUE:
				return value != VALUE_EDEFAULT;
			case QualityPackage.ISA_MEASURE__BEST_VALUE:
				return bestValue != BEST_VALUE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (metric: ");
		result.append(metric);
		result.append(", value: ");
		result.append(value);
		result.append(", bestValue: ");
		result.append(bestValue);
		result.append(')');
		return result.toString();
	}

} //ISAMeasureImpl

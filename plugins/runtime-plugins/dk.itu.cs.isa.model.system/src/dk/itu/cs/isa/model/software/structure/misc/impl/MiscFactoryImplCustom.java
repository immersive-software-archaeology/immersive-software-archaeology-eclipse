package dk.itu.cs.isa.model.software.structure.misc.impl;

import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;

public class MiscFactoryImplCustom extends MiscFactoryImpl {

	@Override
	public <T extends ISAReferenceableElement> ISAReference<T> createISAReference() {
		ISAReferenceImpl<T> isaReference = new ISAReferenceImplCustom<T>();
		return isaReference;
	}
	
}

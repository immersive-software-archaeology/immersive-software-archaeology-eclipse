/**
 */
package dk.itu.cs.isa.model.system.quality;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Measure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getParentEntry <em>Parent Entry</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getMetric <em>Metric</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getValue <em>Value</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#isBestValue <em>Best Value</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAMeasure()
 * @model
 * @generated
 */
public interface ISAMeasure extends EObject {
	/**
	 * Returns the value of the '<em><b>Parent Entry</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getMeasures <em>Measures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Entry</em>' container reference.
	 * @see #setParentEntry(ISAClassifierQualityEntry)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAMeasure_ParentEntry()
	 * @see dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getMeasures
	 * @model opposite="measures" required="true" transient="false"
	 * @generated
	 */
	ISAClassifierQualityEntry getParentEntry();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getParentEntry <em>Parent Entry</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Entry</em>' container reference.
	 * @see #getParentEntry()
	 * @generated
	 */
	void setParentEntry(ISAClassifierQualityEntry value);

	/**
	 * Returns the value of the '<em><b>Metric</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric</em>' attribute.
	 * @see #setMetric(String)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAMeasure_Metric()
	 * @model required="true"
	 * @generated
	 */
	String getMetric();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getMetric <em>Metric</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric</em>' attribute.
	 * @see #getMetric()
	 * @generated
	 */
	void setMetric(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(float)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAMeasure_Value()
	 * @model required="true"
	 * @generated
	 */
	float getValue();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(float value);

	/**
	 * Returns the value of the '<em><b>Best Value</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Best Value</em>' attribute.
	 * @see #setBestValue(boolean)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAMeasure_BestValue()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isBestValue();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAMeasure#isBestValue <em>Best Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Best Value</em>' attribute.
	 * @see #isBestValue()
	 * @generated
	 */
	void setBestValue(boolean value);

} // ISAMeasure

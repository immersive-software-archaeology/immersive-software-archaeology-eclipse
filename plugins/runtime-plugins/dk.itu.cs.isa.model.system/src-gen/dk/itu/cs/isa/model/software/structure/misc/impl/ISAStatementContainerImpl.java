/**
 */
package dk.itu.cs.isa.model.software.structure.misc.impl;

import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Statement Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAStatementContainerImpl#getNumberOfStatements <em>Number Of Statements</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAStatementContainerImpl#getNumberOfExpressions <em>Number Of Expressions</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAStatementContainerImpl#getNumberOfControlFlowSplits <em>Number Of Control Flow Splits</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAStatementContainerImpl#getCognitiveComplexity <em>Cognitive Complexity</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ISAStatementContainerImpl extends ISAReferencingElementImpl implements ISAStatementContainer {
	/**
	 * The default value of the '{@link #getNumberOfStatements() <em>Number Of Statements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfStatements()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_STATEMENTS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfStatements() <em>Number Of Statements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfStatements()
	 * @generated
	 * @ordered
	 */
	protected int numberOfStatements = NUMBER_OF_STATEMENTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberOfExpressions() <em>Number Of Expressions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfExpressions()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_EXPRESSIONS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfExpressions() <em>Number Of Expressions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfExpressions()
	 * @generated
	 * @ordered
	 */
	protected int numberOfExpressions = NUMBER_OF_EXPRESSIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberOfControlFlowSplits() <em>Number Of Control Flow Splits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfControlFlowSplits()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfControlFlowSplits() <em>Number Of Control Flow Splits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfControlFlowSplits()
	 * @generated
	 * @ordered
	 */
	protected int numberOfControlFlowSplits = NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCognitiveComplexity() <em>Cognitive Complexity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCognitiveComplexity()
	 * @generated
	 * @ordered
	 */
	protected static final int COGNITIVE_COMPLEXITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCognitiveComplexity() <em>Cognitive Complexity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCognitiveComplexity()
	 * @generated
	 * @ordered
	 */
	protected int cognitiveComplexity = COGNITIVE_COMPLEXITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAStatementContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiscPackage.Literals.ISA_STATEMENT_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfStatements() {
		return numberOfStatements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfStatements(int newNumberOfStatements) {
		int oldNumberOfStatements = numberOfStatements;
		numberOfStatements = newNumberOfStatements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS, oldNumberOfStatements, numberOfStatements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfExpressions() {
		return numberOfExpressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfExpressions(int newNumberOfExpressions) {
		int oldNumberOfExpressions = numberOfExpressions;
		numberOfExpressions = newNumberOfExpressions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS, oldNumberOfExpressions, numberOfExpressions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfControlFlowSplits() {
		return numberOfControlFlowSplits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfControlFlowSplits(int newNumberOfControlFlowSplits) {
		int oldNumberOfControlFlowSplits = numberOfControlFlowSplits;
		numberOfControlFlowSplits = newNumberOfControlFlowSplits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS, oldNumberOfControlFlowSplits, numberOfControlFlowSplits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getCognitiveComplexity() {
		return cognitiveComplexity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCognitiveComplexity(int newCognitiveComplexity) {
		int oldCognitiveComplexity = cognitiveComplexity;
		cognitiveComplexity = newCognitiveComplexity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY, oldCognitiveComplexity, cognitiveComplexity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS:
				return getNumberOfStatements();
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS:
				return getNumberOfExpressions();
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS:
				return getNumberOfControlFlowSplits();
			case MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY:
				return getCognitiveComplexity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS:
				setNumberOfStatements((Integer)newValue);
				return;
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS:
				setNumberOfExpressions((Integer)newValue);
				return;
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS:
				setNumberOfControlFlowSplits((Integer)newValue);
				return;
			case MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY:
				setCognitiveComplexity((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS:
				setNumberOfStatements(NUMBER_OF_STATEMENTS_EDEFAULT);
				return;
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS:
				setNumberOfExpressions(NUMBER_OF_EXPRESSIONS_EDEFAULT);
				return;
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS:
				setNumberOfControlFlowSplits(NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT);
				return;
			case MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY:
				setCognitiveComplexity(COGNITIVE_COMPLEXITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS:
				return numberOfStatements != NUMBER_OF_STATEMENTS_EDEFAULT;
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS:
				return numberOfExpressions != NUMBER_OF_EXPRESSIONS_EDEFAULT;
			case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS:
				return numberOfControlFlowSplits != NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT;
			case MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY:
				return cognitiveComplexity != COGNITIVE_COMPLEXITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (numberOfStatements: ");
		result.append(numberOfStatements);
		result.append(", numberOfExpressions: ");
		result.append(numberOfExpressions);
		result.append(", numberOfControlFlowSplits: ");
		result.append(numberOfControlFlowSplits);
		result.append(", cognitiveComplexity: ");
		result.append(cognitiveComplexity);
		result.append(')');
		return result.toString();
	}

} //ISAStatementContainerImpl

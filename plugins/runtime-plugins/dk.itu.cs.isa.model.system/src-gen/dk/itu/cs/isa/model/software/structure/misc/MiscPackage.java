/**
 */
package dk.itu.cs.isa.model.software.structure.misc;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscFactory
 * @model kind="package"
 * @generated
 */
public interface MiscPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "misc";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/software/structure/misc";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "misc";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MiscPackage eINSTANCE = dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISANamedSoftwareElementImpl <em>ISA Named Software Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISANamedSoftwareElementImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISANamedSoftwareElement()
	 * @generated
	 */
	int ISA_NAMED_SOFTWARE_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_NAMED_SOFTWARE_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>ISA Named Software Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME = 0;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING = 1;

	/**
	 * The number of operations of the '<em>ISA Named Software Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISATypedElementImpl <em>ISA Typed Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISATypedElementImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISATypedElement()
	 * @generated
	 */
	int ISA_TYPED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPED_ELEMENT__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Array Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPED_ELEMENT__ARRAY_TYPE = 1;

	/**
	 * The number of structural features of the '<em>ISA Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPED_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>ISA Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceableElementImpl <em>ISA Referenceable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceableElementImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAReferenceableElement()
	 * @generated
	 */
	int ISA_REFERENCEABLE_ELEMENT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCEABLE_ELEMENT__NAME = ISA_NAMED_SOFTWARE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES = ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>ISA Referenceable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCEABLE_ELEMENT_FEATURE_COUNT = ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCEABLE_ELEMENT___GET_FULLY_QUALIFIED_NAME = ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCEABLE_ELEMENT___TO_STRING = ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Referenceable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCEABLE_ELEMENT_OPERATION_COUNT = ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISATypeImpl <em>ISA Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISATypeImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAType()
	 * @generated
	 */
	int ISA_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPE__NAME = ISA_REFERENCEABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Incoming References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPE__INCOMING_REFERENCES = ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES;

	/**
	 * The feature id for the '<em><b>Usages As Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPE__USAGES_AS_TYPE = ISA_REFERENCEABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>ISA Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPE_FEATURE_COUNT = ISA_REFERENCEABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPE___GET_FULLY_QUALIFIED_NAME = ISA_REFERENCEABLE_ELEMENT___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPE___TO_STRING = ISA_REFERENCEABLE_ELEMENT___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TYPE_OPERATION_COUNT = ISA_REFERENCEABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAModifiableElementImpl <em>ISA Modifiable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAModifiableElementImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAModifiableElement()
	 * @generated
	 */
	int ISA_MODIFIABLE_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MODIFIABLE_ELEMENT__MODIFIERS = 0;

	/**
	 * The number of structural features of the '<em>ISA Modifiable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MODIFIABLE_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>ISA Modifiable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_MODIFIABLE_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl <em>ISA Referencing Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAReferencingElement()
	 * @generated
	 */
	int ISA_REFERENCING_ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>Type References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCING_ELEMENT__TYPE_REFERENCES = 0;

	/**
	 * The feature id for the '<em><b>Constructor Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS = 1;

	/**
	 * The feature id for the '<em><b>Method Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCING_ELEMENT__METHOD_CALLS = 2;

	/**
	 * The feature id for the '<em><b>Field Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCING_ELEMENT__FIELD_ACCESSES = 3;

	/**
	 * The feature id for the '<em><b>Constant Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES = 4;

	/**
	 * The feature id for the '<em><b>Component Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES = 5;

	/**
	 * The number of structural features of the '<em>ISA Referencing Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCING_ELEMENT_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>ISA Referencing Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCING_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceImpl <em>ISA Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAReference()
	 * @generated
	 */
	int ISA_REFERENCE = 5;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCE__WEIGHT = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCE__TARGET = 1;

	/**
	 * The number of structural features of the '<em>ISA Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCE_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCE___TO_STRING = 0;

	/**
	 * The number of operations of the '<em>ISA Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_REFERENCE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAStatementContainerImpl <em>ISA Statement Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAStatementContainerImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAStatementContainer()
	 * @generated
	 */
	int ISA_STATEMENT_CONTAINER = 7;

	/**
	 * The feature id for the '<em><b>Type References</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__TYPE_REFERENCES = ISA_REFERENCING_ELEMENT__TYPE_REFERENCES;

	/**
	 * The feature id for the '<em><b>Constructor Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__CONSTRUCTOR_CALLS = ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS;

	/**
	 * The feature id for the '<em><b>Method Calls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__METHOD_CALLS = ISA_REFERENCING_ELEMENT__METHOD_CALLS;

	/**
	 * The feature id for the '<em><b>Field Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__FIELD_ACCESSES = ISA_REFERENCING_ELEMENT__FIELD_ACCESSES;

	/**
	 * The feature id for the '<em><b>Constant Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__CONSTANT_ACCESSES = ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES;

	/**
	 * The feature id for the '<em><b>Component Accesses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__COMPONENT_ACCESSES = ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES;

	/**
	 * The feature id for the '<em><b>Number Of Statements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS = ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number Of Expressions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS = ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Number Of Control Flow Splits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS = ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cognitive Complexity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY = ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>ISA Statement Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER_FEATURE_COUNT = ISA_REFERENCING_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>ISA Statement Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_STATEMENT_CONTAINER_OPERATION_COUNT = ISA_REFERENCING_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAParametrizableElementImpl <em>ISA Parametrizable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAParametrizableElementImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAParametrizableElement()
	 * @generated
	 */
	int ISA_PARAMETRIZABLE_ELEMENT = 8;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS = 0;

	/**
	 * The number of structural features of the '<em>ISA Parametrizable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETRIZABLE_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>ISA Parametrizable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETRIZABLE_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAParameterImpl <em>ISA Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAParameterImpl
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAParameter()
	 * @generated
	 */
	int ISA_PARAMETER = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETER__NAME = ISA_NAMED_SOFTWARE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETER__TYPE = ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Array Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETER__ARRAY_TYPE = ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Var Arg</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETER__VAR_ARG = ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ISA Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETER_FEATURE_COUNT = ISA_NAMED_SOFTWARE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Fully Qualified Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETER___GET_FULLY_QUALIFIED_NAME = ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETER___TO_STRING = ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING;

	/**
	 * The number of operations of the '<em>ISA Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_PARAMETER_OPERATION_COUNT = ISA_NAMED_SOFTWARE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAModifier <em>ISA Modifier</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAModifier
	 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAModifier()
	 * @generated
	 */
	int ISA_MODIFIER = 10;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement <em>ISA Named Software Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Named Software Element</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement
	 * @generated
	 */
	EClass getISANamedSoftwareElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement#getName()
	 * @see #getISANamedSoftwareElement()
	 * @generated
	 */
	EAttribute getISANamedSoftwareElement_Name();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement#getFullyQualifiedName() <em>Get Fully Qualified Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Fully Qualified Name</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement#getFullyQualifiedName()
	 * @generated
	 */
	EOperation getISANamedSoftwareElement__GetFullyQualifiedName();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement#toString() <em>To String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>To String</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement#toString()
	 * @generated
	 */
	EOperation getISANamedSoftwareElement__ToString();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement <em>ISA Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Typed Element</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISATypedElement
	 * @generated
	 */
	EClass getISATypedElement();

	/**
	 * Returns the meta object for the reference '{@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#getType()
	 * @see #getISATypedElement()
	 * @generated
	 */
	EReference getISATypedElement_Type();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#isArrayType <em>Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Array Type</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#isArrayType()
	 * @see #getISATypedElement()
	 * @generated
	 */
	EAttribute getISATypedElement_ArrayType();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAType <em>ISA Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Type</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAType
	 * @generated
	 */
	EClass getISAType();

	/**
	 * Returns the meta object for the reference list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAType#getUsagesAsType <em>Usages As Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Usages As Type</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAType#getUsagesAsType()
	 * @see #getISAType()
	 * @generated
	 */
	EReference getISAType_UsagesAsType();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement <em>ISA Modifiable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Modifiable Element</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement
	 * @generated
	 */
	EClass getISAModifiableElement();

	/**
	 * Returns the meta object for the attribute list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement#getModifiers <em>Modifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Modifiers</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement#getModifiers()
	 * @see #getISAModifiableElement()
	 * @generated
	 */
	EAttribute getISAModifiableElement_Modifiers();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement <em>ISA Referencing Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Referencing Element</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement
	 * @generated
	 */
	EClass getISAReferencingElement();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getTypeReferences <em>Type References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Type References</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getTypeReferences()
	 * @see #getISAReferencingElement()
	 * @generated
	 */
	EReference getISAReferencingElement_TypeReferences();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getConstructorCalls <em>Constructor Calls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constructor Calls</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getConstructorCalls()
	 * @see #getISAReferencingElement()
	 * @generated
	 */
	EReference getISAReferencingElement_ConstructorCalls();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getMethodCalls <em>Method Calls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Method Calls</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getMethodCalls()
	 * @see #getISAReferencingElement()
	 * @generated
	 */
	EReference getISAReferencingElement_MethodCalls();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getFieldAccesses <em>Field Accesses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Field Accesses</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getFieldAccesses()
	 * @see #getISAReferencingElement()
	 * @generated
	 */
	EReference getISAReferencingElement_FieldAccesses();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getConstantAccesses <em>Constant Accesses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constant Accesses</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getConstantAccesses()
	 * @see #getISAReferencingElement()
	 * @generated
	 */
	EReference getISAReferencingElement_ConstantAccesses();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getComponentAccesses <em>Component Accesses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Component Accesses</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getComponentAccesses()
	 * @see #getISAReferencingElement()
	 * @generated
	 */
	EReference getISAReferencingElement_ComponentAccesses();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReference <em>ISA Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Reference</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReference
	 * @generated
	 */
	EClass getISAReference();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReference#getWeight <em>Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Weight</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReference#getWeight()
	 * @see #getISAReference()
	 * @generated
	 */
	EAttribute getISAReference_Weight();

	/**
	 * Returns the meta object for the reference '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReference#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReference#getTarget()
	 * @see #getISAReference()
	 * @generated
	 */
	EReference getISAReference_Target();

	/**
	 * Returns the meta object for the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReference#toString() <em>To String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>To String</em>' operation.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReference#toString()
	 * @generated
	 */
	EOperation getISAReference__ToString();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement <em>ISA Referenceable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Referenceable Element</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement
	 * @generated
	 */
	EClass getISAReferenceableElement();

	/**
	 * Returns the meta object for the reference list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement#getIncomingReferences <em>Incoming References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming References</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement#getIncomingReferences()
	 * @see #getISAReferenceableElement()
	 * @generated
	 */
	EReference getISAReferenceableElement_IncomingReferences();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer <em>ISA Statement Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Statement Container</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer
	 * @generated
	 */
	EClass getISAStatementContainer();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfStatements <em>Number Of Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Statements</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfStatements()
	 * @see #getISAStatementContainer()
	 * @generated
	 */
	EAttribute getISAStatementContainer_NumberOfStatements();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfExpressions <em>Number Of Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Expressions</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfExpressions()
	 * @see #getISAStatementContainer()
	 * @generated
	 */
	EAttribute getISAStatementContainer_NumberOfExpressions();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfControlFlowSplits <em>Number Of Control Flow Splits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Control Flow Splits</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getNumberOfControlFlowSplits()
	 * @see #getISAStatementContainer()
	 * @generated
	 */
	EAttribute getISAStatementContainer_NumberOfControlFlowSplits();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getCognitiveComplexity <em>Cognitive Complexity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cognitive Complexity</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer#getCognitiveComplexity()
	 * @see #getISAStatementContainer()
	 * @generated
	 */
	EAttribute getISAStatementContainer_CognitiveComplexity();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement <em>ISA Parametrizable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Parametrizable Element</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement
	 * @generated
	 */
	EClass getISAParametrizableElement();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement#getParameters()
	 * @see #getISAParametrizableElement()
	 * @generated
	 */
	EReference getISAParametrizableElement_Parameters();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAParameter <em>ISA Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Parameter</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAParameter
	 * @generated
	 */
	EClass getISAParameter();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.software.structure.misc.ISAParameter#isVarArg <em>Var Arg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Arg</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAParameter#isVarArg()
	 * @see #getISAParameter()
	 * @generated
	 */
	EAttribute getISAParameter_VarArg();

	/**
	 * Returns the meta object for enum '{@link dk.itu.cs.isa.model.software.structure.misc.ISAModifier <em>ISA Modifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ISA Modifier</em>'.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAModifier
	 * @generated
	 */
	EEnum getISAModifier();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MiscFactory getMiscFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISANamedSoftwareElementImpl <em>ISA Named Software Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISANamedSoftwareElementImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISANamedSoftwareElement()
		 * @generated
		 */
		EClass ISA_NAMED_SOFTWARE_ELEMENT = eINSTANCE.getISANamedSoftwareElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_NAMED_SOFTWARE_ELEMENT__NAME = eINSTANCE.getISANamedSoftwareElement_Name();

		/**
		 * The meta object literal for the '<em><b>Get Fully Qualified Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME = eINSTANCE.getISANamedSoftwareElement__GetFullyQualifiedName();

		/**
		 * The meta object literal for the '<em><b>To String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING = eINSTANCE.getISANamedSoftwareElement__ToString();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISATypedElementImpl <em>ISA Typed Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISATypedElementImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISATypedElement()
		 * @generated
		 */
		EClass ISA_TYPED_ELEMENT = eINSTANCE.getISATypedElement();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_TYPED_ELEMENT__TYPE = eINSTANCE.getISATypedElement_Type();

		/**
		 * The meta object literal for the '<em><b>Array Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_TYPED_ELEMENT__ARRAY_TYPE = eINSTANCE.getISATypedElement_ArrayType();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISATypeImpl <em>ISA Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISATypeImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAType()
		 * @generated
		 */
		EClass ISA_TYPE = eINSTANCE.getISAType();

		/**
		 * The meta object literal for the '<em><b>Usages As Type</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_TYPE__USAGES_AS_TYPE = eINSTANCE.getISAType_UsagesAsType();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAModifiableElementImpl <em>ISA Modifiable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAModifiableElementImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAModifiableElement()
		 * @generated
		 */
		EClass ISA_MODIFIABLE_ELEMENT = eINSTANCE.getISAModifiableElement();

		/**
		 * The meta object literal for the '<em><b>Modifiers</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_MODIFIABLE_ELEMENT__MODIFIERS = eINSTANCE.getISAModifiableElement_Modifiers();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl <em>ISA Referencing Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAReferencingElement()
		 * @generated
		 */
		EClass ISA_REFERENCING_ELEMENT = eINSTANCE.getISAReferencingElement();

		/**
		 * The meta object literal for the '<em><b>Type References</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_REFERENCING_ELEMENT__TYPE_REFERENCES = eINSTANCE.getISAReferencingElement_TypeReferences();

		/**
		 * The meta object literal for the '<em><b>Constructor Calls</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS = eINSTANCE.getISAReferencingElement_ConstructorCalls();

		/**
		 * The meta object literal for the '<em><b>Method Calls</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_REFERENCING_ELEMENT__METHOD_CALLS = eINSTANCE.getISAReferencingElement_MethodCalls();

		/**
		 * The meta object literal for the '<em><b>Field Accesses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_REFERENCING_ELEMENT__FIELD_ACCESSES = eINSTANCE.getISAReferencingElement_FieldAccesses();

		/**
		 * The meta object literal for the '<em><b>Constant Accesses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES = eINSTANCE.getISAReferencingElement_ConstantAccesses();

		/**
		 * The meta object literal for the '<em><b>Component Accesses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES = eINSTANCE.getISAReferencingElement_ComponentAccesses();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceImpl <em>ISA Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAReference()
		 * @generated
		 */
		EClass ISA_REFERENCE = eINSTANCE.getISAReference();

		/**
		 * The meta object literal for the '<em><b>Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_REFERENCE__WEIGHT = eINSTANCE.getISAReference_Weight();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_REFERENCE__TARGET = eINSTANCE.getISAReference_Target();

		/**
		 * The meta object literal for the '<em><b>To String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ISA_REFERENCE___TO_STRING = eINSTANCE.getISAReference__ToString();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceableElementImpl <em>ISA Referenceable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferenceableElementImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAReferenceableElement()
		 * @generated
		 */
		EClass ISA_REFERENCEABLE_ELEMENT = eINSTANCE.getISAReferenceableElement();

		/**
		 * The meta object literal for the '<em><b>Incoming References</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES = eINSTANCE.getISAReferenceableElement_IncomingReferences();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAStatementContainerImpl <em>ISA Statement Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAStatementContainerImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAStatementContainer()
		 * @generated
		 */
		EClass ISA_STATEMENT_CONTAINER = eINSTANCE.getISAStatementContainer();

		/**
		 * The meta object literal for the '<em><b>Number Of Statements</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS = eINSTANCE.getISAStatementContainer_NumberOfStatements();

		/**
		 * The meta object literal for the '<em><b>Number Of Expressions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS = eINSTANCE.getISAStatementContainer_NumberOfExpressions();

		/**
		 * The meta object literal for the '<em><b>Number Of Control Flow Splits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS = eINSTANCE.getISAStatementContainer_NumberOfControlFlowSplits();

		/**
		 * The meta object literal for the '<em><b>Cognitive Complexity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY = eINSTANCE.getISAStatementContainer_CognitiveComplexity();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAParametrizableElementImpl <em>ISA Parametrizable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAParametrizableElementImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAParametrizableElement()
		 * @generated
		 */
		EClass ISA_PARAMETRIZABLE_ELEMENT = eINSTANCE.getISAParametrizableElement();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS = eINSTANCE.getISAParametrizableElement_Parameters();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAParameterImpl <em>ISA Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.ISAParameterImpl
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAParameter()
		 * @generated
		 */
		EClass ISA_PARAMETER = eINSTANCE.getISAParameter();

		/**
		 * The meta object literal for the '<em><b>Var Arg</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_PARAMETER__VAR_ARG = eINSTANCE.getISAParameter_VarArg();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.software.structure.misc.ISAModifier <em>ISA Modifier</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.software.structure.misc.ISAModifier
		 * @see dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl#getISAModifier()
		 * @generated
		 */
		EEnum ISA_MODIFIER = eINSTANCE.getISAModifier();

	}

} //MiscPackage

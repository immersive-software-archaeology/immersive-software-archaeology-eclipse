package dk.itu.cs.isa.model.software.structure.libraries.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

public class ISALibraryImplCustom extends ISALibraryImpl {

	@Override
	public ISANamedSoftwareElement getElement(String qualifiedElementName) {
		if(qualifiedElementName.contains(";")) {
			// Find a member
			String qualifiedClassName = qualifiedElementName.split(";")[0];
			ISAClassifier containingClassifier = findClassifier(qualifiedClassName);
			if(containingClassifier == null)
				return null;
			
			String memberSignature = qualifiedElementName.split(";")[1];
			return findMemberInClassifier(memberSignature, containingClassifier);
		}
		else {
			// Find a classifier
			return findClassifier(qualifiedElementName);
		}
	}

	private ISAClassifier findClassifier(String qualifiedName) {
		for(ISAClassifier classifier : this.getClassifiers()) {
			if(classifier.getFullyQualifiedName().equals(qualifiedName))
				return classifier;
		}
		
		return null;
	}
	
	private ISAMember findMemberInClassifier(String memberSignature, ISAClassifier containingClassifier) {
		if(memberSignature.contains("(")) {
			// Search for a method or constructor
			
			for(ISAMethod method : containingClassifier.getMethods()) {
				if(method.getName().equals(memberSignature)) {
					return method;
				}
			}
			
			if(containingClassifier instanceof ISAClass) {
				for(ISAConstructor constructor : ((ISAClass) containingClassifier).getConstructors()) {
					if(constructor.getName().equals(memberSignature)) {
						return constructor;
					}
				}
			}
		}
		else {
			// Search for a field or constant
			
			for(ISAField field : containingClassifier.getFields()) {
				if(field.getName().equals(memberSignature)) {
					return field;
				}
			}
			
			if(containingClassifier instanceof ISAEnum) {
				for(ISAEnumConstant constant : ((ISAEnum) containingClassifier).getConstants()) {
					if(constant.getName().equals(memberSignature)) {
						return constant;
					}
				}
			}
		}

		return null;
	}
	
}

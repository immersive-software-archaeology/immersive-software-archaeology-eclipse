package dk.itu.cs.isa.events;

import dk.itu.cs.isa.events.events.Event;

public interface ISAEventListener {

	public void notify(Event event);
	
}

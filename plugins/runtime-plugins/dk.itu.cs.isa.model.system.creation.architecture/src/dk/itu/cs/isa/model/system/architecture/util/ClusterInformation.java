package dk.itu.cs.isa.model.system.architecture.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.util.ISASystemModelHelper;

public class ClusterInformation {

	public List<String> classifierQualifiedNames;
	
	public Map<ISAReferenceableElement, Integer> classifierLevelReferences;
	public List<String> packages;
	
	
	
	/**
	 * Create information on an cluster by setting the information fields manually
	 */
	public ClusterInformation(List<String> elementQualifiedNames, Map<ISAReferenceableElement, Integer> references, List<String> packages, double uncertainReferenceOccurences) {
		this.classifierQualifiedNames = elementQualifiedNames;
		this.classifierLevelReferences = references;
		this.packages = packages;
	}
	
	public ClusterInformation(ISACluster cluster) {
		this.classifierQualifiedNames = getClassifierNames(cluster);
		
		this.classifierLevelReferences = ISASystemModelHelper.getAllContainedReferences(cluster);
		this.classifierLevelReferences = ISASystemModelHelper.pullReferencesUpToClassifierLevel(this.classifierLevelReferences);
		
		this.packages = getPackageNames(cluster);
	}
	
	/**
	 * Create information on a singleton cluster based on a classifier
	 */
	public ClusterInformation(ISAClassifier classifier) {
		this.classifierQualifiedNames = new ArrayList<>(1);
		this.classifierQualifiedNames.add(classifier.getFullyQualifiedName());

		this.classifierLevelReferences = ISASystemModelHelper.getAllContainedReferences(classifier);
		this.classifierLevelReferences = ISASystemModelHelper.pullReferencesUpToClassifierLevel(this.classifierLevelReferences);
		
		this.packages = new ArrayList<>(1);
		this.packages.add(classifier.getContainingPackage().getFullyQualifiedName());
	}
	


	private List<String> getClassifierNames(ISACluster cluster) {
		List<String> result = ISAClusteringCachingUtil.INSTANCE.getCachedClassifierNames(cluster);
		if(result != null)
			return result;
		
		result = new LinkedList<>();
		for(ISAClassifier classifier : cluster.getClassifiers()) {
			result.add(classifier.getFullyQualifiedName());
		}
		for(ISACluster child : cluster.getChildClusters()) {
			result.addAll(getClassifierNames(child));
		}
//		ISAClusteringCachingUtil.INSTANCE.cacheClassifierNames(cluster, result);
		return result;
	}
	
	private List<String> getPackageNames(ISACluster cluster) {
		List<String> result = ISAClusteringCachingUtil.INSTANCE.getCachedPackageNames(cluster);
		if(result != null)
			return result;
		
		result = new LinkedList<>();
		for(ISAClassifier classifier : cluster.getClassifiers()) {
			result.add(classifier.getContainingPackage().getFullyQualifiedName());
		}
		for(ISACluster child : cluster.getChildClusters()) {
			result.addAll(getPackageNames(child));
		}
//		ISAClusteringCachingUtil.INSTANCE.cachePackageNames(cluster, result);
		return result;
	}
	

}

/**
 */
package dk.itu.cs.isa.model.software.structure.libraries.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement;
import dk.itu.cs.isa.model.software.structure.classifiers.ISARecord;
import dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAClassImpl;
import dk.itu.cs.isa.model.software.structure.libraries.ISALibraryClassifier;
import dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Library Classifier</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryClassifierImpl#getExtendedInterfaces <em>Extended Interfaces</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryClassifierImpl#getUsagesAsImplementedInterface <em>Usages As Implemented Interface</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryClassifierImpl#getUsagesAsExtendedInterface <em>Usages As Extended Interface</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryClassifierImpl#getConstants <em>Constants</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.libraries.impl.ISALibraryClassifierImpl#getComponents <em>Components</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISALibraryClassifierImpl extends ISAClassImpl implements ISALibraryClassifier {
	/**
	 * The cached value of the '{@link #getExtendedInterfaces() <em>Extended Interfaces</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendedInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAInterface> extendedInterfaces;

	/**
	 * The cached value of the '{@link #getUsagesAsImplementedInterface() <em>Usages As Implemented Interface</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsagesAsImplementedInterface()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAClass> usagesAsImplementedInterface;

	/**
	 * The cached value of the '{@link #getUsagesAsExtendedInterface() <em>Usages As Extended Interface</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsagesAsExtendedInterface()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAInterfaceExtendingElement> usagesAsExtendedInterface;

	/**
	 * The cached value of the '{@link #getConstants() <em>Constants</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstants()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAEnumConstant> constants;

	/**
	 * The cached value of the '{@link #getComponents() <em>Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<ISARecordComponent> components;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISALibraryClassifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibrariesPackage.Literals.ISA_LIBRARY_CLASSIFIER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAInterface> getExtendedInterfaces() {
		if (extendedInterfaces == null) {
			extendedInterfaces = new EObjectWithInverseResolvingEList.ManyInverse<ISAInterface>(ISAInterface.class, this, LibrariesPackage.ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES, ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE);
		}
		return extendedInterfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAClass> getUsagesAsImplementedInterface() {
		if (usagesAsImplementedInterface == null) {
			usagesAsImplementedInterface = new EObjectWithInverseResolvingEList.ManyInverse<ISAClass>(ISAClass.class, this, LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE, ClassifiersPackage.ISA_CLASS__IMPLEMENTED_INTERFACES);
		}
		return usagesAsImplementedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAInterfaceExtendingElement> getUsagesAsExtendedInterface() {
		if (usagesAsExtendedInterface == null) {
			usagesAsExtendedInterface = new EObjectWithInverseResolvingEList.ManyInverse<ISAInterfaceExtendingElement>(ISAInterfaceExtendingElement.class, this, LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE, ClassifiersPackage.ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES);
		}
		return usagesAsExtendedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAEnumConstant> getConstants() {
		if (constants == null) {
			constants = new EObjectContainmentEList<ISAEnumConstant>(ISAEnumConstant.class, this, LibrariesPackage.ISA_LIBRARY_CLASSIFIER__CONSTANTS);
		}
		return constants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISARecordComponent> getComponents() {
		if (components == null) {
			components = new EObjectContainmentEList<ISARecordComponent>(ISARecordComponent.class, this, LibrariesPackage.ISA_LIBRARY_CLASSIFIER__COMPONENTS);
		}
		return components;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtendedInterfaces()).basicAdd(otherEnd, msgs);
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getUsagesAsImplementedInterface()).basicAdd(otherEnd, msgs);
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getUsagesAsExtendedInterface()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES:
				return ((InternalEList<?>)getExtendedInterfaces()).basicRemove(otherEnd, msgs);
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE:
				return ((InternalEList<?>)getUsagesAsImplementedInterface()).basicRemove(otherEnd, msgs);
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE:
				return ((InternalEList<?>)getUsagesAsExtendedInterface()).basicRemove(otherEnd, msgs);
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__CONSTANTS:
				return ((InternalEList<?>)getConstants()).basicRemove(otherEnd, msgs);
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__COMPONENTS:
				return ((InternalEList<?>)getComponents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES:
				return getExtendedInterfaces();
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE:
				return getUsagesAsImplementedInterface();
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE:
				return getUsagesAsExtendedInterface();
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__CONSTANTS:
				return getConstants();
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__COMPONENTS:
				return getComponents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES:
				getExtendedInterfaces().clear();
				getExtendedInterfaces().addAll((Collection<? extends ISAInterface>)newValue);
				return;
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE:
				getUsagesAsImplementedInterface().clear();
				getUsagesAsImplementedInterface().addAll((Collection<? extends ISAClass>)newValue);
				return;
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE:
				getUsagesAsExtendedInterface().clear();
				getUsagesAsExtendedInterface().addAll((Collection<? extends ISAInterfaceExtendingElement>)newValue);
				return;
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__CONSTANTS:
				getConstants().clear();
				getConstants().addAll((Collection<? extends ISAEnumConstant>)newValue);
				return;
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__COMPONENTS:
				getComponents().clear();
				getComponents().addAll((Collection<? extends ISARecordComponent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES:
				getExtendedInterfaces().clear();
				return;
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE:
				getUsagesAsImplementedInterface().clear();
				return;
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE:
				getUsagesAsExtendedInterface().clear();
				return;
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__CONSTANTS:
				getConstants().clear();
				return;
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__COMPONENTS:
				getComponents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES:
				return extendedInterfaces != null && !extendedInterfaces.isEmpty();
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE:
				return usagesAsImplementedInterface != null && !usagesAsImplementedInterface.isEmpty();
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE:
				return usagesAsExtendedInterface != null && !usagesAsExtendedInterface.isEmpty();
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__CONSTANTS:
				return constants != null && !constants.isEmpty();
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__COMPONENTS:
				return components != null && !components.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAInterfaceExtendingElement.class) {
			switch (derivedFeatureID) {
				case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES: return ClassifiersPackage.ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES;
				default: return -1;
			}
		}
		if (baseClass == ISAInterface.class) {
			switch (derivedFeatureID) {
				case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE: return ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE;
				case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE: return ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE;
				default: return -1;
			}
		}
		if (baseClass == ISAEnum.class) {
			switch (derivedFeatureID) {
				case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__CONSTANTS: return ClassifiersPackage.ISA_ENUM__CONSTANTS;
				default: return -1;
			}
		}
		if (baseClass == ISARecord.class) {
			switch (derivedFeatureID) {
				case LibrariesPackage.ISA_LIBRARY_CLASSIFIER__COMPONENTS: return ClassifiersPackage.ISA_RECORD__COMPONENTS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAInterfaceExtendingElement.class) {
			switch (baseFeatureID) {
				case ClassifiersPackage.ISA_INTERFACE_EXTENDING_ELEMENT__EXTENDED_INTERFACES: return LibrariesPackage.ISA_LIBRARY_CLASSIFIER__EXTENDED_INTERFACES;
				default: return -1;
			}
		}
		if (baseClass == ISAInterface.class) {
			switch (baseFeatureID) {
				case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_IMPLEMENTED_INTERFACE: return LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_IMPLEMENTED_INTERFACE;
				case ClassifiersPackage.ISA_INTERFACE__USAGES_AS_EXTENDED_INTERFACE: return LibrariesPackage.ISA_LIBRARY_CLASSIFIER__USAGES_AS_EXTENDED_INTERFACE;
				default: return -1;
			}
		}
		if (baseClass == ISAEnum.class) {
			switch (baseFeatureID) {
				case ClassifiersPackage.ISA_ENUM__CONSTANTS: return LibrariesPackage.ISA_LIBRARY_CLASSIFIER__CONSTANTS;
				default: return -1;
			}
		}
		if (baseClass == ISARecord.class) {
			switch (baseFeatureID) {
				case ClassifiersPackage.ISA_RECORD__COMPONENTS: return LibrariesPackage.ISA_LIBRARY_CLASSIFIER__COMPONENTS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ISALibraryClassifierImpl

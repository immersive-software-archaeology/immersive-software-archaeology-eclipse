package dk.itu.cs.isa.model.system.architecture.hierarchical;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.system.architecture.preferences.ISAPreferencesClustering;
import dk.itu.cs.isa.model.system.architecture.util.ClusterHelper;
import dk.itu.cs.isa.model.system.architecture.util.SimilarityCalculator;

public class ISAClusteringPostprocessor {
	
	private SimilarityCalculator similarityCalculator;

	private int minimumClusterSize;
	private int maximumClusterSize;
	
	
	
	public ISAClusteringPostprocessor() {
		minimumClusterSize = ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MINIMUM_CLUSTER_SIZE, 15);
		maximumClusterSize = ISAPreferencesClustering.getInt(ISAPreferencesClustering.DBSCAN_MAXIMUM_CLUSTER_SIZE, 50);
		similarityCalculator = new SimilarityCalculator();
	}
	
	
	
	public void process(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) {
		monitor.beginTask("Analyzing architecture (post-processing)", IProgressMonitor.UNKNOWN);
		
		moveRootClusterOutliersIntoOwnCluster(isaSystem, monitor);
		pullTogetherSmallRootClusters(isaSystem, monitor);
		
		while(splitUpTooLargeClusters(isaSystem, monitor)) { }
		while(mergeTogetherTooSmallClusters(isaSystem, monitor)) { }
	}
	
	
	
	private boolean mergeTogetherTooSmallClusters(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) {
		List<ISACluster> tooSmallClusters = new ArrayList<>();
		identifyTooSmallClusters(tooSmallClusters, isaSystem.getRootCluster());
		boolean performedClusterMerge = false;
		
		monitor.beginTask("Post-Processing", tooSmallClusters.size());
		for(ISACluster tooSmallCluster : tooSmallClusters) {
			monitor.subTask("Pulling together too small clusters "
					+ "(" + (tooSmallClusters.indexOf(tooSmallCluster)+1) + "/" + tooSmallClusters.size() + "), "
					+ "current cluster's size: " + tooSmallCluster.getClassifiers().size());
			monitor.worked(1);
			
			if(tooSmallCluster.getClassifiers().size() > minimumClusterSize)
				continue;
			
			List<ISACluster> candidateClustersToMergeInto = new ArrayList<>();
			EObject currentParent = tooSmallCluster.eContainer();
			while(currentParent != null && currentParent instanceof ISACluster) {
				candidateClustersToMergeInto = ((ISACluster) currentParent).getChildClusters();
				if(candidateClustersToMergeInto.size() > 1)
					break;
				currentParent = currentParent.eContainer();
			}
			if(candidateClustersToMergeInto.isEmpty())
				continue;
			
			double highestSimilarity = 0;
			ISACluster bestCandidateCluster = null;
			for(ISACluster candidateCluster : candidateClustersToMergeInto) {
				if(tooSmallCluster.equals(candidateCluster))
					continue;
				
				double similarity = similarityCalculator.calculateSimilarity(tooSmallCluster, candidateCluster);
				if(similarity > highestSimilarity || bestCandidateCluster == null) {
					highestSimilarity = similarity;
					bestCandidateCluster = candidateCluster;
				}
			}
			if(bestCandidateCluster == null)
				continue;
			
			bestCandidateCluster.getClassifiers().addAll(tooSmallCluster.getClassifiers());
			bestCandidateCluster.getChildClusters().addAll(tooSmallCluster.getChildClusters());
			ClusterHelper.nameCluster(bestCandidateCluster);
			
			EObject currentCluster = tooSmallCluster;
			while(true) {
				if(currentCluster != null && currentCluster.eContainer() instanceof ISACluster) {
					ISACluster parent = ((ISACluster) currentCluster.eContainer());
					parent.getChildClusters().remove(currentCluster);
					if(parent.getChildClusters().size() > 0)
						break;
					currentCluster = currentCluster.eContainer();
				}
				else break;
			}
			
			performedClusterMerge = true;
		}
		
		return performedClusterMerge;
	}
	
	private void identifyTooSmallClusters(List<ISACluster> tooSmallClusters, ISACluster currentCluster) {
		if(currentCluster.getClassifiers().size() < minimumClusterSize && currentCluster.getClassifiers().size() > 0)
			tooSmallClusters.add(currentCluster);
		for(ISACluster child : currentCluster.getChildClusters())
			identifyTooSmallClusters(tooSmallClusters, child);
	}



	/**
	 * Use some form of agglomerative clustering to split too large clusters apart
	 */
	private boolean splitUpTooLargeClusters(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) {
		List<ISACluster> tooLargeClusters = new ArrayList<>();
		identifyTooLargeClusters(tooLargeClusters, isaSystem.getRootCluster());
		boolean performedClusterSplit = false;

		monitor.beginTask("Post-Processing", tooLargeClusters.size());
		for(ISACluster tooLargeCluster : tooLargeClusters) {
			monitor.subTask("Breaking up too large clusters: "
					+ "(" + (tooLargeClusters.indexOf(tooLargeCluster)+1) + "/" + tooLargeClusters.size() + ")"
					+ " - current cluster's size: " + tooLargeCluster.getClassifiers().size());
			monitor.worked(1);
			
			List<ISACluster> determinedSplit = new ArrayList<>(tooLargeCluster.getClassifiers().size());
			for(ISAClassifier classifier : tooLargeCluster.getClassifiers())
				ClusterHelper.initializeSetOfSingletonClustersRecursively(determinedSplit, classifier);
			
			int desiredNumberOfSplits = (int) Math.ceil(tooLargeCluster.getClassifiers().size() / (double) maximumClusterSize);

			while(determinedSplit.size() > desiredNumberOfSplits) {
				if(monitor.isCanceled())
					break;
				
				ISACluster cluster1 = null;
				ISACluster cluster2 = null;
				double highestTotalSimilarity = 0;
				
				for(int i=0; i<determinedSplit.size(); i++) {
					ISACluster cl1 = determinedSplit.get(i);
					
					for(int j=i+1; j<determinedSplit.size(); j++) {
						ISACluster cl2 = determinedSplit.get(j);
						double similarity = similarityCalculator.calculateSimilarity(cl1, cl2);
						if(similarity > highestTotalSimilarity) {
							highestTotalSimilarity = similarity;
							cluster1 = cl1;
							cluster2 = cl2;
						}
					}
				}
				
				if(cluster1 == null || cluster2 == null)
					break;
				
				int newCombinedClusterSize = cluster1.getClassifiers().size() + cluster2.getClassifiers().size();
				if(newCombinedClusterSize >= maximumClusterSize)
					break;
				
				determinedSplit.remove(cluster2);
				cluster1.getClassifiers().addAll(cluster2.getClassifiers());
				cluster1.getChildClusters().addAll(cluster2.getChildClusters());
				ClusterHelper.nameCluster(cluster1);
			}
			
			ISACluster largestCluster = getLargestCluster(determinedSplit, new ArrayList<>());
			ISACluster otherCluster = ArchitectureFactory.eINSTANCE.createISACluster();
			for(ISACluster remainderCluster : determinedSplit) {
				if(remainderCluster.equals(largestCluster))
					continue;
				otherCluster.getClassifiers().addAll(remainderCluster.getClassifiers());
				otherCluster.getChildClusters().addAll(remainderCluster.getChildClusters());
			}
			ClusterHelper.nameCluster(otherCluster);
			
			tooLargeCluster.getClassifiers().clear();
			tooLargeCluster.getChildClusters().add(largestCluster);
			tooLargeCluster.getChildClusters().add(otherCluster);
			
			performedClusterSplit = true;
		}
		
		return performedClusterSplit;
	}
	
	private void identifyTooLargeClusters(List<ISACluster> tooLargeClusters, ISACluster currentCluster) {
		if(currentCluster.getClassifiers().size() > maximumClusterSize)
			tooLargeClusters.add(currentCluster);
		for(ISACluster child : currentCluster.getChildClusters())
			identifyTooLargeClusters(tooLargeClusters, child);
	}



	/**
	 * Move root level outliers in their own cluster
	 * TODO solve better
	 */
	private void moveRootClusterOutliersIntoOwnCluster(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) {
		ISACluster directChildrenOfRootClusterCluster = ArchitectureFactory.eINSTANCE.createISACluster();
		directChildrenOfRootClusterCluster.getClassifiers().addAll(isaSystem.getRootCluster().getClassifiers());
		isaSystem.getRootCluster().getClassifiers().clear();

		ClusterHelper.nameCluster(isaSystem.getRootCluster());
		ClusterHelper.nameCluster(directChildrenOfRootClusterCluster);
		isaSystem.getRootCluster().getChildClusters().add(directChildrenOfRootClusterCluster);
	}
	
	
	
	/**
	 * Use a kind of agglomerative clustering to pull together small clusters that are cohesive
	 * and thereby reduce the many small planets that sometimes occur.
	 */
	private void pullTogetherSmallRootClusters(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) {
		monitor.subTask("Pulling together cohesive first-level clusters...");
		
		List<ISACluster> firstLevelClusters = isaSystem.getRootCluster().getChildClusters();
		List<ISACluster> unmatchableClusters = new ArrayList<>();

		int numberOfClassifiersInSystem = ClusterHelper.getElementCount(isaSystem.getRootCluster());
		ISACluster currentSmallestCluster = getSmallestCluster(firstLevelClusters, unmatchableClusters);
		while(currentSmallestCluster != null
				&& ClusterHelper.getElementCountNoCaching(currentSmallestCluster) < maximumClusterSize * 2
				&& firstLevelClusters.size() > numberOfClassifiersInSystem / 100) {
			double highestSimilarity = 0;
			ISACluster mostSimilarOther = null;
			
			for(ISACluster otherCluster : firstLevelClusters) {
				if(currentSmallestCluster.equals(otherCluster))
					continue;
				double similarity = similarityCalculator.calculateSimilarity(currentSmallestCluster, otherCluster);
				if(similarity > highestSimilarity) {
					highestSimilarity = similarity;
					mostSimilarOther = otherCluster;
				}
			}
			
			if(mostSimilarOther == null) {
				unmatchableClusters.add(currentSmallestCluster);
			}
			else {
				ISACluster newParent = ArchitectureFactory.eINSTANCE.createISACluster();
				newParent.getChildClusters().add(currentSmallestCluster);
				newParent.getChildClusters().add(mostSimilarOther);
				ClusterHelper.nameCluster(newParent);
				firstLevelClusters.add(newParent);
			}
			
			currentSmallestCluster = getSmallestCluster(firstLevelClusters, unmatchableClusters);
		}
	}
	
	private ISACluster getSmallestCluster(List<ISACluster> clusters, List<ISACluster> unmatchableClusters) {
		int smallestChildSize = Integer.MAX_VALUE;
		ISACluster smallestChild = null;
		
		for(ISACluster cluster : clusters) {
			int childSize = ClusterHelper.getElementCountNoCaching(cluster);
			if(childSize < smallestChildSize && !unmatchableClusters.contains(cluster)) {
				smallestChild = cluster;
				smallestChildSize = childSize;
			}
		}
		
		return smallestChild;
	}
	
	private ISACluster getLargestCluster(List<ISACluster> clusters, List<ISACluster> unmatchableClusters) {
		int largestChildSize = 0;
		ISACluster largestChild = null;
		
		for(ISACluster cluster : clusters) {
			int childSize = ClusterHelper.getElementCountNoCaching(cluster);
			if(childSize > largestChildSize && !unmatchableClusters.contains(cluster)) {
				largestChild = cluster;
				largestChildSize = childSize;
			}
		}
		
		return largestChild;
	}

}









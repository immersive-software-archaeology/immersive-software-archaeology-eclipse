/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Draw Freehand Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getDrawFreehandOperation()
 * @model
 * @generated
 */
public interface DrawFreehandOperation extends AbstractDrawOperation {
} // DrawFreehandOperation

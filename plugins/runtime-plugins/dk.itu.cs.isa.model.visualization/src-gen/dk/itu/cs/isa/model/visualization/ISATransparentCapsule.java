/**
 */
package dk.itu.cs.isa.model.visualization;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Transparent Capsule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule#getSubCapsules <em>Sub Capsules</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule#getSpaceStations <em>Space Stations</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule#getTags <em>Tags</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISATransparentCapsule()
 * @model
 * @generated
 */
public interface ISATransparentCapsule extends ISANamedVisualElement {
	/**
	 * Returns the value of the '<em><b>Sub Capsules</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Capsules</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISATransparentCapsule_SubCapsules()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISATransparentCapsule> getSubCapsules();

	/**
	 * Returns the value of the '<em><b>Space Stations</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISASpaceStation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Space Stations</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISATransparentCapsule_SpaceStations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISASpaceStation> getSpaceStations();

	/**
	 * Returns the value of the '<em><b>Tags</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tags</em>' attribute list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISATransparentCapsule_Tags()
	 * @model
	 * @generated
	 */
	EList<String> getTags();

} // ISATransparentCapsule

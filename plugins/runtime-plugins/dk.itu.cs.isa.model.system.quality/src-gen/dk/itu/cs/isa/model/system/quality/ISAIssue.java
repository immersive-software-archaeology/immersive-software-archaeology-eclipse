/**
 */
package dk.itu.cs.isa.model.system.quality;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Issue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getParentEntry <em>Parent Entry</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getName <em>Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getOccurence <em>Occurence</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getSeverity <em>Severity</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getHint <em>Hint</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getEffortToFixInMinutes <em>Effort To Fix In Minutes</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAIssue()
 * @model
 * @generated
 */
public interface ISAIssue extends EObject {
	/**
	 * Returns the value of the '<em><b>Parent Entry</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getIssues <em>Issues</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Entry</em>' container reference.
	 * @see #setParentEntry(ISAClassifierQualityEntry)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAIssue_ParentEntry()
	 * @see dk.itu.cs.isa.model.system.quality.ISAClassifierQualityEntry#getIssues
	 * @model opposite="issues" required="true" transient="false"
	 * @generated
	 */
	ISAClassifierQualityEntry getParentEntry();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getParentEntry <em>Parent Entry</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Entry</em>' container reference.
	 * @see #getParentEntry()
	 * @generated
	 */
	void setParentEntry(ISAClassifierQualityEntry value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.itu.cs.isa.model.system.quality.ISAQualityIssueType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueType
	 * @see #setType(ISAQualityIssueType)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAIssue_Type()
	 * @model required="true"
	 * @generated
	 */
	ISAQualityIssueType getType();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueType
	 * @see #getType()
	 * @generated
	 */
	void setType(ISAQualityIssueType value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAIssue_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Occurence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Occurence</em>' containment reference.
	 * @see #setOccurence(ISAFilePassage)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAIssue_Occurence()
	 * @model containment="true"
	 * @generated
	 */
	ISAFilePassage getOccurence();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getOccurence <em>Occurence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Occurence</em>' containment reference.
	 * @see #getOccurence()
	 * @generated
	 */
	void setOccurence(ISAFilePassage value);

	/**
	 * Returns the value of the '<em><b>Severity</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Severity</em>' attribute.
	 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity
	 * @see #setSeverity(ISAQualityIssueSeverity)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAIssue_Severity()
	 * @model required="true"
	 * @generated
	 */
	ISAQualityIssueSeverity getSeverity();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getSeverity <em>Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Severity</em>' attribute.
	 * @see dk.itu.cs.isa.model.system.quality.ISAQualityIssueSeverity
	 * @see #getSeverity()
	 * @generated
	 */
	void setSeverity(ISAQualityIssueSeverity value);

	/**
	 * Returns the value of the '<em><b>Hint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hint</em>' attribute.
	 * @see #setHint(String)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAIssue_Hint()
	 * @model
	 * @generated
	 */
	String getHint();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getHint <em>Hint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hint</em>' attribute.
	 * @see #getHint()
	 * @generated
	 */
	void setHint(String value);

	/**
	 * Returns the value of the '<em><b>Effort To Fix In Minutes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Effort To Fix In Minutes</em>' attribute.
	 * @see #setEffortToFixInMinutes(int)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAIssue_EffortToFixInMinutes()
	 * @model
	 * @generated
	 */
	int getEffortToFixInMinutes();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAIssue#getEffortToFixInMinutes <em>Effort To Fix In Minutes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Effort To Fix In Minutes</em>' attribute.
	 * @see #getEffortToFixInMinutes()
	 * @generated
	 */
	void setEffortToFixInMinutes(int value);

} // ISAIssue

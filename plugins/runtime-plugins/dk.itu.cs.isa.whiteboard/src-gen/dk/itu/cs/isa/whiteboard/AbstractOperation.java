/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractOperation()
 * @model abstract="true"
 * @generated
 */
public interface AbstractOperation extends AbstractWhiteboardEvent {
} // AbstractOperation

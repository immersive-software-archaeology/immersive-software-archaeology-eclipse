/**
 */
package dk.itu.cs.isa.model.software.structure.libraries.impl;

import dk.itu.cs.isa.model.software.structure.libraries.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LibrariesFactoryImpl extends EFactoryImpl implements LibrariesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LibrariesFactory init() {
		try {
			LibrariesFactory theLibrariesFactory = (LibrariesFactory)EPackage.Registry.INSTANCE.getEFactory(LibrariesPackage.eNS_URI);
			if (theLibrariesFactory != null) {
				return theLibrariesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new LibrariesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibrariesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case LibrariesPackage.ISA_LIBRARY: return createISALibrary();
			case LibrariesPackage.ISA_PRIMITIVE_TYPE: return createISAPrimitiveType();
			case LibrariesPackage.ISA_LIBRARY_CLASSIFIER: return createISALibraryClassifier();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISALibrary createISALibrary() {
		ISALibraryImpl isaLibrary = new ISALibraryImpl();
		return isaLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAPrimitiveType createISAPrimitiveType() {
		ISAPrimitiveTypeImpl isaPrimitiveType = new ISAPrimitiveTypeImpl();
		return isaPrimitiveType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISALibraryClassifier createISALibraryClassifier() {
		ISALibraryClassifierImpl isaLibraryClassifier = new ISALibraryClassifierImpl();
		return isaLibraryClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LibrariesPackage getLibrariesPackage() {
		return (LibrariesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static LibrariesPackage getPackage() {
		return LibrariesPackage.eINSTANCE;
	}

} //LibrariesFactoryImpl

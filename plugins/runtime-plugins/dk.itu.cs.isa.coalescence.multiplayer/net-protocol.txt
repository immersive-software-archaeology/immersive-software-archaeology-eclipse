
> Failure Packet
		1 byte			message type
						 > 0 = general failure
						 > 1 = outdated packet
		1 byte			message length
		x byte			message



> Connect Packet
	request:
		1 byte			message type, value = 8
		1 byte			system name length
		x byte			system name
	response:
		1 byte			message type, value = 8
		1 byte			contains client's id to be stored in the client
						and used for subsequent packets



> Disconnect Packet
	request:
		1 byte			message type, value = 16
		1 byte			contains client's id
	response:
		1 byte			message type, value = 16



> Transform Update Packet

	request:
	
		1 byte			message type, value = 32
		
		1 byte			client id
		
		8 byte	long	client-side time stamp
		1 byte			package number
		
		1 byte			client current fps capped at 200
		
			4 byte	float	head position - x
			4 byte	float	head position - y
			4 byte	float	head position - z
			
			4 byte	float	head rotation - x
			4 byte	float	head rotation - y
			4 byte	float	head rotation - z
		
		1 byte			left hand pose
		
			4 byte	float	left hand position - x
			4 byte	float	left hand position - y
			4 byte	float	left hand position - z
			
			4 byte	float	left hand rotation - x
			4 byte	float	left hand rotation - y
			4 byte	float	left hand rotation - z
		
			4 byte	float	index finger tip position - x
			4 byte	float	index finger tip position - y
			4 byte	float	index finger tip position - z
		
			4 byte	int		ID of object moved with left hand:
			
				4 byte	float	object position - x
				4 byte	float	object position - y
				4 byte	float	object position - z
				
				4 byte	float	object rotation - x
				4 byte	float	object rotation - y
				4 byte	float	object rotation - z
		
		1 byte			right hand pose
		
			4 byte	float	right hand position - x
			4 byte	float	right hand position - y
			4 byte	float	right hand position - z
			
			4 byte	float	right hand rotation - x
			4 byte	float	right hand rotation - y
			4 byte	float	right hand rotation - z
		
			4 byte	float	index finger tip position - x
			4 byte	float	index finger tip position - y
			4 byte	float	index finger tip position - z
		
			4 byte	int		ID of object moved with right hand:
			
				4 byte	float	object position - x
				4 byte	float	object position - y
				4 byte	float	object position - z
				
				4 byte	float	object rotation - x
				4 byte	float	object rotation - y
				4 byte	float	object rotation - z
		
	response:
		size =	27 bytes +
				(#clients - 1) * (
					1 [client id] * 1 byte +
					2 [hand poses] * 1 byte +
					2 [index finger tip positions] * 1 [vector] * 3 [floats] * 4 bytes +
					2 [moved object IDs] * 1 [integer] * 4 bytes +
					5 [transforms: head, lHand, lHandObject, rHand, rHandObject] * 2 [vectors] * 3 [floats] * 4 bytes
				)
			 =	27 bytes + #clients	* 155
			 	-> 2 clients = 182 bytes
			 	-> 3 clients = 337 bytes
			 	-> 4 clients = 492 bytes
			 	-> 5 clients = 647 bytes
			 	-> 6 clients = 802 bytes
					
	
		1 byte			message type, value = 32
		
		8 byte	long	server-side time stamp
		1 byte			package number
		
		8 byte	long	last visualization event timestamp
		8 byte	long	last whiteboard event timestamp
	
		1 byte			number of other clients - information follows
						
		1 byte			client 1 id - information follows
		
			4 byte	float	client 1 head position - x
			4 byte	float	client 1 head position - y
			4 byte	float	client 1 head position - z
			
			4 byte	float	client 1 head rotation - x
			4 byte	float	client 1 head rotation - y
			4 byte	float	client 1 head rotation - z
			
			1 byte			client 1 left hand pose
			
				4 byte	float	client 1 left hand position - x
				4 byte	float	etc...
				
				...
		
				4 byte	int		ID of object moved by client 1 with
								left hand:
				
					4 byte	float	object position - x
					4 byte	float	etc...
					
					...
			
			1 byte			client 1 right hand pose
			
				4 byte	float	client 1 right hand position - x
				4 byte	float	etc...
				
				...
		
				4 byte	int		ID of object moved by client 1 with
								right hand:
				
					4 byte	float	object position - x
					4 byte	float	etc...
					
					...
						
		1 byte			client 2 id - information follows
		
			4 byte	float	client 2 head position - x
			4 byte	float	etc...
			
			1 byte			client 2 left hand pose
				
				... position, rotation
					
					... object with position and rotation
			
			1 byte			client 2 right hand pose
				
				... position, rotation
					
					... object with position and rotation
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
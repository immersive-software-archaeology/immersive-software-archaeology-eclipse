/**
 */
package dk.itu.cs.isa.events.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Input Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.StringInputEvent#getUserInput <em>User Input</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getStringInputEvent()
 * @model
 * @generated
 */
public interface StringInputEvent extends InputEvent {
	/**
	 * Returns the value of the '<em><b>User Input</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Input</em>' attribute.
	 * @see #setUserInput(String)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getStringInputEvent_UserInput()
	 * @model required="true"
	 * @generated
	 */
	String getUserInput();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.StringInputEvent#getUserInput <em>User Input</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Input</em>' attribute.
	 * @see #getUserInput()
	 * @generated
	 */
	void setUserInput(String value);

} // StringInputEvent

package dk.itu.cs.isa.model.visualization.creation.java.multiview;

import java.util.LinkedList;
import java.util.List;

import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;

public class MultiViewModelCleanupUtil {
	
	public static boolean deleteEmptyCapsules(ISATransparentCapsule capsule) {
		if(capsule.getSpaceStations().size() == 0 && capsule.getSubCapsules().size() == 0 && capsule.eContainer() != null) {
			if(capsule.eContainer() instanceof ISAVisualizationModel)
				throw new RuntimeException("The visualization model is empty.");
			else if(capsule.eContainer() instanceof ISATransparentCapsule)
				((ISATransparentCapsule) capsule.eContainer()).getSubCapsules().remove(capsule);
			return true;
		}
		for(int i=0; i<capsule.getSubCapsules().size(); i++) {
			if(deleteEmptyCapsules(capsule.getSubCapsules().get(i)))
				i--;
		}
		return false;
	}
	
	
	
	public static void collapseCapsules(ISATransparentCapsule root, String delimiter) {
		List<ISATransparentCapsule> capsulesToCollapse = new LinkedList<>();
		collectCapsulesToCollapse(capsulesToCollapse, root);

		for(ISATransparentCapsule capsuleToCollapse : capsulesToCollapse) {
			ISATransparentCapsule child = capsuleToCollapse.getSubCapsules().get(0);
			
			if(capsuleToCollapse.eContainer() instanceof ISAVisualizationModel) {
				((ISAVisualizationModel) capsuleToCollapse.eContainer()).setSubjectSystemCapsule(child);
			}
			else if(capsuleToCollapse.eContainer() instanceof ISATransparentCapsule) {
				child.setName(capsuleToCollapse.getName() + delimiter + child.getName());
				child.setQualifiedName(((ISATransparentCapsule) capsuleToCollapse.eContainer()).getQualifiedName() + delimiter + child.getName());
				
				((ISATransparentCapsule) capsuleToCollapse.eContainer()).getSubCapsules().add(child);
				((ISATransparentCapsule) capsuleToCollapse.eContainer()).getSubCapsules().remove(capsuleToCollapse);
			}
		}
	}

	private static void collectCapsulesToCollapse(List<ISATransparentCapsule> capsulesToCollapse, ISATransparentCapsule capsule) {
		if(capsule.getSpaceStations().size() == 0 && capsule.getSubCapsules().size() == 1 && capsule.eContainer() != null)
			capsulesToCollapse.add(capsule);
		for(ISATransparentCapsule subCapsule : capsule.getSubCapsules())
			collectCapsulesToCollapse(capsulesToCollapse, subCapsule);
	}

}

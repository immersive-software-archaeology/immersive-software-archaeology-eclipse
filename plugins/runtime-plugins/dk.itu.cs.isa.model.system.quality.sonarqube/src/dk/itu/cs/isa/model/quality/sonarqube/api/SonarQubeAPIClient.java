package dk.itu.cs.isa.model.quality.sonarqube.api;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.Builder;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.quality.sonarqube.hook.SonarQubeWebHookServer;
import dk.itu.cs.isa.model.quality.sonarqube.preferences.ISAPreferencesSonarQube;

public class SonarQubeAPIClient {
	
	public static final int OFFLINE = 0, ONLINE_ERROR = 1, ONLINE_SANE = 2;

	private static final String TOKEN_NAME = "ISA Analysis Token (only temporary)";
	private static final String WEB_HOOK_NAME = "ISA Analysis Web Hook";
	
	private HttpClient client;
	
	
	
	public SonarQubeAPIClient() {
		client = HttpClient.newHttpClient();
	}
	
	
	
	/**
	 * Pings the sonar qube server, using the credentials specified
	 * in the Eclipse ISA preference page.<br>
	 * Cf. SonarQubeAPIClient.ONLINE_SANE, SonarQubeAPIClient.ONLINE_ERROR, SonarQubeAPIClient.OFFLINE
	 * @return state of server, i.e., ONLINE_SANE, ONLINE_ERROR, or OFFLINE
	 */
	public int pingServer() throws IOException, InterruptedException {
		try {
			sendRequest("GET", "system/ping");
			return ONLINE_SANE;
		}
		catch(SonarQubeServerError e) {
			// Server running, yet spitting out error,
			// probably because user is using wrong credentials
			return ONLINE_ERROR;
		}
		catch(ConnectException e) {
			return OFFLINE;
		}
	}
	
	
	
	public void revokePreviousToken() throws IOException, InterruptedException {
		sendRequest("POST", "user_tokens/revoke", "name="+TOKEN_NAME);
	}

	public String generateToken() throws IOException, InterruptedException {
		JsonElement responseJson = sendRequest("POST", "user_tokens/generate", "name="+TOKEN_NAME);
		return responseJson.getAsJsonObject().get("token").getAsString();
	}



	public boolean doesSonarQubeProjectExist(String projectName) throws IOException, InterruptedException {
		JsonElement responseJson = sendRequest("GET", "projects/search", "projects="+projectName);
		return responseJson.getAsJsonObject().get("paging").getAsJsonObject().get("total").getAsInt() > 0;
	}

	public void createSonarQubeProject(String projectName) throws IOException, InterruptedException {
		sendRequest("POST", "projects/create",
				"name="+projectName,
				"project="+projectName);
	}
	
	
	
	public void enableLocalWebHooks() throws IOException, InterruptedException {
		sendRequest("POST", "settings/set",
				"key=sonar.validateWebhooks",
				"value=false");
	}
	
	public void deleteAllWebHooks(String projectName) throws IOException, InterruptedException {
		List<String> allWebhookKeys = getWebHooks(projectName);
		for(String key : allWebhookKeys) {
			sendRequest("POST", "webhooks/delete",
					"webhook="+key);
		}
	}

	public List<String> getWebHooks(String projectName) throws IOException, InterruptedException {
		JsonElement responseJson = sendRequest("GET", "webhooks/list", "project="+projectName);
		JsonArray webhookArray = responseJson.getAsJsonObject().get("webhooks").getAsJsonArray();
		
		List<String> webhookKeyList = new ArrayList<>(webhookArray.size());
		for(JsonElement webhook : webhookArray) {
			webhookKeyList.add(webhook.getAsJsonObject().get("key").getAsString());
		}
		return webhookKeyList;
	}

	public void setWebHook(String projectName) throws IOException, InterruptedException {
		sendRequest("POST", "webhooks/create",
				"name="+WEB_HOOK_NAME,
				"project="+projectName,
				"url="+SonarQubeWebHookServer.INSTANCE.getServerAddress());
	}



	public List<JsonElement> getAllMeasures(String projectName, String[] qualityMetrics) throws IOException, InterruptedException {
		JsonElement firstJson = getMeasures(projectName, qualityMetrics, 1);
		
		int numberOfElements = firstJson.getAsJsonObject().get("paging").getAsJsonObject().get("total").getAsInt();
		int numberOfPages = 1 + numberOfElements / 500;
		
		List<JsonElement> result = new ArrayList<>(numberOfElements);
		for(int i=1; i<=numberOfPages; i++) {
			JsonElement json = getMeasures(projectName, qualityMetrics, i);
			for(JsonElement entry : json.getAsJsonObject().get("components").getAsJsonArray()) {
				result.add(entry);
			}
		}
		
		return result;
	}
	
	private JsonElement getMeasures(String projectName, String[] qualityMetrics, int page) throws IOException, InterruptedException {
//		List<String> metricNamesList = new ArrayList<>(ISAMetrics.VALUES.size());
//		for(ISAMetrics measure : ISAMetrics.VALUES) {
//			if(measure.getValue() != 0)
//				metricNamesList.add(measure.getLiteral());
//		}
		
		return sendRequest("GET", "measures/component_tree",
				"additionalFields=metrics",
				"component="+projectName,
				"metricKeys="+String.join(",", qualityMetrics),
				"qualifiers=TRK,DIR,FIL",
				"strategy=all",
				"ps=500", // entries per page
				"p="+page // page number
			);
	}
	
	
	
	public JsonObject getAllIssues(String projectName, IProgressMonitor monitor) throws IOException, InterruptedException {
		JsonObject result = new JsonObject();
		result.add("issues", new JsonArray());
		result.add("rules", new JsonArray());

		addIssuesOfTypeToResult(result, projectName, "CODE_SMELL", monitor);
		addIssuesOfTypeToResult(result, projectName, "BUG", monitor);
		addIssuesOfTypeToResult(result, projectName, "VULNERABILITY", monitor);

		return result;
	}
	
	/**
	 * The method might split up the search for issues, because the SonarQube
	 * server cannot handle responses with more than 10k results (the underlying
	 * ElasticSearch Engine cannot handle larger data sets).
	 */
	private void addIssuesOfTypeToResult(JsonObject result, String projectName, String type, IProgressMonitor monitor) throws IOException, InterruptedException {
		JsonObject testJson = doGetIssues(projectName, type, null, 1).getAsJsonObject();
		if(testJson.get("paging").getAsJsonObject().get("total").getAsInt() < 10000) {
			// All results can be fetched at once
			monitor.subTask("Fetching issues from SonarQube server (less than 10k results)");
			addIssuesAndRulesByTypeAndRule(result, projectName, type, null);
		}
		else {
			// 10k results or more, the search for issues must be split up
			// --> split up by individual rules
			List<JsonElement> rules = getAllJavaRules(type);
			for(JsonElement rule : rules) {
				monitor.subTask("Fetching issues from SonarQube server (more than 10k results, splitting up: type=" + type + ", rule " + rules.indexOf(rule) + "/" + rules.size() + ")");
				String ruleKey = rule.getAsJsonObject().get("key").getAsString();
				addIssuesAndRulesByTypeAndRule(result, projectName, type, ruleKey);
			}
		}
	}
	
	private void addIssuesAndRulesByTypeAndRule(JsonObject result, String projectName, String type, String ruleKey) throws IOException, InterruptedException {
		JsonElement testJson = doGetIssues(projectName, type, ruleKey, 1);
		int numberOfElements = testJson.getAsJsonObject().get("paging").getAsJsonObject().get("total").getAsInt();
		if(numberOfElements >= 10000) {
			ISAEventSystem.fireWarningMessage("Quality Analysis incompletable",
					"SonarQube returned more than 10k elements for following query:\n\n"
					+ "project name: " + projectName + "\n"
					+ "issue type: " + type + "\n"
					+ "rule key: " + ruleKey + "\n\n"
					+ "However, the system you are current analyzing has more quality issues than that. "
					+ "As a result, the quality model that is created right now will not contain all "
					+ "quality issues.");
			numberOfElements = 9999;
		}
		
		int numberOfPages = 1 + numberOfElements / 500;
		for(int i=1; i<=numberOfPages; i++) {
			JsonElement json = doGetIssues(projectName, type, ruleKey, i);
			
			for(JsonElement entry : json.getAsJsonObject().get("issues").getAsJsonArray())
				result.get("issues").getAsJsonArray().add(entry);
			
			for(JsonElement entry : json.getAsJsonObject().get("rules").getAsJsonArray())
				if(!result.get("rules").getAsJsonArray().contains(entry))
					result.get("rules").getAsJsonArray().add(entry);
		}
	}
	
	private JsonElement doGetIssues(String projectName, String types, String ruleKey, int page) throws IOException, InterruptedException {
		List<String> parameters = new ArrayList<>();
		parameters.add("componentKeys="+projectName);
		parameters.add("types="+types);
		parameters.add("additionalFields=_all");
		if(ruleKey != null)
			parameters.add("rules="+ruleKey);
		parameters.add("ps=500");
		parameters.add("p="+page);
		
		return sendRequest("GET", "issues/search", parameters.toArray(new String[] {}));
	}
	
	
	
	public List<JsonElement> getAllJavaRules(String types) throws IOException, InterruptedException {
		JsonObject firstJson = getJavaRules(types, 1).getAsJsonObject();
		int numberOfElements = firstJson.getAsJsonObject().get("total").getAsInt();
		int numberOfPages = 1 + numberOfElements / 500;
		
		List<JsonElement> result = new ArrayList<>(numberOfElements);
		for(int i=1; i<=numberOfPages; i++) {
			JsonElement json = getJavaRules(types, i);
			for(JsonElement entry : json.getAsJsonObject().get("rules").getAsJsonArray()) {
				result.add(entry);
			}
		}
		
		return result;
	}
	
	private JsonElement getJavaRules(String types, int pageNumber) throws IOException, InterruptedException {
		return sendRequest("GET", "rules/search",
				"types="+types,
				"languages=java",
				"ps=500", // entries per page
				"p="+1 // page number
			);
	}
	
	
	
	
	
	private JsonElement sendRequest(String method, String apiURL, String ... parameters) throws IOException, InterruptedException {
//		JsonObject requestBodyJson = new JsonObject();
//		requestBodyJson.addProperty("name", "temporary token for ISA quality analysis");
//		String requestBody = requestBodyJson.toString();
		
		String parametersCombined = "";
		
		if(parameters.length > 0) {
			List<String> parametersEncoded = new ArrayList<>(parameters.length);
			for(String parameter : parameters) {
				String key = parameter.substring(0, parameter.indexOf('='));
				String value = parameter.substring(parameter.indexOf('=')+1, parameter.length());
				parametersEncoded.add(key +"="+ URLEncoder.encode(value, "UTF-8"));
			}
			
			parametersCombined = "?" + String.join("&", parametersEncoded);
		}
		
		String login = ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_QUBE_LOGIN, null);
		if(login == null) {
			(new ISAPreferencesSonarQube()).initializeDefaultPreferences();
			login = ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_QUBE_LOGIN, "admin");
		}
		String password = ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_QUBE_PASSWORD, "admin");
		
		Builder requestBuilder = HttpRequest.newBuilder()
				.uri(URI.create("http://localhost:9000/api/"+ apiURL + parametersCombined))
				.header("content-type", "application/x-www-form-urlencoded")
				.header("Authorization", "Basic " + Base64.getEncoder().encodeToString((login+":"+password).getBytes()));
        
		HttpRequest request;
		if(method.equals("POST")) {
			request = requestBuilder.POST(HttpRequest.BodyPublishers.noBody()).build();
//									.POST(HttpRequest.BodyPublishers.ofString(requestBody))
		}
		else {
			request = requestBuilder.build();
		}
		
		HttpResponse<String> response;
		try {
			response = client.send(request, HttpResponse.BodyHandlers.ofString());
		}
		catch(ConnectException e) {
			throw new ConnectException("Could not establish a connection to the SonarQube Server (is the server started?): " + e.getMessage());
		}
		
		if(response.statusCode() >= 400) {
			throw new SonarQubeServerError("Bad response from SonarQube server: " + response.statusCode()
						+ "\n\n" + response.body());
		}
		return JsonParser.parseString(response.body());
	}

}

















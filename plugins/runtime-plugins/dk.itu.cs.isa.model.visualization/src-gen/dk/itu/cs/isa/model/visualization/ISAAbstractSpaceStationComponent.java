/**
 */
package dk.itu.cs.isa.model.visualization;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Abstract Space Station Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getRelationsToSpaceStationCenters <em>Relations To Space Station Centers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getRelationsToBlocks <em>Relations To Blocks</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getRelationsToAntennas <em>Relations To Antennas</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAAbstractSpaceStationComponent()
 * @model abstract="true"
 * @generated
 */
public interface ISAAbstractSpaceStationComponent extends ISANamedVisualElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(ISASpaceStationRelation)
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAAbstractSpaceStationComponent_Type()
	 * @model containment="true"
	 * @generated
	 */
	ISASpaceStationRelation getType();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ISASpaceStationRelation value);

	/**
	 * Returns the value of the '<em><b>Relations To Space Station Centers</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISASpaceStationRelation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relations To Space Station Centers</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAAbstractSpaceStationComponent_RelationsToSpaceStationCenters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISASpaceStationRelation> getRelationsToSpaceStationCenters();

	/**
	 * Returns the value of the '<em><b>Relations To Blocks</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISABlockRelation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relations To Blocks</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAAbstractSpaceStationComponent_RelationsToBlocks()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISABlockRelation> getRelationsToBlocks();

	/**
	 * Returns the value of the '<em><b>Relations To Antennas</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.visualization.ISAAntennaRelation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relations To Antennas</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage#getISAAbstractSpaceStationComponent_RelationsToAntennas()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAAntennaRelation> getRelationsToAntennas();

} // ISAAbstractSpaceStationComponent

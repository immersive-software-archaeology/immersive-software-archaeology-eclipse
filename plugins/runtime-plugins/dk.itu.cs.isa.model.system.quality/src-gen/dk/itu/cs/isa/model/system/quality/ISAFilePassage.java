/**
 */
package dk.itu.cs.isa.model.system.quality;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA File Passage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getStartLine <em>Start Line</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getEndLine <em>End Line</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getStartChar <em>Start Char</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getEndChar <em>End Char</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAFilePassage()
 * @model
 * @generated
 */
public interface ISAFilePassage extends EObject {
	/**
	 * Returns the value of the '<em><b>Start Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Line</em>' attribute.
	 * @see #setStartLine(int)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAFilePassage_StartLine()
	 * @model required="true"
	 * @generated
	 */
	int getStartLine();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getStartLine <em>Start Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Line</em>' attribute.
	 * @see #getStartLine()
	 * @generated
	 */
	void setStartLine(int value);

	/**
	 * Returns the value of the '<em><b>End Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Line</em>' attribute.
	 * @see #setEndLine(int)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAFilePassage_EndLine()
	 * @model required="true"
	 * @generated
	 */
	int getEndLine();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getEndLine <em>End Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Line</em>' attribute.
	 * @see #getEndLine()
	 * @generated
	 */
	void setEndLine(int value);

	/**
	 * Returns the value of the '<em><b>Start Char</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Char</em>' attribute.
	 * @see #setStartChar(int)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAFilePassage_StartChar()
	 * @model required="true"
	 * @generated
	 */
	int getStartChar();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getStartChar <em>Start Char</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Char</em>' attribute.
	 * @see #getStartChar()
	 * @generated
	 */
	void setStartChar(int value);

	/**
	 * Returns the value of the '<em><b>End Char</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Char</em>' attribute.
	 * @see #setEndChar(int)
	 * @see dk.itu.cs.isa.model.system.quality.QualityPackage#getISAFilePassage_EndChar()
	 * @model required="true"
	 * @generated
	 */
	int getEndChar();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.system.quality.ISAFilePassage#getEndChar <em>End Char</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Char</em>' attribute.
	 * @see #getEndChar()
	 * @generated
	 */
	void setEndChar(int value);

} // ISAFilePassage

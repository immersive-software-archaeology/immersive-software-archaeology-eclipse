package dk.itu.cs.isa.model.system.coalescence.ui;

import org.eclipse.core.resources.IFile;

import dk.itu.cs.isa.coalescence.server.extensions.ExtensionPointHandler;
import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.coalescence.server.ui.views.ISACoalescenceServerStatusView;
import dk.itu.cs.isa.eclipse.ui.activation.GUIActivationListener;
import dk.itu.cs.isa.model.system.coalescence.openfile.OpenFileListener;
import dk.itu.cs.isa.model.system.coalescence.openfile.OpenFileRequestHandler;

public class OpenFileTrigger implements GUIActivationListener, OpenFileListener {

	@Override
	public boolean openFileInEditor(IFile file) {
		// just forward the call
		return ISACoalescenceServerStatusView.FILE_OPENER.showFileInEditor(file);
	}

	@Override
	public void start() {
		for(ISAAbstractCoalescenceRequestHandler handler : ExtensionPointHandler.INSTANCE.getRequestHandlers()) {
			if(handler instanceof OpenFileRequestHandler) {
				((OpenFileRequestHandler) handler).registerListener(this);
				return;
			}
		}
	}

	@Override
	public void stop() {
	}

}

package dk.itu.cs.isa.model.system.architecture.hierarchical.divisive.dbscan;

import org.eclipse.core.runtime.IProgressMonitor;

import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.system.architecture.hierarchical.HierarchicalArchitectureAnalyzer;
import dk.itu.cs.isa.model.system.architecture.hierarchical.ISAClusteringPostprocessor;
import dk.itu.cs.isa.model.system.architecture.labeling.ClusterLabelingUtil;
import dk.itu.cs.isa.model.system.architecture.preferences.ISAPreferencesClustering;

public class TopDownDBSCANArchitectureAnalyzer extends HierarchicalArchitectureAnalyzer {
	
	private ISADBSCANClustererManager dbScanClusterManager;
	private ISAClusteringPostprocessor postProcessor;
	
	
	
	@Override
	public void initializeInstance() {
		super.initializeInstance();
	}
	
	@Override
	public void setupBeforeRun() {
		super.setupBeforeRun();
		dbScanClusterManager = new ISADBSCANClustererManager(initialSingletonClusters, stats);
		postProcessor = new ISAClusteringPostprocessor();
	}

	@Override
	public void cleanUpAfterRun() {
		super.cleanUpAfterRun();
		dbScanClusterManager = null;
		postProcessor = null;
	}
	
	
	
	@Override
	public void run(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) throws Exception {
		super.run(isaSystem, monitor);

		ISAPreferencesClustering.writeMetaInformation(isaSystem);
		ISACluster rootCluster = dbScanClusterManager.startClustering(monitor);
		isaSystem.setRootCluster(rootCluster);
		
		postProcessor.process(isaSystem, monitor);
		
		try {
			ClusterLabelingUtil labeler = new ClusterLabelingUtil();
			labeler.label(isaSystem.getRootCluster());
		} catch(Exception e) {
			ISAEventSystem.fireErrorMessage("Labeling clusters failed", e);
		}
	}
	
	

	@SuppressWarnings("unused")
	private ISACluster searchClustersForClassifier(ISACluster current, String classifierName) {
		for(ISAClassifier classifier : current.getClassifiers()) {
			if(classifier.getName().contains(classifierName)) {
				return current;
			}
		}
		for(ISACluster child : current.getChildClusters()) {
			ISACluster result = searchClustersForClassifier(child, classifierName);
			if(result != null)
				return result;
		}
		return null;
	}

}

/**
 */
package dk.itu.cs.isa.multiplayer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Show Relation Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent#getElementName <em>Element Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent#getRelationType <em>Relation Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent#getDirection <em>Direction</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getShowRelationEvent()
 * @model
 * @generated
 */
public interface ShowRelationEvent extends AbstractEvent {
	/**
	 * Returns the value of the '<em><b>Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Name</em>' attribute.
	 * @see #setElementName(String)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getShowRelationEvent_ElementName()
	 * @model required="true"
	 * @generated
	 */
	String getElementName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent#getElementName <em>Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Name</em>' attribute.
	 * @see #getElementName()
	 * @generated
	 */
	void setElementName(String value);

	/**
	 * Returns the value of the '<em><b>Relation Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.itu.cs.isa.multiplayer.RelationType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relation Type</em>' attribute.
	 * @see dk.itu.cs.isa.multiplayer.RelationType
	 * @see #setRelationType(RelationType)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getShowRelationEvent_RelationType()
	 * @model required="true"
	 * @generated
	 */
	RelationType getRelationType();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent#getRelationType <em>Relation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relation Type</em>' attribute.
	 * @see dk.itu.cs.isa.multiplayer.RelationType
	 * @see #getRelationType()
	 * @generated
	 */
	void setRelationType(RelationType value);

	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link dk.itu.cs.isa.multiplayer.Direction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see dk.itu.cs.isa.multiplayer.Direction
	 * @see #setDirection(Direction)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getShowRelationEvent_Direction()
	 * @model required="true"
	 * @generated
	 */
	Direction getDirection();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.ShowRelationEvent#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see dk.itu.cs.isa.multiplayer.Direction
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(Direction value);

} // ShowRelationEvent

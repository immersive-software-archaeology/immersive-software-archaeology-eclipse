package dk.itu.cs.isa.model.system.creation.java.visitors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.AssertStatement;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.ContinueStatement;
import org.eclipse.jdt.core.dom.CreationReference;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EmptyStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.ExpressionMethodReference;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.LabeledStatement;
import org.eclipse.jdt.core.dom.LambdaExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.SuperMethodReference;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchExpression;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.SynchronizedStatement;
import org.eclipse.jdt.core.dom.ThisExpression;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.TypeDeclarationStatement;
import org.eclipse.jdt.core.dom.TypeLiteral;
import org.eclipse.jdt.core.dom.TypeMethodReference;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

public final class CountVisitor extends ASTVisitor {

	private int stmtCount = 0;
	private int exprCount = 0;
	
	

	public int getStatementCount() {
		return stmtCount;
	}
	
	public int getExpressionsCount() {
		return exprCount;
	}
	
	
	
	// EXPRESSIONS
	
	
	
	@Override
	public boolean visit(ArrayAccess node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ArrayCreation node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ArrayInitializer node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(Assignment node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(BooleanLiteral node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(CastExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(CharacterLiteral node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ConditionalExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(CreationReference node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ExpressionMethodReference node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(FieldAccess node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(InfixExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(InstanceofExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(LambdaExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(MethodInvocation node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(SimpleName node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(QualifiedName node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(NullLiteral node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(NumberLiteral node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ParenthesizedExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(PostfixExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(PrefixExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(StringLiteral node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperFieldAccess node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperMethodInvocation node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperMethodReference node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(SwitchExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ThisExpression node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(TypeLiteral node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(TypeMethodReference node) {
		exprCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(VariableDeclarationExpression node) {
		exprCount++;
		return super.visit(node);
	}
	
	
	
	// STATEMENTS
	


	@Override
	public boolean visit(AssertStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(Block node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(BreakStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ConstructorInvocation node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ContinueStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(DoStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(EmptyStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(EnhancedForStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ExpressionStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ForStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(IfStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(LabeledStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ReturnStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperConstructorInvocation node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(SwitchCase node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(SwitchStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(SynchronizedStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(ThrowStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(TryStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(TypeDeclarationStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(VariableDeclarationStatement node) {
		stmtCount++;
		return super.visit(node);
	}

	@Override
	public boolean visit(WhileStatement node) {
		stmtCount++;
		return super.visit(node);
	}

}
package dk.itu.cs.isa.coalescence.server.extensions;

public interface ISACoalescenceServerObserver {

	public void notifyServerStart();
	public void notifyServerStop();

}
 
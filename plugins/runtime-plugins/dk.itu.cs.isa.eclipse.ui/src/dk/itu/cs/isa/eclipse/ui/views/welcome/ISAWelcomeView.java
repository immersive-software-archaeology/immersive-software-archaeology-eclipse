package dk.itu.cs.isa.eclipse.ui.views.welcome;

import java.util.List;

import dk.itu.cs.isa.eclipse.ui.views.ISAHTMLView;
import dk.itu.cs.isa.model.examples.ExampleSystemPopulator;
import dk.itu.cs.isa.util.FileUtil;

public class ISAWelcomeView extends ISAHTMLView {

	@Override
	protected String getHTMLCode() {
		try {
			List<String> htmlLines = FileUtil.readContentFromPluginFile("dk.itu.cs.isa.eclipse.ui", "html/welcome/index.html");
			
			for(int i=0; i<htmlLines.size(); i++)
				if(htmlLines.get(i).contains("ISA-PLACEHOLDER-MODEL-LIST"))
					htmlLines.addAll(i+1, ExampleSystemPopulator.INSTANCE.getExampleSystemsInfoAsHTMLCode());
			
			return String.join(System.lineSeparator(), htmlLines);
		} catch (Exception e) {
			return "Welcome page could not initialize: " + e.getMessage();
		}
	}
	
}

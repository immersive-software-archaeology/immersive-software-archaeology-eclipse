/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassifiersFactoryImpl extends EFactoryImpl implements ClassifiersFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ClassifiersFactory init() {
		try {
			ClassifiersFactory theClassifiersFactory = (ClassifiersFactory)EPackage.Registry.INSTANCE.getEFactory(ClassifiersPackage.eNS_URI);
			if (theClassifiersFactory != null) {
				return theClassifiersFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ClassifiersFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifiersFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ClassifiersPackage.ISA_ABSTRACT_CLASS: return createISAAbstractClass();
			case ClassifiersPackage.ISA_CONCRETE_CLASS: return createISAConcreteClass();
			case ClassifiersPackage.ISA_INTERFACE: return createISAInterface();
			case ClassifiersPackage.ISA_ENUM: return createISAEnum();
			case ClassifiersPackage.ISA_RECORD: return createISARecord();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAAbstractClass createISAAbstractClass() {
		ISAAbstractClassImpl isaAbstractClass = new ISAAbstractClassImpl();
		return isaAbstractClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAConcreteClass createISAConcreteClass() {
		ISAConcreteClassImpl isaConcreteClass = new ISAConcreteClassImpl();
		return isaConcreteClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAInterface createISAInterface() {
		ISAInterfaceImpl isaInterface = new ISAInterfaceImpl();
		return isaInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAEnum createISAEnum() {
		ISAEnumImpl isaEnum = new ISAEnumImpl();
		return isaEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISARecord createISARecord() {
		ISARecordImpl isaRecord = new ISARecordImpl();
		return isaRecord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ClassifiersPackage getClassifiersPackage() {
		return (ClassifiersPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ClassifiersPackage getPackage() {
		return ClassifiersPackage.eINSTANCE;
	}

} //ClassifiersFactoryImpl

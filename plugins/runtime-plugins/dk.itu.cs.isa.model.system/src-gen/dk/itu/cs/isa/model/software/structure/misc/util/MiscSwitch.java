/**
 */
package dk.itu.cs.isa.model.software.structure.misc.util;

import dk.itu.cs.isa.model.software.structure.misc.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage
 * @generated
 */
public class MiscSwitch<T1> extends Switch<T1> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MiscPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiscSwitch() {
		if (modelPackage == null) {
			modelPackage = MiscPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T1 doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT: {
				ISANamedSoftwareElement isaNamedSoftwareElement = (ISANamedSoftwareElement)theEObject;
				T1 result = caseISANamedSoftwareElement(isaNamedSoftwareElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MiscPackage.ISA_TYPED_ELEMENT: {
				ISATypedElement isaTypedElement = (ISATypedElement)theEObject;
				T1 result = caseISATypedElement(isaTypedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MiscPackage.ISA_TYPE: {
				ISAType isaType = (ISAType)theEObject;
				T1 result = caseISAType(isaType);
				if (result == null) result = caseISAReferenceableElement(isaType);
				if (result == null) result = caseISANamedSoftwareElement(isaType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MiscPackage.ISA_MODIFIABLE_ELEMENT: {
				ISAModifiableElement isaModifiableElement = (ISAModifiableElement)theEObject;
				T1 result = caseISAModifiableElement(isaModifiableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MiscPackage.ISA_REFERENCING_ELEMENT: {
				ISAReferencingElement isaReferencingElement = (ISAReferencingElement)theEObject;
				T1 result = caseISAReferencingElement(isaReferencingElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MiscPackage.ISA_REFERENCE: {
				ISAReference<?> isaReference = (ISAReference<?>)theEObject;
				T1 result = caseISAReference(isaReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MiscPackage.ISA_REFERENCEABLE_ELEMENT: {
				ISAReferenceableElement isaReferenceableElement = (ISAReferenceableElement)theEObject;
				T1 result = caseISAReferenceableElement(isaReferenceableElement);
				if (result == null) result = caseISANamedSoftwareElement(isaReferenceableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MiscPackage.ISA_STATEMENT_CONTAINER: {
				ISAStatementContainer isaStatementContainer = (ISAStatementContainer)theEObject;
				T1 result = caseISAStatementContainer(isaStatementContainer);
				if (result == null) result = caseISAReferencingElement(isaStatementContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MiscPackage.ISA_PARAMETRIZABLE_ELEMENT: {
				ISAParametrizableElement isaParametrizableElement = (ISAParametrizableElement)theEObject;
				T1 result = caseISAParametrizableElement(isaParametrizableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MiscPackage.ISA_PARAMETER: {
				ISAParameter isaParameter = (ISAParameter)theEObject;
				T1 result = caseISAParameter(isaParameter);
				if (result == null) result = caseISANamedSoftwareElement(isaParameter);
				if (result == null) result = caseISATypedElement(isaParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Named Software Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Named Software Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseISANamedSoftwareElement(ISANamedSoftwareElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Typed Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Typed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseISATypedElement(ISATypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseISAType(ISAType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Modifiable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Modifiable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseISAModifiableElement(ISAModifiableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Referencing Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Referencing Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseISAReferencingElement(ISAReferencingElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T extends ISAReferenceableElement> T1 caseISAReference(ISAReference<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Referenceable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Referenceable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseISAReferenceableElement(ISAReferenceableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Statement Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Statement Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseISAStatementContainer(ISAStatementContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Parametrizable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Parametrizable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseISAParametrizableElement(ISAParametrizableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseISAParameter(ISAParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T1 defaultCase(EObject object) {
		return null;
	}

} //MiscSwitch

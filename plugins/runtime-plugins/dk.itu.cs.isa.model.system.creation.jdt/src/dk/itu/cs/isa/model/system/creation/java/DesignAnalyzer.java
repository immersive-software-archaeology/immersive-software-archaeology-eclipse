package dk.itu.cs.isa.model.system.creation.java;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;

import dk.itu.cs.isa.analyzer.exceptions.UserAbortException;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemFactory;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;

public class DesignAnalyzer {

	private final FilesystemFactory factory = FilesystemFactory.eINSTANCE;
	
	private ClassifierAnalyzer classifierAnalyzer;
	private ReferenceHandler referenceHandler;
	
	
	
	public DesignAnalyzer() {
		referenceHandler = new ReferenceHandler();
		classifierAnalyzer = new ClassifierAnalyzer(referenceHandler);
	}



	public void clear() {
		this.classifierAnalyzer = null;
		this.referenceHandler = null;
	}
	
	
	
	public void analyzeJavaProject(ISASoftwareSystemModel isaSystem, IJavaProject jdtProject, IProgressMonitor monitor) throws CoreException, IOException, ClassNotFoundException, UserAbortException {
		ISAProject isaProject = factory.createISAProject();
		isaProject.setName(jdtProject.getElementName());
		isaSystem.getProjects().add(isaProject);
		
//		String[] classPathEntries = JavaRuntime.computeDefaultRuntimeClassPath(jdtProject);
//
//		List<URL> urlList = new ArrayList<URL>();
//		for (int i = 0; i < classPathEntries.length; i++) {
//			String entry = classPathEntries[i];
//			IPath path = new Path(entry);
//			URL url = path.toFile().toURI().toURL();
//			urlList.add(url);
//		}
//
//		ClassLoader parentClassLoader = jdtProject.getClass().getClassLoader();
//		URL[] urls = (URL[]) urlList.toArray(new URL[urlList.size()]);
//		URLClassLoader classLoader = new URLClassLoader(urls, parentClassLoader);
		
		// List of source (non-byte-code) package roots
		List<IPackageFragmentRoot> sourcePackageRoots = new LinkedList<>();
		int sourcePackageRootsChildrenCount = 0;
		for (IPackageFragmentRoot jdtPackageRoot : jdtProject.getPackageFragmentRoots()) {
			if(jdtPackageRoot.getKind() == IPackageFragmentRoot.K_SOURCE) {
				sourcePackageRoots.add(jdtPackageRoot);
				sourcePackageRootsChildrenCount += jdtPackageRoot.getChildren().length;
			}
		}
		
		monitor.beginTask("Analyzing project \"" + jdtProject.getElementName() + "\"", sourcePackageRootsChildrenCount);
		
		// dont use .getAllPackageFragmentRoots(), as that method returns entries from all projects!
		for (IPackageFragmentRoot jdtSourcePackageRoot : sourcePackageRoots) {
			analyzePackageRoot(isaProject, jdtSourcePackageRoot, monitor);
		}
		
		SystemModelCleaningUtil.removeEmptyPackages(isaProject);
//		classLoader.close();
	}
	
	public void resolveAndRegisterReferences(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) throws Exception {
		referenceHandler.resolveReferences(monitor);
		classifierAnalyzer.resolveSuperClassifiers(isaSystem, monitor);
		
		monitor.beginTask("Settings method and constructor names with correct signatures", IProgressMonitor.UNKNOWN);
		classifierAnalyzer.setMethodAndConstructorNamesWithResolvedSignatures(isaSystem, monitor);
	}

	
	
	/**
	 * This method analyzes a JDT package root and create a corresponding entry in the ISA model.
	 * A package root can be a source folder, a .jar file, or an entry in the Java runtime library.
	 * @param monitor 
	 * @throws UserAbortException 
	 */
	private void analyzePackageRoot(ISAProject isaProject, IPackageFragmentRoot jdtPackageRoot, IProgressMonitor monitor) throws JavaModelException, ClassNotFoundException, UserAbortException {
		ISAPackageRoot isaPackageRoot = factory.createISAPackageRoot();
		isaPackageRoot.setName(jdtPackageRoot.getPath().removeFirstSegments(1).toString());
		isaPackageRoot.setParentProject(isaProject);
		
		// TODO: Do something with this value?
		//boolean containedInOtherProject = (!jdtPackageRoot.getJavaProject().getElementName().equals(isaProject.getName()));
		
		// either IPackageFragmentRoot.K_BINARY
		// or IPackageFragmentRoot.K_SOURCE
		isaPackageRoot.setBinary(jdtPackageRoot.getKind() == IPackageFragmentRoot.K_BINARY);
		
		for (IJavaElement child : jdtPackageRoot.getChildren()) {
			monitor.subTask("Analyzing package \"" + child.getElementName() + "\"");
			analyzePackage(isaPackageRoot, (IPackageFragment) child);
			
			monitor.worked(1);
			if(monitor.isCanceled())
				throw new UserAbortException();
		}
	}
	
	

	private void analyzePackage(ISAPackageRoot isaPackageRoot, IPackageFragment jdtPackage) throws JavaModelException, ClassNotFoundException {
		if(jdtPackage.getChildren().length == 0)
			// no need to analyze a package that has no containment
			// e.g., JDT lists every default package for some reason
			return;

		// name is the fully qualified package name
		String[] nameSegments = jdtPackage.getElementName().split("\\.");
		
		ISAPackageContainer isaPackage = isaPackageRoot;
		
		// We must find or otherwise create the correct package nested in our model hierarchy
		for(int i=0; i<nameSegments.length; i++) {
			ISAPackage currentPackage = null;
			
			// Try to find the package - it might have been created by analyzing another package root!
			for(ISAPackage subPackage : isaPackage.getSubPackages()) {
				if(subPackage.getName().equals(nameSegments[i])) {
					currentPackage = subPackage;
					break;
				}
			}
			
			if(currentPackage == null) {
				// The package was not yet created, do that before proceeding
				currentPackage = factory.createISAPackage();
				currentPackage.setParentContainer(isaPackage);
				currentPackage.setName(nameSegments[i]);
			}
			
			isaPackage = currentPackage;
		}
		if(!(isaPackage instanceof ISAPackage))
			throw new RuntimeException("System Analyzer was not able to create or find a package for " + jdtPackage.getElementName());
		
		for (IJavaElement child : jdtPackage.getChildren()) {
			//analyzePackageFragment(isaPackage, child, classLoader);
			if(child.getElementType() == IJavaElement.COMPILATION_UNIT) {
				classifierAnalyzer.analyzeCompilationUnit((ISAPackage) isaPackage, (ICompilationUnit) child);
			}
			else if(child.getElementType() == IJavaElement.CLASS_FILE) {
				// TODO analyze byte code! 
				classifierAnalyzer.analyzeClassFile((ISAPackage) isaPackage, (IClassFile) child);
			}
			else {
				throw new RuntimeException("A child of a package was neither a binary class file nor a source compilation unit... found IJavaElement.type: " + child.getElementType());
			}
		}
	}

}











/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers;

import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;

import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Classifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getFields <em>Fields</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getMethods <em>Methods</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getFilePath <em>File Path</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClassifier()
 * @model abstract="true"
 * @generated
 */
public interface ISAClassifier extends ISAType, ISAClassifierContainer, ISAMember {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer#getClassifiers <em>Classifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(ISAClassifierContainer)
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClassifier_Parent()
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer#getClassifiers
	 * @model opposite="classifiers" required="true" transient="false"
	 * @generated
	 */
	ISAClassifierContainer getParent();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(ISAClassifierContainer value);

	/**
	 * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.members.ISAField}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClassifier_Fields()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAField> getFields();

	/**
	 * Returns the value of the '<em><b>Methods</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.members.ISAMethod}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Methods</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClassifier_Methods()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAMethod> getMethods();

	/**
	 * Returns the value of the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File Path</em>' attribute.
	 * @see #setFilePath(String)
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage#getISAClassifier_FilePath()
	 * @model
	 * @generated
	 */
	String getFilePath();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier#getFilePath <em>File Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File Path</em>' attribute.
	 * @see #getFilePath()
	 * @generated
	 */
	void setFilePath(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 * @generated
	 */
	ISAPackage getContainingPackage();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<ISAMember> getContainedMembers();

} // ISAClassifier

/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage
 * @generated
 */
public interface WhiteboardFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WhiteboardFactory eINSTANCE = dk.itu.cs.isa.whiteboard.impl.WhiteboardFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>History</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>History</em>'.
	 * @generated
	 */
	WhiteboardHistory createWhiteboardHistory();

	/**
	 * Returns a new object of class '<em>Whiteboard</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Whiteboard</em>'.
	 * @generated
	 */
	Whiteboard createWhiteboard();

	/**
	 * Returns a new object of class '<em>Pin</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pin</em>'.
	 * @generated
	 */
	Pin createPin();

	/**
	 * Returns a new object of class '<em>Photo</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Photo</em>'.
	 * @generated
	 */
	Photo createPhoto();

	/**
	 * Returns a new object of class '<em>ISA Vector3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Vector3</em>'.
	 * @generated
	 */
	ISAVector3 createISAVector3();

	/**
	 * Returns a new object of class '<em>ISA Vector2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Vector2</em>'.
	 * @generated
	 */
	ISAVector2 createISAVector2();

	/**
	 * Returns a new object of class '<em>ISA Color</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Color</em>'.
	 * @generated
	 */
	ISAColor createISAColor();

	/**
	 * Returns a new object of class '<em>Abstract Whiteboard Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Whiteboard Event</em>'.
	 * @generated
	 */
	AbstractWhiteboardEvent createAbstractWhiteboardEvent();

	/**
	 * Returns a new object of class '<em>Board Spawn Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Board Spawn Event</em>'.
	 * @generated
	 */
	BoardSpawnEvent createBoardSpawnEvent();

	/**
	 * Returns a new object of class '<em>Board Duplicate Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Board Duplicate Event</em>'.
	 * @generated
	 */
	BoardDuplicateEvent createBoardDuplicateEvent();

	/**
	 * Returns a new object of class '<em>Board Destroy Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Board Destroy Event</em>'.
	 * @generated
	 */
	BoardDestroyEvent createBoardDestroyEvent();

	/**
	 * Returns a new object of class '<em>Undo Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Undo Event</em>'.
	 * @generated
	 */
	UndoEvent createUndoEvent();

	/**
	 * Returns a new object of class '<em>Redo Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Redo Event</em>'.
	 * @generated
	 */
	RedoEvent createRedoEvent();

	/**
	 * Returns a new object of class '<em>Pin Split Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pin Split Operation</em>'.
	 * @generated
	 */
	PinSplitOperation createPinSplitOperation();

	/**
	 * Returns a new object of class '<em>Pins Remove All Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pins Remove All Operation</em>'.
	 * @generated
	 */
	PinsRemoveAllOperation createPinsRemoveAllOperation();

	/**
	 * Returns a new object of class '<em>Pin Software Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pin Software Add Operation</em>'.
	 * @generated
	 */
	PinSoftwareAddOperation createPinSoftwareAddOperation();

	/**
	 * Returns a new object of class '<em>Pin Audio Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pin Audio Add Operation</em>'.
	 * @generated
	 */
	PinAudioAddOperation createPinAudioAddOperation();

	/**
	 * Returns a new object of class '<em>Pin Remove Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pin Remove Operation</em>'.
	 * @generated
	 */
	PinRemoveOperation createPinRemoveOperation();

	/**
	 * Returns a new object of class '<em>Pin Move Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pin Move Operation</em>'.
	 * @generated
	 */
	PinMoveOperation createPinMoveOperation();

	/**
	 * Returns a new object of class '<em>Draw Freehand Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Draw Freehand Operation</em>'.
	 * @generated
	 */
	DrawFreehandOperation createDrawFreehandOperation();

	/**
	 * Returns a new object of class '<em>Draw Module Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Draw Module Operation</em>'.
	 * @generated
	 */
	DrawModuleOperation createDrawModuleOperation();

	/**
	 * Returns a new object of class '<em>Draw Arrow Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Draw Arrow Operation</em>'.
	 * @generated
	 */
	DrawArrowOperation createDrawArrowOperation();

	/**
	 * Returns a new object of class '<em>Paint Clear All Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Paint Clear All Operation</em>'.
	 * @generated
	 */
	PaintClearAllOperation createPaintClearAllOperation();

	/**
	 * Returns a new object of class '<em>Paint Erase Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Paint Erase Operation</em>'.
	 * @generated
	 */
	PaintEraseOperation createPaintEraseOperation();

	/**
	 * Returns a new object of class '<em>Board Enlarge Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Board Enlarge Operation</em>'.
	 * @generated
	 */
	BoardEnlargeOperation createBoardEnlargeOperation();

	/**
	 * Returns a new object of class '<em>Photo Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Photo Add Operation</em>'.
	 * @generated
	 */
	PhotoAddOperation createPhotoAddOperation();

	/**
	 * Returns a new object of class '<em>Photo Move Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Photo Move Operation</em>'.
	 * @generated
	 */
	PhotoMoveOperation createPhotoMoveOperation();

	/**
	 * Returns a new object of class '<em>Photo Remove Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Photo Remove Operation</em>'.
	 * @generated
	 */
	PhotoRemoveOperation createPhotoRemoveOperation();

	/**
	 * Returns a new object of class '<em>Visible Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Visible Relation</em>'.
	 * @generated
	 */
	VisibleRelation createVisibleRelation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WhiteboardPackage getWhiteboardPackage();

} //WhiteboardFactory

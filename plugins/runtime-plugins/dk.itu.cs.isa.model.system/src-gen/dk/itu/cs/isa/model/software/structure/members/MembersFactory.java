/**
 */
package dk.itu.cs.isa.model.software.structure.members;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage
 * @generated
 */
public interface MembersFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MembersFactory eINSTANCE = dk.itu.cs.isa.model.software.structure.members.impl.MembersFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ISA Member</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Member</em>'.
	 * @generated
	 */
	ISAMember createISAMember();

	/**
	 * Returns a new object of class '<em>ISA Enum Constant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Enum Constant</em>'.
	 * @generated
	 */
	ISAEnumConstant createISAEnumConstant();

	/**
	 * Returns a new object of class '<em>ISA Record Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Record Component</em>'.
	 * @generated
	 */
	ISARecordComponent createISARecordComponent();

	/**
	 * Returns a new object of class '<em>ISA Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Field</em>'.
	 * @generated
	 */
	ISAField createISAField();

	/**
	 * Returns a new object of class '<em>ISA Constructor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Constructor</em>'.
	 * @generated
	 */
	ISAConstructor createISAConstructor();

	/**
	 * Returns a new object of class '<em>ISA Concrete Method</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Concrete Method</em>'.
	 * @generated
	 */
	ISAConcreteMethod createISAConcreteMethod();

	/**
	 * Returns a new object of class '<em>ISA Abstract Method</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Abstract Method</em>'.
	 * @generated
	 */
	ISAAbstractMethod createISAAbstractMethod();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MembersPackage getMembersPackage();

} //MembersFactory

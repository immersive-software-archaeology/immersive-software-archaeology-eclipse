/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Head Info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getHeadInfo()
 * @model
 * @generated
 */
public interface HeadInfo extends BodyPartInfo {
} // HeadInfo

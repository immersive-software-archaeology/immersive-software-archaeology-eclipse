package dk.itu.cs.isa.model.software.structure.impl;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;

public class StructureFactoryImplCustom extends StructureFactoryImpl {

	@Override
	public ISASoftwareSystemModel createISASoftwareSystemModel() {
		return new ISASoftwareSystemModelImplCustom();
	}

}

/**
 */
package dk.itu.cs.isa.model.software.structure.impl;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.StructureFactory;
import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage;

import dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;

import dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;

import dk.itu.cs.isa.model.software.structure.filesystem.impl.FilesystemPackageImpl;

import dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage;

import dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl;

import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StructurePackageImpl extends EPackageImpl implements StructurePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaSoftwareSystemModelEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.software.structure.StructurePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StructurePackageImpl() {
		super(eNS_URI, StructureFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link StructurePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StructurePackage init() {
		if (isInited) return (StructurePackage)EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredStructurePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		StructurePackageImpl theStructurePackage = registeredStructurePackage instanceof StructurePackageImpl ? (StructurePackageImpl)registeredStructurePackage : new StructurePackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);
		ArchitecturePackageImpl theArchitecturePackage = (ArchitecturePackageImpl)(registeredPackage instanceof ArchitecturePackageImpl ? registeredPackage : ArchitecturePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(FilesystemPackage.eNS_URI);
		FilesystemPackageImpl theFilesystemPackage = (FilesystemPackageImpl)(registeredPackage instanceof FilesystemPackageImpl ? registeredPackage : FilesystemPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LibrariesPackage.eNS_URI);
		LibrariesPackageImpl theLibrariesPackage = (LibrariesPackageImpl)(registeredPackage instanceof LibrariesPackageImpl ? registeredPackage : LibrariesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl)(registeredPackage instanceof ClassifiersPackageImpl ? registeredPackage : ClassifiersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);
		MembersPackageImpl theMembersPackage = (MembersPackageImpl)(registeredPackage instanceof MembersPackageImpl ? registeredPackage : MembersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);
		MiscPackageImpl theMiscPackage = (MiscPackageImpl)(registeredPackage instanceof MiscPackageImpl ? registeredPackage : MiscPackage.eINSTANCE);

		// Create package meta-data objects
		theStructurePackage.createPackageContents();
		theArchitecturePackage.createPackageContents();
		theFilesystemPackage.createPackageContents();
		theLibrariesPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theMembersPackage.createPackageContents();
		theMiscPackage.createPackageContents();

		// Initialize created meta-data
		theStructurePackage.initializePackageContents();
		theArchitecturePackage.initializePackageContents();
		theFilesystemPackage.initializePackageContents();
		theLibrariesPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theMembersPackage.initializePackageContents();
		theMiscPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theStructurePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(StructurePackage.eNS_URI, theStructurePackage);
		return theStructurePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISASoftwareSystemModel() {
		return isaSoftwareSystemModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISASoftwareSystemModel_Projects() {
		return (EReference)isaSoftwareSystemModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISASoftwareSystemModel_RootCluster() {
		return (EReference)isaSoftwareSystemModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISASoftwareSystemModel_Libraries() {
		return (EReference)isaSoftwareSystemModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISASoftwareSystemModel_MetaInfo() {
		return (EAttribute)isaSoftwareSystemModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISASoftwareSystemModel__GetSystemElementByName__String() {
		return isaSoftwareSystemModelEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISASoftwareSystemModel__GetLibraryElementByName__String_int_boolean() {
		return isaSoftwareSystemModelEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StructureFactory getStructureFactory() {
		return (StructureFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaSoftwareSystemModelEClass = createEClass(ISA_SOFTWARE_SYSTEM_MODEL);
		createEReference(isaSoftwareSystemModelEClass, ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS);
		createEReference(isaSoftwareSystemModelEClass, ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER);
		createEReference(isaSoftwareSystemModelEClass, ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES);
		createEAttribute(isaSoftwareSystemModelEClass, ISA_SOFTWARE_SYSTEM_MODEL__META_INFO);
		createEOperation(isaSoftwareSystemModelEClass, ISA_SOFTWARE_SYSTEM_MODEL___GET_SYSTEM_ELEMENT_BY_NAME__STRING);
		createEOperation(isaSoftwareSystemModelEClass, ISA_SOFTWARE_SYSTEM_MODEL___GET_LIBRARY_ELEMENT_BY_NAME__STRING_INT_BOOLEAN);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ArchitecturePackage theArchitecturePackage = (ArchitecturePackage)EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);
		FilesystemPackage theFilesystemPackage = (FilesystemPackage)EPackage.Registry.INSTANCE.getEPackage(FilesystemPackage.eNS_URI);
		LibrariesPackage theLibrariesPackage = (LibrariesPackage)EPackage.Registry.INSTANCE.getEPackage(LibrariesPackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage)EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);
		MembersPackage theMembersPackage = (MembersPackage)EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);
		MiscPackage theMiscPackage = (MiscPackage)EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theArchitecturePackage);
		getESubpackages().add(theFilesystemPackage);
		getESubpackages().add(theLibrariesPackage);
		getESubpackages().add(theClassifiersPackage);
		getESubpackages().add(theMembersPackage);
		getESubpackages().add(theMiscPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		isaSoftwareSystemModelEClass.getESuperTypes().add(theMiscPackage.getISANamedSoftwareElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(isaSoftwareSystemModelEClass, ISASoftwareSystemModel.class, "ISASoftwareSystemModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISASoftwareSystemModel_Projects(), theFilesystemPackage.getISAProject(), theFilesystemPackage.getISAProject_Parent(), "projects", null, 1, -1, ISASoftwareSystemModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISASoftwareSystemModel_RootCluster(), theArchitecturePackage.getISACluster(), null, "rootCluster", null, 1, 1, ISASoftwareSystemModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISASoftwareSystemModel_Libraries(), theLibrariesPackage.getISALibrary(), null, "libraries", null, 0, -1, ISASoftwareSystemModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISASoftwareSystemModel_MetaInfo(), ecorePackage.getEString(), "metaInfo", null, 0, -1, ISASoftwareSystemModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getISASoftwareSystemModel__GetSystemElementByName__String(), theMiscPackage.getISANamedSoftwareElement(), "getSystemElementByName", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "qualifiedName", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getISASoftwareSystemModel__GetLibraryElementByName__String_int_boolean(), theMiscPackage.getISANamedSoftwareElement(), "getLibraryElementByName", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "qualifiedName", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expectedType", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "createOnDemand", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //StructurePackageImpl

package dk.itu.cs.isa.coalescence.server.extensions;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.events.ISAEventSystem;

public class ExtensionPointHandler {
	
	public static final ExtensionPointHandler INSTANCE = new ExtensionPointHandler();

	private static final String EXTENSION_POINT_ID = "dk.itu.cs.isa.coalescence.server.ISACoalescenceServerExtension";
	
	
	
	private ExtensionPointHandler() { }
	
	
	
	private List<ISAAbstractCoalescenceRequestHandler> requestHandlers = new LinkedList<>();
	private List<String> allRegisteredPaths = new LinkedList<>();

	private List<ISACoalescenceServerObserver> serverObservers = new LinkedList<>();
	
	private List<ISAAbstractSocketImplementation> sockets = new LinkedList<>();
	
	
	
	public List<ISAAbstractCoalescenceRequestHandler> getRequestHandlers() {
		return requestHandlers;
	}

	public List<String> getRegisteredPaths() {
		return new LinkedList<>(allRegisteredPaths);
	}

	public List<ISACoalescenceServerObserver> getServerObservers() {
		return serverObservers;
	}

	public List<ISAAbstractSocketImplementation> getSockets() {
		return sockets;
	}
	
	
	
	public void readExtensionPoint() {
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(EXTENSION_POINT_ID);
		for (IConfigurationElement configurationElement : config) {
			if(configurationElement.getName().equals("request_handler")) {
				try {
					ISAAbstractCoalescenceRequestHandler implementation = (ISAAbstractCoalescenceRequestHandler) configurationElement.createExecutableExtension("implementation");
					implementation.setName(configurationElement.getAttribute("name"));
					implementation.setTargetPath(configurationElement.getAttribute("path"));
					requestHandlers.add(implementation);
				}
				catch(CoreException e) {
					e.printStackTrace();
				}
			}
			else if(configurationElement.getName().equals("server_observer")) {
				try {
					ISACoalescenceServerObserver implementation = (ISACoalescenceServerObserver) configurationElement.createExecutableExtension("implementation");
					serverObservers.add(implementation);
				}
				catch(CoreException e) {
					e.printStackTrace();
				}
			}
			else if(configurationElement.getName().equals("udp_socket")) {
				try {
					String name = configurationElement.getAttribute("name");
					if(name == null)
						name = "<unnamed>";
					
					ISAAbstractSocketImplementation implementation = (ISAAbstractSocketImplementation) configurationElement.createExecutableExtension("implementation");
					sockets.add(implementation);
				}
				catch(CoreException e) {
					e.printStackTrace();
				}
			}
		}
		for(ISAAbstractCoalescenceRequestHandler handler : requestHandlers) {
			String path = handler.getTargetPath();
			if(!allRegisteredPaths.contains(path))
				allRegisteredPaths.add(handler.getTargetPath());
			else
				ISAEventSystem.fireStandardConsoleMessage("A path was registered multiple times with the ISA Coalescence Server: " + path);
		}
		Collections.sort(allRegisteredPaths);
	}

}















/**
 */
package dk.itu.cs.isa.model.visualization;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.visualization.VisualizationPackage
 * @generated
 */
public interface VisualizationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VisualizationFactory eINSTANCE = dk.itu.cs.isa.model.visualization.impl.VisualizationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ISA Named Visual Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Named Visual Element</em>'.
	 * @generated
	 */
	ISANamedVisualElement createISANamedVisualElement();

	/**
	 * Returns a new object of class '<em>ISA Visualization Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Visualization Model</em>'.
	 * @generated
	 */
	ISAVisualizationModel createISAVisualizationModel();

	/**
	 * Returns a new object of class '<em>ISA Vector3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Vector3</em>'.
	 * @generated
	 */
	ISAVector3 createISAVector3();

	/**
	 * Returns a new object of class '<em>ISA Transparent Capsule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Transparent Capsule</em>'.
	 * @generated
	 */
	ISATransparentCapsule createISATransparentCapsule();

	/**
	 * Returns a new object of class '<em>ISA Space Station</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Space Station</em>'.
	 * @generated
	 */
	ISASpaceStation createISASpaceStation();

	/**
	 * Returns a new object of class '<em>ISA Space Station Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Space Station Block</em>'.
	 * @generated
	 */
	ISASpaceStationBlock createISASpaceStationBlock();

	/**
	 * Returns a new object of class '<em>ISA Space Station Antenna</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Space Station Antenna</em>'.
	 * @generated
	 */
	ISASpaceStationAntenna createISASpaceStationAntenna();

	/**
	 * Returns a new object of class '<em>ISA Space Station Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Space Station Relation</em>'.
	 * @generated
	 */
	ISASpaceStationRelation createISASpaceStationRelation();

	/**
	 * Returns a new object of class '<em>ISA Block Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Block Relation</em>'.
	 * @generated
	 */
	ISABlockRelation createISABlockRelation();

	/**
	 * Returns a new object of class '<em>ISA Antenna Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Antenna Relation</em>'.
	 * @generated
	 */
	ISAAntennaRelation createISAAntennaRelation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	VisualizationPackage getVisualizationPackage();

} //VisualizationFactory

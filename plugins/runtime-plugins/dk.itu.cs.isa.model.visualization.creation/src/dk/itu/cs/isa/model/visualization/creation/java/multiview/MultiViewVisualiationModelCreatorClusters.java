package dk.itu.cs.isa.model.visualization.creation.java.multiview;

import org.eclipse.core.runtime.IProgressMonitor;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;

public class MultiViewVisualiationModelCreatorClusters extends AbstractMultiViewVisualiationModelCreator {
	
	@Override
	public ISAVisualizationModel run(ISASoftwareSystemModel softwareModel, IProgressMonitor monitor) throws Exception {
		monitor.beginTask("Creating Visualization Model", 3);
		monitor.subTask("Transforming software model to visualization model");
		
		ISAVisualizationModel model = factory.createISAVisualizationModel();
		model.setName(softwareModel.getName());
		
		ISATransparentCapsule rootCapsule = createCapsule(softwareModel.getRootCluster());
		model.setSubjectSystemCapsule(rootCapsule);
		
		model.getLibraryCapsules().addAll(createLibraryCapsules(softwareModel.getLibraries()));
		
		monitor.worked(1);
		monitor.subTask("Cleaning up model structure");

		MultiViewModelCleanupUtil.deleteEmptyCapsules(rootCapsule);
		MultiViewModelCleanupUtil.collapseCapsules(rootCapsule, ".");
		
		monitor.worked(1);
		monitor.subTask("Initializing references");
		
		referenceInitializer.initializeRelations();
		
		monitor.worked(1);
		return model;
	}



	private ISATransparentCapsule createCapsule(ISACluster cluster) {
		ISATransparentCapsule capsule = factory.createISATransparentCapsule();
		// TODO: where do names come from? --> set manually by user!
//		capsule.setName(null);
//		capsule.setQualifiedName(null);
		
		for(ISAWordOccurrenceEntry wordOccurence : cluster.getTopWordOccurrences()) {
			capsule.getTags().add(wordOccurence.getWord());
		}

		for(ISACluster subCluster : cluster.getChildClusters()) {
			ISATransparentCapsule subCapsule = createCapsule(subCluster);
			capsule.getSubCapsules().add(subCapsule);
		}
		
		for(ISAClassifier classifier : cluster.getClassifiers()) {
			capsule.getSpaceStations().add(createSpaceStation(classifier));
		}
		
		return capsule;
	}

}















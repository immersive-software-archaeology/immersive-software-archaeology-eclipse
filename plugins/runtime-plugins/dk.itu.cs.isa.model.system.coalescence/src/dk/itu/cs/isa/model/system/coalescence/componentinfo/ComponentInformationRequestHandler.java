package dk.itu.cs.isa.model.system.coalescence.componentinfo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import dk.itu.cs.isa.coalescence.server.ISACoalescenceServer;
import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifier;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;
import dk.itu.cs.isa.model.visualization.ISASpaceStation;
import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;
import dk.itu.cs.isa.storage.ISAModelStorage;

@Deprecated
@SuppressWarnings(value = { "all" })
public class ComponentInformationRequestHandler extends ISAAbstractCoalescenceRequestHandler {

	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		String cityName = baseRequest.getParameter("cityQualifiedName");
		
		if(systemName == null || cityName == null) {
			String examplePath1 = "systemName=Ant&cityQualifiedName=City;Line.Line.Line City";
			String examplePath2 = "systemName=ArgoUML&cityQualifiedName=City;List.Event.Event City";
			String explanation = "This path is mapped to functionality that provides raw data on the software elements represented by visual elements.";
			printInvalidParametersHTMLPage(baseRequest, response, explanation, examplePath1, examplePath2);
			return;
		}
		
		JSONObject responseJSON;
		try {
			responseJSON = createComponentInformationJSON(systemName, cityName);
		}
		catch(Exception e) {
			e.printStackTrace();
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_BAD_REQUEST, "Error while handling request", e);
			return;
		}
		
		response.setContentType("application/json; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
		PrintWriter out = response.getWriter();
		out.println(responseJSON.toString(2));
		out.close();
		
		baseRequest.setHandled(true);
	}
	
	

	private JSONObject createComponentInformationJSON(String systemName, String cityName) {
		JSONObject json = new JSONObject();
		
		ISASoftwareSystemModel structureModel = ISAModelStorage.INSTANCE.loadStructureModel(systemName);
		ISAVisualizationModel visModel = ISAModelStorage.INSTANCE.loadVisualizationModel(systemName);
		if(visModel == null || structureModel == null)
			throw new NullPointerException(
					(visModel == null ? "Visualization Model ":"")
					+ (visModel == null && structureModel == null ? "and ":"")
					+ (structureModel == null ? "Structure Model ":"")
					+ "could not be loaded for system \""+ systemName +"\".");
		
		ISATransparentCapsule capsule = null; // TODO
		json.put("qualifiedName", capsule.getQualifiedName());
		
		json.put("tags", capsule.getTags());

		JSONArray classifierArray = new JSONArray();
		for(ISASpaceStation spaceStation : capsule.getSpaceStations()) {
			String qualifiedName = spaceStation.getQualifiedName();
			ISANamedSoftwareElement element = structureModel.getSystemElementByName(qualifiedName);
			if(element instanceof ISAClassifier) {
				classifierArray.put(convertClassifierToJSON((ISAClassifier) element));
			}
		}
		json.put("classifiers", classifierArray);
		
		return json;
	}
	
	

	private JSONObject convertClassifierToJSON(ISAClassifier classifier) {
		JSONObject json = new JSONObject();
		
		json.put("qualifiedName", classifier.getFullyQualifiedName());
		addModifierListToJSON(classifier, json);
		
		JSONArray fieldsArray = new JSONArray();
		for(ISAMember member : classifier.getContainedMembers()) {
			fieldsArray.put(convertMemberToJSON(member));
		}
		json.put("members", fieldsArray);
		
		if(!classifier.getClassifiers().isEmpty()) {
			JSONArray classifierArray = new JSONArray();
			for(ISAClassifier nestedClassifier : classifier.getClassifiers()) {
				classifierArray.put(convertClassifierToJSON(nestedClassifier));
			}
			json.put("nestedClassifiers", classifierArray);
		}
		return json;
	}

	private JSONObject convertMemberToJSON(ISAMember member) {
		JSONObject json = new JSONObject();
		
		String reflectedClassName = member.getClass().getSimpleName();
		if(reflectedClassName.endsWith("Impl"))
			reflectedClassName = reflectedClassName.substring(0, reflectedClassName.length()-4);
		if(reflectedClassName.startsWith("ISA"))
			reflectedClassName = reflectedClassName.substring(3);
		json.put("kind", reflectedClassName);
		
		json.put("name", member.getName());
		addModifierListToJSON(member, json);
		
		if(member instanceof ISATypedElement) {
			json.put("type", ((ISATypedElement) member).getType());
		}
		if(member instanceof ISAReferencingElement) {
			List<ISAReference<? extends ISAReferenceableElement>> refs = new LinkedList<>();
			refs.addAll(((ISAReferencingElement) member).getFieldAccesses());
			refs.addAll(((ISAReferencingElement) member).getConstantAccesses());
			refs.addAll(((ISAReferencingElement) member).getMethodCalls());
			refs.addAll(((ISAReferencingElement) member).getConstructorCalls());
			refs.addAll(((ISAReferencingElement) member).getTypeReferences());
			
			int refWeight = 0;
			for(ISAReference<? extends ISAReferenceableElement> ref : refs) {
				refWeight += ref.getWeight();
			}
			
			json.put("outgoingReferenceCount", refWeight);
		}
		if(member instanceof ISAStatementContainer) {
			json.put("numberOfStatements", ((ISAStatementContainer) member).getNumberOfStatements());
			json.put("numberOfExpressions", ((ISAStatementContainer) member).getNumberOfExpressions());
			json.put("numberOfControlFlowSplits", ((ISAStatementContainer) member).getNumberOfControlFlowSplits());
		}
		if(member instanceof ISAParametrizableElement) {
			JSONArray parameterTypeArray = new JSONArray();
			for(ISAParameter param : ((ISAParametrizableElement) member).getParameters()) {
				parameterTypeArray.put(param.getType());
			}
			json.put("parameterTypes", parameterTypeArray);
		}
		
		return json;
	}
	
	private void addModifierListToJSON(ISAModifiableElement el, JSONObject json) {
		JSONArray modifiersArray = new JSONArray();
		for(ISAModifier modifier : el.getModifiers()) {
			modifiersArray.put(modifier.getName());
		}
		json.put("modifiers", modifiersArray);
	}

}
























package dk.itu.cs.isa.model.system.creation.java.visitors;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.ContinueStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.LabeledStatement;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.SuperMethodReference;
import org.eclipse.jdt.core.dom.TypeMethodReference;

import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

public final class ReferenceTrackingVisitor extends ASTVisitor {

	public class Entry {
		public String qualifiedName;
		public int weight;
		public int type;
		
		public Entry(String qualifiedName, int type, int weight) {
			this.qualifiedName = qualifiedName;
			this.type = type;
			this.weight = weight;
		}
	}
	
	private Map<String, Entry> referencedElements = new LinkedHashMap<>();
	

	
	

	private void registerReferencedElement(String newElementQualifiedName, int type, int weight) {
		if(newElementQualifiedName == null)
			return;
		
		Entry entry = referencedElements.get(newElementQualifiedName);
		if(entry == null)
			referencedElements.put(newElementQualifiedName, new Entry(newElementQualifiedName, type, weight));
		else
			entry.weight += weight;
	}
	
	public Map<String, Entry> getReferencedElements() {
		return referencedElements;
	}

	
	
	
	
	@SuppressWarnings("unused")
	private boolean hasAnonymousParent(ASTNode node) {
		if(node.getParent() == null)
			return false;
		if(node.getParent() instanceof AnonymousClassDeclaration)
			return true;
		return hasAnonymousParent(node.getParent());
	}

	/*
	 * Idea: Track references by visiting name AST nodes,
	 * then try to derive the fully qualified name within
	 * the context of the containing compilation unit.
	 */
	@Override
	public boolean visit(SimpleName node) {
		// Do not track name declarations of methods and fields etc. - those are not references, they are declarations!
		if(node.getParent() instanceof BodyDeclaration) {
			return super.visit(node);
		}
		
		try {
			IBinding referenceBinding = node.resolveBinding();
			if(referenceBinding == null) {
				if(node.getParent() instanceof LabeledStatement) {
					// Labeled statements (e.g., a label for a loop) cannot be resolved
					return super.visit(node);
				}
				else if(node.getParent() instanceof ContinueStatement) {
					// Because of the above reason, references to these labels cannot be resolved
					return super.visit(node);
				}
				else if(node.getParent() instanceof BreakStatement) {
					// Because of the above reason, references to these labels cannot be resolved
					return super.visit(node);
				}
				else {
					// Stumbled across an unresolved reference!
					ISAEventSystem.fireStandardConsoleMessage(node.toString() + "\n\t-> reference could not be resolved...");
				}
			}
			
			if(referenceBinding == null) {
				throw new RuntimeException("A JDT reference binding was not set, please build your workspace and run the analysis again.");
			}
			IJavaElement referenceTarget = referenceBinding.getJavaElement();
			
			// The following is necessary to resolve synthetic bindings,
			// e.g, method calls to a record component that have no method in code as counter part 
			if(referenceTarget == null)
				if(referenceBinding instanceof IMethodBinding)
					for(IVariableBinding field : ((IMethodBinding) referenceBinding).getDeclaringClass().getDeclaredFields())
						if(field.getName().equals(node.getFullyQualifiedName()))
							if(field.getType().getQualifiedName().equals(node.resolveTypeBinding().getQualifiedName()))
								referenceTarget = field.getJavaElement();
			
			if(referenceTarget == null) {
				// This condition is for cases like ".length" on arrays or ".values()" on enums, which cannot be resolved to actual members
				ISAEventSystem.fireStandardConsoleMessage("No corresponding Java element: " + referenceBinding);
				return super.visit(node);
			}
			
			int type;
			switch (referenceTarget.getElementType()) {

			case IJavaElement.TYPE:
				IType targetType = (IType) referenceTarget;
				if(targetType.isEnum())
					type = ClassifiersPackage.ISA_ENUM;
				else if(targetType.isInterface())
					type = ClassifiersPackage.ISA_INTERFACE;
				else if(targetType.isRecord())
					type = ClassifiersPackage.ISA_RECORD;
				else
					type = ClassifiersPackage.ISA_CLASS;
				
				registerReferencedElement(targetType.getFullyQualifiedName('.'), type, 1);
				break;
				
			case IJavaElement.FIELD:
				IField targetField = (IField) referenceTarget;
				if(targetField.isEnumConstant())
					type = MembersPackage.ISA_ENUM_CONSTANT;
				else if(targetField.isRecordComponent())
					type = MembersPackage.ISA_RECORD_COMPONENT;
				else
					type = MembersPackage.ISA_FIELD;
				
				registerReferencedElement(buildQualifiedMemberName(targetField), type, 1);
				break;
				
			case IJavaElement.METHOD:
				IMethod targetMethod = (IMethod) referenceTarget;
				if(targetMethod.isConstructor())
					type = MembersPackage.ISA_CONSTRUCTOR;
				else
					type = MembersPackage.ISA_METHOD;
				
				registerReferencedElement(buildQualifiedMethodName(targetMethod), type, 1);
				break;

			// Ignore these:
			case IJavaElement.LOCAL_VARIABLE:
			case IJavaElement.PACKAGE_FRAGMENT:
			case IJavaElement.TYPE_PARAMETER:
				break;
				
			default:
				ISAEventSystem.fireStandardConsoleMessage("Ignored reference target: " + referenceTarget.getClass().getSimpleName());
			}
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		return super.visit(node);
	}
	
	private String buildQualifiedMemberName(IMember member) throws JavaModelException {
		String qualifiedName = "";
		qualifiedName += member.getDeclaringType().getFullyQualifiedName();
		qualifiedName += ";";
		qualifiedName += member.getElementName();
		
		return qualifiedName;
	}
		
	private String buildQualifiedMethodName(IMethod method) throws JavaModelException {
		String qualifiedName = buildQualifiedMemberName(method);
		
		qualifiedName += "(";
		List<String> parameterTypes = new ArrayList<>(method.getParameterTypes().length);
		for(ILocalVariable parameter : method.getParameters()) {
			parameterTypes.add(resolveSignature(parameter.getTypeSignature(), method.getDeclaringType()));
		}
		qualifiedName += String.join(",", parameterTypes);
		qualifiedName += ")";
		
		return qualifiedName;
	}
	
	private String resolveSignature(String signature, IType declaringType) throws JavaModelException {
		String qualifier = Signature.getSignatureQualifier(signature);
		String name = Signature.getSignatureSimpleName(signature);//.replaceAll("/", ".");

		if (qualifier == null || qualifier.isBlank()) {
			// This method call is crazy slow whenever the resolution of the
			// binding is done from (or to??) a class file.
			// > https://bugs.eclipse.org/bugs/show_bug.cgi?id=273268
			String[][] resolvingResults;
			try {				
				resolvingResults = declaringType.resolveType(name);
			}
			catch(NullPointerException e) {
				// There seem to be hickups sometimes after starting an Eclipse instance,
				// simply running the method again seems to fix that.
				ISAEventSystem.fireStandardConsoleMessage("Hickup when resolving type for element with name=" + name + " - will simply again one more time...");
				resolvingResults = declaringType.resolveType(name);
			}
			
			if (resolvingResults == null || resolvingResults.length == 0)
				// Handle primitive types
				return name;
			
			// If this point is reached, name should contain the simple name of
			// a qualifier, that was successfully resolved in the context of its
			// declaring (contained) type.
			// Take the first resolving result and calculate the qualified name
			String[] nameSegments = resolvingResults[0];
		    if(nameSegments != null) {
		    	name = new String();
		        for(int i=0 ; i < nameSegments.length ; i++) {
		            if(name.length() > 0) {
		            	name += '.';
		            }
		            String part = nameSegments[i];
		            if(part != null) {
		            	name += part;
		            }
		        }
		    }
			return name;
		}
		return qualifier + "." + name;
	}
	
	

	@Override
	public boolean visit(QualifiedName node) {
//		addEntryToReferencedTypes(node.getQualifier() + "." + node.getName());
		return super.visit(node);
	}
	
	@Override
	public boolean visit(ClassInstanceCreation node) {
//		addEntryToReferencedTypes(node.getType().toString());
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperMethodInvocation node) {
//		addEntryToReferencedTypes("super." + node.getName());
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperMethodReference node) {
//		addEntryToReferencedTypes(node.getQualifier() + "." + node.getName());
		return super.visit(node);
	}

	@Override
	public boolean visit(TypeMethodReference node) {
//		addEntryToReferencedTypes(node.getType() + "." + node.getName());
		return super.visit(node);
	}

}
/**
 */
package dk.itu.cs.isa.model.software.structure.impl;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;

import dk.itu.cs.isa.model.software.structure.libraries.ISALibrary;

import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

import dk.itu.cs.isa.model.software.structure.misc.impl.ISANamedSoftwareElementImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Software System Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.impl.ISASoftwareSystemModelImpl#getProjects <em>Projects</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.impl.ISASoftwareSystemModelImpl#getRootCluster <em>Root Cluster</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.impl.ISASoftwareSystemModelImpl#getLibraries <em>Libraries</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.impl.ISASoftwareSystemModelImpl#getMetaInfo <em>Meta Info</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISASoftwareSystemModelImpl extends ISANamedSoftwareElementImpl implements ISASoftwareSystemModel {
	/**
	 * The cached value of the '{@link #getProjects() <em>Projects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjects()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAProject> projects;

	/**
	 * The cached value of the '{@link #getRootCluster() <em>Root Cluster</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootCluster()
	 * @generated
	 * @ordered
	 */
	protected ISACluster rootCluster;

	/**
	 * The cached value of the '{@link #getLibraries() <em>Libraries</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLibraries()
	 * @generated
	 * @ordered
	 */
	protected EList<ISALibrary> libraries;

	/**
	 * The cached value of the '{@link #getMetaInfo() <em>Meta Info</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaInfo()
	 * @generated
	 * @ordered
	 */
	protected EList<String> metaInfo;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISASoftwareSystemModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StructurePackage.Literals.ISA_SOFTWARE_SYSTEM_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAProject> getProjects() {
		if (projects == null) {
			projects = new EObjectContainmentWithInverseEList<ISAProject>(ISAProject.class, this, StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS, FilesystemPackage.ISA_PROJECT__PARENT);
		}
		return projects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISACluster getRootCluster() {
		return rootCluster;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootCluster(ISACluster newRootCluster, NotificationChain msgs) {
		ISACluster oldRootCluster = rootCluster;
		rootCluster = newRootCluster;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER, oldRootCluster, newRootCluster);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRootCluster(ISACluster newRootCluster) {
		if (newRootCluster != rootCluster) {
			NotificationChain msgs = null;
			if (rootCluster != null)
				msgs = ((InternalEObject)rootCluster).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER, null, msgs);
			if (newRootCluster != null)
				msgs = ((InternalEObject)newRootCluster).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER, null, msgs);
			msgs = basicSetRootCluster(newRootCluster, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER, newRootCluster, newRootCluster));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISALibrary> getLibraries() {
		if (libraries == null) {
			libraries = new EObjectContainmentEList<ISALibrary>(ISALibrary.class, this, StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES);
		}
		return libraries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getMetaInfo() {
		if (metaInfo == null) {
			metaInfo = new EDataTypeUniqueEList<String>(String.class, this, StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__META_INFO);
		}
		return metaInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISANamedSoftwareElement getSystemElementByName(String qualifiedName) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISANamedSoftwareElement getLibraryElementByName(String qualifiedName, int expectedType, boolean createOnDemand) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getProjects()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS:
				return ((InternalEList<?>)getProjects()).basicRemove(otherEnd, msgs);
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER:
				return basicSetRootCluster(null, msgs);
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES:
				return ((InternalEList<?>)getLibraries()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS:
				return getProjects();
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER:
				return getRootCluster();
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES:
				return getLibraries();
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__META_INFO:
				return getMetaInfo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS:
				getProjects().clear();
				getProjects().addAll((Collection<? extends ISAProject>)newValue);
				return;
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER:
				setRootCluster((ISACluster)newValue);
				return;
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES:
				getLibraries().clear();
				getLibraries().addAll((Collection<? extends ISALibrary>)newValue);
				return;
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__META_INFO:
				getMetaInfo().clear();
				getMetaInfo().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS:
				getProjects().clear();
				return;
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER:
				setRootCluster((ISACluster)null);
				return;
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES:
				getLibraries().clear();
				return;
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__META_INFO:
				getMetaInfo().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__PROJECTS:
				return projects != null && !projects.isEmpty();
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__ROOT_CLUSTER:
				return rootCluster != null;
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__LIBRARIES:
				return libraries != null && !libraries.isEmpty();
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL__META_INFO:
				return metaInfo != null && !metaInfo.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL___GET_SYSTEM_ELEMENT_BY_NAME__STRING:
				return getSystemElementByName((String)arguments.get(0));
			case StructurePackage.ISA_SOFTWARE_SYSTEM_MODEL___GET_LIBRARY_ELEMENT_BY_NAME__STRING_INT_BOOLEAN:
				return getLibraryElementByName((String)arguments.get(0), (Integer)arguments.get(1), (Boolean)arguments.get(2));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (metaInfo: ");
		result.append(metaInfo);
		result.append(')');
		return result.toString();
	}

} //ISASoftwareSystemModelImpl

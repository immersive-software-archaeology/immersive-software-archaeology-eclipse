package dk.itu.cs.isa.coalescence.server.requests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

public class WelcomePageRequestHandler extends ISAAbstractCoalescenceRequestHandler {

	public void handle(Request baseRequest, HttpServletResponse response, String target, String ... validPaths) throws IOException {
		List<String> linesToPrint = new ArrayList<>();
		if(!target.strip().equals("/"))
			linesToPrint.add("<i>The path \""+target+"\" is not mapped to an operation!</i>");
		
		linesToPrint.add("This Server acts as connector between the ISA Eclipse Plugins and the ISA VR application.<br>Following paths are supported:");
		String listOfValidPaths = "<ul>";
		for(String validPath : validPaths)
			listOfValidPaths += "<li><a href=\""+ validPath +"\">"+ validPath +"</a></li>";
		listOfValidPaths += "</ul>";
		linesToPrint.add(listOfValidPaths);

		linesToPrint.add("<br>");
		linesToPrint.add("<a target=\"_blank\" href=\"/download\">Download the ISA VR client</a>!");
//		linesToPrint.add("<a target=\"_blank\" href=\"https://forms.gle/GvmS3njnkd7DvFE18\">To the survey</a>!");
		
		printHTMLPage(baseRequest, response, HttpServletResponse.SC_NOT_FOUND, "ISA Coalesence Server", null, linesToPrint.toArray(new String[0]));
    }

	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		throw new UnsupportedOperationException("Not implemented");
	}
    
}

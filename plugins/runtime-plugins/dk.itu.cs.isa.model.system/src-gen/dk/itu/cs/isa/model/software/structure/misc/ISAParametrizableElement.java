/**
 */
package dk.itu.cs.isa.model.software.structure.misc;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Parametrizable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAParametrizableElement()
 * @model abstract="true"
 * @generated
 */
public interface ISAParametrizableElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISAParameter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAParametrizableElement_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAParameter> getParameters();

} // ISAParametrizableElement

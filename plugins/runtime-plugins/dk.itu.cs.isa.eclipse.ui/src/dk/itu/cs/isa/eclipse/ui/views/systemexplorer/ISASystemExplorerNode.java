package dk.itu.cs.isa.eclipse.ui.views.systemexplorer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.graphics.Image;

import dk.itu.cs.isa.eclipse.ui.ImageHelper;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.util.ISASystemModelToFileSystemUtil;
import dk.itu.cs.isa.model.visualization.ISANamedVisualElement;
import dk.itu.cs.isa.model.visualization.ISASpaceStation;
import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.preferences.ISAPreferences;
import dk.itu.cs.isa.storage.ISAModelStorage;

class ISASystemExplorerNode implements IAdaptable {
	
	private List<ISASystemExplorerNode> children;

	protected String label;
	protected ISANamedVisualElement visualizationModelElement;
	protected ISASystemExplorerNode parent;
	
	

	public ISASystemExplorerNode(String name) {
		this.children = new ArrayList<ISASystemExplorerNode>();
		this.visualizationModelElement = null;
		this.label = name;
	}

	public ISASystemExplorerNode(ISANamedVisualElement element) {
		this.children = new ArrayList<ISASystemExplorerNode>();
		this.visualizationModelElement = element;
		
		if(visualizationModelElement instanceof ISATransparentCapsule)
			this.label = visualizationModelElement.getQualifiedName();
		else
			this.label = visualizationModelElement.getName();
		
		if(this.label == null || this.label.isBlank())
			this.label = "<unnamed>";
//		if(visualizationModelElement.getQualifiedName() != null && !visualizationModelElement.getQualifiedName().isBlank())
//			this.name += " [" + visualizationModelElement.getQualifiedName() + "]";
//		if(!visualizationModelElement.getTags().isEmpty())
//			this.name += " [" + String.join(", ", visualizationModelElement.getTags()) + "]";
	}
	
	

	public String getLabel() {
		return label;
	}

	public void setParent(ISASystemExplorerNode parent) {
		this.parent = parent;
	}

	public ISASystemExplorerNode getParent() {
		return parent;
	}

	public void addChild(ISASystemExplorerNode child) {
		children.add(child);
		child.setParent(this);
	}

	public void removeChild(ISASystemExplorerNode child) {
		children.remove(child);
		child.setParent(null);
	}

	public ISASystemExplorerNode[] getChildren() {
		return (ISASystemExplorerNode[]) children.toArray(new ISASystemExplorerNode[children.size()]);
	}

	public boolean hasChildren() {
		return children.size() > 0;
	}
	
	public boolean isClassifierNode() {
		return this.visualizationModelElement instanceof ISASpaceStation;
	}
	
	public ISANamedVisualElement getVisualizationModelElement() {
		return visualizationModelElement;
	}

	public String getQualifiedName() {
		if(visualizationModelElement != null)
			return visualizationModelElement.getQualifiedName();
		else return "";
	}

	public String getToolTipMessage() {
		if(visualizationModelElement instanceof ISATransparentCapsule) {
			List<String> tags = ((ISATransparentCapsule) visualizationModelElement).getTags();
			if(!tags.isEmpty())
				return "Tags: " + String.join(", ", tags);
		}
		
		return getQualifiedName();
	}

	@Override
	public String toString() {
//		return this.getClass().getName() + ": " + this.name;
		return this.label;
	}

	@Override
	public <T> T getAdapter(Class<T> key) {
		return null;
	}

	public Image getImage() {
		if(visualizationModelElement instanceof ISATransparentCapsule)
			return ImageHelper.getImage("icons/icon-16-module.png");
		if(visualizationModelElement instanceof ISASpaceStation)
			return ImageHelper.getImage("icons/icon-16-spacestation.png");
		
		return ImageHelper.getImage("icons/icon-16.png");
	}

	public IFile getFile() {
		if(this.visualizationModelElement == null)
			return null;
		
		String qualifiedElementName = this.visualizationModelElement.getQualifiedName();
		if(qualifiedElementName == null || qualifiedElementName.isBlank())
			return null;
		
		String modelName = ISAPreferences.getString(ISAPreferences.LAST_MODEL_NAME, null);
		if(modelName == null)
			return null;
		
//		ModelStorageEntry entry = ISAModelStorage.INSTANCE.getEntry(modelName);
//		String structureModelPath = entry.getFullFilePathToStructureModel();
//		Resource structureModelRes = ISAModelStorage.INSTANCE.loadResource(structureModelPath, false);
//		ISASoftwareSystemModel model = (ISASoftwareSystemModel) structureModelRes.getContents().get(0);
		
		ISASoftwareSystemModel model = ISAModelStorage.INSTANCE.loadStructureModel(modelName);
		
		ISANamedSoftwareElement isaClassifier = model.getSystemElementByName(qualifiedElementName);
		if(!(isaClassifier instanceof ISAClassifier))
			return null;
		return ISASystemModelToFileSystemUtil.getFileForClassifier((ISAClassifier) isaClassifier);
	}
	
}












/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem.impl;

import dk.itu.cs.isa.model.software.structure.StructurePackage;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage;

import dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitecturePackageImpl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;

import dk.itu.cs.isa.model.software.structure.classifiers.impl.ClassifiersPackageImpl;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemFactory;
import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageContainer;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;

import dk.itu.cs.isa.model.software.structure.impl.StructurePackageImpl;

import dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage;

import dk.itu.cs.isa.model.software.structure.libraries.impl.LibrariesPackageImpl;

import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.members.impl.MembersPackageImpl;

import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.MiscPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FilesystemPackageImpl extends EPackageImpl implements FilesystemPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaProjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaPackageContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaPackageRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaPackageEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FilesystemPackageImpl() {
		super(eNS_URI, FilesystemFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FilesystemPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FilesystemPackage init() {
		if (isInited) return (FilesystemPackage)EPackage.Registry.INSTANCE.getEPackage(FilesystemPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFilesystemPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FilesystemPackageImpl theFilesystemPackage = registeredFilesystemPackage instanceof FilesystemPackageImpl ? (FilesystemPackageImpl)registeredFilesystemPackage : new FilesystemPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);
		StructurePackageImpl theStructurePackage = (StructurePackageImpl)(registeredPackage instanceof StructurePackageImpl ? registeredPackage : StructurePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ArchitecturePackage.eNS_URI);
		ArchitecturePackageImpl theArchitecturePackage = (ArchitecturePackageImpl)(registeredPackage instanceof ArchitecturePackageImpl ? registeredPackage : ArchitecturePackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(LibrariesPackage.eNS_URI);
		LibrariesPackageImpl theLibrariesPackage = (LibrariesPackageImpl)(registeredPackage instanceof LibrariesPackageImpl ? registeredPackage : LibrariesPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl)(registeredPackage instanceof ClassifiersPackageImpl ? registeredPackage : ClassifiersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MembersPackage.eNS_URI);
		MembersPackageImpl theMembersPackage = (MembersPackageImpl)(registeredPackage instanceof MembersPackageImpl ? registeredPackage : MembersPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);
		MiscPackageImpl theMiscPackage = (MiscPackageImpl)(registeredPackage instanceof MiscPackageImpl ? registeredPackage : MiscPackage.eINSTANCE);

		// Create package meta-data objects
		theFilesystemPackage.createPackageContents();
		theStructurePackage.createPackageContents();
		theArchitecturePackage.createPackageContents();
		theLibrariesPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theMembersPackage.createPackageContents();
		theMiscPackage.createPackageContents();

		// Initialize created meta-data
		theFilesystemPackage.initializePackageContents();
		theStructurePackage.initializePackageContents();
		theArchitecturePackage.initializePackageContents();
		theLibrariesPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theMembersPackage.initializePackageContents();
		theMiscPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFilesystemPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FilesystemPackage.eNS_URI, theFilesystemPackage);
		return theFilesystemPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAProject() {
		return isaProjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAProject_Parent() {
		return (EReference)isaProjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAProject_PackageRoots() {
		return (EReference)isaProjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISAProject__GetPackage__String() {
		return isaProjectEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getISAProject__GetElement__String() {
		return isaProjectEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAPackageContainer() {
		return isaPackageContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAPackageContainer_SubPackages() {
		return (EReference)isaPackageContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAPackageRoot() {
		return isaPackageRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAPackageRoot_ParentProject() {
		return (EReference)isaPackageRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getISAPackageRoot_Binary() {
		return (EAttribute)isaPackageRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISAPackage() {
		return isaPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getISAPackage_ParentContainer() {
		return (EReference)isaPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FilesystemFactory getFilesystemFactory() {
		return (FilesystemFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		isaProjectEClass = createEClass(ISA_PROJECT);
		createEReference(isaProjectEClass, ISA_PROJECT__PARENT);
		createEReference(isaProjectEClass, ISA_PROJECT__PACKAGE_ROOTS);
		createEOperation(isaProjectEClass, ISA_PROJECT___GET_PACKAGE__STRING);
		createEOperation(isaProjectEClass, ISA_PROJECT___GET_ELEMENT__STRING);

		isaPackageContainerEClass = createEClass(ISA_PACKAGE_CONTAINER);
		createEReference(isaPackageContainerEClass, ISA_PACKAGE_CONTAINER__SUB_PACKAGES);

		isaPackageRootEClass = createEClass(ISA_PACKAGE_ROOT);
		createEReference(isaPackageRootEClass, ISA_PACKAGE_ROOT__PARENT_PROJECT);
		createEAttribute(isaPackageRootEClass, ISA_PACKAGE_ROOT__BINARY);

		isaPackageEClass = createEClass(ISA_PACKAGE);
		createEReference(isaPackageEClass, ISA_PACKAGE__PARENT_CONTAINER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MiscPackage theMiscPackage = (MiscPackage)EPackage.Registry.INSTANCE.getEPackage(MiscPackage.eNS_URI);
		StructurePackage theStructurePackage = (StructurePackage)EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage)EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		isaProjectEClass.getESuperTypes().add(theMiscPackage.getISANamedSoftwareElement());
		isaPackageContainerEClass.getESuperTypes().add(theMiscPackage.getISANamedSoftwareElement());
		isaPackageRootEClass.getESuperTypes().add(this.getISAPackageContainer());
		isaPackageEClass.getESuperTypes().add(this.getISAPackageContainer());
		isaPackageEClass.getESuperTypes().add(theClassifiersPackage.getISAClassifierContainer());

		// Initialize classes, features, and operations; add parameters
		initEClass(isaProjectEClass, ISAProject.class, "ISAProject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAProject_Parent(), theStructurePackage.getISASoftwareSystemModel(), theStructurePackage.getISASoftwareSystemModel_Projects(), "parent", null, 1, 1, ISAProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getISAProject_PackageRoots(), this.getISAPackageRoot(), this.getISAPackageRoot_ParentProject(), "packageRoots", null, 0, -1, ISAProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getISAProject__GetPackage__String(), this.getISAPackage(), "getPackage", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "packageName", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getISAProject__GetElement__String(), theMiscPackage.getISANamedSoftwareElement(), "getElement", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "qualifiedElementName", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(isaPackageContainerEClass, ISAPackageContainer.class, "ISAPackageContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAPackageContainer_SubPackages(), this.getISAPackage(), this.getISAPackage_ParentContainer(), "subPackages", null, 0, -1, ISAPackageContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaPackageRootEClass, ISAPackageRoot.class, "ISAPackageRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAPackageRoot_ParentProject(), this.getISAProject(), this.getISAProject_PackageRoots(), "parentProject", null, 1, 1, ISAPackageRoot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAPackageRoot_Binary(), ecorePackage.getEBoolean(), "binary", null, 1, 1, ISAPackageRoot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaPackageEClass, ISAPackage.class, "ISAPackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getISAPackage_ParentContainer(), this.getISAPackageContainer(), this.getISAPackageContainer_SubPackages(), "parentContainer", null, 1, 1, ISAPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //FilesystemPackageImpl

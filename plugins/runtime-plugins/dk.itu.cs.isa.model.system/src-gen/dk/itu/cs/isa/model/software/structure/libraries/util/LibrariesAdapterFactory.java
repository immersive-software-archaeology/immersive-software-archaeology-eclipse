/**
 */
package dk.itu.cs.isa.model.software.structure.libraries.util;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement;
import dk.itu.cs.isa.model.software.structure.classifiers.ISARecord;
import dk.itu.cs.isa.model.software.structure.libraries.*;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.libraries.LibrariesPackage
 * @generated
 */
public class LibrariesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static LibrariesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibrariesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = LibrariesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LibrariesSwitch<Adapter> modelSwitch =
		new LibrariesSwitch<Adapter>() {
			@Override
			public Adapter caseISALibrary(ISALibrary object) {
				return createISALibraryAdapter();
			}
			@Override
			public Adapter caseISAPrimitiveType(ISAPrimitiveType object) {
				return createISAPrimitiveTypeAdapter();
			}
			@Override
			public Adapter caseISALibraryClassifier(ISALibraryClassifier object) {
				return createISALibraryClassifierAdapter();
			}
			@Override
			public Adapter caseISAClassifierContainer(ISAClassifierContainer object) {
				return createISAClassifierContainerAdapter();
			}
			@Override
			public Adapter caseISANamedSoftwareElement(ISANamedSoftwareElement object) {
				return createISANamedSoftwareElementAdapter();
			}
			@Override
			public Adapter caseISAReferenceableElement(ISAReferenceableElement object) {
				return createISAReferenceableElementAdapter();
			}
			@Override
			public Adapter caseISAType(ISAType object) {
				return createISATypeAdapter();
			}
			@Override
			public Adapter caseISAModifiableElement(ISAModifiableElement object) {
				return createISAModifiableElementAdapter();
			}
			@Override
			public Adapter caseISAMember(ISAMember object) {
				return createISAMemberAdapter();
			}
			@Override
			public Adapter caseISAClassifier(ISAClassifier object) {
				return createISAClassifierAdapter();
			}
			@Override
			public Adapter caseISAConstructorContainer(ISAConstructorContainer object) {
				return createISAConstructorContainerAdapter();
			}
			@Override
			public Adapter caseISAClass(ISAClass object) {
				return createISAClassAdapter();
			}
			@Override
			public Adapter caseISAInterfaceExtendingElement(ISAInterfaceExtendingElement object) {
				return createISAInterfaceExtendingElementAdapter();
			}
			@Override
			public Adapter caseISAInterface(ISAInterface object) {
				return createISAInterfaceAdapter();
			}
			@Override
			public Adapter caseISAEnum(ISAEnum object) {
				return createISAEnumAdapter();
			}
			@Override
			public Adapter caseISARecord(ISARecord object) {
				return createISARecordAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibrary <em>ISA Library</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.ISALibrary
	 * @generated
	 */
	public Adapter createISALibraryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.libraries.ISAPrimitiveType <em>ISA Primitive Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.ISAPrimitiveType
	 * @generated
	 */
	public Adapter createISAPrimitiveTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.libraries.ISALibraryClassifier <em>ISA Library Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.libraries.ISALibraryClassifier
	 * @generated
	 */
	public Adapter createISALibraryClassifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer <em>ISA Classifier Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer
	 * @generated
	 */
	public Adapter createISAClassifierContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement <em>ISA Named Software Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement
	 * @generated
	 */
	public Adapter createISANamedSoftwareElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement <em>ISA Referenceable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement
	 * @generated
	 */
	public Adapter createISAReferenceableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAType <em>ISA Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAType
	 * @generated
	 */
	public Adapter createISATypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement <em>ISA Modifiable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement
	 * @generated
	 */
	public Adapter createISAModifiableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAMember <em>ISA Member</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAMember
	 * @generated
	 */
	public Adapter createISAMemberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier <em>ISA Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier
	 * @generated
	 */
	public Adapter createISAClassifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer <em>ISA Constructor Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer
	 * @generated
	 */
	public Adapter createISAConstructorContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAClass <em>ISA Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAClass
	 * @generated
	 */
	public Adapter createISAClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement <em>ISA Interface Extending Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement
	 * @generated
	 */
	public Adapter createISAInterfaceExtendingElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface <em>ISA Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface
	 * @generated
	 */
	public Adapter createISAInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum <em>ISA Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum
	 * @generated
	 */
	public Adapter createISAEnumAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.classifiers.ISARecord <em>ISA Record</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.classifiers.ISARecord
	 * @generated
	 */
	public Adapter createISARecordAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //LibrariesAdapterFactory

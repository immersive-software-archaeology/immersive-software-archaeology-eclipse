/**
 */
package dk.itu.cs.isa.model.software.structure.misc;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAType#getUsagesAsType <em>Usages As Type</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAType()
 * @model
 * @generated
 */
public interface ISAType extends ISAReferenceableElement {
	/**
	 * Returns the value of the '<em><b>Usages As Type</b></em>' reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Usages As Type</em>' reference list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAType_UsagesAsType()
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISATypedElement#getType
	 * @model opposite="type"
	 * @generated
	 */
	EList<ISATypedElement> getUsagesAsType();

} // ISAType

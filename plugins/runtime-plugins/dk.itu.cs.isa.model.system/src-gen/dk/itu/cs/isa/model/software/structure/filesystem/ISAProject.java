/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;

import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getParent <em>Parent</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getPackageRoots <em>Package Roots</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAProject()
 * @model
 * @generated
 */
public interface ISAProject extends ISANamedSoftwareElement {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getProjects <em>Projects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(ISASoftwareSystemModel)
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAProject_Parent()
	 * @see dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel#getProjects
	 * @model opposite="projects" required="true" transient="false"
	 * @generated
	 */
	ISASoftwareSystemModel getParent();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(ISASoftwareSystemModel value);

	/**
	 * Returns the value of the '<em><b>Package Roots</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot}.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#getParentProject <em>Parent Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Roots</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAProject_PackageRoots()
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#getParentProject
	 * @model opposite="parentProject" containment="true"
	 * @generated
	 */
	EList<ISAPackageRoot> getPackageRoots();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model packageNameRequired="true"
	 * @generated
	 */
	ISAPackage getPackage(String packageName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model qualifiedElementNameRequired="true"
	 * @generated
	 */
	ISANamedSoftwareElement getElement(String qualifiedElementName);

} // ISAProject

package dk.itu.cs.isa.whiteboard.requests;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.util.EcoreUtilISA;
import dk.itu.cs.isa.util.FileUtil;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;

public class WhiteboardFetchTutorialRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	private static final String PLUGIN_NAME = "dk.itu.cs.isa.whiteboard";
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		try {
			response.setContentType("application/xml; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_OK);
			
			File structureModelFile = FileUtil.getFileFromPlugin(PLUGIN_NAME, "examples/tutorial.whiteboard");
			Resource resource = EcoreUtilISA.loadResource(new ResourceSetImpl(), structureModelFile.getAbsolutePath(), false);
			
			if(resource == null)
				throw new NullPointerException("Tutorial whiteboard resource could not be found: " + structureModelFile.getAbsolutePath());
			
			((WhiteboardHistory) resource.getContents().get(0)).getWhiteboards().clear();
			
			out.println(EcoreUtilISA.getXMLStringForModel(resource));
			baseRequest.setHandled(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_CONFLICT, "Error", e, "The tutorial could not be loaded");
			baseRequest.setHandled(true);
		}
		finally {
			out.close();
		}
	}
}
/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Package Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#getParentProject <em>Parent Project</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#isBinary <em>Binary</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAPackageRoot()
 * @model
 * @generated
 */
public interface ISAPackageRoot extends ISAPackageContainer {
	/**
	 * Returns the value of the '<em><b>Parent Project</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getPackageRoots <em>Package Roots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Project</em>' container reference.
	 * @see #setParentProject(ISAProject)
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAPackageRoot_ParentProject()
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.ISAProject#getPackageRoots
	 * @model opposite="packageRoots" required="true" transient="false"
	 * @generated
	 */
	ISAProject getParentProject();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#getParentProject <em>Parent Project</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Project</em>' container reference.
	 * @see #getParentProject()
	 * @generated
	 */
	void setParentProject(ISAProject value);

	/**
	 * Returns the value of the '<em><b>Binary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binary</em>' attribute.
	 * @see #setBinary(boolean)
	 * @see dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage#getISAPackageRoot_Binary()
	 * @model required="true"
	 * @generated
	 */
	boolean isBinary();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot#isBinary <em>Binary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binary</em>' attribute.
	 * @see #isBinary()
	 * @generated
	 */
	void setBinary(boolean value);

} // ISAPackageRoot

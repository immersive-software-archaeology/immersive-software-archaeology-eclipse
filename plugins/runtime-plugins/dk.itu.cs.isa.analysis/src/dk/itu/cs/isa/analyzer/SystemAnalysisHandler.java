package dk.itu.cs.isa.analyzer;

import static dk.itu.cs.isa.analyzer.util.NameGuessingUtil.guessName;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.internal.WorkingSet;

import dk.itu.cs.isa.analyzer.exceptions.UserAbortException;
import dk.itu.cs.isa.analyzer.interfaces.ISystemAnalysisObserver;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelArchitectureAnalyzer;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelCreator;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelQualityAnalyzer;
import dk.itu.cs.isa.analyzer.interfaces.IVisualiationModelCreator;
import dk.itu.cs.isa.analyzer.util.ResourceDebugUtil;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.events.events.AnalysisSetupEvent;
import dk.itu.cs.isa.events.events.OutputEvent;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel;
import dk.itu.cs.isa.model.system.quality.QualityFactory;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;
import dk.itu.cs.isa.preferences.ISAPreferences;
import dk.itu.cs.isa.storage.ISAModelStorage;
import dk.itu.cs.isa.util.EcoreUtilISA;

@SuppressWarnings("restriction")
public class SystemAnalysisHandler extends AbstractHandler {

	private static IProgressMonitor currentMonitor = null;
	
	private ISASoftwareSystemModel structureModel;
	private ISASoftwareSystemQualityModel qualityModel;
	private ISAVisualizationModel visualizationModel;
	
	private boolean conductSAR;
	
	
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		if(currentMonitor != null) {
			int returnCode = ISAEventSystem.fireConfirmationDialog("Analysis already running",
					"Another instance of the ISA analysis seems to be running already!\n"
					+ "You cannot start multiple analysis processes at the same time.\n\n"
					+ "Do you wish to abort the other analysis instance now?",
					"Close",
					"Abort other Analysis Instance").getReturnCode();
			if(returnCode != 0)
				currentMonitor.setCanceled(true);
			return null;
		}
		ISelection selection = HandlerUtil.getActiveMenuSelection(event);

		List<IProject> selectedProjects;
		if (selection instanceof TreeSelection)
			selectedProjects = getSelectedProjects(selection);
		else throw new RuntimeException("Unexpected type of element selection: " + selection.getClass().getSimpleName());
		
		AnalysisSetupEvent nameInputEvent = ISAEventSystem.fireAnalysisSetupDialog(guessName(selectedProjects));
		if(nameInputEvent.getReturnCode() != 0) {
			return null;
		}
		
		final String name = nameInputEvent.getSystemName();
		if(name == null) {
			ISAEventSystem.fireWarningMessage("Please specify a name for your model!");
			return execute(event);
		}
		ISAPreferences.setString(ISAPreferences.LAST_MODEL_NAME, name);

		ISystemModelCreator systemModelCreator;
		ISystemModelQualityAnalyzer qualityAnalyzer;
		ISystemModelArchitectureAnalyzer architectureAnalyzer;
		IVisualiationModelCreator visualizationModelCreator;
		try {
			systemModelCreator = SystemAnalysisExtensionHandler.INSTANCE.getDefaultSystemModelCreatorFromExtensions();
			qualityAnalyzer = SystemAnalysisExtensionHandler.INSTANCE.getDefaultSystemModelQualityAnalyzerFromExtensions();
			architectureAnalyzer = SystemAnalysisExtensionHandler.INSTANCE.getDefaultArchitectureAnalyzerFromExtensions();
			visualizationModelCreator = SystemAnalysisExtensionHandler.INSTANCE.getDefaultVisualizationModelCreatorFromExtensions();

			systemModelCreator.setupBeforeRun();
			qualityAnalyzer.setupBeforeRun();
			architectureAnalyzer.setupBeforeRun();
			visualizationModelCreator.setupBeforeRun();
		} catch (Exception e) {
			if(!(e instanceof UserAbortException)) {
				e.printStackTrace();
				ISAEventSystem.fireErrorMessage("Something went wrong during the instantiation of a model creator!", e);
			}
			return null;
		}

		structureModel = ResourceDebugUtil.loadStructureDebugResources(name);
		qualityModel = ResourceDebugUtil.loadQualityDebugResources(name);
		conductSAR = true;
		if(structureModel != null || qualityModel != null) {
			int returnCode = ISAEventSystem.fireConfirmationDialog("Debug Resources Found",
					"Debug resources were found for system\""+ name +"\".\n"
					+ "At what step do you wish to start the analysis?",
					"Design Structure Analysis",
					"Quality Analysis",
					"Architecture Analysis",
					"Visualization Model Creation").getReturnCode();
			switch(returnCode) {
			case 0:
				// Start from scratch
				structureModel = null;
				qualityModel = null;
				conductSAR = true;
				break;
			case 1:
				// Start with Quality analysis
				qualityModel = null;
				conductSAR = true;
				break;
			case 2:
				// Start with SAR
				conductSAR = true;
				break;
			case 3:
				// Only regenerate the visualization model
				conductSAR = false;
				break;
			}
		}

		Job job = Job.create("Creating ISA Models", (ICoreRunnable) monitor -> {
			currentMonitor = monitor;
			long executionStart = System.currentTimeMillis();
			ISAEventSystem.fireStandardConsoleMessage("Starting analysis: " + name);
			
			try {
				if(structureModel == null) {
					structureModel = systemModelCreator.run(name, selectedProjects, monitor);
					EcoreUtilISA.scanForUncontainedElements(structureModel, ISASoftwareSystemModel.class);
					if(structureModel == null)
						throw new NullPointerException("Software System Model creation yielded empty model.");
				}
				else ISAEventSystem.fireStandardConsoleMessage("Skipping structure analysis...");
				
				if(monitor.isCanceled())
					return;
				
				if(qualityModel == null && ISAPreferences.getBoolean(ISAPreferences.SYSTEM_MODEL_QUALITY_ANALYZER_EXECUTE, true)) {
					qualityModel = QualityFactory.eINSTANCE.createISASoftwareSystemQualityModel();
					qualityModel.setName(name);
					
					try {
						qualityAnalyzer.run(structureModel, qualityModel, selectedProjects, monitor);
						EcoreUtilISA.scanForUncontainedElements(qualityModel, ISASoftwareSystemQualityModel.class);
					}
					catch(UserAbortException e) {
						return;
					}
					catch(Exception e) {
						e.printStackTrace();
						Display.getDefault().syncExec(() -> {
							String errorMsg = e.toString();
							if(errorMsg.length() > 1000)
								errorMsg = errorMsg.substring(0, 1000) + "...";
							OutputEvent out = ISAEventSystem.fireConfirmationDialog(
									"Quality Analysis did not succeed",
									errorMsg
									+ "\n\nPlease check the ISA Eclipse preference page (Window > Preferences > ISA)."
									+ "\nDo you wish to continue with the analysis anyways?"
									+ " If so, you will not be able to investigate the quality of your system.");
							if(out.getReturnCode() != 0)
								monitor.setCanceled(true);
						});
						if(monitor.isCanceled())
							return;
					}
				}
				else ISAEventSystem.fireStandardConsoleMessage("Skipping quality analysis...");
				
				if(conductSAR && ISAPreferences.getBoolean(ISAPreferences.SYSTEM_MODEL_SAR_EXECUTE, true)) {
					architectureAnalyzer.run(structureModel, monitor);
					EcoreUtilISA.scanForUncontainedElements(structureModel, ISASoftwareSystemModel.class);
					if(monitor.isCanceled())
						return;
				}
				else ISAEventSystem.fireStandardConsoleMessage("Skipping software architecture recovery...");
				
				visualizationModel = visualizationModelCreator.run(structureModel, monitor);
				if(visualizationModel == null)
					throw new NullPointerException("Visualization Model creation yielded empty model.");
				EcoreUtilISA.scanForUncontainedElements(visualizationModel, ISAVisualizationModel.class);
				if(monitor.isCanceled())
					return;
				
				try {
					ISAModelStorage.INSTANCE.createEntry(name, monitor, structureModel, qualityModel, visualizationModel);
				} catch (Exception e) {
					throw new RuntimeException("Could not store the models.\n\n"
							+ "Is the VR application still running and preventing "
							+ "the ISA Eclipse plugins from overwriting existing models?", e);
				}
			} catch (Exception e) {
				if(!(e instanceof UserAbortException)) {
					e.printStackTrace();
					ISAEventSystem.fireStandardConsoleMessage("Analysis of system " + name + " failed: " + e.getMessage());
					Display.getDefault().asyncExec(() -> {
						ISAEventSystem.fireErrorMessage("Software System Analysis did not succeed!", e);
					});
				}
				return;
			}
			finally {
				systemModelCreator.cleanUpAfterRun();
				qualityAnalyzer.cleanUpAfterRun();
				architectureAnalyzer.cleanUpAfterRun();
				visualizationModelCreator.cleanUpAfterRun();

				try {
					ResourceDebugUtil.storeStructureModelDebugResource(structureModel);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					ResourceDebugUtil.storeSystemQualityDebugResource(qualityModel);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					ResourceDebugUtil.storeVisualizationModelDebugResource(visualizationModel);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				currentMonitor = null;
			}

			Display.getDefault().asyncExec(() -> {
				ISAEventSystem.fireStandardConsoleMessage("Analysis finished successfully, visualization model created: " + name);
				for(ISystemAnalysisObserver observer : SystemAnalysisExtensionHandler.INSTANCE.getAllSystemAnalysisObserversFromExtensions()) {
					observer.notifyAnalysisFinished(name, (int) (System.currentTimeMillis()-executionStart));
				}
			});
		});
	
		job.setPriority(Job.LONG);
		job.schedule();
		return null;
	}
	
	
	
	private List<IProject> getSelectedProjects(ISelection selection) {
		List<IProject> selectedProjects = new ArrayList<>();
		for (TreePath selectionPath : ((TreeSelection) selection).getPaths()) {
			Object selectedObject = selectionPath.getLastSegment();
				
			if(selectedObject instanceof IProject) {
				selectedProjects.add((IProject) selectedObject);
			}
			else if(selectedObject instanceof IJavaProject) {
				selectedProjects.add(((IJavaProject) selectedObject).getProject());
			}
			else if(selectedObject instanceof WorkingSet) {
				for(IAdaptable element : ((WorkingSet) selectedObject).getElements()) {
					IProject project;
					if(element instanceof IProject) {
						project = (IProject) element;
					}
					else if(element instanceof IJavaProject) {
						project = ((IJavaProject) element).getProject();
					}
					else continue;
					
					if(!selectedProjects.contains(project))
						selectedProjects.add(project);
				}
			}
		}
		return selectedProjects;
	}
	
	
	
}

























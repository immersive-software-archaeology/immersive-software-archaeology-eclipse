/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Photo Remove Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.PhotoRemoveOperation#isPlayDissolveAnimation <em>Play Dissolve Animation</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhotoRemoveOperation()
 * @model
 * @generated
 */
public interface PhotoRemoveOperation extends AbstractPhotoOperation {
	/**
	 * Returns the value of the '<em><b>Play Dissolve Animation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Play Dissolve Animation</em>' attribute.
	 * @see #setPlayDissolveAnimation(boolean)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhotoRemoveOperation_PlayDissolveAnimation()
	 * @model required="true"
	 * @generated
	 */
	boolean isPlayDissolveAnimation();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.PhotoRemoveOperation#isPlayDissolveAnimation <em>Play Dissolve Animation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Play Dissolve Animation</em>' attribute.
	 * @see #isPlayDissolveAnimation()
	 * @generated
	 */
	void setPlayDissolveAnimation(boolean value);

} // PhotoRemoveOperation

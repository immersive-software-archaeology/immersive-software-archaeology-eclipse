/**
 */
package dk.itu.cs.isa.model.software.structure.members;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Constructor Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer#getConstructors <em>Constructors</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAConstructorContainer()
 * @model abstract="true"
 * @generated
 */
public interface ISAConstructorContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Constructors</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.members.ISAConstructor}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constructors</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAConstructorContainer_Constructors()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAConstructor> getConstructors();

} // ISAConstructorContainer

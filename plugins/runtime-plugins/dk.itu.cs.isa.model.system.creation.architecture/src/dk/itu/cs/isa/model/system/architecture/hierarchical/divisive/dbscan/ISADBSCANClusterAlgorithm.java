package dk.itu.cs.isa.model.system.architecture.hierarchical.divisive.dbscan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NullArgumentException;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.system.architecture.util.SimilarityCalculator;



/**
 * This class is an adapted copy of the Apache Commons Math DBSCAN implementation
 */
public class ISADBSCANClusterAlgorithm {
	
	private final ArchitectureFactory factory = ArchitectureFactory.eINSTANCE;
	
	private SimilarityCalculator measures;
	
	/** Maximum radius of the neighborhood to be considered. */
    private final double eps;

    /** Minimum number of points needed for a cluster. */
    private final int minPts;

    /** Status of a point during the clustering process. */
    private enum PointStatus {
        /** The point is considered to be noise. */
        NOISE,
        /** The point is already part of a cluster. */
        PART_OF_CLUSTER
    }

    
    
	public ISADBSCANClusterAlgorithm(double eps, int minPts) {
        if (eps < 0.0d) {
            throw new NotPositiveException(eps);
        }
        if (minPts <= 0) {
            throw new IllegalArgumentException("minPts must at least be 1");
        }
        this.eps = eps;
        this.minPts = minPts;
        
        this.measures = new SimilarityCalculator();
	}
	
	
	
	public List<ISACluster> cluster(final List<ISAClassifier> classifiers) throws NullArgumentException {
		if(classifiers == null || classifiers.isEmpty()) {
			throw new NullPointerException();
		}

        final List<ISACluster> clusters = new ArrayList<ISACluster>();
        final Map<ISAClassifier, PointStatus> visited = new HashMap<ISAClassifier, PointStatus>();

        for (final ISAClassifier point : classifiers) {
            if (visited.get(point) != null) {
                continue;
            }
            final List<ISAClassifier> neighbors = getNeighbors(point, classifiers);
            if (neighbors.size() >= minPts) {
                // DBSCAN does not care about center points
                final ISACluster cluster = factory.createISACluster();
                clusters.add(expandCluster(cluster, point, neighbors, classifiers, visited));
            } else {
                visited.put(point, PointStatus.NOISE);
            }
        }

        return clusters;
    }
	
	
	
	private ISACluster expandCluster(final ISACluster cluster, final ISAClassifier point, final List<ISAClassifier> neighbors,
			final Collection<ISAClassifier> points, final Map<ISAClassifier, PointStatus> visited) {
		addPointToCluster(cluster, point);
		visited.put(point, PointStatus.PART_OF_CLUSTER);

		List<ISAClassifier> seeds = new ArrayList<ISAClassifier>(neighbors);
		int index = 0;
		while (index < seeds.size()) {
			final ISAClassifier current = seeds.get(index);
			PointStatus pStatus = visited.get(current);
			// only check non-visited points
			if (pStatus == null) {
				final List<ISAClassifier> currentNeighbors = getNeighbors(current, points);
				if (currentNeighbors.size() >= minPts) {
					seeds = merge(seeds, currentNeighbors);
				}
			}

			if (pStatus != PointStatus.PART_OF_CLUSTER) {
				visited.put(current, PointStatus.PART_OF_CLUSTER);
				addPointToCluster(cluster, current);
			}

			index++;
		}
		return cluster;
	}
	
    private void addPointToCluster(ISACluster cluster, ISAClassifier current) {
    	cluster.getClassifiers().add(current);
	}



	private List<ISAClassifier> getNeighbors(final ISAClassifier point, final Collection<ISAClassifier> points) {
        final List<ISAClassifier> neighbors = new ArrayList<ISAClassifier>();
        for (final ISAClassifier neighbor : points) {
            if (point != neighbor && distance(neighbor, point) <= eps) {
                neighbors.add(neighbor);
            }
        }
        return neighbors;
    }



	private List<ISAClassifier> merge(final List<ISAClassifier> one, final List<ISAClassifier> two) {
        final Set<ISAClassifier> oneSet = new HashSet<ISAClassifier>(one);
        for (ISAClassifier item : two) {
            if (!oneSet.contains(item)) {
                one.add(item);
            }
        }
        return one;
    }
    
    
    
    private double distance(ISAClassifier neighbor, ISAClassifier point) {
		return 1d-measures.calculateSimilarity(neighbor, point);
	}

}

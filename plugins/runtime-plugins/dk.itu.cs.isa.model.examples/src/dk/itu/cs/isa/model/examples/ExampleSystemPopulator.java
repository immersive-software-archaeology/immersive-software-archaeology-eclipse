package dk.itu.cs.isa.model.examples;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.StructurePackage;
import dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel;
import dk.itu.cs.isa.model.system.quality.QualityPackage;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;
import dk.itu.cs.isa.storage.ISAModelStorage;
import dk.itu.cs.isa.util.FileUtil;

public class ExampleSystemPopulator {
	
	private static final String PLUGIN_NAME = "dk.itu.cs.isa.model.examples";
	
	public static ExampleSystemPopulator INSTANCE = new ExampleSystemPopulator();
	
	
	
	private Bundle bundle;
	private Map<String, List<String>> systemNameToModelLocationInExamplePlugin = new HashMap<>();
	
	private ExampleSystemPopulator() {
		bundle = Platform.getBundle(PLUGIN_NAME);
		try {
			setupModelMapping();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void setupModelMapping() throws URISyntaxException, IOException {
		for(String exampleSystemModelFolder : getExampleSystemModelFolders()) {
			String modelName = exampleSystemModelFolder.split("/")[1];

			File structureModelFile = null;
			File qualityModelFile = null;
			File visualizationModelFile = null;
			try {
				structureModelFile = FileUtil.getFileFromPlugin(PLUGIN_NAME, exampleSystemModelFolder + modelName + "." + StructurePackage.eNAME);
			} catch(Exception e) {
			}
			try {
				qualityModelFile = FileUtil.getFileFromPlugin(PLUGIN_NAME, exampleSystemModelFolder + modelName + "." + QualityPackage.eNAME);
			} catch(Exception e) {
			}
			try {
				visualizationModelFile = FileUtil.getFileFromPlugin(PLUGIN_NAME, exampleSystemModelFolder + modelName + "." + VisualizationPackage.eNAME);
			} catch(Exception e) {
			}
			
			List<String> modelPathsInExamplePlugin = new ArrayList<>();
			if(structureModelFile == null || !structureModelFile.exists() || visualizationModelFile == null || !visualizationModelFile.exists())
				continue;
			
			modelPathsInExamplePlugin.add(structureModelFile.getAbsolutePath());
			modelPathsInExamplePlugin.add(visualizationModelFile.getAbsolutePath());
			
			if(qualityModelFile != null && qualityModelFile.exists())
				modelPathsInExamplePlugin.add(qualityModelFile.getAbsolutePath());
			
			systemNameToModelLocationInExamplePlugin.put(modelName, modelPathsInExamplePlugin);
		}
	}

	/**
	 * Reads the model folder which is a root folder of the example plugin.
	 * @return string list of paths to the folders each containing a set of example system models.
	 */
	private List<String> getExampleSystemModelFolders() {
		Enumeration<String> modelFolderEntries = bundle.getEntryPaths("models");
		
		List<String> modelFolders = new ArrayList<>();
		if(modelFolderEntries != null) {
			while(modelFolderEntries.hasMoreElements()) {
				modelFolders.add(modelFolderEntries.nextElement());
			}
		}
		return modelFolders;
	}
	
	
	
	public Collection<String> getExampleSystemsInfoAsHTMLCode() {
		List<String> htmlLines = new ArrayList<>();
		htmlLines.add("<ul>");
		
		for(String systemName : systemNameToModelLocationInExamplePlugin.keySet()) {
			htmlLines.add("<li style=\"margin-top: 20px;\"><strong>" + systemName + "</strong></li>");
			htmlLines.add("<ul>");
			for(String modelLocation : systemNameToModelLocationInExamplePlugin.get(systemName)) {
				htmlLines.add("<li>" + modelLocation.substring(modelLocation.lastIndexOf(".")+1) + "</li>");
			}
			htmlLines.add("</ul>");
		}
		
		htmlLines.add("</ul>");
		return htmlLines;
	}
	
	public void populateExampleSystems() throws IOException {
		for(String systemName : systemNameToModelLocationInExamplePlugin.keySet()) {
			List<String> modelPathsInExamplePlugin = systemNameToModelLocationInExamplePlugin.get(systemName);
			ISASoftwareSystemModel structureModel = null;
			ISASoftwareSystemQualityModel qualityModel = null;
			ISAVisualizationModel visualizationModel = null;
			
			for(String modelPath : modelPathsInExamplePlugin) {
				if(modelPath.endsWith(StructurePackage.eNAME))
					structureModel = ISAModelStorage.INSTANCE.loadModel(modelPath);
				else if(modelPath.endsWith(QualityPackage.eNAME))
					qualityModel = ISAModelStorage.INSTANCE.loadModel(modelPath);
				else if(modelPath.endsWith(VisualizationPackage.eNAME))
					visualizationModel = ISAModelStorage.INSTANCE.loadModel(modelPath);
			}
			
			ISAModelStorage.INSTANCE.createEntry(systemName, new NullProgressMonitor(), structureModel, qualityModel, visualizationModel);
		}
	}

}
















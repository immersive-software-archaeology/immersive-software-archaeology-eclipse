/**
 */
package dk.itu.cs.isa.events.events;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Analysis Setup Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.AnalysisSetupEvent#getSystemName <em>System Name</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getAnalysisSetupEvent()
 * @model
 * @generated
 */
public interface AnalysisSetupEvent extends InputEvent {
	/**
	 * Returns the value of the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Name</em>' attribute.
	 * @see #setSystemName(String)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getAnalysisSetupEvent_SystemName()
	 * @model required="true"
	 * @generated
	 */
	String getSystemName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.AnalysisSetupEvent#getSystemName <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Name</em>' attribute.
	 * @see #getSystemName()
	 * @generated
	 */
	void setSystemName(String value);

} // AnalysisSetupEvent

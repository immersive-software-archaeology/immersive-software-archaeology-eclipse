package dk.itu.cs.isa.multiplayer.udp;

import java.nio.ByteBuffer;

public class DatagramReader {

    private final ByteBuffer longBuffer = ByteBuffer.allocate(Long.BYTES);
	private final ByteBuffer intBuffer = ByteBuffer.allocate(Integer.BYTES);
    private final ByteBuffer floatBuffer = ByteBuffer.allocate(Float.BYTES);
    
    private final byte[] datagram;
    private int index = 0;
    
    
	
	public DatagramReader(byte[] datagram) {
		this.datagram = datagram;
	}
	
	public byte[] getDatagram() {
		return datagram;
	}
	
	
	
	public byte readByte() {
		return datagram[index++];
	}
	
	public long readLong() {
		byte[] bytes = new byte[Long.BYTES];
		for(int i=0; i<bytes.length; i++) {
			bytes[i] = readByte();
		}
		return toLong(bytes);
	}
	
	public int readInt() {
		byte[] bytes = new byte[Integer.BYTES];
		for(int i=0; i<bytes.length; i++) {
			bytes[i] = readByte();
		}
		return toInt(bytes);
	}
	
	public float readFloat() {
		byte[] bytes = new byte[Float.BYTES];
		for(int i=0; i<bytes.length; i++) {
			bytes[i] = readByte();
		}
		return toFloat(bytes);
	}

	public float[] readFloatArray(int dimensionality) {
		float[] result = new float[dimensionality];
		for(int i=0; i<dimensionality; i++) {
			result[i] = readFloat();
		}
		return result;
	}

	public String readString(int systemNameLength) {
		byte[] bytes = new byte[systemNameLength];
		for(int i=0; i<bytes.length; i++) {
			bytes[i] = readByte();
		}
		return new String(bytes);
	}
    
    

    private long toLong(byte[] bytes) {
    	longBuffer.clear();
	    longBuffer.put(bytes);
	    longBuffer.flip(); //need flip 
	    return longBuffer.getLong();
	}

	private int toInt(byte[] bytes) {
		intBuffer.clear();
	    intBuffer.put(bytes);
	    intBuffer.flip(); //need flip 
	    return intBuffer.getInt();
	}

    private float toFloat(byte[] bytes) {
    	floatBuffer.clear();
	    floatBuffer.put(bytes);
	    floatBuffer.flip(); //need flip 
	    return floatBuffer.getFloat();
    	
//    	byte[] receivedBytes = packet.getData();
//		int receivedBytesAsInt = (receivedBytes[0] & 0xFF) 
//	            | ((receivedBytes[1] & 0xFF) << 8) 
//	            | ((receivedBytes[2] & 0xFF) << 16) 
//	            | ((receivedBytes[3] & 0xFF) << 24);
//		System.out.println(Float.intBitsToFloat(receivedBytesAsInt));
	}

}

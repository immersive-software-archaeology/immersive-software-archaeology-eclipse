package dk.itu.cs.isa.analyzer.interfaces;

import org.eclipse.core.runtime.IProgressMonitor;

import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;

public interface IVisualiationModelCreator extends ISAModelHandler {

	public ISAVisualizationModel run(ISASoftwareSystemModel softwareModel, IProgressMonitor monitor) throws Exception;

}

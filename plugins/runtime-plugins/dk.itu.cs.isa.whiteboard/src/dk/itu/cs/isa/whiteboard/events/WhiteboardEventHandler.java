package dk.itu.cs.isa.whiteboard.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.util.EcoreUtil;

import dk.itu.cs.isa.whiteboard.AbstractCompoundOperation;
import dk.itu.cs.isa.whiteboard.AbstractOperation;
import dk.itu.cs.isa.whiteboard.AbstractPinOperation;
import dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent;
import dk.itu.cs.isa.whiteboard.BoardDestroyEvent;
import dk.itu.cs.isa.whiteboard.BoardDuplicateEvent;
import dk.itu.cs.isa.whiteboard.BoardEnlargeOperation;
import dk.itu.cs.isa.whiteboard.BoardSpawnEvent;
import dk.itu.cs.isa.whiteboard.Photo;
import dk.itu.cs.isa.whiteboard.PhotoAddOperation;
import dk.itu.cs.isa.whiteboard.PhotoMoveOperation;
import dk.itu.cs.isa.whiteboard.PhotoRemoveOperation;
import dk.itu.cs.isa.whiteboard.Pin;
import dk.itu.cs.isa.whiteboard.PinSoftwareAddOperation;
import dk.itu.cs.isa.whiteboard.PinAudioAddOperation;
import dk.itu.cs.isa.whiteboard.PinMoveOperation;
import dk.itu.cs.isa.whiteboard.PinRemoveOperation;
import dk.itu.cs.isa.whiteboard.PinSplitOperation;
import dk.itu.cs.isa.whiteboard.PinsRemoveAllOperation;
import dk.itu.cs.isa.whiteboard.RedoEvent;
import dk.itu.cs.isa.whiteboard.UndoEvent;
import dk.itu.cs.isa.whiteboard.Whiteboard;
import dk.itu.cs.isa.whiteboard.WhiteboardFactory;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardHistoryUtil;
import dk.itu.cs.isa.whiteboard.WhiteboardStorage;

public class WhiteboardEventHandler {
	
	private enum ExecutionMode {
		EXECUTE, UNDO, REDO
	}
	
	
	
	private Whiteboard whiteboard;
	
	private WhiteboardOperationConsistencyChecker checker;
	
	
	
	public WhiteboardEventHandler(String systemName, int whiteboardId) {
		checker = new WhiteboardOperationConsistencyChecker(this);
		whiteboard = WhiteboardStorage.INSTANCE.getWhiteboardByID(systemName, whiteboardId);
	}
	
	public Whiteboard getWhiteboard() {
		return this.whiteboard;
	}
	
	
	
	public void handleEvent(String systemName, WhiteboardHistory historyForSystem, AbstractWhiteboardEvent submittedOperation) throws Exception {
		checker.check(historyForSystem, submittedOperation);
		AbstractWhiteboardEvent eventToHandle;
		ExecutionMode mode;
		int undoDepthChange;
		
		if(submittedOperation instanceof UndoEvent) {
			undoDepthChange = +1;
			mode = ExecutionMode.UNDO;
			eventToHandle = findOperationOnStack(historyForSystem, whiteboard.getUndoDepth());
		}
		else if(submittedOperation instanceof RedoEvent) {
			undoDepthChange = -1;
			mode = ExecutionMode.REDO;
			eventToHandle = findOperationOnStack(historyForSystem, whiteboard.getUndoDepth() - 1);
		}
		else {
			undoDepthChange = 0;
			mode = ExecutionMode.EXECUTE;
			eventToHandle = submittedOperation;
		}
		
		if(eventToHandle instanceof BoardSpawnEvent)
			handleEvent(systemName, historyForSystem, (BoardSpawnEvent) eventToHandle, mode);
		else if(eventToHandle instanceof BoardDestroyEvent)
			handleEvent(systemName, historyForSystem, (BoardDestroyEvent) eventToHandle, mode);
		else if(eventToHandle instanceof BoardDuplicateEvent)
			handleEvent(systemName, historyForSystem, (BoardDuplicateEvent) eventToHandle, mode);
		else if(eventToHandle instanceof BoardEnlargeOperation)
			handleEvent(systemName, historyForSystem, (BoardEnlargeOperation) eventToHandle, mode);
		
		else if(eventToHandle instanceof PinSplitOperation)
			handleEvent(systemName, historyForSystem, (PinSplitOperation) eventToHandle, mode);
		else if(eventToHandle instanceof PinsRemoveAllOperation)
			handleEvent(systemName, historyForSystem, (PinsRemoveAllOperation) eventToHandle, mode);
		else if(eventToHandle instanceof PinSoftwareAddOperation)
			handleEvent(systemName, historyForSystem, (PinSoftwareAddOperation) eventToHandle, mode);
		else if(eventToHandle instanceof PinAudioAddOperation)
			handleEvent(systemName, historyForSystem, (PinAudioAddOperation) eventToHandle, mode);
		else if(eventToHandle instanceof PinRemoveOperation)
			handleEvent(systemName, historyForSystem, (PinRemoveOperation) eventToHandle, mode);
		else if(eventToHandle instanceof PinMoveOperation)
			handleEvent(systemName, historyForSystem, (PinMoveOperation) eventToHandle, mode);
		
//		else if(eventToHandle instanceof AbstractDrawOperation)
//			handleEvent(systemName, historyForSystem, (AbstractDrawOperation) eventToHandle, mode);

		else if(eventToHandle instanceof PhotoAddOperation)
			handleEvent(systemName, historyForSystem, (PhotoAddOperation) eventToHandle, mode);
		else if(eventToHandle instanceof PhotoMoveOperation)
			handleEvent(systemName, historyForSystem, (PhotoMoveOperation) eventToHandle, mode);
		else if(eventToHandle instanceof PhotoRemoveOperation)
			handleEvent(systemName, historyForSystem, (PhotoRemoveOperation) eventToHandle, mode);
		
		if(undoDepthChange == 0)
			whiteboard.setUndoDepth(0);
		else
			whiteboard.setUndoDepth(whiteboard.getUndoDepth() + undoDepthChange);
	}

	private AbstractOperation findOperationOnStack(WhiteboardHistory historyForSystem, int stepsToGoBackFromUndoStack) {
		if(whiteboard == null)
			throw new NullPointerException("Cannot find operation in event log, because the whiteboard was not set.");
		
		int currentDepth = 0;
		for (int i = historyForSystem.getEventLog().size() - 1; i >= 0; i--) {
			AbstractWhiteboardEvent event = historyForSystem.getEventLog().get(i);
			if(event.getWhiteboardId() != whiteboard.getId())
				continue;
			if(!(event instanceof AbstractOperation))
				continue;
			
			if(currentDepth == stepsToGoBackFromUndoStack)
				return (AbstractOperation)event;
			currentDepth ++;
		}
		throw new NullPointerException("Did not find an operation on the stack based on current undo location: is the event log corrupted?");
	}
	
	

	private void handleEvent(String systemName, WhiteboardHistory history, BoardSpawnEvent eventToHandle, ExecutionMode mode) {
		if(mode != ExecutionMode.EXECUTE)
			throw new UnsupportedOperationException("Undoing or redoing a board spawn event is not possible");
		if(whiteboard != null)
			throw new UnsupportedOperationException("A whiteboard already exist - it cannot be spawned again");
		
		whiteboard = WhiteboardFactory.eINSTANCE.createWhiteboard();
		whiteboard.setId(eventToHandle.getWhiteboardId());
		whiteboard.setPosition(EcoreUtil.copy(eventToHandle.getWorldSpacePosition()));
		whiteboard.setRotation(EcoreUtil.copy(eventToHandle.getWorldSpaceRotation()));
		
		history.getWhiteboards().add(whiteboard);
	}

	private void handleEvent(String systemName, WhiteboardHistory history, BoardDuplicateEvent duplicateOperation, ExecutionMode mode) throws Exception {
		if(mode != ExecutionMode.EXECUTE)
			throw new UnsupportedOperationException("Undoing or redoing a board spawn event is not possible");

		int newWhiteboardId = WhiteboardStorage.INSTANCE.findAvailableWhiteboardId(systemName);
		
		List<AbstractWhiteboardEvent> copiedEvents = new ArrayList<>();
		Map<Integer, Integer> pinIdReplacementTable = new HashMap<>();
		for(AbstractWhiteboardEvent event : history.getEventLog()) {
			if(event instanceof BoardDuplicateEvent)
				continue;
			if(event.getWhiteboardId() != duplicateOperation.getWhiteboardId())
				continue;
			
			// copy this one!
			AbstractWhiteboardEvent copiedEvent = EcoreUtil.copy(event);
			copiedEvents.add(copiedEvent);

			if(copiedEvent instanceof BoardSpawnEvent) {
				((BoardSpawnEvent)copiedEvent).setWorldSpacePosition(EcoreUtil.copy(duplicateOperation.getWorldSpacePosition()));
				((BoardSpawnEvent)copiedEvent).setWorldSpaceRotation(EcoreUtil.copy(duplicateOperation.getWorldSpaceRotation()));
			}
			
			adjustInformation(systemName, duplicateOperation.getSubmittingClientId(), newWhiteboardId, pinIdReplacementTable, copiedEvent);
			if(copiedEvent instanceof AbstractCompoundOperation)
				for(AbstractOperation subOp : ((AbstractCompoundOperation) copiedEvent).getSubOperations())
					adjustInformation(systemName, duplicateOperation.getSubmittingClientId(), newWhiteboardId, pinIdReplacementTable, subOp);
		}
		
		copiedEvents.get(copiedEvents.size()-1).setConfirmationRequired(true);
		
		WhiteboardEventHandler opHandler = WhiteboardStorage.INSTANCE.getWhiteboardOperationHandler(systemName, newWhiteboardId);
		for(AbstractWhiteboardEvent copiedEvent : copiedEvents) {
			opHandler.handleEvent(systemName, history, copiedEvent);
			history.getEventLog().add(copiedEvent);
		}
	}
	
	private void adjustInformation(String systemName, int submittingClientId, int newWhiteboardId, Map<Integer, Integer> pinIdReplacementTable, AbstractWhiteboardEvent copiedEvent) {
		copiedEvent.setTimestamp(System.currentTimeMillis());
		copiedEvent.setWhiteboardId(newWhiteboardId);
		copiedEvent.setSubmittingClientId(submittingClientId);
		
		if(copiedEvent instanceof PinSoftwareAddOperation) {
			int oldPinId = ((PinSoftwareAddOperation)copiedEvent).getPinId();
			int newPinId = WhiteboardStorage.INSTANCE.findAvailablePinId(systemName);
			pinIdReplacementTable.put(oldPinId, newPinId);
		}
		
		if(copiedEvent instanceof AbstractPinOperation) {
			int oldPinId = ((AbstractPinOperation)copiedEvent).getPinId();
			((AbstractPinOperation)copiedEvent).setPinId(pinIdReplacementTable.get(oldPinId));
		}
		else if(copiedEvent instanceof PinSplitOperation) {
			int oldPinId = ((PinSplitOperation)copiedEvent).getPinId();
			((PinSplitOperation)copiedEvent).setPinId(pinIdReplacementTable.get(oldPinId));
		}
	}

	private void handleEvent(String systemName, WhiteboardHistory history, BoardDestroyEvent eventToHandle, ExecutionMode mode) {
		if(mode != ExecutionMode.EXECUTE)
			throw new UnsupportedOperationException("Undoing or redoing a board spawn event is not possible");
		WhiteboardStorage.INSTANCE.removeWhiteboardOperationHandler(systemName, whiteboard.getId());
		history.getWhiteboards().remove(whiteboard);
	}



	private void handleEvent(String systemName, WhiteboardHistory history, PinSoftwareAddOperation operationToHandle, ExecutionMode mode) {
		if(operationToHandle.getPinId() <= 0)
			operationToHandle.setPinId(WhiteboardStorage.INSTANCE.findAvailablePinId(systemName));
		
		if(mode == ExecutionMode.EXECUTE || mode == ExecutionMode.REDO)
			WhiteboardHistoryUtil.addPin(whiteboard, operationToHandle.getPinId(),
					operationToHandle.getPinnedElementName(), EcoreUtil.copy(operationToHandle.getColor()),
					EcoreUtil.copy(operationToHandle.getTextureSpacePosition()), EcoreUtil.copy(operationToHandle.getLocalSpacePosition()));
		else if(mode == ExecutionMode.UNDO)
			WhiteboardHistoryUtil.removePin(whiteboard, operationToHandle.getPinId());
	}

	private void handleEvent(String systemName, WhiteboardHistory history, PinAudioAddOperation operationToHandle, ExecutionMode mode) {
		if(operationToHandle.getPinId() <= 0)
			operationToHandle.setPinId(WhiteboardStorage.INSTANCE.findAvailablePinId(systemName));
		
		if(mode == ExecutionMode.EXECUTE || mode == ExecutionMode.REDO)
			WhiteboardHistoryUtil.addPin(whiteboard, operationToHandle.getPinId(),
					operationToHandle.getPinnedElementName(), EcoreUtil.copy(operationToHandle.getColor()),
					EcoreUtil.copy(operationToHandle.getTextureSpacePosition()), EcoreUtil.copy(operationToHandle.getLocalSpacePosition()));
		else if(mode == ExecutionMode.UNDO)
			WhiteboardHistoryUtil.removePin(whiteboard, operationToHandle.getPinId());
	}
	
	private void handleEvent(String systemName, WhiteboardHistory history, PinRemoveOperation operationToHandle, ExecutionMode mode) {
		if(mode == ExecutionMode.EXECUTE || mode == ExecutionMode.REDO) {
			Pin pin = WhiteboardHistoryUtil.getPinOnWhiteboard(whiteboard, operationToHandle.getPinId());
			if(operationToHandle.getColor() == null)
				operationToHandle.setColor(EcoreUtil.copy(pin.getColor()));
			if(operationToHandle.getTextureSpacePosition() == null)
				operationToHandle.setTextureSpacePosition(EcoreUtil.copy(pin.getTextureSpacePosition()));
			WhiteboardHistoryUtil.removePin(whiteboard, operationToHandle.getPinId());
		}
		else if(mode == ExecutionMode.UNDO)
			WhiteboardHistoryUtil.addPin(whiteboard, operationToHandle.getPinId(),
					operationToHandle.getPinnedElementName(), EcoreUtil.copy(operationToHandle.getColor()),
					EcoreUtil.copy(operationToHandle.getTextureSpacePosition()), EcoreUtil.copy(operationToHandle.getLocalSpacePosition()));
	}
	
	private void handleEvent(String systemName, WhiteboardHistory history, PinsRemoveAllOperation operationToHandle, ExecutionMode mode) {
		for(AbstractOperation subOperation : operationToHandle.getSubOperations()) {
			if(subOperation instanceof PinRemoveOperation)
				handleEvent(systemName, history, (PinRemoveOperation) subOperation, mode);
			else throw new UnsupportedOperationException("Found an unexpected kind of operation in a pins remove all operation: " + subOperation.getClass().getSimpleName());
		}
	}
	
	private void handleEvent(String systemName, WhiteboardHistory history, PinMoveOperation operationToHandle, ExecutionMode mode) {
		if(mode == ExecutionMode.EXECUTE || mode == ExecutionMode.REDO) {
			Pin pin = WhiteboardHistoryUtil.getPinOnWhiteboard(whiteboard, operationToHandle.getPinId());
			if(operationToHandle.getPreTextureSpacePosition() == null)
				operationToHandle.setPreTextureSpacePosition(EcoreUtil.copy(pin.getTextureSpacePosition()));
			pin.setTextureSpacePosition(EcoreUtil.copy(operationToHandle.getTextureSpacePosition()));
		}
		else if(mode == ExecutionMode.UNDO) {
			Pin pin = WhiteboardHistoryUtil.getPinOnWhiteboard(whiteboard, operationToHandle.getPinId());
			pin.setTextureSpacePosition(EcoreUtil.copy(operationToHandle.getPreTextureSpacePosition()));
		}
	}
	
	private void handleEvent(String systemName, WhiteboardHistory history, PinSplitOperation operationToHandle, ExecutionMode mode) throws Exception {
		for(AbstractOperation subOperation : operationToHandle.getSubOperations()) {
			if(subOperation instanceof PinSoftwareAddOperation)
				handleEvent(systemName, history, (PinSoftwareAddOperation) subOperation, mode);
			else if(subOperation instanceof PinMoveOperation)
				handleEvent(systemName, history, (PinMoveOperation) subOperation, mode);
			else if(subOperation instanceof PinRemoveOperation)
				handleEvent(systemName, history, (PinRemoveOperation) subOperation, mode);
			else throw new UnsupportedOperationException("Found an unexpected kind of operation in a pin split operation: " + subOperation.getClass().getSimpleName());
		}
	}

	
	
//	private void handleEvent(String systemName, WhiteboardHistory history, AbstractDrawOperation operationToHandle, ExecutionMode mode) {
//		if(mode == ExecutionMode.EXECUTE || mode == ExecutionMode.REDO)
//			whiteboard.setBitmapBase64(operationToHandle.getPostBitmapBase64());
//		else if(mode == ExecutionMode.UNDO)
//			whiteboard.setBitmapBase64(operationToHandle.getPreBitmapBase64());
//	}
	
	private void handleEvent(String systemName, WhiteboardHistory history, BoardEnlargeOperation operationToHandle, ExecutionMode mode) {
		if(mode == ExecutionMode.EXECUTE || mode == ExecutionMode.REDO)
			whiteboard.setSize(whiteboard.getSize() + 1);
		else if(mode == ExecutionMode.UNDO)
			whiteboard.setSize(whiteboard.getSize() - 1);
		
//		handleEvent(systemName, history, (AbstractDrawOperation) operationToHandle, mode);
	}



	private void handleEvent(String systemName, WhiteboardHistory history, PhotoAddOperation operationToHandle, ExecutionMode mode) {
		if(operationToHandle.getPhotoId() <= 0)
			operationToHandle.setPhotoId(WhiteboardStorage.INSTANCE.findAvailablePhotoId(systemName));
		
		if(mode == ExecutionMode.EXECUTE || mode == ExecutionMode.REDO)
			WhiteboardHistoryUtil.addPhoto(whiteboard, operationToHandle.getPhotoId(), operationToHandle.getBitmapBase64(),
					EcoreUtil.copy(operationToHandle.getTextureSpacePosition()), EcoreUtil.copy(operationToHandle.getLocalSpacePosition()),
					EcoreUtil.copy(operationToHandle.getLocalSpaceForwardDirection()), EcoreUtil.copy(operationToHandle.getLocalSpaceUpwardDirection())/*,
					EcoreUtil.copy(operationToHandle.getSystemPositionWhenPhotoWasTaken()), operationToHandle.getSystemSizeWhenPhotoWasTaken(),
					EcoreUtil.copyAll(operationToHandle.getRelationsVisibleWhenPhotoWasTaken())*/);
		else if(mode == ExecutionMode.UNDO)
			WhiteboardHistoryUtil.removePhoto(whiteboard, operationToHandle.getPhotoId());
	}
	
	private void handleEvent(String systemName, WhiteboardHistory history, PhotoMoveOperation operationToHandle, ExecutionMode mode) {
		if(mode == ExecutionMode.EXECUTE || mode == ExecutionMode.REDO) {
			Photo photo = WhiteboardHistoryUtil.getPhotoOnWhiteboard(whiteboard, operationToHandle.getPhotoId());
			if(operationToHandle.getPreTextureSpacePosition() == null) {
				operationToHandle.setPreTextureSpacePosition(EcoreUtil.copy(photo.getTextureSpacePosition()));
				operationToHandle.setPreLocalSpacePosition(EcoreUtil.copy(photo.getLocalSpacePosition()));
				operationToHandle.setPreLocalSpaceForwardDirection(EcoreUtil.copy(photo.getLocalSpaceForwardDirection()));
				operationToHandle.setPreLocalSpaceUpwardDirection(EcoreUtil.copy(photo.getLocalSpaceUpwardDirection()));
			}
			photo.setTextureSpacePosition(EcoreUtil.copy(operationToHandle.getTextureSpacePosition()));
			photo.setLocalSpacePosition(EcoreUtil.copy(operationToHandle.getLocalSpacePosition()));
			photo.setLocalSpaceForwardDirection(EcoreUtil.copy(operationToHandle.getLocalSpaceForwardDirection()));
			photo.setLocalSpaceUpwardDirection(EcoreUtil.copy(operationToHandle.getLocalSpaceUpwardDirection()));
		}
		else if(mode == ExecutionMode.UNDO) {
			Photo photo = WhiteboardHistoryUtil.getPhotoOnWhiteboard(whiteboard, operationToHandle.getPhotoId());
			photo.setTextureSpacePosition(EcoreUtil.copy(operationToHandle.getPreTextureSpacePosition()));
			photo.setLocalSpacePosition(EcoreUtil.copy(operationToHandle.getPreLocalSpacePosition()));
			photo.setLocalSpaceForwardDirection(EcoreUtil.copy(operationToHandle.getPreLocalSpaceForwardDirection()));
			photo.setLocalSpaceUpwardDirection(EcoreUtil.copy(operationToHandle.getPreLocalSpaceUpwardDirection()));
		}
	}
	
	private void handleEvent(String systemName, WhiteboardHistory history, PhotoRemoveOperation operationToHandle, ExecutionMode mode) {
		if(mode == ExecutionMode.EXECUTE || mode == ExecutionMode.REDO) {
			Photo photo = WhiteboardHistoryUtil.getPhotoOnWhiteboard(whiteboard, operationToHandle.getPhotoId());
			if(operationToHandle.getBitmapBase64() == null) {
				operationToHandle.setBitmapBase64(photo.getBitmapBase64());
				operationToHandle.setTextureSpacePosition(EcoreUtil.copy(photo.getTextureSpacePosition()));
				operationToHandle.setLocalSpacePosition(EcoreUtil.copy(photo.getLocalSpacePosition()));
				operationToHandle.setLocalSpaceForwardDirection(EcoreUtil.copy(photo.getLocalSpaceForwardDirection()));
				operationToHandle.setLocalSpaceUpwardDirection(EcoreUtil.copy(photo.getLocalSpaceUpwardDirection()));
//				operationToHandle.setSystemPositionWhenPhotoWasTaken(EcoreUtil.copy(photo.getSystemPositionWhenPhotoWasTaken()));
//				operationToHandle.setSystemSizeWhenPhotoWasTaken(photo.getSystemSizeWhenPhotoWasTaken());
//				operationToHandle.getRelationsVisibleWhenPhotoWasTaken().addAll(EcoreUtil.copyAll(photo.getRelationsVisibleWhenPhotoWasTaken()));
			}
			WhiteboardHistoryUtil.removePhoto(whiteboard, operationToHandle.getPhotoId());
		}
		else if(mode == ExecutionMode.UNDO)
			WhiteboardHistoryUtil.addPhoto(whiteboard, operationToHandle.getPhotoId(), operationToHandle.getBitmapBase64(),
					EcoreUtil.copy(operationToHandle.getTextureSpacePosition()), EcoreUtil.copy(operationToHandle.getLocalSpacePosition()),
					EcoreUtil.copy(operationToHandle.getLocalSpaceForwardDirection()), EcoreUtil.copy(operationToHandle.getLocalSpaceUpwardDirection())/*,
					EcoreUtil.copy(operationToHandle.getSystemPositionWhenPhotoWasTaken()), operationToHandle.getSystemSizeWhenPhotoWasTaken(),
					EcoreUtil.copyAll(operationToHandle.getRelationsVisibleWhenPhotoWasTaken())*/);
	}
	
}





























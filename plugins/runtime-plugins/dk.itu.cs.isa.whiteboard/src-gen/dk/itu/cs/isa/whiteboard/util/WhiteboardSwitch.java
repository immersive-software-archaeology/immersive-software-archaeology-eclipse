/**
 */
package dk.itu.cs.isa.whiteboard.util;

import dk.itu.cs.isa.whiteboard.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage
 * @generated
 */
public class WhiteboardSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static WhiteboardPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhiteboardSwitch() {
		if (modelPackage == null) {
			modelPackage = WhiteboardPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case WhiteboardPackage.WHITEBOARD_HISTORY: {
				WhiteboardHistory whiteboardHistory = (WhiteboardHistory)theEObject;
				T result = caseWhiteboardHistory(whiteboardHistory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.WHITEBOARD: {
				Whiteboard whiteboard = (Whiteboard)theEObject;
				T result = caseWhiteboard(whiteboard);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PIN: {
				Pin pin = (Pin)theEObject;
				T result = casePin(pin);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PHOTO: {
				Photo photo = (Photo)theEObject;
				T result = casePhoto(photo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.ISA_VECTOR3: {
				ISAVector3 isaVector3 = (ISAVector3)theEObject;
				T result = caseISAVector3(isaVector3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.ISA_VECTOR2: {
				ISAVector2 isaVector2 = (ISAVector2)theEObject;
				T result = caseISAVector2(isaVector2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.ISA_COLOR: {
				ISAColor isaColor = (ISAColor)theEObject;
				T result = caseISAColor(isaColor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.ABSTRACT_WHITEBOARD_EVENT: {
				AbstractWhiteboardEvent abstractWhiteboardEvent = (AbstractWhiteboardEvent)theEObject;
				T result = caseAbstractWhiteboardEvent(abstractWhiteboardEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.BOARD_SPAWN_EVENT: {
				BoardSpawnEvent boardSpawnEvent = (BoardSpawnEvent)theEObject;
				T result = caseBoardSpawnEvent(boardSpawnEvent);
				if (result == null) result = caseAbstractWhiteboardEvent(boardSpawnEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.BOARD_DUPLICATE_EVENT: {
				BoardDuplicateEvent boardDuplicateEvent = (BoardDuplicateEvent)theEObject;
				T result = caseBoardDuplicateEvent(boardDuplicateEvent);
				if (result == null) result = caseAbstractWhiteboardEvent(boardDuplicateEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.BOARD_DESTROY_EVENT: {
				BoardDestroyEvent boardDestroyEvent = (BoardDestroyEvent)theEObject;
				T result = caseBoardDestroyEvent(boardDestroyEvent);
				if (result == null) result = caseAbstractWhiteboardEvent(boardDestroyEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.UNDO_EVENT: {
				UndoEvent undoEvent = (UndoEvent)theEObject;
				T result = caseUndoEvent(undoEvent);
				if (result == null) result = caseAbstractWhiteboardEvent(undoEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.REDO_EVENT: {
				RedoEvent redoEvent = (RedoEvent)theEObject;
				T result = caseRedoEvent(redoEvent);
				if (result == null) result = caseAbstractWhiteboardEvent(redoEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.ABSTRACT_OPERATION: {
				AbstractOperation abstractOperation = (AbstractOperation)theEObject;
				T result = caseAbstractOperation(abstractOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(abstractOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.ABSTRACT_COMPOUND_OPERATION: {
				AbstractCompoundOperation abstractCompoundOperation = (AbstractCompoundOperation)theEObject;
				T result = caseAbstractCompoundOperation(abstractCompoundOperation);
				if (result == null) result = caseAbstractOperation(abstractCompoundOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(abstractCompoundOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PIN_SPLIT_OPERATION: {
				PinSplitOperation pinSplitOperation = (PinSplitOperation)theEObject;
				T result = casePinSplitOperation(pinSplitOperation);
				if (result == null) result = caseAbstractCompoundOperation(pinSplitOperation);
				if (result == null) result = caseAbstractOperation(pinSplitOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(pinSplitOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PINS_REMOVE_ALL_OPERATION: {
				PinsRemoveAllOperation pinsRemoveAllOperation = (PinsRemoveAllOperation)theEObject;
				T result = casePinsRemoveAllOperation(pinsRemoveAllOperation);
				if (result == null) result = caseAbstractCompoundOperation(pinsRemoveAllOperation);
				if (result == null) result = caseAbstractOperation(pinsRemoveAllOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(pinsRemoveAllOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.ABSTRACT_PIN_OPERATION: {
				AbstractPinOperation abstractPinOperation = (AbstractPinOperation)theEObject;
				T result = caseAbstractPinOperation(abstractPinOperation);
				if (result == null) result = caseAbstractOperation(abstractPinOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(abstractPinOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PIN_SOFTWARE_ADD_OPERATION: {
				PinSoftwareAddOperation pinSoftwareAddOperation = (PinSoftwareAddOperation)theEObject;
				T result = casePinSoftwareAddOperation(pinSoftwareAddOperation);
				if (result == null) result = caseAbstractPinOperation(pinSoftwareAddOperation);
				if (result == null) result = caseAbstractOperation(pinSoftwareAddOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(pinSoftwareAddOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PIN_AUDIO_ADD_OPERATION: {
				PinAudioAddOperation pinAudioAddOperation = (PinAudioAddOperation)theEObject;
				T result = casePinAudioAddOperation(pinAudioAddOperation);
				if (result == null) result = caseAbstractPinOperation(pinAudioAddOperation);
				if (result == null) result = caseAbstractOperation(pinAudioAddOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(pinAudioAddOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PIN_REMOVE_OPERATION: {
				PinRemoveOperation pinRemoveOperation = (PinRemoveOperation)theEObject;
				T result = casePinRemoveOperation(pinRemoveOperation);
				if (result == null) result = caseAbstractPinOperation(pinRemoveOperation);
				if (result == null) result = caseAbstractOperation(pinRemoveOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(pinRemoveOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PIN_MOVE_OPERATION: {
				PinMoveOperation pinMoveOperation = (PinMoveOperation)theEObject;
				T result = casePinMoveOperation(pinMoveOperation);
				if (result == null) result = caseAbstractPinOperation(pinMoveOperation);
				if (result == null) result = caseAbstractOperation(pinMoveOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(pinMoveOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.ABSTRACT_DRAW_OPERATION: {
				AbstractDrawOperation abstractDrawOperation = (AbstractDrawOperation)theEObject;
				T result = caseAbstractDrawOperation(abstractDrawOperation);
				if (result == null) result = caseAbstractOperation(abstractDrawOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(abstractDrawOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.DRAW_FREEHAND_OPERATION: {
				DrawFreehandOperation drawFreehandOperation = (DrawFreehandOperation)theEObject;
				T result = caseDrawFreehandOperation(drawFreehandOperation);
				if (result == null) result = caseAbstractDrawOperation(drawFreehandOperation);
				if (result == null) result = caseAbstractOperation(drawFreehandOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(drawFreehandOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.DRAW_MODULE_OPERATION: {
				DrawModuleOperation drawModuleOperation = (DrawModuleOperation)theEObject;
				T result = caseDrawModuleOperation(drawModuleOperation);
				if (result == null) result = caseAbstractDrawOperation(drawModuleOperation);
				if (result == null) result = caseAbstractOperation(drawModuleOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(drawModuleOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.DRAW_ARROW_OPERATION: {
				DrawArrowOperation drawArrowOperation = (DrawArrowOperation)theEObject;
				T result = caseDrawArrowOperation(drawArrowOperation);
				if (result == null) result = caseAbstractDrawOperation(drawArrowOperation);
				if (result == null) result = caseAbstractOperation(drawArrowOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(drawArrowOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PAINT_CLEAR_ALL_OPERATION: {
				PaintClearAllOperation paintClearAllOperation = (PaintClearAllOperation)theEObject;
				T result = casePaintClearAllOperation(paintClearAllOperation);
				if (result == null) result = caseAbstractDrawOperation(paintClearAllOperation);
				if (result == null) result = caseAbstractOperation(paintClearAllOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(paintClearAllOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PAINT_ERASE_OPERATION: {
				PaintEraseOperation paintEraseOperation = (PaintEraseOperation)theEObject;
				T result = casePaintEraseOperation(paintEraseOperation);
				if (result == null) result = caseAbstractDrawOperation(paintEraseOperation);
				if (result == null) result = caseAbstractOperation(paintEraseOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(paintEraseOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.BOARD_ENLARGE_OPERATION: {
				BoardEnlargeOperation boardEnlargeOperation = (BoardEnlargeOperation)theEObject;
				T result = caseBoardEnlargeOperation(boardEnlargeOperation);
				if (result == null) result = caseAbstractDrawOperation(boardEnlargeOperation);
				if (result == null) result = caseAbstractOperation(boardEnlargeOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(boardEnlargeOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.ABSTRACT_PHOTO_OPERATION: {
				AbstractPhotoOperation abstractPhotoOperation = (AbstractPhotoOperation)theEObject;
				T result = caseAbstractPhotoOperation(abstractPhotoOperation);
				if (result == null) result = caseAbstractOperation(abstractPhotoOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(abstractPhotoOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PHOTO_ADD_OPERATION: {
				PhotoAddOperation photoAddOperation = (PhotoAddOperation)theEObject;
				T result = casePhotoAddOperation(photoAddOperation);
				if (result == null) result = caseAbstractPhotoOperation(photoAddOperation);
				if (result == null) result = caseAbstractOperation(photoAddOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(photoAddOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PHOTO_MOVE_OPERATION: {
				PhotoMoveOperation photoMoveOperation = (PhotoMoveOperation)theEObject;
				T result = casePhotoMoveOperation(photoMoveOperation);
				if (result == null) result = caseAbstractPhotoOperation(photoMoveOperation);
				if (result == null) result = caseAbstractOperation(photoMoveOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(photoMoveOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.PHOTO_REMOVE_OPERATION: {
				PhotoRemoveOperation photoRemoveOperation = (PhotoRemoveOperation)theEObject;
				T result = casePhotoRemoveOperation(photoRemoveOperation);
				if (result == null) result = caseAbstractPhotoOperation(photoRemoveOperation);
				if (result == null) result = caseAbstractOperation(photoRemoveOperation);
				if (result == null) result = caseAbstractWhiteboardEvent(photoRemoveOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WhiteboardPackage.VISIBLE_RELATION: {
				VisibleRelation visibleRelation = (VisibleRelation)theEObject;
				T result = caseVisibleRelation(visibleRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>History</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>History</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhiteboardHistory(WhiteboardHistory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Whiteboard</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Whiteboard</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhiteboard(Whiteboard object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pin</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pin</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePin(Pin object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Photo</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Photo</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhoto(Photo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Vector3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Vector3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAVector3(ISAVector3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Vector2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Vector2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAVector2(ISAVector2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Color</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Color</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAColor(ISAColor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Whiteboard Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Whiteboard Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractWhiteboardEvent(AbstractWhiteboardEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Board Spawn Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Board Spawn Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoardSpawnEvent(BoardSpawnEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Board Duplicate Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Board Duplicate Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoardDuplicateEvent(BoardDuplicateEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Board Destroy Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Board Destroy Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoardDestroyEvent(BoardDestroyEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Undo Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Undo Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUndoEvent(UndoEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Redo Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Redo Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRedoEvent(RedoEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractOperation(AbstractOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Compound Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Compound Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractCompoundOperation(AbstractCompoundOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pin Split Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pin Split Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePinSplitOperation(PinSplitOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pins Remove All Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pins Remove All Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePinsRemoveAllOperation(PinsRemoveAllOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Pin Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Pin Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractPinOperation(AbstractPinOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pin Software Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pin Software Add Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePinSoftwareAddOperation(PinSoftwareAddOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pin Audio Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pin Audio Add Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePinAudioAddOperation(PinAudioAddOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pin Remove Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pin Remove Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePinRemoveOperation(PinRemoveOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pin Move Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pin Move Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePinMoveOperation(PinMoveOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Draw Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Draw Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractDrawOperation(AbstractDrawOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Draw Freehand Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Draw Freehand Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDrawFreehandOperation(DrawFreehandOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Draw Module Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Draw Module Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDrawModuleOperation(DrawModuleOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Draw Arrow Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Draw Arrow Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDrawArrowOperation(DrawArrowOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Paint Clear All Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Paint Clear All Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePaintClearAllOperation(PaintClearAllOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Paint Erase Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Paint Erase Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePaintEraseOperation(PaintEraseOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Board Enlarge Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Board Enlarge Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoardEnlargeOperation(BoardEnlargeOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Photo Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Photo Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractPhotoOperation(AbstractPhotoOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Photo Add Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Photo Add Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhotoAddOperation(PhotoAddOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Photo Move Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Photo Move Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhotoMoveOperation(PhotoMoveOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Photo Remove Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Photo Remove Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhotoRemoveOperation(PhotoRemoveOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Visible Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Visible Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVisibleRelation(VisibleRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //WhiteboardSwitch

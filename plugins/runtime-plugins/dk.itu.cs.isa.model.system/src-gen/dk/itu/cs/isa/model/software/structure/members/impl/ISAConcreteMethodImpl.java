/**
 */
package dk.itu.cs.isa.model.software.structure.members.impl;

import dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.members.MembersPackage;

import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifier;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Concrete Method</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getName <em>Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getIncomingReferences <em>Incoming References</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getModifiers <em>Modifiers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getNumberInOrderOfDeclaration <em>Number In Order Of Declaration</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getStartPositionInDocument <em>Start Position In Document</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getEndPositionInDocument <em>End Position In Document</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getType <em>Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#isArrayType <em>Array Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getNumberOfStatements <em>Number Of Statements</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getNumberOfExpressions <em>Number Of Expressions</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getNumberOfControlFlowSplits <em>Number Of Control Flow Splits</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.members.impl.ISAConcreteMethodImpl#getCognitiveComplexity <em>Cognitive Complexity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAConcreteMethodImpl extends ISAReferencingElementImpl implements ISAConcreteMethod {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIncomingReferences() <em>Incoming References</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<?>> incomingReferences;

	/**
	 * The cached value of the '{@link #getModifiers() <em>Modifiers</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAModifier> modifiers;

	/**
	 * The default value of the '{@link #getNumberInOrderOfDeclaration() <em>Number In Order Of Declaration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberInOrderOfDeclaration()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberInOrderOfDeclaration() <em>Number In Order Of Declaration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberInOrderOfDeclaration()
	 * @generated
	 * @ordered
	 */
	protected int numberInOrderOfDeclaration = NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartPositionInDocument() <em>Start Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected static final int START_POSITION_IN_DOCUMENT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStartPositionInDocument() <em>Start Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected int startPositionInDocument = START_POSITION_IN_DOCUMENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndPositionInDocument() <em>End Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected static final int END_POSITION_IN_DOCUMENT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEndPositionInDocument() <em>End Position In Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndPositionInDocument()
	 * @generated
	 * @ordered
	 */
	protected int endPositionInDocument = END_POSITION_IN_DOCUMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ISAType type;

	/**
	 * The default value of the '{@link #isArrayType() <em>Array Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArrayType()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ARRAY_TYPE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isArrayType() <em>Array Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArrayType()
	 * @generated
	 * @ordered
	 */
	protected boolean arrayType = ARRAY_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAParameter> parameters;

	/**
	 * The default value of the '{@link #getNumberOfStatements() <em>Number Of Statements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfStatements()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_STATEMENTS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfStatements() <em>Number Of Statements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfStatements()
	 * @generated
	 * @ordered
	 */
	protected int numberOfStatements = NUMBER_OF_STATEMENTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberOfExpressions() <em>Number Of Expressions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfExpressions()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_EXPRESSIONS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfExpressions() <em>Number Of Expressions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfExpressions()
	 * @generated
	 * @ordered
	 */
	protected int numberOfExpressions = NUMBER_OF_EXPRESSIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberOfControlFlowSplits() <em>Number Of Control Flow Splits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfControlFlowSplits()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfControlFlowSplits() <em>Number Of Control Flow Splits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfControlFlowSplits()
	 * @generated
	 * @ordered
	 */
	protected int numberOfControlFlowSplits = NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCognitiveComplexity() <em>Cognitive Complexity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCognitiveComplexity()
	 * @generated
	 * @ordered
	 */
	protected static final int COGNITIVE_COMPLEXITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCognitiveComplexity() <em>Cognitive Complexity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCognitiveComplexity()
	 * @generated
	 * @ordered
	 */
	protected int cognitiveComplexity = COGNITIVE_COMPLEXITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAConcreteMethodImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MembersPackage.Literals.ISA_CONCRETE_METHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<?>> getIncomingReferences() {
		if (incomingReferences == null) {
			incomingReferences = new EObjectWithInverseResolvingEList<ISAReference<?>>(ISAReference.class, this, MembersPackage.ISA_CONCRETE_METHOD__INCOMING_REFERENCES, MiscPackage.ISA_REFERENCE__TARGET);
		}
		return incomingReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAModifier> getModifiers() {
		if (modifiers == null) {
			modifiers = new EDataTypeUniqueEList<ISAModifier>(ISAModifier.class, this, MembersPackage.ISA_CONCRETE_METHOD__MODIFIERS);
		}
		return modifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberInOrderOfDeclaration() {
		return numberInOrderOfDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberInOrderOfDeclaration(int newNumberInOrderOfDeclaration) {
		int oldNumberInOrderOfDeclaration = numberInOrderOfDeclaration;
		numberInOrderOfDeclaration = newNumberInOrderOfDeclaration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__NUMBER_IN_ORDER_OF_DECLARATION, oldNumberInOrderOfDeclaration, numberInOrderOfDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getStartPositionInDocument() {
		return startPositionInDocument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartPositionInDocument(int newStartPositionInDocument) {
		int oldStartPositionInDocument = startPositionInDocument;
		startPositionInDocument = newStartPositionInDocument;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__START_POSITION_IN_DOCUMENT, oldStartPositionInDocument, startPositionInDocument));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getEndPositionInDocument() {
		return endPositionInDocument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndPositionInDocument(int newEndPositionInDocument) {
		int oldEndPositionInDocument = endPositionInDocument;
		endPositionInDocument = newEndPositionInDocument;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__END_POSITION_IN_DOCUMENT, oldEndPositionInDocument, endPositionInDocument));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (ISAType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MembersPackage.ISA_CONCRETE_METHOD__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetType(ISAType newType, NotificationChain msgs) {
		ISAType oldType = type;
		type = newType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__TYPE, oldType, newType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(ISAType newType) {
		if (newType != type) {
			NotificationChain msgs = null;
			if (type != null)
				msgs = ((InternalEObject)type).eInverseRemove(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
			if (newType != null)
				msgs = ((InternalEObject)newType).eInverseAdd(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
			msgs = basicSetType(newType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__TYPE, newType, newType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isArrayType() {
		return arrayType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setArrayType(boolean newArrayType) {
		boolean oldArrayType = arrayType;
		arrayType = newArrayType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__ARRAY_TYPE, oldArrayType, arrayType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAParameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<ISAParameter>(ISAParameter.class, this, MembersPackage.ISA_CONCRETE_METHOD__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfStatements() {
		return numberOfStatements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfStatements(int newNumberOfStatements) {
		int oldNumberOfStatements = numberOfStatements;
		numberOfStatements = newNumberOfStatements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_STATEMENTS, oldNumberOfStatements, numberOfStatements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfExpressions() {
		return numberOfExpressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfExpressions(int newNumberOfExpressions) {
		int oldNumberOfExpressions = numberOfExpressions;
		numberOfExpressions = newNumberOfExpressions;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_EXPRESSIONS, oldNumberOfExpressions, numberOfExpressions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfControlFlowSplits() {
		return numberOfControlFlowSplits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfControlFlowSplits(int newNumberOfControlFlowSplits) {
		int oldNumberOfControlFlowSplits = numberOfControlFlowSplits;
		numberOfControlFlowSplits = newNumberOfControlFlowSplits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_CONTROL_FLOW_SPLITS, oldNumberOfControlFlowSplits, numberOfControlFlowSplits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getCognitiveComplexity() {
		return cognitiveComplexity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCognitiveComplexity(int newCognitiveComplexity) {
		int oldCognitiveComplexity = cognitiveComplexity;
		cognitiveComplexity = newCognitiveComplexity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MembersPackage.ISA_CONCRETE_METHOD__COGNITIVE_COMPLEXITY, oldCognitiveComplexity, cognitiveComplexity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFullyQualifiedName() {
		return dk.itu.cs.isa.model.software.structure.util.ISASystemModelHelper.getFullyQualifiedName(this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ": " + (this.getName() == null ? "<unnamed>" : "\"" + this.getName() + "\"") + " [contained: " + (this.eContainer() != null) + "] @" + this.hashCode();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MembersPackage.ISA_CONCRETE_METHOD__INCOMING_REFERENCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncomingReferences()).basicAdd(otherEnd, msgs);
			case MembersPackage.ISA_CONCRETE_METHOD__TYPE:
				if (type != null)
					msgs = ((InternalEObject)type).eInverseRemove(this, MiscPackage.ISA_TYPE__USAGES_AS_TYPE, ISAType.class, msgs);
				return basicSetType((ISAType)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MembersPackage.ISA_CONCRETE_METHOD__INCOMING_REFERENCES:
				return ((InternalEList<?>)getIncomingReferences()).basicRemove(otherEnd, msgs);
			case MembersPackage.ISA_CONCRETE_METHOD__TYPE:
				return basicSetType(null, msgs);
			case MembersPackage.ISA_CONCRETE_METHOD__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MembersPackage.ISA_CONCRETE_METHOD__NAME:
				return getName();
			case MembersPackage.ISA_CONCRETE_METHOD__INCOMING_REFERENCES:
				return getIncomingReferences();
			case MembersPackage.ISA_CONCRETE_METHOD__MODIFIERS:
				return getModifiers();
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_IN_ORDER_OF_DECLARATION:
				return getNumberInOrderOfDeclaration();
			case MembersPackage.ISA_CONCRETE_METHOD__START_POSITION_IN_DOCUMENT:
				return getStartPositionInDocument();
			case MembersPackage.ISA_CONCRETE_METHOD__END_POSITION_IN_DOCUMENT:
				return getEndPositionInDocument();
			case MembersPackage.ISA_CONCRETE_METHOD__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case MembersPackage.ISA_CONCRETE_METHOD__ARRAY_TYPE:
				return isArrayType();
			case MembersPackage.ISA_CONCRETE_METHOD__PARAMETERS:
				return getParameters();
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_STATEMENTS:
				return getNumberOfStatements();
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_EXPRESSIONS:
				return getNumberOfExpressions();
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_CONTROL_FLOW_SPLITS:
				return getNumberOfControlFlowSplits();
			case MembersPackage.ISA_CONCRETE_METHOD__COGNITIVE_COMPLEXITY:
				return getCognitiveComplexity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MembersPackage.ISA_CONCRETE_METHOD__NAME:
				setName((String)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__INCOMING_REFERENCES:
				getIncomingReferences().clear();
				getIncomingReferences().addAll((Collection<? extends ISAReference<?>>)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__MODIFIERS:
				getModifiers().clear();
				getModifiers().addAll((Collection<? extends ISAModifier>)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_IN_ORDER_OF_DECLARATION:
				setNumberInOrderOfDeclaration((Integer)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__START_POSITION_IN_DOCUMENT:
				setStartPositionInDocument((Integer)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__END_POSITION_IN_DOCUMENT:
				setEndPositionInDocument((Integer)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__TYPE:
				setType((ISAType)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__ARRAY_TYPE:
				setArrayType((Boolean)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends ISAParameter>)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_STATEMENTS:
				setNumberOfStatements((Integer)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_EXPRESSIONS:
				setNumberOfExpressions((Integer)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_CONTROL_FLOW_SPLITS:
				setNumberOfControlFlowSplits((Integer)newValue);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__COGNITIVE_COMPLEXITY:
				setCognitiveComplexity((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_CONCRETE_METHOD__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__INCOMING_REFERENCES:
				getIncomingReferences().clear();
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__MODIFIERS:
				getModifiers().clear();
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_IN_ORDER_OF_DECLARATION:
				setNumberInOrderOfDeclaration(NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__START_POSITION_IN_DOCUMENT:
				setStartPositionInDocument(START_POSITION_IN_DOCUMENT_EDEFAULT);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__END_POSITION_IN_DOCUMENT:
				setEndPositionInDocument(END_POSITION_IN_DOCUMENT_EDEFAULT);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__TYPE:
				setType((ISAType)null);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__ARRAY_TYPE:
				setArrayType(ARRAY_TYPE_EDEFAULT);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__PARAMETERS:
				getParameters().clear();
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_STATEMENTS:
				setNumberOfStatements(NUMBER_OF_STATEMENTS_EDEFAULT);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_EXPRESSIONS:
				setNumberOfExpressions(NUMBER_OF_EXPRESSIONS_EDEFAULT);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_CONTROL_FLOW_SPLITS:
				setNumberOfControlFlowSplits(NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT);
				return;
			case MembersPackage.ISA_CONCRETE_METHOD__COGNITIVE_COMPLEXITY:
				setCognitiveComplexity(COGNITIVE_COMPLEXITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MembersPackage.ISA_CONCRETE_METHOD__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MembersPackage.ISA_CONCRETE_METHOD__INCOMING_REFERENCES:
				return incomingReferences != null && !incomingReferences.isEmpty();
			case MembersPackage.ISA_CONCRETE_METHOD__MODIFIERS:
				return modifiers != null && !modifiers.isEmpty();
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_IN_ORDER_OF_DECLARATION:
				return numberInOrderOfDeclaration != NUMBER_IN_ORDER_OF_DECLARATION_EDEFAULT;
			case MembersPackage.ISA_CONCRETE_METHOD__START_POSITION_IN_DOCUMENT:
				return startPositionInDocument != START_POSITION_IN_DOCUMENT_EDEFAULT;
			case MembersPackage.ISA_CONCRETE_METHOD__END_POSITION_IN_DOCUMENT:
				return endPositionInDocument != END_POSITION_IN_DOCUMENT_EDEFAULT;
			case MembersPackage.ISA_CONCRETE_METHOD__TYPE:
				return type != null;
			case MembersPackage.ISA_CONCRETE_METHOD__ARRAY_TYPE:
				return arrayType != ARRAY_TYPE_EDEFAULT;
			case MembersPackage.ISA_CONCRETE_METHOD__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_STATEMENTS:
				return numberOfStatements != NUMBER_OF_STATEMENTS_EDEFAULT;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_EXPRESSIONS:
				return numberOfExpressions != NUMBER_OF_EXPRESSIONS_EDEFAULT;
			case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_CONTROL_FLOW_SPLITS:
				return numberOfControlFlowSplits != NUMBER_OF_CONTROL_FLOW_SPLITS_EDEFAULT;
			case MembersPackage.ISA_CONCRETE_METHOD__COGNITIVE_COMPLEXITY:
				return cognitiveComplexity != COGNITIVE_COMPLEXITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISANamedSoftwareElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONCRETE_METHOD__NAME: return MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == ISAReferenceableElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONCRETE_METHOD__INCOMING_REFERENCES: return MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES;
				default: return -1;
			}
		}
		if (baseClass == ISAModifiableElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONCRETE_METHOD__MODIFIERS: return MiscPackage.ISA_MODIFIABLE_ELEMENT__MODIFIERS;
				default: return -1;
			}
		}
		if (baseClass == ISAMember.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_IN_ORDER_OF_DECLARATION: return MembersPackage.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION;
				case MembersPackage.ISA_CONCRETE_METHOD__START_POSITION_IN_DOCUMENT: return MembersPackage.ISA_MEMBER__START_POSITION_IN_DOCUMENT;
				case MembersPackage.ISA_CONCRETE_METHOD__END_POSITION_IN_DOCUMENT: return MembersPackage.ISA_MEMBER__END_POSITION_IN_DOCUMENT;
				default: return -1;
			}
		}
		if (baseClass == ISATypedElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONCRETE_METHOD__TYPE: return MiscPackage.ISA_TYPED_ELEMENT__TYPE;
				case MembersPackage.ISA_CONCRETE_METHOD__ARRAY_TYPE: return MiscPackage.ISA_TYPED_ELEMENT__ARRAY_TYPE;
				default: return -1;
			}
		}
		if (baseClass == ISAParametrizableElement.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONCRETE_METHOD__PARAMETERS: return MiscPackage.ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS;
				default: return -1;
			}
		}
		if (baseClass == ISAMethod.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ISAStatementContainer.class) {
			switch (derivedFeatureID) {
				case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_STATEMENTS: return MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS;
				case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_EXPRESSIONS: return MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS;
				case MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_CONTROL_FLOW_SPLITS: return MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS;
				case MembersPackage.ISA_CONCRETE_METHOD__COGNITIVE_COMPLEXITY: return MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISANamedSoftwareElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT__NAME: return MembersPackage.ISA_CONCRETE_METHOD__NAME;
				default: return -1;
			}
		}
		if (baseClass == ISAReferenceableElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_REFERENCEABLE_ELEMENT__INCOMING_REFERENCES: return MembersPackage.ISA_CONCRETE_METHOD__INCOMING_REFERENCES;
				default: return -1;
			}
		}
		if (baseClass == ISAModifiableElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_MODIFIABLE_ELEMENT__MODIFIERS: return MembersPackage.ISA_CONCRETE_METHOD__MODIFIERS;
				default: return -1;
			}
		}
		if (baseClass == ISAMember.class) {
			switch (baseFeatureID) {
				case MembersPackage.ISA_MEMBER__NUMBER_IN_ORDER_OF_DECLARATION: return MembersPackage.ISA_CONCRETE_METHOD__NUMBER_IN_ORDER_OF_DECLARATION;
				case MembersPackage.ISA_MEMBER__START_POSITION_IN_DOCUMENT: return MembersPackage.ISA_CONCRETE_METHOD__START_POSITION_IN_DOCUMENT;
				case MembersPackage.ISA_MEMBER__END_POSITION_IN_DOCUMENT: return MembersPackage.ISA_CONCRETE_METHOD__END_POSITION_IN_DOCUMENT;
				default: return -1;
			}
		}
		if (baseClass == ISATypedElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_TYPED_ELEMENT__TYPE: return MembersPackage.ISA_CONCRETE_METHOD__TYPE;
				case MiscPackage.ISA_TYPED_ELEMENT__ARRAY_TYPE: return MembersPackage.ISA_CONCRETE_METHOD__ARRAY_TYPE;
				default: return -1;
			}
		}
		if (baseClass == ISAParametrizableElement.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_PARAMETRIZABLE_ELEMENT__PARAMETERS: return MembersPackage.ISA_CONCRETE_METHOD__PARAMETERS;
				default: return -1;
			}
		}
		if (baseClass == ISAMethod.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ISAStatementContainer.class) {
			switch (baseFeatureID) {
				case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_STATEMENTS: return MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_STATEMENTS;
				case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_EXPRESSIONS: return MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_EXPRESSIONS;
				case MiscPackage.ISA_STATEMENT_CONTAINER__NUMBER_OF_CONTROL_FLOW_SPLITS: return MembersPackage.ISA_CONCRETE_METHOD__NUMBER_OF_CONTROL_FLOW_SPLITS;
				case MiscPackage.ISA_STATEMENT_CONTAINER__COGNITIVE_COMPLEXITY: return MembersPackage.ISA_CONCRETE_METHOD__COGNITIVE_COMPLEXITY;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == ISANamedSoftwareElement.class) {
			switch (baseOperationID) {
				case MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___GET_FULLY_QUALIFIED_NAME: return MembersPackage.ISA_CONCRETE_METHOD___GET_FULLY_QUALIFIED_NAME;
				case MiscPackage.ISA_NAMED_SOFTWARE_ELEMENT___TO_STRING: return MembersPackage.ISA_CONCRETE_METHOD___TO_STRING;
				default: return -1;
			}
		}
		if (baseClass == ISAReferenceableElement.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == ISAModifiableElement.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == ISAMember.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == ISATypedElement.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == ISAParametrizableElement.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == ISAMethod.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == ISAStatementContainer.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MembersPackage.ISA_CONCRETE_METHOD___GET_FULLY_QUALIFIED_NAME:
				return getFullyQualifiedName();
			case MembersPackage.ISA_CONCRETE_METHOD___TO_STRING:
				return toString();
		}
		return super.eInvoke(operationID, arguments);
	}

} //ISAConcreteMethodImpl

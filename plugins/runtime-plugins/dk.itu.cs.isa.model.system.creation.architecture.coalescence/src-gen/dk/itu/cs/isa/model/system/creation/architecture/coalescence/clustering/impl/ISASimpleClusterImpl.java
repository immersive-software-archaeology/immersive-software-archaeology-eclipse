/**
 */
package dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl;

import dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringPackage;
import dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Simple Cluster</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ISASimpleClusterImpl#getElementNames <em>Element Names</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.impl.ISASimpleClusterImpl#getSubClusters <em>Sub Clusters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISASimpleClusterImpl extends MinimalEObjectImpl.Container implements ISASimpleCluster {
	/**
	 * The cached value of the '{@link #getElementNames() <em>Element Names</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementNames()
	 * @generated
	 * @ordered
	 */
	protected EList<String> elementNames;

	/**
	 * The cached value of the '{@link #getSubClusters() <em>Sub Clusters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubClusters()
	 * @generated
	 * @ordered
	 */
	protected EList<ISASimpleCluster> subClusters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISASimpleClusterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClusteringPackage.Literals.ISA_SIMPLE_CLUSTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getElementNames() {
		if (elementNames == null) {
			elementNames = new EDataTypeUniqueEList<String>(String.class, this, ClusteringPackage.ISA_SIMPLE_CLUSTER__ELEMENT_NAMES);
		}
		return elementNames;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISASimpleCluster> getSubClusters() {
		if (subClusters == null) {
			subClusters = new EObjectContainmentEList<ISASimpleCluster>(ISASimpleCluster.class, this, ClusteringPackage.ISA_SIMPLE_CLUSTER__SUB_CLUSTERS);
		}
		return subClusters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClusteringPackage.ISA_SIMPLE_CLUSTER__SUB_CLUSTERS:
				return ((InternalEList<?>)getSubClusters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClusteringPackage.ISA_SIMPLE_CLUSTER__ELEMENT_NAMES:
				return getElementNames();
			case ClusteringPackage.ISA_SIMPLE_CLUSTER__SUB_CLUSTERS:
				return getSubClusters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClusteringPackage.ISA_SIMPLE_CLUSTER__ELEMENT_NAMES:
				getElementNames().clear();
				getElementNames().addAll((Collection<? extends String>)newValue);
				return;
			case ClusteringPackage.ISA_SIMPLE_CLUSTER__SUB_CLUSTERS:
				getSubClusters().clear();
				getSubClusters().addAll((Collection<? extends ISASimpleCluster>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClusteringPackage.ISA_SIMPLE_CLUSTER__ELEMENT_NAMES:
				getElementNames().clear();
				return;
			case ClusteringPackage.ISA_SIMPLE_CLUSTER__SUB_CLUSTERS:
				getSubClusters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClusteringPackage.ISA_SIMPLE_CLUSTER__ELEMENT_NAMES:
				return elementNames != null && !elementNames.isEmpty();
			case ClusteringPackage.ISA_SIMPLE_CLUSTER__SUB_CLUSTERS:
				return subClusters != null && !subClusters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (elementNames: ");
		result.append(elementNames);
		result.append(')');
		return result.toString();
	}

} //ISASimpleClusterImpl

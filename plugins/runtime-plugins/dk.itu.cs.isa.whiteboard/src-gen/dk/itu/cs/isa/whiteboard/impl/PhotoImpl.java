/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.ISAVector2;
import dk.itu.cs.isa.whiteboard.ISAVector3;
import dk.itu.cs.isa.whiteboard.Photo;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Photo</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoImpl#getId <em>Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoImpl#getTextureSpacePosition <em>Texture Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoImpl#getLocalSpacePosition <em>Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoImpl#getLocalSpaceForwardDirection <em>Local Space Forward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoImpl#getLocalSpaceUpwardDirection <em>Local Space Upward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.impl.PhotoImpl#getBitmapBase64 <em>Bitmap Base64</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PhotoImpl extends MinimalEObjectImpl.Container implements Photo {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTextureSpacePosition() <em>Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTextureSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector2 textureSpacePosition;

	/**
	 * The cached value of the '{@link #getLocalSpacePosition() <em>Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSpacePosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 localSpacePosition;

	/**
	 * The cached value of the '{@link #getLocalSpaceForwardDirection() <em>Local Space Forward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSpaceForwardDirection()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 localSpaceForwardDirection;

	/**
	 * The cached value of the '{@link #getLocalSpaceUpwardDirection() <em>Local Space Upward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalSpaceUpwardDirection()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 localSpaceUpwardDirection;

	/**
	 * The default value of the '{@link #getBitmapBase64() <em>Bitmap Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitmapBase64()
	 * @generated
	 * @ordered
	 */
	protected static final String BITMAP_BASE64_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBitmapBase64() <em>Bitmap Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitmapBase64()
	 * @generated
	 * @ordered
	 */
	protected String bitmapBase64 = BITMAP_BASE64_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhotoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WhiteboardPackage.Literals.PHOTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector2 getTextureSpacePosition() {
		return textureSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTextureSpacePosition(ISAVector2 newTextureSpacePosition, NotificationChain msgs) {
		ISAVector2 oldTextureSpacePosition = textureSpacePosition;
		textureSpacePosition = newTextureSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__TEXTURE_SPACE_POSITION, oldTextureSpacePosition, newTextureSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTextureSpacePosition(ISAVector2 newTextureSpacePosition) {
		if (newTextureSpacePosition != textureSpacePosition) {
			NotificationChain msgs = null;
			if (textureSpacePosition != null)
				msgs = ((InternalEObject)textureSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO__TEXTURE_SPACE_POSITION, null, msgs);
			if (newTextureSpacePosition != null)
				msgs = ((InternalEObject)newTextureSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO__TEXTURE_SPACE_POSITION, null, msgs);
			msgs = basicSetTextureSpacePosition(newTextureSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__TEXTURE_SPACE_POSITION, newTextureSpacePosition, newTextureSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getLocalSpacePosition() {
		return localSpacePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocalSpacePosition(ISAVector3 newLocalSpacePosition, NotificationChain msgs) {
		ISAVector3 oldLocalSpacePosition = localSpacePosition;
		localSpacePosition = newLocalSpacePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__LOCAL_SPACE_POSITION, oldLocalSpacePosition, newLocalSpacePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalSpacePosition(ISAVector3 newLocalSpacePosition) {
		if (newLocalSpacePosition != localSpacePosition) {
			NotificationChain msgs = null;
			if (localSpacePosition != null)
				msgs = ((InternalEObject)localSpacePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO__LOCAL_SPACE_POSITION, null, msgs);
			if (newLocalSpacePosition != null)
				msgs = ((InternalEObject)newLocalSpacePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO__LOCAL_SPACE_POSITION, null, msgs);
			msgs = basicSetLocalSpacePosition(newLocalSpacePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__LOCAL_SPACE_POSITION, newLocalSpacePosition, newLocalSpacePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getLocalSpaceForwardDirection() {
		return localSpaceForwardDirection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocalSpaceForwardDirection(ISAVector3 newLocalSpaceForwardDirection, NotificationChain msgs) {
		ISAVector3 oldLocalSpaceForwardDirection = localSpaceForwardDirection;
		localSpaceForwardDirection = newLocalSpaceForwardDirection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__LOCAL_SPACE_FORWARD_DIRECTION, oldLocalSpaceForwardDirection, newLocalSpaceForwardDirection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalSpaceForwardDirection(ISAVector3 newLocalSpaceForwardDirection) {
		if (newLocalSpaceForwardDirection != localSpaceForwardDirection) {
			NotificationChain msgs = null;
			if (localSpaceForwardDirection != null)
				msgs = ((InternalEObject)localSpaceForwardDirection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO__LOCAL_SPACE_FORWARD_DIRECTION, null, msgs);
			if (newLocalSpaceForwardDirection != null)
				msgs = ((InternalEObject)newLocalSpaceForwardDirection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO__LOCAL_SPACE_FORWARD_DIRECTION, null, msgs);
			msgs = basicSetLocalSpaceForwardDirection(newLocalSpaceForwardDirection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__LOCAL_SPACE_FORWARD_DIRECTION, newLocalSpaceForwardDirection, newLocalSpaceForwardDirection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getLocalSpaceUpwardDirection() {
		return localSpaceUpwardDirection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocalSpaceUpwardDirection(ISAVector3 newLocalSpaceUpwardDirection, NotificationChain msgs) {
		ISAVector3 oldLocalSpaceUpwardDirection = localSpaceUpwardDirection;
		localSpaceUpwardDirection = newLocalSpaceUpwardDirection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__LOCAL_SPACE_UPWARD_DIRECTION, oldLocalSpaceUpwardDirection, newLocalSpaceUpwardDirection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalSpaceUpwardDirection(ISAVector3 newLocalSpaceUpwardDirection) {
		if (newLocalSpaceUpwardDirection != localSpaceUpwardDirection) {
			NotificationChain msgs = null;
			if (localSpaceUpwardDirection != null)
				msgs = ((InternalEObject)localSpaceUpwardDirection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO__LOCAL_SPACE_UPWARD_DIRECTION, null, msgs);
			if (newLocalSpaceUpwardDirection != null)
				msgs = ((InternalEObject)newLocalSpaceUpwardDirection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WhiteboardPackage.PHOTO__LOCAL_SPACE_UPWARD_DIRECTION, null, msgs);
			msgs = basicSetLocalSpaceUpwardDirection(newLocalSpaceUpwardDirection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__LOCAL_SPACE_UPWARD_DIRECTION, newLocalSpaceUpwardDirection, newLocalSpaceUpwardDirection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBitmapBase64() {
		return bitmapBase64;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitmapBase64(String newBitmapBase64) {
		String oldBitmapBase64 = bitmapBase64;
		bitmapBase64 = newBitmapBase64;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WhiteboardPackage.PHOTO__BITMAP_BASE64, oldBitmapBase64, bitmapBase64));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO__TEXTURE_SPACE_POSITION:
				return basicSetTextureSpacePosition(null, msgs);
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_POSITION:
				return basicSetLocalSpacePosition(null, msgs);
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_FORWARD_DIRECTION:
				return basicSetLocalSpaceForwardDirection(null, msgs);
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_UPWARD_DIRECTION:
				return basicSetLocalSpaceUpwardDirection(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO__ID:
				return getId();
			case WhiteboardPackage.PHOTO__TEXTURE_SPACE_POSITION:
				return getTextureSpacePosition();
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_POSITION:
				return getLocalSpacePosition();
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_FORWARD_DIRECTION:
				return getLocalSpaceForwardDirection();
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_UPWARD_DIRECTION:
				return getLocalSpaceUpwardDirection();
			case WhiteboardPackage.PHOTO__BITMAP_BASE64:
				return getBitmapBase64();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO__ID:
				setId((Integer)newValue);
				return;
			case WhiteboardPackage.PHOTO__TEXTURE_SPACE_POSITION:
				setTextureSpacePosition((ISAVector2)newValue);
				return;
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_POSITION:
				setLocalSpacePosition((ISAVector3)newValue);
				return;
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_FORWARD_DIRECTION:
				setLocalSpaceForwardDirection((ISAVector3)newValue);
				return;
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_UPWARD_DIRECTION:
				setLocalSpaceUpwardDirection((ISAVector3)newValue);
				return;
			case WhiteboardPackage.PHOTO__BITMAP_BASE64:
				setBitmapBase64((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO__ID:
				setId(ID_EDEFAULT);
				return;
			case WhiteboardPackage.PHOTO__TEXTURE_SPACE_POSITION:
				setTextureSpacePosition((ISAVector2)null);
				return;
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_POSITION:
				setLocalSpacePosition((ISAVector3)null);
				return;
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_FORWARD_DIRECTION:
				setLocalSpaceForwardDirection((ISAVector3)null);
				return;
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_UPWARD_DIRECTION:
				setLocalSpaceUpwardDirection((ISAVector3)null);
				return;
			case WhiteboardPackage.PHOTO__BITMAP_BASE64:
				setBitmapBase64(BITMAP_BASE64_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WhiteboardPackage.PHOTO__ID:
				return id != ID_EDEFAULT;
			case WhiteboardPackage.PHOTO__TEXTURE_SPACE_POSITION:
				return textureSpacePosition != null;
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_POSITION:
				return localSpacePosition != null;
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_FORWARD_DIRECTION:
				return localSpaceForwardDirection != null;
			case WhiteboardPackage.PHOTO__LOCAL_SPACE_UPWARD_DIRECTION:
				return localSpaceUpwardDirection != null;
			case WhiteboardPackage.PHOTO__BITMAP_BASE64:
				return BITMAP_BASE64_EDEFAULT == null ? bitmapBase64 != null : !BITMAP_BASE64_EDEFAULT.equals(bitmapBase64);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", bitmapBase64: ");
		result.append(bitmapBase64);
		result.append(')');
		return result.toString();
	}

} //PhotoImpl

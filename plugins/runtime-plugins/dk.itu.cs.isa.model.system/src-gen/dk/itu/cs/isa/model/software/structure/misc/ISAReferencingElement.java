/**
 */
package dk.itu.cs.isa.model.software.structure.misc;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;

import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Referencing Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getTypeReferences <em>Type References</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getConstructorCalls <em>Constructor Calls</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getMethodCalls <em>Method Calls</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getFieldAccesses <em>Field Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getConstantAccesses <em>Constant Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement#getComponentAccesses <em>Component Accesses</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReferencingElement()
 * @model abstract="true"
 * @generated
 */
public interface ISAReferencingElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Type References</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISAReference}<code>&lt;dk.itu.cs.isa.model.software.structure.misc.ISAType&gt;</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type References</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReferencingElement_TypeReferences()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAReference<ISAType>> getTypeReferences();

	/**
	 * Returns the value of the '<em><b>Constructor Calls</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISAReference}<code>&lt;dk.itu.cs.isa.model.software.structure.members.ISAConstructor&gt;</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constructor Calls</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReferencingElement_ConstructorCalls()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAReference<ISAConstructor>> getConstructorCalls();

	/**
	 * Returns the value of the '<em><b>Method Calls</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISAReference}<code>&lt;dk.itu.cs.isa.model.software.structure.members.ISAMethod&gt;</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method Calls</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReferencingElement_MethodCalls()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAReference<ISAMethod>> getMethodCalls();

	/**
	 * Returns the value of the '<em><b>Field Accesses</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISAReference}<code>&lt;dk.itu.cs.isa.model.software.structure.members.ISAField&gt;</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Field Accesses</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReferencingElement_FieldAccesses()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAReference<ISAField>> getFieldAccesses();

	/**
	 * Returns the value of the '<em><b>Constant Accesses</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISAReference}<code>&lt;dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant&gt;</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Accesses</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReferencingElement_ConstantAccesses()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAReference<ISAEnumConstant>> getConstantAccesses();

	/**
	 * Returns the value of the '<em><b>Component Accesses</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.model.software.structure.misc.ISAReference}<code>&lt;dk.itu.cs.isa.model.software.structure.members.ISARecordComponent&gt;</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Accesses</em>' containment reference list.
	 * @see dk.itu.cs.isa.model.software.structure.misc.MiscPackage#getISAReferencingElement_ComponentAccesses()
	 * @model containment="true"
	 * @generated
	 */
	EList<ISAReference<ISARecordComponent>> getComponentAccesses();

} // ISAReferencingElement

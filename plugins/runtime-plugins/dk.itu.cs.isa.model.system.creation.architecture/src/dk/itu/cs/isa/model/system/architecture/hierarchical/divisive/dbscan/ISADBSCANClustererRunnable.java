package dk.itu.cs.isa.model.system.architecture.hierarchical.divisive.dbscan;

import java.util.ArrayList;
import java.util.List;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;

public class ISADBSCANClustererRunnable implements Runnable {

	private ISADBSCANClustererManager clusteringManager;
	
	private List<Double> epsilonValuesToCover = new ArrayList<>();
	private int minumumClusterSize;
	
	private List<ISAClassifier> listOfAllClassifiers;
	
	

	public ISADBSCANClustererRunnable(ISADBSCANClustererManager clusteringManager,
			int threadIndex, int numberOfThreads,
			List<Double> epsilonValues, int minumumClusterSize,
			List<ISAClassifier> listOfAllClassifiers) {
		
		this.clusteringManager = clusteringManager;
		
		this.minumumClusterSize = minumumClusterSize;
		this.listOfAllClassifiers = listOfAllClassifiers;
		
		int startEpsilonIndex = (int) Math.floor((double)threadIndex * (1/(double)numberOfThreads) * (double)epsilonValues.size());
		int endEpsilonIndex = (int) Math.floor((double)(threadIndex+1) * (1/(double)numberOfThreads) * (double)epsilonValues.size());
		
		for(int i=startEpsilonIndex; i<endEpsilonIndex; i++) {
			epsilonValuesToCover.add(epsilonValues.get(i));
		}
	}
	
	
	
	@Override
	public void run() {
		for(double eps : epsilonValuesToCover) {
			ISADBSCANClusterAlgorithm algorithm = new ISADBSCANClusterAlgorithm(eps, minumumClusterSize);
			List<ISACluster> identifiedClusters = algorithm.cluster(listOfAllClassifiers);
//			ISACluster remainderCluster = createOutlierCluster(identifiedClusters);
			List<ISAClassifier> outliers = calculateOutliers(identifiedClusters);
			
			double ratioOfClusteredClassifiers = 1d - (double) outliers.size() / (double) listOfAllClassifiers.size();
			clusteringManager.reportBack(eps, identifiedClusters, outliers, ratioOfClusteredClassifiers);
			
			if(clusteringManager.getMonitor().isCanceled())
				return;
		}
	}
	
	private List<ISAClassifier> calculateOutliers(List<ISACluster> identifiedClusters) {
		// Make a copy of all classifiers
		List<ISAClassifier> unclusteredClassifiers = new ArrayList<>(listOfAllClassifiers);
		
		// Then delete those that were clustered
		for(ISACluster identifiedCluster : identifiedClusters)
			unclusteredClassifiers.removeAll(identifiedCluster.getClassifiers());
		
		return unclusteredClassifiers;
	}
	
	/**
	 * Puts all outliers in an outlier cluster
	 * @param identifiedClusters
	 * @param unmatchedClassifiers 
	 */
	@SuppressWarnings("unused")
	private ISACluster createOutlierCluster(List<ISACluster> identifiedClusters) {
		// Make a copy of all classifiers
		List<ISAClassifier> unclusteredClassifiers = new ArrayList<>(listOfAllClassifiers);
		
		// Then delete those that were clustered
		for(ISACluster identifiedCluster : identifiedClusters) {
			unclusteredClassifiers.removeAll(identifiedCluster.getClassifiers());
		}
		
		ISACluster outlierCluster = ArchitectureFactory.eINSTANCE.createISACluster();
		outlierCluster.setName("Outlier Cluster");
		outlierCluster.getClassifiers().addAll(unclusteredClassifiers);
		
		return outlierCluster;
	}

}












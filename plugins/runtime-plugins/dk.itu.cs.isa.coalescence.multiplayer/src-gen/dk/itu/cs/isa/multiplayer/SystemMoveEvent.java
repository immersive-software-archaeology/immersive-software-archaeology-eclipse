/**
 */
package dk.itu.cs.isa.multiplayer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System Move Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent#getPosition <em>Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent#getRotation <em>Rotation</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent#getScale <em>Scale</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getSystemMoveEvent()
 * @model
 * @generated
 */
public interface SystemMoveEvent extends AbstractEvent {
	/**
	 * Returns the value of the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' containment reference.
	 * @see #setPosition(ISAVector3)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getSystemMoveEvent_Position()
	 * @model containment="true"
	 * @generated
	 */
	ISAVector3 getPosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent#getPosition <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' containment reference.
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rotation</em>' containment reference.
	 * @see #setRotation(ISAVector3)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getSystemMoveEvent_Rotation()
	 * @model containment="true"
	 * @generated
	 */
	ISAVector3 getRotation();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent#getRotation <em>Rotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rotation</em>' containment reference.
	 * @see #getRotation()
	 * @generated
	 */
	void setRotation(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Scale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale</em>' attribute.
	 * @see #setScale(float)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getSystemMoveEvent_Scale()
	 * @model required="true"
	 * @generated
	 */
	float getScale();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.SystemMoveEvent#getScale <em>Scale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scale</em>' attribute.
	 * @see #getScale()
	 * @generated
	 */
	void setScale(float value);

} // SystemMoveEvent

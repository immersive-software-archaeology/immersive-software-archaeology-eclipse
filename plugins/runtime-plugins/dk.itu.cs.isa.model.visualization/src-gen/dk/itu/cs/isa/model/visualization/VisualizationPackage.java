/**
 */
package dk.itu.cs.isa.model.visualization;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.visualization.VisualizationFactory
 * @model kind="package"
 * @generated
 */
public interface VisualizationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "visualization";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/visualization";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "visualization";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VisualizationPackage eINSTANCE = dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISANamedVisualElementImpl <em>ISA Named Visual Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISANamedVisualElementImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISANamedVisualElement()
	 * @generated
	 */
	int ISA_NAMED_VISUAL_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_NAMED_VISUAL_ELEMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_NAMED_VISUAL_ELEMENT__QUALIFIED_NAME = 1;

	/**
	 * The number of structural features of the '<em>ISA Named Visual Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>ISA Named Visual Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_NAMED_VISUAL_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAVisualizationModelImpl <em>ISA Visualization Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISAVisualizationModelImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAVisualizationModel()
	 * @generated
	 */
	int ISA_VISUALIZATION_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VISUALIZATION_MODEL__NAME = ISA_NAMED_VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VISUALIZATION_MODEL__QUALIFIED_NAME = ISA_NAMED_VISUAL_ELEMENT__QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Subject System Capsule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Library Capsules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VISUALIZATION_MODEL__LIBRARY_CAPSULES = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Model Creator Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VISUALIZATION_MODEL__MODEL_CREATOR_NAME = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ISA Visualization Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VISUALIZATION_MODEL_FEATURE_COUNT = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>ISA Visualization Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VISUALIZATION_MODEL_OPERATION_COUNT = ISA_NAMED_VISUAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAVector3Impl <em>ISA Vector3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISAVector3Impl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAVector3()
	 * @generated
	 */
	int ISA_VECTOR3 = 2;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__Z = 2;

	/**
	 * The number of structural features of the '<em>ISA Vector3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>ISA Vector3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISATransparentCapsuleImpl <em>ISA Transparent Capsule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISATransparentCapsuleImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISATransparentCapsule()
	 * @generated
	 */
	int ISA_TRANSPARENT_CAPSULE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TRANSPARENT_CAPSULE__NAME = ISA_NAMED_VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TRANSPARENT_CAPSULE__QUALIFIED_NAME = ISA_NAMED_VISUAL_ELEMENT__QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Sub Capsules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TRANSPARENT_CAPSULE__SUB_CAPSULES = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Space Stations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TRANSPARENT_CAPSULE__SPACE_STATIONS = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TRANSPARENT_CAPSULE__TAGS = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ISA Transparent Capsule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TRANSPARENT_CAPSULE_FEATURE_COUNT = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>ISA Transparent Capsule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_TRANSPARENT_CAPSULE_OPERATION_COUNT = ISA_NAMED_VISUAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl <em>ISA Space Station</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStation()
	 * @generated
	 */
	int ISA_SPACE_STATION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION__NAME = ISA_NAMED_VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION__QUALIFIED_NAME = ISA_NAMED_VISUAL_ELEMENT__QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION__POSITION = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Satellite Stations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION__SATELLITE_STATIONS = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION__BLOCKS = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Antennas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION__ANTENNAS = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION__TYPE = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION__PUBLIC = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Inheritance Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION__INHERITANCE_RELATIONS = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>ISA Space Station</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_FEATURE_COUNT = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>ISA Space Station</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_OPERATION_COUNT = ISA_NAMED_VISUAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAAbstractSpaceStationComponentImpl <em>ISA Abstract Space Station Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISAAbstractSpaceStationComponentImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAAbstractSpaceStationComponent()
	 * @generated
	 */
	int ISA_ABSTRACT_SPACE_STATION_COMPONENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_SPACE_STATION_COMPONENT__NAME = ISA_NAMED_VISUAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_SPACE_STATION_COMPONENT__QUALIFIED_NAME = ISA_NAMED_VISUAL_ELEMENT__QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Relations To Space Station Centers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Relations To Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Relations To Antennas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>ISA Abstract Space Station Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT = ISA_NAMED_VISUAL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>ISA Abstract Space Station Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_SPACE_STATION_COMPONENT_OPERATION_COUNT = ISA_NAMED_VISUAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl <em>ISA Space Station Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStationBlock()
	 * @generated
	 */
	int ISA_SPACE_STATION_BLOCK = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__NAME = ISA_ABSTRACT_SPACE_STATION_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__QUALIFIED_NAME = ISA_ABSTRACT_SPACE_STATION_COMPONENT__QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__TYPE = ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE;

	/**
	 * The feature id for the '<em><b>Relations To Space Station Centers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__RELATIONS_TO_SPACE_STATION_CENTERS = ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS;

	/**
	 * The feature id for the '<em><b>Relations To Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__RELATIONS_TO_BLOCKS = ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS;

	/**
	 * The feature id for the '<em><b>Relations To Antennas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__RELATIONS_TO_ANTENNAS = ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__HEIGHT = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Diameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__DIAMETER = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Constructor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__CONSTRUCTOR = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__ABSTRACT = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__PUBLIC = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>System Entry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>ISA Space Station Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK_FEATURE_COUNT = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>ISA Space Station Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_BLOCK_OPERATION_COUNT = ISA_ABSTRACT_SPACE_STATION_COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationAntennaImpl <em>ISA Space Station Antenna</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISASpaceStationAntennaImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStationAntenna()
	 * @generated
	 */
	int ISA_SPACE_STATION_ANTENNA = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA__NAME = ISA_ABSTRACT_SPACE_STATION_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA__QUALIFIED_NAME = ISA_ABSTRACT_SPACE_STATION_COMPONENT__QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA__TYPE = ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE;

	/**
	 * The feature id for the '<em><b>Relations To Space Station Centers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA__RELATIONS_TO_SPACE_STATION_CENTERS = ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS;

	/**
	 * The feature id for the '<em><b>Relations To Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA__RELATIONS_TO_BLOCKS = ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS;

	/**
	 * The feature id for the '<em><b>Relations To Antennas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA__RELATIONS_TO_ANTENNAS = ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS;

	/**
	 * The feature id for the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA__PUBLIC = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Enum Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA__ENUM_CONSTANT = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Record Component</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA__RECORD_COMPONENT = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ISA Space Station Antenna</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA_FEATURE_COUNT = ISA_ABSTRACT_SPACE_STATION_COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>ISA Space Station Antenna</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_ANTENNA_OPERATION_COUNT = ISA_ABSTRACT_SPACE_STATION_COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAAbstractVisualRelationImpl <em>ISA Abstract Visual Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISAAbstractVisualRelationImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAAbstractVisualRelation()
	 * @generated
	 */
	int ISA_ABSTRACT_VISUAL_RELATION = 8;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_VISUAL_RELATION__WEIGHT = 0;

	/**
	 * The number of structural features of the '<em>ISA Abstract Visual Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_VISUAL_RELATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>ISA Abstract Visual Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ABSTRACT_VISUAL_RELATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationRelationImpl <em>ISA Space Station Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISASpaceStationRelationImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStationRelation()
	 * @generated
	 */
	int ISA_SPACE_STATION_RELATION = 9;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_RELATION__WEIGHT = ISA_ABSTRACT_VISUAL_RELATION__WEIGHT;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_RELATION__TARGET = ISA_ABSTRACT_VISUAL_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>ISA Space Station Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_RELATION_FEATURE_COUNT = ISA_ABSTRACT_VISUAL_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>ISA Space Station Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_SPACE_STATION_RELATION_OPERATION_COUNT = ISA_ABSTRACT_VISUAL_RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISABlockRelationImpl <em>ISA Block Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISABlockRelationImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISABlockRelation()
	 * @generated
	 */
	int ISA_BLOCK_RELATION = 10;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_BLOCK_RELATION__WEIGHT = ISA_ABSTRACT_VISUAL_RELATION__WEIGHT;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_BLOCK_RELATION__TARGET = ISA_ABSTRACT_VISUAL_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>ISA Block Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_BLOCK_RELATION_FEATURE_COUNT = ISA_ABSTRACT_VISUAL_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>ISA Block Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_BLOCK_RELATION_OPERATION_COUNT = ISA_ABSTRACT_VISUAL_RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAAntennaRelationImpl <em>ISA Antenna Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.impl.ISAAntennaRelationImpl
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAAntennaRelation()
	 * @generated
	 */
	int ISA_ANTENNA_RELATION = 11;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ANTENNA_RELATION__WEIGHT = ISA_ABSTRACT_VISUAL_RELATION__WEIGHT;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ANTENNA_RELATION__TARGET = ISA_ABSTRACT_VISUAL_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>ISA Antenna Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ANTENNA_RELATION_FEATURE_COUNT = ISA_ABSTRACT_VISUAL_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>ISA Antenna Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_ANTENNA_RELATION_OPERATION_COUNT = ISA_ABSTRACT_VISUAL_RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationType <em>ISA Space Station Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationType
	 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStationType()
	 * @generated
	 */
	int ISA_SPACE_STATION_TYPE = 12;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISANamedVisualElement <em>ISA Named Visual Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Named Visual Element</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISANamedVisualElement
	 * @generated
	 */
	EClass getISANamedVisualElement();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISANamedVisualElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISANamedVisualElement#getName()
	 * @see #getISANamedVisualElement()
	 * @generated
	 */
	EAttribute getISANamedVisualElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISANamedVisualElement#getQualifiedName <em>Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Qualified Name</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISANamedVisualElement#getQualifiedName()
	 * @see #getISANamedVisualElement()
	 * @generated
	 */
	EAttribute getISANamedVisualElement_QualifiedName();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel <em>ISA Visualization Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Visualization Model</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAVisualizationModel
	 * @generated
	 */
	EClass getISAVisualizationModel();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getSubjectSystemCapsule <em>Subject System Capsule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subject System Capsule</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getSubjectSystemCapsule()
	 * @see #getISAVisualizationModel()
	 * @generated
	 */
	EReference getISAVisualizationModel_SubjectSystemCapsule();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getLibraryCapsules <em>Library Capsules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Library Capsules</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getLibraryCapsules()
	 * @see #getISAVisualizationModel()
	 * @generated
	 */
	EReference getISAVisualizationModel_LibraryCapsules();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getModelCreatorName <em>Model Creator Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Creator Name</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAVisualizationModel#getModelCreatorName()
	 * @see #getISAVisualizationModel()
	 * @generated
	 */
	EAttribute getISAVisualizationModel_ModelCreatorName();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISAVector3 <em>ISA Vector3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Vector3</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAVector3
	 * @generated
	 */
	EClass getISAVector3();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISAVector3#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAVector3#getX()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_X();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISAVector3#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAVector3#getY()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_Y();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISAVector3#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAVector3#getZ()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_Z();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule <em>ISA Transparent Capsule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Transparent Capsule</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISATransparentCapsule
	 * @generated
	 */
	EClass getISATransparentCapsule();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule#getSubCapsules <em>Sub Capsules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Capsules</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISATransparentCapsule#getSubCapsules()
	 * @see #getISATransparentCapsule()
	 * @generated
	 */
	EReference getISATransparentCapsule_SubCapsules();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule#getSpaceStations <em>Space Stations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Space Stations</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISATransparentCapsule#getSpaceStations()
	 * @see #getISATransparentCapsule()
	 * @generated
	 */
	EReference getISATransparentCapsule_SpaceStations();

	/**
	 * Returns the meta object for the attribute list '{@link dk.itu.cs.isa.model.visualization.ISATransparentCapsule#getTags <em>Tags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Tags</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISATransparentCapsule#getTags()
	 * @see #getISATransparentCapsule()
	 * @generated
	 */
	EAttribute getISATransparentCapsule_Tags();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation <em>ISA Space Station</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Space Station</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStation
	 * @generated
	 */
	EClass getISASpaceStation();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Position</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStation#getPosition()
	 * @see #getISASpaceStation()
	 * @generated
	 */
	EReference getISASpaceStation_Position();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getSatelliteStations <em>Satellite Stations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Satellite Stations</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStation#getSatelliteStations()
	 * @see #getISASpaceStation()
	 * @generated
	 */
	EReference getISASpaceStation_SatelliteStations();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getBlocks <em>Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Blocks</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStation#getBlocks()
	 * @see #getISASpaceStation()
	 * @generated
	 */
	EReference getISASpaceStation_Blocks();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getAntennas <em>Antennas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Antennas</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStation#getAntennas()
	 * @see #getISASpaceStation()
	 * @generated
	 */
	EReference getISASpaceStation_Antennas();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStation#getType()
	 * @see #getISASpaceStation()
	 * @generated
	 */
	EAttribute getISASpaceStation_Type();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#isPublic <em>Public</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStation#isPublic()
	 * @see #getISASpaceStation()
	 * @generated
	 */
	EAttribute getISASpaceStation_Public();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISASpaceStation#getInheritanceRelations <em>Inheritance Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Inheritance Relations</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStation#getInheritanceRelations()
	 * @see #getISASpaceStation()
	 * @generated
	 */
	EReference getISASpaceStation_InheritanceRelations();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent <em>ISA Abstract Space Station Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Abstract Space Station Component</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent
	 * @generated
	 */
	EClass getISAAbstractSpaceStationComponent();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getType()
	 * @see #getISAAbstractSpaceStationComponent()
	 * @generated
	 */
	EReference getISAAbstractSpaceStationComponent_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getRelationsToSpaceStationCenters <em>Relations To Space Station Centers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relations To Space Station Centers</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getRelationsToSpaceStationCenters()
	 * @see #getISAAbstractSpaceStationComponent()
	 * @generated
	 */
	EReference getISAAbstractSpaceStationComponent_RelationsToSpaceStationCenters();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getRelationsToBlocks <em>Relations To Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relations To Blocks</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getRelationsToBlocks()
	 * @see #getISAAbstractSpaceStationComponent()
	 * @generated
	 */
	EReference getISAAbstractSpaceStationComponent_RelationsToBlocks();

	/**
	 * Returns the meta object for the containment reference list '{@link dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getRelationsToAntennas <em>Relations To Antennas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relations To Antennas</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent#getRelationsToAntennas()
	 * @see #getISAAbstractSpaceStationComponent()
	 * @generated
	 */
	EReference getISAAbstractSpaceStationComponent_RelationsToAntennas();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock <em>ISA Space Station Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Space Station Block</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationBlock
	 * @generated
	 */
	EClass getISASpaceStationBlock();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#getHeight <em>Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Height</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#getHeight()
	 * @see #getISASpaceStationBlock()
	 * @generated
	 */
	EAttribute getISASpaceStationBlock_Height();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#getDiameter <em>Diameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Diameter</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#getDiameter()
	 * @see #getISASpaceStationBlock()
	 * @generated
	 */
	EAttribute getISASpaceStationBlock_Diameter();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isConstructor <em>Constructor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constructor</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isConstructor()
	 * @see #getISASpaceStationBlock()
	 * @generated
	 */
	EAttribute getISASpaceStationBlock_Constructor();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isAbstract()
	 * @see #getISASpaceStationBlock()
	 * @generated
	 */
	EAttribute getISASpaceStationBlock_Abstract();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isPublic <em>Public</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isPublic()
	 * @see #getISASpaceStationBlock()
	 * @generated
	 */
	EAttribute getISASpaceStationBlock_Public();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isSystemEntry <em>System Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Entry</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationBlock#isSystemEntry()
	 * @see #getISASpaceStationBlock()
	 * @generated
	 */
	EAttribute getISASpaceStationBlock_SystemEntry();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna <em>ISA Space Station Antenna</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Space Station Antenna</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna
	 * @generated
	 */
	EClass getISASpaceStationAntenna();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isPublic <em>Public</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isPublic()
	 * @see #getISASpaceStationAntenna()
	 * @generated
	 */
	EAttribute getISASpaceStationAntenna_Public();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isEnumConstant <em>Enum Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enum Constant</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isEnumConstant()
	 * @see #getISASpaceStationAntenna()
	 * @generated
	 */
	EAttribute getISASpaceStationAntenna_EnumConstant();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isRecordComponent <em>Record Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Record Component</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna#isRecordComponent()
	 * @see #getISASpaceStationAntenna()
	 * @generated
	 */
	EAttribute getISASpaceStationAntenna_RecordComponent();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISAAbstractVisualRelation <em>ISA Abstract Visual Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Abstract Visual Relation</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAAbstractVisualRelation
	 * @generated
	 */
	EClass getISAAbstractVisualRelation();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.model.visualization.ISAAbstractVisualRelation#getWeight <em>Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Weight</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAAbstractVisualRelation#getWeight()
	 * @see #getISAAbstractVisualRelation()
	 * @generated
	 */
	EAttribute getISAAbstractVisualRelation_Weight();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationRelation <em>ISA Space Station Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Space Station Relation</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationRelation
	 * @generated
	 */
	EClass getISASpaceStationRelation();

	/**
	 * Returns the meta object for the reference '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationRelation#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationRelation#getTarget()
	 * @see #getISASpaceStationRelation()
	 * @generated
	 */
	EReference getISASpaceStationRelation_Target();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISABlockRelation <em>ISA Block Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Block Relation</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISABlockRelation
	 * @generated
	 */
	EClass getISABlockRelation();

	/**
	 * Returns the meta object for the reference '{@link dk.itu.cs.isa.model.visualization.ISABlockRelation#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISABlockRelation#getTarget()
	 * @see #getISABlockRelation()
	 * @generated
	 */
	EReference getISABlockRelation_Target();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.model.visualization.ISAAntennaRelation <em>ISA Antenna Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Antenna Relation</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAAntennaRelation
	 * @generated
	 */
	EClass getISAAntennaRelation();

	/**
	 * Returns the meta object for the reference '{@link dk.itu.cs.isa.model.visualization.ISAAntennaRelation#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISAAntennaRelation#getTarget()
	 * @see #getISAAntennaRelation()
	 * @generated
	 */
	EReference getISAAntennaRelation_Target();

	/**
	 * Returns the meta object for enum '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationType <em>ISA Space Station Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ISA Space Station Type</em>'.
	 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationType
	 * @generated
	 */
	EEnum getISASpaceStationType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VisualizationFactory getVisualizationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISANamedVisualElementImpl <em>ISA Named Visual Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISANamedVisualElementImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISANamedVisualElement()
		 * @generated
		 */
		EClass ISA_NAMED_VISUAL_ELEMENT = eINSTANCE.getISANamedVisualElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_NAMED_VISUAL_ELEMENT__NAME = eINSTANCE.getISANamedVisualElement_Name();

		/**
		 * The meta object literal for the '<em><b>Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_NAMED_VISUAL_ELEMENT__QUALIFIED_NAME = eINSTANCE.getISANamedVisualElement_QualifiedName();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAVisualizationModelImpl <em>ISA Visualization Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISAVisualizationModelImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAVisualizationModel()
		 * @generated
		 */
		EClass ISA_VISUALIZATION_MODEL = eINSTANCE.getISAVisualizationModel();

		/**
		 * The meta object literal for the '<em><b>Subject System Capsule</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE = eINSTANCE.getISAVisualizationModel_SubjectSystemCapsule();

		/**
		 * The meta object literal for the '<em><b>Library Capsules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_VISUALIZATION_MODEL__LIBRARY_CAPSULES = eINSTANCE.getISAVisualizationModel_LibraryCapsules();

		/**
		 * The meta object literal for the '<em><b>Model Creator Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VISUALIZATION_MODEL__MODEL_CREATOR_NAME = eINSTANCE.getISAVisualizationModel_ModelCreatorName();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAVector3Impl <em>ISA Vector3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISAVector3Impl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAVector3()
		 * @generated
		 */
		EClass ISA_VECTOR3 = eINSTANCE.getISAVector3();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__X = eINSTANCE.getISAVector3_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__Y = eINSTANCE.getISAVector3_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__Z = eINSTANCE.getISAVector3_Z();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISATransparentCapsuleImpl <em>ISA Transparent Capsule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISATransparentCapsuleImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISATransparentCapsule()
		 * @generated
		 */
		EClass ISA_TRANSPARENT_CAPSULE = eINSTANCE.getISATransparentCapsule();

		/**
		 * The meta object literal for the '<em><b>Sub Capsules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_TRANSPARENT_CAPSULE__SUB_CAPSULES = eINSTANCE.getISATransparentCapsule_SubCapsules();

		/**
		 * The meta object literal for the '<em><b>Space Stations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_TRANSPARENT_CAPSULE__SPACE_STATIONS = eINSTANCE.getISATransparentCapsule_SpaceStations();

		/**
		 * The meta object literal for the '<em><b>Tags</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_TRANSPARENT_CAPSULE__TAGS = eINSTANCE.getISATransparentCapsule_Tags();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl <em>ISA Space Station</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISASpaceStationImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStation()
		 * @generated
		 */
		EClass ISA_SPACE_STATION = eINSTANCE.getISASpaceStation();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SPACE_STATION__POSITION = eINSTANCE.getISASpaceStation_Position();

		/**
		 * The meta object literal for the '<em><b>Satellite Stations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SPACE_STATION__SATELLITE_STATIONS = eINSTANCE.getISASpaceStation_SatelliteStations();

		/**
		 * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SPACE_STATION__BLOCKS = eINSTANCE.getISASpaceStation_Blocks();

		/**
		 * The meta object literal for the '<em><b>Antennas</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SPACE_STATION__ANTENNAS = eINSTANCE.getISASpaceStation_Antennas();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION__TYPE = eINSTANCE.getISASpaceStation_Type();

		/**
		 * The meta object literal for the '<em><b>Public</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION__PUBLIC = eINSTANCE.getISASpaceStation_Public();

		/**
		 * The meta object literal for the '<em><b>Inheritance Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SPACE_STATION__INHERITANCE_RELATIONS = eINSTANCE.getISASpaceStation_InheritanceRelations();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAAbstractSpaceStationComponentImpl <em>ISA Abstract Space Station Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISAAbstractSpaceStationComponentImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAAbstractSpaceStationComponent()
		 * @generated
		 */
		EClass ISA_ABSTRACT_SPACE_STATION_COMPONENT = eINSTANCE.getISAAbstractSpaceStationComponent();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_ABSTRACT_SPACE_STATION_COMPONENT__TYPE = eINSTANCE.getISAAbstractSpaceStationComponent_Type();

		/**
		 * The meta object literal for the '<em><b>Relations To Space Station Centers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_SPACE_STATION_CENTERS = eINSTANCE.getISAAbstractSpaceStationComponent_RelationsToSpaceStationCenters();

		/**
		 * The meta object literal for the '<em><b>Relations To Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_BLOCKS = eINSTANCE.getISAAbstractSpaceStationComponent_RelationsToBlocks();

		/**
		 * The meta object literal for the '<em><b>Relations To Antennas</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_ABSTRACT_SPACE_STATION_COMPONENT__RELATIONS_TO_ANTENNAS = eINSTANCE.getISAAbstractSpaceStationComponent_RelationsToAntennas();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl <em>ISA Space Station Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISASpaceStationBlockImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStationBlock()
		 * @generated
		 */
		EClass ISA_SPACE_STATION_BLOCK = eINSTANCE.getISASpaceStationBlock();

		/**
		 * The meta object literal for the '<em><b>Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION_BLOCK__HEIGHT = eINSTANCE.getISASpaceStationBlock_Height();

		/**
		 * The meta object literal for the '<em><b>Diameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION_BLOCK__DIAMETER = eINSTANCE.getISASpaceStationBlock_Diameter();

		/**
		 * The meta object literal for the '<em><b>Constructor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION_BLOCK__CONSTRUCTOR = eINSTANCE.getISASpaceStationBlock_Constructor();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION_BLOCK__ABSTRACT = eINSTANCE.getISASpaceStationBlock_Abstract();

		/**
		 * The meta object literal for the '<em><b>Public</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION_BLOCK__PUBLIC = eINSTANCE.getISASpaceStationBlock_Public();

		/**
		 * The meta object literal for the '<em><b>System Entry</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION_BLOCK__SYSTEM_ENTRY = eINSTANCE.getISASpaceStationBlock_SystemEntry();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationAntennaImpl <em>ISA Space Station Antenna</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISASpaceStationAntennaImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStationAntenna()
		 * @generated
		 */
		EClass ISA_SPACE_STATION_ANTENNA = eINSTANCE.getISASpaceStationAntenna();

		/**
		 * The meta object literal for the '<em><b>Public</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION_ANTENNA__PUBLIC = eINSTANCE.getISASpaceStationAntenna_Public();

		/**
		 * The meta object literal for the '<em><b>Enum Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION_ANTENNA__ENUM_CONSTANT = eINSTANCE.getISASpaceStationAntenna_EnumConstant();

		/**
		 * The meta object literal for the '<em><b>Record Component</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_SPACE_STATION_ANTENNA__RECORD_COMPONENT = eINSTANCE.getISASpaceStationAntenna_RecordComponent();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAAbstractVisualRelationImpl <em>ISA Abstract Visual Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISAAbstractVisualRelationImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAAbstractVisualRelation()
		 * @generated
		 */
		EClass ISA_ABSTRACT_VISUAL_RELATION = eINSTANCE.getISAAbstractVisualRelation();

		/**
		 * The meta object literal for the '<em><b>Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_ABSTRACT_VISUAL_RELATION__WEIGHT = eINSTANCE.getISAAbstractVisualRelation_Weight();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISASpaceStationRelationImpl <em>ISA Space Station Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISASpaceStationRelationImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStationRelation()
		 * @generated
		 */
		EClass ISA_SPACE_STATION_RELATION = eINSTANCE.getISASpaceStationRelation();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_SPACE_STATION_RELATION__TARGET = eINSTANCE.getISASpaceStationRelation_Target();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISABlockRelationImpl <em>ISA Block Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISABlockRelationImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISABlockRelation()
		 * @generated
		 */
		EClass ISA_BLOCK_RELATION = eINSTANCE.getISABlockRelation();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_BLOCK_RELATION__TARGET = eINSTANCE.getISABlockRelation_Target();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.impl.ISAAntennaRelationImpl <em>ISA Antenna Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.impl.ISAAntennaRelationImpl
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISAAntennaRelation()
		 * @generated
		 */
		EClass ISA_ANTENNA_RELATION = eINSTANCE.getISAAntennaRelation();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ISA_ANTENNA_RELATION__TARGET = eINSTANCE.getISAAntennaRelation_Target();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.model.visualization.ISASpaceStationType <em>ISA Space Station Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.model.visualization.ISASpaceStationType
		 * @see dk.itu.cs.isa.model.visualization.impl.VisualizationPackageImpl#getISASpaceStationType()
		 * @generated
		 */
		EEnum ISA_SPACE_STATION_TYPE = eINSTANCE.getISASpaceStationType();

	}

} //VisualizationPackage

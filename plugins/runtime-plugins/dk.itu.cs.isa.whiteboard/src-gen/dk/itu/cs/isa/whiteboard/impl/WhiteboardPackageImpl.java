/**
 */
package dk.itu.cs.isa.whiteboard.impl;

import dk.itu.cs.isa.whiteboard.AbstractCompoundOperation;
import dk.itu.cs.isa.whiteboard.AbstractDrawOperation;
import dk.itu.cs.isa.whiteboard.AbstractOperation;
import dk.itu.cs.isa.whiteboard.AbstractPhotoOperation;
import dk.itu.cs.isa.whiteboard.AbstractPinOperation;
import dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent;
import dk.itu.cs.isa.whiteboard.BoardDestroyEvent;
import dk.itu.cs.isa.whiteboard.BoardDuplicateEvent;
import dk.itu.cs.isa.whiteboard.BoardEnlargeOperation;
import dk.itu.cs.isa.whiteboard.BoardSpawnEvent;
import dk.itu.cs.isa.whiteboard.DrawArrowOperation;
import dk.itu.cs.isa.whiteboard.DrawFreehandOperation;
import dk.itu.cs.isa.whiteboard.DrawModuleOperation;
import dk.itu.cs.isa.whiteboard.ISAColor;
import dk.itu.cs.isa.whiteboard.ISAVector2;
import dk.itu.cs.isa.whiteboard.ISAVector3;
import dk.itu.cs.isa.whiteboard.PaintClearAllOperation;
import dk.itu.cs.isa.whiteboard.PaintEraseOperation;
import dk.itu.cs.isa.whiteboard.Photo;
import dk.itu.cs.isa.whiteboard.PhotoAddOperation;
import dk.itu.cs.isa.whiteboard.PhotoMoveOperation;
import dk.itu.cs.isa.whiteboard.PhotoRemoveOperation;
import dk.itu.cs.isa.whiteboard.Pin;
import dk.itu.cs.isa.whiteboard.PinAudioAddOperation;
import dk.itu.cs.isa.whiteboard.PinMoveOperation;
import dk.itu.cs.isa.whiteboard.PinRemoveOperation;
import dk.itu.cs.isa.whiteboard.PinSoftwareAddOperation;
import dk.itu.cs.isa.whiteboard.PinSplitOperation;
import dk.itu.cs.isa.whiteboard.PinsRemoveAllOperation;
import dk.itu.cs.isa.whiteboard.RedoEvent;
import dk.itu.cs.isa.whiteboard.UndoEvent;
import dk.itu.cs.isa.whiteboard.VisibleRelation;
import dk.itu.cs.isa.whiteboard.Whiteboard;
import dk.itu.cs.isa.whiteboard.WhiteboardFactory;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WhiteboardPackageImpl extends EPackageImpl implements WhiteboardPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass whiteboardHistoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass whiteboardEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pinEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass photoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaVector3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaVector2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaColorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractWhiteboardEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass boardSpawnEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass boardDuplicateEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass boardDestroyEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass undoEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass redoEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractCompoundOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pinSplitOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pinsRemoveAllOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractPinOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pinSoftwareAddOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pinAudioAddOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pinRemoveOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pinMoveOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractDrawOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass drawFreehandOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass drawModuleOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass drawArrowOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paintClearAllOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paintEraseOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass boardEnlargeOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractPhotoOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass photoAddOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass photoMoveOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass photoRemoveOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass visibleRelationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WhiteboardPackageImpl() {
		super(eNS_URI, WhiteboardFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link WhiteboardPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WhiteboardPackage init() {
		if (isInited) return (WhiteboardPackage)EPackage.Registry.INSTANCE.getEPackage(WhiteboardPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredWhiteboardPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		WhiteboardPackageImpl theWhiteboardPackage = registeredWhiteboardPackage instanceof WhiteboardPackageImpl ? (WhiteboardPackageImpl)registeredWhiteboardPackage : new WhiteboardPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theWhiteboardPackage.createPackageContents();

		// Initialize created meta-data
		theWhiteboardPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWhiteboardPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WhiteboardPackage.eNS_URI, theWhiteboardPackage);
		return theWhiteboardPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWhiteboardHistory() {
		return whiteboardHistoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWhiteboardHistory_SystemName() {
		return (EAttribute)whiteboardHistoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhiteboardHistory_Whiteboards() {
		return (EReference)whiteboardHistoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhiteboardHistory_EventLog() {
		return (EReference)whiteboardHistoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWhiteboard() {
		return whiteboardEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWhiteboard_Id() {
		return (EAttribute)whiteboardEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhiteboard_ContainingHistory() {
		return (EReference)whiteboardEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWhiteboard_BitmapBase64() {
		return (EAttribute)whiteboardEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhiteboard_Pins() {
		return (EReference)whiteboardEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhiteboard_Photos() {
		return (EReference)whiteboardEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhiteboard_Position() {
		return (EReference)whiteboardEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWhiteboard_Rotation() {
		return (EReference)whiteboardEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWhiteboard_Size() {
		return (EAttribute)whiteboardEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWhiteboard_UndoDepth() {
		return (EAttribute)whiteboardEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPin() {
		return pinEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPin_Id() {
		return (EAttribute)pinEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPin_ElementQualifiedName() {
		return (EAttribute)pinEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPin_TextureSpacePosition() {
		return (EReference)pinEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPin_LocalSpacePosition() {
		return (EReference)pinEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPin_Color() {
		return (EReference)pinEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPin_Whiteboard() {
		return (EReference)pinEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhoto() {
		return photoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhoto_Id() {
		return (EAttribute)photoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhoto_TextureSpacePosition() {
		return (EReference)photoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhoto_LocalSpacePosition() {
		return (EReference)photoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhoto_LocalSpaceForwardDirection() {
		return (EReference)photoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhoto_LocalSpaceUpwardDirection() {
		return (EReference)photoEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhoto_BitmapBase64() {
		return (EAttribute)photoEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAVector3() {
		return isaVector3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_X() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_Y() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_Z() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAVector2() {
		return isaVector2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector2_X() {
		return (EAttribute)isaVector2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector2_Y() {
		return (EAttribute)isaVector2EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAColor() {
		return isaColorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAColor_R() {
		return (EAttribute)isaColorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAColor_G() {
		return (EAttribute)isaColorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAColor_B() {
		return (EAttribute)isaColorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAColor_A() {
		return (EAttribute)isaColorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractWhiteboardEvent() {
		return abstractWhiteboardEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractWhiteboardEvent_SubmittingClientId() {
		return (EAttribute)abstractWhiteboardEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractWhiteboardEvent_WhiteboardId() {
		return (EAttribute)abstractWhiteboardEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractWhiteboardEvent_Timestamp() {
		return (EAttribute)abstractWhiteboardEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractWhiteboardEvent_ConfirmationRequired() {
		return (EAttribute)abstractWhiteboardEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBoardSpawnEvent() {
		return boardSpawnEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBoardSpawnEvent_WorldSpacePosition() {
		return (EReference)boardSpawnEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBoardSpawnEvent_WorldSpaceRotation() {
		return (EReference)boardSpawnEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBoardDuplicateEvent() {
		return boardDuplicateEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBoardDuplicateEvent_WorldSpacePosition() {
		return (EReference)boardDuplicateEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBoardDuplicateEvent_WorldSpaceRotation() {
		return (EReference)boardDuplicateEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBoardDestroyEvent() {
		return boardDestroyEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUndoEvent() {
		return undoEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRedoEvent() {
		return redoEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractOperation() {
		return abstractOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractCompoundOperation() {
		return abstractCompoundOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractCompoundOperation_SubOperations() {
		return (EReference)abstractCompoundOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPinSplitOperation() {
		return pinSplitOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPinSplitOperation_PinnedElementName() {
		return (EAttribute)pinSplitOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPinSplitOperation_PinId() {
		return (EAttribute)pinSplitOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPinsRemoveAllOperation() {
		return pinsRemoveAllOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractPinOperation() {
		return abstractPinOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractPinOperation_PinId() {
		return (EAttribute)abstractPinOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractPinOperation_PinnedElementName() {
		return (EAttribute)abstractPinOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPinOperation_TextureSpacePosition() {
		return (EReference)abstractPinOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPinOperation_LocalSpacePosition() {
		return (EReference)abstractPinOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPinOperation_Color() {
		return (EReference)abstractPinOperationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPinSoftwareAddOperation() {
		return pinSoftwareAddOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPinAudioAddOperation() {
		return pinAudioAddOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPinAudioAddOperation_SoundWaveBase64() {
		return (EAttribute)pinAudioAddOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPinAudioAddOperation_SoundWaveFrequency() {
		return (EAttribute)pinAudioAddOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPinRemoveOperation() {
		return pinRemoveOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPinMoveOperation() {
		return pinMoveOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPinMoveOperation_PreTextureSpacePosition() {
		return (EReference)pinMoveOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractDrawOperation() {
		return abstractDrawOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractDrawOperation_Color() {
		return (EReference)abstractDrawOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractDrawOperation_PenSize() {
		return (EAttribute)abstractDrawOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractDrawOperation_DrawnLines() {
		return (EAttribute)abstractDrawOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDrawFreehandOperation() {
		return drawFreehandOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDrawModuleOperation() {
		return drawModuleOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDrawArrowOperation() {
		return drawArrowOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDrawArrowOperation_AutoDrawArrowHead() {
		return (EAttribute)drawArrowOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPaintClearAllOperation() {
		return paintClearAllOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPaintEraseOperation() {
		return paintEraseOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBoardEnlargeOperation() {
		return boardEnlargeOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractPhotoOperation() {
		return abstractPhotoOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractPhotoOperation_PhotoId() {
		return (EAttribute)abstractPhotoOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractPhotoOperation_BitmapBase64() {
		return (EAttribute)abstractPhotoOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPhotoOperation_LocalSpacePosition() {
		return (EReference)abstractPhotoOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPhotoOperation_LocalSpaceForwardDirection() {
		return (EReference)abstractPhotoOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPhotoOperation_LocalSpaceUpwardDirection() {
		return (EReference)abstractPhotoOperationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPhotoOperation_TextureSpacePosition() {
		return (EReference)abstractPhotoOperationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPhotoOperation_CameraPositionWhenPhotoWasTaken() {
		return (EReference)abstractPhotoOperationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPhotoOperation_CameraRotationWhenPhotoWasTaken() {
		return (EReference)abstractPhotoOperationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPhotoOperation_RelationsVisibleWhenPhotoWasTaken() {
		return (EReference)abstractPhotoOperationEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractPhotoOperation_OpenCapsulesElementQualifiedNamesWhenPhotoWasTaken() {
		return (EAttribute)abstractPhotoOperationEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractPhotoOperation_SystemSizeWhenPhotoWasTaken() {
		return (EAttribute)abstractPhotoOperationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractPhotoOperation_SystemPositionWhenPhotoWasTaken() {
		return (EReference)abstractPhotoOperationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhotoAddOperation() {
		return photoAddOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhotoMoveOperation() {
		return photoMoveOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhotoMoveOperation_PreLocalSpacePosition() {
		return (EReference)photoMoveOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhotoMoveOperation_PreLocalSpaceForwardDirection() {
		return (EReference)photoMoveOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhotoMoveOperation_PreLocalSpaceUpwardDirection() {
		return (EReference)photoMoveOperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPhotoMoveOperation_PreTextureSpacePosition() {
		return (EReference)photoMoveOperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPhotoRemoveOperation() {
		return photoRemoveOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPhotoRemoveOperation_PlayDissolveAnimation() {
		return (EAttribute)photoRemoveOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVisibleRelation() {
		return visibleRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisibleRelation_ElementName() {
		return (EAttribute)visibleRelationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisibleRelation_Forward() {
		return (EAttribute)visibleRelationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVisibleRelation_RelationType() {
		return (EAttribute)visibleRelationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhiteboardFactory getWhiteboardFactory() {
		return (WhiteboardFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		whiteboardHistoryEClass = createEClass(WHITEBOARD_HISTORY);
		createEAttribute(whiteboardHistoryEClass, WHITEBOARD_HISTORY__SYSTEM_NAME);
		createEReference(whiteboardHistoryEClass, WHITEBOARD_HISTORY__WHITEBOARDS);
		createEReference(whiteboardHistoryEClass, WHITEBOARD_HISTORY__EVENT_LOG);

		whiteboardEClass = createEClass(WHITEBOARD);
		createEAttribute(whiteboardEClass, WHITEBOARD__ID);
		createEReference(whiteboardEClass, WHITEBOARD__CONTAINING_HISTORY);
		createEAttribute(whiteboardEClass, WHITEBOARD__BITMAP_BASE64);
		createEReference(whiteboardEClass, WHITEBOARD__PINS);
		createEReference(whiteboardEClass, WHITEBOARD__PHOTOS);
		createEReference(whiteboardEClass, WHITEBOARD__POSITION);
		createEReference(whiteboardEClass, WHITEBOARD__ROTATION);
		createEAttribute(whiteboardEClass, WHITEBOARD__SIZE);
		createEAttribute(whiteboardEClass, WHITEBOARD__UNDO_DEPTH);

		pinEClass = createEClass(PIN);
		createEAttribute(pinEClass, PIN__ID);
		createEAttribute(pinEClass, PIN__ELEMENT_QUALIFIED_NAME);
		createEReference(pinEClass, PIN__TEXTURE_SPACE_POSITION);
		createEReference(pinEClass, PIN__LOCAL_SPACE_POSITION);
		createEReference(pinEClass, PIN__COLOR);
		createEReference(pinEClass, PIN__WHITEBOARD);

		photoEClass = createEClass(PHOTO);
		createEAttribute(photoEClass, PHOTO__ID);
		createEReference(photoEClass, PHOTO__TEXTURE_SPACE_POSITION);
		createEReference(photoEClass, PHOTO__LOCAL_SPACE_POSITION);
		createEReference(photoEClass, PHOTO__LOCAL_SPACE_FORWARD_DIRECTION);
		createEReference(photoEClass, PHOTO__LOCAL_SPACE_UPWARD_DIRECTION);
		createEAttribute(photoEClass, PHOTO__BITMAP_BASE64);

		isaVector3EClass = createEClass(ISA_VECTOR3);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__X);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__Y);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__Z);

		isaVector2EClass = createEClass(ISA_VECTOR2);
		createEAttribute(isaVector2EClass, ISA_VECTOR2__X);
		createEAttribute(isaVector2EClass, ISA_VECTOR2__Y);

		isaColorEClass = createEClass(ISA_COLOR);
		createEAttribute(isaColorEClass, ISA_COLOR__R);
		createEAttribute(isaColorEClass, ISA_COLOR__G);
		createEAttribute(isaColorEClass, ISA_COLOR__B);
		createEAttribute(isaColorEClass, ISA_COLOR__A);

		abstractWhiteboardEventEClass = createEClass(ABSTRACT_WHITEBOARD_EVENT);
		createEAttribute(abstractWhiteboardEventEClass, ABSTRACT_WHITEBOARD_EVENT__SUBMITTING_CLIENT_ID);
		createEAttribute(abstractWhiteboardEventEClass, ABSTRACT_WHITEBOARD_EVENT__WHITEBOARD_ID);
		createEAttribute(abstractWhiteboardEventEClass, ABSTRACT_WHITEBOARD_EVENT__TIMESTAMP);
		createEAttribute(abstractWhiteboardEventEClass, ABSTRACT_WHITEBOARD_EVENT__CONFIRMATION_REQUIRED);

		boardSpawnEventEClass = createEClass(BOARD_SPAWN_EVENT);
		createEReference(boardSpawnEventEClass, BOARD_SPAWN_EVENT__WORLD_SPACE_POSITION);
		createEReference(boardSpawnEventEClass, BOARD_SPAWN_EVENT__WORLD_SPACE_ROTATION);

		boardDuplicateEventEClass = createEClass(BOARD_DUPLICATE_EVENT);
		createEReference(boardDuplicateEventEClass, BOARD_DUPLICATE_EVENT__WORLD_SPACE_POSITION);
		createEReference(boardDuplicateEventEClass, BOARD_DUPLICATE_EVENT__WORLD_SPACE_ROTATION);

		boardDestroyEventEClass = createEClass(BOARD_DESTROY_EVENT);

		undoEventEClass = createEClass(UNDO_EVENT);

		redoEventEClass = createEClass(REDO_EVENT);

		abstractOperationEClass = createEClass(ABSTRACT_OPERATION);

		abstractCompoundOperationEClass = createEClass(ABSTRACT_COMPOUND_OPERATION);
		createEReference(abstractCompoundOperationEClass, ABSTRACT_COMPOUND_OPERATION__SUB_OPERATIONS);

		pinSplitOperationEClass = createEClass(PIN_SPLIT_OPERATION);
		createEAttribute(pinSplitOperationEClass, PIN_SPLIT_OPERATION__PINNED_ELEMENT_NAME);
		createEAttribute(pinSplitOperationEClass, PIN_SPLIT_OPERATION__PIN_ID);

		pinsRemoveAllOperationEClass = createEClass(PINS_REMOVE_ALL_OPERATION);

		abstractPinOperationEClass = createEClass(ABSTRACT_PIN_OPERATION);
		createEAttribute(abstractPinOperationEClass, ABSTRACT_PIN_OPERATION__PIN_ID);
		createEAttribute(abstractPinOperationEClass, ABSTRACT_PIN_OPERATION__PINNED_ELEMENT_NAME);
		createEReference(abstractPinOperationEClass, ABSTRACT_PIN_OPERATION__TEXTURE_SPACE_POSITION);
		createEReference(abstractPinOperationEClass, ABSTRACT_PIN_OPERATION__LOCAL_SPACE_POSITION);
		createEReference(abstractPinOperationEClass, ABSTRACT_PIN_OPERATION__COLOR);

		pinSoftwareAddOperationEClass = createEClass(PIN_SOFTWARE_ADD_OPERATION);

		pinAudioAddOperationEClass = createEClass(PIN_AUDIO_ADD_OPERATION);
		createEAttribute(pinAudioAddOperationEClass, PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_BASE64);
		createEAttribute(pinAudioAddOperationEClass, PIN_AUDIO_ADD_OPERATION__SOUND_WAVE_FREQUENCY);

		pinRemoveOperationEClass = createEClass(PIN_REMOVE_OPERATION);

		pinMoveOperationEClass = createEClass(PIN_MOVE_OPERATION);
		createEReference(pinMoveOperationEClass, PIN_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION);

		abstractDrawOperationEClass = createEClass(ABSTRACT_DRAW_OPERATION);
		createEReference(abstractDrawOperationEClass, ABSTRACT_DRAW_OPERATION__COLOR);
		createEAttribute(abstractDrawOperationEClass, ABSTRACT_DRAW_OPERATION__PEN_SIZE);
		createEAttribute(abstractDrawOperationEClass, ABSTRACT_DRAW_OPERATION__DRAWN_LINES);

		drawFreehandOperationEClass = createEClass(DRAW_FREEHAND_OPERATION);

		drawModuleOperationEClass = createEClass(DRAW_MODULE_OPERATION);

		drawArrowOperationEClass = createEClass(DRAW_ARROW_OPERATION);
		createEAttribute(drawArrowOperationEClass, DRAW_ARROW_OPERATION__AUTO_DRAW_ARROW_HEAD);

		paintClearAllOperationEClass = createEClass(PAINT_CLEAR_ALL_OPERATION);

		paintEraseOperationEClass = createEClass(PAINT_ERASE_OPERATION);

		boardEnlargeOperationEClass = createEClass(BOARD_ENLARGE_OPERATION);

		abstractPhotoOperationEClass = createEClass(ABSTRACT_PHOTO_OPERATION);
		createEAttribute(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__PHOTO_ID);
		createEAttribute(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__BITMAP_BASE64);
		createEReference(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_POSITION);
		createEReference(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_FORWARD_DIRECTION);
		createEReference(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__LOCAL_SPACE_UPWARD_DIRECTION);
		createEReference(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__TEXTURE_SPACE_POSITION);
		createEReference(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__CAMERA_POSITION_WHEN_PHOTO_WAS_TAKEN);
		createEReference(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__CAMERA_ROTATION_WHEN_PHOTO_WAS_TAKEN);
		createEReference(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__SYSTEM_POSITION_WHEN_PHOTO_WAS_TAKEN);
		createEAttribute(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__SYSTEM_SIZE_WHEN_PHOTO_WAS_TAKEN);
		createEReference(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__RELATIONS_VISIBLE_WHEN_PHOTO_WAS_TAKEN);
		createEAttribute(abstractPhotoOperationEClass, ABSTRACT_PHOTO_OPERATION__OPEN_CAPSULES_ELEMENT_QUALIFIED_NAMES_WHEN_PHOTO_WAS_TAKEN);

		photoAddOperationEClass = createEClass(PHOTO_ADD_OPERATION);

		photoMoveOperationEClass = createEClass(PHOTO_MOVE_OPERATION);
		createEReference(photoMoveOperationEClass, PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_POSITION);
		createEReference(photoMoveOperationEClass, PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_FORWARD_DIRECTION);
		createEReference(photoMoveOperationEClass, PHOTO_MOVE_OPERATION__PRE_LOCAL_SPACE_UPWARD_DIRECTION);
		createEReference(photoMoveOperationEClass, PHOTO_MOVE_OPERATION__PRE_TEXTURE_SPACE_POSITION);

		photoRemoveOperationEClass = createEClass(PHOTO_REMOVE_OPERATION);
		createEAttribute(photoRemoveOperationEClass, PHOTO_REMOVE_OPERATION__PLAY_DISSOLVE_ANIMATION);

		visibleRelationEClass = createEClass(VISIBLE_RELATION);
		createEAttribute(visibleRelationEClass, VISIBLE_RELATION__ELEMENT_NAME);
		createEAttribute(visibleRelationEClass, VISIBLE_RELATION__FORWARD);
		createEAttribute(visibleRelationEClass, VISIBLE_RELATION__RELATION_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		boardSpawnEventEClass.getESuperTypes().add(this.getAbstractWhiteboardEvent());
		boardDuplicateEventEClass.getESuperTypes().add(this.getAbstractWhiteboardEvent());
		boardDestroyEventEClass.getESuperTypes().add(this.getAbstractWhiteboardEvent());
		undoEventEClass.getESuperTypes().add(this.getAbstractWhiteboardEvent());
		redoEventEClass.getESuperTypes().add(this.getAbstractWhiteboardEvent());
		abstractOperationEClass.getESuperTypes().add(this.getAbstractWhiteboardEvent());
		abstractCompoundOperationEClass.getESuperTypes().add(this.getAbstractOperation());
		pinSplitOperationEClass.getESuperTypes().add(this.getAbstractCompoundOperation());
		pinsRemoveAllOperationEClass.getESuperTypes().add(this.getAbstractCompoundOperation());
		abstractPinOperationEClass.getESuperTypes().add(this.getAbstractOperation());
		pinSoftwareAddOperationEClass.getESuperTypes().add(this.getAbstractPinOperation());
		pinAudioAddOperationEClass.getESuperTypes().add(this.getAbstractPinOperation());
		pinRemoveOperationEClass.getESuperTypes().add(this.getAbstractPinOperation());
		pinMoveOperationEClass.getESuperTypes().add(this.getAbstractPinOperation());
		abstractDrawOperationEClass.getESuperTypes().add(this.getAbstractOperation());
		drawFreehandOperationEClass.getESuperTypes().add(this.getAbstractDrawOperation());
		drawModuleOperationEClass.getESuperTypes().add(this.getAbstractDrawOperation());
		drawArrowOperationEClass.getESuperTypes().add(this.getAbstractDrawOperation());
		paintClearAllOperationEClass.getESuperTypes().add(this.getAbstractDrawOperation());
		paintEraseOperationEClass.getESuperTypes().add(this.getAbstractDrawOperation());
		boardEnlargeOperationEClass.getESuperTypes().add(this.getAbstractDrawOperation());
		abstractPhotoOperationEClass.getESuperTypes().add(this.getAbstractOperation());
		photoAddOperationEClass.getESuperTypes().add(this.getAbstractPhotoOperation());
		photoMoveOperationEClass.getESuperTypes().add(this.getAbstractPhotoOperation());
		photoRemoveOperationEClass.getESuperTypes().add(this.getAbstractPhotoOperation());

		// Initialize classes, features, and operations; add parameters
		initEClass(whiteboardHistoryEClass, WhiteboardHistory.class, "WhiteboardHistory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWhiteboardHistory_SystemName(), ecorePackage.getEString(), "systemName", null, 1, 1, WhiteboardHistory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhiteboardHistory_Whiteboards(), this.getWhiteboard(), this.getWhiteboard_ContainingHistory(), "whiteboards", null, 1, -1, WhiteboardHistory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhiteboardHistory_EventLog(), this.getAbstractWhiteboardEvent(), null, "eventLog", null, 0, -1, WhiteboardHistory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(whiteboardEClass, Whiteboard.class, "Whiteboard", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWhiteboard_Id(), ecorePackage.getEInt(), "id", null, 1, 1, Whiteboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhiteboard_ContainingHistory(), this.getWhiteboardHistory(), this.getWhiteboardHistory_Whiteboards(), "containingHistory", null, 1, 1, Whiteboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWhiteboard_BitmapBase64(), ecorePackage.getEString(), "bitmapBase64", null, 1, 1, Whiteboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhiteboard_Pins(), this.getPin(), this.getPin_Whiteboard(), "pins", null, 0, -1, Whiteboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhiteboard_Photos(), this.getPhoto(), null, "photos", null, 0, -1, Whiteboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhiteboard_Position(), this.getISAVector3(), null, "position", null, 1, 1, Whiteboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWhiteboard_Rotation(), this.getISAVector3(), null, "rotation", null, 1, 1, Whiteboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWhiteboard_Size(), ecorePackage.getEInt(), "size", null, 1, 1, Whiteboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWhiteboard_UndoDepth(), ecorePackage.getEInt(), "undoDepth", null, 1, 1, Whiteboard.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pinEClass, Pin.class, "Pin", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPin_Id(), ecorePackage.getEInt(), "id", null, 1, 1, Pin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPin_ElementQualifiedName(), ecorePackage.getEString(), "elementQualifiedName", null, 1, 1, Pin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPin_TextureSpacePosition(), this.getISAVector2(), null, "textureSpacePosition", null, 1, 1, Pin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPin_LocalSpacePosition(), this.getISAVector3(), null, "localSpacePosition", null, 1, 1, Pin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPin_Color(), this.getISAColor(), null, "color", null, 1, 1, Pin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPin_Whiteboard(), this.getWhiteboard(), this.getWhiteboard_Pins(), "whiteboard", null, 1, 1, Pin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(photoEClass, Photo.class, "Photo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPhoto_Id(), ecorePackage.getEInt(), "id", null, 1, 1, Photo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhoto_TextureSpacePosition(), this.getISAVector2(), null, "textureSpacePosition", null, 1, 1, Photo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhoto_LocalSpacePosition(), this.getISAVector3(), null, "localSpacePosition", null, 1, 1, Photo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhoto_LocalSpaceForwardDirection(), this.getISAVector3(), null, "localSpaceForwardDirection", null, 1, 1, Photo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhoto_LocalSpaceUpwardDirection(), this.getISAVector3(), null, "localSpaceUpwardDirection", null, 1, 1, Photo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPhoto_BitmapBase64(), ecorePackage.getEString(), "bitmapBase64", null, 1, 1, Photo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaVector3EClass, ISAVector3.class, "ISAVector3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAVector3_X(), ecorePackage.getEFloat(), "x", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVector3_Y(), ecorePackage.getEFloat(), "y", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVector3_Z(), ecorePackage.getEFloat(), "z", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaVector2EClass, ISAVector2.class, "ISAVector2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAVector2_X(), ecorePackage.getEFloat(), "x", null, 1, 1, ISAVector2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVector2_Y(), ecorePackage.getEFloat(), "y", null, 1, 1, ISAVector2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaColorEClass, ISAColor.class, "ISAColor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAColor_R(), ecorePackage.getEFloat(), "r", null, 1, 1, ISAColor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAColor_G(), ecorePackage.getEFloat(), "g", null, 1, 1, ISAColor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAColor_B(), ecorePackage.getEFloat(), "b", null, 1, 1, ISAColor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAColor_A(), ecorePackage.getEFloat(), "a", null, 1, 1, ISAColor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractWhiteboardEventEClass, AbstractWhiteboardEvent.class, "AbstractWhiteboardEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractWhiteboardEvent_SubmittingClientId(), ecorePackage.getEInt(), "submittingClientId", null, 1, 1, AbstractWhiteboardEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractWhiteboardEvent_WhiteboardId(), ecorePackage.getEInt(), "whiteboardId", null, 1, 1, AbstractWhiteboardEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractWhiteboardEvent_Timestamp(), ecorePackage.getELong(), "timestamp", null, 1, 1, AbstractWhiteboardEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractWhiteboardEvent_ConfirmationRequired(), ecorePackage.getEBoolean(), "confirmationRequired", null, 1, 1, AbstractWhiteboardEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(boardSpawnEventEClass, BoardSpawnEvent.class, "BoardSpawnEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBoardSpawnEvent_WorldSpacePosition(), this.getISAVector3(), null, "worldSpacePosition", null, 1, 1, BoardSpawnEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBoardSpawnEvent_WorldSpaceRotation(), this.getISAVector3(), null, "worldSpaceRotation", null, 1, 1, BoardSpawnEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(boardDuplicateEventEClass, BoardDuplicateEvent.class, "BoardDuplicateEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBoardDuplicateEvent_WorldSpacePosition(), this.getISAVector3(), null, "worldSpacePosition", null, 1, 1, BoardDuplicateEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBoardDuplicateEvent_WorldSpaceRotation(), this.getISAVector3(), null, "worldSpaceRotation", null, 1, 1, BoardDuplicateEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(boardDestroyEventEClass, BoardDestroyEvent.class, "BoardDestroyEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(undoEventEClass, UndoEvent.class, "UndoEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(redoEventEClass, RedoEvent.class, "RedoEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractOperationEClass, AbstractOperation.class, "AbstractOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractCompoundOperationEClass, AbstractCompoundOperation.class, "AbstractCompoundOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractCompoundOperation_SubOperations(), this.getAbstractOperation(), null, "subOperations", null, 0, -1, AbstractCompoundOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pinSplitOperationEClass, PinSplitOperation.class, "PinSplitOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPinSplitOperation_PinnedElementName(), ecorePackage.getEString(), "pinnedElementName", null, 1, 1, PinSplitOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPinSplitOperation_PinId(), ecorePackage.getEInt(), "pinId", null, 1, 1, PinSplitOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pinsRemoveAllOperationEClass, PinsRemoveAllOperation.class, "PinsRemoveAllOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractPinOperationEClass, AbstractPinOperation.class, "AbstractPinOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractPinOperation_PinId(), ecorePackage.getEInt(), "pinId", null, 1, 1, AbstractPinOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractPinOperation_PinnedElementName(), ecorePackage.getEString(), "pinnedElementName", null, 1, 1, AbstractPinOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPinOperation_TextureSpacePosition(), this.getISAVector2(), null, "textureSpacePosition", null, 1, 1, AbstractPinOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPinOperation_LocalSpacePosition(), this.getISAVector3(), null, "localSpacePosition", null, 1, 1, AbstractPinOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPinOperation_Color(), this.getISAColor(), null, "color", null, 1, 1, AbstractPinOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pinSoftwareAddOperationEClass, PinSoftwareAddOperation.class, "PinSoftwareAddOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pinAudioAddOperationEClass, PinAudioAddOperation.class, "PinAudioAddOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPinAudioAddOperation_SoundWaveBase64(), ecorePackage.getEString(), "soundWaveBase64", null, 1, 1, PinAudioAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPinAudioAddOperation_SoundWaveFrequency(), ecorePackage.getEInt(), "soundWaveFrequency", null, 1, 1, PinAudioAddOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pinRemoveOperationEClass, PinRemoveOperation.class, "PinRemoveOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pinMoveOperationEClass, PinMoveOperation.class, "PinMoveOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPinMoveOperation_PreTextureSpacePosition(), this.getISAVector2(), null, "preTextureSpacePosition", null, 1, 1, PinMoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractDrawOperationEClass, AbstractDrawOperation.class, "AbstractDrawOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractDrawOperation_Color(), this.getISAColor(), null, "color", null, 1, 1, AbstractDrawOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractDrawOperation_PenSize(), ecorePackage.getEInt(), "penSize", null, 1, 1, AbstractDrawOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractDrawOperation_DrawnLines(), ecorePackage.getEString(), "drawnLines", null, 1, -1, AbstractDrawOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(drawFreehandOperationEClass, DrawFreehandOperation.class, "DrawFreehandOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(drawModuleOperationEClass, DrawModuleOperation.class, "DrawModuleOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(drawArrowOperationEClass, DrawArrowOperation.class, "DrawArrowOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDrawArrowOperation_AutoDrawArrowHead(), ecorePackage.getEBoolean(), "autoDrawArrowHead", null, 1, 1, DrawArrowOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(paintClearAllOperationEClass, PaintClearAllOperation.class, "PaintClearAllOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(paintEraseOperationEClass, PaintEraseOperation.class, "PaintEraseOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(boardEnlargeOperationEClass, BoardEnlargeOperation.class, "BoardEnlargeOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractPhotoOperationEClass, AbstractPhotoOperation.class, "AbstractPhotoOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractPhotoOperation_PhotoId(), ecorePackage.getEInt(), "photoId", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractPhotoOperation_BitmapBase64(), ecorePackage.getEString(), "bitmapBase64", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPhotoOperation_LocalSpacePosition(), this.getISAVector3(), null, "localSpacePosition", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPhotoOperation_LocalSpaceForwardDirection(), this.getISAVector3(), null, "localSpaceForwardDirection", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPhotoOperation_LocalSpaceUpwardDirection(), this.getISAVector3(), null, "localSpaceUpwardDirection", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPhotoOperation_TextureSpacePosition(), this.getISAVector2(), null, "textureSpacePosition", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPhotoOperation_CameraPositionWhenPhotoWasTaken(), this.getISAVector3(), null, "cameraPositionWhenPhotoWasTaken", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPhotoOperation_CameraRotationWhenPhotoWasTaken(), this.getISAVector3(), null, "cameraRotationWhenPhotoWasTaken", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPhotoOperation_SystemPositionWhenPhotoWasTaken(), this.getISAVector3(), null, "systemPositionWhenPhotoWasTaken", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractPhotoOperation_SystemSizeWhenPhotoWasTaken(), ecorePackage.getEFloat(), "systemSizeWhenPhotoWasTaken", null, 1, 1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractPhotoOperation_RelationsVisibleWhenPhotoWasTaken(), this.getVisibleRelation(), null, "relationsVisibleWhenPhotoWasTaken", null, 0, -1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractPhotoOperation_OpenCapsulesElementQualifiedNamesWhenPhotoWasTaken(), ecorePackage.getEString(), "openCapsulesElementQualifiedNamesWhenPhotoWasTaken", null, 0, -1, AbstractPhotoOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(photoAddOperationEClass, PhotoAddOperation.class, "PhotoAddOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(photoMoveOperationEClass, PhotoMoveOperation.class, "PhotoMoveOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPhotoMoveOperation_PreLocalSpacePosition(), this.getISAVector3(), null, "preLocalSpacePosition", null, 1, 1, PhotoMoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhotoMoveOperation_PreLocalSpaceForwardDirection(), this.getISAVector3(), null, "preLocalSpaceForwardDirection", null, 1, 1, PhotoMoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhotoMoveOperation_PreLocalSpaceUpwardDirection(), this.getISAVector3(), null, "preLocalSpaceUpwardDirection", null, 1, 1, PhotoMoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPhotoMoveOperation_PreTextureSpacePosition(), this.getISAVector2(), null, "preTextureSpacePosition", null, 1, 1, PhotoMoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(photoRemoveOperationEClass, PhotoRemoveOperation.class, "PhotoRemoveOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPhotoRemoveOperation_PlayDissolveAnimation(), ecorePackage.getEBoolean(), "playDissolveAnimation", null, 1, 1, PhotoRemoveOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(visibleRelationEClass, VisibleRelation.class, "VisibleRelation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVisibleRelation_ElementName(), ecorePackage.getEString(), "elementName", null, 1, 1, VisibleRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVisibleRelation_Forward(), ecorePackage.getEBoolean(), "forward", null, 1, 1, VisibleRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVisibleRelation_RelationType(), ecorePackage.getEInt(), "relationType", null, 1, 1, VisibleRelation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //WhiteboardPackageImpl

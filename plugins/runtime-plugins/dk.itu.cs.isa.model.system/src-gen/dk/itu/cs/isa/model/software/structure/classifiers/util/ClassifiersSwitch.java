/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.util;

import dk.itu.cs.isa.model.software.structure.classifiers.*;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage
 * @generated
 */
public class ClassifiersSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ClassifiersPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifiersSwitch() {
		if (modelPackage == null) {
			modelPackage = ClassifiersPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ClassifiersPackage.ISA_CLASSIFIER_CONTAINER: {
				ISAClassifierContainer isaClassifierContainer = (ISAClassifierContainer)theEObject;
				T result = caseISAClassifierContainer(isaClassifierContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassifiersPackage.ISA_CLASSIFIER: {
				ISAClassifier isaClassifier = (ISAClassifier)theEObject;
				T result = caseISAClassifier(isaClassifier);
				if (result == null) result = caseISAType(isaClassifier);
				if (result == null) result = caseISAClassifierContainer(isaClassifier);
				if (result == null) result = caseISAMember(isaClassifier);
				if (result == null) result = caseISAReferenceableElement(isaClassifier);
				if (result == null) result = caseISAModifiableElement(isaClassifier);
				if (result == null) result = caseISANamedSoftwareElement(isaClassifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassifiersPackage.ISA_CLASS: {
				ISAClass isaClass = (ISAClass)theEObject;
				T result = caseISAClass(isaClass);
				if (result == null) result = caseISAClassifier(isaClass);
				if (result == null) result = caseISAConstructorContainer(isaClass);
				if (result == null) result = caseISAType(isaClass);
				if (result == null) result = caseISAClassifierContainer(isaClass);
				if (result == null) result = caseISAMember(isaClass);
				if (result == null) result = caseISAReferenceableElement(isaClass);
				if (result == null) result = caseISAModifiableElement(isaClass);
				if (result == null) result = caseISANamedSoftwareElement(isaClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassifiersPackage.ISA_ABSTRACT_CLASS: {
				ISAAbstractClass isaAbstractClass = (ISAAbstractClass)theEObject;
				T result = caseISAAbstractClass(isaAbstractClass);
				if (result == null) result = caseISAClass(isaAbstractClass);
				if (result == null) result = caseISAClassifier(isaAbstractClass);
				if (result == null) result = caseISAConstructorContainer(isaAbstractClass);
				if (result == null) result = caseISAType(isaAbstractClass);
				if (result == null) result = caseISAClassifierContainer(isaAbstractClass);
				if (result == null) result = caseISAMember(isaAbstractClass);
				if (result == null) result = caseISAReferenceableElement(isaAbstractClass);
				if (result == null) result = caseISAModifiableElement(isaAbstractClass);
				if (result == null) result = caseISANamedSoftwareElement(isaAbstractClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassifiersPackage.ISA_CONCRETE_CLASS: {
				ISAConcreteClass isaConcreteClass = (ISAConcreteClass)theEObject;
				T result = caseISAConcreteClass(isaConcreteClass);
				if (result == null) result = caseISAClass(isaConcreteClass);
				if (result == null) result = caseISAClassifier(isaConcreteClass);
				if (result == null) result = caseISAConstructorContainer(isaConcreteClass);
				if (result == null) result = caseISAType(isaConcreteClass);
				if (result == null) result = caseISAClassifierContainer(isaConcreteClass);
				if (result == null) result = caseISAMember(isaConcreteClass);
				if (result == null) result = caseISAReferenceableElement(isaConcreteClass);
				if (result == null) result = caseISAModifiableElement(isaConcreteClass);
				if (result == null) result = caseISANamedSoftwareElement(isaConcreteClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassifiersPackage.ISA_INTERFACE: {
				ISAInterface isaInterface = (ISAInterface)theEObject;
				T result = caseISAInterface(isaInterface);
				if (result == null) result = caseISAClassifier(isaInterface);
				if (result == null) result = caseISAInterfaceExtendingElement(isaInterface);
				if (result == null) result = caseISAType(isaInterface);
				if (result == null) result = caseISAClassifierContainer(isaInterface);
				if (result == null) result = caseISAMember(isaInterface);
				if (result == null) result = caseISAReferenceableElement(isaInterface);
				if (result == null) result = caseISAModifiableElement(isaInterface);
				if (result == null) result = caseISANamedSoftwareElement(isaInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassifiersPackage.ISA_ENUM: {
				ISAEnum isaEnum = (ISAEnum)theEObject;
				T result = caseISAEnum(isaEnum);
				if (result == null) result = caseISAClassifier(isaEnum);
				if (result == null) result = caseISAConstructorContainer(isaEnum);
				if (result == null) result = caseISAType(isaEnum);
				if (result == null) result = caseISAClassifierContainer(isaEnum);
				if (result == null) result = caseISAMember(isaEnum);
				if (result == null) result = caseISAReferenceableElement(isaEnum);
				if (result == null) result = caseISAModifiableElement(isaEnum);
				if (result == null) result = caseISANamedSoftwareElement(isaEnum);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassifiersPackage.ISA_RECORD: {
				ISARecord isaRecord = (ISARecord)theEObject;
				T result = caseISARecord(isaRecord);
				if (result == null) result = caseISAClassifier(isaRecord);
				if (result == null) result = caseISAConstructorContainer(isaRecord);
				if (result == null) result = caseISAInterfaceExtendingElement(isaRecord);
				if (result == null) result = caseISAType(isaRecord);
				if (result == null) result = caseISAClassifierContainer(isaRecord);
				if (result == null) result = caseISAMember(isaRecord);
				if (result == null) result = caseISAReferenceableElement(isaRecord);
				if (result == null) result = caseISAModifiableElement(isaRecord);
				if (result == null) result = caseISANamedSoftwareElement(isaRecord);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ClassifiersPackage.ISA_INTERFACE_EXTENDING_ELEMENT: {
				ISAInterfaceExtendingElement isaInterfaceExtendingElement = (ISAInterfaceExtendingElement)theEObject;
				T result = caseISAInterfaceExtendingElement(isaInterfaceExtendingElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Classifier Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Classifier Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAClassifierContainer(ISAClassifierContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Classifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Classifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAClassifier(ISAClassifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAClass(ISAClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Abstract Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Abstract Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAAbstractClass(ISAAbstractClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Concrete Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Concrete Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAConcreteClass(ISAConcreteClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAInterface(ISAInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Enum</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Enum</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAEnum(ISAEnum object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Record</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Record</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISARecord(ISARecord object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Interface Extending Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Interface Extending Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAInterfaceExtendingElement(ISAInterfaceExtendingElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Named Software Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Named Software Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISANamedSoftwareElement(ISANamedSoftwareElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Referenceable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Referenceable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAReferenceableElement(ISAReferenceableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAType(ISAType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Modifiable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Modifiable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAModifiableElement(ISAModifiableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Member</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Member</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAMember(ISAMember object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISA Constructor Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISA Constructor Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISAConstructorContainer(ISAConstructorContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ClassifiersSwitch

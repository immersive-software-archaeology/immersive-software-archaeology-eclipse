/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Photo Add Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhotoAddOperation()
 * @model
 * @generated
 */
public interface PhotoAddOperation extends AbstractPhotoOperation {
} // PhotoAddOperation

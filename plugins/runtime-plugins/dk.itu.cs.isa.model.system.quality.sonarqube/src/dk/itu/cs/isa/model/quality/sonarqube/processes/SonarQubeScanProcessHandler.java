package dk.itu.cs.isa.model.quality.sonarqube.processes;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import dk.itu.cs.isa.analyzer.exceptions.UserAbortException;
import dk.itu.cs.isa.model.quality.sonarqube.preferences.ISAPreferencesSonarQube;
import dk.itu.cs.isa.model.quality.sonarqube.processes.io.SonarProcessesOutputHandler;


public class SonarQubeScanProcessHandler {
	
	public static final String NAME = "SONAR_QUBE_SCANNER_PROCESS";
	
	private String absoluteRootPath;
	
	private List<String> srcFolders = new ArrayList<>();
	private List<String> binFolders = new ArrayList<>();
	


	public boolean scanProject(String name, List<IProject> analyzedProjects, String token, boolean firstAttempt, IProgressMonitor monitor) throws UserAbortException, IOException, InterruptedException, JavaModelException {
		String sonarScannerExecutablePath = ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_SCANNER_EXECUTABLE_PATH, null);
		if(sonarScannerExecutablePath == null) {
			new ISAPreferencesSonarQube().initializeDefaultPreferences();
			sonarScannerExecutablePath = ISAPreferencesSonarQube.getString(ISAPreferencesSonarQube.SONAR_SCANNER_EXECUTABLE_PATH, null);
		}
		
		if(sonarScannerExecutablePath.isEmpty()) {
			throw new RuntimeException("SonarScanner Executable not specified!\n\n"
					+ "Please have a look at the ISA preference page (Window > Preferences > ISA) and specify "
					+ "a valid path to the SonarScanner executable.");
		}
		
		absoluteRootPath = findCommonRootPath(analyzedProjects);
		
		try {
			getJavaFolders(analyzedProjects);
			if(srcFolders.isEmpty())
				return false;
			makeJavaFoldersRelativeToRootPath(srcFolders);
			makeJavaFoldersRelativeToRootPath(binFolders);
			
			// Nice tutorial on scanner settings: https://www.devopsschool.com/tutorial/sonarqube/sonarqube-properties.html
			/*
				"C:\Eclipse\sonar-scanner-4.6.2.2472-windows\bin\sonar-scanner.bat"
				-D"sonar.projectKey=Test"
				-D"sonar.sources=src"
				-D"sonar.language=java"
				-D"sonar.java.binaries=bin"
				-D"sonar.projectBaseDir=C:\Repositories\immersive-software-archaeology-test-system-zoo\Ant"
				-D"sonar.host.url=http://localhost:9000"
				-D"sonar.login=4cd855344a64d22bee33921a13b802f3d13eee16"
			*/
			List<String> commandSegments = new ArrayList<>();
			commandSegments.add(sonarScannerExecutablePath);
			commandSegments.add("-D\"sonar.projectKey="+ name +"\"");
			commandSegments.add("-D\"sonar.sources="+ String.join(",", srcFolders) +"\"");
			commandSegments.add("-D\"sonar.language=java\"");
			commandSegments.add("-D\"sonar.java.binaries="+ String.join(",", binFolders) +"\"");
			commandSegments.add("-D\"sonar.inclusions=**/*.java\"");
			commandSegments.add("-D\"sonar.projectBaseDir="+ absoluteRootPath +"\"");
			commandSegments.add("-D\"sonar.host.url=http://localhost:9000\"");
			commandSegments.add("-D\"sonar.login="+ token +"\"");
			if(ISAPreferencesSonarQube.getBoolean(ISAPreferencesSonarQube.SONAR_SCANNER_DEBUG_MODE, false))
				commandSegments.add("--debug");

			ProcessBuilder sonarQubeProcessBuilder = new ProcessBuilder(commandSegments);
			sonarQubeProcessBuilder.directory(new File(absoluteRootPath));
			Process sonarQubeProcess = sonarQubeProcessBuilder.start();
			
			SonarProcessesOutputHandler.notifyStart(NAME);
			while(true) {
				Thread.sleep(100);
				
				BufferedReader errorReader = new BufferedReader(new InputStreamReader(sonarQubeProcess.getErrorStream()));
				if(errorReader.ready()) {
					String errorMsg = "";
					String errorLine;
					while(errorReader.ready() && (errorLine = errorReader.readLine()) != null) {
						System.out.println(errorLine);
						SonarProcessesOutputHandler.notifyError(NAME, errorLine);
						errorMsg += errorLine + "\n";
					}
					throw new RuntimeException(errorMsg);
				}
				
				BufferedReader inputReader = new BufferedReader(new InputStreamReader(sonarQubeProcess.getInputStream()));
				String outputLine;
				while(inputReader.ready() && (outputLine = inputReader.readLine()) != null) {
					System.out.println(outputLine);
					SonarProcessesOutputHandler.notifyOutput(NAME, outputLine);
					monitor.subTask(outputLine);
				}
				
				if(!sonarQubeProcess.isAlive()) {
					return true;
				} if(monitor.isCanceled()) {
					sonarQubeProcess.destroyForcibly();
					throw new UserAbortException();
				}
			}
		}
		catch(IOException e) {
			if(firstAttempt) {
				new ISAPreferencesSonarQube().initializeDefaultSonarScannerPath();
				return scanProject(name, analyzedProjects, token, false, monitor);
			}
			else return false;
		}
		finally {
			cleanUpAfterRun(absoluteRootPath);
		}
	}
	
	

	private void getJavaFolders(List<IProject> analyzedProjects) throws JavaModelException {
		for(IProject project : analyzedProjects) {
			IJavaProject jdtProject = JavaCore.create(project);
			for(IClasspathEntry entry : jdtProject.getRawClasspath()) {
				if(entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
					String absoluteSrcFolderPath = convertToAbsolutePath(entry.getPath());
					if(absoluteSrcFolderPath != null && new File(absoluteSrcFolderPath).exists())
						srcFolders.add(absoluteSrcFolderPath);
				}
				else if(entry.getEntryKind() == IClasspathEntry.CPE_LIBRARY) {
					String absoluteLibFilePath = convertToAbsolutePath(entry.getPath());
					if(absoluteLibFilePath != null && new File(absoluteLibFilePath).exists())
						binFolders.add(absoluteLibFilePath);
				}
			}
			
			String absoluteBinFolderPath = convertToAbsolutePath(jdtProject.getOutputLocation());
			if(absoluteBinFolderPath != null && new File(absoluteBinFolderPath).exists())
				binFolders.add(absoluteBinFolderPath);
			
//			for(IPackageFragmentRoot root : jdtProject.getAllPackageFragmentRoots()) {
//				String absolutePackageFragmentPath = convertToAbsolutePath(root.getPath());
//				System.out.println(absolutePackageFragmentPath);
//				System.out.println();
//			}
		}
	}
	
	private String convertToAbsolutePath(IPath path) {
		IFolder folder = ResourcesPlugin.getWorkspace().getRoot().getFolder(path);
		if(folder.exists())
			return folder.getLocation().toOSString();
		
		IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
		if(file.exists())
			return file.getLocation().toOSString();
		
		return null;
		
//		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(path.segment(0));
//		path = path.removeFirstSegments(1);
//		
//		IFile file = project.getFile(path);
//		if(file.exists()) {
//			IPath projectInternalPath = path.makeRelativeTo(project.getFullPath());
//			String absolutePath = project.getFile(projectInternalPath).getLocation().toOSString();
//			return absolutePath;
//		}
//		
//		if(path.isAbsolute())
//			return path.toOSString();
//		return null;
	}
	
	private void makeJavaFoldersRelativeToRootPath(List<String> javaFolders) {
		for(int i=0; i<javaFolders.size(); i++) {
			String absolutePath = javaFolders.get(i);
			String relativePath = absolutePath.replace(absoluteRootPath, "");
			if(relativePath.startsWith("\\"))
				relativePath = relativePath.substring(1);
			javaFolders.set(i, relativePath);
		}
	}
	
	private String findCommonRootPath(List<IProject> analyzedProjects) {
		List<String> absoluteProjectPaths = new ArrayList<>(analyzedProjects.size());
		for(IProject project : analyzedProjects) {
			absoluteProjectPaths.add(project.getLocation().toOSString());
		}
		if(absoluteProjectPaths.size() == 1)
			return absoluteProjectPaths.get(0);
		
		String commonPath = "";
		try {
		searchLoop:
			for(int i=0; true; i++) {
				char letter = absoluteProjectPaths.get(0).charAt(i);
				for(int j=1; j<absoluteProjectPaths.size(); j++) {
					if(letter != absoluteProjectPaths.get(j).charAt(i))
						break searchLoop;
				}
				commonPath += letter;
			}
		}
		catch(IndexOutOfBoundsException e) {
			// do nothing
			System.out.println("end");
		}
		
		if(commonPath.isEmpty()) {
			throw new RuntimeException(
					"Your Eclipse project organization is not compatible with the Sonar Scanner!\n"
					+ "Please organize the Eclipse projects that form your system in a way that "
					+ "they share a common root folder.");
		}
		if(!commonPath.endsWith(File.separator)) {
			// the determined common path cannot be used as is,
			// it contains a fraction of a commonly named folder at its end
			commonPath = commonPath.substring(0, commonPath.lastIndexOf(File.separator));
		}
		
		return commonPath;
	}

	public void cleanUpAfterRun(String absoluteRootPath) {
		File projectFolderFile = new File(absoluteRootPath);
		if(!projectFolderFile.exists())
			return;
		
		for(File child : projectFolderFile.listFiles()) {
			if(child.getName().equals(".scannerwork")) {
				deleteRecursively(child);
			}
		}
	}
	
	private boolean deleteRecursively(File file) {
		if(file.isFile())
			return file.delete();
		
		for(File child : file.listFiles()) {
			if(!deleteRecursively(child))
				return false;
		}
		return file.delete();
	}
	
	
	
}

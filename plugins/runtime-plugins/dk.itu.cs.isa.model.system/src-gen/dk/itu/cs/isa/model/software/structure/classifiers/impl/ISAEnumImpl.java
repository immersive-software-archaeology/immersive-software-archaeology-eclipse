/**
 */
package dk.itu.cs.isa.model.software.structure.classifiers.impl;

import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;

import dk.itu.cs.isa.model.software.structure.members.MembersPackage;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Enum</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAEnumImpl#getConstructors <em>Constructors</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.classifiers.impl.ISAEnumImpl#getConstants <em>Constants</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAEnumImpl extends ISAClassifierImpl implements ISAEnum {
	/**
	 * The cached value of the '{@link #getConstructors() <em>Constructors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstructors()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAConstructor> constructors;

	/**
	 * The cached value of the '{@link #getConstants() <em>Constants</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstants()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAEnumConstant> constants;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAEnumImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.ISA_ENUM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAEnumConstant> getConstants() {
		if (constants == null) {
			constants = new EObjectContainmentEList<ISAEnumConstant>(ISAEnumConstant.class, this, ClassifiersPackage.ISA_ENUM__CONSTANTS);
		}
		return constants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAConstructor> getConstructors() {
		if (constructors == null) {
			constructors = new EObjectContainmentEList<ISAConstructor>(ISAConstructor.class, this, ClassifiersPackage.ISA_ENUM__CONSTRUCTORS);
		}
		return constructors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ClassifiersPackage.ISA_ENUM__CONSTRUCTORS:
				return ((InternalEList<?>)getConstructors()).basicRemove(otherEnd, msgs);
			case ClassifiersPackage.ISA_ENUM__CONSTANTS:
				return ((InternalEList<?>)getConstants()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ClassifiersPackage.ISA_ENUM__CONSTRUCTORS:
				return getConstructors();
			case ClassifiersPackage.ISA_ENUM__CONSTANTS:
				return getConstants();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ClassifiersPackage.ISA_ENUM__CONSTRUCTORS:
				getConstructors().clear();
				getConstructors().addAll((Collection<? extends ISAConstructor>)newValue);
				return;
			case ClassifiersPackage.ISA_ENUM__CONSTANTS:
				getConstants().clear();
				getConstants().addAll((Collection<? extends ISAEnumConstant>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_ENUM__CONSTRUCTORS:
				getConstructors().clear();
				return;
			case ClassifiersPackage.ISA_ENUM__CONSTANTS:
				getConstants().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ClassifiersPackage.ISA_ENUM__CONSTRUCTORS:
				return constructors != null && !constructors.isEmpty();
			case ClassifiersPackage.ISA_ENUM__CONSTANTS:
				return constants != null && !constants.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISAConstructorContainer.class) {
			switch (derivedFeatureID) {
				case ClassifiersPackage.ISA_ENUM__CONSTRUCTORS: return MembersPackage.ISA_CONSTRUCTOR_CONTAINER__CONSTRUCTORS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISAConstructorContainer.class) {
			switch (baseFeatureID) {
				case MembersPackage.ISA_CONSTRUCTOR_CONTAINER__CONSTRUCTORS: return ClassifiersPackage.ISA_ENUM__CONSTRUCTORS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ISAEnumImpl

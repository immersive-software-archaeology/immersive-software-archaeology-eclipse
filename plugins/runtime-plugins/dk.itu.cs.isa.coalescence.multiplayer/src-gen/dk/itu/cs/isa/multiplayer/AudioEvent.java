/**
 */
package dk.itu.cs.isa.multiplayer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Audio Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.AudioEvent#getPinId <em>Pin Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.AudioEvent#isPlay <em>Play</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.AudioEvent#getPlaybackTime <em>Playback Time</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getAudioEvent()
 * @model
 * @generated
 */
public interface AudioEvent extends AbstractEvent {
	/**
	 * Returns the value of the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pin Id</em>' attribute.
	 * @see #setPinId(int)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getAudioEvent_PinId()
	 * @model required="true"
	 * @generated
	 */
	int getPinId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.AudioEvent#getPinId <em>Pin Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pin Id</em>' attribute.
	 * @see #getPinId()
	 * @generated
	 */
	void setPinId(int value);

	/**
	 * Returns the value of the '<em><b>Play</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Play</em>' attribute.
	 * @see #setPlay(boolean)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getAudioEvent_Play()
	 * @model required="true"
	 * @generated
	 */
	boolean isPlay();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.AudioEvent#isPlay <em>Play</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Play</em>' attribute.
	 * @see #isPlay()
	 * @generated
	 */
	void setPlay(boolean value);

	/**
	 * Returns the value of the '<em><b>Playback Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Playback Time</em>' attribute.
	 * @see #setPlaybackTime(float)
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#getAudioEvent_PlaybackTime()
	 * @model required="true"
	 * @generated
	 */
	float getPlaybackTime();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.multiplayer.AudioEvent#getPlaybackTime <em>Playback Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Playback Time</em>' attribute.
	 * @see #getPlaybackTime()
	 * @generated
	 */
	void setPlaybackTime(float value);

} // AudioEvent

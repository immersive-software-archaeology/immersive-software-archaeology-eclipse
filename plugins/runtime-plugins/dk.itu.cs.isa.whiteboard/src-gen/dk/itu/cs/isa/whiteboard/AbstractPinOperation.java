/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Pin Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getPinId <em>Pin Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getPinnedElementName <em>Pinned Element Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getTextureSpacePosition <em>Texture Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getLocalSpacePosition <em>Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getColor <em>Color</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPinOperation()
 * @model abstract="true"
 * @generated
 */
public interface AbstractPinOperation extends AbstractOperation {
	/**
	 * Returns the value of the '<em><b>Pin Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pin Id</em>' attribute.
	 * @see #setPinId(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPinOperation_PinId()
	 * @model required="true"
	 * @generated
	 */
	int getPinId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getPinId <em>Pin Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pin Id</em>' attribute.
	 * @see #getPinId()
	 * @generated
	 */
	void setPinId(int value);

	/**
	 * Returns the value of the '<em><b>Pinned Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pinned Element Name</em>' attribute.
	 * @see #setPinnedElementName(String)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPinOperation_PinnedElementName()
	 * @model required="true"
	 * @generated
	 */
	String getPinnedElementName();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getPinnedElementName <em>Pinned Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pinned Element Name</em>' attribute.
	 * @see #getPinnedElementName()
	 * @generated
	 */
	void setPinnedElementName(String value);

	/**
	 * Returns the value of the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Texture Space Position</em>' containment reference.
	 * @see #setTextureSpacePosition(ISAVector2)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPinOperation_TextureSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector2 getTextureSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getTextureSpacePosition <em>Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Texture Space Position</em>' containment reference.
	 * @see #getTextureSpacePosition()
	 * @generated
	 */
	void setTextureSpacePosition(ISAVector2 value);

	/**
	 * Returns the value of the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Space Position</em>' containment reference.
	 * @see #setLocalSpacePosition(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPinOperation_LocalSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getLocalSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getLocalSpacePosition <em>Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Space Position</em>' containment reference.
	 * @see #getLocalSpacePosition()
	 * @generated
	 */
	void setLocalSpacePosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' containment reference.
	 * @see #setColor(ISAColor)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractPinOperation_Color()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAColor getColor();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.AbstractPinOperation#getColor <em>Color</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' containment reference.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(ISAColor value);

} // AbstractPinOperation

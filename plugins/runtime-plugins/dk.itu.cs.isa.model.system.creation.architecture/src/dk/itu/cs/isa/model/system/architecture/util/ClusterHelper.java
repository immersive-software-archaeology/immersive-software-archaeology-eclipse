package dk.itu.cs.isa.model.system.architecture.util;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;

public class ClusterHelper {
	
	/**
	 * Recursively search for classifiers and assign them to a flat list of
	 * singleton components that is then used for the clustering
	 */
	public static void initializeSetOfSingletonClustersRecursively(Collection<ISACluster> initialSingletonClusters, EObject currentObject) {
		if(currentObject instanceof ISAClassifier) {
			ISACluster newSingletonCluster = ArchitectureFactory.eINSTANCE.createISACluster();
			newSingletonCluster.getClassifiers().add((ISAClassifier) currentObject);
			newSingletonCluster.setName("Singleton Cluster " + ((ISAClassifier) currentObject).getFullyQualifiedName());
			initialSingletonClusters.add(newSingletonCluster);
		}
		else {
			for(EObject child : currentObject.eContents()) {
				initializeSetOfSingletonClustersRecursively(initialSingletonClusters, child);
			}
		}
	}
	

	
	public static int getElementCount(ISACluster cluster) {
		Integer result = ISAClusteringCachingUtil.INSTANCE.getCachedContentCount(cluster);
		if(result != null)
			return result;
		
		result = cluster.getClassifiers().size();
		for(ISACluster child : cluster.getChildClusters()) {
			result += getElementCount(child);
		}
		ISAClusteringCachingUtil.INSTANCE.cacheContentCount(cluster, result);
		return result;
	}
	
	public static int getElementCountNoCaching(ISACluster cluster) {
		Integer result = cluster.getClassifiers().size();
		for(ISACluster child : cluster.getChildClusters()) {
			result += getElementCountNoCaching(child);
		}
		return result;
	}



	public static void nameCluster(ISACluster cluster) {
		cluster.setName("Cluster ["
				+ "classifiers: "+ cluster.getClassifiers().size() + ", "
				+ "sub-cluster: " + cluster.getChildClusters().size()
				+ "]");
	}
	
}














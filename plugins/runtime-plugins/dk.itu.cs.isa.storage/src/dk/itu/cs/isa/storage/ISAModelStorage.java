package dk.itu.cs.isa.storage;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.StructurePackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.system.quality.ISASoftwareSystemQualityModel;
import dk.itu.cs.isa.model.system.quality.QualityPackage;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;
import dk.itu.cs.isa.util.EcoreUtilISA;

public class ISAModelStorage {

	public static ISAModelStorage INSTANCE = new ISAModelStorage();
	
	
	
	private static final String BUNDLE_ID = "dk.itu.cs.isa.storage";
	private static final String MODELS_FOLDER_NAME = "models";
	private static final String TEMP_FOLDER_NAME = "temp";

	private static final String STRUCTURE_FILE_EXTENSION = StructurePackage.eNAME;
	private static final String QUALITY_FILE_EXTENSION = QualityPackage.eNAME;
	private static final String VISUALIZATION_FILE_EXTENSION = VisualizationPackage.eNAME;

	private File rootFolder;
	private File modelsFolder;
	private File tempFolder;
	private String storageMetaDataPath;
	
	private ResourceSet resourceSet;
	private ModelStorage storageModel;
	
	

	private ISAModelStorage() {
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(STRUCTURE_FILE_EXTENSION, new EcoreResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(QUALITY_FILE_EXTENSION, new EcoreResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(VISUALIZATION_FILE_EXTENSION, new EcoreResourceFactoryImpl());
		try {
			startUp();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void startUp() throws IOException {
		IPath rootFolderPath = Platform.getStateLocation(Platform.getBundle(BUNDLE_ID));
		rootFolder = rootFolderPath.toFile();
		if(rootFolder.isDirectory()) {
			if(rootFolder.canRead()) {
				
				// Get the Storage meta data
				storageMetaDataPath = rootFolder.getAbsolutePath() + File.separator + "ISA.model.storage.data.xml";
				Resource storageResource;
				try {
					storageResource = (new ResourceSetImpl()).getResource(URI.createFileURI(storageMetaDataPath), true);
				} catch(Exception e) {
					storageResource = (new ResourceSetImpl()).createResource(URI.createFileURI(storageMetaDataPath));
				}
				
				// Get the models folder
				String modelsFolderPath = rootFolder.getAbsolutePath() + File.separator + MODELS_FOLDER_NAME;
				modelsFolder = new File(modelsFolderPath);
				if(!modelsFolder.isDirectory()) {
					if(!modelsFolder.mkdirs()) {
						throw new IOException("Failed to create models folder at \"" + modelsFolderPath + "\"");
					}
				}

				// Get the temp folder
				String tempFolderPath = rootFolder.getAbsolutePath() + File.separator + TEMP_FOLDER_NAME;
				tempFolder = new File(tempFolderPath);
				if(!tempFolder.isDirectory()) {
					if(!tempFolder.mkdirs()) {
						throw new IOException("Failed to create temp folder at \"" + modelsFolderPath + "\"");
					}
				}
				
				for(EObject root : storageResource.getContents()) {
					if(root instanceof ModelStorage) {
						storageModel = (ModelStorage) root;
						break;
					}
				}
				if(storageModel == null) {
					storageModel = StorageFactory.eINSTANCE.createModelStorage();
					storageResource.getContents().add(storageModel);
				}
				storageModel.setBaseFolder(modelsFolder.getAbsolutePath() + File.separator);
				saveStorageModelXMLEncoded();
			}
			else {
				if(rootFolder.setReadable(true)) {
					startUp();
				}
				else {
					throw new IOException("Cannot set read permissions for the state location of bundle \"" + BUNDLE_ID + "\"");
				}
			}
		}
	}

	private void saveStorageModelXMLEncoded() throws IOException {
//		ByteArrayOutputStream stream = new ByteArrayOutputStream();
//		storageModel.eResource().save(stream, Collections.emptyMap());
//		stream.close();
		storageModel.eResource().save(Collections.EMPTY_MAP);
	}



	public File getRootFolder() {
		return rootFolder;
	}

	public File getTempFolder() {
		return tempFolder;
	}

	
	
	/**
	 * Creates a new entry to the model store and stores it persistently within the Eclipse workspace.
	 * The path for saving the models will be derived by the system's name.
	 * If an entry with equal path already exists, its entry will be overridden.
	 */
	public void createEntry(String systemName, IProgressMonitor monitor,
			ISASoftwareSystemModel structureModel,
			ISASoftwareSystemQualityModel qualityModel,
			ISAVisualizationModel visualizationModel) throws IOException {
		
		if(storageModel == null) {
			throw new IOException("The Model Storage was not loaded.");
		}
		
		monitor.beginTask("Creating a model storage entry for system \"" + systemName + "\"", 1);
		
		String fileName = "unencodable_model_" + systemName.hashCode();
		try {
			fileName = URLEncoder.encode(systemName, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
        	ISAEventSystem.fireWarningMessage("Could not encode model name: " + systemName);
        }
//		String fullGenericModelPath = modelsFolder.getAbsolutePath() + File.separator + fileName;
		
		ModelStorageEntry storageEntry = getEntry(systemName);
		if(storageEntry != null) {
			storageModel.getEntries().remove(storageEntry);
		}

		storageEntry = StorageFactory.eINSTANCE.createModelStorageEntry();
		storageEntry.setSystemName(systemName);
		storageEntry.setNumberOfClassifiers(calculateNumberOfClassifiers(structureModel));
		storageEntry.setStructureModelFileName(fileName + "." + STRUCTURE_FILE_EXTENSION);
		storageEntry.setQualityModelFileName(fileName + "." + QUALITY_FILE_EXTENSION);
		storageEntry.setVisualizationModelFileName(fileName + "." + VISUALIZATION_FILE_EXTENSION);
		storageModel.getEntries().add(storageEntry);
		
		monitor.subTask("Storing models to disk.");
		saveModelToDisk(structureModel, storageEntry.getStructureModelFileName());
		saveModelToDisk(qualityModel, storageEntry.getQualityModelFileName());
		saveModelToDisk(visualizationModel, storageEntry.getVisualizationModelFileName());

		monitor.subTask("Storing model storage state to disk.");
		saveStorageModelXMLEncoded();
	}

	private int calculateNumberOfClassifiers(ISASoftwareSystemModel structureModel) {
		int result = 0;
		for(EObject child : structureModel.getProjects())
			result += calculateNumberOfClassifiers(child);
		return result;
	}

	private int calculateNumberOfClassifiers(EObject e) {
		int result = 0;
		if(e instanceof ISAClassifier)
			result++;
		for(EObject child : e.eContents())
			result += calculateNumberOfClassifiers(child);
		return result;
	}



	private void saveModelToDisk(EObject model, String fileName) throws IOException {
		if(model == null)
			return;

		Resource resource = loadResource(modelsFolder.getAbsolutePath() + File.separator + fileName, true);
		resource.getContents().clear();
		resource.getContents().add(model);
		resource.save(Collections.EMPTY_MAP);
	}
	
	public Resource loadResource(String fullPath, boolean createOnDemand) {
		return EcoreUtilISA.loadResource(resourceSet, fullPath, createOnDemand);
	}
	
	@SuppressWarnings("unchecked")
	public <ModelType> ModelType loadModel(String fullPath) {
		Resource resource = loadResource(fullPath, false);
		if(resource == null || resource.getContents().isEmpty()) {
			throw new IllegalArgumentException("<h2>Empty Model</h2>"
					+ "<p>The model for the given path \""+ fullPath +"\" is empty.</p>");
		}
		return (ModelType) resource.getContents().get(0);
	}
	


	public void clear() throws IOException {
		for(File child : modelsFolder.listFiles()) {
			if(!child.delete()) {
				throw new IOException("Could not delete model file \"" + child.getName() + "\". Please delete your local visualization files manually at:"
						+ "\n\n" + modelsFolder);
			}
		}
		resourceSet.getResources().clear();
		storageModel.getEntries().clear();
		saveStorageModelXMLEncoded();
	}
	
	
	
	public String getStorageMetaDataPath() {
		return this.storageMetaDataPath;
	}
	
	public ModelStorage getStorageMetaData() {
		return storageModel;
	}
	
	public List<ModelStorageEntry> getAllEntries() {
		return storageModel.getEntries();
	}
	
	public ModelStorageEntry getEntry(String systemName) {
		for(ModelStorageEntry entry : getAllEntries()) {
			if(entry.getSystemName().equals(systemName)) {
				return entry;
			}
		}
		return null;
	}



	public List<String> getAllEntryNames() {
		List<ModelStorageEntry> entries = getAllEntries();
		List<String> allNames = new ArrayList<>(entries.size());
		for(ModelStorageEntry entry : entries) {
			allNames.add(entry.getSystemName());
		}
		return allNames;
	}
	
	
	
	public ISASoftwareSystemModel loadStructureModel(String systemName) {
		ModelStorageEntry entry = getEntry(systemName);
		if(entry == null)
			return null;
		return (ISASoftwareSystemModel) loadModel(modelsFolder.getAbsolutePath() + File.separator + entry.getStructureModelFileName());
	}
	
	public ISASoftwareSystemQualityModel loadQualityModel(String systemName) {
		ModelStorageEntry entry = getEntry(systemName);
		if(entry == null)
			return null;
		return (ISASoftwareSystemQualityModel) loadModel(modelsFolder.getAbsolutePath() + File.separator + entry.getQualityModelFileName());
	}
	
	public ISAVisualizationModel loadVisualizationModel(String systemName) {
		ModelStorageEntry entry = getEntry(systemName);
		if(entry == null)
			return null;
		return (ISAVisualizationModel) loadModel(modelsFolder.getAbsolutePath() + File.separator + entry.getVisualizationModelFileName());
	}

}



/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Photo</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Photo#getId <em>Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Photo#getTextureSpacePosition <em>Texture Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Photo#getLocalSpacePosition <em>Local Space Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Photo#getLocalSpaceForwardDirection <em>Local Space Forward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Photo#getLocalSpaceUpwardDirection <em>Local Space Upward Direction</em>}</li>
 *   <li>{@link dk.itu.cs.isa.whiteboard.Photo#getBitmapBase64 <em>Bitmap Base64</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhoto()
 * @model
 * @generated
 */
public interface Photo extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhoto_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Photo#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Texture Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Texture Space Position</em>' containment reference.
	 * @see #setTextureSpacePosition(ISAVector2)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhoto_TextureSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector2 getTextureSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Photo#getTextureSpacePosition <em>Texture Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Texture Space Position</em>' containment reference.
	 * @see #getTextureSpacePosition()
	 * @generated
	 */
	void setTextureSpacePosition(ISAVector2 value);

	/**
	 * Returns the value of the '<em><b>Local Space Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Space Position</em>' containment reference.
	 * @see #setLocalSpacePosition(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhoto_LocalSpacePosition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getLocalSpacePosition();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Photo#getLocalSpacePosition <em>Local Space Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Space Position</em>' containment reference.
	 * @see #getLocalSpacePosition()
	 * @generated
	 */
	void setLocalSpacePosition(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Local Space Forward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Space Forward Direction</em>' containment reference.
	 * @see #setLocalSpaceForwardDirection(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhoto_LocalSpaceForwardDirection()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getLocalSpaceForwardDirection();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Photo#getLocalSpaceForwardDirection <em>Local Space Forward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Space Forward Direction</em>' containment reference.
	 * @see #getLocalSpaceForwardDirection()
	 * @generated
	 */
	void setLocalSpaceForwardDirection(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Local Space Upward Direction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Space Upward Direction</em>' containment reference.
	 * @see #setLocalSpaceUpwardDirection(ISAVector3)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhoto_LocalSpaceUpwardDirection()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ISAVector3 getLocalSpaceUpwardDirection();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Photo#getLocalSpaceUpwardDirection <em>Local Space Upward Direction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Space Upward Direction</em>' containment reference.
	 * @see #getLocalSpaceUpwardDirection()
	 * @generated
	 */
	void setLocalSpaceUpwardDirection(ISAVector3 value);

	/**
	 * Returns the value of the '<em><b>Bitmap Base64</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bitmap Base64</em>' attribute.
	 * @see #setBitmapBase64(String)
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPhoto_BitmapBase64()
	 * @model required="true"
	 * @generated
	 */
	String getBitmapBase64();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.whiteboard.Photo#getBitmapBase64 <em>Bitmap Base64</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bitmap Base64</em>' attribute.
	 * @see #getBitmapBase64()
	 * @generated
	 */
	void setBitmapBase64(String value);

} // Photo

package dk.itu.cs.isa.model.system.creation.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.AnnotationTypeDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.RecordDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import dk.itu.cs.isa.analyzer.exceptions.UserAbortException;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersFactory;
import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterfaceExtendingElement;
import dk.itu.cs.isa.model.software.structure.classifiers.ISARecord;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackage;
import dk.itu.cs.isa.model.software.structure.libraries.ISALibrary;
import dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.members.MembersFactory;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifier;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;
import dk.itu.cs.isa.model.software.structure.misc.MiscFactory;
import dk.itu.cs.isa.model.system.creation.java.preferences.ISAPreferencesJDT;
import dk.itu.cs.isa.model.system.creation.java.visitors.CognitiveComplexityVisitor;
import dk.itu.cs.isa.model.system.creation.java.visitors.ControlFlowSplitVisitor;
import dk.itu.cs.isa.model.system.creation.java.visitors.CountVisitor;

public class ClassifierAnalyzer {

	private final ClassifiersFactory classifierFactory = ClassifiersFactory.eINSTANCE;
	private final MembersFactory memberFactory = MembersFactory.eINSTANCE;
	private final MiscFactory miscFactory = MiscFactory.eINSTANCE;

	private Map<ISAClass, String> classImplementationMap = new HashMap<>();
	private Map<ISAClassifier, List<String>> interfaceImplementationMap = new HashMap<>();
	private ReferenceHandler referenceHandler;

	public ClassifierAnalyzer(ReferenceHandler referenceHandler) {
		this.referenceHandler = referenceHandler;
	}



	/**
	 * <b>IMPORTANT</b>: Must be called AFTER references have been resolved!<br>
	 * <br>
	 * Handles the processing of class and interface extensions.
	 */
	public void resolveSuperClassifiers(ISASoftwareSystemModel isaSystem, IProgressMonitor monitor) throws UserAbortException {
		monitor.beginTask("Resolving extended classes and interfaces", classImplementationMap.size() + interfaceImplementationMap.size());
		
		// Resolve extended classes via their name
		monitor.subTask("Resolving extended classes");
		for(Entry<ISAClass, String> entry : classImplementationMap.entrySet()) {
			ISAClass classifier = entry.getKey();
			String targetClass = entry.getValue();

			ISAClass extendedClass = (ISAClass) isaSystem.getSystemElementByName(targetClass);
			if(extendedClass == null)
				extendedClass = (ISAClass) isaSystem.getLibraryElementByName(targetClass, ClassifiersPackage.ISA_CLASS, true);
			classifier.setExtendedClass(extendedClass);

			monitor.worked(1);
			if (monitor.isCanceled())
				throw new UserAbortException();
		}

		// Resolve implemented interfaces via their name
		monitor.subTask("Resolving implemented interfaces");
		for(Entry<ISAClassifier, List<String>> entry : interfaceImplementationMap.entrySet()) {
			ISAClassifier classifier = entry.getKey();
			List<String> targetInterfaces = entry.getValue();
			
			for(String targetInterface : targetInterfaces) {
				ISAInterface implementedInterface = (ISAInterface) isaSystem.getSystemElementByName(targetInterface);
				if(implementedInterface == null)
					implementedInterface = (ISAInterface) isaSystem.getLibraryElementByName(targetInterface, ClassifiersPackage.ISA_INTERFACE, true);
				
				if(classifier instanceof ISAClass)
					((ISAClass) classifier).getImplementedInterfaces().add(implementedInterface);
				else if(classifier instanceof ISAInterfaceExtendingElement)
					((ISAInterfaceExtendingElement) classifier).getExtendedInterfaces().add(implementedInterface);
				else
					throw new IllegalArgumentException("Unexpected type for classifier while resolving implemented interfaces: " + classifier.getClass().getSimpleName());
			}

			monitor.worked(1);
			if (monitor.isCanceled())
				throw new UserAbortException();
		}
	}
	
	public void setMethodAndConstructorNamesWithResolvedSignatures(EObject current, IProgressMonitor monitor) throws UserAbortException {
		if (monitor.isCanceled())
			throw new UserAbortException();

		if(current instanceof ISAParametrizableElement && current instanceof ISANamedSoftwareElement) {
			String newNameWithSignature = ((ISANamedSoftwareElement) current).getName() + "(";
			List<ISAParameter> parameters = ((ISAParametrizableElement) current).getParameters();
			List<String> parameterTypes = new ArrayList<>(parameters.size());
			for(ISAParameter param : parameters) {
				if(param.getType() == null) {
					parameterTypes.add("void");
				}
				else {
					String name = param.getType().getName();
					if(name.contains(".")) {
						String[] segments = name.split("\\.");
						name = segments[segments.length-1];
					}
					parameterTypes.add(name);
				}
			}
			newNameWithSignature += String.join(", ", parameterTypes);
			newNameWithSignature += ")";
			((ISANamedSoftwareElement) current).setName(newNameWithSignature);
		}
		
		if(current instanceof ISALibrary)
			return;
		for(EObject child : current.eContents())
			setMethodAndConstructorNamesWithResolvedSignatures(child, monitor);
	}
	
	

	/**
	 * Analyze source code files (.java)
	 * @param isaPackage
	 * @param jdtCompilationUnityProxy
	 */
	public void analyzeCompilationUnit(ISAPackage isaPackage, ICompilationUnit jdtCompilationUnityProxy) {
		ASTParser parser = ASTParser.newParser(ISAPreferencesJDT.getComplianceLevel());
		parser.setResolveBindings(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setBindingsRecovery(true);
		parser.setSource(jdtCompilationUnityProxy);
		
		CompilationUnit jdtCompilationUnitAST = (CompilationUnit) parser.createAST(null);
		IPath path = jdtCompilationUnitAST.getJavaElement().getPath();
		
		for(Object compilationUnitChild : jdtCompilationUnitAST.types()) {
			if(compilationUnitChild instanceof TypeDeclaration) {
				analyzeClassifier(isaPackage, (TypeDeclaration) compilationUnitChild, path);
			}
			else if(compilationUnitChild instanceof EnumDeclaration) {
				analyzeClassifier(isaPackage, (EnumDeclaration) compilationUnitChild, path);
			}
			else if(compilationUnitChild instanceof RecordDeclaration) {
				analyzeClassifier(isaPackage, (RecordDeclaration) compilationUnitChild, path);
			}
			else if(compilationUnitChild instanceof AnnotationTypeDeclaration) {
				// do nothing :)
				// to see what an annotation type declaration is, go here:
				// https://docs.oracle.com/javase/tutorial/java/annotations/declaring.html
			}
			else {
				throw new UnsupportedOperationException("compilation unit child is not a type, but instead a ... " + compilationUnitChild.getClass().getSimpleName());
			}
		}
	}

	/**
	 * Analyze byte code files (.class)
	 * @param isaPackage
	 * @param jdtClassFile
	 */
	public void analyzeClassFile(ISAPackage isaPackage, IClassFile jdtClassFile) {
		ASTParser parser = ASTParser.newParser(ISAPreferencesJDT.getComplianceLevel());
		parser.setResolveBindings(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setBindingsRecovery(true);
		parser.setSource(jdtClassFile);
		
		// Not implemented, as we want to analyze source code and not their libraries.
		throw new UnsupportedOperationException();
//		System.out.println(parser.createAST(null));
	}
	
	

	private void analyzeClassifier(ISAClassifierContainer parentElement, AbstractTypeDeclaration jdtTypeOrEnumDeclaration, IPath pathToFile) {
		ISAClassifier isaClassifier;
		
		if(jdtTypeOrEnumDeclaration instanceof TypeDeclaration) {
			TypeDeclaration jdtTypeDeclaration = (TypeDeclaration) jdtTypeOrEnumDeclaration;
			
			if(jdtTypeDeclaration.isInterface())
				isaClassifier = classifierFactory.createISAInterface();
			else if(Modifier.isAbstract(jdtTypeDeclaration.getModifiers()))
				isaClassifier = classifierFactory.createISAAbstractClass();
			else
				isaClassifier = classifierFactory.createISAConcreteClass();
			
			if(jdtTypeDeclaration.getSuperclassType() != null) {
				ITypeBinding resolvedTypeBinding = jdtTypeDeclaration.getSuperclassType().resolveBinding();
				classImplementationMap.put((ISAClass) isaClassifier, resolvedTypeBinding.getQualifiedName());
			}
			if(jdtTypeDeclaration.superInterfaceTypes() != null) {
				List<String> interfaces = new ArrayList<>();
				for(Object superInterface : jdtTypeDeclaration.superInterfaceTypes()) {
					ITypeBinding resolvedTypeBinding = ((Type) superInterface).resolveBinding();
					interfaces.add(resolvedTypeBinding.getQualifiedName());
				}
				if(interfaces.size() > 0)
					interfaceImplementationMap.put(isaClassifier, interfaces);
			}
		}
		else if(jdtTypeOrEnumDeclaration instanceof EnumDeclaration) {
			EnumDeclaration jdtEnumDeclaration = (EnumDeclaration) jdtTypeOrEnumDeclaration;
			
			isaClassifier = classifierFactory.createISAEnum();
			
			for(Object jdtEnumConstant : jdtEnumDeclaration.enumConstants()) {
				if(jdtEnumConstant instanceof EnumConstantDeclaration) {
					analyzeEnumConstant((ISAEnum) isaClassifier, (EnumConstantDeclaration) jdtEnumConstant);
				}
			}
		}
		else if(jdtTypeOrEnumDeclaration instanceof RecordDeclaration) {
			RecordDeclaration jdtRecordDeclaration = (RecordDeclaration) jdtTypeOrEnumDeclaration;
			
			isaClassifier = classifierFactory.createISARecord();
			
			for(FieldDeclaration jdtFieldDeclaration : jdtRecordDeclaration.getFields())
				analyzeField(isaClassifier, jdtFieldDeclaration);
			
			for(Object recordComponentObject : jdtRecordDeclaration.recordComponents()) {
				SingleVariableDeclaration recordComponentDeclaration = (SingleVariableDeclaration) recordComponentObject;
				analyzeRecordComponentDeclaration((ISARecord) isaClassifier, recordComponentDeclaration);
			}
			
			if(jdtRecordDeclaration.superInterfaceTypes() != null) {
				List<String> interfaces = new ArrayList<>();
				for(Object superInterface : jdtRecordDeclaration.superInterfaceTypes()) {
					ITypeBinding resolvedTypeBinding = ((Type) superInterface).resolveBinding();
					interfaces.add(resolvedTypeBinding.getQualifiedName());
				}
				if(interfaces.size() > 0)
					interfaceImplementationMap.put(isaClassifier, interfaces);
			}
		}
		else throw new UnsupportedOperationException();
		
		isaClassifier.setName(jdtTypeOrEnumDeclaration.getName().getIdentifier());
		if(!pathToFile.makeRelative().lastSegment().startsWith(isaClassifier.getName()))
			isaClassifier.setFilePath(pathToFile.makeRelative().toString());
		
		if(parentElement instanceof ISAClassifier) {
			setMemberSpecificAttributes(jdtTypeOrEnumDeclaration, isaClassifier);
		}
		
		for(Object declaration : jdtTypeOrEnumDeclaration.bodyDeclarations()) {
			if(declaration instanceof AbstractTypeDeclaration)
				analyzeClassifier(isaClassifier, (AbstractTypeDeclaration) declaration, pathToFile);
			
			else if(declaration instanceof FieldDeclaration)
				analyzeField(isaClassifier, (FieldDeclaration) declaration);
			
			else if(declaration instanceof MethodDeclaration) {
				if(((MethodDeclaration) declaration).isConstructor()) {
					analyzeConstructor(isaClassifier, (MethodDeclaration) declaration, false);
				}
				/*else if(jdtMethodOrConstructor.isCompactConstructor()) {
					analyzeConstructor(isaClassifier, jdtMethodOrConstructor, true);
				}*/
				else {
					analyzeMethod(isaClassifier, (MethodDeclaration) declaration);
				}
			}
			
			else if(declaration instanceof Initializer) {
				// Nothing to do here, is covered by reference resolving visitors
			}
			
			else System.err.println("Unhandled kind of declaration in class \"" + isaClassifier.getName() + "\": " + declaration.getClass().getSimpleName());
		}
		
		addModifiers(isaClassifier, jdtTypeOrEnumDeclaration.getModifiers(), ISAModifier.NONE);
		parentElement.getClassifiers().add(isaClassifier);
//		referenceHandler.registerReferencingElement(jdtTypeOrEnumDeclaration, isaClassifier);
	}
	
	
	
	private void analyzeRecordComponentDeclaration(ISARecord isaClassifier, SingleVariableDeclaration recordComponentDeclaration) {
		ISARecordComponent isaRecordComponent = memberFactory.createISARecordComponent();
		isaClassifier.getComponents().add(isaRecordComponent);

		isaRecordComponent.getModifiers().add(ISAModifier.PUBLIC);
		registerType(isaRecordComponent, recordComponentDeclaration.getType());
		setMemberSpecificAttributes(recordComponentDeclaration, isaRecordComponent);
		isaRecordComponent.setName(recordComponentDeclaration.getName().getIdentifier());
	}

	private void analyzeEnumConstant(ISAEnum isaEnum, EnumConstantDeclaration jdtEnumConstant) {
		ISAEnumConstant isaEnumConst = memberFactory.createISAEnumConstant();
		isaEnum.getConstants().add(isaEnumConst);
		
		setMemberSpecificAttributes(jdtEnumConstant, isaEnumConst);
		isaEnumConst.setName(jdtEnumConstant.getName().getIdentifier());
	}

	private void analyzeField(ISAClassifier isaClassifier, FieldDeclaration jdtField) {
		for(Object fieldFragment : jdtField.fragments()) {
			ISAField isaField = memberFactory.createISAField();
			isaClassifier.getFields().add(isaField);
			 
			addModifiers(isaField, jdtField.getModifiers(), ISAModifier.NONE);
			registerType(isaField, jdtField.getType());

			setMemberSpecificAttributes(jdtField, isaField);
			isaField.setName(((VariableDeclarationFragment) fieldFragment).getName().getIdentifier());
			
			referenceHandler.registerReferencingElement((VariableDeclarationFragment) fieldFragment, isaField);
		}
	}

	private void analyzeConstructor(ISAClassifier isaClassifier, MethodDeclaration jdtConstructor, boolean isCompact) {
		ISAConstructor isaConstructor = memberFactory.createISAConstructor();
		if(isaClassifier instanceof ISAClass)
			((ISAClass) isaClassifier).getConstructors().add(isaConstructor);
		else if(isaClassifier instanceof ISAEnum)
			((ISAEnum) isaClassifier).getConstructors().add(isaConstructor);
		else if(isaClassifier instanceof ISARecord)
			((ISARecord) isaClassifier).getConstructors().add(isaConstructor);
		else throw new RuntimeException("Only classes and enums can contain constructors");
		
		setStmtListContainerValues(isaConstructor, jdtConstructor);
		addModifiers(isaConstructor, jdtConstructor.getModifiers(), ISAModifier.NONE);
		setParameters(isaConstructor, jdtConstructor.parameters());
		
		setMemberSpecificAttributes(jdtConstructor, isaConstructor);
		isaConstructor.setName(jdtConstructor.getName().getIdentifier());
		
		referenceHandler.registerReferencingElement(jdtConstructor, isaConstructor);
	}

	private void analyzeMethod(ISAClassifier isaClassifier, MethodDeclaration jdtMethod) {
		ISAMethod isaMethod;
		if(Modifier.isAbstract(jdtMethod.getModifiers()) || jdtMethod.getBody() == null) {
			isaMethod = memberFactory.createISAAbstractMethod();
		}
		else {
			isaMethod = memberFactory.createISAConcreteMethod();
			setStmtListContainerValues((ISAConcreteMethod) isaMethod, jdtMethod);
			referenceHandler.registerReferencingElement(jdtMethod, (ISAConcreteMethod) isaMethod);
		}

		isaClassifier.getMethods().add(isaMethod);
		
		addModifiers(isaMethod, jdtMethod.getModifiers(), ISAModifier.NONE);
		setParameters(isaMethod, jdtMethod.parameters());
		registerType(isaMethod, jdtMethod.getReturnType2());
		
		setMemberSpecificAttributes(jdtMethod, isaMethod);
		isaMethod.setName(jdtMethod.getName().getIdentifier());
	}
	
	
	
	private void setMemberSpecificAttributes(ASTNode jdtMemberNode, ISAMember isaMember) {
		ASTNode parent = jdtMemberNode.getParent();
		StructuralPropertyDescriptor location = jdtMemberNode.getLocationInParent();
		isaMember.setNumberInOrderOfDeclaration(((List<?>) parent.getStructuralProperty(location)).indexOf(jdtMemberNode));
		isaMember.setStartPositionInDocument(jdtMemberNode.getStartPosition());
		isaMember.setEndPositionInDocument(jdtMemberNode.getStartPosition() + jdtMemberNode.getLength());
	}
	
	private void setStmtListContainerValues(ISAStatementContainer isaContainer, MethodDeclaration jdtMethodOrConstructor) {
		// Count expressions and statements
		{
			CountVisitor visitor = new CountVisitor();
			jdtMethodOrConstructor.accept(visitor);
			
			isaContainer.setNumberOfExpressions(visitor.getExpressionsCount());
			isaContainer.setNumberOfStatements(visitor.getStatementCount());
		}

		// Count control flow splits
		{
			ControlFlowSplitVisitor visitor = new ControlFlowSplitVisitor();
			jdtMethodOrConstructor.accept(visitor);
			
			isaContainer.setNumberOfControlFlowSplits(visitor.getNumberOfControlFlowSplits());
		}

		// Calculate cognitive complexity
		{
			CognitiveComplexityVisitor visitor = new CognitiveComplexityVisitor(jdtMethodOrConstructor);
			jdtMethodOrConstructor.accept(visitor);
			
			isaContainer.setCognitiveComplexity(visitor.getCalculatedComplexity());
		}
	}

	private void addModifiers(ISAModifiableElement element, int modifiers, ISAModifier defaultVisibility) {
		if(Modifier.isStatic(modifiers))
			element.getModifiers().add(ISAModifier.STATIC);
		if(Modifier.isFinal(modifiers))
			element.getModifiers().add(ISAModifier.FINAL);
		
		if(Modifier.isPrivate(modifiers))
			element.getModifiers().add(ISAModifier.PRIVATE);
		else if(Modifier.isPublic(modifiers))
			element.getModifiers().add(ISAModifier.PUBLIC);
		else if(Modifier.isProtected(modifiers))
			element.getModifiers().add(ISAModifier.PROTECTED);
		else
			element.getModifiers().add(defaultVisibility);
	}
	
	private void setParameters(ISAParametrizableElement isaParametrizable, List<?> parameters) {
		for(Object parameter : parameters) {
			if(parameter instanceof VariableDeclaration) {
				ISAParameter newParameter = miscFactory.createISAParameter();
				newParameter.setName(((VariableDeclaration) parameter).getName().getIdentifier());
				isaParametrizable.getParameters().add(newParameter);
				
				if(parameter instanceof SingleVariableDeclaration) {
					registerType(newParameter, ((SingleVariableDeclaration) parameter).getType());
					newParameter.setVarArg(((SingleVariableDeclaration) parameter).isVarargs());
				}
				else if(parameter instanceof VariableDeclarationFragment) {
//					((VariableDeclarationFragment) parameter);
					ISAEventSystem.fireStandardConsoleMessage("A parameter is not a single variable "
							+ "declaration, conversion to ISA model needs to be extended here!");
				}
				else ISAEventSystem.fireStandardConsoleMessage("A variable declaration parameter "
						+ "was of unexpected kind: " + parameter.getClass().getSimpleName());
			}
			else ISAEventSystem.fireStandardConsoleMessage("A parameter was of unexpected kind: "
					+ "" + parameter.getClass().getSimpleName());
		}
	}
	
	private void registerType(ISATypedElement element, Type type) {
		if(type == null) {
			referenceHandler.registerType(element, null);
		}
		else {			
			ITypeBinding resolvedTypeBinding = type.resolveBinding();
			referenceHandler.registerType(element, resolvedTypeBinding.getQualifiedName());
			element.setArrayType(type.isArrayType());
		}
	}

}




















package dk.itu.cs.isa.model.software.structure.filesystem.impl;

import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;

public class FilesystemFactoryImplCustom extends FilesystemFactoryImpl {
	
	@Override
	public ISAProject createISAProject() {
		return new ISAProjectImplCustom();
	}

}

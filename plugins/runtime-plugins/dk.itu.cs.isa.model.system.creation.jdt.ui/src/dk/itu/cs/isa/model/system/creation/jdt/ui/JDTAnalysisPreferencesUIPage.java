package dk.itu.cs.isa.model.system.creation.jdt.ui;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

import dk.itu.cs.isa.model.system.creation.java.preferences.ISAPreferencesJDT;

public class JDTAnalysisPreferencesUIPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	
	public JDTAnalysisPreferencesUIPage() {
		// TODO add fancy image!
		super(GRID);
	}



	@Override
	public void createFieldEditors() {
		if(ISAPreferencesJDT.getString(ISAPreferencesJDT.JDT_COMPLIANCE_LEVEL, null) == null)
			performDefaults();
		
		addSpace();
		
		String[][] javaVersions = new String[14][2];
		javaVersions[0] = new String[]  { "Java 1.3", "3" };
		javaVersions[1] = new String[]  { "Java 1.4", "4" };
		javaVersions[2] = new String[]  { "Java 1.5", "5" };
		javaVersions[3] = new String[]  { "Java 1.6", "6" };
		javaVersions[4] = new String[]  { "Java 1.7", "7" };
		javaVersions[5] = new String[]  { "Java 1.8", "8" };
		javaVersions[6] = new String[]  { "Java 9",   "9" };
		javaVersions[7] = new String[]  { "Java 10",  "10" };
		javaVersions[8] = new String[]  { "Java 11",  "11" };
		javaVersions[9] = new String[]  { "Java 12",  "12" };
		javaVersions[10] = new String[] { "Java 13",  "13" };
		javaVersions[11] = new String[] { "Java 14",  "14" };
		javaVersions[12] = new String[] { "Java 15",  "15" };
		javaVersions[13] = new String[] { "Java 16",  "16" };
		javaVersions[13] = new String[] { "Java 17",  "17" };
//		javaVersions[13] = new String[] { "Java 18",  "18" };
//		javaVersions[13] = new String[] { "Java 19",  "19" };
//		javaVersions[13] = new String[] { "Java 20",  "20" };
		addField(new ComboFieldEditor(ISAPreferencesJDT.JDT_COMPLIANCE_LEVEL, "&JDT Parser Compliance Level:", javaVersions, getFieldEditorParent()));
		
		addSpace();
	}

	private void addSpace() {
		new Label(getFieldEditorParent(), SWT.NONE).setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
	}
	
	

	@Override
	public void init(IWorkbench workbench) {
		setTitle("ISA - Immersive Software Archeology");
		setDescription("Settings page for system structure analysis based on Eclipse JDT plugins");

		setPreferenceStore(new ScopedPreferenceStore(InstanceScope.INSTANCE, ISAPreferencesJDT.STORAGE_ID));
	}
	
	
	
	@Override
	public void performDefaults() {
		super.performDefaults();
		
		new ISAPreferencesJDT().initializeDefaultPreferences();
		
		checkState();
		updateApplyButton();
	}
	
}

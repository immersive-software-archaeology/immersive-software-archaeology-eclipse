/**
 */
package dk.itu.cs.isa.model.software.structure.members.util;

import dk.itu.cs.isa.model.software.structure.members.*;

import dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage
 * @generated
 */
public class MembersAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MembersPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MembersAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MembersPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MembersSwitch<Adapter> modelSwitch =
		new MembersSwitch<Adapter>() {
			@Override
			public Adapter caseISAConstructorContainer(ISAConstructorContainer object) {
				return createISAConstructorContainerAdapter();
			}
			@Override
			public Adapter caseISAMember(ISAMember object) {
				return createISAMemberAdapter();
			}
			@Override
			public Adapter caseISAField(ISAField object) {
				return createISAFieldAdapter();
			}
			@Override
			public Adapter caseISAEnumConstant(ISAEnumConstant object) {
				return createISAEnumConstantAdapter();
			}
			@Override
			public Adapter caseISARecordComponent(ISARecordComponent object) {
				return createISARecordComponentAdapter();
			}
			@Override
			public Adapter caseISAConstructor(ISAConstructor object) {
				return createISAConstructorAdapter();
			}
			@Override
			public Adapter caseISAMethod(ISAMethod object) {
				return createISAMethodAdapter();
			}
			@Override
			public Adapter caseISAConcreteMethod(ISAConcreteMethod object) {
				return createISAConcreteMethodAdapter();
			}
			@Override
			public Adapter caseISAAbstractMethod(ISAAbstractMethod object) {
				return createISAAbstractMethodAdapter();
			}
			@Override
			public Adapter caseISANamedSoftwareElement(ISANamedSoftwareElement object) {
				return createISANamedSoftwareElementAdapter();
			}
			@Override
			public Adapter caseISAReferenceableElement(ISAReferenceableElement object) {
				return createISAReferenceableElementAdapter();
			}
			@Override
			public Adapter caseISAModifiableElement(ISAModifiableElement object) {
				return createISAModifiableElementAdapter();
			}
			@Override
			public Adapter caseISAReferencingElement(ISAReferencingElement object) {
				return createISAReferencingElementAdapter();
			}
			@Override
			public Adapter caseISATypedElement(ISATypedElement object) {
				return createISATypedElementAdapter();
			}
			@Override
			public Adapter caseISAStatementContainer(ISAStatementContainer object) {
				return createISAStatementContainerAdapter();
			}
			@Override
			public Adapter caseISAParametrizableElement(ISAParametrizableElement object) {
				return createISAParametrizableElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer <em>ISA Constructor Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAConstructorContainer
	 * @generated
	 */
	public Adapter createISAConstructorContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAMember <em>ISA Member</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAMember
	 * @generated
	 */
	public Adapter createISAMemberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant <em>ISA Enum Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant
	 * @generated
	 */
	public Adapter createISAEnumConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISARecordComponent <em>ISA Record Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISARecordComponent
	 * @generated
	 */
	public Adapter createISARecordComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAField <em>ISA Field</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAField
	 * @generated
	 */
	public Adapter createISAFieldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAConstructor <em>ISA Constructor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAConstructor
	 * @generated
	 */
	public Adapter createISAConstructorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAMethod <em>ISA Method</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAMethod
	 * @generated
	 */
	public Adapter createISAMethodAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod <em>ISA Concrete Method</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod
	 * @generated
	 */
	public Adapter createISAConcreteMethodAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.members.ISAAbstractMethod <em>ISA Abstract Method</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.members.ISAAbstractMethod
	 * @generated
	 */
	public Adapter createISAAbstractMethodAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement <em>ISA Named Software Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement
	 * @generated
	 */
	public Adapter createISANamedSoftwareElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement <em>ISA Referenceable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement
	 * @generated
	 */
	public Adapter createISAReferenceableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement <em>ISA Modifiable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAModifiableElement
	 * @generated
	 */
	public Adapter createISAModifiableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement <em>ISA Referencing Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement
	 * @generated
	 */
	public Adapter createISAReferencingElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISATypedElement <em>ISA Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISATypedElement
	 * @generated
	 */
	public Adapter createISATypedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer <em>ISA Statement Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer
	 * @generated
	 */
	public Adapter createISAStatementContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement <em>ISA Parametrizable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement
	 * @generated
	 */
	public Adapter createISAParametrizableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //MembersAdapterFactory

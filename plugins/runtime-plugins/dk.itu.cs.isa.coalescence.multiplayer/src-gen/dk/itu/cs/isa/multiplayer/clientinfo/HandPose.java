/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Hand Pose</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage#getHandPose()
 * @model
 * @generated
 */
public enum HandPose implements Enumerator {
	/**
	 * The '<em><b>DEFAULT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DEFAULT_VALUE
	 * @generated
	 * @ordered
	 */
	DEFAULT(0, "DEFAULT", "DEFAULT"),

	/**
	 * The '<em><b>FIST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIST_VALUE
	 * @generated
	 * @ordered
	 */
	FIST(1, "FIST", "FIST"),

	/**
	 * The '<em><b>POINTING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #POINTING_VALUE
	 * @generated
	 * @ordered
	 */
	POINTING(2, "POINTING", "POINTING"),

	/**
	 * The '<em><b>WHITEBOARD SPAWN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WHITEBOARD_SPAWN_VALUE
	 * @generated
	 * @ordered
	 */
	WHITEBOARD_SPAWN(3, "WHITEBOARD_SPAWN", "WHITEBOARD_SPAWN"),

	/**
	 * The '<em><b>WHITEBOARD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WHITEBOARD_VALUE
	 * @generated
	 * @ordered
	 */
	WHITEBOARD(4, "WHITEBOARD", "WHITEBOARD"),

	/**
	 * The '<em><b>PIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PIN_VALUE
	 * @generated
	 * @ordered
	 */
	PIN(5, "PIN", "PIN"),

	/**
	 * The '<em><b>PEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PEN_VALUE
	 * @generated
	 * @ordered
	 */
	PEN(6, "PEN", "PEN"),

	/**
	 * The '<em><b>PREVIEW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PREVIEW_VALUE
	 * @generated
	 * @ordered
	 */
	PREVIEW(7, "PREVIEW", "PREVIEW"),

	/**
	 * The '<em><b>CAPSULE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAPSULE_VALUE
	 * @generated
	 * @ordered
	 */
	CAPSULE(8, "CAPSULE", "CAPSULE"),

	/**
	 * The '<em><b>STATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STATION_VALUE
	 * @generated
	 * @ordered
	 */
	STATION(9, "STATION", "STATION"),

	/**
	 * The '<em><b>UI SOFTWARE CANVAS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UI_SOFTWARE_CANVAS_VALUE
	 * @generated
	 * @ordered
	 */
	UI_SOFTWARE_CANVAS(10, "UI_SOFTWARE_CANVAS", "UI_SOFTWARE_CANVAS"),

	/**
	 * The '<em><b>CAMERA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAMERA_VALUE
	 * @generated
	 * @ordered
	 */
	CAMERA(11, "CAMERA", "CAMERA"),

	/**
	 * The '<em><b>PICTURE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PICTURE_VALUE
	 * @generated
	 * @ordered
	 */
	PICTURE(12, "PICTURE", "PICTURE"),

	/**
	 * The '<em><b>SYSTEM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYSTEM_VALUE
	 * @generated
	 * @ordered
	 */
	SYSTEM(13, "SYSTEM", "SYSTEM"), /**
	 * The '<em><b>MICROPHONE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MICROPHONE_VALUE
	 * @generated
	 * @ordered
	 */
	MICROPHONE(14, "MICROPHONE", "MICROPHONE"), /**
	 * The '<em><b>UI SOFTWARE CANVAS SCROLL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UI_SOFTWARE_CANVAS_SCROLL_VALUE
	 * @generated
	 * @ordered
	 */
	UI_SOFTWARE_CANVAS_SCROLL(15, "UI_SOFTWARE_CANVAS_SCROLL", "UI_SOFTWARE_CANVAS_SCROLL");

	/**
	 * The '<em><b>DEFAULT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DEFAULT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DEFAULT_VALUE = 0;

	/**
	 * The '<em><b>FIST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIST
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIST_VALUE = 1;

	/**
	 * The '<em><b>POINTING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #POINTING
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int POINTING_VALUE = 2;

	/**
	 * The '<em><b>WHITEBOARD SPAWN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WHITEBOARD_SPAWN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int WHITEBOARD_SPAWN_VALUE = 3;

	/**
	 * The '<em><b>WHITEBOARD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WHITEBOARD
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int WHITEBOARD_VALUE = 4;

	/**
	 * The '<em><b>PIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PIN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PIN_VALUE = 5;

	/**
	 * The '<em><b>PEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PEN_VALUE = 6;

	/**
	 * The '<em><b>PREVIEW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PREVIEW
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PREVIEW_VALUE = 7;

	/**
	 * The '<em><b>CAPSULE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAPSULE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CAPSULE_VALUE = 8;

	/**
	 * The '<em><b>STATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STATION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STATION_VALUE = 9;

	/**
	 * The '<em><b>UI SOFTWARE CANVAS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UI_SOFTWARE_CANVAS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UI_SOFTWARE_CANVAS_VALUE = 10;

	/**
	 * The '<em><b>CAMERA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAMERA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CAMERA_VALUE = 11;

	/**
	 * The '<em><b>PICTURE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PICTURE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PICTURE_VALUE = 12;

	/**
	 * The '<em><b>SYSTEM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYSTEM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SYSTEM_VALUE = 13;

	/**
	 * The '<em><b>MICROPHONE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MICROPHONE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MICROPHONE_VALUE = 14;

	/**
	 * The '<em><b>UI SOFTWARE CANVAS SCROLL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UI_SOFTWARE_CANVAS_SCROLL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UI_SOFTWARE_CANVAS_SCROLL_VALUE = 15;

	/**
	 * An array of all the '<em><b>Hand Pose</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final HandPose[] VALUES_ARRAY =
		new HandPose[] {
			DEFAULT,
			FIST,
			POINTING,
			WHITEBOARD_SPAWN,
			WHITEBOARD,
			PIN,
			PEN,
			PREVIEW,
			CAPSULE,
			STATION,
			UI_SOFTWARE_CANVAS,
			CAMERA,
			PICTURE,
			SYSTEM,
			MICROPHONE,
			UI_SOFTWARE_CANVAS_SCROLL,
		};

	/**
	 * A public read-only list of all the '<em><b>Hand Pose</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<HandPose> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Hand Pose</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static HandPose get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			HandPose result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Hand Pose</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static HandPose getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			HandPose result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Hand Pose</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static HandPose get(int value) {
		switch (value) {
			case DEFAULT_VALUE: return DEFAULT;
			case FIST_VALUE: return FIST;
			case POINTING_VALUE: return POINTING;
			case WHITEBOARD_SPAWN_VALUE: return WHITEBOARD_SPAWN;
			case WHITEBOARD_VALUE: return WHITEBOARD;
			case PIN_VALUE: return PIN;
			case PEN_VALUE: return PEN;
			case PREVIEW_VALUE: return PREVIEW;
			case CAPSULE_VALUE: return CAPSULE;
			case STATION_VALUE: return STATION;
			case UI_SOFTWARE_CANVAS_VALUE: return UI_SOFTWARE_CANVAS;
			case CAMERA_VALUE: return CAMERA;
			case PICTURE_VALUE: return PICTURE;
			case SYSTEM_VALUE: return SYSTEM;
			case MICROPHONE_VALUE: return MICROPHONE;
			case UI_SOFTWARE_CANVAS_SCROLL_VALUE: return UI_SOFTWARE_CANVAS_SCROLL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private HandPose(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //HandPose

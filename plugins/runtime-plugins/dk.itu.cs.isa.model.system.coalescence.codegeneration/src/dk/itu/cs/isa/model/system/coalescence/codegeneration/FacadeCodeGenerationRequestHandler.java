package dk.itu.cs.isa.model.system.coalescence.codegeneration;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.ToolFactory;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifier;
import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.util.ISASystemModelToFileSystemUtil;
import dk.itu.cs.isa.model.system.creation.java.preferences.ISAPreferencesJDT;
import dk.itu.cs.isa.storage.ISAModelStorage;
import dk.itu.cs.isa.util.FileUtil;
import dk.itu.cs.isa.util.StringUtil;

public class FacadeCodeGenerationRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		String classifierQualifiedNamesConcatenated = baseRequest.getParameter("classifierQualifiedNames");
		
		if(systemName == null || classifierQualifiedNamesConcatenated == null) {
			String examplePath1 = "systemName=JetUML&"
					+ "classifierQualifiedNames=org.jetuml.gui.EditorFrame,org.jetuml.gui.GuiUtils,org.jetuml.persistence.AbstractContext";
			printInvalidParametersHTMLPage(baseRequest, response, null, examplePath1);
			return;
		}

		PrintWriter out = response.getWriter();
		try {
			ISASoftwareSystemModel structureModel = ISAModelStorage.INSTANCE.loadStructureModel(systemName);
			if(structureModel == null)
				throw new NullPointerException("Cannot load system model for \"" + systemName + "\". Did the meta model change?");
			
			String[] classifierQualifiedNames = classifierQualifiedNamesConcatenated.split(",");
			ISAClassifier[] classifiers = new ISAClassifier[classifierQualifiedNames.length];
			
			for(int i=0; i<classifierQualifiedNames.length; i++) {
				ISAClassifier resolvedClassifier = (ISAClassifier) structureModel.getSystemElementByName(classifierQualifiedNames[i]);
				if(resolvedClassifier == null)
					throw new NullPointerException("Cannot find classifier for \"" + classifierQualifiedNames[i] + "\"");
					
				classifiers[i] = resolvedClassifier;
			}
			
			String generatedSource = generateFacadeForClassifiers(classifiers);
			
			response.setContentType("text/java; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_OK);
			
			out.print(generatedSource);
			baseRequest.setHandled(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_FORBIDDEN, "Something went wrong", e);
			baseRequest.setHandled(true);
		}
		finally {
			out.close();
		}
	}
	
	
	
	private String generateFacadeForClassifiers(ISAClassifier[] classifiers) throws Exception {
		List<ISAMethod> publicMethods = new ArrayList<>();
		List<String> packageNames = new ArrayList<String>();
		for(ISAClassifier classifier : classifiers) {
			publicMethods.addAll(getPublicMethods(classifier));
			packageNames.add(classifier.getContainingPackage().getFullyQualifiedName());
		}

		IFile classifierFile = ISASystemModelToFileSystemUtil.getFileForClassifier(classifiers[0]);
		IProject classifierProject = classifierFile.getProject();
		IJavaProject javaProject = JavaCore.create(classifierProject);
		IFolder targetSourceFolder = CodeGenerationUtil.createNewSourceFolder(classifierProject, javaProject, "src-gen-isa");

		String facadeInterfaceName = deriveFacadeClassName(classifiers);
		String facadeInterfacePackage = deriveFacadePackage(packageNames);
		String pathToFacadeInterface = facadeInterfacePackage.replaceAll("\\.", "/") + "/" + facadeInterfaceName + ".java";
		IFile facadeInterfaceFile = FileUtil.createEmptyFileInFolder(targetSourceFolder, pathToFacadeInterface, true, true, null);
		
		String code = generateCode(facadeInterfaceFile, facadeInterfacePackage, facadeInterfaceName, publicMethods);
		Map<String, String> options = javaProject.getOptions(true);
		code = CodeGenerationUtil.formatCode(code, ToolFactory.createCodeFormatter(options));
		
		facadeInterfaceFile.setContents(new ByteArrayInputStream(code.getBytes()), IFile.FORCE, null);
		
		return code;
	}

	private String deriveFacadePackage(List<String> packageNames) {
		String packageName;
		if(packageNames.size() > 1)
			packageName = StringUtil.calculateLongestCommonSubString(packageNames);
		else
			packageName = packageNames.get(0);
		if(packageName.endsWith("."))
			packageName = packageName.substring(0, packageName.length()-1);
		return packageName;
	}

	private String deriveFacadeClassName(ISAClassifier[] classifiers) {
		String facadeClassName = "";
		for(ISAClassifier classifier : classifiers)
			facadeClassName += classifier.getName();
		return facadeClassName + "Facade";
	}

	private List<ISAMethod> getPublicMethods(ISAClassifier classifier) {
		List<ISAMethod> publicMethods = new ArrayList<>();
		for(ISAMethod method : classifier.getMethods()) {
			if(method.getModifiers().contains(ISAModifier.PUBLIC))
				publicMethods.add(method);
		}
		return publicMethods;
	}
	
	

	@SuppressWarnings("unchecked")
	private String generateCode(IFile fileToGenerateInto, String packageName, String interfaceName, List<ISAMethod> publicMethods) throws Exception {
		ICompilationUnit compilationUnit = JavaCore.createCompilationUnitFrom(fileToGenerateInto);
		ASTParser parser = ASTParser.newParser(ISAPreferencesJDT.getComplianceLevel());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setResolveBindings(true);
		parser.setSource(compilationUnit);
		// Parse the source code and generate an AST.
		CompilationUnit compilationUnitNode = (CompilationUnit) parser.createAST(null);
		compilationUnitNode.recordModifications();

		AST ast = compilationUnitNode.getAST();
	    
		PackageDeclaration packageDeclaration = ast.newPackageDeclaration();
		packageDeclaration.setName(ast.newName(packageName));
		compilationUnitNode.setPackage(packageDeclaration);
		
//		ImportDeclaration importDeclaration = ast.newImportDeclaration();
//		QualifiedName name = 
//		    ast.newQualifiedName(
//		    ast.newSimpleName("java"),
//		    ast.newSimpleName("util"));
//		importDeclaration.setName(name);
//		importDeclaration.setOnDemand(true);
//		compilationUnitNode.imports().add(importDeclaration);
		
		TypeDeclaration type = ast.newTypeDeclaration();
		type.setInterface(true);
		type.modifiers().add(ast.newModifier(Modifier.ModifierKeyword.PUBLIC_KEYWORD));
	    type.setName(ast.newSimpleName(interfaceName));
	    compilationUnitNode.types().add(type);
	    
	    for(ISAMethod isaMethod : publicMethods) {
	    	MethodDeclaration jdtMethod = generateInterfaceMethodForMethod(ast, isaMethod);
	    	type.bodyDeclarations().add(jdtMethod);
	    }
	    
	    return compilationUnitNode.toString();
	}
	
	@SuppressWarnings("unchecked")
	private MethodDeclaration generateInterfaceMethodForMethod(AST ast, ISAMethod isaMethod) {
    	MethodDeclaration jdtMethod = ast.newMethodDeclaration();
    	jdtMethod.setName(ast.newSimpleName(((ISAClassifier)isaMethod.eContainer()).getName() + "$" + isaMethod.getName()));
    	jdtMethod.setConstructor(false);
		jdtMethod.modifiers().add(ast.newModifier(Modifier.ModifierKeyword.PUBLIC_KEYWORD));

		Type returnType = CodeGenerationUtil.getTypeForQualifiedName(ast, isaMethod.getType().getFullyQualifiedName());
    	jdtMethod.setReturnType2(returnType);
    	
    	for(ISAParameter isaParameter : isaMethod.getParameters()) {
    		SingleVariableDeclaration jdtParameter = ast.newSingleVariableDeclaration();
    		jdtParameter.setName(ast.newSimpleName(isaParameter.getName()));
			jdtParameter.setVarargs(isaParameter.isVarArg());
    		
    		Type parameterType = CodeGenerationUtil.getTypeForQualifiedName(ast, isaParameter.getType().getFullyQualifiedName());
    		if(isaParameter.isArrayType())
    			parameterType = ast.newArrayType(parameterType);
    		jdtParameter.setType(parameterType);
    		
    		jdtMethod.parameters().add(jdtParameter);
    	}
    	
    	jdtMethod.typeParameters();
		
    	return jdtMethod;
	}

}










/**
 */
package dk.itu.cs.isa.model.software.structure.architecture;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.model.software.structure.architecture.ArchitecturePackage
 * @generated
 */
public interface ArchitectureFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArchitectureFactory eINSTANCE = dk.itu.cs.isa.model.software.structure.architecture.impl.ArchitectureFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ISA Cluster</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Cluster</em>'.
	 * @generated
	 */
	ISACluster createISACluster();

	/**
	 * Returns a new object of class '<em>ISA Word Occurrence Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ISA Word Occurrence Entry</em>'.
	 * @generated
	 */
	ISAWordOccurrenceEntry createISAWordOccurrenceEntry();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ArchitecturePackage getArchitecturePackage();

} //ArchitectureFactory

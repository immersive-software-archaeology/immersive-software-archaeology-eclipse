package dk.itu.cs.isa.model.system.quality.sonarqube.ui.view;

import dk.itu.cs.isa.eclipse.ui.views.server.ISAServerStatusView;
import dk.itu.cs.isa.model.quality.sonarqube.processes.SonarQubeServerProcessHandler;

public class SonarQubeServerStatusView extends ISAServerStatusView {

	@Override
	protected void setServer() {
		this.server = SonarQubeServerProcessHandler.INSTANCE;
	}
	
}

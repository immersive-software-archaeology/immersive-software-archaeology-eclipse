/**
 */
package dk.itu.cs.isa.model.software.structure.filesystem.impl;

import dk.itu.cs.isa.model.software.structure.filesystem.FilesystemPackage;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Package Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageRootImpl#getParentProject <em>Parent Project</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.filesystem.impl.ISAPackageRootImpl#isBinary <em>Binary</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAPackageRootImpl extends ISAPackageContainerImpl implements ISAPackageRoot {
	/**
	 * The default value of the '{@link #isBinary() <em>Binary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBinary()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BINARY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBinary() <em>Binary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBinary()
	 * @generated
	 * @ordered
	 */
	protected boolean binary = BINARY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAPackageRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FilesystemPackage.Literals.ISA_PACKAGE_ROOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAProject getParentProject() {
		if (eContainerFeatureID() != FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT) return null;
		return (ISAProject)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentProject(ISAProject newParentProject, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentProject, FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentProject(ISAProject newParentProject) {
		if (newParentProject != eInternalContainer() || (eContainerFeatureID() != FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT && newParentProject != null)) {
			if (EcoreUtil.isAncestor(this, newParentProject))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentProject != null)
				msgs = ((InternalEObject)newParentProject).eInverseAdd(this, FilesystemPackage.ISA_PROJECT__PACKAGE_ROOTS, ISAProject.class, msgs);
			msgs = basicSetParentProject(newParentProject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT, newParentProject, newParentProject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isBinary() {
		return binary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBinary(boolean newBinary) {
		boolean oldBinary = binary;
		binary = newBinary;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FilesystemPackage.ISA_PACKAGE_ROOT__BINARY, oldBinary, binary));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentProject((ISAProject)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT:
				return basicSetParentProject(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT:
				return eInternalContainer().eInverseRemove(this, FilesystemPackage.ISA_PROJECT__PACKAGE_ROOTS, ISAProject.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT:
				return getParentProject();
			case FilesystemPackage.ISA_PACKAGE_ROOT__BINARY:
				return isBinary();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT:
				setParentProject((ISAProject)newValue);
				return;
			case FilesystemPackage.ISA_PACKAGE_ROOT__BINARY:
				setBinary((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT:
				setParentProject((ISAProject)null);
				return;
			case FilesystemPackage.ISA_PACKAGE_ROOT__BINARY:
				setBinary(BINARY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FilesystemPackage.ISA_PACKAGE_ROOT__PARENT_PROJECT:
				return getParentProject() != null;
			case FilesystemPackage.ISA_PACKAGE_ROOT__BINARY:
				return binary != BINARY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (binary: ");
		result.append(binary);
		result.append(')');
		return result.toString();
	}

} //ISAPackageRootImpl

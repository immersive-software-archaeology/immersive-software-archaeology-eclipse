package dk.itu.cs.isa.model.system.creation.srcml;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.w3c.dom.Element;

import dk.itu.cs.isa.analyzer.exceptions.UserAbortException;
import dk.itu.cs.isa.analyzer.interfaces.ISystemModelCreator;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.StructureFactory;
import dk.itu.cs.isa.preferences.ISAPreferences;

@Deprecated
public class SystemModelCreatorSrcML implements ISystemModelCreator {

	private Map<String, Map<String, Element>> sortedCodeWithExtension;

	@Override
	public void initializeInstance() {
	}

	@Override
	public void setupBeforeRun() {
		sortedCodeWithExtension = new HashMap<>();
	}

	@Override
	public void cleanUpAfterRun() {
		sortedCodeWithExtension = null;
	}

	@Override
	public ISASoftwareSystemModel run(String name, List<IProject> selectedProjects, IProgressMonitor monitor) throws Exception {
		throw new UnsupportedOperationException("The SrcML based system analysis is currently not available - we plan to integrate it in a future release.");
	}

	public ISASoftwareSystemModel runActual(String name, List<IProject> selectedProjects, IProgressMonitor monitor) throws Exception {
		ISASoftwareSystemModel systemModel = StructureFactory.eINSTANCE.createISASoftwareSystemModel();
		systemModel.setName(name);

		sortedCodeWithExtension.put("java", new HashMap<String, Element>());
		sortedCodeWithExtension.put("cs", new HashMap<String, Element>());
		sortedCodeWithExtension.put("cpp", new HashMap<String, Element>());

		monitor.beginTask("Analysing projects", selectedProjects.size());
		for (IProject project : selectedProjects) {
			monitor.subTask(project.getName());

			searchAndSortCode(project, monitor);

			monitor.worked(1);
			monitor.subTask("");
		}

		return systemModel;
	}

	private void searchAndSortCode(IContainer container, IProgressMonitor monitor) throws Exception {
		for (IResource member : container.members()) {
			if (monitor.isCanceled())
				throw new UserAbortException();

			if (member instanceof IContainer) {
				searchAndSortCode((IContainer) member, monitor);
			} else if (member instanceof IFile) {
				IFile file = (IFile) member;
				Map<String, Element> codeArtifactsMap = sortedCodeWithExtension.get(file.getFileExtension());
				if (codeArtifactsMap == null) {
					continue;
				}

				String fileLocation = file.getLocation().toOSString();
				codeArtifactsMap.put(fileLocation, getXMLviaSrcML(fileLocation));
			}
		}
	}

	private Element getXMLviaSrcML(String fileName) throws Exception {
		ProcessBuilder processBuilder = new ProcessBuilder("srcml", fileName);

//		int srcmlTimeout = ISAPreferences.getInt("SRCML_EXECUTABLE_TIMEOUT", 2000);
		String srcmlExePath = ISAPreferences.getString("SRCML_EXECUTABLE_PATH", null);
		if (srcmlExePath == null) {
			throw new RuntimeException(
					"SrcML Executable Directory not specified! Please have a look at the ISA preference settings via: Window > Preferences > ISA");
		}

		Element result = null;

		processBuilder.directory(new File(srcmlExePath));
		Process process = processBuilder.start();

		InputStream inputStream = process.getInputStream();
		result = XMLHelper.parseXML(inputStream);

		inputStream.close();

//		InputStream errorStream = process.getErrorStream();
//		int i;
//		StringBuilder error = new StringBuilder();
//		while ((i = errorStream.read()) != -1) {
//			error.append((char) i);
//		}
//		System.out.println(error.toString());
//		errorStream.close();

		process.destroy();
		return result;
	}

}

/**
 */
package dk.itu.cs.isa.storage.impl;

import dk.itu.cs.isa.storage.ModelStorage;
import dk.itu.cs.isa.storage.ModelStorageEntry;
import dk.itu.cs.isa.storage.StoragePackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model Storage</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.storage.impl.ModelStorageImpl#getEntries <em>Entries</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.impl.ModelStorageImpl#getBaseFolder <em>Base Folder</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelStorageImpl extends MinimalEObjectImpl.Container implements ModelStorage {
	/**
	 * The cached value of the '{@link #getEntries() <em>Entries</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntries()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelStorageEntry> entries;

	/**
	 * The default value of the '{@link #getBaseFolder() <em>Base Folder</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseFolder()
	 * @generated
	 * @ordered
	 */
	protected static final String BASE_FOLDER_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getBaseFolder() <em>Base Folder</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseFolder()
	 * @generated
	 * @ordered
	 */
	protected String baseFolder = BASE_FOLDER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelStorageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StoragePackage.Literals.MODEL_STORAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ModelStorageEntry> getEntries() {
		if (entries == null) {
			entries = new EObjectContainmentEList<ModelStorageEntry>(ModelStorageEntry.class, this, StoragePackage.MODEL_STORAGE__ENTRIES);
		}
		return entries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBaseFolder() {
		return baseFolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBaseFolder(String newBaseFolder) {
		String oldBaseFolder = baseFolder;
		baseFolder = newBaseFolder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StoragePackage.MODEL_STORAGE__BASE_FOLDER, oldBaseFolder, baseFolder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StoragePackage.MODEL_STORAGE__ENTRIES:
				return ((InternalEList<?>)getEntries()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StoragePackage.MODEL_STORAGE__ENTRIES:
				return getEntries();
			case StoragePackage.MODEL_STORAGE__BASE_FOLDER:
				return getBaseFolder();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StoragePackage.MODEL_STORAGE__ENTRIES:
				getEntries().clear();
				getEntries().addAll((Collection<? extends ModelStorageEntry>)newValue);
				return;
			case StoragePackage.MODEL_STORAGE__BASE_FOLDER:
				setBaseFolder((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StoragePackage.MODEL_STORAGE__ENTRIES:
				getEntries().clear();
				return;
			case StoragePackage.MODEL_STORAGE__BASE_FOLDER:
				setBaseFolder(BASE_FOLDER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StoragePackage.MODEL_STORAGE__ENTRIES:
				return entries != null && !entries.isEmpty();
			case StoragePackage.MODEL_STORAGE__BASE_FOLDER:
				return BASE_FOLDER_EDEFAULT == null ? baseFolder != null : !BASE_FOLDER_EDEFAULT.equals(baseFolder);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (baseFolder: ");
		result.append(baseFolder);
		result.append(')');
		return result.toString();
	}

} //ModelStorageImpl

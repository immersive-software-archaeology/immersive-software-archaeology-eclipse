package dk.itu.cs.isa.whiteboard.requests;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.util.FileUtil;
import dk.itu.cs.isa.whiteboard.AbstractWhiteboardEvent;
import dk.itu.cs.isa.whiteboard.Photo;
import dk.itu.cs.isa.whiteboard.Pin;
import dk.itu.cs.isa.whiteboard.PinAudioAddOperation;
import dk.itu.cs.isa.whiteboard.Whiteboard;
import dk.itu.cs.isa.whiteboard.WhiteboardHistory;
import dk.itu.cs.isa.whiteboard.WhiteboardStorage;

public class WhiteboardHtmlDisplayRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		String index = baseRequest.getParameter("index");
		
		if(systemName == null || index == null) {
			String examplePath1 = "systemName=JetUML&index=0";
			printInvalidParametersHTMLPage(baseRequest, response, null, examplePath1);
			return;
		}

		PrintWriter out = response.getWriter();
		try {
			response.setContentType("text/html; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_OK);
			
			out.print(getHTMLCode(systemName, Integer.valueOf(index)));
			
			baseRequest.setHandled(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_FORBIDDEN, "Error", e, "Could not display whiteboard.");
			baseRequest.setHandled(true);
		}
		finally {
			out.close();
		}
	}

	private String getHTMLCode(String systemName, int index) {
		Whiteboard board = WhiteboardStorage.INSTANCE.getWhiteboardByIndex(systemName, index);
		int numberOfBoards = WhiteboardStorage.INSTANCE.getNumberOfWhiteboardsForSystem(systemName);
		
		try {
			if(board == null)
				throw new NullPointerException("No Whiteboard available");
			
			List<String> htmlLines = FileUtil.readContentFromPluginFile("dk.itu.cs.isa.whiteboard", "html/index.html");

			boolean isInPinDefinition = false;
			String pinHTML = "";
			
			boolean isInPhotoDefinition = false;
			String photoHTML = "";
			
			for(int i=0; i<htmlLines.size(); i++) {
				if(htmlLines.get(i).contains("id=\"drawing\""))
					htmlLines.set(i, "<img id=\"drawing\" class=\"selector\" src=\"data:image/png;base64, " + board.getBitmapBase64() + "\"/>");
				
				if(htmlLines.get(i).contains("<span>0/1</span>"))
					htmlLines.set(i, "<span>" + (index+1) + "/" + numberOfBoards + "</span>");

				if(htmlLines.get(i).contains("<!-- pin start -->")) {
					isInPinDefinition = true;
				}
				else if(htmlLines.get(i).contains("<!-- pin end -->")) {
					isInPinDefinition = false;
					htmlLines.set(i, createPinHTML(board, pinHTML));
				}
				else if(isInPinDefinition) {
					pinHTML += htmlLines.get(i) + "\n";
					htmlLines.set(i, "");
				}

				if(htmlLines.get(i).contains("<!-- photo start -->")) {
					isInPhotoDefinition = true;
				}
				else if(htmlLines.get(i).contains("<!-- photo end -->")) {
					isInPhotoDefinition = false;
					htmlLines.set(i, createPhotoHTML(board, photoHTML));
				}
				else if(isInPhotoDefinition) {
					photoHTML += htmlLines.get(i) + "\n";
					htmlLines.set(i, "");
				}
			}
			
			return String.join(System.lineSeparator(), htmlLines);
		} catch (Exception e) {
//			e.printStackTrace();
			return createErrorHTML(e);
		}
	}
	
	private String createPinHTML(Whiteboard board, String pinBoilerPlate) {
		String html = "";
		
		for(Pin pin : board.getPins()) {
			String pinHTML = pinBoilerPlate;
			int r = 238,g = 238,b = 238;
			if(pin.getColor() != null) {
				r = (int) (pin.getColor().getR()*255);
				g = (int) (pin.getColor().getG()*255);
				b = (int) (pin.getColor().getB()*255);
			}
			
			pinHTML = pinHTML.replace("class=\"st0\"", "style=\"fill:rgba("+ r +","+ g +","+ b +",1);\"");
//			pinHTML = pinHTML.replace("class=\"st0\"", "style=\"\"");
			pinHTML = pinHTML.replace("style=\"\"", "style=\"left: "+ (pin.getTextureSpacePosition().getX() - 25) +"px; top: "+ (pin.getTextureSpacePosition().getY() - 25) +"px;\"");
			
			if(pin.getElementQualifiedName() != null) {
				// Software Element Pin
				String systemName = board.getContainingHistory().getSystemName();
				String elementName = pin.getElementQualifiedName();
				pinHTML = pinHTML.replace("GEN_ACTION", "openInIDE('" + systemName + "', '" + elementName + "')");
				pinHTML = pinHTML.replace("AUDIO_BASE64", "");
				pinHTML = pinHTML.replace("dk.itu.cs.isa.ExampleName", elementName);
			}
			else {
				// Audio Pin
				pinHTML = pinHTML.replace("GEN_ACTION", "playSound(this)");
				pinHTML = pinHTML.replace("AUDIO_BASE64", getSoundWaveBase64(pin));
				pinHTML = pinHTML.replace("dk.itu.cs.isa.ExampleName", "Audio Pin");
			}
			
			html += pinHTML + "\n" + "\n";
		}
		
		return html;
	}
	
	private String getSoundWaveBase64(Pin pin) {
		WhiteboardHistory history = pin.getWhiteboard().getContainingHistory();
		for(AbstractWhiteboardEvent ev : history.getEventLog()) {
			if(ev instanceof PinAudioAddOperation) {
				PinAudioAddOperation pinAddOperation = (PinAudioAddOperation) ev;
				if(pinAddOperation.getPinId() == pin.getId())
					return "data:audio/wav;base64," + pinAddOperation.getSoundWaveBase64();
			}
		}
		return "TODO: default error sound";
	}

	private String createPhotoHTML(Whiteboard board, String photoBoilerPlate) {
		String html = "";
		
		for(Photo photo : board.getPhotos()) {
			String photoHTML = photoBoilerPlate;
			
			double rad = Math.atan2(photo.getLocalSpaceForwardDirection().getZ(), photo.getLocalSpaceForwardDirection().getX());
			double rotationInDegrees = rad * 180d / Math.PI;
		    if (rotationInDegrees < 0)
		    	rotationInDegrees = 360 + rotationInDegrees;
			
			photoHTML = photoHTML.replaceFirst("class=\"photo selector\" style=\"\"", "class=\"photo selector\" style=\""
					+ "transform: rotate(" + rotationInDegrees + "deg);"
					+ "left: " + (photo.getTextureSpacePosition().getX() - 75) + "px;"
					+ "top: " + (photo.getTextureSpacePosition().getY() - 75) + "px;"
					+ "\"");
			photoHTML = photoHTML.replace("src=\"testboard.png\"", "src=\"data:image/png;base64, " + photo.getBitmapBase64() + "\"");
			
			html += photoHTML + "\n" + "\n";
		}
		
		return html;
	}
	
	private String createErrorHTML(Exception e) {
		return "<div style=\"font-family: 'Open Sans', Sans-serif; padding: 50px;\">"
				+ "Whiteboard view could not initialize:<br><br><strong>" + e.getMessage() + "<strong>"
				+ "</div>";
	}

}










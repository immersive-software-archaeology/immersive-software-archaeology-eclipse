/**
 */
package dk.itu.cs.isa.multiplayer.impl;

import dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent;
import dk.itu.cs.isa.multiplayer.ElementType;
import dk.itu.cs.isa.multiplayer.ISAVector3;
import dk.itu.cs.isa.multiplayer.MultiplayerPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Change Element Active State Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.ChangeElementActiveStateEventImpl#getElementType <em>Element Type</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.ChangeElementActiveStateEventImpl#getElementId <em>Element Id</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.ChangeElementActiveStateEventImpl#isNewState <em>New State</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.ChangeElementActiveStateEventImpl#getPosition <em>Position</em>}</li>
 *   <li>{@link dk.itu.cs.isa.multiplayer.impl.ChangeElementActiveStateEventImpl#getRotation <em>Rotation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ChangeElementActiveStateEventImpl extends AbstractEventImpl implements ChangeElementActiveStateEvent {
	/**
	 * The default value of the '{@link #getElementType() <em>Element Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementType()
	 * @generated
	 * @ordered
	 */
	protected static final ElementType ELEMENT_TYPE_EDEFAULT = ElementType.OTHER;

	/**
	 * The cached value of the '{@link #getElementType() <em>Element Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementType()
	 * @generated
	 * @ordered
	 */
	protected ElementType elementType = ELEMENT_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getElementId() <em>Element Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementId()
	 * @generated
	 * @ordered
	 */
	protected static final int ELEMENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getElementId() <em>Element Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementId()
	 * @generated
	 * @ordered
	 */
	protected int elementId = ELEMENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isNewState() <em>New State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNewState()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NEW_STATE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNewState() <em>New State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNewState()
	 * @generated
	 * @ordered
	 */
	protected boolean newState = NEW_STATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPosition() <em>Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosition()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 position;

	/**
	 * The cached value of the '{@link #getRotation() <em>Rotation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRotation()
	 * @generated
	 * @ordered
	 */
	protected ISAVector3 rotation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChangeElementActiveStateEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MultiplayerPackage.Literals.CHANGE_ELEMENT_ACTIVE_STATE_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementType getElementType() {
		return elementType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementType(ElementType newElementType) {
		ElementType oldElementType = elementType;
		elementType = newElementType == null ? ELEMENT_TYPE_EDEFAULT : newElementType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_TYPE, oldElementType, elementType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getElementId() {
		return elementId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementId(int newElementId) {
		int oldElementId = elementId;
		elementId = newElementId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_ID, oldElementId, elementId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNewState() {
		return newState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNewState(boolean newNewState) {
		boolean oldNewState = newState;
		newState = newNewState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__NEW_STATE, oldNewState, newState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getPosition() {
		return position;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPosition(ISAVector3 newPosition, NotificationChain msgs) {
		ISAVector3 oldPosition = position;
		position = newPosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION, oldPosition, newPosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosition(ISAVector3 newPosition) {
		if (newPosition != position) {
			NotificationChain msgs = null;
			if (position != null)
				msgs = ((InternalEObject)position).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION, null, msgs);
			if (newPosition != null)
				msgs = ((InternalEObject)newPosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION, null, msgs);
			msgs = basicSetPosition(newPosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION, newPosition, newPosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 getRotation() {
		return rotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRotation(ISAVector3 newRotation, NotificationChain msgs) {
		ISAVector3 oldRotation = rotation;
		rotation = newRotation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION, oldRotation, newRotation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRotation(ISAVector3 newRotation) {
		if (newRotation != rotation) {
			NotificationChain msgs = null;
			if (rotation != null)
				msgs = ((InternalEObject)rotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION, null, msgs);
			if (newRotation != null)
				msgs = ((InternalEObject)newRotation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION, null, msgs);
			msgs = basicSetRotation(newRotation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION, newRotation, newRotation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION:
				return basicSetPosition(null, msgs);
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION:
				return basicSetRotation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_TYPE:
				return getElementType();
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_ID:
				return getElementId();
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__NEW_STATE:
				return isNewState();
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION:
				return getPosition();
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION:
				return getRotation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_TYPE:
				setElementType((ElementType)newValue);
				return;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_ID:
				setElementId((Integer)newValue);
				return;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__NEW_STATE:
				setNewState((Boolean)newValue);
				return;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION:
				setPosition((ISAVector3)newValue);
				return;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION:
				setRotation((ISAVector3)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_TYPE:
				setElementType(ELEMENT_TYPE_EDEFAULT);
				return;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_ID:
				setElementId(ELEMENT_ID_EDEFAULT);
				return;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__NEW_STATE:
				setNewState(NEW_STATE_EDEFAULT);
				return;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION:
				setPosition((ISAVector3)null);
				return;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION:
				setRotation((ISAVector3)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_TYPE:
				return elementType != ELEMENT_TYPE_EDEFAULT;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_ID:
				return elementId != ELEMENT_ID_EDEFAULT;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__NEW_STATE:
				return newState != NEW_STATE_EDEFAULT;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION:
				return position != null;
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION:
				return rotation != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (elementType: ");
		result.append(elementType);
		result.append(", elementId: ");
		result.append(elementId);
		result.append(", newState: ");
		result.append(newState);
		result.append(')');
		return result.toString();
	}

} //ChangeElementActiveStateEventImpl

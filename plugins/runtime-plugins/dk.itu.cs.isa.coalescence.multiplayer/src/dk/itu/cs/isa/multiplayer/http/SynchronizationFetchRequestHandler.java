package dk.itu.cs.isa.multiplayer.http;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.extensions.ExtensionPointHandler;
import dk.itu.cs.isa.coalescence.server.extensions.ISAAbstractSocketImplementation;
import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.multiplayer.udp.SynchronizationUdpServer;
import dk.itu.cs.isa.util.EcoreUtilISA;

public class SynchronizationFetchRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		String latestUpdateTimestampString = baseRequest.getParameter("latestUpdateTimestamp");
		long latestUpdateTimestamp;
		try {
			latestUpdateTimestamp = Long.parseLong(latestUpdateTimestampString);
		} catch(NumberFormatException e) {
			latestUpdateTimestamp = 0;
		}
		
		if(systemName == null) {
			String examplePath1 = "systemName=ArgoUML";
			String examplePath2 = "systemName=JetUML&latestUpdateTimestamp=" + (System.currentTimeMillis() - 1000*60*60);
			String explanation = "You can specify a timestamp to pull only events that happened after the time point it specifies";
			printInvalidParametersHTMLPage(baseRequest, response, explanation, examplePath1, examplePath2);
			return;
		}

		PrintWriter out = response.getWriter();
		try {
			response.setContentType("application/xml; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_OK);
			
			SynchronizationUdpServer server = null;
			for(ISAAbstractSocketImplementation s : ExtensionPointHandler.INSTANCE.getSockets()) {
				if(s instanceof SynchronizationUdpServer) {
					server = (SynchronizationUdpServer) s;
					break;
				}
			}
			if(server == null)
				 throw new NullPointerException("Synchronization socket not found.");
			
			out.println(EcoreUtilISA.getXMLStringForModel(server.fetchOperations(systemName, latestUpdateTimestamp)));
			baseRequest.setHandled(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			printHTMLPage(baseRequest, response, HttpServletResponse.SC_FORBIDDEN, "Error", e, "Event could not be synchronized");
			baseRequest.setHandled(true);
		}
		finally {
			out.close();
		}
	}

}










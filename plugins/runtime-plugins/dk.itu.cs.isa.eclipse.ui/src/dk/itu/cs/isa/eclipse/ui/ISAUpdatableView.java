package dk.itu.cs.isa.eclipse.ui;

public interface ISAUpdatableView {
	
	public void update();

}

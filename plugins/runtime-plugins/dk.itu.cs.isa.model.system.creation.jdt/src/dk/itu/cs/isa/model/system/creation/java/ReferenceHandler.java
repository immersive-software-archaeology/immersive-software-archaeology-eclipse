package dk.itu.cs.isa.model.system.creation.java;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.dom.ASTNode;

import dk.itu.cs.isa.analyzer.exceptions.UserAbortException;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.classifiers.ClassifiersPackage;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;

public class ReferenceHandler {

	private Map<ASTNode, ISAReferencingElement> elementMapping = new LinkedHashMap<>();
	private Map<ISATypedElement, String> elementTypeNameMapping = new LinkedHashMap<>();
	
	private Exception workerException;
	private int finishedReferenceResolvingWorkers;
	private int availableCores;



	public void registerReferencingElement(ASTNode jdtElement, ISAReferencingElement isaElement) {
		elementMapping.put(jdtElement, isaElement);
	}

	public void registerType(ISATypedElement element, String qualifiedName) {
		elementTypeNameMapping.put(element, qualifiedName);
	}




	/**
	 * After this method was called, the system model should contain references to elements according to the respective elements' types.<br>
	 * For instance, a class will contain all references to method, fields, and other classes that are somewhere contained in it.<br>
	 * A member will contain all references to classes and methods that are contained in it.<br><br>
	 * <strong>IMPORTANT</strong>: a class' contained references supersedes the references contained in their member!
	 */
	public void resolveReferences(IProgressMonitor monitor) throws Exception {
		monitor.beginTask("Setting up references", elementMapping.keySet().size());
		ISASoftwareSystemModel isaSystem = getContainingSystemModel((EObject) elementMapping.values().toArray()[0]);
		
		// It is important that types are resolved before references are set,
		// because otherwise parameter types are not available which makes it impossible to calculate method signatures
		// which makes it impossible to resolve method calls
		monitor.subTask("Resolving member types...");
		resolveTypes(isaSystem);

		monitor.subTask("Resolving and calculating method calls, field accesses and type references...");
		finishedReferenceResolvingWorkers = 0;
		availableCores = Runtime.getRuntime().availableProcessors();

		// Resolve all references of the elements stored in the map
		for(int i=0; i<availableCores; i++) {
			ReferenceResolvingWorker worker = new ReferenceResolvingWorker(this, i, availableCores, isaSystem, elementMapping, monitor);
        	(new Thread(null, worker, "ISA Reference Resolving Worker "+(i+1))).start();
        }
		
		while(finishedReferenceResolvingWorkers < availableCores && workerException == null) {
			// Wait for all threads to return their results...!
			Thread.sleep(20);
			if(monitor.isCanceled())
				throw new UserAbortException();
		}

		if(workerException != null)
			throw workerException;
	}
	
	private ISASoftwareSystemModel getContainingSystemModel(EObject current) {
		if(current == null)
			throw new RuntimeException("Referencing element not contained inside a model!");
		if(current instanceof ISASoftwareSystemModel)
			return (ISASoftwareSystemModel) current;
		else return getContainingSystemModel(current.eContainer());
	}
	
	private void resolveTypes(ISASoftwareSystemModel isaSystem) {
		for(ISATypedElement element : elementTypeNameMapping.keySet()) {
			String typeQualifiedName = elementTypeNameMapping.get(element);
			ISANamedSoftwareElement type = isaSystem.getSystemElementByName(typeQualifiedName);
			if(type == null)
				type = isaSystem.getLibraryElementByName(typeQualifiedName, ClassifiersPackage.ISA_CLASS, true);
			
			// if type is null, it is set to void, simply do not assign a type then
			if(type instanceof ISAType)
				element.setType((ISAType) type);
			else if(type != null)
				throw new RuntimeException("Type reference was resolved to an object of type " + type);
		}
	}



	public synchronized void callBackForReferenceResolvingWorkers(int startIndex, int workedElementsCount) {
		finishedReferenceResolvingWorkers++;
		ISAEventSystem.fireStandardConsoleMessage("Reference resolving: " + finishedReferenceResolvingWorkers + " / " + availableCores + " workers finished."
				+ "\t| start index: " + startIndex
				+ "\t| handled elements: " + workedElementsCount);
	}

	public synchronized void callBackForReferenceResolvingWorkers(Exception e) {
		workerException = e;
	}
}

/**
 */
package dk.itu.cs.isa.whiteboard;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Compound Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.whiteboard.AbstractCompoundOperation#getSubOperations <em>Sub Operations</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractCompoundOperation()
 * @model abstract="true"
 * @generated
 */
public interface AbstractCompoundOperation extends AbstractOperation {
	/**
	 * Returns the value of the '<em><b>Sub Operations</b></em>' containment reference list.
	 * The list contents are of type {@link dk.itu.cs.isa.whiteboard.AbstractOperation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Operations</em>' containment reference list.
	 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getAbstractCompoundOperation_SubOperations()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractOperation> getSubOperations();

} // AbstractCompoundOperation

/**
 */
package dk.itu.cs.isa.model.visualization.impl;

import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.model.visualization.ISAVisualizationModel;
import dk.itu.cs.isa.model.visualization.VisualizationPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Visualization Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISAVisualizationModelImpl#getSubjectSystemCapsule <em>Subject System Capsule</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISAVisualizationModelImpl#getLibraryCapsules <em>Library Capsules</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.visualization.impl.ISAVisualizationModelImpl#getModelCreatorName <em>Model Creator Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ISAVisualizationModelImpl extends ISANamedVisualElementImpl implements ISAVisualizationModel {
	/**
	 * The cached value of the '{@link #getSubjectSystemCapsule() <em>Subject System Capsule</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubjectSystemCapsule()
	 * @generated
	 * @ordered
	 */
	protected ISATransparentCapsule subjectSystemCapsule;

	/**
	 * The cached value of the '{@link #getLibraryCapsules() <em>Library Capsules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLibraryCapsules()
	 * @generated
	 * @ordered
	 */
	protected EList<ISATransparentCapsule> libraryCapsules;

	/**
	 * The default value of the '{@link #getModelCreatorName() <em>Model Creator Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelCreatorName()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_CREATOR_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelCreatorName() <em>Model Creator Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelCreatorName()
	 * @generated
	 * @ordered
	 */
	protected String modelCreatorName = MODEL_CREATOR_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAVisualizationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VisualizationPackage.Literals.ISA_VISUALIZATION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISATransparentCapsule getSubjectSystemCapsule() {
		return subjectSystemCapsule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubjectSystemCapsule(ISATransparentCapsule newSubjectSystemCapsule, NotificationChain msgs) {
		ISATransparentCapsule oldSubjectSystemCapsule = subjectSystemCapsule;
		subjectSystemCapsule = newSubjectSystemCapsule;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE, oldSubjectSystemCapsule, newSubjectSystemCapsule);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubjectSystemCapsule(ISATransparentCapsule newSubjectSystemCapsule) {
		if (newSubjectSystemCapsule != subjectSystemCapsule) {
			NotificationChain msgs = null;
			if (subjectSystemCapsule != null)
				msgs = ((InternalEObject)subjectSystemCapsule).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VisualizationPackage.ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE, null, msgs);
			if (newSubjectSystemCapsule != null)
				msgs = ((InternalEObject)newSubjectSystemCapsule).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VisualizationPackage.ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE, null, msgs);
			msgs = basicSetSubjectSystemCapsule(newSubjectSystemCapsule, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE, newSubjectSystemCapsule, newSubjectSystemCapsule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ISATransparentCapsule> getLibraryCapsules() {
		if (libraryCapsules == null) {
			libraryCapsules = new EObjectContainmentEList<ISATransparentCapsule>(ISATransparentCapsule.class, this, VisualizationPackage.ISA_VISUALIZATION_MODEL__LIBRARY_CAPSULES);
		}
		return libraryCapsules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelCreatorName() {
		return modelCreatorName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelCreatorName(String newModelCreatorName) {
		String oldModelCreatorName = modelCreatorName;
		modelCreatorName = newModelCreatorName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VisualizationPackage.ISA_VISUALIZATION_MODEL__MODEL_CREATOR_NAME, oldModelCreatorName, modelCreatorName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE:
				return basicSetSubjectSystemCapsule(null, msgs);
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__LIBRARY_CAPSULES:
				return ((InternalEList<?>)getLibraryCapsules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE:
				return getSubjectSystemCapsule();
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__LIBRARY_CAPSULES:
				return getLibraryCapsules();
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__MODEL_CREATOR_NAME:
				return getModelCreatorName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE:
				setSubjectSystemCapsule((ISATransparentCapsule)newValue);
				return;
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__LIBRARY_CAPSULES:
				getLibraryCapsules().clear();
				getLibraryCapsules().addAll((Collection<? extends ISATransparentCapsule>)newValue);
				return;
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__MODEL_CREATOR_NAME:
				setModelCreatorName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE:
				setSubjectSystemCapsule((ISATransparentCapsule)null);
				return;
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__LIBRARY_CAPSULES:
				getLibraryCapsules().clear();
				return;
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__MODEL_CREATOR_NAME:
				setModelCreatorName(MODEL_CREATOR_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__SUBJECT_SYSTEM_CAPSULE:
				return subjectSystemCapsule != null;
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__LIBRARY_CAPSULES:
				return libraryCapsules != null && !libraryCapsules.isEmpty();
			case VisualizationPackage.ISA_VISUALIZATION_MODEL__MODEL_CREATOR_NAME:
				return MODEL_CREATOR_NAME_EDEFAULT == null ? modelCreatorName != null : !MODEL_CREATOR_NAME_EDEFAULT.equals(modelCreatorName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (modelCreatorName: ");
		result.append(modelCreatorName);
		result.append(')');
		return result.toString();
	}

} //ISAVisualizationModelImpl

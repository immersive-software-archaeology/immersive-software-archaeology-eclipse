package dk.itu.cs.isa.multiplayer.http;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import dk.itu.cs.isa.coalescence.server.extensions.ExtensionPointHandler;
import dk.itu.cs.isa.coalescence.server.extensions.ISAAbstractSocketImplementation;
import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.multiplayer.udp.SynchronizationUdpServer;
import dk.itu.cs.isa.storage.ISAModelStorage;
import dk.itu.cs.isa.storage.ModelStorageEntry;

public class ServerStatusRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
    	response.setContentType("application/json; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
		PrintWriter out = response.getWriter();
		out.print(getSocketInfoAsJSON());
		out.close();
		
		baseRequest.setHandled(true);
	}
	
	private String getSocketInfoAsJSON() {
		SynchronizationUdpServer syncSocket = null;
		for(ISAAbstractSocketImplementation currentSocket : ExtensionPointHandler.INSTANCE.getSockets())
			if(currentSocket instanceof SynchronizationUdpServer)
				syncSocket = (SynchronizationUdpServer) currentSocket;

		JSONArray systemsArray = new JSONArray();
		if(syncSocket != null) {
			for(ModelStorageEntry modelStorageEntry : ISAModelStorage.INSTANCE.getAllEntries()) {
				JSONArray clientsArray = new JSONArray();
				for(int i=0; i<syncSocket.getNumberOfConnectedClients(modelStorageEntry.getSystemName()); i++) {
					JSONObject clientObject = new JSONObject();
					clientObject.put("id", syncSocket.getClientId(modelStorageEntry.getSystemName(), i));
					clientObject.put("color", syncSocket.getClientColor(modelStorageEntry.getSystemName(), i));
					clientsArray.put(clientObject);
				}

				JSONObject systemEntryObject = new JSONObject();
				systemEntryObject.put("systemName", modelStorageEntry.getSystemName());
				systemEntryObject.put("connectedClients", clientsArray);
				
				systemsArray.put(systemEntryObject);
			}
		}

		JSONObject responseObject = new JSONObject();
		responseObject.put("maxClients", SynchronizationUdpServer.maxClients);
		responseObject.put("entries", systemsArray);
    	return responseObject.toString(2);
    }
	
}

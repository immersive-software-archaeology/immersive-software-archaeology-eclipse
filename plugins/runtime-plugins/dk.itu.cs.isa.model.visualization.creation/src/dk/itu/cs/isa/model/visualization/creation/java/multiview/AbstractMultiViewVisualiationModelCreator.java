package dk.itu.cs.isa.model.visualization.creation.java.multiview;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import dk.itu.cs.isa.analyzer.interfaces.IVisualiationModelCreator;
import dk.itu.cs.isa.events.ISAEventSystem;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAEnum;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;
import dk.itu.cs.isa.model.software.structure.classifiers.ISARecord;
import dk.itu.cs.isa.model.software.structure.libraries.ISALibrary;
import dk.itu.cs.isa.model.software.structure.members.ISAConcreteMethod;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.misc.ISAModifier;
import dk.itu.cs.isa.model.software.structure.misc.ISAParameter;
import dk.itu.cs.isa.model.software.structure.misc.ISAStatementContainer;
import dk.itu.cs.isa.model.visualization.ISASpaceStation;
import dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna;
import dk.itu.cs.isa.model.visualization.ISASpaceStationBlock;
import dk.itu.cs.isa.model.visualization.ISASpaceStationType;
import dk.itu.cs.isa.model.visualization.ISATransparentCapsule;
import dk.itu.cs.isa.model.visualization.VisualizationFactory;

public abstract class AbstractMultiViewVisualiationModelCreator implements IVisualiationModelCreator {

	protected VisualizationFactory factory = VisualizationFactory.eINSTANCE;
	
	protected MultiViewModelReferenceInitializer referenceInitializer;
	
	
	
	@Override
	public void initializeInstance() {
		
	}
	
	@Override
	public void setupBeforeRun() {
		referenceInitializer = new MultiViewModelReferenceInitializer();
	}
	
	@Override
	public void cleanUpAfterRun() {
		referenceInitializer = null;
	}
	
	

	protected List<ISATransparentCapsule> createLibraryCapsules(List<ISALibrary> libs) {
		List<ISATransparentCapsule> capsules = new ArrayList<>(libs.size());
		for(ISALibrary lib : libs) {
			ISATransparentCapsule libCapsule = VisualizationFactory.eINSTANCE.createISATransparentCapsule();
			libCapsule.setName(lib.getName());
			
			for(ISAClassifier classifier : lib.getClassifiers()) {
				libCapsule.getSpaceStations().add(createSpaceStation(classifier));
			}
			capsules.add(libCapsule);
		}
		return capsules;
	}
	


	protected ISASpaceStation createSpaceStation(ISAClassifier classifier) {
		ISASpaceStation station = factory.createISASpaceStation();
		referenceInitializer.classifierToSpaceStationMap.put(classifier, station);
		
		station.setName(classifier.getName());
		station.setQualifiedName(classifier.getFullyQualifiedName());

		if(classifier instanceof ISAClass)
			station.setType(ISASpaceStationType.CLASS);
		else if(classifier instanceof ISAInterface)
			station.setType(ISASpaceStationType.INTERFACE);
		else if(classifier instanceof ISAEnum)
			station.setType(ISASpaceStationType.ENUM);
		else if(classifier instanceof ISARecord)
			station.setType(ISASpaceStationType.RECORD);
		else
			throw new RuntimeException("Classifier " + classifier.getName() + " is of unknown type: " + classifier.getClass().getName());
		
		station.setPublic(classifier.getModifiers().contains(ISAModifier.PUBLIC));
		
		for(ISAClassifier nestedClassifier : classifier.getClassifiers()) {
			ISASpaceStation nestedStation = createSpaceStation(nestedClassifier);
			station.getSatelliteStations().add(nestedStation);
		}

		for(ISAMember member : getMethodsAndConstructorsInDeclaredOrder(classifier)) {
			ISASpaceStationBlock block = createBlock(member);
			station.getBlocks().add(block);
		}
		
		for(ISAField field : classifier.getFields()) {
			station.getAntennas().add(createAttributeAntenna(field));
		}
		
		if(classifier instanceof ISAEnum) {
			for(ISAEnumConstant constant : ((ISAEnum) classifier).getConstants()) {
				station.getAntennas().add(createAttributeAntenna(constant));
			}
		}
		
		if(classifier instanceof ISARecord) {
			for(ISARecordComponent component : ((ISARecord) classifier).getComponents()) {
				station.getAntennas().add(createAttributeAntenna(component));
			}
		}
		
		return station;
	}
	
	
	
	protected ISASpaceStationAntenna createAttributeAntenna(ISAMember fieldOrConstant) {
		ISASpaceStationAntenna newAntenna = factory.createISASpaceStationAntenna();
		referenceInitializer.attributeToAntennaMap.put(fieldOrConstant, newAntenna);
		
		if(fieldOrConstant instanceof ISAField)
			newAntenna.setEnumConstant(false);
		else if(fieldOrConstant instanceof ISAEnumConstant)
			newAntenna.setEnumConstant(true);
		else if(fieldOrConstant instanceof ISARecordComponent)
			newAntenna.setRecordComponent(true);
		else
			throw new RuntimeException("Antenna Source " + fieldOrConstant.getName() + " is of unknown type: " + fieldOrConstant.getClass().getName());
		
		newAntenna.setName(fieldOrConstant.getName());
		newAntenna.setQualifiedName(fieldOrConstant.getFullyQualifiedName());
		newAntenna.setPublic(fieldOrConstant.getModifiers().contains(ISAModifier.PUBLIC));
		
		return newAntenna;
	}
	
	

	protected List<ISAMember> getMethodsAndConstructorsInDeclaredOrder(ISAClassifier classifier) {
		Map<Integer, List<ISAMember>> indexToMemberMap = new TreeMap<>();
		if(classifier instanceof ISAClass) {
			for(ISAConstructor constructor : ((ISAClass) classifier).getConstructors()) {
				extendList(indexToMemberMap, constructor, constructor.getNumberInOrderOfDeclaration());
			}
		}
		if(classifier instanceof ISAEnum) {
			for(ISAConstructor constructor : ((ISAEnum) classifier).getConstructors()) {
				extendList(indexToMemberMap, constructor, constructor.getNumberInOrderOfDeclaration());
			}
		}
		for(ISAMethod method : classifier.getMethods()) {
			extendList(indexToMemberMap, method, method.getNumberInOrderOfDeclaration());
		}
		
		List<ISAMember> result = new ArrayList<>();
		for(List<ISAMember> entriesOnIndex : indexToMemberMap.values()) {
			result.addAll(entriesOnIndex);
		}
		return result;
	}
	
	private <T> void extendList(Map<Integer, List<T>> listToExtend, T block, int index) {
		List<T> existingEntry = listToExtend.get(index);
		if(existingEntry == null) {
			existingEntry = new ArrayList<>();
			listToExtend.put(index, existingEntry);
		}
		existingEntry.add(block);
	}

	protected ISASpaceStationBlock createBlock(ISAMember member) {
		ISASpaceStationBlock block = factory.createISASpaceStationBlock();
		referenceInitializer.functionToBlockMap.put(member, block);
		
		block.setName(member.getName());
		block.setQualifiedName(member.getFullyQualifiedName());
		
		if(member instanceof ISAStatementContainer) {
			block.setAbstract(false);
			block.setHeight(0.2f + (float) ((ISAStatementContainer) member).getNumberOfExpressions() / 100f);
			block.setDiameter(2f + (float) Math.sqrt(((ISAStatementContainer) member).getCognitiveComplexity()));
		}
		else {
			block.setAbstract(true);
			block.setHeight(1f);
			block.setDiameter(2f);
		}
		
		block.setConstructor(member instanceof ISAConstructor);
		block.setPublic(member.getModifiers().contains(ISAModifier.PUBLIC));
		
		// main method's name must be "main", spelled in non-capital letters
		// https://docs.oracle.com/javase/specs/jls/se8/html/jls-12.html
		if(member instanceof ISAConcreteMethod) {
			if(member.getName().equals("main")) {
				if(member.getModifiers().contains(ISAModifier.PUBLIC)) {
					if(member.getModifiers().contains(ISAModifier.STATIC)) {
						List<ISAParameter> parameters = ((ISAConcreteMethod) member).getParameters();
						if(parameters.size() == 1 && parameters.get(0).getType().getFullyQualifiedName().equals("java.lang.String")) {
							 // TODO check that it is an array of strings
							block.setSystemEntry(true);
						}
						else {
							ISAEventSystem.fireStandardConsoleMessage("Did visualization model creation miss a main method? " + member.getFullyQualifiedName());
						}
					}
				}
			}
		}
		
		return block;
	}

}















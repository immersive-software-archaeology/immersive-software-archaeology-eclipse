package dk.itu.cs.isa.model.visualization.creation.java.multiview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import dk.itu.cs.isa.model.software.structure.classifiers.ISAClass;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAInterface;
import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMember;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;
import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferenceableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.visualization.ISAAbstractSpaceStationComponent;
import dk.itu.cs.isa.model.visualization.ISAAntennaRelation;
import dk.itu.cs.isa.model.visualization.ISABlockRelation;
import dk.itu.cs.isa.model.visualization.ISASpaceStation;
import dk.itu.cs.isa.model.visualization.ISASpaceStationAntenna;
import dk.itu.cs.isa.model.visualization.ISASpaceStationBlock;
import dk.itu.cs.isa.model.visualization.ISASpaceStationRelation;
import dk.itu.cs.isa.model.visualization.VisualizationFactory;

public class MultiViewModelReferenceInitializer {

	private VisualizationFactory factory = VisualizationFactory.eINSTANCE;

	public Map<ISAClassifier, ISASpaceStation> classifierToSpaceStationMap = new HashMap<>();
	public Map<ISAMember, ISASpaceStationBlock> functionToBlockMap = new HashMap<>();
	public Map<ISAMember, ISASpaceStationAntenna> attributeToAntennaMap = new HashMap<>();
	
	

	public void initializeRelations() {
		// Set inheritance relations of system space stations
		for(ISAClassifier classifier : classifierToSpaceStationMap.keySet()) {
			ISASpaceStation thisStation = classifierToSpaceStationMap.get(classifier);
			
			if(classifier instanceof ISAClass) {
				ISAClass extendedClass = ((ISAClass) classifier).getExtendedClass();
				if(extendedClass != null) {
					extendSpaceStationRelationList(thisStation.getInheritanceRelations(), extendedClass, 1);
				}
				
				for(ISAInterface implementedInterface : ((ISAClass) classifier).getImplementedInterfaces()) {
					extendSpaceStationRelationList(thisStation.getInheritanceRelations(), implementedInterface, 1);
				}
			}
			else if(classifier instanceof ISAInterface) {
				for(ISAInterface extendedInterface : ((ISAInterface) classifier).getExtendedInterfaces()) {
					extendSpaceStationRelationList(thisStation.getInheritanceRelations(), extendedInterface, 1);
				}
			}
		}
		
		List<ISAMember> allMembers = new ArrayList<>();
		allMembers.addAll(attributeToAntennaMap.keySet());
		allMembers.addAll(functionToBlockMap.keySet());

		// Set relations of station blocks and antennas
		for(ISAMember member : allMembers) {
			if(!(member instanceof ISAReferencingElement))
				continue;
			
			ISAAbstractSpaceStationComponent visualElementForMember = functionToBlockMap.get(member);
			if(visualElementForMember == null)
				visualElementForMember = attributeToAntennaMap.get(member);
			if(visualElementForMember == null)
				throw new RuntimeException("Could not find antenna or block for: " + member.getFullyQualifiedName());
			
			for(ISAReference<ISAType> reference : ((ISAReferencingElement) member).getTypeReferences())
				extendSpaceStationRelationList(visualElementForMember.getRelationsToSpaceStationCenters(), reference.getTarget(), reference.getWeight());

			for(ISAReference<ISARecordComponent> reference : ((ISAReferencingElement) member).getComponentAccesses())
				extendAntennaRelationList(visualElementForMember.getRelationsToAntennas(), reference.getTarget(), reference.getWeight());
			for(ISAReference<ISAEnumConstant> reference : ((ISAReferencingElement) member).getConstantAccesses())
				extendAntennaRelationList(visualElementForMember.getRelationsToAntennas(), reference.getTarget(), reference.getWeight());
			for(ISAReference<ISAField> reference : ((ISAReferencingElement) member).getFieldAccesses())
				extendAntennaRelationList(visualElementForMember.getRelationsToAntennas(), reference.getTarget(), reference.getWeight());
			
			for(ISAReference<ISAConstructor> reference : ((ISAReferencingElement) member).getConstructorCalls())
				extendBlockRelationList(visualElementForMember.getRelationsToBlocks(), reference.getTarget(), reference.getWeight());
			for(ISAReference<ISAMethod> reference : ((ISAReferencingElement) member).getMethodCalls())
				extendBlockRelationList(visualElementForMember.getRelationsToBlocks(), reference.getTarget(), reference.getWeight());
		}
	}
	


	private void extendSpaceStationRelationList(EList<ISASpaceStationRelation> existingRelations, ISAReferenceableElement referencedConstantOrField, int weight) {
		if(referencedConstantOrField == null)
			throw new NullPointerException("Reference has no target");

		ISASpaceStation referencedStation = classifierToSpaceStationMap.get(referencedConstantOrField);
		if(referencedStation == null)
			throw new NullPointerException("Could not find a space station for: " + referencedConstantOrField);
		
		ISASpaceStationRelation newRelation = null;
		for(ISASpaceStationRelation existingRelation : existingRelations) {
			if(existingRelation.getTarget().equals(referencedStation)) {
				newRelation = existingRelation;
				break;
			}
		}
		
		if(newRelation == null) {
			newRelation = factory.createISASpaceStationRelation();
			newRelation.setTarget(referencedStation);
			existingRelations.add(newRelation);
		}
		
		newRelation.setWeight(newRelation.getWeight() + weight);
	}
	
	private void extendBlockRelationList(EList<ISABlockRelation> existingRelations, ISAReferenceableElement referencedConstructorOrMethod, int weight) {
		if(referencedConstructorOrMethod == null)
			throw new NullPointerException("Reference has no target");

		ISASpaceStationBlock referencedBlock = functionToBlockMap.get(referencedConstructorOrMethod);
		if(referencedBlock == null)
			throw new NullPointerException("Could not find an antenna for: " + referencedConstructorOrMethod);
		
		ISABlockRelation newRelation = null;
		for(ISABlockRelation existingRelation : existingRelations) {
			if(existingRelation.getTarget().equals(referencedBlock)) {
				newRelation = existingRelation;
				break;
			}
		}
		
		if(newRelation == null) {
			newRelation = factory.createISABlockRelation();
			newRelation.setTarget(referencedBlock);
			existingRelations.add(newRelation);
		}
	
		newRelation.setWeight(newRelation.getWeight() + weight);
	}

	private void extendAntennaRelationList(EList<ISAAntennaRelation> existingRelations, ISAReferenceableElement referencedConstantOrField, int weight) {
		if(referencedConstantOrField == null)
			throw new NullPointerException("Reference has no target");

		ISASpaceStationAntenna referencedAntenna = attributeToAntennaMap.get(referencedConstantOrField);
		if(referencedAntenna == null)
			throw new NullPointerException("Could not find an antenna for: " + referencedConstantOrField);
		
		ISAAntennaRelation newRelation = null;
		for(ISAAntennaRelation existingRelation : existingRelations) {
			if(existingRelation.getTarget().equals(referencedAntenna)) {
				newRelation = existingRelation;
				break;
			}
		}
		
		if(newRelation == null) {
			newRelation = factory.createISAAntennaRelation();
			newRelation.setTarget(referencedAntenna);
			existingRelations.add(newRelation);
		}
	
		newRelation.setWeight(newRelation.getWeight() + weight);
	}

}

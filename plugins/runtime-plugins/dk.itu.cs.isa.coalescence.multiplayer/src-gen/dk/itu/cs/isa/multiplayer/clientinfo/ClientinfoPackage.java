/**
 */
package dk.itu.cs.isa.multiplayer.clientinfo;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoFactory
 * @model kind="package"
 * @generated
 */
public interface ClientinfoPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "clientinfo";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://itu.dk/isa/multiplayer/clientinfo";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "clientinfo";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClientinfoPackage eINSTANCE = dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl.init();

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl <em>Client Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getClientInfo()
	 * @generated
	 */
	int CLIENT_INFO = 0;

	/**
	 * The feature id for the '<em><b>System Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__SYSTEM_NAME = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__ID = 1;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__COLOR = 2;

	/**
	 * The feature id for the '<em><b>Head</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__HEAD = 3;

	/**
	 * The feature id for the '<em><b>Left Hand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__LEFT_HAND = 4;

	/**
	 * The feature id for the '<em><b>Right Hand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__RIGHT_HAND = 5;

	/**
	 * The feature id for the '<em><b>Fps</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__FPS = 6;

	/**
	 * The feature id for the '<em><b>Ping</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__PING = 7;

	/**
	 * The feature id for the '<em><b>Connect Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__CONNECT_TIMESTAMP = 8;

	/**
	 * The feature id for the '<em><b>Last Server Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__LAST_SERVER_TIMESTAMP = 9;

	/**
	 * The feature id for the '<em><b>Last Client Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO__LAST_CLIENT_TIMESTAMP = 10;

	/**
	 * The number of structural features of the '<em>Client Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Client Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.BodyPartInfoImpl <em>Body Part Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.BodyPartInfoImpl
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getBodyPartInfo()
	 * @generated
	 */
	int BODY_PART_INFO = 1;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_PART_INFO__POSITION = 0;

	/**
	 * The feature id for the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_PART_INFO__ROTATION = 1;

	/**
	 * The number of structural features of the '<em>Body Part Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_PART_INFO_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Body Part Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_PART_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.HeadInfoImpl <em>Head Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.HeadInfoImpl
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getHeadInfo()
	 * @generated
	 */
	int HEAD_INFO = 2;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_INFO__POSITION = BODY_PART_INFO__POSITION;

	/**
	 * The feature id for the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_INFO__ROTATION = BODY_PART_INFO__ROTATION;

	/**
	 * The number of structural features of the '<em>Head Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_INFO_FEATURE_COUNT = BODY_PART_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Head Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEAD_INFO_OPERATION_COUNT = BODY_PART_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.HandInfoImpl <em>Hand Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.HandInfoImpl
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getHandInfo()
	 * @generated
	 */
	int HAND_INFO = 3;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_INFO__POSITION = BODY_PART_INFO__POSITION;

	/**
	 * The feature id for the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_INFO__ROTATION = BODY_PART_INFO__ROTATION;

	/**
	 * The feature id for the '<em><b>Index Finger Tip Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_INFO__INDEX_FINGER_TIP_POSITION = BODY_PART_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pose</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_INFO__POSE = BODY_PART_INFO_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Held Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_INFO__HELD_OBJECT = BODY_PART_INFO_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hand Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_INFO_FEATURE_COUNT = BODY_PART_INFO_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Hand Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_INFO_OPERATION_COUNT = BODY_PART_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.HandHeldObjectInfoImpl <em>Hand Held Object Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.HandHeldObjectInfoImpl
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getHandHeldObjectInfo()
	 * @generated
	 */
	int HAND_HELD_OBJECT_INFO = 4;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_HELD_OBJECT_INFO__POSITION = BODY_PART_INFO__POSITION;

	/**
	 * The feature id for the '<em><b>Rotation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_HELD_OBJECT_INFO__ROTATION = BODY_PART_INFO__ROTATION;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_HELD_OBJECT_INFO__ID = BODY_PART_INFO_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Hand Held Object Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_HELD_OBJECT_INFO_FEATURE_COUNT = BODY_PART_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Hand Held Object Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HAND_HELD_OBJECT_INFO_OPERATION_COUNT = BODY_PART_INFO_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ISAVector3Impl <em>ISA Vector3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ISAVector3Impl
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getISAVector3()
	 * @generated
	 */
	int ISA_VECTOR3 = 5;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__Y = 1;

	/**
	 * The feature id for the '<em><b>Z</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3__Z = 2;

	/**
	 * The number of structural features of the '<em>ISA Vector3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>ISA Vector3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISA_VECTOR3_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandPose <em>Hand Pose</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandPose
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getHandPose()
	 * @generated
	 */
	int HAND_POSE = 6;


	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo <em>Client Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Client Info</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo
	 * @generated
	 */
	EClass getClientInfo();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getSystemName <em>System Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Name</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getSystemName()
	 * @see #getClientInfo()
	 * @generated
	 */
	EAttribute getClientInfo_SystemName();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getId()
	 * @see #getClientInfo()
	 * @generated
	 */
	EAttribute getClientInfo_Id();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getColor()
	 * @see #getClientInfo()
	 * @generated
	 */
	EAttribute getClientInfo_Color();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getHead <em>Head</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Head</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getHead()
	 * @see #getClientInfo()
	 * @generated
	 */
	EReference getClientInfo_Head();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLeftHand <em>Left Hand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Hand</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLeftHand()
	 * @see #getClientInfo()
	 * @generated
	 */
	EReference getClientInfo_LeftHand();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getRightHand <em>Right Hand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Hand</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getRightHand()
	 * @see #getClientInfo()
	 * @generated
	 */
	EReference getClientInfo_RightHand();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getFps <em>Fps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fps</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getFps()
	 * @see #getClientInfo()
	 * @generated
	 */
	EAttribute getClientInfo_Fps();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getPing <em>Ping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ping</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getPing()
	 * @see #getClientInfo()
	 * @generated
	 */
	EAttribute getClientInfo_Ping();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getConnectTimestamp <em>Connect Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Connect Timestamp</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getConnectTimestamp()
	 * @see #getClientInfo()
	 * @generated
	 */
	EAttribute getClientInfo_ConnectTimestamp();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLastServerTimestamp <em>Last Server Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Server Timestamp</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLastServerTimestamp()
	 * @see #getClientInfo()
	 * @generated
	 */
	EAttribute getClientInfo_LastServerTimestamp();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLastClientTimestamp <em>Last Client Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Client Timestamp</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ClientInfo#getLastClientTimestamp()
	 * @see #getClientInfo()
	 * @generated
	 */
	EAttribute getClientInfo_LastClientTimestamp();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo <em>Body Part Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Body Part Info</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo
	 * @generated
	 */
	EClass getBodyPartInfo();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Position</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo#getPosition()
	 * @see #getBodyPartInfo()
	 * @generated
	 */
	EReference getBodyPartInfo_Position();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo#getRotation <em>Rotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rotation</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.BodyPartInfo#getRotation()
	 * @see #getBodyPartInfo()
	 * @generated
	 */
	EReference getBodyPartInfo_Rotation();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.clientinfo.HeadInfo <em>Head Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Head Info</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HeadInfo
	 * @generated
	 */
	EClass getHeadInfo();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo <em>Hand Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hand Info</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandInfo
	 * @generated
	 */
	EClass getHandInfo();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getIndexFingerTipPosition <em>Index Finger Tip Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Index Finger Tip Position</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getIndexFingerTipPosition()
	 * @see #getHandInfo()
	 * @generated
	 */
	EReference getHandInfo_IndexFingerTipPosition();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getPose <em>Pose</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pose</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getPose()
	 * @see #getHandInfo()
	 * @generated
	 */
	EAttribute getHandInfo_Pose();

	/**
	 * Returns the meta object for the containment reference '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getHeldObject <em>Held Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Held Object</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandInfo#getHeldObject()
	 * @see #getHandInfo()
	 * @generated
	 */
	EReference getHandInfo_HeldObject();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo <em>Hand Held Object Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hand Held Object Info</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo
	 * @generated
	 */
	EClass getHandHeldObjectInfo();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandHeldObjectInfo#getId()
	 * @see #getHandHeldObjectInfo()
	 * @generated
	 */
	EAttribute getHandHeldObjectInfo_Id();

	/**
	 * Returns the meta object for class '{@link dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3 <em>ISA Vector3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISA Vector3</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3
	 * @generated
	 */
	EClass getISAVector3();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3#getX()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_X();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3#getY()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_Y();

	/**
	 * Returns the meta object for the attribute '{@link dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3#getZ <em>Z</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Z</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3#getZ()
	 * @see #getISAVector3()
	 * @generated
	 */
	EAttribute getISAVector3_Z();

	/**
	 * Returns the meta object for enum '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandPose <em>Hand Pose</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Hand Pose</em>'.
	 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandPose
	 * @generated
	 */
	EEnum getHandPose();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ClientinfoFactory getClientinfoFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl <em>Client Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientInfoImpl
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getClientInfo()
		 * @generated
		 */
		EClass CLIENT_INFO = eINSTANCE.getClientInfo();

		/**
		 * The meta object literal for the '<em><b>System Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT_INFO__SYSTEM_NAME = eINSTANCE.getClientInfo_SystemName();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT_INFO__ID = eINSTANCE.getClientInfo_Id();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT_INFO__COLOR = eINSTANCE.getClientInfo_Color();

		/**
		 * The meta object literal for the '<em><b>Head</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLIENT_INFO__HEAD = eINSTANCE.getClientInfo_Head();

		/**
		 * The meta object literal for the '<em><b>Left Hand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLIENT_INFO__LEFT_HAND = eINSTANCE.getClientInfo_LeftHand();

		/**
		 * The meta object literal for the '<em><b>Right Hand</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLIENT_INFO__RIGHT_HAND = eINSTANCE.getClientInfo_RightHand();

		/**
		 * The meta object literal for the '<em><b>Fps</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT_INFO__FPS = eINSTANCE.getClientInfo_Fps();

		/**
		 * The meta object literal for the '<em><b>Ping</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT_INFO__PING = eINSTANCE.getClientInfo_Ping();

		/**
		 * The meta object literal for the '<em><b>Connect Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT_INFO__CONNECT_TIMESTAMP = eINSTANCE.getClientInfo_ConnectTimestamp();

		/**
		 * The meta object literal for the '<em><b>Last Server Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT_INFO__LAST_SERVER_TIMESTAMP = eINSTANCE.getClientInfo_LastServerTimestamp();

		/**
		 * The meta object literal for the '<em><b>Last Client Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT_INFO__LAST_CLIENT_TIMESTAMP = eINSTANCE.getClientInfo_LastClientTimestamp();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.BodyPartInfoImpl <em>Body Part Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.BodyPartInfoImpl
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getBodyPartInfo()
		 * @generated
		 */
		EClass BODY_PART_INFO = eINSTANCE.getBodyPartInfo();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BODY_PART_INFO__POSITION = eINSTANCE.getBodyPartInfo_Position();

		/**
		 * The meta object literal for the '<em><b>Rotation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BODY_PART_INFO__ROTATION = eINSTANCE.getBodyPartInfo_Rotation();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.HeadInfoImpl <em>Head Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.HeadInfoImpl
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getHeadInfo()
		 * @generated
		 */
		EClass HEAD_INFO = eINSTANCE.getHeadInfo();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.HandInfoImpl <em>Hand Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.HandInfoImpl
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getHandInfo()
		 * @generated
		 */
		EClass HAND_INFO = eINSTANCE.getHandInfo();

		/**
		 * The meta object literal for the '<em><b>Index Finger Tip Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAND_INFO__INDEX_FINGER_TIP_POSITION = eINSTANCE.getHandInfo_IndexFingerTipPosition();

		/**
		 * The meta object literal for the '<em><b>Pose</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAND_INFO__POSE = eINSTANCE.getHandInfo_Pose();

		/**
		 * The meta object literal for the '<em><b>Held Object</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HAND_INFO__HELD_OBJECT = eINSTANCE.getHandInfo_HeldObject();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.HandHeldObjectInfoImpl <em>Hand Held Object Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.HandHeldObjectInfoImpl
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getHandHeldObjectInfo()
		 * @generated
		 */
		EClass HAND_HELD_OBJECT_INFO = eINSTANCE.getHandHeldObjectInfo();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HAND_HELD_OBJECT_INFO__ID = eINSTANCE.getHandHeldObjectInfo_Id();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.impl.ISAVector3Impl <em>ISA Vector3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ISAVector3Impl
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getISAVector3()
		 * @generated
		 */
		EClass ISA_VECTOR3 = eINSTANCE.getISAVector3();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__X = eINSTANCE.getISAVector3_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__Y = eINSTANCE.getISAVector3_Y();

		/**
		 * The meta object literal for the '<em><b>Z</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ISA_VECTOR3__Z = eINSTANCE.getISAVector3_Z();

		/**
		 * The meta object literal for the '{@link dk.itu.cs.isa.multiplayer.clientinfo.HandPose <em>Hand Pose</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.HandPose
		 * @see dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl#getHandPose()
		 * @generated
		 */
		EEnum HAND_POSE = eINSTANCE.getHandPose();

	}

} //ClientinfoPackage

/**
 */
package dk.itu.cs.isa.events.events;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Console Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.events.events.ConsoleEvent#getTimestamp <em>Timestamp</em>}</li>
 * </ul>
 *
 * @see dk.itu.cs.isa.events.events.EventsPackage#getConsoleEvent()
 * @model
 * @generated
 */
public interface ConsoleEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(Date)
	 * @see dk.itu.cs.isa.events.events.EventsPackage#getConsoleEvent_Timestamp()
	 * @model required="true"
	 * @generated
	 */
	Date getTimestamp();

	/**
	 * Sets the value of the '{@link dk.itu.cs.isa.events.events.ConsoleEvent#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(Date value);

} // ConsoleEvent

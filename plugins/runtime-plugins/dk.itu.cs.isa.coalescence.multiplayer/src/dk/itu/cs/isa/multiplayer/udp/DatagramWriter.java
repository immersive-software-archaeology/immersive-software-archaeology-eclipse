package dk.itu.cs.isa.multiplayer.udp;

import java.nio.ByteBuffer;

import dk.itu.cs.isa.multiplayer.clientinfo.HandPose;
import dk.itu.cs.isa.multiplayer.clientinfo.ISAVector3;

public class DatagramWriter {

    private final ByteBuffer longBuffer = ByteBuffer.allocate(Long.BYTES);
	private final ByteBuffer intBuffer = ByteBuffer.allocate(Integer.BYTES);
    private final ByteBuffer floatBuffer = ByteBuffer.allocate(Float.BYTES);

    private final byte[] datagram;
//    private final int checksumLength;
    private int index;
	
	
	
	
	public DatagramWriter(int datagramLength) {
		this.datagram = new byte[datagramLength];
		
//		this.checksumLength = checksumLength;
//		this.index = checksumLength;
		
		this.index = 0;
	}
	
	public byte[] getDatagram() {
		return datagram;
	}



//	public void writeCheckSum() {
//		CRC32 crc = new CRC32();
//		crc.update(datagram, checksumLength, datagram.length - checksumLength);
//		long checkSum = crc.getValue();
//		
//		byte[] checkSumBytes = toBytes(checkSum);
//		for (int i = 0; i < checksumLength; i++) {
//			if(datagram[i] != 0)
//				throw new IllegalArgumentException("bytes for checksum were already written, aborting...");
//			datagram[i] = checkSumBytes[i+4];
//		}
//	}



	public void write(byte b) {
		datagram[index++] = b;
	}

	public void write(long l) {
		for(byte b : toBytes(l))
			write(b);
	}

	public void write(int i) {
		for(byte b : toBytes(i))
			write(b);
	}

	public void write(float f) {
		for(byte b : toBytes(f))
			write(b);
	}

	public void write(float[] fs) {
		for(float f : fs)
			for(byte b : toBytes(f))
				write(b);
	}

	public void write(String message) {
		for(byte b : message.getBytes())
			write(b);
	}

	public void write(ISAVector3 vector) {
		write(vector.getX());
		write(vector.getY());
		write(vector.getZ());
	}

	public void write(HandPose pose) {
		write((byte)pose.getValue());
	}
	
	
    
    private byte[] toBytes(long x) {
    	longBuffer.clear();
	    longBuffer.putLong(x);
	    return longBuffer.array();
	}
	
	private byte[] toBytes(int x) {
		intBuffer.clear();
	    intBuffer.putInt(x);
	    return intBuffer.array();
	}
	
	public byte[] toBytes(float x) {
    	floatBuffer.clear();
	    floatBuffer.putFloat(x);
	    return floatBuffer.array();
	}
	
	
	
	public String toString() {
		String result = "DatagramWriter: length = " + datagram.length;
		result += ", content=";
		for(byte b : datagram)
			result += " " + Integer.toBinaryString(b & 0xFF);
		return result;
	}
	
	
	

}

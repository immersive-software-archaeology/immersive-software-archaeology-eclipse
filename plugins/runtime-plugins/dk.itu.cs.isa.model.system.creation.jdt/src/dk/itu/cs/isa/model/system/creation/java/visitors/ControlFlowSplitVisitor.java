package dk.itu.cs.isa.model.system.creation.java.visitors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.WhileStatement;

public final class ControlFlowSplitVisitor extends ASTVisitor {

	private int splits = 0;
	
	

	public int getNumberOfControlFlowSplits() {
		return splits;
	}
	
	
	
	// EXPRESSIONS
	
	
	
	@Override
	public boolean visit(ConditionalExpression node) {
		splits++;
		return super.visit(node);
	}
	
	
	
	// STATEMENTS
	


	@Override
	public boolean visit(DoStatement node) {
		splits++;
		return super.visit(node);
	}
	
	@Override
	public boolean visit(EnhancedForStatement node) {
		splits++;
		return super.visit(node);
	}
	
	@Override
	public boolean visit(ForStatement node) {
		splits++;
		return super.visit(node);
	}
	
	@Override
	public boolean visit(IfStatement node) {
		splits++;
		if(node.getElseStatement() != null)
			splits++;
		return super.visit(node);
	}
	
	@Override
	public boolean visit(SwitchCase node) {
		splits++;
		return super.visit(node);
	}
	
	@Override
	public boolean visit(TryStatement node) {
		splits += node.catchClauses().size();
		return super.visit(node);
	}
	
	@Override
	public boolean visit(WhileStatement node) {
		splits++;
		return super.visit(node);
	}

}
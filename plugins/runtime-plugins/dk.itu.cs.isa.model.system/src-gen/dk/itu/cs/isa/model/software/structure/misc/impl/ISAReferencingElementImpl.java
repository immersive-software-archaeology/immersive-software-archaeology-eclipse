/**
 */
package dk.itu.cs.isa.model.software.structure.misc.impl;

import dk.itu.cs.isa.model.software.structure.members.ISAConstructor;
import dk.itu.cs.isa.model.software.structure.members.ISAEnumConstant;
import dk.itu.cs.isa.model.software.structure.members.ISAField;
import dk.itu.cs.isa.model.software.structure.members.ISAMethod;

import dk.itu.cs.isa.model.software.structure.members.ISARecordComponent;
import dk.itu.cs.isa.model.software.structure.misc.ISAReference;
import dk.itu.cs.isa.model.software.structure.misc.ISAReferencingElement;
import dk.itu.cs.isa.model.software.structure.misc.ISAType;
import dk.itu.cs.isa.model.software.structure.misc.MiscPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ISA Referencing Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl#getTypeReferences <em>Type References</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl#getConstructorCalls <em>Constructor Calls</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl#getMethodCalls <em>Method Calls</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl#getFieldAccesses <em>Field Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl#getConstantAccesses <em>Constant Accesses</em>}</li>
 *   <li>{@link dk.itu.cs.isa.model.software.structure.misc.impl.ISAReferencingElementImpl#getComponentAccesses <em>Component Accesses</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ISAReferencingElementImpl extends MinimalEObjectImpl.Container implements ISAReferencingElement {
	/**
	 * The cached value of the '{@link #getTypeReferences() <em>Type References</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeReferences()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAType>> typeReferences;

	/**
	 * The cached value of the '{@link #getConstructorCalls() <em>Constructor Calls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstructorCalls()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAConstructor>> constructorCalls;

	/**
	 * The cached value of the '{@link #getMethodCalls() <em>Method Calls</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethodCalls()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAMethod>> methodCalls;

	/**
	 * The cached value of the '{@link #getFieldAccesses() <em>Field Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFieldAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAField>> fieldAccesses;

	/**
	 * The cached value of the '{@link #getConstantAccesses() <em>Constant Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISAEnumConstant>> constantAccesses;

	/**
	 * The cached value of the '{@link #getComponentAccesses() <em>Component Accesses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentAccesses()
	 * @generated
	 * @ordered
	 */
	protected EList<ISAReference<ISARecordComponent>> componentAccesses;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ISAReferencingElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MiscPackage.Literals.ISA_REFERENCING_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAType>> getTypeReferences() {
		if (typeReferences == null) {
			typeReferences = new EObjectContainmentEList<ISAReference<ISAType>>(ISAReference.class, this, MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES);
		}
		return typeReferences;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAConstructor>> getConstructorCalls() {
		if (constructorCalls == null) {
			constructorCalls = new EObjectContainmentEList<ISAReference<ISAConstructor>>(ISAReference.class, this, MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS);
		}
		return constructorCalls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAMethod>> getMethodCalls() {
		if (methodCalls == null) {
			methodCalls = new EObjectContainmentEList<ISAReference<ISAMethod>>(ISAReference.class, this, MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS);
		}
		return methodCalls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAField>> getFieldAccesses() {
		if (fieldAccesses == null) {
			fieldAccesses = new EObjectContainmentEList<ISAReference<ISAField>>(ISAReference.class, this, MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES);
		}
		return fieldAccesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISAEnumConstant>> getConstantAccesses() {
		if (constantAccesses == null) {
			constantAccesses = new EObjectContainmentEList<ISAReference<ISAEnumConstant>>(ISAReference.class, this, MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES);
		}
		return constantAccesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISAReference<ISARecordComponent>> getComponentAccesses() {
		if (componentAccesses == null) {
			componentAccesses = new EObjectContainmentEList<ISAReference<ISARecordComponent>>(ISAReference.class, this, MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES);
		}
		return componentAccesses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES:
				return ((InternalEList<?>)getTypeReferences()).basicRemove(otherEnd, msgs);
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS:
				return ((InternalEList<?>)getConstructorCalls()).basicRemove(otherEnd, msgs);
			case MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS:
				return ((InternalEList<?>)getMethodCalls()).basicRemove(otherEnd, msgs);
			case MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES:
				return ((InternalEList<?>)getFieldAccesses()).basicRemove(otherEnd, msgs);
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES:
				return ((InternalEList<?>)getConstantAccesses()).basicRemove(otherEnd, msgs);
			case MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES:
				return ((InternalEList<?>)getComponentAccesses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES:
				return getTypeReferences();
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS:
				return getConstructorCalls();
			case MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS:
				return getMethodCalls();
			case MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES:
				return getFieldAccesses();
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES:
				return getConstantAccesses();
			case MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES:
				return getComponentAccesses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES:
				getTypeReferences().clear();
				getTypeReferences().addAll((Collection<? extends ISAReference<ISAType>>)newValue);
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS:
				getConstructorCalls().clear();
				getConstructorCalls().addAll((Collection<? extends ISAReference<ISAConstructor>>)newValue);
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS:
				getMethodCalls().clear();
				getMethodCalls().addAll((Collection<? extends ISAReference<ISAMethod>>)newValue);
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES:
				getFieldAccesses().clear();
				getFieldAccesses().addAll((Collection<? extends ISAReference<ISAField>>)newValue);
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES:
				getConstantAccesses().clear();
				getConstantAccesses().addAll((Collection<? extends ISAReference<ISAEnumConstant>>)newValue);
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES:
				getComponentAccesses().clear();
				getComponentAccesses().addAll((Collection<? extends ISAReference<ISARecordComponent>>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES:
				getTypeReferences().clear();
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS:
				getConstructorCalls().clear();
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS:
				getMethodCalls().clear();
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES:
				getFieldAccesses().clear();
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES:
				getConstantAccesses().clear();
				return;
			case MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES:
				getComponentAccesses().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MiscPackage.ISA_REFERENCING_ELEMENT__TYPE_REFERENCES:
				return typeReferences != null && !typeReferences.isEmpty();
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTRUCTOR_CALLS:
				return constructorCalls != null && !constructorCalls.isEmpty();
			case MiscPackage.ISA_REFERENCING_ELEMENT__METHOD_CALLS:
				return methodCalls != null && !methodCalls.isEmpty();
			case MiscPackage.ISA_REFERENCING_ELEMENT__FIELD_ACCESSES:
				return fieldAccesses != null && !fieldAccesses.isEmpty();
			case MiscPackage.ISA_REFERENCING_ELEMENT__CONSTANT_ACCESSES:
				return constantAccesses != null && !constantAccesses.isEmpty();
			case MiscPackage.ISA_REFERENCING_ELEMENT__COMPONENT_ACCESSES:
				return componentAccesses != null && !componentAccesses.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ISAReferencingElementImpl

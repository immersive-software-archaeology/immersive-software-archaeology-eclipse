package dk.itu.cs.isa.model.quality.sonarqube.processes.io;

public interface ISonarProcessOutputConsumer {
	
	public void startNewRun();
	
	public void processOutputLine(String line);
	
	public void processErrorLine(String line);

}

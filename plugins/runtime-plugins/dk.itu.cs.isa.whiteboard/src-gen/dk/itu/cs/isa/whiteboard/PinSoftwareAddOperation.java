/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pin Software Add Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinSoftwareAddOperation()
 * @model
 * @generated
 */
public interface PinSoftwareAddOperation extends AbstractPinOperation {
} // PinSoftwareAddOperation

/**
 */
package dk.itu.cs.isa.model.software.structure.members;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Enum Constant</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAEnumConstant()
 * @model
 * @generated
 */
public interface ISAEnumConstant extends ISAMember {
} // ISAEnumConstant

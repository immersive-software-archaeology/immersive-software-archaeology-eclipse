package dk.itu.cs.isa.model.system.architecture.labeling;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.eclipse.emf.ecore.EObject;

import dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.architecture.ISAWordOccurrenceEntry;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;

public class ClusterLabelingUtil {

	private static final int INITIAL_WORDS_COUNT = 30;
	private static final int FINAL_WORDS_COUNT = 10;
	
	private static final String[] IGNORED_WORDS = new String[] {
			"get", "gets",
			"set", "sets",
			"is", "has", "can",

			"add", "adds",
			"remove", "removes",
			
			"do", "does",
			"perform", "performs",
			
			"to", "from",
			"in", "out",
			"with", "and", "or",
			
			"non", "not",
			"when", "if",
			"the"
	};
	
	

	public Map<String, Integer> label(ISACluster cluster) {
		Map<String, Integer> result = doLabel(cluster);
		//makeTopWordsUniqueAlongHierarchy(cluster, new LinkedList<>());
		return result;
	}
	
	public Map<String, Integer> doLabel(ISACluster cluster) {
		Map<String, Integer> wordOccurrences = new TreeMap<>();
		
		List<ISANamedSoftwareElement> allNamedChildren = new LinkedList<>();
		for(ISAClassifier classifier : cluster.getClassifiers()) {
			collectAllNamedChildren(allNamedChildren, classifier);
		}
		for(ISANamedSoftwareElement element : allNamedChildren) {
			addEntriesToOccurenceMap(wordOccurrences, deriveNameSegments(element));
		}
		
		for(ISACluster child : cluster.getChildClusters()) {
			Map<String, Integer> childWordOccurences = doLabel(child);
			addEntriesToOccurenceMap(wordOccurrences, childWordOccurences);
		}
		
		// remove double entries and irrelevant words ("is", "set", etc.)
		cleanUpWordOccurences(wordOccurrences);
		
		// Boost longer words
		for(Entry<String, Integer> entry : wordOccurrences.entrySet())
			entry.setValue(entry.getKey().length() * entry.getValue());
		
		// Sort words according to their occurrence
		List<Entry<String, Integer>> sortedWordOccurrences = new ArrayList<>(wordOccurrences.entrySet());
        sortedWordOccurrences.sort(Entry.comparingByValue());
		
        setWordOccurrences(cluster, sortedWordOccurrences);
		return wordOccurrences;
	}



	private void collectAllNamedChildren(List<ISANamedSoftwareElement> resultList, EObject currentElement) {
		if(currentElement instanceof ISANamedSoftwareElement)
			resultList.add((ISANamedSoftwareElement) currentElement);
		for(EObject child : currentElement.eContents())
			collectAllNamedChildren(resultList, child);
	}
	
	private String[] deriveNameSegments(ISANamedSoftwareElement element) {
		// Camel case split
		// https://stackoverflow.com/questions/7593969/regex-to-split-camelcase-or-titlecase-advanced
//		return element.getName().split("(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])");
		return element.getName().split("((?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z]))|_");
	}

	private void cleanUpWordOccurences(Map<String, Integer> wordOccurrences) {
		List<String> words = new ArrayList<>(wordOccurrences.keySet());
		for(int i=0; i<words.size(); i++) {
			String name = words.get(i);
			
			if(isIrrelevantWord(name)) {
				wordOccurrences.remove(name);
				words.remove(i--);
				continue;
			}
				
			for(int j=i+1; j<words.size(); j++) {
				String nameToCompareTo = words.get(j);
				if(areEquivalent(name, nameToCompareTo)) {
					// pull together
					wordOccurrences.put(name, wordOccurrences.get(name) + wordOccurrences.get(nameToCompareTo));
					wordOccurrences.remove(nameToCompareTo);
					words.remove(j);
				}
			}
		}
	}
	
	private boolean areEquivalent(ISAWordOccurrenceEntry entry1, ISAWordOccurrenceEntry entry2) {
		return areEquivalent(entry1.getWord(), entry2.getWord());
	}
	
	private boolean areEquivalent(String name1, String name2) {
		if(name1.equalsIgnoreCase(name2))
			return true;
		if(name1.equalsIgnoreCase(name2+"s"))
			return true;
		if(name2.equalsIgnoreCase(name1+"s"))
			return true;
		return false;
	}
	
	private boolean isIrrelevantWord(String word) {
		if(word.length() <= 2)
			return true;
		
		for(String ignoredWord : IGNORED_WORDS)
			if(ignoredWord.equalsIgnoreCase(word))
				return true;
		
		return false;
	}
	
	

	private void addEntriesToOccurenceMap(Map<String, Integer> resultMap, String[] newEntries) {
		for(String newEntry : newEntries) {
			Integer valueInResult = resultMap.get(newEntry);
			if(valueInResult == null) {
				// No such entry in result map yet, add it
				resultMap.put(newEntry, 1);
			}
			else {
				// Result map already contained an entry for key, add value together
				resultMap.put(newEntry, valueInResult + 1);
			}
		}
	}

	private void addEntriesToOccurenceMap(Map<String, Integer> resultMap, Map<String, Integer> mapToIntegrate) {
		for(String key : mapToIntegrate.keySet()) {
			Integer valueInResult = resultMap.get(key);
			if(valueInResult == null) {
				// No such entry in result map yet, add it
				resultMap.put(key, mapToIntegrate.get(key));
			}
			else {
				// Result map already contained an entry for key, add value together
				resultMap.put(key, valueInResult + mapToIntegrate.get(key));
			}
		}
	}



	private String setWordOccurrences(ISACluster cluster, List<Entry<String, Integer>> sortedWordOccurences) {
		cluster.getTopWordOccurrences().clear();
		List<String> segments = new ArrayList<>(INITIAL_WORDS_COUNT);
		for(int i = sortedWordOccurences.size()-1; i >= Math.max(0, sortedWordOccurences.size()-INITIAL_WORDS_COUNT); i--) {
			String name = sortedWordOccurences.get(i).getKey().toLowerCase();
//			name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
			
			ISAWordOccurrenceEntry newEntry = ArchitectureFactory.eINSTANCE.createISAWordOccurrenceEntry();
			newEntry.setWord(name);
			newEntry.setCount(sortedWordOccurences.get(i).getValue());
			cluster.getTopWordOccurrences().add(newEntry);
		}
		return String.join(", ", segments);
	}
	
	

	/**
	 * Remove all words that are top words of the parent element
	 * @param cluster
	 */
	@SuppressWarnings("unused")
	private void makeTopWordsUniqueAlongHierarchy(ISACluster cluster, List<ISAWordOccurrenceEntry> parentWords) {
		ISACluster parent = getParent(cluster);
		if(parent != null)
			parentWords.addAll(parent.getTopWordOccurrences());
		
		for(ISAWordOccurrenceEntry parentWord : parentWords) {
			for(ISAWordOccurrenceEntry thisWord : cluster.getTopWordOccurrences()) {
				if(areEquivalent(thisWord, parentWord)) {
					cluster.getTopWordOccurrences().remove(thisWord);
					break;
				}
			}
		}
		
		while(cluster.getTopWordOccurrences().size() > FINAL_WORDS_COUNT) {
			cluster.getTopWordOccurrences().remove(cluster.getTopWordOccurrences().size()-1);
		}
		
		parentWords.addAll(cluster.getTopWordOccurrences());
		for(ISACluster child : cluster.getChildClusters())
			makeTopWordsUniqueAlongHierarchy(child, new LinkedList<>(parentWords));
	}
	
	private ISACluster getParent(ISACluster c) {
		EObject parent = c.eContainer();
		if(parent != null && parent instanceof ISACluster)
			return (ISACluster) parent;
		return null;
	}

}












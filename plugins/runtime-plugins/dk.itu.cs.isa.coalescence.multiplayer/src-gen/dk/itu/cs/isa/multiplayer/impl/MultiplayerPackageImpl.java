/**
 */
package dk.itu.cs.isa.multiplayer.impl;

import dk.itu.cs.isa.multiplayer.AbstractEvent;
import dk.itu.cs.isa.multiplayer.AudioEvent;
import dk.itu.cs.isa.multiplayer.ChangeElementActiveStateEvent;
import dk.itu.cs.isa.multiplayer.ChangeNameplateVisibilityEvent;
import dk.itu.cs.isa.multiplayer.ClearAllRelationsEvent;
import dk.itu.cs.isa.multiplayer.ClearOneRelationEvent;
import dk.itu.cs.isa.multiplayer.ClearRelationsEvent;
import dk.itu.cs.isa.multiplayer.Direction;
import dk.itu.cs.isa.multiplayer.ElementType;
import dk.itu.cs.isa.multiplayer.EventLog;
import dk.itu.cs.isa.multiplayer.ISAVector3;
import dk.itu.cs.isa.multiplayer.MultiplayerFactory;
import dk.itu.cs.isa.multiplayer.MultiplayerPackage;
import dk.itu.cs.isa.multiplayer.RelationType;
import dk.itu.cs.isa.multiplayer.ShowRelationEvent;
import dk.itu.cs.isa.multiplayer.SphereOpenCloseEvent;
import dk.itu.cs.isa.multiplayer.SystemMoveEvent;

import dk.itu.cs.isa.multiplayer.clientinfo.ClientinfoPackage;

import dk.itu.cs.isa.multiplayer.clientinfo.impl.ClientinfoPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MultiplayerPackageImpl extends EPackageImpl implements MultiplayerPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventLogEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass showRelationEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clearOneRelationEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clearRelationsEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clearAllRelationsEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeElementActiveStateEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemMoveEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeNameplateVisibilityEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sphereOpenCloseEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass audioEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isaVector3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum relationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum directionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum elementTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dk.itu.cs.isa.multiplayer.MultiplayerPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MultiplayerPackageImpl() {
		super(eNS_URI, MultiplayerFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link MultiplayerPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MultiplayerPackage init() {
		if (isInited) return (MultiplayerPackage)EPackage.Registry.INSTANCE.getEPackage(MultiplayerPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredMultiplayerPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		MultiplayerPackageImpl theMultiplayerPackage = registeredMultiplayerPackage instanceof MultiplayerPackageImpl ? (MultiplayerPackageImpl)registeredMultiplayerPackage : new MultiplayerPackageImpl();

		isInited = true;

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ClientinfoPackage.eNS_URI);
		ClientinfoPackageImpl theClientinfoPackage = (ClientinfoPackageImpl)(registeredPackage instanceof ClientinfoPackageImpl ? registeredPackage : ClientinfoPackage.eINSTANCE);

		// Create package meta-data objects
		theMultiplayerPackage.createPackageContents();
		theClientinfoPackage.createPackageContents();

		// Initialize created meta-data
		theMultiplayerPackage.initializePackageContents();
		theClientinfoPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMultiplayerPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MultiplayerPackage.eNS_URI, theMultiplayerPackage);
		return theMultiplayerPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventLog() {
		return eventLogEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventLog_Log() {
		return (EReference)eventLogEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractEvent() {
		return abstractEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractEvent_Timestamp() {
		return (EAttribute)abstractEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractEvent_SystemName() {
		return (EAttribute)abstractEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractEvent_ClientId() {
		return (EAttribute)abstractEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShowRelationEvent() {
		return showRelationEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowRelationEvent_ElementName() {
		return (EAttribute)showRelationEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowRelationEvent_RelationType() {
		return (EAttribute)showRelationEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShowRelationEvent_Direction() {
		return (EAttribute)showRelationEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClearOneRelationEvent() {
		return clearOneRelationEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClearOneRelationEvent_ElementName() {
		return (EAttribute)clearOneRelationEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClearOneRelationEvent_RelationType() {
		return (EAttribute)clearOneRelationEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClearOneRelationEvent_Direction() {
		return (EAttribute)clearOneRelationEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClearRelationsEvent() {
		return clearRelationsEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClearRelationsEvent_ElementName() {
		return (EAttribute)clearRelationsEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClearAllRelationsEvent() {
		return clearAllRelationsEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeElementActiveStateEvent() {
		return changeElementActiveStateEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeElementActiveStateEvent_ElementType() {
		return (EAttribute)changeElementActiveStateEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeElementActiveStateEvent_ElementId() {
		return (EAttribute)changeElementActiveStateEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeElementActiveStateEvent_NewState() {
		return (EAttribute)changeElementActiveStateEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeElementActiveStateEvent_Position() {
		return (EReference)changeElementActiveStateEventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeElementActiveStateEvent_Rotation() {
		return (EReference)changeElementActiveStateEventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemMoveEvent() {
		return systemMoveEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemMoveEvent_Position() {
		return (EReference)systemMoveEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemMoveEvent_Rotation() {
		return (EReference)systemMoveEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSystemMoveEvent_Scale() {
		return (EAttribute)systemMoveEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeNameplateVisibilityEvent() {
		return changeNameplateVisibilityEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChangeNameplateVisibilityEvent_Visible() {
		return (EAttribute)changeNameplateVisibilityEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSphereOpenCloseEvent() {
		return sphereOpenCloseEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSphereOpenCloseEvent_ElementName() {
		return (EAttribute)sphereOpenCloseEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSphereOpenCloseEvent_NowOpen() {
		return (EAttribute)sphereOpenCloseEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAudioEvent() {
		return audioEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAudioEvent_PinId() {
		return (EAttribute)audioEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAudioEvent_Play() {
		return (EAttribute)audioEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAudioEvent_PlaybackTime() {
		return (EAttribute)audioEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISAVector3() {
		return isaVector3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_X() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_Y() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getISAVector3_Z() {
		return (EAttribute)isaVector3EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRelationType() {
		return relationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDirection() {
		return directionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getElementType() {
		return elementTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplayerFactory getMultiplayerFactory() {
		return (MultiplayerFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		eventLogEClass = createEClass(EVENT_LOG);
		createEReference(eventLogEClass, EVENT_LOG__LOG);

		abstractEventEClass = createEClass(ABSTRACT_EVENT);
		createEAttribute(abstractEventEClass, ABSTRACT_EVENT__TIMESTAMP);
		createEAttribute(abstractEventEClass, ABSTRACT_EVENT__SYSTEM_NAME);
		createEAttribute(abstractEventEClass, ABSTRACT_EVENT__CLIENT_ID);

		showRelationEventEClass = createEClass(SHOW_RELATION_EVENT);
		createEAttribute(showRelationEventEClass, SHOW_RELATION_EVENT__ELEMENT_NAME);
		createEAttribute(showRelationEventEClass, SHOW_RELATION_EVENT__RELATION_TYPE);
		createEAttribute(showRelationEventEClass, SHOW_RELATION_EVENT__DIRECTION);

		clearOneRelationEventEClass = createEClass(CLEAR_ONE_RELATION_EVENT);
		createEAttribute(clearOneRelationEventEClass, CLEAR_ONE_RELATION_EVENT__ELEMENT_NAME);
		createEAttribute(clearOneRelationEventEClass, CLEAR_ONE_RELATION_EVENT__RELATION_TYPE);
		createEAttribute(clearOneRelationEventEClass, CLEAR_ONE_RELATION_EVENT__DIRECTION);

		clearRelationsEventEClass = createEClass(CLEAR_RELATIONS_EVENT);
		createEAttribute(clearRelationsEventEClass, CLEAR_RELATIONS_EVENT__ELEMENT_NAME);

		clearAllRelationsEventEClass = createEClass(CLEAR_ALL_RELATIONS_EVENT);

		changeElementActiveStateEventEClass = createEClass(CHANGE_ELEMENT_ACTIVE_STATE_EVENT);
		createEAttribute(changeElementActiveStateEventEClass, CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_TYPE);
		createEAttribute(changeElementActiveStateEventEClass, CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ELEMENT_ID);
		createEAttribute(changeElementActiveStateEventEClass, CHANGE_ELEMENT_ACTIVE_STATE_EVENT__NEW_STATE);
		createEReference(changeElementActiveStateEventEClass, CHANGE_ELEMENT_ACTIVE_STATE_EVENT__POSITION);
		createEReference(changeElementActiveStateEventEClass, CHANGE_ELEMENT_ACTIVE_STATE_EVENT__ROTATION);

		systemMoveEventEClass = createEClass(SYSTEM_MOVE_EVENT);
		createEReference(systemMoveEventEClass, SYSTEM_MOVE_EVENT__POSITION);
		createEReference(systemMoveEventEClass, SYSTEM_MOVE_EVENT__ROTATION);
		createEAttribute(systemMoveEventEClass, SYSTEM_MOVE_EVENT__SCALE);

		changeNameplateVisibilityEventEClass = createEClass(CHANGE_NAMEPLATE_VISIBILITY_EVENT);
		createEAttribute(changeNameplateVisibilityEventEClass, CHANGE_NAMEPLATE_VISIBILITY_EVENT__VISIBLE);

		sphereOpenCloseEventEClass = createEClass(SPHERE_OPEN_CLOSE_EVENT);
		createEAttribute(sphereOpenCloseEventEClass, SPHERE_OPEN_CLOSE_EVENT__ELEMENT_NAME);
		createEAttribute(sphereOpenCloseEventEClass, SPHERE_OPEN_CLOSE_EVENT__NOW_OPEN);

		audioEventEClass = createEClass(AUDIO_EVENT);
		createEAttribute(audioEventEClass, AUDIO_EVENT__PIN_ID);
		createEAttribute(audioEventEClass, AUDIO_EVENT__PLAY);
		createEAttribute(audioEventEClass, AUDIO_EVENT__PLAYBACK_TIME);

		isaVector3EClass = createEClass(ISA_VECTOR3);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__X);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__Y);
		createEAttribute(isaVector3EClass, ISA_VECTOR3__Z);

		// Create enums
		relationTypeEEnum = createEEnum(RELATION_TYPE);
		directionEEnum = createEEnum(DIRECTION);
		elementTypeEEnum = createEEnum(ELEMENT_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ClientinfoPackage theClientinfoPackage = (ClientinfoPackage)EPackage.Registry.INSTANCE.getEPackage(ClientinfoPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theClientinfoPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		showRelationEventEClass.getESuperTypes().add(this.getAbstractEvent());
		clearOneRelationEventEClass.getESuperTypes().add(this.getAbstractEvent());
		clearRelationsEventEClass.getESuperTypes().add(this.getAbstractEvent());
		clearAllRelationsEventEClass.getESuperTypes().add(this.getAbstractEvent());
		changeElementActiveStateEventEClass.getESuperTypes().add(this.getAbstractEvent());
		systemMoveEventEClass.getESuperTypes().add(this.getAbstractEvent());
		changeNameplateVisibilityEventEClass.getESuperTypes().add(this.getAbstractEvent());
		sphereOpenCloseEventEClass.getESuperTypes().add(this.getAbstractEvent());
		audioEventEClass.getESuperTypes().add(this.getAbstractEvent());

		// Initialize classes, features, and operations; add parameters
		initEClass(eventLogEClass, EventLog.class, "EventLog", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventLog_Log(), this.getAbstractEvent(), null, "log", null, 0, -1, EventLog.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractEventEClass, AbstractEvent.class, "AbstractEvent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractEvent_Timestamp(), ecorePackage.getELong(), "timestamp", null, 1, 1, AbstractEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractEvent_SystemName(), ecorePackage.getEString(), "systemName", null, 1, 1, AbstractEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractEvent_ClientId(), ecorePackage.getEInt(), "clientId", null, 1, 1, AbstractEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(showRelationEventEClass, ShowRelationEvent.class, "ShowRelationEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getShowRelationEvent_ElementName(), ecorePackage.getEString(), "elementName", null, 1, 1, ShowRelationEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getShowRelationEvent_RelationType(), this.getRelationType(), "relationType", null, 1, 1, ShowRelationEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getShowRelationEvent_Direction(), this.getDirection(), "direction", null, 1, 1, ShowRelationEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clearOneRelationEventEClass, ClearOneRelationEvent.class, "ClearOneRelationEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClearOneRelationEvent_ElementName(), ecorePackage.getEString(), "elementName", null, 1, 1, ClearOneRelationEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClearOneRelationEvent_RelationType(), this.getRelationType(), "relationType", null, 1, 1, ClearOneRelationEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClearOneRelationEvent_Direction(), this.getDirection(), "direction", null, 1, 1, ClearOneRelationEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clearRelationsEventEClass, ClearRelationsEvent.class, "ClearRelationsEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClearRelationsEvent_ElementName(), ecorePackage.getEString(), "elementName", null, 1, 1, ClearRelationsEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clearAllRelationsEventEClass, ClearAllRelationsEvent.class, "ClearAllRelationsEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(changeElementActiveStateEventEClass, ChangeElementActiveStateEvent.class, "ChangeElementActiveStateEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getChangeElementActiveStateEvent_ElementType(), this.getElementType(), "elementType", null, 1, 1, ChangeElementActiveStateEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChangeElementActiveStateEvent_ElementId(), ecorePackage.getEInt(), "elementId", null, 1, 1, ChangeElementActiveStateEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getChangeElementActiveStateEvent_NewState(), ecorePackage.getEBoolean(), "newState", null, 1, 1, ChangeElementActiveStateEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChangeElementActiveStateEvent_Position(), this.getISAVector3(), null, "position", null, 0, 1, ChangeElementActiveStateEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChangeElementActiveStateEvent_Rotation(), this.getISAVector3(), null, "rotation", null, 0, 1, ChangeElementActiveStateEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(systemMoveEventEClass, SystemMoveEvent.class, "SystemMoveEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSystemMoveEvent_Position(), this.getISAVector3(), null, "position", null, 0, 1, SystemMoveEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemMoveEvent_Rotation(), this.getISAVector3(), null, "rotation", null, 0, 1, SystemMoveEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSystemMoveEvent_Scale(), ecorePackage.getEFloat(), "scale", null, 1, 1, SystemMoveEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(changeNameplateVisibilityEventEClass, ChangeNameplateVisibilityEvent.class, "ChangeNameplateVisibilityEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getChangeNameplateVisibilityEvent_Visible(), ecorePackage.getEBoolean(), "visible", null, 1, 1, ChangeNameplateVisibilityEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sphereOpenCloseEventEClass, SphereOpenCloseEvent.class, "SphereOpenCloseEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSphereOpenCloseEvent_ElementName(), ecorePackage.getEString(), "elementName", null, 1, 1, SphereOpenCloseEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSphereOpenCloseEvent_NowOpen(), ecorePackage.getEBoolean(), "nowOpen", null, 1, 1, SphereOpenCloseEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(audioEventEClass, AudioEvent.class, "AudioEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAudioEvent_PinId(), ecorePackage.getEInt(), "pinId", null, 1, 1, AudioEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAudioEvent_Play(), ecorePackage.getEBoolean(), "play", null, 1, 1, AudioEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAudioEvent_PlaybackTime(), ecorePackage.getEFloat(), "playbackTime", null, 1, 1, AudioEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(isaVector3EClass, ISAVector3.class, "ISAVector3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getISAVector3_X(), ecorePackage.getEFloat(), "x", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVector3_Y(), ecorePackage.getEFloat(), "y", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getISAVector3_Z(), ecorePackage.getEFloat(), "z", null, 1, 1, ISAVector3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(relationTypeEEnum, RelationType.class, "RelationType");
		addEEnumLiteral(relationTypeEEnum, RelationType.TYPE_REFERENCE);
		addEEnumLiteral(relationTypeEEnum, RelationType.METHOD_CALL);
		addEEnumLiteral(relationTypeEEnum, RelationType.FIELD_ACCESS);

		initEEnum(directionEEnum, Direction.class, "Direction");
		addEEnumLiteral(directionEEnum, Direction.FORWARD);
		addEEnumLiteral(directionEEnum, Direction.BACKWARD);

		initEEnum(elementTypeEEnum, ElementType.class, "ElementType");
		addEEnumLiteral(elementTypeEEnum, ElementType.OTHER);
		addEEnumLiteral(elementTypeEEnum, ElementType.VISUAL_ELEMENT);
		addEEnumLiteral(elementTypeEEnum, ElementType.PIN);

		// Create resource
		createResource(eNS_URI);
	}

} //MultiplayerPackageImpl

/**
 */
package dk.itu.cs.isa.storage.impl;

import dk.itu.cs.isa.storage.ModelStorageEntry;
import dk.itu.cs.isa.storage.StoragePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model Storage Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dk.itu.cs.isa.storage.impl.ModelStorageEntryImpl#getSystemName <em>System Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.impl.ModelStorageEntryImpl#getNumberOfClassifiers <em>Number Of Classifiers</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.impl.ModelStorageEntryImpl#getStructureModelFileName <em>Structure Model File Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.impl.ModelStorageEntryImpl#getQualityModelFileName <em>Quality Model File Name</em>}</li>
 *   <li>{@link dk.itu.cs.isa.storage.impl.ModelStorageEntryImpl#getVisualizationModelFileName <em>Visualization Model File Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelStorageEntryImpl extends MinimalEObjectImpl.Container implements ModelStorageEntry {
	/**
	 * The default value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemName()
	 * @generated
	 * @ordered
	 */
	protected static final String SYSTEM_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSystemName() <em>System Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystemName()
	 * @generated
	 * @ordered
	 */
	protected String systemName = SYSTEM_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberOfClassifiers() <em>Number Of Classifiers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfClassifiers()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_CLASSIFIERS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfClassifiers() <em>Number Of Classifiers</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfClassifiers()
	 * @generated
	 * @ordered
	 */
	protected int numberOfClassifiers = NUMBER_OF_CLASSIFIERS_EDEFAULT;

	/**
	 * The default value of the '{@link #getStructureModelFileName() <em>Structure Model File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStructureModelFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String STRUCTURE_MODEL_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStructureModelFileName() <em>Structure Model File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStructureModelFileName()
	 * @generated
	 * @ordered
	 */
	protected String structureModelFileName = STRUCTURE_MODEL_FILE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getQualityModelFileName() <em>Quality Model File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualityModelFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String QUALITY_MODEL_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getQualityModelFileName() <em>Quality Model File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualityModelFileName()
	 * @generated
	 * @ordered
	 */
	protected String qualityModelFileName = QUALITY_MODEL_FILE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVisualizationModelFileName() <em>Visualization Model File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisualizationModelFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String VISUALIZATION_MODEL_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVisualizationModelFileName() <em>Visualization Model File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisualizationModelFileName()
	 * @generated
	 * @ordered
	 */
	protected String visualizationModelFileName = VISUALIZATION_MODEL_FILE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelStorageEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StoragePackage.Literals.MODEL_STORAGE_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSystemName() {
		return systemName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSystemName(String newSystemName) {
		String oldSystemName = systemName;
		systemName = newSystemName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StoragePackage.MODEL_STORAGE_ENTRY__SYSTEM_NAME, oldSystemName, systemName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNumberOfClassifiers() {
		return numberOfClassifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberOfClassifiers(int newNumberOfClassifiers) {
		int oldNumberOfClassifiers = numberOfClassifiers;
		numberOfClassifiers = newNumberOfClassifiers;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StoragePackage.MODEL_STORAGE_ENTRY__NUMBER_OF_CLASSIFIERS, oldNumberOfClassifiers, numberOfClassifiers));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStructureModelFileName() {
		return structureModelFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStructureModelFileName(String newStructureModelFileName) {
		String oldStructureModelFileName = structureModelFileName;
		structureModelFileName = newStructureModelFileName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StoragePackage.MODEL_STORAGE_ENTRY__STRUCTURE_MODEL_FILE_NAME, oldStructureModelFileName, structureModelFileName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getQualityModelFileName() {
		return qualityModelFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setQualityModelFileName(String newQualityModelFileName) {
		String oldQualityModelFileName = qualityModelFileName;
		qualityModelFileName = newQualityModelFileName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StoragePackage.MODEL_STORAGE_ENTRY__QUALITY_MODEL_FILE_NAME, oldQualityModelFileName, qualityModelFileName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVisualizationModelFileName() {
		return visualizationModelFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVisualizationModelFileName(String newVisualizationModelFileName) {
		String oldVisualizationModelFileName = visualizationModelFileName;
		visualizationModelFileName = newVisualizationModelFileName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StoragePackage.MODEL_STORAGE_ENTRY__VISUALIZATION_MODEL_FILE_NAME, oldVisualizationModelFileName, visualizationModelFileName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StoragePackage.MODEL_STORAGE_ENTRY__SYSTEM_NAME:
				return getSystemName();
			case StoragePackage.MODEL_STORAGE_ENTRY__NUMBER_OF_CLASSIFIERS:
				return getNumberOfClassifiers();
			case StoragePackage.MODEL_STORAGE_ENTRY__STRUCTURE_MODEL_FILE_NAME:
				return getStructureModelFileName();
			case StoragePackage.MODEL_STORAGE_ENTRY__QUALITY_MODEL_FILE_NAME:
				return getQualityModelFileName();
			case StoragePackage.MODEL_STORAGE_ENTRY__VISUALIZATION_MODEL_FILE_NAME:
				return getVisualizationModelFileName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StoragePackage.MODEL_STORAGE_ENTRY__SYSTEM_NAME:
				setSystemName((String)newValue);
				return;
			case StoragePackage.MODEL_STORAGE_ENTRY__NUMBER_OF_CLASSIFIERS:
				setNumberOfClassifiers((Integer)newValue);
				return;
			case StoragePackage.MODEL_STORAGE_ENTRY__STRUCTURE_MODEL_FILE_NAME:
				setStructureModelFileName((String)newValue);
				return;
			case StoragePackage.MODEL_STORAGE_ENTRY__QUALITY_MODEL_FILE_NAME:
				setQualityModelFileName((String)newValue);
				return;
			case StoragePackage.MODEL_STORAGE_ENTRY__VISUALIZATION_MODEL_FILE_NAME:
				setVisualizationModelFileName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StoragePackage.MODEL_STORAGE_ENTRY__SYSTEM_NAME:
				setSystemName(SYSTEM_NAME_EDEFAULT);
				return;
			case StoragePackage.MODEL_STORAGE_ENTRY__NUMBER_OF_CLASSIFIERS:
				setNumberOfClassifiers(NUMBER_OF_CLASSIFIERS_EDEFAULT);
				return;
			case StoragePackage.MODEL_STORAGE_ENTRY__STRUCTURE_MODEL_FILE_NAME:
				setStructureModelFileName(STRUCTURE_MODEL_FILE_NAME_EDEFAULT);
				return;
			case StoragePackage.MODEL_STORAGE_ENTRY__QUALITY_MODEL_FILE_NAME:
				setQualityModelFileName(QUALITY_MODEL_FILE_NAME_EDEFAULT);
				return;
			case StoragePackage.MODEL_STORAGE_ENTRY__VISUALIZATION_MODEL_FILE_NAME:
				setVisualizationModelFileName(VISUALIZATION_MODEL_FILE_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StoragePackage.MODEL_STORAGE_ENTRY__SYSTEM_NAME:
				return SYSTEM_NAME_EDEFAULT == null ? systemName != null : !SYSTEM_NAME_EDEFAULT.equals(systemName);
			case StoragePackage.MODEL_STORAGE_ENTRY__NUMBER_OF_CLASSIFIERS:
				return numberOfClassifiers != NUMBER_OF_CLASSIFIERS_EDEFAULT;
			case StoragePackage.MODEL_STORAGE_ENTRY__STRUCTURE_MODEL_FILE_NAME:
				return STRUCTURE_MODEL_FILE_NAME_EDEFAULT == null ? structureModelFileName != null : !STRUCTURE_MODEL_FILE_NAME_EDEFAULT.equals(structureModelFileName);
			case StoragePackage.MODEL_STORAGE_ENTRY__QUALITY_MODEL_FILE_NAME:
				return QUALITY_MODEL_FILE_NAME_EDEFAULT == null ? qualityModelFileName != null : !QUALITY_MODEL_FILE_NAME_EDEFAULT.equals(qualityModelFileName);
			case StoragePackage.MODEL_STORAGE_ENTRY__VISUALIZATION_MODEL_FILE_NAME:
				return VISUALIZATION_MODEL_FILE_NAME_EDEFAULT == null ? visualizationModelFileName != null : !VISUALIZATION_MODEL_FILE_NAME_EDEFAULT.equals(visualizationModelFileName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (systemName: ");
		result.append(systemName);
		result.append(", numberOfClassifiers: ");
		result.append(numberOfClassifiers);
		result.append(", structureModelFileName: ");
		result.append(structureModelFileName);
		result.append(", qualityModelFileName: ");
		result.append(qualityModelFileName);
		result.append(", visualizationModelFileName: ");
		result.append(visualizationModelFileName);
		result.append(')');
		return result.toString();
	}

} //ModelStorageEntryImpl

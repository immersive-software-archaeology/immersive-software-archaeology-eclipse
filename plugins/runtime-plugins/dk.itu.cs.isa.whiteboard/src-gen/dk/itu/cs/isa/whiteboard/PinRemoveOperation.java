/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pin Remove Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPinRemoveOperation()
 * @model
 * @generated
 */
public interface PinRemoveOperation extends AbstractPinOperation {
} // PinRemoveOperation

package dk.itu.cs.isa.eclipse.ui.views.server;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import dk.itu.cs.isa.coalescence.server.ISAServer;
import dk.itu.cs.isa.coalescence.server.ISAServer.ServerState;
import dk.itu.cs.isa.eclipse.ui.ImageHelper;
import dk.itu.cs.isa.events.ISAEventSystem;

public abstract class ISAServerStatusView extends ViewPart {
	
	protected ISAServer server;
	
	private boolean disposed;

	private Label statusIconLabel;
	private Label statusTextLabel;

	private Text addressText;
	
	private Button startButton;
	private Button stopButton;

	private Image onlineStatusImage;
	private Image offlineStatusImage;
	private Image startServerImage;
	private Image stopServerImage;
	private Image browserImage;

	public ISAServerStatusView() {
		super();
		setServer();
	}
	
	protected abstract void setServer();

	@Override
	public void setFocus() {
		startButton.setFocus();
	}
	
	protected void refreshThreadSafe(ServerState serverState, String serverAddress, boolean canBeStarted, boolean canBeStopped) {
		if (serverState.equals(ServerState.ONLINE)) {
			statusIconLabel.setImage(onlineStatusImage);
			statusTextLabel.setText("Online");
		} else if (serverState.equals(ServerState.STARTING)) {
			statusIconLabel.setImage(offlineStatusImage);
			statusTextLabel.setText("Starting up...");
		} else if (serverState.equals(ServerState.STOPPING)) {
			statusIconLabel.setImage(offlineStatusImage);
			statusTextLabel.setText("Shutting down...");
		} else if (serverState.equals(ServerState.OFFLINE)) {
			statusIconLabel.setImage(offlineStatusImage);
			statusTextLabel.setText("Offline");
		} else {
			statusIconLabel.setImage(offlineStatusImage);
			statusTextLabel.setText("Unknown Status!");
		}
		
		if(canBeStarted)
			startButton.setEnabled(true);
		else
			startButton.setEnabled(false);
		
		if(canBeStopped)
			stopButton.setEnabled(true);
		else
			stopButton.setEnabled(false);
		
		addressText.setText(serverAddress);
	}

	@Override
	public void createPartControl(Composite parent) {
		onlineStatusImage = ImageHelper.getImage("icons/icon-16-server-online.png");
		offlineStatusImage = ImageHelper.getImage("icons/icon-16-server-offline.png");
		startServerImage = ImageHelper.getImage("icons/icon-16-server-start.png");
		stopServerImage = ImageHelper.getImage("icons/icon-16-server-stop.png");
		browserImage = ImageHelper.getImage("icons/icon-16-browser.png");
		
		GridLayout layout = new GridLayout(5, false);
		layout.horizontalSpacing = 8;
		layout.verticalSpacing = 16;
		layout.marginLeft = 8;
		layout.marginRight = 8;
		parent.setLayout(layout);

		Label titleTextLabel = new Label(parent, SWT.CENTER | SWT.WRAP);
		titleTextLabel.setText(getTitle());
		titleTextLabel.setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));
		titleTextLabel.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 5, 1));

		// Status

		Label statusLabel = new Label(parent, SWT.RIGHT);
		statusLabel.setText("Server Status:");
//		statusLabel.setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));
		statusLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 2, 1));

		statusIconLabel = new Label(parent, SWT.LEFT);
		statusIconLabel.setImage(offlineStatusImage);
		statusIconLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

		statusTextLabel = new Label(parent, SWT.LEFT);
		statusTextLabel.setText("Checking status...");
		statusTextLabel.setFont(JFaceResources.getFontRegistry().getItalic(JFaceResources.DEFAULT_FONT));
		statusTextLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		
		// Buttons

		startButton = new Button(parent, SWT.PUSH | SWT.CENTER);
		startButton.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, true, false, 1, 1));
//		startButton.setText("Start");
		startButton.setImage(startServerImage);
		startButton.setEnabled(false);
		startButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				if (e.type == SWT.Selection) {
					if(server.startServer()) {
						startButton.setEnabled(false);
					}
				}
			}
		});

		stopButton = new Button(parent, SWT.PUSH | SWT.CENTER);
		stopButton.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
//		stopButton.setText("Stop");
		stopButton.setImage(stopServerImage);
		stopButton.setEnabled(false);
		stopButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				if (event.type == SWT.Selection) {
					if(server.stopServer()) {
						stopButton.setEnabled(false);
					}
				}
			}
		});
		
		// Address
		
		addressText = new Text(parent, SWT.CENTER | SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		addressText.setText("...");
//		statusLabel.setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));
		addressText.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 2, 1));
		
		Button browserButton = new Button(parent, SWT.PUSH | SWT.CENTER);
		browserButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		browserButton.setImage(browserImage);
		browserButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				if (e.type == SWT.Selection) {
					StringSelection stringSelection = new StringSelection(addressText.getText());
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard.setContents(stringSelection, null);
					
					if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
					    try {
							Desktop.getDesktop().browse(new URI(addressText.getText()));
						} catch (IOException | URISyntaxException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});

		Runnable serverStatusRefreshRunnable = new Runnable() {
			@Override
			public void run() {
				while (true) {
					if(disposed)
						return;
					
					try {
						ServerState serverState = server.getServerState();
						String serverAddress = server.getServerAddress();
						boolean canBeStarted = server.canBeStarted();
						boolean canBeStopped = server.canBeStopped();
						Display.getDefault().asyncExec(() -> {
							if(!disposed) {
								refreshThreadSafe(serverState, serverAddress, canBeStarted, canBeStopped);
								parent.layout();
							}
						});
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						ISAEventSystem.fireErrorMessage("Interrupted exception for server status refresh "
								+ "thread. Will exit the thread and stop updating server state", e);
						return;
					}
				}
			}
		};
		(new Thread(serverStatusRefreshRunnable, "ISAServerStatusUpdateThread: " + this.getClass().getSimpleName())).start();
	}

	@Override
	public void dispose() {
		disposed = true;
		onlineStatusImage.dispose();
		offlineStatusImage.dispose();
		startServerImage.dispose();
		stopServerImage.dispose();
		browserImage.dispose();
		super.dispose();
	}

}

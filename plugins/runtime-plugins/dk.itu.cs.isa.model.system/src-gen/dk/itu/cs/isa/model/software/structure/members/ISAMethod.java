/**
 */
package dk.itu.cs.isa.model.software.structure.members;

import dk.itu.cs.isa.model.software.structure.misc.ISAParametrizableElement;
import dk.itu.cs.isa.model.software.structure.misc.ISATypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISA Method</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.model.software.structure.members.MembersPackage#getISAMethod()
 * @model abstract="true"
 * @generated
 */
public interface ISAMethod extends ISAMember, ISATypedElement, ISAParametrizableElement {
} // ISAMethod

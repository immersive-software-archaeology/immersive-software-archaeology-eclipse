package dk.itu.cs.isa.model.system.creation.architecture.coalescence;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jetty.server.Request;

import dk.itu.cs.isa.coalescence.server.requests.ISAAbstractCoalescenceRequestHandler;
import dk.itu.cs.isa.model.software.structure.ISASoftwareSystemModel;
import dk.itu.cs.isa.model.software.structure.architecture.ArchitectureFactory;
import dk.itu.cs.isa.model.software.structure.architecture.ISACluster;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifier;
import dk.itu.cs.isa.model.software.structure.classifiers.ISAClassifierContainer;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAPackageRoot;
import dk.itu.cs.isa.model.software.structure.filesystem.ISAProject;
import dk.itu.cs.isa.model.software.structure.misc.ISANamedSoftwareElement;
import dk.itu.cs.isa.model.system.architecture.hierarchical.divisive.dbscan.ISADBSCANClustererManager;
import dk.itu.cs.isa.model.system.architecture.util.ISAClusteringCachingUtil;
import dk.itu.cs.isa.model.system.coalescence.openfile.OpenFileListener;
import dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ClusteringFactory;
import dk.itu.cs.isa.model.system.creation.architecture.coalescence.clustering.ISASimpleCluster;
import dk.itu.cs.isa.storage.ISAModelStorage;
import dk.itu.cs.isa.util.EcoreUtilISA;

public class ClusterRequestHandler extends ISAAbstractCoalescenceRequestHandler {
	
	private List<OpenFileListener> listeners = new ArrayList<>();

	public void registerListener(OpenFileListener newListener) {
		listeners.add(newListener);
	}
	
	
	
	@Override
	public void handle(Request baseRequest, HttpServletResponse response) throws IOException {
		String systemName = baseRequest.getParameter("systemName");
		String packageName = baseRequest.getParameter("packageName");
		
		if(systemName == null || packageName == null) {
			String examplePath1 = "systemName=JetUML&packageName=org.jetuml.rendering";
			printInvalidParametersHTMLPage(baseRequest, response, null, examplePath1);
			return;
		}
		
		try {
			ISASoftwareSystemModel structureModel = ISAModelStorage.INSTANCE.loadStructureModel(systemName);
			if(structureModel == null)
				throw new NullPointerException("Cannot load system model for \"" + systemName + "\". Did the meta model change?");

			ISANamedSoftwareElement elementToCluster = structureModel.getSystemElementByName(packageName);

			ISASimpleCluster result;
			if(elementToCluster instanceof ISAProject || elementToCluster instanceof ISAPackageRoot) {
				// These do not contain classifiers to split directly, don't do anything here...
				// Instead, the front-end will just display the respective child elements
				result = ClusteringFactory.eINSTANCE.createISASimpleCluster();
			}
			else if(elementToCluster instanceof ISAClassifierContainer) {
				ISAClassifierContainer isaPackage = (ISAClassifierContainer) elementToCluster;
				if(isaPackage.getClassifiers().size() <= 3) {
					// Too few elements to actually do clustering...
					result = ClusteringFactory.eINSTANCE.createISASimpleCluster();
					for(ISAClassifier classifier : isaPackage.getClassifiers())
						result.getElementNames().add(classifier.getFullyQualifiedName());
				}
				else {
					ISAClusteringCachingUtil.INSTANCE.reset();
					
					// Initialize the set of singletons as the classifiers of the package
					List<ISACluster> initialSingletonClusters = new LinkedList<>();
					for(ISAClassifier classifier : isaPackage.getClassifiers()) {
						ISACluster newSingletonCluster = ArchitectureFactory.eINSTANCE.createISACluster();
						newSingletonCluster.getClassifiers().add(classifier);
						newSingletonCluster.setName("Singleton Cluster " + (classifier).getFullyQualifiedName());
						initialSingletonClusters.add(newSingletonCluster);
					}
					
					ISADBSCANClustererManager dbScanClusterManager = new ISADBSCANClustererManager(initialSingletonClusters, null, 1, initialSingletonClusters.size()-1);
					ISACluster rootCluster = dbScanClusterManager.startClustering(new NullProgressMonitor());
	
					result = createClusterStructure(rootCluster);
				}
			}
			else throw new RuntimeException("Unexpected type of element to cluster: " + elementToCluster.getFullyQualifiedName());

			response.setContentType("application/xml; charset=utf-8");
			response.setStatus(HttpServletResponse.SC_OK);
			
			PrintWriter out = response.getWriter();
			out.println(EcoreUtilISA.getXMLStringForModel(result));
			out.close();
			
			baseRequest.setHandled(true);
		}
		catch(Exception e) {
			e.printStackTrace();
			
			printHTMLPage(baseRequest, response,
					HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Clustering failed",
					e,
					"The clustering operation could not be executed.");
		}
	}

	private ISASimpleCluster createClusterStructure(ISACluster cluster) {
		ISASimpleCluster result = ClusteringFactory.eINSTANCE.createISASimpleCluster();
		
		for(ISAClassifier classifier : cluster.getClassifiers())
			result.getElementNames().add(classifier.getFullyQualifiedName());
		for(ISACluster subCluster : cluster.getChildClusters())
			result.getSubClusters().add(createClusterStructure(subCluster));
		
		return result;
	}

}


















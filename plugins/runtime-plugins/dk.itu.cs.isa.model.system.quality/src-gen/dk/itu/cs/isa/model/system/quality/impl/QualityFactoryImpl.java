/**
 */
package dk.itu.cs.isa.model.system.quality.impl;

import dk.itu.cs.isa.model.system.quality.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class QualityFactoryImpl extends EFactoryImpl implements QualityFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static QualityFactory init() {
		try {
			QualityFactory theQualityFactory = (QualityFactory)EPackage.Registry.INSTANCE.getEFactory(QualityPackage.eNS_URI);
			if (theQualityFactory != null) {
				return theQualityFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new QualityFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualityFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case QualityPackage.ISA_SOFTWARE_SYSTEM_QUALITY_MODEL: return createISASoftwareSystemQualityModel();
			case QualityPackage.ISA_CLASSIFIER_QUALITY_ENTRY: return createISAClassifierQualityEntry();
			case QualityPackage.ISA_MEASURE: return createISAMeasure();
			case QualityPackage.ISA_ISSUE: return createISAIssue();
			case QualityPackage.ISA_FILE_PASSAGE: return createISAFilePassage();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case QualityPackage.ISA_QUALITY_ISSUE_TYPE:
				return createISAQualityIssueTypeFromString(eDataType, initialValue);
			case QualityPackage.ISA_QUALITY_ISSUE_SEVERITY:
				return createISAQualityIssueSeverityFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case QualityPackage.ISA_QUALITY_ISSUE_TYPE:
				return convertISAQualityIssueTypeToString(eDataType, instanceValue);
			case QualityPackage.ISA_QUALITY_ISSUE_SEVERITY:
				return convertISAQualityIssueSeverityToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISASoftwareSystemQualityModel createISASoftwareSystemQualityModel() {
		ISASoftwareSystemQualityModelImpl isaSoftwareSystemQualityModel = new ISASoftwareSystemQualityModelImpl();
		return isaSoftwareSystemQualityModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAClassifierQualityEntry createISAClassifierQualityEntry() {
		ISAClassifierQualityEntryImpl isaClassifierQualityEntry = new ISAClassifierQualityEntryImpl();
		return isaClassifierQualityEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAMeasure createISAMeasure() {
		ISAMeasureImpl isaMeasure = new ISAMeasureImpl();
		return isaMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAIssue createISAIssue() {
		ISAIssueImpl isaIssue = new ISAIssueImpl();
		return isaIssue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ISAFilePassage createISAFilePassage() {
		ISAFilePassageImpl isaFilePassage = new ISAFilePassageImpl();
		return isaFilePassage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAQualityIssueType createISAQualityIssueTypeFromString(EDataType eDataType, String initialValue) {
		ISAQualityIssueType result = ISAQualityIssueType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertISAQualityIssueTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAQualityIssueSeverity createISAQualityIssueSeverityFromString(EDataType eDataType, String initialValue) {
		ISAQualityIssueSeverity result = ISAQualityIssueSeverity.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertISAQualityIssueSeverityToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public QualityPackage getQualityPackage() {
		return (QualityPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static QualityPackage getPackage() {
		return QualityPackage.eINSTANCE;
	}

} //QualityFactoryImpl

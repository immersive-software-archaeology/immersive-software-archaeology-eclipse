/**
 */
package dk.itu.cs.isa.whiteboard;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Paint Erase Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dk.itu.cs.isa.whiteboard.WhiteboardPackage#getPaintEraseOperation()
 * @model
 * @generated
 */
public interface PaintEraseOperation extends AbstractDrawOperation {
} // PaintEraseOperation

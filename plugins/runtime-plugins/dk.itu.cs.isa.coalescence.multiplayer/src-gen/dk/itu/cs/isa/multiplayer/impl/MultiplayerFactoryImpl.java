/**
 */
package dk.itu.cs.isa.multiplayer.impl;

import dk.itu.cs.isa.multiplayer.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MultiplayerFactoryImpl extends EFactoryImpl implements MultiplayerFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MultiplayerFactory init() {
		try {
			MultiplayerFactory theMultiplayerFactory = (MultiplayerFactory)EPackage.Registry.INSTANCE.getEFactory(MultiplayerPackage.eNS_URI);
			if (theMultiplayerFactory != null) {
				return theMultiplayerFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MultiplayerFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplayerFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MultiplayerPackage.EVENT_LOG: return createEventLog();
			case MultiplayerPackage.SHOW_RELATION_EVENT: return createShowRelationEvent();
			case MultiplayerPackage.CLEAR_ONE_RELATION_EVENT: return createClearOneRelationEvent();
			case MultiplayerPackage.CLEAR_RELATIONS_EVENT: return createClearRelationsEvent();
			case MultiplayerPackage.CLEAR_ALL_RELATIONS_EVENT: return createClearAllRelationsEvent();
			case MultiplayerPackage.CHANGE_ELEMENT_ACTIVE_STATE_EVENT: return createChangeElementActiveStateEvent();
			case MultiplayerPackage.SYSTEM_MOVE_EVENT: return createSystemMoveEvent();
			case MultiplayerPackage.CHANGE_NAMEPLATE_VISIBILITY_EVENT: return createChangeNameplateVisibilityEvent();
			case MultiplayerPackage.SPHERE_OPEN_CLOSE_EVENT: return createSphereOpenCloseEvent();
			case MultiplayerPackage.AUDIO_EVENT: return createAudioEvent();
			case MultiplayerPackage.ISA_VECTOR3: return createISAVector3();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MultiplayerPackage.RELATION_TYPE:
				return createRelationTypeFromString(eDataType, initialValue);
			case MultiplayerPackage.DIRECTION:
				return createDirectionFromString(eDataType, initialValue);
			case MultiplayerPackage.ELEMENT_TYPE:
				return createElementTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MultiplayerPackage.RELATION_TYPE:
				return convertRelationTypeToString(eDataType, instanceValue);
			case MultiplayerPackage.DIRECTION:
				return convertDirectionToString(eDataType, instanceValue);
			case MultiplayerPackage.ELEMENT_TYPE:
				return convertElementTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventLog createEventLog() {
		EventLogImpl eventLog = new EventLogImpl();
		return eventLog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowRelationEvent createShowRelationEvent() {
		ShowRelationEventImpl showRelationEvent = new ShowRelationEventImpl();
		return showRelationEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClearOneRelationEvent createClearOneRelationEvent() {
		ClearOneRelationEventImpl clearOneRelationEvent = new ClearOneRelationEventImpl();
		return clearOneRelationEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClearRelationsEvent createClearRelationsEvent() {
		ClearRelationsEventImpl clearRelationsEvent = new ClearRelationsEventImpl();
		return clearRelationsEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClearAllRelationsEvent createClearAllRelationsEvent() {
		ClearAllRelationsEventImpl clearAllRelationsEvent = new ClearAllRelationsEventImpl();
		return clearAllRelationsEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeElementActiveStateEvent createChangeElementActiveStateEvent() {
		ChangeElementActiveStateEventImpl changeElementActiveStateEvent = new ChangeElementActiveStateEventImpl();
		return changeElementActiveStateEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemMoveEvent createSystemMoveEvent() {
		SystemMoveEventImpl systemMoveEvent = new SystemMoveEventImpl();
		return systemMoveEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeNameplateVisibilityEvent createChangeNameplateVisibilityEvent() {
		ChangeNameplateVisibilityEventImpl changeNameplateVisibilityEvent = new ChangeNameplateVisibilityEventImpl();
		return changeNameplateVisibilityEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SphereOpenCloseEvent createSphereOpenCloseEvent() {
		SphereOpenCloseEventImpl sphereOpenCloseEvent = new SphereOpenCloseEventImpl();
		return sphereOpenCloseEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AudioEvent createAudioEvent() {
		AudioEventImpl audioEvent = new AudioEventImpl();
		return audioEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISAVector3 createISAVector3() {
		ISAVector3Impl isaVector3 = new ISAVector3Impl();
		return isaVector3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationType createRelationTypeFromString(EDataType eDataType, String initialValue) {
		RelationType result = RelationType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRelationTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Direction createDirectionFromString(EDataType eDataType, String initialValue) {
		Direction result = Direction.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDirectionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementType createElementTypeFromString(EDataType eDataType, String initialValue) {
		ElementType result = ElementType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertElementTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplayerPackage getMultiplayerPackage() {
		return (MultiplayerPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MultiplayerPackage getPackage() {
		return MultiplayerPackage.eINSTANCE;
	}

} //MultiplayerFactoryImpl

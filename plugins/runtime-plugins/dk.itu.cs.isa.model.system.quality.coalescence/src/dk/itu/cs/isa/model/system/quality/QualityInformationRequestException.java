package dk.itu.cs.isa.model.system.quality;

public class QualityInformationRequestException extends Exception {

	private static final long serialVersionUID = 3405731081238484148L;
	private String title;
	
	public QualityInformationRequestException(String title, String message) {
		super(message);
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

}
